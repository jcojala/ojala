/*global $:false */
'use strict';

$(document).ready(function(){
	$( '.side-nav' ).hover(function() {
		$(this).toggleClass('box-open');
		$('.main-container').toggleClass('container-open');
	});
	$('.combobox').combobox();
});