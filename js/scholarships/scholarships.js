'use strict';

var scholarship = {
	ALERT_DELAY: 2000,
	tagsOption: {
		width: '100%',
		defaultText:'',
		placeholderColor : '#666666',
		onAddTag: function(){
			var emails =$('#emails').val().split(','),
			       re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

			for (var i = 0; i < emails.length ; i++) {
				if(!re.test(emails[i])){
					$('#emails_tagsinput span.tag:contains("' + emails[i] + '")').addClass('invalid');
				}
			}
		}
	},

	showSuccessAlert: function(text){
		$('#alert-bar').hide()
			.removeClass('alert-danger hidden')
			.addClass('alert-success')
			.text(text)
			.fadeIn().delay(scholarship.ALERT_DELAY).fadeOut();
	},

	showErrorAlert: function(text){
		$('#alert-bar').hide()
			.removeClass('alert-success hidden')
			.addClass('alert-danger')
			.text(text)
			.fadeIn().delay(scholarship.ALERT_DELAY).fadeOut();
	},

	shareFacebook: function(url, text){
		FB.ui(
		  {
		    method: 'feed',
		    name: $('.page-header h1').text(),
		    link: url,
		    picture: 'https://oja.la/images/social/ojala.jpg',
		    caption: $('.page-header p').text(),
		    description: text,
		  },
		  function(response) {
			if (response && response.post_id) {
				scholarship.showSuccessAlert('Invitación compartida correctamente');
			} else {
				scholarship.showErrorAlert('No se compartió la invitación');
			}
		  }
		);
	},

	shareTwitter: function(text, url){
		var  w = 600,
		 h = 250,
		 left = (screen.width/2)-(w/2),
		 top = (screen.height/2)-(h/2),
		 winParams = 'titlebar=0,toolbar=0,width='+ w + ',height='+h+',menubar=0,left='+ left + ',top='+top,
		 url = 'https://twitter.com/share?text=' + text + '&url=' + url + '&via=oja_la&lang=es';

		window.open(url,'twitterWin',winParams);
	},

	copyUrlEvent: function(){
		client.on( 'load', function(client) {
			client.on( "complete", function(client, args) {
				scholarship.showSuccessAlert('Enlace copiado: ' + args.text);
			});
		} );
	},

	sendEmails: function(url, emails, link){
		$.ajax(url, {
			type: 'POST',
			dataType: 'json',
			data: {
				emails: emails,
				link: link
			},
			beforeSend: function(){
				$('#sendEmails').prop('disabled', true);
				$('#sendEmails span').text('Enviando...');
			},
			error: function(data){
				scholarship.showErrorAlert('Error: ' + data.msg);

			},
			success: function(data){
				if(!data.error){
					$('#emails').importTags('');
					scholarship.showSuccessAlert(data.msg);

				} else {
					scholarship.showErrorAlert('Error: ' + data.msg);
				}
			},
			complete: function(){
				$('#sendEmails span').text('Enviar');
				$('#sendEmails').prop('disabled', false);
			},
		});
	},
};

jQuery(document).ready(function($)
{
	$('#emails').tagsInput(scholarship.tagsOption);

	$('button.facebook').on('click', function(){
		scholarship.shareFacebook($(this).data('url'), $(this).data('text'));
	});

	$('button.twitter').on('click', function(){
		scholarship.shareTwitter( $(this).data('text'),$(this).data('url'));
	});

	$('#sendEmails').on('click', function(e){
		e.preventDefault();
		scholarship.sendEmails($(this).data('url'), $('#emails').val(), $('#mailLink').val());
	});

	scholarship.copyUrlEvent();
});