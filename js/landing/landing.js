/*global $:false */
/*global baseUrl:false */
/*global olark:false */
'use strict';
var ojaLanding = {
	id_user: null,
	email: null,
	originalLeft: null,
	chatAnswers: {},
	apiAnswers: {},
	animation: {
		queue: null,
		currentSlide: 'Main page',
		limit: null,
		callback: function(){}
	},
	activateEventMessages: function(){
		var wistiaEmbed = $("#wistia_embed")[0].wistiaApi;
		wistiaEmbed.bind("play", function () {
			olark('api.chat.sendNotificationToOperator', {body: 'El usuario ha iniciado la reproducción del vídeo'});
		});
		wistiaEmbed.bind("pause", function () {
			olark('api.chat.sendNotificationToOperator', {body: 'El usuario ha pausado el vídeo'});
		});
		wistiaEmbed.bind("end", function () {
			olark('api.chat.sendNotificationToOperator', {body: 'El usuario ha terminado de ver el vídeo'});
		});
	},
	animateSlide: function(index, limit, callback) {
		if(index <= limit){
			$('.keys-nav').show();
			ojaLanding.animation.currentSlide = index;
			ojaLanding.animation.sliding = true;
			ojaLanding.animation.limit = limit;
			ojaLanding.animation.callback = callback;

			$('.slide' + index).fadeIn('slow', function(){
				ojaLanding.animation.queue = setTimeout(
					"$('.slide' + ojaLanding.animation.currentSlide).fadeOut(); ojaLanding.animateSlide(ojaLanding.animation.currentSlide + 1, ojaLanding.animation.limit, ojaLanding.animation.callback);",
					2500
				);
			});
		} else {
			$('.keys-nav').hide();
			callback();
		}
	},
	keyNavigation: function(event) {
		if(event.keyCode === 37)
		{
			var blockSlides = [1,15, 8]; //Slides que no permiten ir para atrás,
			if(ojaLanding.animation.queue && !isNaN(ojaLanding.animation.currentSlide) && blockSlides.indexOf(ojaLanding.animation.currentSlide) === -1){
				clearTimeout(ojaLanding.animation.queue);
				ojaLanding.animation.queue = null;
				$('.slide' + ojaLanding.animation.currentSlide).fadeOut();
				ojaLanding.animateSlide(
					ojaLanding.animation.currentSlide - 1,
					ojaLanding.animation.limit,
					ojaLanding.animation.callback
				);
			}
		} else if(event.keyCode === 39 && !isNaN(ojaLanding.animation.currentSlide)) {
			var blockSlides = [8];

			if(blockSlides.indexOf(ojaLanding.animation.currentSlide) === -1) {
				clearTimeout(ojaLanding.animation.queue);
				ojaLanding.animation.queue = null;
				$('.slide' + ojaLanding.animation.currentSlide).fadeOut();
				ojaLanding.animateSlide(
					ojaLanding.animation.currentSlide + 1,
					ojaLanding.animation.limit,
					ojaLanding.animation.callback
				);
			}
		}
	},
	register: function(email){
		$.ajax({
			url: baseUrl + '/api/regFromLanding',
				type: 'post',
				data: {email: ojaLanding.email}
		}).done(function(data) {
			if(data){
				ojaLanding.id_user = data;
			}
		});
	},
	startChat: function() {
		$('#main, #how-works, #questions-wrapper, #questions-warn, #registration-form').hide();
		$('#main, #how-works, #questions-wrapper, #questions-warn, #registration-form').addClass('hide');
		
		ojaLanding.animateSlide(15,15, function(){
			$('#chat').fadeIn();
			var i = 1, msg = $('#username').val() + ' (' + ojaLanding.email + ') se ha conectado y ha respondido:\n';
		
			for(var attribute in ojaLanding.chatAnswers){
				msg += i + '-\t' + attribute + ':\n\t' + ojaLanding.chatAnswers[attribute] + '\n\n';
				i++;
			}

			$(".olrk-noquirks").fadeIn("slow");
			
			olark('api.visitor.updateEmailAddress', {emailAddress: ojaLanding.email});
			olark('api.visitor.updateFullName', {fullName: $('#username').val()});
			olark('api.chat.sendNotificationToOperator', {body: msg});

			//Activa el envio de data de wistia
			ojaLanding.activateEventMessages();
		});

		ojaLanding.sendAnswers();
	},
	startHowAnimation: function(){
		$('#main').addClass('hide');
		$('#how-works').removeClass('hide');
		ojaLanding.animateSlide(1, 8, ojaLanding.startQuestionsWarning);
	},
	startRegistration: function(){
		$('#questions-wrapper').addClass('hide');
		$('#registration-form').fadeIn();
	},
	startQuestionsWarning: function() {
		$('#main').addClass('hide').hide();
		$('#how-works').removeClass('hide');
		ojaLanding.animateSlide(7, 8, function(){
			$('#questions-wrapper').hide();
			$('#questions-wrapper').removeClass('hide');
			$('#questions-wrapper').fadeIn();

			$('#how-works').addClass('hide');
			ojaLanding.animation.currentSlide = 'Questions';
		});
	},
	showVideo: function(course){
		var lightBox = $('#videoSlide'),
			slug = course.substr(course.lastIndexOf('/') + 1), //obtiene el slug desde la URL
			url = baseUrl + '/site/courseInfo';

		$('.loading-wrapper').show();
		$('.video').removeClass('animated flipInY').addClass('animated flipOutY');

		$.post(url, {slug: slug}, function(data){
			var course = JSON.parse(data);
			$('.video h1').html(course.name);
			$('.video iframe').attr('src', '//fast.wistia.net/embed/iframe/' + course.wistiaUid  + '?videoFoam=true');
			$('.programa').html(course.content);
			$('.loading-wrapper').hide();
			$('.video').removeClass('animated flipOutY').addClass('animated flipInY');
			ojaLanding.activateEventMessages();
		});
	},
	save: function(number, qstn, value, id_question, id_answer){
		if(number===1) { //Cambia el video de acuerdo a la primera pregunta
			ojaLanding.selectVideo(id_answer);
		}

		number++;
		ojaLanding.chatAnswers[qstn] = value;
		ojaLanding.apiAnswers[id_question] = id_answer;

		$('#questions-wrapper > div').hide();

		if($('#question-' + number).length){
			$('#question-' + number).fadeIn();
		}
		else {
			ojaLanding.startRegistration();
		}
	},
	selectVideo: function(answer){
		var title, wistiaUid, content;
		switch(answer){
			case 2:
				title = $('#mediumCourse').data('title');
				content = $('#mediumCourse').data('content');
				wistiaUid = $('#mediumCourse').val();
				break;
			case 3: //Advanced
				title = $('#advancedCourse').data('title');
				content = $('#advancedCourse').data('content');
				wistiaUid = $('#advancedCourse').val();
				break;
			default: 
				return;
		}

		$('.video h1').html(title);
		$('.video iframe').attr('src', '//fast.wistia.net/embed/iframe/' + wistiaUid  + '?videoFoam=true');
		$('.programa').html(content);
	},
	sendAnswers: function(){
		var url = baseUrl + '/api/landingAnswers';
		$.post(url, {answers: ojaLanding.apiAnswers, email: ojaLanding.email});
	}
};

$(document).ready(function () {
	$('.btns .how').on('click', function(){
		ojaLanding.startHowAnimation();
	});

	$('.btns .start').on('click', function(){
		ojaLanding.startQuestionsWarning();
	});

	$('#username').on('keyup', function(){
		if($(this).val()===''){
			$('#usermail').fadeOut('fast');
			$('#usermail').val('');
			$('#register').fadeOut('fast');
		} else {
			if(!$('#usermail').is(':visible')){
				$('#usermail').fadeIn();
			}
		}
	});
	
	$('#usermail').on('keyup', function(){
		var pattern = new RegExp(/^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/),
			email = $('#usermail').val();

		if(pattern.test(email) && email !=''){
   			$('#register').fadeIn();
 		}
	});

	$('#registration').on('submit', function(e){
		e.preventDefault();
		ojaLanding.email = $('#usermail').val();

		ojaLanding.register(ojaLanding.email);
		ojaLanding.startChat();
		
		$('#registration').addClass('hide');
	});

	$('#show-programa').on('click', function(){
		$('.programa').css('height', 'auto');
	});

	$( "#show-programa" ).click(function() {
	  $( ".programa" ).animate({
	      height: "auto",
	  }, 1500 );
	});

	//Navegación por teclas
	$("body").keydown(function(e) {
		ojaLanding.keyNavigation(e);
	});

	//Navegación por teclas en la pantalla (con clic)
	$(".keys-nav a.back").on('click', function(e) {
		e.preventDefault();
		e.keyCode = 37; //Simulo presión del teclado
		ojaLanding.keyNavigation(e);
	});

	$(".keys-nav a.next").on('click', function(e) {
		e.preventDefault();
		e.keyCode = 39; //Simulo presión del teclado
		ojaLanding.keyNavigation(e);
	});	



	
	olark('api.chat.onCommandFromOperator', function(event) {
		if (event.command.name === 'curso') {
		    	ojaLanding.showVideo(event.command.body);
		}
	});

	//Evento de registro cuando un usuario se va
	window.onbeforeunload = function(e) {
		_kmq.push(['record', 'Onboarding leave', {
			'Slide': ojaLanding.animation.currentSlide
		}]);
	};
});