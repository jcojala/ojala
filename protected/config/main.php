<?php
require('settings.php');

return array(
	'basePath'=>dirname(__FILE__).DIRECTORY_SEPARATOR.'..',
	'name'=>'Ojala',

	// preloading 'log' component
	'preload'=>array('log'),
	'language' => 'es',
 	'sourceLanguage'=>'es_VE', 

	// autoloading model and component classes
	'import'=>array(
		'application.models.*',
		'application.models.admin.*',
		'application.models.campaign.*',
		'application.models.studyplan.*',
		'application.models.advisor.*',
		'application.components.*',
		'application.components.widgets.*',
		'application.components.ojala.*',
		'application.components.ojalapayments.*',
		'application.components.ojalapayments.stripe.*',
		'application.components.ojalapayments.paypal.*',
		'application.components.ojalapayments.conekta.*',
		'application.vendors.phpexcel.PHPExcel',
                'application.components.translator.*',
		'ext.yiireport.*',
		'application.components.fpdf.*',
	),

	'modules'=> $modules,	

	// application components
	'components'=>array(

		'user'=>array(
			'allowAutoLogin'=>true,
	    		'authTimeout' => 604800, //Una semana
		),
		'session' => array(
			'class' => 'CDbHttpSession',
			'connectionID' => 'db',
			'autoCreateSessionTable' => true,
			'cookieParams' => array
			(
				'lifetime' => (86400 * 60),
			),
			'timeout' => (86400 * 60),    
		),
		'urlManager'=>array(
			'class' => 'CUrlManager',
			'urlFormat'=>'path',
			'showScriptName'=>false,
 			'caseSensitive'=>false,  
			'rules'=>array(
				'gii'=>'gii',
				'becas/referido/<email>'=>'becas/referido',
                                'curso/proximo/<slug>'=>'site/propuesta',
				'cursos/<cat>'=>'site/cursos',
				'curso/<cat>/<slug>'=>'site/curso',
				'curso/<cat>/<slug>/<slugc>'=>'site/clase',
				'foro/<cat>/<slug>/<slugc>'=>'site/foro',
				'diplomado/<slug>'=>'site/diplomado',
				'admin/reportes/<action:\w+>'=>'reportes/<action>',
				'<action:\w+>'=>'site/<action>',
				'<controller:\w+>/<id:\d+>1'=>'<controller>/view',
				'<controller:\w+>/<action:\w+>/<id:\d+>'=>'<controller>/<action>',
                            
			),
		),
		'facebook' => array(
		    'class' => 'application.components.facebook.FacebookAPP',
		),
		'EajaxUpload' => array(
			'class' => 'ext.EAjaxUpload.EAjaxUpload',
			'enabled' => true,
		),
		'image'=>array(     
			'class'=>'application.extensions.image.CImageComponent',
			'driver'=>'GD', 
		),
		'mailer' => array(
	      		'class' => 'application.extensions.mailer.EMailer',
      			'pathViews' => 'application.views.email',
	      		'pathLayouts' => 'application.views.email.layouts'
	   	),

		'ePdf' => array(
			'class' => 'ext.yii-pdf.EYiiPdf',
			'params' => array(
				'mpdf' => array(
					'librarySourcePath' => 'application.vendors.mpdf.*',
					'constants'         => array(
						'_MPDF_TEMP_PATH' => Yii::getPathOfAlias('application.runtime'),
					),
					'class'=>'mpdf', // the literal class filename to be loaded from the vendors folder
					/*'defaultParams'     => array( // More info: http://mpdf1.com/manual/index.php?tid=184
					  'mode'              => '', //  This parameter specifies the mode of the new document.
					  'format'            => 'A4', // format A4, A5, ...
					  'default_font_size' => 0, // Sets the default document font size in points (pt)
					  'default_font'      => '', // Sets the default font-family for the new document.
					  'mgl'               => 15, // margin_left. Sets the page margins for the new document.
					  'mgr'               => 15, // margin_right
					  'mgt'               => 16, // margin_top
					  'mgb'               => 16, // margin_bottom
					  'mgh'               => 9, // margin_header
					  'mgf'               => 9, // margin_footer
					  'orientation'       => 'P', // landscape or portrait orientation
					)*/
				),
				'HTML2PDF' => array(
					'librarySourcePath' => 'application.vendors.html2pdf.*',
					'classFile'         => 'html2pdf.class.php', // For adding to Yii::$classMap
					/*'defaultParams'     => array( // More info: http://wiki.spipu.net/doku.php?id=html2pdf:en:v4:accueil
					    'orientation' => 'P', // landscape or portrait orientation
					    'format'      => 'A4', // format A4, A5, ...
					    'language'    => 'en', // language: fr, en, it ...
					    'unicode'     => true, // TRUE means clustering the input text IS unicode (default = true)
					    'encoding'    => 'UTF-8', // charset encoding; Default is UTF-8
					    'marges'      => array(5, 5, 5, 8), // margins by default, in order (left, top, right, bottom)
					)*/
				)
			),
		),

		//Settings controlled in settings.php
		'db'=>$db,
		'db2'=>$db2,
		'db3'=>$db3,
		'ojalabDB'=>$ojalabDB,
		'metrics' => $metrics,
		'Paypal' => $paypal,
		'log'=> $log,
		
		'errorHandler'=>array(
		'errorAction'=>'site/error',
		),
	),
	// application-level parameters that can be accessed
	// using Yii::app()->params['paramName']
	'params'=>array(
		'adminEmail'=>'fr@oja.la',
		'advisorEmails'=> $advisorEmails,
		'cookieCryptKey'=>'28Aq1xg74Ptu9sFBM0DFqi8tjKqbK9J1', //Llave al azar para cifrar/descifrar información
		'devEmails'=> $devEmails,
		'fbAppId'=>'203807993010072',
		'fbAppUrl'=>'https://apps.facebook.com/ojalaedu/',
		'fbDevAppId'=>'478709982151992',
		'fromEmail'=>'no-responder@oja.la',
		'coverPath'=> 'images/covers/',
	),
);
