<?php

/**
 * This is the model class for table "course".
 *
 * The followings are the available columns in table 'course':
 * @property integer $id_course
 * @property integer $id_instructor
 * @property integer $id_cathegory
 * @property integer $id_user
 * @property integer $id_state
 * @property integer $id_level
 * @property string $name
 * @property string $cathegory
 * @property string $cost
 * @property string $cover
 * @property string $upload
 * @property string $create
 * @property string $publish
 * @property string $description
 * @property string $requirements
 * @property string $learn
 * @property string $directed
 * @property string $trailer
 * @property double $duration
 * @property string $hidden
 * @property string $slug
 * @property string $date
 * @property string $wistia_uid
 * @property integer $active
 * @property string $structure_nat
 * @property string $structure_format
 *
 * The followings are the available model relations:
 * @property User $idUser
 * @property User $idInstructor
 * @property Level $idLevel
 * @property Cathegory $idCathegory
 * @property CourseSuscription[] $courseSuscriptions
 * @property CourseTag[] $courseTags
 * @property GroupCourse[] $groupCourses
 * @property Lesson[] $lessons
 * @property Link[] $links
 */
class Course extends CActiveRecord {

    const COVER_MEDIUM_HEIGHT = 250;
    const COVER_MEDIUM_SUFFIX = 'medio.jpg';
    const COVER_MEDIUM_WIDTH = 350;
    const COVER_MIN_HEIGHT = 143;
    const COVER_MIN_SUFFIX = 'small.jpg';
    const COVER_MIN_WIDTH = 200;

    public function tableName() {
        return 'course';
    }

    public function rules() {
        return array(
            array('id_cathegory, id_state, id_level', 'required'),
            array('id_instructor, id_cathegory, id_user, id_state, id_level, active', 'numerical', 'integerOnly' => true),
            array('duration', 'numerical'),
            array('cost, cover, hidden, date', 'length', 'max' => 45),
            array('name, slug', 'length', 'max' => 500),
            array('cathegory', 'length', 'max' => 255),
            array('wistia_uid', 'length', 'max' => 100),
            array('upload, create, publish, description, requirements, learn, directed, trailer, structure_nat, structure_format', 'safe'),
            array('id_course,directed,trailer, learn, requirements, id_instructor, id_cathegory, id_old, id_user, id_state, id_level, name, cathegory, cost, cover, upload, create, publish, description, duration, hidden, slug, date, wistia_uid, active, structure_nat, structure_format', 'safe', 'on' => 'search'),
        );
    }

    public function relations() {
        return array(
            'user' => array(self::BELONGS_TO, 'User', 'id_user'),
            'state' => array(self::BELONGS_TO, 'State', 'id_state'),
            'instructor' => array(self::BELONGS_TO, 'User', 'id_instructor'),
            'level' => array(self::BELONGS_TO, 'Level', 'id_level'),
            'category' => array(self::BELONGS_TO, 'Cathegory', 'id_cathegory'),
            'courseSuscriptions' => array(self::HAS_MANY, 'CourseSuscription', 'id_course'),
            'courseTags' => array(self::HAS_MANY, 'CourseTag', 'id_course'),
            'groupCourses' => array(self::HAS_MANY, 'GroupCourse', 'id_course'),
            'lessons' => array(self::HAS_MANY, 'Lesson', 'id_course', 'order' => '`lessons`.id_lesson ASC', 'alias' => 'lessons'),
            'links' => array(self::HAS_MANY, 'Link', 'id_course'),
            'studyPlans' => array(self::MANY_MANY, 'StudyPlan', 'study_plan_course(id_course, id_user)'),
            'tags' => array(self::MANY_MANY, 'Tag', 'course_tag(id_course, id_tag)'),
        );
    }

    public function attributeLabels() {
        return array(
            'id_course' => 'Id Course',
            'id_instructor' => 'Id Instructor',
            'id_cathegory' => 'Categoría',
            'id_user' => 'Id User',
            'id_state' => 'Id State',
            'id_level' => 'Nivel',
            'name' => 'Nombre',
            'cathegory' => 'Categoría',
            'cost' => 'Costo',
            'cover' => 'Cover',
            'upload' => 'Upload',
            'create' => 'Create',
            'publish' => 'Publish',
            'description' => 'Descripción',
            'requirements' => '¿Cuáles son los requisitos?',
            'learn' => '¿Qué voy a aprender?',
            'directed' => '¿A quién va dirigido?',
            'trailer' => 'Trailer',
            'duration' => 'Duración (Seg)',
            'hidden' => 'Hidden',
            'slug' => 'Slug',
            'date' => 'Date',
            'wistia_uid' => 'ID Wistia',
            'active' => 'Active',
			'structure_nat'=>'Estructura Inicial',
			'structure_format'=>'Estructura Formateada'
        );
    }

    public function search() {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id_course', $this->id_course);
        $criteria->compare('id_instructor', $this->id_instructor);
        $criteria->compare('id_cathegory', $this->id_cathegory);
        $criteria->compare('id_user', $this->id_user);
        $criteria->compare('id_state', $this->id_state);
        $criteria->compare('id_level', $this->id_level);
        $criteria->compare('name', $this->name, true);
        $criteria->compare('cathegory', $this->cathegory, true);
        $criteria->compare('cost', $this->cost, true);
        $criteria->compare('cover', $this->cover, true);
        $criteria->compare('upload', $this->upload, true);
        $criteria->compare('create', $this->create, true);
        $criteria->compare('publish', $this->publish, true);
        $criteria->compare('description', $this->description, true);
        $criteria->compare('requirements', $this->requirements, true);
        $criteria->compare('learn', $this->learn, true);
        $criteria->compare('directed', $this->directed, true);
        $criteria->compare('trailer', $this->trailer, true);
        $criteria->compare('duration', $this->duration);
        $criteria->compare('hidden', $this->hidden, true);
        $criteria->compare('slug', $this->slug, true);
        $criteria->compare('date', $this->date, true);
        $criteria->compare('wistia_uid', $this->wistia_uid, true);
        $criteria->compare('active', $this->active);
		$criteria->compare('structure_nat', $this->structure_nat);
		$criteria->compare('structure_format', $this->structure_format);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    public function generateThumbnails($force = false) {
        $imgPath = getcwd() . '/' . Yii::app()->params['coverPath'];
        $coverPath = $imgPath . $this->cover;

        if (!empty($this->cover) && file_exists($coverPath)) {
            $coverFilename = $imgPath . substr($this->cover, 0, -4);
            $mediumPath = $coverFilename . self::COVER_MEDIUM_SUFFIX;
            $smallPath = $coverFilename . self::COVER_MIN_SUFFIX;

            if (!file_exists($mediumPath) || $force) {
                $image = Yii::app()->image->load($coverPath);
                $image->resize(self::COVER_MEDIUM_WIDTH, self::COVER_MEDIUM_HEIGHT);
                $image->save($mediumPath);
            }

            if (!file_exists($smallPath) || $force) {
                $image = Yii::app()->image->load($coverPath);
                $image->resize(self::COVER_MIN_WIDTH, self::COVER_MIN_HEIGHT);
                $image->save($smallPath);
            }
        }
    }

    public function deleteImages() {
        $imgPath = getcwd() . '/' . Yii::app()->params['coverPath'];
        $coverPath = $imgPath . $this->cover;

        $coverFilename = $imgPath . substr($this->cover, 0, -4);
        $mediumPath = $coverFilename . self::COVER_MEDIUM_SUFFIX;
        $smallPath = $coverFilename . self::COVER_MIN_SUFFIX;

        if (file_exists($coverPath) && !is_dir($coverPath)) {
            unlink($coverPath);
        }

        if (file_exists($mediumPath) && !is_dir($coverPath)) {
            unlink($mediumPath);
        }

        if (file_exists($smallPath) && !is_dir($coverPath)) {
            unlink($smallPath);
        }
    }

    /**
     * Función para obtener el nombre de un archivo
     */
    private static function coverFilename($coverFilename, $size) {
        $base = Yii::app()->request->baseUrl . '/' . Yii::app()->params['coverPath'];

        switch ($size) {
            case 'small':
                $imgUrl = $coverFilename . self::COVER_MIN_SUFFIX;
                break;
            case 'medium':
                $imgUrl = $coverFilename . self::COVER_MEDIUM_SUFFIX;
                break;
            default:
                $imgUrl = $coverFilename;
        }

        return $base . $imgUrl;
    }

    /**
     * Funcion para obtener el thumbnail
     * 
     * @param string $size 	TamaÃ±o del thumbnail a devolver (small, medium, full)
     */
    public function getCoverURL($size = 'full') {
        if (!$size != 'full')
            $coverFilename = substr($this->cover, 0, -4);

        return self::coverFilename($coverFilename, $size);
    }

    public static function coverUrl($coverName, $size = 'full') {
        if (!$size != 'full')
            $coverFilename = substr($coverName, 0, -4);

        return self::coverFilename($coverFilename, $size);
    }

    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    public function scopes() {
        return array(
            'published' => array('condition'=>'t.active=1 AND t.publish <= NOW() AND t.id_state=' . State::PUBLISHED),
            'programmed'=>array('condition'=>'t.active!=0 AND t.publish > NOW() AND t.id_state=' . State::PUBLISHED),
        	'draft' => array ('condition'=>'t.active!=0 AND t.id_state=' . State::DRAFT),
            'hidden' => array ('condition'=>'t.active!=0 AND t.id_state=' . State::HIDDEN),
            'proposal' => array ('condition'=>'t.id_state=' . State::PROPOSAL),
        	'reviewedProposal' => array ('condition'=>'t.id_state=' . State::REVIEWED_PROPOSAL)
        );
    }

    public function getUrl() {
        return Yii::app()->createUrl('curso') . '/' . $this->category->slug . '/' . $this->slug;
    }

    public function getContent() {
        $i = 1;
        $html = '';

        foreach ($this->lessons as $lesson) {
            $html .= '<div class="mod">';
            $html .= '<h3><span>Módulo ' . $i . '</span>' . $lesson->na_lesson . '</h3>';
            $html .= '<ul>';

            foreach ($lesson->topics as $topic) {
                $html .= '<li>' . $topic->na_topic . '</li>';
            }

            $html .= '</ul></div>';
            $i++;
        }

        return $html;
    }

    /**
     * Devuelve el principal video de
     * @return int ID de wistia del video principal
     */
    public function getMainVideo() {
        return empty($this->trailer) ? $this->lessons[0]->topics[0]->wistia_uid : $this->trailer;
    }

    public static function getCourseIdFromName($name) {
        $course = self::model()->find(array(
            'select' => 'id_course',
            'condition' => 'name=:name',
            'params' => array(':name' => $name)
        ));

        return $course ? $course->id_course : NULL;
    }

    public function getPropuestas($filtro, $name = null) {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id_course', $this->id_course);
        $criteria->compare('id_state', $this->id_state);
        $criteria->compare('active', $this->active);
        $criteria->addCondition('t.id_state = 4 OR t.id_state = 5');
        $criteria->with = array(
            'category' => array('select' => 'na_cathegory'),
            'level' => array('select' => 'na_level'),
            'courseTags'
        );
        if ($name != null) {
            $criteria->addCondition("t.name LIKE '%" . $name . "%'");
        }

        if ($filtro == 1) {

            $criteria->addCondition('t.active = 1');
        } else {

            if ($filtro == 2) {

                $criteria->addCondition('t.active = 0');
            }
        }

        return new CActiveDataProvider($this, array(
            'pagination' => array(
                'pageSize' => 100,
            ),
            'criteria' => $criteria,
        ));
    }

}
