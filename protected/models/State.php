<?php

/**
 * This is the model class for table "state".
 *
 * The followings are the available columns in table 'state':
 * @property integer $id_state
 * @property string $na_state
 *
 */
class State extends CActiveRecord
{
	const PUBLISHED = 1;
	const DRAFT = 2;
	const HIDDEN = 3;
	const PROPOSAL = 4;
	const REVIEWED_PROPOSAL = 5;

	public function tableName()
	{
		return 'state';
	}


	public function rules()
	{
		return array(
			array('na_state', 'required'),
			array('na_state', 'length', 'max'=>45),
			array('id_state, na_state', 'safe', 'on'=>'search'),
		);
	}

	public function relations()
	{
		return array(
			'course' => array(self::BELONGS_TO, 'Course', 'id_course'),
		);
	}

	public function attributeLabels()
	{
		return array(
			'id_state' => 'Id State',
			'na_state' => 'Na State',
		);
	}

	public function search()
	{
		$criteria=new CDbCriteria;

		$criteria->compare('id_state',$this->id_state);
		$criteria->compare('na_state',$this->na_state,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
