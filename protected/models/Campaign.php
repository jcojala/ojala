<?php

/**
 * This is the model class for table "campaign".
 *
 * The followings are the available columns in table 'campaign':
 * @property integer $id_campaign
 * @property integer $id_service
 * @property string $utm_source
 * @property string $utm_medium
 * @property string $utm_term
 * @property string $utm_content
 * @property string $utm_campaign
 *
 * The followings are the available model relations:
 * @property Service $idService
 * @property Coupon[] $coupons
 */
class Campaign extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'campaign';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id_service', 'numerical', 'integerOnly'=>true),
			array('utm_source, utm_medium, utm_term, utm_content, utm_campaign', 'length', 'max'=>50),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id_campaign, id_service, utm_source, utm_medium, utm_term, utm_content, utm_campaign', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'idService' => array(self::BELONGS_TO, 'Service', 'id_service'),
			'coupons' => array(self::HAS_MANY, 'Coupon', 'id_campaign'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_campaign' => 'Id Campaign',
			'id_service' => 'Id Service',
			'utm_source' => 'Utm Source',
			'utm_medium' => 'Utm Medium',
			'utm_term' => 'Utm Term',
			'utm_content' => 'Utm Content',
			'utm_campaign' => 'Utm Campaign',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id_campaign',$this->id_campaign);
		$criteria->compare('id_service',$this->id_service);
		$criteria->compare('utm_source',$this->utm_source,true);
		$criteria->compare('utm_medium',$this->utm_medium,true);
		$criteria->compare('utm_term',$this->utm_term,true);
		$criteria->compare('utm_content',$this->utm_content,true);
		$criteria->compare('utm_campaign',$this->utm_campaign,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Campaign the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
