<?php

/**
 * This is the model class for table "course_tag".
 *
 * The followings are the available columns in table 'course_tag':
 * @property integer $id_ctag
 * @property integer $id_course
 * @property integer $id_tag
 * @property integer $id_correlation
 *
 * The followings are the available model relations:
 * @property Course $idCourse
 * @property Tag $idTag
 * @property CorrelationTag $idCorrelation
 */
class CourseTag extends CActiveRecord
{
	public function tableName()
	{
		return 'course_tag';
	}

	public function rules()
	{
		return array(
			array('id_course, id_tag', 'required'),
			array('id_course, id_tag', 'numerical', 'integerOnly'=>true),
			array('id_ctag, id_course, id_tag', 'safe', 'on'=>'search'),
		);
	}

	public function relations()
	{
		return array(
			'course' => array(self::BELONGS_TO, 'Course', 'id_course'),
			'tag' => array(self::BELONGS_TO, 'Tag', 'id_tag'),
			'correlation' => array(self::BELONGS_TO, 'CorrelationTag', 'id_correlation'),
		);
	}

	public function attributeLabels()
	{
		return array(
			'id_ctag' => 'Id Ctag',
			'id_course' => 'Id Course',
			'id_tag' => 'Id Tag',
		);
	}

	public function search()
	{
		$criteria=new CDbCriteria;

		$criteria->compare('id_ctag',$this->id_ctag);
		$criteria->compare('id_course',$this->id_course);
		$criteria->compare('id_tag',$this->id_tag);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
