<?php

/**
 * This is the model class for table "group".
 *
 * The followings are the available columns in table 'group':
 * @property integer $id_group
 * @property string $nb_group
 * @property string $description
 * @property integer $cost
 * @property string $create_date
 * @property string $image
 * @property string $slug
 *
 * The followings are the available model relations:
 * @property GroupCourse[] $groupCourses
 */
class Group extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'group';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('nb_group, description, cost, create_date', 'required', 'message'=>'Disculpa, pero necesitamos esta información'),
			array('cost', 'numerical', 'integerOnly'=>true, 'message'=>'El precio es un numero!'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id_group, nb_group, description, cost, slug, create_date, image', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'groupCourses' => array(self::HAS_MANY, 'GroupCourse', 'id_group'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_group' => 'Id Group',
			'nb_group' => 'Nb Group',
			'description' => 'Description',
			'cost' => 'Cost',
			'create_date' => 'Create Date',
			'image' => 'Image',
			'slug' => 'Image',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id_group',$this->id_group);
		$criteria->compare('nb_group',$this->nb_group,true);
		$criteria->compare('description',$this->description,true);
		$criteria->compare('cost',$this->cost);
		$criteria->compare('create_date',$this->create_date,true);
		$criteria->compare('image',$this->image,true);
		$criteria->compare('slug',$this->slug,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Group the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
