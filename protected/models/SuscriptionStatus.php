<?php

/**
 * This is the model class for table "suscription_status".
 *
 * The followings are the available columns in table 'suscription_status':
 * @property integer $id_sstatus
 * @property integer $id_status
 * @property integer $id_suscription
 * @property integer $active
 *
 * The followings are the available model relations:
 * @property Notification[] $notifications
 * @property Status $idStatus
 * @property Suscription $idSuscription
 */
class SuscriptionStatus extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'suscription_status';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id_status, id_suscription', 'required'),
			array('id_status, id_suscription, active', 'numerical', 'integerOnly'=>true),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id_sstatus, id_status, id_suscription, active', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'notifications' => array(self::HAS_MANY, 'Notification', 'id_sstatus'),
			'idStatus' => array(self::BELONGS_TO, 'Status', 'id_status'),
			'idSuscription' => array(self::BELONGS_TO, 'Suscription', 'id_suscription'),
		);
	}

	public function attributeLabels()
	{
		return array(
			'id_sstatus' => 'Id Sstatus',
			'id_status' => 'Id Status',
			'id_suscription' => 'Id Suscription',
			'active' => 'Active',
		);
	}

	public function search()
	{
		$criteria=new CDbCriteria;

		$criteria->compare('id_sstatus',$this->id_sstatus);
		$criteria->compare('id_status',$this->id_status);
		$criteria->compare('id_suscription',$this->id_suscription);
		$criteria->compare('active',$this->active);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	public function beforeSave(){
		SuscriptionStatus::model()->updateAll(
			array('active'=>0),
			'id_suscription=:id_suscription AND active=1',
			array(':id_suscription'=>$this->id_suscription)
		);

		return parent::beforeSave();
	}
}
