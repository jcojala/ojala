<?php

/**
 * This is the model class for table "topic".
 *
 * The followings are the available columns in table 'topic':
 * @property integer $id_topic
 * @property integer $id_lesson
 * @property integer $id_ttype
 * @property string $na_topic
 * @property string $wistia_uid
 * @property double $duration
 * @property string $description
 * @property integer $active
 * @property integer $public
 * @property integer $position
 *
 * The followings are the available model relations:
 * @property Progress[] $progresses
 * @property Lesson $idLesson
 * @property TopicType $idTtype
 */
class Topic extends CActiveRecord
{
	public function tableName()
	{
		return 'topic';
	}

	public function rules()
	{
		return array(
			array('id_lesson, id_ttype, na_topic, wistia_uid', 'required'),
			array('id_lesson, id_ttype, active, public, position', 'numerical', 'integerOnly'=>true),
			array('duration', 'numerical'),
			array('na_topic', 'length', 'max'=>500),
			array('wistia_uid', 'length', 'max'=>100),
			array('description', 'safe'),

			array('id_topic, id_lesson, id_ttype, na_topic, wistia_uid, duration, description, active, position', 'safe', 'on'=>'search'),
		);
	}

	public function relations()
	{
		return array(
			'progresses' => array(self::HAS_MANY, 'Progress', 'id_topic'),
			'lesson' => array(self::BELONGS_TO, 'Lesson', 'id_lesson'),
			'topicType' => array(self::BELONGS_TO, 'TopicType', 'id_ttype'),
		);
	}

	public function attributeLabels()
	{
		return array(
			'id_topic' => 'Id Topic',
			'id_lesson' => 'Id Lesson',
			'id_ttype' => 'Id Ttype',
			'na_topic' => 'Na Topic',
			'wistia_uid' => 'Wistia Uid',
			'duration' => 'Duration',
			'description' => 'Description',
			'active' => 'Active',
			'public' => 'Public',
			'position' => 'Position',
		);
	}

	public function search()
	{
		$criteria=new CDbCriteria;

		$criteria->compare('id_topic',$this->id_topic);
		$criteria->compare('id_lesson',$this->id_lesson);
		$criteria->compare('id_ttype',$this->id_ttype);
		$criteria->compare('na_topic',$this->na_topic,true);
		$criteria->compare('wistia_uid',$this->wistia_uid,true);
		$criteria->compare('duration',$this->duration);
		$criteria->compare('description',$this->description,true);
		$criteria->compare('public',$this->public);
		$criteria->compare('active',$this->active);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
