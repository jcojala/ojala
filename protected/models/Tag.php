<?php

/**
 * This is the model class for table "tag".
 *
 * The followings are the available columns in table 'tag':
 * @property integer $id_tag
 * @property string $tag
 *
 * The followings are the available model relations:
 * @property CourseTag[] $courseTags
 */
class Tag extends CActiveRecord
{
	public function tableName()
	{
		return 'tag';
	}

	public function rules()
	{
		return array(
			array('tag', 'required'),
			array('tag', 'length', 'max'=>45),
			array('id_tag, tag', 'safe', 'on'=>'search'),
		);
	}

	public function relations()
	{
		return array(
			'courseTags' => array(self::HAS_MANY, 'CourseTag', 'id_tag'),
                        'proposalTags' => array(self::HAS_MANY, 'ProposalTag', 'id_tag'),
		);
	}

	public function attributeLabels()
	{
		return array(
			'id_tag' => 'Id Tag',
			'tag' => 'Tag',
		);
	}

	public function search()
	{
		$criteria=new CDbCriteria;

		$criteria->compare('id_tag',$this->id_tag);
		$criteria->compare('tag',$this->tag,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * Función para obtener el ID de un tag, si no existe lo guarda y devuelve el id
	 * @param  	string 	$tag 	Nombre del tag a buscar
	 * @return 	integer        	Id del tag
	 */
	public static function retrieveId($tagName)
	{
		$tag = self::model()->find(array(
			'select'=>'id_tag',
			'condition'=>'tag=:tag',
			'params' => array(':tag'=>$tagName)
		));

		if(!$tag){
			$tag = new Tag;
			$tag->tag = $tagName;
			$tag->save();
		}

		return $tag->id_tag;
	}
}
