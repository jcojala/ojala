<?php

/**
 * This is the model class for table "lesson".
 *
 * The followings are the available columns in table 'lesson':
 * @property integer $id_lesson
 * @property integer $id_course
 * @property string $na_lesson
 * @property double $duration
 * @property string $description
 * @property integer $active
 *
 * The followings are the available model relations:
 * @property Course $idCourse
 * @property Topic[] $topics
 */
class Lesson extends CActiveRecord
{
	public function tableName()
	{
		return 'lesson';
	}

	public function rules()
	{
		return array(
			array('id_course, na_lesson', 'required'),
			array('id_course, active', 'numerical', 'integerOnly'=>true),
			array('duration', 'numerical'),
			array('na_lesson', 'length', 'max'=>500),
			array('description', 'length', 'max'=>45),

			array('id_lesson, id_course, na_lesson, duration, description, active', 'safe', 'on'=>'search'),
		);
	}

	public function relations()
	{
		return array(
			'idCourse' => array(self::BELONGS_TO, 'Course', 'id_course'),
			'topics' => array(self::HAS_MANY, 'Topic', 'id_lesson', 'order'=>'`topics`.id_lesson ASC', 'alias'=>'topics'),
		);
	}

	public function attributeLabels()
	{
		return array(
			'id_lesson' => 'Id Lesson',
			'id_course' => 'Id Course',
			'na_lesson' => 'Na Lesson',
			'duration' => 'Duration',
			'description' => 'Description',
			'active' => 'Active',
		);
	}

	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id_lesson',$this->id_lesson);
		$criteria->compare('id_course',$this->id_course);
		$criteria->compare('na_lesson',$this->na_lesson,true);
		$criteria->compare('duration',$this->duration);
		$criteria->compare('description',$this->description,true);
		$criteria->compare('active',$this->active);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
