<?php

/**
 * This is the model class for table "link_proposal".
 *
 * The followings are the available columns in table 'link_proposal':
 * @property integer $id_link
 * @property integer $id_user
 * @property integer $id_course
 * @property integer $id_ltype
 * @property string $email
 *
 * The followings are the available model relations:
 * @property Course $idCourse
 * @property LinkType $idLtype
 * @property User $idUser
 */
class LinkProposal extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'link_proposal';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id_user, id_course, id_ltype', 'numerical', 'integerOnly'=>true),
			array('email', 'length', 'max'=>50),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id_link, id_user, id_course, id_ltype, email', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'idCourse' => array(self::BELONGS_TO, 'Course', 'id_course'),
			'idLtype' => array(self::BELONGS_TO, 'LinkType', 'id_ltype'),
			'idUser' => array(self::BELONGS_TO, 'User', 'id_user'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_link' => 'Id Link',
			'id_user' => 'Id User',
			'id_course' => 'Id Course',
			'id_ltype' => 'Id Ltype',
			'email' => 'Email',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id_link',$this->id_link);
		$criteria->compare('id_user',$this->id_user);
		$criteria->compare('id_course',$this->id_course);
		$criteria->compare('id_ltype',$this->id_ltype);
		$criteria->compare('email',$this->email,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return LinkProposal the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
