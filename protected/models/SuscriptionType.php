<?php

/**
 * This is the model class for table "suscription_type".
 *
 * The followings are the available columns in table 'suscription_type':
 * @property integer $id_stype
 * @property string $na_stype
 * @property integer $access
 * @property integer $active
 * @property integer $id_service
 * @property integer $amount
 *
 * The followings are the available model relations:
 * @property Price[] $prices
 * @property Suscription[] $suscriptions
 */
class SuscriptionType extends CActiveRecord
{
	const SCHOLARSHIP = 17;
	const SCHOLARSHIP_TXT = 'Beca';
	const TWOBYONE= 46;
	const TWOBYONE_TXT = 'Amigos2x1';
	
	public function tableName()
	{
		return 'suscription_type';
	}

	public function rules()
	{
		return array(
			array('na_stype', 'required'),
			array('access, active', 'numerical', 'integerOnly'=>true),
			array('na_stype', 'length', 'max'=>45),

			array('id_stype, na_stype, id_service, access, active, amount', 'safe', 'on'=>'search'),
		);
	}

	public function relations()
	{
		return array(
			'prices' => array(self::HAS_MANY, 'Price', 'id_stype'),
			'suscriptions' => array(self::HAS_MANY, 'Suscription', 'id_stype'),
		);
	}

	public function attributeLabels()
	{
		return array(
			'id_stype' => 'Id Stype',
			'na_stype' => 'Na Stype',
			'access' => 'Access',
			'amount' => 'Amount',
			'id_service' => 'id_service',
			'active' => 'Active',
		);
	}

	public function search()
	{
		$criteria=new CDbCriteria;

		$criteria->compare('id_stype',$this->id_stype);
		$criteria->compare('na_stype',$this->na_stype,true);
		$criteria->compare('access',$this->access);
		$criteria->compare('active',$this->active);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
