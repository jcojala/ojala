<?php

/**
 * This is the model class for table "country".
 *
 * The followings are the available columns in table 'country':
 * @property integer $id_country
 * @property string $name
 *
 * The followings are the available model relations:
 * @property Region[] $regions
 */
class Country extends CActiveRecord
{
	public function tableName()
	{
		return 'country';
	}

	public function rules()
	{
		return array(
			array('name', 'length', 'max'=>45),
			array('phone_code', 'numerical'),
			array('phone_code', 'length', 'max'=>3),
			array('id_country, name, phone_code', 'safe', 'on'=>'search'),
		);
	}

	public function relations()
	{
		return array(
			'regions' => array(self::HAS_MANY, 'Region', 'id_country'),
		);
	}

	public function attributeLabels()
	{
		return array(
			'id_country' => 'País',
			'name' => 'Nombre',
			'phone_code' => 'Código telefónico',
		);
	}

	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
