<?php

/**
 * This is the model class for table "pay".
 *
 * The followings are the available columns in table 'pay':
 * @property integer $id_pay
 * @property integer $id_ptype
 * @property integer $id_suscription
 * @property string $date
 * @property double $amount
 * @property string $code
 * @property string $email
 * @property string $status
 * @property string $currency
 *
 * The followings are the available model relations:
 * @property Suscription $idSuscription
 * @property PayType $idPtype
 * @property PayInfo[] $payInfos
 */
class Pay extends CActiveRecord
{
	const SUCCESS = 'Completed';
	const FAILED = 'Failed';
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'pay';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id_ptype', 'required'),
			array('id_ptype, id_suscription', 'numerical', 'integerOnly'=>true),
			array('amount', 'numerical'),
			array('code', 'length', 'max'=>255),
			array('email', 'length', 'max'=>100),
			array('status', 'length', 'max'=>50),
			array('currency', 'length', 'max'=>5),
			array('date', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id_pay, id_ptype, id_suscription, date, amount, code, email, status, currency', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'idPtype' => array(self::BELONGS_TO, 'PayType', 'id_ptype'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_pay' => 'Id Pay',
			'id_ptype' => 'Id Ptype',
			'id_suscription' => 'Id Suscription',
			'date' => 'Date',
			'amount' => 'Amount',
			'code' => 'Code',
			'email' => 'Email',
			'status' => 'Status',
			'currency' => 'Currency',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id_pay',$this->id_pay);
		$criteria->compare('id_ptype',$this->id_ptype);
		$criteria->compare('id_suscription',$this->id_suscription);
		$criteria->compare('date',$this->date,true);
		$criteria->compare('amount',$this->amount);
		$criteria->compare('code',$this->code,true);
		$criteria->compare('email',$this->email,true);
		$criteria->compare('status',$this->status,true);
		$criteria->compare('currency',$this->currency,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Pay the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
