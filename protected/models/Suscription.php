<?php

/**
 * This is the model class for table "suscription".
 *
 * The followings are the available columns in table 'suscription':
 * @property integer $id_suscription
 * @property integer $id_user
 * @property integer $id_stype
 * @property integer $id_campaign
 * @property string $date
 * @property string $active
 * @property string $id_service
 *
 * The followings are the available model relations:
 * @property Coupon[] $coupons
 * @property CourseSuscription[] $courseSuscriptions
 * @property Pay[] $pays
 * @property PayMode[] $payModes
 * @property Price[] $prices
 * @property Campaign $idCampaign
 * @property SuscriptionType $idStype
 * @property User $idUser
 * @property SuscriptionStatus[] $suscriptionStatuses
 */
class Suscription extends CActiveRecord
{
	public $subscriptionDate;
	public $cancelationDate;

	public function tableName()
	{
		return 'suscription';
	}

	public function rules()
	{
		return array(
			array('id_user, id_stype, date, active', 'required', 'except'=>'subscriber'),
			array('id_user, id_stype, id_frequency, id_campaign', 'numerical', 'integerOnly'=>true),
			array('active', 'length', 'max'=>45),

			//Subscriber form
			array('subscriptionDate, id_service','required','on'=>'subscriber'),
			array('cancelationDate','safe','on'=>'subscriber'),
			array('id_suscription, id_user, id_stype, id_service, id_campaign, date,  active', 'safe', 'on'=>'search'),
		);
	}

	public function relations()
	{
		return array(
			'coupons' => array(self::HAS_MANY, 'Coupon', 'id_suscription'),
			'courseSuscriptions' => array(self::HAS_MANY, 'CourseSuscription', 'id_suscription'),
			'allPays' => array(self::HAS_MANY, 'Pay', 'id_suscription'),
			'pays' => array(self::HAS_MANY, 'Pay', 'id_suscription', 'on'=>"pays.amount>0 AND pays.status='Completed'",'order'=>'pays.id_pay DESC'),
			'payModes' => array(self::HAS_MANY, 'PayMode', 'id_suscription'),
			'prices' => array(self::HAS_MANY, 'Price', 'id_suscription'),
			'idCampaign' => array(self::BELONGS_TO, 'Campaign', 'id_campaign'),
			'type' => array(self::BELONGS_TO, 'SuscriptionType', 'id_stype'),
			'users' => array(self::BELONGS_TO, 'User', 'id_user'),
			'statuses' => array(self::HAS_MANY, 'SuscriptionStatus', 'id_suscription', 'order'=>'statuses.date DESC','on'=>'statuses.active=1'),
		);
	}

	public function scopes()
	{
		return array(
			'activated'=>array(
				'condition'=>'status=1',
			),
		);
	}

	public function attributeLabels()
	{
		return array(
			'id_suscription' => 'Código',
			'id_user' => 'Código del usuario',
			'id_stype' => 'Tipo de suscripción',
			'id_campaign' => 'Código de la campaña',
			'date' => 'Fecha de suscripción',
			'id_service' => 'Código en la pasarela de pago',
			'exp_date' => 'Próximo pago',
			'gateway' => 'Pasarela de pago',
			'with_downloads' => 'Acceso al material',
			'active' => 'Activo',
			'subscriptionDate'=>'Fecha de suscripción',
			'cancelationDate'=>'Fecha de cancelación'
		);
	}

	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id_suscription',$this->id_suscription);
		$criteria->compare('id_user',$this->id_user);
		$criteria->compare('id_stype',$this->id_stype);
		$criteria->compare('id_campaign',$this->id_campaign);
		$criteria->compare('date',$this->date,true);
		$criteria->compare('id_service',$this->id_service,true);
		$criteria->compare('active',$this->active,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	public function getActiveStr()
	{
		switch($this->active)
		{
			case '1':
				return 'Activo';
			case '2':
				return 'Deudor';
			default: 
				return 'Cancelado';
		}
	}
}
