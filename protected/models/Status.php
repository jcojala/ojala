<?php

/**
 * This is the model class for table "status".
 *
 * The followings are the available columns in table 'status':
 * @property integer $id_status
 * @property string $na_status
 *
 * The followings are the available model relations:
 * @property SuscriptionStatus[] $suscriptionStatuses
 */
class Status extends CActiveRecord
{
	const ACTIVE = 1;
	const CANCELED = 2;
	const DEBT = 3;
	const DEBT_STANDBY = 4;
	const HIDDEN = 5; //Significa que la suscripción pasó a ser de prueba

	public function tableName()
	{
		return 'status';
	}

	public function rules()
	{
		return array(
			array('na_status', 'required'),
			array('na_status', 'length', 'max'=>45),
			array('id_status, na_status', 'safe', 'on'=>'search'),
		);
	}

	public function relations()
	{
		return array(
			'suscriptionStatuses' => array(self::HAS_MANY, 'SuscriptionStatus', 'id_status'),
		);
	}

	public function attributeLabels()
	{
		return array(
			'id_status' => 'Id Status',
			'na_status' => 'Na Status',
		);
	}

	public function search()
	{
		$criteria=new CDbCriteria;

		$criteria->compare('id_status',$this->id_status);
		$criteria->compare('na_status',$this->na_status,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	public static function listData($emptyLabel = NULL)
	{
		$statuses = self::model()->findAll('id_status!=4');

		if(!empty($emptyLabel)){
			$statuses = array(''=>$emptyLabel) + $statuses;
		}

		return CHtml::listData($statuses, 'id_status', 'na_status');
	}

	/**
	 * En las suscripciones, los estados en el campo de active
	 * funcionan distinto, esta función se encarga de transformarlo
	 */
	public static function convertToSubscription($id_sstatus)
	{
		$subStatus = 0;

		switch($id_sstatus)
		{
			case Status::ACTIVE:
				$subStatus = 1;
				break;
			case Status::CANCELED:
			case Status::DEBT:
			case Status::DEBT_STANDBY:
				$subStatus = 0;
				break;
		}

		return $subStatus;
	}

	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}

