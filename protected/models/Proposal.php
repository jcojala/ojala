<?php

/**
 * This is the model class for table "proposal".
 *
 * The followings are the available columns in table 'proposal':
 * @property integer $id_proposal
 * @property string $name
 * @property integer $id_cathegory
 * @property integer $id_level
 * @property string $learn
 * @property string $cover
 * @property string $create
 * @property integer $active
 *
 * The followings are the available model relations:
 * @property LinkProposal[] $linkProposals
 * @property Cathegory $idCathegory
 * @property Level $idLevel
 */
class Proposal extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
    
        public $tags;
        const COVER_MEDIUM_HEIGHT = 250;
	const COVER_MEDIUM_SUFFIX = 'medio.jpg';
	const COVER_MEDIUM_WIDTH = 350;
	const COVER_MIN_HEIGHT = 143;
	const COVER_MIN_SUFFIX = 'small.jpg';
	const COVER_MIN_WIDTH = 200;
    
	public function tableName()
	{
		return 'proposal';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id_cathegory, id_level, active', 'numerical', 'integerOnly'=>true),
			array('name', 'length', 'max'=>50),
			array('learn', 'length', 'max'=>150),
			array('cover', 'length', 'max'=>45),
			array('create', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id_proposal, name, id_cathegory, id_level, learn, cover, create, active', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'linkProposals' => array(self::HAS_MANY, 'LinkProposal', 'id_proposal'),
                        'ProposalTags' => array(self::HAS_MANY, 'ProposalTag', 'id_proposal'),
			'idCathegory' => array(self::BELONGS_TO, 'Cathegory', 'id_cathegory'),
			'idLevel' => array(self::BELONGS_TO, 'Level', 'id_level'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_proposal' => 'Id Proposal',
			'name' => 'Name',
			'id_cathegory' => 'Id Cathegory',
			'id_level' => 'Id Level',
			'learn' => 'Learn',
			'cover' => 'Cover',
			'create' => 'Create',
			'active' => 'Active',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;
		$criteria->compare('id_proposal',$this->id_proposal);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('id_cathegory',$this->id_cathegory);
		$criteria->compare('id_level',$this->id_level);
		$criteria->compare('learn',$this->learn,true);
		$criteria->compare('cover',$this->cover,true);
		$criteria->compare('create',$this->create,true);
		$criteria->compare('active',$this->active);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
        
        public function generateThumbnails($force = false)
	{
		$imgPath = getcwd() . '/' . Yii::app()->params['coverProposalPath'];
		$coverPath = $imgPath . $this->cover;

		if(!empty($this->cover) && file_exists( $coverPath )) {
			$coverFilename = $imgPath . substr($this->cover, 0, -4);
			$mediumPath = $coverFilename  . self::COVER_MEDIUM_SUFFIX; 
			$smallPath = $coverFilename . self::COVER_MIN_SUFFIX; 
			
			if (!file_exists( $mediumPath ) || $force)
			{
				$image = Yii::app()->image->load($coverPath);
				$image->resize(self::COVER_MEDIUM_WIDTH, self::COVER_MEDIUM_HEIGHT);
				$image->save($mediumPath);
			}

			if (!file_exists( $smallPath )  || $force)
			{
				$image = Yii::app()->image->load($coverPath);
				$image->resize(self::COVER_MIN_WIDTH, self::COVER_MIN_HEIGHT);
				$image->save($smallPath);
			}
		}
	}

	public function deleteImages()
	{
		$imgPath = getcwd() . '/' . Yii::app()->params['coverProposalPath'];
		$coverPath = $imgPath . $this->cover;

		$coverFilename = $imgPath . substr($this->cover, 0, -4);
		$mediumPath = $coverFilename  . self::COVER_MEDIUM_SUFFIX; 
		$smallPath = $coverFilename . self::COVER_MIN_SUFFIX; 

		if(file_exists($coverPath) && !is_dir($coverPath)){
			unlink($coverPath);
		}

		if(file_exists($mediumPath) && !is_dir($coverPath)){
			unlink($mediumPath);
		}

		if(file_exists($smallPath) && !is_dir($coverPath)){
			unlink($smallPath);
		}
	}

	/**
	 * Función para obtener el nombre de un archivo
	 */
	private static function coverFilename($coverFilename, $size)
	{
		$base = Yii::app()->request->baseUrl . '/' . Yii::app()->params['coverProposalPath'];

		switch($size){
			case 'small':
				$imgUrl = $coverFilename . self::COVER_MIN_SUFFIX;
				break;
			case 'medium':
				$imgUrl = $coverFilename . self::COVER_MEDIUM_SUFFIX;
				break;
			default:
				$imgUrl = $coverFilename;
		}

		return $base . $imgUrl;
	}

	/**
	 * Funcion para obtener el thumbnail
	 * 
	 * @param string $size 	Tamaño del thumbnail a devolver (small, medium, full)
	 */
	public function getCoverURL($size = 'full')
	{
		if(!$size != 'full')
			$coverFilename = substr($this->cover, 0, -4);

		return self::coverFilename($coverFilename, $size);
	}

	public static function coverUrl($coverName ,$size = 'full')
	{
		if(!$size != 'full')
			$coverFilename = substr($coverName, 0, -4);

		return self::coverFilename($coverFilename, $size);
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Proposal the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
