 <?php

/**
 * This is the model class for table "region".
 *
 * The followings are the available columns in table 'region':
 * @property integer $id_region
 * @property integer $id_country
 * @property string $name
 * @property string $area_code
 *
 * The followings are the available model relations:
 * @property Country $idCountry
 * @property User[] $users
 */
class Region extends CActiveRecord
{
	public function tableName()
	{
		return 'region';
	}

	public function rules()
	{
		return array(
			array('id_country, name', 'required'),
			array('id_country', 'numerical, area_code', 'integerOnly'=>true),
			array('name', 'length', 'max'=>45),
			array('id_region, id_country, name', 'safe', 'on'=>'search'),
		);
	}

	public function relations()
	{
		return array(
			'country' => array(self::BELONGS_TO, 'Country', 'id_country'),
			'users' => array(self::HAS_MANY, 'User', 'id_region'),
		);
	}

	public function attributeLabels()
	{
		return array(
			'id_region' => 'Región',
			'id_country' => 'País',
			'name' => 'Nombre',
			'area_code' => 'Código de área',
		);
	}

	public function search()
	{
		$criteria=new CDbCriteria;

		$criteria->compare('id_region',$this->id_region);
		$criteria->compare('id_country',$this->id_country);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('area_code',$this->name,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
