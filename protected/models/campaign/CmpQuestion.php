<?php

/**
 * This is the model class for table "cmp_question".
 *
 * The followings are the available columns in table 'cmp_question':
 * @property integer $id_question
 * @property integer $id_campaign
 * @property string $question
 *
 * The followings are the available model relations:
 * @property CmpAnswer[] $cmpAnswers
 * @property CmpCampaign $idCampaign
 * @property Tag[] $tags
 */
class CmpQuestion extends CActiveRecord
{
	public $tagString;
	public $answersArray;
	
	public function tableName()
	{
		return 'cmp_question';
	}

	public function rules()
	{
		return array(
			array('id_campaign, question', 'required'),
			array('id_campaign', 'numerical', 'integerOnly'=>true),
			array('id_question, id_campaign, question', 'safe', 'on'=>'search'),
		);
	}

	public function relations()
	{
		return array(
			'answers' => array(self::MANY_MANY, 'CmpAnswer', 'cmp_answer_question(id_question, id_answer)'),
			'campaign' => array(self::BELONGS_TO, 'CmpCampaign', 'id_campaign'),
			'tags' => array(self::MANY_MANY, 'Tag', 'cmp_tag_question(id_question, id_tag)'),
		);
	}

	public function attributeLabels()
	{
		return array(
			'id_question' => 'Código',
			'id_campaign' => 'Campaña',
			'question' => 'Pregunta',
			'answers' => 'Respuestas posibles',
			'campaign.name' => 'Campaña',
			'tagString' => 'Etiquetas',
		);
	}

	public function search()
	{
		$criteria=new CDbCriteria;

		$criteria->compare('id_question',$this->id_question);
		$criteria->compare('id_campaign',$this->id_campaign);
		$criteria->compare('question',$this->question,true);
		$criteria->with = 'campaign';

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}


	public function getTagsStr()
	{
		$tags = array();

		foreach($this->tags as $tag){
			$tags [] = $tag->tag;
		}

		return implode(',', $tags);
	}

	public function afterFind()
	{
		$this->tagString = $this->tagsStr;
	}
}
