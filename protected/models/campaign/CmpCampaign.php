<?php

/**
 * This is the model class for table "cmp_campaign".
 *
 * The followings are the available columns in table 'cmp_campaign':
 * @property integer $id_campaign
 * @property string $name
 *
 * The followings are the available model relations:
 * @property CmpQuestion[] $cmpQuestions
 */
class CmpCampaign extends CActiveRecord
{
	public $qstnArray = array();
	public $courseBasicName;
	public $courseMediumName;
	public $courseAdvancedName;

	public function tableName()
	{
		return 'cmp_campaign';
	}

	public function rules()
	{
		return array(
			array('name, id_course_basic, id_course_medium, id_course_advanced, courseBasicName, courseMediumName, courseAdvancedName,qstnArray', 'required'),
			array('name', 'length', 'max'=>100),
			array('id_campaign, name', 'safe', 'on'=>'search'),
		);
	}

	public function relations()
	{
		return array(
			'questions' => array(self::HAS_MANY, 'CmpQuestion', 'id_campaign'),
			'basicCourse' => array(self::BELONGS_TO, 'Course', 'id_course_basic'),
			'mediumCourse' => array(self::BELONGS_TO, 'Course', 'id_course_medium'),
			'advancedCourse' => array(self::BELONGS_TO, 'Course', 'id_course_advanced'),
		);
	}

	public function attributeLabels()
	{
		return array(
			'id_campaign' => 'Código',
			'courseBasicName' => 'Curso básico',
			'courseMediumName' => 'Curso medio',
			'courseAdvancedName' => 'Curso avanzado',
			'name' => 'Nombre',
			'url' => 'Dirección',
		);
	}

	public function search()
	{
		$criteria=new CDbCriteria;

		$criteria->compare('name', $this->name,true);
		$criteria->compare('id_campaign', $this->id_campaign,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	public function getUrl()
	{
		if(!empty($this->id_campaign)){
			$baseUrl =  Yii::app()->request->hostInfo;
			$path = Yii::app()->createUrl('l/landing',array('id'=>$this->id_campaign));
			return $baseUrl . $path;
		} else {
			return NULL;
		}
	}

	public function getDeletable()
	{
		$connection=Yii::app()->db;
        		$command=$connection->createCommand();

		$command->setSelect("COUNT(id_user)");
		$command->setFrom(CmpAnswerUser::model()->tableName(). ' au');
		$command->join(CmpAnswerQuestion::model()->tableName() . ' aq', 'aq.id_question=au.id_question');
		$command->join(CmpQuestion::model()->tableName() . ' q', 'q.id_question=aq.id_question');
		$command->setWhere('q.id_campaign=:id_campaign');
		$command->params = array(':id_campaign'=>$this->id_campaign);
		$userAnswers = $command->queryScalar();

		return intval($userAnswers) === 0;
	}


	/**
	 * Guarda los sub objetos de una pregunta en la BD
	 * @return boolean si se pudo guardar correctamente todos los valores
	 */
	public function saveSubObjectsFromPost()
	{
		if(isset($_POST['question']))
		{
			$errors = 0;
			$index = 0;

			foreach ($_POST['question'] as $question)
			{
				if($index==0){
					$index++;
					continue;
				}

				if(empty($question['question'])){
					$this->addError('qstnArray', 'Debe escribir todos los datos de la pregunta');
					$errors++;
				} else {

					$qstn = NULL;

					if(!empty($question['id_question'])){
						$qstn = CmpQuestion::model()->findByPk($question['id_question']);
					}

					if($qstn){
						//Eliminar asociaciones de tags existentes (no se elimina el tag porque es util para el futuro)
						CmpTagQuestion::model()->deleteAll('id_question=:id_question', array('id_question'=>$qstn->id_question));
					} else {
						$qstn = new CmpQuestion;
					}

					$qstn->question = $question['question'];
					$qstn->id_campaign = $this->id_campaign;

					if(!$qstn->save()){
						return false;
					}

					//Guardar tags
					if(isset($question['tagString'])){
						$qstn->tagString = $question['tagString'];
						$tags = explode(',', $question['tagString']);

						foreach ($tags as $tag) {
							$t = new CmpTagQuestion;
							$t->id_question = $qstn->id_question;
							$t->id_tag = Tag::retrieveId($tag);

							if(!$t->save()){
								$qstn->addError('tagString', 'Chequee el formato de las etiquetas');
								$errors++;
							}
						}
					} else {
						$qstn->addError('tagString','Faltan las etiquetas');
						$errors++;
					}

					$qstn->answersArray = array();

					if(isset($question['answersArray']))
					{

						//Borrar las asociaciones de respuestas no respondidas por un usuario
						try{
							CmpAnswerQuestion::model()->deleteAll(array(
								'condition'=>'id_question=:id_question',
								'params'=> array(':id_question'=>$qstn->id_question)
							));
						} catch(CDbException $e){}

						//Guardar respuestas
						foreach ($question['answersArray'] as $idAnswer)
						{
							if(!CmpAnswerQuestion::model()->exists(
								'id_question=:id_question AND id_answer=:id_answer', array(
									'id_question'=>$qstn->id_question,
									'id_answer'=>$idAnswer,
							))) {
								$aq = new CmpAnswerQuestion;
								$aq->id_answer = $idAnswer;
								$aq->id_question = $qstn->id_question;

								if(!$aq->save()){
									$qstn->addError('answers', 'Seleccione alguna respuesta');
									$errors++;
								}
							}
							$qstn->answersArray[] = $idAnswer;
						}
					} else {
						$qstn->addError('answers', 'Faltan las respuestas de la preguntas');
						$errors++;
					}

					$this->qstnArray[] = $qstn;
				}

				$index++;
			}

			return $errors === 0;
		} else {
			return false;
		}
	}

	public function beforeValidate()
	{
		$this->id_course_basic = Course::getCourseIdFromName($this->courseBasicName);
		$this->id_course_medium = Course::getCourseIdFromName($this->courseMediumName);
		$this->id_course_advanced = Course::getCourseIdFromName($this->courseAdvancedName);

		if (count($this->qstnArray) === 0){
			$this->addError('questions','Falta por agregar las preguntas');
		}

		if($this->hasErrors()) {
			return false;
		}

		return parent::beforeValidate();
	}

	public function beforeDelete()
	{
		foreach($this->questions as $question){
			CmpTagQuestion::model()->deleteAll('id_question=:id_question', array(
				':id_question' => $question->id_question
			));

			CmpAnswerQuestion::model()->deleteAll('id_question=:id_question', array(
				':id_question' => $question->id_question
			));

			$question->delete();
		}

		return parent::beforeDelete();
	}

	public function afterFind()
	{
		if($this->basicCourse){
			$this->courseBasicName = $this->basicCourse->name;
		}

		if($this->mediumCourse){
			$this->courseMediumName = $this->mediumCourse->name;
		}

		if($this->advancedCourse){
			$this->courseAdvancedName = $this->advancedCourse->name;
		}
	}

	/**
	 * Imprime un campo de tipo curso
	 * @param  CActiveForm 	$form 		Objeto de tipo formulario para crear los campos activos
	 * @param  CController 	$controller 	Controlador actual
	 * @param  string 			$name 		nombre del atributo a usar
	 */
	public function printCourseField($form, $controller, $name)
	{
		?>
			<div class="row">
				<div class="form-group"	>
					<?php echo $form->label($this,$name, array('class'=>'col-lg-3')) ?>
					<div class="col-lg-9">
						<?php
							$controller->widget('zii.widgets.jui.CJuiAutoComplete',array(
								'model'=>$this,
								'attribute'=>$name,
								'sourceUrl'=> Yii::app()->createUrl('admin/obtenerCursos'),
								'options'=>array(
									'minLength'=>'2',
								),
								'htmlOptions'=>array('class'=>'form-control '),
							));
							echo $form->error($this,$name);
						?>
					</div>
				</div>
			</div>
		<?php

	}
}
