<?php

/**
 * This is the model class for table "cmp_tag_question".
 *
 * The followings are the available columns in table 'cmp_tag_question':
 * @property integer $id_question
 * @property integer $id_tag
 */
class CmpTagQuestion extends CActiveRecord
{
	public function tableName()
	{
		return 'cmp_tag_question';
	}

	public function rules()
	{
		return array(
			array('id_question, id_tag', 'required'),
			array('id_question, id_tag', 'numerical', 'integerOnly'=>true),
			array('id_question, id_tag', 'safe', 'on'=>'search'),
		);
	}

	public function relations()
	{
		return array(
		);
	}

	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
