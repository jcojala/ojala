<?php

/**
 * This is the model class for table "cmp_answer".
 *
 * The followings are the available columns in table 'cmp_answer':
 * @property integer $id_answer
 * @property string $value
 *
 * The followings are the available model relations:
 * @property CmpQuestion[] $cmpQuestions
 */
class CmpAnswer extends CActiveRecord
{
	public function tableName()
	{
		return 'cmp_answer';
	}

	public function rules()
	{
		return array(
			array('value', 'required'),
			array('value', 'length', 'max'=>45),
			array('id_answer, value', 'safe', 'on'=>'search'),
		);
	}

	public function relations()
	{
		return array(
			'questions' => array(self::MANY_MANY, 'CmpQuestion', 'cmp_answer_question(id_answer, id_question)'),
		);
	}

	public function attributeLabels()
	{
		return array(
			'id_answer' => 'Id Answer',
			'value' => 'Value',
		);
	}

	public function search()
	{
		$criteria=new CDbCriteria;

		$criteria->compare('id_answer',$this->id_answer);
		$criteria->compare('value',$this->value,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
