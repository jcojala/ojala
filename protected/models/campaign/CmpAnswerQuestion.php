<?php

/**
 * This is the model class for table "cmp_answer_question".
 *
 * The followings are the available columns in table 'cmp_answer_question':
 * @property integer $id_answer
 * @property integer $id_question
 *
 * The followings are the available model relations:
 * @property CmpAnswerUser[] $cmpAnswerUsers
 * @property CmpAnswerUser[] $cmpAnswerUsers1
 */
class CmpAnswerQuestion extends CActiveRecord
{
	public function tableName()
	{
		return 'cmp_answer_question';
	}

	public function rules()
	{
		return array(
			array('id_answer, id_question', 'required'),
			array('id_answer, id_question', 'numerical', 'integerOnly'=>true),
			array('id_answer, id_question', 'safe', 'on'=>'search'),
		);
	}

	public function relations()
	{
		return array(
			'answers' => array(self::HAS_MANY, 'CmpAnswerUser', 'id_answer'),
			'questions' => array(self::HAS_MANY, 'CmpAnswerUser', 'id_question'),
		);
	}

	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
