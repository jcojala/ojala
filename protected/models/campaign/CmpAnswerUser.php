<?php

/**
 * This is the model class for table "cmp_answer_user".
 *
 * The followings are the available columns in table 'cmp_answer_user':
 * @property integer $id_answer
 * @property integer $id_question
 * @property integer $id_user
 *
 * The followings are the available model relations:
 * @property CmpAnswerQuestion $idAnswer
 * @property CmpAnswerQuestion $idQuestion
 * @property User $idUser
 */
class CmpAnswerUser extends CActiveRecord
{
	public function tableName()
	{
		return 'cmp_answer_user';
	}

	public function rules()
	{
		return array(
			array('id_answer, id_question, id_user', 'required'),
			array('id_answer, id_question, id_user', 'numerical', 'integerOnly'=>true),
			array('id_answer, id_question, id_user', 'safe', 'on'=>'search'),
		);
	}

	public function relations()
	{
		return array(
			'answer' => array(self::BELONGS_TO, 'CmpAnswerQuestion', 'id_answer'),
			'question' => array(self::BELONGS_TO, 'CmpAnswerQuestion', 'id_question'),
			'user' => array(self::BELONGS_TO, 'User', 'id_user'),
		);
	}

	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
