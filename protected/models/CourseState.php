<?php

/**
 * This is the model class for table "course_state".
 *
 * The followings are the available columns in table 'course_state':
 * @property integer $id_cstate
 * @property integer $id_course
 * @property integer $id_stype
 * @property integer $active
 *
 * The followings are the available model relations:
 * @property Course $idCourse
 * @property State $idStype
 */
class CourseState extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'course_state';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id_course, id_stype', 'required'),
			array('id_course, id_stype, active', 'numerical', 'integerOnly'=>true),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id_cstate, id_course, id_stype, active', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'idCourse' => array(self::BELONGS_TO, 'Course', 'id_course'),
			'idStype' => array(self::BELONGS_TO, 'State', 'id_stype'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_cstate' => 'Id Cstate',
			'id_course' => 'Id Course',
			'id_stype' => 'Id Stype',
			'active' => 'Active',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id_cstate',$this->id_cstate);
		$criteria->compare('id_course',$this->id_course);
		$criteria->compare('id_stype',$this->id_stype);
		$criteria->compare('active',$this->active);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return CourseState the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
