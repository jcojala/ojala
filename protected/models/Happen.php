<?php

/**
 * This is the model class for table "happen".
 *
 * The followings are the available columns in table 'happen':
 * @property integer $id_happen
 * @property integer $id_event
 * @property integer $id_user
 *
 * The followings are the available model relations:
 * @property Event $idEvent
 * @property User $idUser
 */
class Happen extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'happen';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id_event, id_user', 'required'),
			array('id_campaign', 'safe'),
			array('id_event, id_user', 'numerical', 'integerOnly'=>true),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id_happen, id_event, id_user, id_campaign', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'idEvent' => array(self::BELONGS_TO, 'Event', 'id_event'),
			'idUser' => array(self::BELONGS_TO, 'User', 'id_user'),
			'idCampaign' => array(self::BELONGS_TO, 'Campaign', 'id_campaign'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_happen' => 'Id Happen',
			'id_event' => 'Id Event',
			'id_user' => 'Id User',
			'id_campaign' => 'Id Campaign',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id_happen',$this->id_happen);
		$criteria->compare('id_event',$this->id_event);
		$criteria->compare('id_user',$this->id_user);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Happen the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	public function beforeValidate()
	{
		if(empty($this->id_user))
			$this->id_user = Yii::app()->user->id;

		if(isset(Yii::app()->request->cookies['utm_campaign'])){
			$campaign = Campaign::model()->findByAttributes( array(
				'utm_campaign'=> isset(Yii::app()->request->cookies['utm_campaign']) ? Yii::app()->request->cookies['utm_campaign'] : NULL,
				'utm_medium'=>isset(Yii::app()->request->cookies['utm_medium']) ? Yii::app()->request->cookies['utm_medium'] : NULL,
				'utm_term'=>isset(Yii::app()->request->cookies['utm_term']) ? Yii::app()->request->cookies['utm_term'] : NULL,
				'utm_content'=>isset(Yii::app()->request->cookies['utm_content']) ? Yii::app()->request->cookies['utm_content'] : NULL,
				'utm_source'=>isset(Yii::app()->request->cookies['utm_source']) ? Yii::app()->request->cookies['utm_source'] : NULL
			));
			$this->id_campaign = empty($campaign) ? NULL : $campaign->id_campaign;
		} else {
			$this->id_campaign = NULL;
		}

		return parent::beforeValidate();
	}
}
