<?php

/**
 * This is the model class for table "pay_information".
 *
 * The followings are the available columns in table 'pay_information':
 * @property integer $id_pinfo
 * @property integer $id_pay
 * @property integer $id_pmode
 * @property integer $id_ptype
 * @property double $amount
 * @property integer $active
 *
 * The followings are the available model relations:
 * @property Pay $idPay
 * @property PayMode $idPmode
 * @property PayType $idPtype
 */
class PayInformation extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'pay_information';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id_pay, id_pmode, id_ptype', 'required'),
			array('id_pay, id_pmode, id_ptype, active', 'numerical', 'integerOnly'=>true),
			array('amount', 'numerical'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id_pinfo, id_pay, id_pmode, id_ptype, amount, active', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'idPay' => array(self::BELONGS_TO, 'Pay', 'id_pay'),
			'idPmode' => array(self::BELONGS_TO, 'PayMode', 'id_pmode'),
			'idPtype' => array(self::BELONGS_TO, 'PayType', 'id_ptype'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_pinfo' => 'Id Pinfo',
			'id_pay' => 'Id Pay',
			'id_pmode' => 'Id Pmode',
			'id_ptype' => 'Id Ptype',
			'amount' => 'Amount',
			'active' => 'Active',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id_pinfo',$this->id_pinfo);
		$criteria->compare('id_pay',$this->id_pay);
		$criteria->compare('id_pmode',$this->id_pmode);
		$criteria->compare('id_ptype',$this->id_ptype);
		$criteria->compare('amount',$this->amount);
		$criteria->compare('active',$this->active);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return PayInformation the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
