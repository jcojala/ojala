<?php

/**
 * This is the model class for table "customers".
 *
 * The followings are the available columns in table 'customers':
 * @property string $id
 * @property string $Description
 * @property string $Email
 * @property string $Created
 * @property string $Delinquent
 * @property string $CardLast4
 * @property string $CardType
 * @property string $CardExpMonth
 * @property string $CardExpYear
 * @property string $CardName
 * @property string $CardAddressLine1
 * @property string $CardAddressLine2
 * @property string $CardAddressCity
 * @property string $CardAddressState
 * @property string $CardAddressCountry
 * @property string $CardAddressZip
 * @property string $CardIssueCountry
 * @property string $CardFingerprint
 * @property string $CardCVCStatus
 * @property string $CardAVSZipStatus
 * @property string $CardAVSLine1Status
 * @property string $Plan
 */
class Customers extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'customers';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id, Description, Email, Created, Delinquent, CardLast4, CardType, CardExpMonth, CardExpYear, CardName, CardAddressLine1, CardAddressLine2, CardAddressCity, CardAddressState, CardAddressCountry, CardAddressZip, CardIssueCountry, CardFingerprint, CardCVCStatus, CardAVSZipStatus, CardAVSLine1Status, Plan', 'length', 'max'=>255),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, Description, Email, Created, Delinquent, CardLast4, CardType, CardExpMonth, CardExpYear, CardName, CardAddressLine1, CardAddressLine2, CardAddressCity, CardAddressState, CardAddressCountry, CardAddressZip, CardIssueCountry, CardFingerprint, CardCVCStatus, CardAVSZipStatus, CardAVSLine1Status, Plan', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'Description' => 'Description',
			'Email' => 'Email',
			'Created' => 'Created',
			'Delinquent' => 'Delinquent',
			'CardLast4' => 'Card Last4',
			'CardType' => 'Card Type',
			'CardExpMonth' => 'Card Exp Month',
			'CardExpYear' => 'Card Exp Year',
			'CardName' => 'Card Name',
			'CardAddressLine1' => 'Card Address Line1',
			'CardAddressLine2' => 'Card Address Line2',
			'CardAddressCity' => 'Card Address City',
			'CardAddressState' => 'Card Address State',
			'CardAddressCountry' => 'Card Address Country',
			'CardAddressZip' => 'Card Address Zip',
			'CardIssueCountry' => 'Card Issue Country',
			'CardFingerprint' => 'Card Fingerprint',
			'CardCVCStatus' => 'Card Cvcstatus',
			'CardAVSZipStatus' => 'Card Avszip Status',
			'CardAVSLine1Status' => 'Card Avsline1 Status',
			'Plan' => 'Plan',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('Description',$this->Description,true);
		$criteria->compare('Email',$this->Email,true);
		$criteria->compare('Created',$this->Created,true);
		$criteria->compare('Delinquent',$this->Delinquent,true);
		$criteria->compare('CardLast4',$this->CardLast4,true);
		$criteria->compare('CardType',$this->CardType,true);
		$criteria->compare('CardExpMonth',$this->CardExpMonth,true);
		$criteria->compare('CardExpYear',$this->CardExpYear,true);
		$criteria->compare('CardName',$this->CardName,true);
		$criteria->compare('CardAddressLine1',$this->CardAddressLine1,true);
		$criteria->compare('CardAddressLine2',$this->CardAddressLine2,true);
		$criteria->compare('CardAddressCity',$this->CardAddressCity,true);
		$criteria->compare('CardAddressState',$this->CardAddressState,true);
		$criteria->compare('CardAddressCountry',$this->CardAddressCountry,true);
		$criteria->compare('CardAddressZip',$this->CardAddressZip,true);
		$criteria->compare('CardIssueCountry',$this->CardIssueCountry,true);
		$criteria->compare('CardFingerprint',$this->CardFingerprint,true);
		$criteria->compare('CardCVCStatus',$this->CardCVCStatus,true);
		$criteria->compare('CardAVSZipStatus',$this->CardAVSZipStatus,true);
		$criteria->compare('CardAVSLine1Status',$this->CardAVSLine1Status,true);
		$criteria->compare('Plan',$this->Plan,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Customers the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
