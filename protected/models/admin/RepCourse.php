<?php

/**
 * This is the model class for table "rep_course".
 *
 * The followings are the available columns in table 'rep_course':
 * @property integer $id_course
 * @property string $name
 * @property string $na_category
 * @property string $instructor_name
 */
class RepCourse extends CActiveRecordAdmin
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'rep_course';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('name', 'required'),
			array('id_course', 'numerical', 'integerOnly'=>true),
			array('name, na_category', 'length', 'max'=>500),
			array('instructor_name', 'length', 'max'=>100),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id_course, name, na_category, instructor_name', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_course' => 'Id Course',
			'name' => 'Name',
			'na_category' => 'Na Category',
			'instructor_name' => 'Instructor Name',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id_course',$this->id_course);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('na_category',$this->na_category,true);
		$criteria->compare('instructor_name',$this->instructor_name,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * @return CDbConnection the database connection used for this class
	 */
	public function getDbConnection()
	{
		return Yii::app()->db2;
	}
	
	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecordAdmin descendants!
	 * @param string $className active record class name.
	 * @return RepCourse the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
