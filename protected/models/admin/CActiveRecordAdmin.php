<?php
/**
* Class to override DB connection
*/
class CActiveRecordAdmin extends CActiveRecord
{

	public function getDbConnection()
	{
		return Yii::app()->db2;
	}
}