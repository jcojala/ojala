<?php
/**
* Función para visualizar los suscriptores
*/
class Subscribers extends CFormModel
{
	public $active;
	public $email1;
	public $email2;
	public $fullname;
	public $id_service;
	public $id_user;
	public $lastStatus;
	public $subscriptionDate;
	public $fromSubDate;
	public $toSubDate;
	public $gateway;

	public function rules(){
 		return array(
			array('active, email1, email2, fullname, id_service, id_user, lastStatus, subscriptionDate, fromSubDate, toSubDate, gateway', 'safe')
		);
	}

	/**
	 * Función para obtener el estado actual de la suscripción
	 */
	public static function getSubscriptionState($subscriptionState, $lastStatus, $lastStatusDate)
	{
		$estado = 'N/D';
		$lastStatusDate = empty($lastStatusDate) ? 'N/D' : OjalaUtils::convertDateFromDB($lastStatusDate);
		$status = isset($lastStatus) ? $lastStatus : 1;

		switch($subscriptionState){
			case 1:
				$estado = '<span class="text-success">Activa</span>';
				break;
			case 2:
				$estado = "<span class=\"text-danger\">Deudora ($lastStatusDate)</span>";
				break;
			case 0:
				switch($status){
					case 1:
						$status = 'Inactiva';
						break;
					case 2:
						$status = 'Cancelada';
						break;
					case 3:
						$status = 'Deudora';
						break;
					case 4:
						$status = 'Esperando pago';
						break;
					case 5:
						$status = 'Oculto';
						break;
				}
				$estado = "<span class=\"text-danger\">$status ($lastStatusDate)</span>";
				break;
		}

		return $estado;
	}

	/**
	 * Función para generar el url del perfil remoto (Si existe)
	 */
	static public function getGatewayProfile($idService, $subIdService)
	{
		if(substr($idService, 0,4) === 'cus_'){
			return 'https://dashboard.stripe.com/customers/' . $idService;
		}

		if(substr($subIdService, 0,2) == 'I-' || substr($idService, 0,2) == 'I-' ){
			return PaypalWrapper::getDomain() . 'us/cgi-bin/webscr?cmd=_profile-recurring-payments&encrypted_profile_id=' . $subIdService;
		}

		return '';
	}

	/**
	 * Función para conocer el gateway utilizado
	 */
	static public function getGateway($id_service, $id_stype)
	{
		$prefix = substr($id_service, 0, 2);

		switch($id_stype){
			case SuscriptionType::SCHOLARSHIP:
				return 'Beca';
			case SuscriptionType::TWOBYONE:
				return 'Amigos 2x1';
		}

		switch($prefix){
			case 'NP':
				return 'NPI';
			case 'su':
				return 'Stripe';
			case 'I-':
				return 'Paypal';
			case 'SU':
			case '53':
				return 'Conekta';
			default:
				return 'Desconocido';
		}
	}

	static public function gatewayList($emptyValue = '')
	{
		return array(
			''=>$emptyValue,
			'su'=>'Stripe',
			'I-'=>'Paypal',
			'SU'=>'Conekta',
			'Beca'=>'Beca',
			'Amigos2x1'=>'Amigos 2x1',
			'NPI'=>'NPI'
		);
	}

	public function getSQL()
	{
		$maxDate = NULL;

		if(!empty($this->toSubDate)){
			$maxDate .= sprintf("AND s2.`date`<='%s 23:59:59' ", OjalaUtils::convertDateToDB($this->toSubDate));
		}

		$sql = "SELECT CONCAT(IFNULL(u.`name`,''), ' ',IFNULL(u.`lastname`,'')) as fullname,`email1`,`email2`, 
					s1.id_suscription, u.`id_service`,  s1.`date` AS subscriptionDate, s1.`active`,u.`id_user`,
					s1.id_service AS subIdService, s1.exp_date, s1.id_stype, s1.id_ptype, 
					ss.`date` AS lastStatusDate, ss.`id_status` AS lastStatus, st.currency
				FROM suscription s1
				LEFT OUTER JOIN suscription s2
					ON s1.id_user = s2.id_user AND s1.`date` < s2.`date` $maxDate
				INNER JOIN `user` u ON s1.id_user=u.id_user
				LEFT JOIN `suscription_status` ss ON ss.id_suscription=s1.id_suscription AND ss.active = 1
				LEFT JOIN `suscription_type` st ON st.id_stype=s1.id_stype
				WHERE s2.id_user IS NULL AND s1.id_course IS NULL ";

		if(!empty($this->fullname)){
			$sql .= sprintf("AND (u.name LIKE '%s%%' OR u.lastname LIKE '%s%%') ", $this->fullname,$this->fullname);
		}

		if(!empty($this->email1)){
			$sql .= sprintf("AND u.email1 LIKE '%s%%' ", $this->email1);
		}

		if(!empty($this->email2)){
			$sql .= sprintf("AND u.email2 LIKE '%s%%' ", $this->email2);
		}

		if(!empty($this->subscriptionDate)){
			$sql .= sprintf("AND DATE(s1.`date`)='%s' ", OjalaUtils::convertDateToDB($this->subscriptionDate));
		}

		if(is_numeric($this->lastStatus) && $this->lastStatus != Status::HIDDEN){
			$sql .= sprintf("AND IFNULL(ss.`id_status`,1) =%s AND s1.active=1 ", $this->lastStatus);
		}

		if($this->lastStatus != Status::HIDDEN){
			$sql .= sprintf("AND IFNULL(ss.`id_status`,1) !=5 AND NOT (s1.id_stype=17 AND s1.active=0) ", $this->lastStatus);
		}

		if($this->lastStatus == Status::HIDDEN){
			$sql .= sprintf("AND IFNULL(ss.`id_status`,1) = %s ", Status::HIDDEN);
		}

		if(!empty($this->gateway)){
			$sql .= sprintf("AND s1.id_service COLLATE utf8_bin LIKE '%s%%' ", $this->gateway);
		}

		if(!empty($this->fromSubDate)){
			$sql .= sprintf("AND s1.`date`>='%s 00:00:00' ", OjalaUtils::convertDateToDB($this->fromSubDate));
		}

		if(!empty($this->toSubDate)){
			$sql .= sprintf("AND s1.`date`<='%s 23:59:59' ", OjalaUtils::convertDateToDB($this->toSubDate));
		}

		$sql .= " GROUP BY s1.id_user ";

		//echo $sql;die;

		return $sql;
	}

	public function getSqlDataProvider()
	{

		$sql = $this->getSQL();
		$rawData = Yii::app()->db->createCommand($sql);
		$count = Yii::app()->db->createCommand("SELECT COUNT(*) FROM (${sql}) as count_alias")->queryScalar();

		$model = new CSqlDataProvider($rawData, array(
			'keyField' => 'id_user', 
			'totalItemCount' => $count,
				'sort' => array(
					'attributes' => array(
						'id_user','fullname', 'email1','email2','id_service',
						'active','lastStatus',
						'subscriptionDate'=>array(
							'asc'=>'s1.date ASC',
							'desc'=>'s1.date DESC'
						),
						'gateway'=>array(
							'asc'=>'s1.id_service ASC',
							'desc'=>'s1.id_service DESC'
						)
					),
					'defaultOrder' => 's1.`date` DESC',
				),
				'pagination' => array(
					'pageSize' => 200,
				),
		));

		return $model;
	}
}
