<?php
 /**
 * Clase para generar los reportes en el lado admin
 */
 class Reports 
 {
 	
 	function __construct()
 	{
 		# code...
 	}

 	private function registerPHPExcel()
    {
        Yii::import('ext.yiiexcel.YiiExcel', true);
        Yii::registerAutoloader(array('YiiExcel', 'autoload'), true);
     
        PHPExcel_Shared_ZipStreamWrapper::register();
     
        if (ini_get('mbstring.func_overload') & 2) {
            throw new Exception('Multibyte function overloading in PHP must be disabled for string functions (2).');
        }

        PHPExcel_Shared_String::buildCharacterSets();
    }


 	public function createSuscriptores($type = 'excel5', $render = true)
 	{
 		$this->registerPHPExcel();
        $dbData = $this->getSuscriptoresData();
        $r = new YiiReport(array('template'=> 'suscriptores.xls'));
        
        $r->load(array(array(
            'id'=>'sus',
            'repeat'=>true,
            'data'=>$dbData,
            'minRows'=>1
        )));
        
        if($render)
            echo $r->render($type, 'ReporteSuscriptores');
        else
            return $r->save($type, 'ReporteSuscriptores');
 	}

 	public function getSuscriptoresData($tipor=null, $deudores = 0)
 	{
        if($tipor && $tipor=="mensual")
            $tipo="month";
        else
            $tipo="week";

 		$connection=Yii::app()->db2;
        $command=$connection->createCommand();
        $command->setSelect("rt.id_time, rt.week, rt.month, rt.year");
        $command->setFrom(RepHappen::model()->tableName() . ' rh');
        $command->join(RepTime::model()->tableName() . ' rt', "rt.id_time=rh.id_time");
        $command->setWhere("rt.id_time > '2013-03-05'");
        $command->setGroup("rt.".$tipo.', rt.year');
        $command->setOrder("rt.year ASC, rt.month ASC, rt.week ASC");
        $list=$command->queryAll();
        $list2=array();
        $acumulado=0;

        $count=1;
        foreach($list as $item)
        {
            $command=$connection->createCommand();
            $command->setSelect("rh.id_happen");
            $command->setFrom(RepHappen::model()->tableName() . ' rh');
            $command->join(RepEvent::model()->tableName() . ' re', "re.id_event=rh.id_event");
            $command->join(RepTime::model()->tableName() . ' rt', "rt.id_time=rh.id_time");
            $command->setWhere("re.na_event='suscriptor' AND rt.".$tipo."=".$item[$tipo]." AND rt.year=".$item['year']);
            $nuevos = count($command->queryAll());

            $command=$connection->createCommand();
            $command->setSelect("rh.id_happen");
            $command->setFrom(RepHappen::model()->tableName() . ' rh');
            $command->join(RepEvent::model()->tableName() . ' re', "re.id_event=rh.id_event");
            $command->join(RepTime::model()->tableName() . ' rt', "rt.id_time=rh.id_time");
            $command->setWhere("re.na_event='cancelado' AND rt.".$tipo."=".$item[$tipo]." AND rt.year=".$item['year']);
            $cancelados = count($command->queryAll());

            $acumulado+=$nuevos-$cancelados;
            $crate=0;
            if($acumulado>0)
                $crate=($cancelados*100)/$acumulado;

            $ref = date('d-m-Y', strtotime($item['id_time'].' +1 day'));
            $inicio = date('d-m-Y', strtotime('previous Wednesday '.$ref));
            $fin = date('d-m-Y', strtotime('next Tuesday '.$inicio));

            $inibd = date('Y-m-d', strtotime($inicio));
            $finbd = date('Y-m-d', strtotime($fin));

            if($tipor && $tipor == "mensual")
            {
                $d = new DateTime(date('Y-m-d', strtotime($item['id_time'])));
                $d->modify('first day of this month');
                $inicio = $d->format('d-m-Y');
                $d->modify('last day of this month');
                $fin = $d->format('d-m-Y');

                $inibd = date('Y-m-d', strtotime($inicio));
                $finbd = date('Y-m-d', strtotime($fin));
            }

            $list2[]=$item+array('nuevos'=>$nuevos, 'cancelados'=>$cancelados, 'numero'=>$count, 'acumulado'=>$acumulado, 'crate'=>round($crate, 2).' %', 'inicio'=>$inicio, 'fin'=>$fin, 'inibd'=>$inibd, 'finbd'=>$finbd);
            $count++;
        }

        return array_reverse($list2);
 	}
}
