<?php

/**
 * This is the model class for table "weekly_report".
 *
 * The followings are the available columns in table 'weekly_report':
 * @property integer $week
 * @property string $start_date
 * @property string $end_date
 * @property string $cant_suscriptores
 * @property string $nuevos
 * @property string $cancelados
 * @property string $logrado
 */
class WeeklyReport extends CActiveRecordAdmin
{
	const INITIAL_PROJECTION_MONEY = 658;
	const INITIAL_PROJECTION_DATE = '2013-10-02';
	private $initialProjectDateTime;

	public function __construct($scenario)
	{
		$this->initialProjectDateTime = new DateTime(self::INITIAL_PROJECTION_DATE);
	  	parent::__construct($scenario);
	}

	public function tableName()
	{
		return 'weekly_report';
	}

	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('week', 'numerical', 'integerOnly'=>true),
			array('cant_suscriptores, nuevos, cancelados', 'length', 'max'=>21),
			array('logrado', 'length', 'max'=>32),
			array('start_date, end_date', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('week, start_date, end_date, cant_suscriptores, nuevos, cancelados, logrado', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'week' => 'Week',
			'start_date' => 'Fecha de inicio',
			'end_date' => 'Fecha final',
			'cant_suscriptores' => 'Suscriptores',
			'nuevos' => 'Nuevos',
			'cancelados' => 'Cancelados',
			'logrado' => 'Logrado',
		);
	}

	public function getGoal()
	{
		$tmpDate = new DateTime($this->start_date);
		return pow(1.1,round($tmpDate->diff($this->initialProjectDateTime)->days/7)) * self::INITIAL_PROJECTION_MONEY;
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('week',$this->week);
		$criteria->compare('start_date',$this->start_date,true);
		//$criteria->compare('goal',$this->goal,true);
		$criteria->compare('end_date',$this->end_date,true);
		$criteria->compare('cant_suscriptores',$this->cant_suscriptores,true);
		$criteria->compare('nuevos',$this->nuevos,true);
		$criteria->compare('cancelados',$this->cancelados,true);
		$criteria->compare('logrado',$this->logrado,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * @return CDbConnection the database connection used for this class
	 */
	public function getDbConnection()
	{
		return Yii::app()->db2;
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecordAdmin descendants!
	 * @param string $className active record class name.
	 * @return WeeklyReport the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
