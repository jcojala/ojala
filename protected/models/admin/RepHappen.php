<?php

/**
 * This is the model class for table "rep_happen".
 *
 * The followings are the available columns in table 'rep_happen':
 * @property integer $id_happen
 * @property string $id_time
 * @property integer $id_user
 * @property integer $id_course
 * @property integer $id_lesson
 * @property integer $id_suscription
 * @property integer $id_group
 * @property integer $id_utm
 * @property integer $id_event
 * @property integer $id_currency
 * @property integer $id_ptype
 * @property string $monto
 * @property string $city
 * @property string $country
 * @property string $created_at
 */
class RepHappen extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'rep_happen';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id_time, id_user, created_at', 'required'),
			array('id_user, id_course, id_lesson, id_suscription, id_group, id_utm, id_event, id_currency, id_ptype', 'numerical', 'integerOnly'=>true),
			array('monto', 'length', 'max'=>10),
			array('city, country', 'length', 'max'=>50),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id_happen, id_time, id_user, id_course, id_lesson, id_suscription, id_group, id_utm, id_event, id_currency, id_ptype, monto, city, country, created_at', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_happen' => 'Id Happen',
			'id_time' => 'Id Time',
			'id_user' => 'Id User',
			'id_course' => 'Id Course',
			'id_lesson' => 'Id Lesson',
			'id_suscription' => 'Id Suscription',
			'id_group' => 'Id Group',
			'id_utm' => 'Id Utm',
			'id_event' => 'Id Event',
			'id_currency' => 'Id Currency',
			'id_ptype' => 'Id Ptype',
			'monto' => 'Monto',
			'city' => 'City',
			'country' => 'Country',
			'created_at' => 'Created At',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id_happen',$this->id_happen);
		$criteria->compare('id_time',$this->id_time,true);
		$criteria->compare('id_user',$this->id_user);
		$criteria->compare('id_course',$this->id_course);
		$criteria->compare('id_lesson',$this->id_lesson);
		$criteria->compare('id_suscription',$this->id_suscription);
		$criteria->compare('id_group',$this->id_group);
		$criteria->compare('id_utm',$this->id_utm);
		$criteria->compare('id_event',$this->id_event);
		$criteria->compare('id_currency',$this->id_currency);
		$criteria->compare('id_ptype',$this->id_ptype);
		$criteria->compare('monto',$this->monto,true);
		$criteria->compare('city',$this->city,true);
		$criteria->compare('country',$this->country,true);
		$criteria->compare('created_at',$this->created_at,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * @return CDbConnection the database connection used for this class
	 */
	public function getDbConnection()
	{
		return Yii::app()->db2;
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return RepHappen the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
