<?php

/**
 * This is the model class for table "pay_mode".
 *
 * The followings are the available columns in table 'pay_mode':
 * @property integer $id_pmode
 * @property integer $id_ptype
 * @property integer $id_suscription
 * @property double $amount
 * @property string $id_service
 * @property string $card
 * @property string $ccv
 * @property string $month
 * @property string $year
 * @property string $user
 * @property string $info
 * @property integer $active
 *
 * The followings are the available model relations:
 * @property PayInformation[] $payInformations
 * @property PayType $idPtype
 * @property Suscription $idSuscription
 */
class PayMode extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'pay_mode';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id_ptype, id_suscription', 'required'),
			array('id_ptype, id_suscription, active', 'numerical', 'integerOnly'=>true),
			array('amount', 'numerical'),
			array('id_service, ccv, month, year, user', 'length', 'max'=>45),
			array('card, info', 'length', 'max'=>100),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id_pmode, id_ptype, id_suscription, amount, id_service, card, ccv, month, year, user, info, active', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'payInformations' => array(self::HAS_MANY, 'PayInformation', 'id_pmode'),
			'idPtype' => array(self::BELONGS_TO, 'PayType', 'id_ptype'),
			'idSuscription' => array(self::BELONGS_TO, 'Suscription', 'id_suscription'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_pmode' => 'Id Pmode',
			'id_ptype' => 'Id Ptype',
			'id_suscription' => 'Id Suscription',
			'amount' => 'Amount',
			'id_service' => 'Id Service',
			'card' => 'Card',
			'ccv' => 'Ccv',
			'month' => 'Month',
			'year' => 'Year',
			'user' => 'User',
			'info' => 'Info',
			'active' => 'Active',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id_pmode',$this->id_pmode);
		$criteria->compare('id_ptype',$this->id_ptype);
		$criteria->compare('id_suscription',$this->id_suscription);
		$criteria->compare('amount',$this->amount);
		$criteria->compare('id_service',$this->id_service,true);
		$criteria->compare('card',$this->card,true);
		$criteria->compare('ccv',$this->ccv,true);
		$criteria->compare('month',$this->month,true);
		$criteria->compare('year',$this->year,true);
		$criteria->compare('user',$this->user,true);
		$criteria->compare('info',$this->info,true);
		$criteria->compare('active',$this->active);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return PayMode the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
