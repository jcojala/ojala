<?php

/**
 * This is the model class for table "user".
 *
 * The followings are the available columns in table 'user':
 * @property integer $id_user
 * @property integer $id_utype
 * @property integer $id_refuser
 * @property integer $id_campaign
 * @property string $role
 * @property string $user
 * @property string $pass
 * @property string $name
 * @property string $lastname
 * @property string $gender
 * @property string $image
 * @property string $link
 * @property string $birthday
 * @property string $email1
 * @property string $email2
 * @property string $email_status
 * @property string $intercom
 * @property string $timezone
 * @property string $newsletter
 * @property string $occupation
 * @property string $create
 * @property string $last_sign
 * @property string $last_ip
 * @property string $curr_sign
 * @property string $curr_ip
 * @property string $verifcode
 * @property string $nickname
 * @property string $about
 * @property string $fb_id
 * @property string $id_service
 * @property integer $id_refuser
 * @property string $invitation_from
 *
 * The followings are the available model relations:
 * @property Course[] $courses
 * @property Course[] $courses1
 * @property Happen[] $happens
 * @property Link[] $links
 * @property Search[] $searches
 * @property Suscription[] $suscriptions
 * @property Campaign $idCampaign
 * @property User $idRefuser
 * @property User[] $users
 * @property UserType $idUtype
 */
class User extends CActiveRecord
{
	const MAX_SCHOLARSHIP_MONTHS = '12';

	const TYPE_ADMIN = 1;
	const TYPE_INSTRUCTOR = 2;
	const TYPE_STUDENT = 3;
	const TYPE_LEAD = 4;
	const TYPE_AAC = 5;

	const ERROR_NOT_FOUND = 'No se ha encontrado el usuario(a)';

	public $repeatpass;
	public $subscriptions;
	public $phoneType;

	public function tableName()
	{
		return 'user';
	}

	public function rules()
	{
		return array(

			//Basic Validations
			array('id_utype, user, name', 'required', 'except'=>'subscriber'),

			array('id_utype, id_refuser, id_campaign', 'numerical', 'integerOnly'=>true),
			array('name, lastname, occupation, nickname', 'match', 'pattern' => '/^[0-9a-zA-Z_áéíóúñ\s]*$/', 'message' => 'Este campo solo puede contener letras', 'except'=>'subscriber,changePass'),
			array('id_utype, user, email1', 'required', 'message'=>'Disculpa, pero necesitamos esta información'),
			array('user, lastname, birthday, email1, email2, verifcode', 'length', 'max'=>100),
			array('pass, occupation', 'length', 'max'=>255),
			array('phone', 'length', 'max'=>20),

			array('role, gender, intercom, timezone, last_sign, last_ip, curr_sign, curr_ip', 'length', 'max'=>45),
			array('email_status, newsletter', 'length', 'max'=>5),
			array('date, nickname, about, id_service, phoneType','safe'),

			// Email validations
			array('email1', 'unique', 'on' => 'insert', 'message' => 'Parece que ya tienes una cuenta en Oja.la'),
			array('email1', 'email', 'message' => 'Asegurate que tu correo esté bien escrito!'),

			// User validations
			array('name', 'length', 'max'=>200),
			array('image, link', 'length', 'max'=>500),
			array('name', 'required', 'message'=>'Nos gustría conocerte mejor, escribe tu nombre'),

			//Subscriber modifications
			array('id_service, id_user','required','on'=>'subscriber'),

			//Password
			array('repeatpass','safe','on'=>'insert,changePass'),
			array('pass', 'required', 'message'=>'Para tu seguridad necesitamos una clave de mínimo 5 carácteres','on'=>'insert,changePass'),
			array('pass, repeatpass', 'length', 'min'=>5, 'tooShort'=>Yii::t("translation", "Revisa tu contraseña, mínimo 5 carácteres, queremos q sea segura"),'on'=>'insert, changePass'),
			array('repeatpass', 'compare', 'compareAttribute'=>'pass', 'message'=>"Asegurate que las contraseñas coincidan", 'on'=>'insert,changePass'),
			array('repeatpass', 'required', 'message'=>'Y para estar seguros, necesitamos que repitas tu clave','on'=>'insert,changePass'),

			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id_user, name, id_service, lastname, email1, email2, subscriptions', 'safe', 'on'=>'search'),
		);
	}

	public function relations()
	{
		return array(
			'courses' => array(self::HAS_MANY, 'Course', 'id_user'),
			'courses1' => array(self::HAS_MANY, 'Course', 'id_instructor'),
			'happens' => array(self::HAS_MANY, 'Happen', 'id_user'),
			'links' => array(self::HAS_MANY, 'Link', 'id_user'),
                        'linkProposals' => array(self::HAS_MANY, 'LinkProposal', 'id_user'),
			'searches' => array(self::HAS_MANY, 'Search', 'id_user'),
			'suscriptions' => array(self::HAS_MANY, 'Suscription', 'id_user', 'order'=>'suscriptions.date DESC'),
			'diplomados' => array(self::MANY_MANY, 'Group', 'group_suscription(id_user, id_group)'),
			'subDiplo' => array(self::HAS_MANY, 'GroupSuscription', 'id_user','on'=>'subDiplo.active=1'),
			'idCampaign' => array(self::BELONGS_TO, 'Campaign', 'id_campaign'),
			'idRefuser' => array(self::BELONGS_TO, 'User', 'id_refuser'),
			'users' => array(self::HAS_MANY, 'User', 'id_refuser'),
			'idUtype' => array(self::BELONGS_TO, 'UserType', 'id_utype'),
			'campaignAnswers' => array(self::HAS_MANY, 'CmpAnswerUser', 'user_id_user'),
			'region' => array(self::BELONGS_TO, 'Region', 'id_region'),
		);
	}

	public function attributeLabels()
	{
		return array(
			'id_user' => 'Código',
			'id_utype' => 'Tipo de usuario',
			'id_refuser' => 'Referido por',
			'id_campaign' => 'Id Campaign',
			'role' => 'Rol',
			'pass' => 'Contraseña',
			'repeatpass' => 'Confirma tu contraseña',
			'name' => 'Nombres',
			'lastname' => 'Apellidos',
			'fullname' => 'Nombres y apellidos',
			'gender' => 'Sexo',
			'image' => 'Imágen',
			'link' => 'Sitio web',
			'birthday' => 'Fecha de nacimiento',
			'email1' => 'Correo principal',
			'email2' => 'Correo secundario',
			'email_status' => 'Email Status',
			'intercom' => 'intercom',
			'timezone' => 'Timezone',
			'newsletter' => 'Newsletter',
			'occupation' => 'Profesión',
			'create' => 'Fecha de registro',
			'last_sign' => 'Última sesión',
			'last_ip' => 'Last Ip',
			'curr_sign' => 'Curr Sign',
			'curr_ip' => 'Curr Ip',
			'verifcode' => 'Verifcode',
			'nickname' => 'Nickname',
			'about' => '¿Quién eres tú?',
			'id_service' => 'Código remoto del cliente',
			'phone' => 'Teléfono',
		);
	}

	public function search()
	{
		$criteria=new CDbCriteria;
		$criteria->with = array('suscriptions');

		$criteria->compare('id_user',$this->id_user);
		$criteria->compare('name',$this->fullnameTbl,true);
		$criteria->compare('lastname',$this->lastname,true);
		$criteria->compare('email1',$this->email1,true);
		$criteria->compare('email2',$this->email2,true);
		$criteria->compare('id_service',$this->id_service,true);

		return new CActiveDataProvider($this, array(
		  'criteria' => $criteria
		));
	}

	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	public function getFullname()
	{
		return $this->name . ' ' . $this->lastname;
	}

	public function getFullPhoneNumber()
	{
		if(isset($this->region, $this->region->country))
		{
			$number = '(+' . $this->region->country->phone_code . ') ';
			
			if((!empty($this->region->area_code) || $this->region->area_code == 0) && $this->phoneType == 'Fijo' ) {
				$number .= $this->region->area_code . '-';
			}
			return $number . $this->phone;
		}else{
			return '';
		}

	}

	/**
	 * Función que simula el atributo de si es nuevo
	 * suscriptor, para ello cuenta la cantidad de pagos 
	 * que tiene
	 * 
	 * @return bool 	si es nuevo suscriptor
	 */
	public function getIsNewSubscriptor()
	{
		$connection=Yii::app()->db;
		$command = $connection->createCommand();

		$command->setSelect("COUNT(id_pay)");
		$command->setFrom(Pay::model()->tableName() . ' p');
		$command->setWhere('email=:email1 OR email=:email2');

		$qnty = $command->queryScalar(array(
			':email1'=>$this->email1, 
			':email2'=>$this->email2
		));

		return $qnty <= 1;
	}

	public function getPayedReferrals()
	{
		$refSentId = PayType::model()->findByAttributes(array('na_ptype' =>'REFERRAL_SENT' ))->id_ptype;
		$refReceivedId = PayType::model()->findByAttributes(array('na_ptype' =>'REFERRAL_RECEIVED' ))->id_ptype;

		return Pay::model()->count(
			'id_ptype=:sentId OR id_ptype=:receivedId', array(
				':sentId'=>$refSentId,
				':receivedId' =>$refReceivedId
			)
		);
	}

	public function addExtraMonth( $suscription)
	{
		if($this->payedReferrals >= self::MAX_SCHOLARSHIP_MONTHS ) {
			return;
		}

		$suscription->exp_date = date('Y-m-d H:i:s', strtotime( $suscription->exp_date . "+1 month"));
		$suscription->update();

		$pay = new Pay;
		$pay->id_ptype = PayType::model()->findByAttributes(array('na_ptype' =>'REFERRAL_RECEIVED' ))->id_ptype;
		$pay->id_suscription = $suscription->id_suscription;
		$pay->date = new CDbExpression('NOW()');
		$pay->email = $this->email1;
		$pay->plan = SuscriptionType::model()->findByAttributes(array('na_stype' =>'Mes gratuito por invitación' ))->id_stype;
		$pay->amount = 0;
		$pay->status = "Completed";
		$pay->code = $this->invitation_from;
		$pay->save();

		$s_status = new SuscriptionStatus;
		$s_status->id_status = 1;
		$s_status->date = new CDbExpression('NOW()');
		$s_status->id_suscription = $suscription->id_suscription;
		$s_status->save();

		$referredUser = NULL;  //Se declara aquí para optimizar la velocidad en los siguientes if
		if(!empty($this->invitation_from))
			$referredUser = self::model()->find('email1=:email', array(':email'=>$this->invitation_from));
		else
			$referredUser = self::model()->findByPk($this->id_refuser);

		if($referredUser->payedReferrals < self::MAX_SCHOLARSHIP_MONTHS )
		{
			$referredSuscription = Suscription::model()->findByAttributes(array('id_user'=>$referredUser->id_user));

			if($referredSuscription === NULL || (SuscriptionType::model()->findByPk($referredSuscription->id_stype)->recurrence == 0))
			{
				//Crear nueva suscripción al ser encontrado el usuario y validado el pago por Stripe
				$referredSuscription = new Suscription;
				$referredSuscription->id_user = $referredUser->id_user;
				$referredSuscription->id_stype = SuscriptionType::model()->findByAttributes(array('na_stype' =>'Mes gratuito por invitación' ))->id_stype;
				$referredSuscription->id_ptype = PayType::model()->findByAttributes(array('na_ptype' =>'REFERRAL_SENT' ))->id_ptype;
				$referredSuscription->date = new CDbExpression('NOW()');
				$referredSuscription->active = 1;
				$referredSuscription->status = 1;
				$referredSuscription->display_name = $referredUser->name." ".$referredUser->lastname;
				$referredSuscription->paid = true;
				$referredSuscription->id_service = NULL;
				$referredSuscription->exp_date = date('Y-m-d H:i:s', strtotime("+1 month"));
				$referredSuscription->save();
			} else {
				$referredSuscription->exp_date = date('Y-m-d H:i:s', strtotime( $suscription->exp_date . "+1 month"));
				$referredSuscription->update();
			}

			$pay = new Pay;
			$pay->id_ptype = PayType::model()->findByAttributes(array('na_ptype' =>'REFERRAL_SENT' ))->id_ptype;
			$pay->id_suscription = $referredSuscription->id_suscription;
			$pay->date = new CDbExpression('NOW()');
			$pay->email = $referredUser->email1;
			$pay->plan = SuscriptionType::model()->findByAttributes(array('na_stype' =>'Mes gratuito por invitación' ))->id_stype;
			$pay->amount = 0;
			$pay->status = "Completed";
			$pay->code = $this->invitation_from;
			$pay->save();

			$s_status = new SuscriptionStatus;
			$s_status->id_status = 1;
			$s_status->date = new CDbExpression('NOW()');
			$s_status->id_suscription = $referredSuscription->id_suscription;
			$s_status->save();

			Mail::sendAcceptedInvitation($referredUser, $this);
			Happens::suscriptorPorInvitacion($this->email1, $this->invitation_from);
		}

		$this->invitation_from = NULL;
	}

	public static function hasActiveSubscription($userId, $gateway = '', $active = 1)
	{
		$criteria = new CDbCriteria();
		$criteria->addCondition('t.active=:active');
		$criteria->addCondition('t.id_user=:id');
		$criteria->addCondition('st.recurrence=1');
		$criteria->join = 'INNER JOIN ' . SuscriptionType::model()->tableName() . ' st ON st.id_stype = t.id_stype';
		$criteria->params = array(':id'=>$userId, ':active'=> $active);
		$criteria->order = 't.`date` DESC';

		switch($gateway) {
			case 'paypal':
				$criteria->addCondition("t.id_service NOT REGEXP '^sub'");
				break;
			case 'stripe':
				$criteria->addCondition("t.id_service REGEXP '^sub'");
				break;
		}

		return Suscription::model()->exists($criteria);
	}

	public static function nextContact($userId)
	{
        $connection=Yii::app()->db;
        $command = $connection->createCommand();
        $command->setSelect("c.date_contact");
        $command->setFrom(Contact::model()->tableName(). ' c');
        $command->setWhere("c.id_user=". $userId ." AND DATE(c.date_contact) >= DATE(NOW())");
        $command->setOrder("c.modified");
        $listProximos = $command->queryRow();
        if(isset($listProximos['date_contact']))
			return $listProximos['date_contact'];
		else
			return "No Existe";
	}

	public static function lastContact($userId)
	{
		$connection=Yii::app()->db;
        $command = $connection->createCommand();
        $command->setSelect("c.date_contact, c.modified, id_state");
        $command->setFrom(Contact::model()->tableName(). ' c');
        $command->setWhere("c.id_user=".$userId." AND (c.id_state=2 OR c.id_state=3)");
        $command->setOrder("c.modified");
        $listPasados = $command->queryRow();

        if(isset($listPasados['modified']))
			return $listPasados['modified'];
		else
			return "No Existe";
	}

	public static function lastLogin($userId)
	{
		$usuario = User::model()->findByAttributes(array('email1'=>$userId));
		if($usuario)
		{
			$repuser=RepUser::model()->findByAttributes(array('email1'=>$userId));
			if(!$repuser)
				$repuser=RepUser::model()->findByAttributes(array('email1'=>$usuario->email2));
			if($repuser)
			{
				$connection=Yii::app()->db2;
		        $command=$connection->createCommand();
		        $command->setSelect("h.created_at");
		        $command->setFrom(RepHappen::model()->tableName() . ' h');
		        $command->join(RepEvent::model()->tableName() . ' e', "h.id_event=e.id_event");
		        $command->setWhere(@"(e.`na_event`='clase' OR e.`na_event`='login') AND h.id_user=".$repuser->id_user);
		        $command->setOrder("h.created_at DESC");
		        $sus=$command->queryRow();
				
				return Dates::FechaRelativa($sus['created_at']);
			}
			else
			{
				return '';
			}
		}
		else
		{
			return '';
		}
	}

	/**
	 * Obtiene la suscripción activa de un usuario
	 * @param  integer  $id      	id del usuario a buscarle suscripción
	 * @param  string    $gateway 	Permite especificar la pasarela utilizada
	 * @return mixed          			Devuelve la suscripción, en caso de no existir devuelve NULL
	 */
	public function activeSubscription($gateway = '')
	{
		$criteria = new CDbCriteria();
		$criteria->addCondition('t.active = 1');
		$criteria->addCondition('t.id_user=:id');
		$criteria->addCondition('st.recurrence=1');
		$criteria->join = 'INNER JOIN ' . SuscriptionType::model()->tableName() . ' st ON st.id_stype = t.id_stype';
		$criteria->order = 't.date DESC';
		$criteria->params = array(':id'=>$this->id_user);

		switch(strtolower($gateway)) {
			case 'paypal':
				$criteria->addCondition("t.id_service NOT REGEXP '^sub'");
				break;
			case 'stripe':
				$criteria->addCondition("t.id_service REGEXP '^sub'");
				break;
		}

		return Suscription::model()->find($criteria);
	}
}
