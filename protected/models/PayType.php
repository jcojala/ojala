<?php

/**
 * This is the model class for table "pay_type".
 *
 * The followings are the available columns in table 'pay_type':
 * @property integer $id_ptype
 * @property string $na_ptype
 * @property integer $active
 *
 * The followings are the available model relations:
 * @property PayInformation[] $payInformations
 * @property PayMode[] $payModes
 */
class PayType extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'pay_type';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('na_ptype', 'required'),
			array('active', 'numerical', 'integerOnly'=>true),
			array('na_ptype', 'length', 'max'=>45),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id_ptype, na_ptype, active', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'payInformations' => array(self::HAS_MANY, 'PayInformation', 'id_ptype'),
			'payModes' => array(self::HAS_MANY, 'PayMode', 'id_ptype'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_ptype' => 'Id Ptype',
			'na_ptype' => 'Na Ptype',
			'active' => 'Active',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id_ptype',$this->id_ptype);
		$criteria->compare('na_ptype',$this->na_ptype,true);
		$criteria->compare('active',$this->active);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return PayType the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	public static function getTableValue($value)
	{
		switch($value){
			case 'REFERRAL_SENT':
				return 'Invitación invitada';
			case 'REFERRAL_RECEIVED':
				return 'Invitación recibida';
			case 'CONEKTA':
				return 'Conekta';
			case 'STRIPE':
				return 'Stripe';
		}
		
		return $value;
	}
}
