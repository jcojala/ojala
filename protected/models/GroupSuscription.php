<?php

/**
 * This is the model class for table "group_suscription".
 *
 * The followings are the available columns in table 'group_suscription':
 * @property integer $id_group_susc
 * @property integer $id_group
 * @property integer $id_user
 * @property string $date
 * @property integer $active
 * @property integer $current_level
 *
 * The followings are the available model relations:
 * @property Group $idGroup
 * @property User $idUser
 */
class GroupSuscription extends CActiveRecord
{
	public function tableName()
	{
		return 'group_suscription';
	}

	public function rules()
	{
		return array(
			array('id_group, date, active, current_level', 'required'),
			array('id_group, id_user, active, current_level', 'numerical', 'integerOnly'=>true),
			array('id_group_susc, id_group, id_user, date, active, current_level', 'safe', 'on'=>'search'),
		);
	}

	public function relations()
	{
		return array(
			'group' => array(self::BELONGS_TO, 'Group', 'id_group'),
			'user' => array(self::BELONGS_TO, 'User', 'id_user'),
		);
	}

	public function attributeLabels()
	{
		return array(
			'id_group_susc' => 'Id Group Susc',
			'id_group' => 'Id Group',
			'id_user' => 'Id User',
			'date' => 'Date',
			'active' => 'Active',
			'current_level' => 'Current Level',
		);
	}

	public function search()
	{
		$criteria=new CDbCriteria;

		$criteria->compare('id_group_susc',$this->id_group_susc);
		$criteria->compare('id_group',$this->id_group);
		$criteria->compare('id_user',$this->id_user);
		$criteria->compare('date',$this->date,true);
		$criteria->compare('active',$this->active);
		$criteria->compare('current_level',$this->current_level);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
