<?php

/**
 * This is the model class for table "course_suscription".
 *
 * The followings are the available columns in table 'course_suscription':
 * @property integer $id_csuscription
 * @property integer $id_course
 * @property integer $id_user
 * @property integer $id_progress
 * @property integer $current_seg
 * @property integer $active
 *
 * The followings are the available model relations:
 * @property Certificate[] $certificates
 * @property Course $idCourse
 * @property Suscription $idSuscription
 * @property Progress $idProgress
 * @property Progress[] $progresses
 */
class CourseSuscription extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'course_suscription';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id_course, id_user', 'required'),
			array('id_course, id_user, id_progress, current_seg, active', 'numerical', 'integerOnly'=>true),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id_csuscription, id_course, id_user, id_progress, current_seg, active', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'certificates' => array(self::HAS_MANY, 'Certificate', 'id_csuscription'),
			'idCourse' => array(self::BELONGS_TO, 'Course', 'id_course'),
			'idSuscription' => array(self::BELONGS_TO, 'Suscription', 'id_user'),
			'idProgress' => array(self::BELONGS_TO, 'Progress', 'id_progress'),
			'progresses' => array(self::HAS_MANY, 'Progress', 'id_csuscription'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_csuscription' => 'Id Csuscription',
			'id_course' => 'Id Course',
			'id_user' => 'Id Suscription',
			'id_progress' => 'Id Progress',
			'current_seg' => 'Current Seg',
			'active' => 'Active',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id_csuscription',$this->id_csuscription);
		$criteria->compare('id_course',$this->id_course);
		$criteria->compare('id_user',$this->id_user);
		$criteria->compare('id_progress',$this->id_progress);
		$criteria->compare('current_seg',$this->current_seg);
		$criteria->compare('active',$this->active);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return CourseSuscription the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
