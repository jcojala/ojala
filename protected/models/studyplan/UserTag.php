<?php

/**
 * This is the model class for table "user_tag".
 *
 * The followings are the available columns in table 'user_tag':
 * @property integer $id_user
 * @property integer $id_tag
 * @property integer $id_correlation
 *
 * The followings are the available model relations:
 * @property User $idUser
 * @property Tag $idTag
 * @property CorrelationTag $idCorrelation
 */
class UserTag extends CActiveRecord
{
	public function tableName()
	{
		return 'user_tag';
	}

	public function rules()
	{
		return array(
			array('id_user, id_tag, id_correlation', 'required'),
			array('id_user, id_tag, id_correlation', 'numerical', 'integerOnly'=>true),
			array('id_user, id_tag, id_correlation', 'safe', 'on'=>'search'),
		);
	}

	public function relations()
	{
		return array(
			'user' => array(self::BELONGS_TO, 'User', 'id_user'),
			'tag' => array(self::BELONGS_TO, 'Tag', 'id_tag'),
			'correlation' => array(self::BELONGS_TO, 'CorrelationTag', 'id_correlation'),
		);
	}

	public function attributeLabels()
	{
		return array(
			'id_user' => 'Id User',
			'id_tag' => 'Id Tag',
			'id_correlation' => 'Id Correlation',
		);
	}

	public function search()
	{
		$criteria=new CDbCriteria;

		$criteria->compare('id_user',$this->id_user);
		$criteria->compare('id_tag',$this->id_tag);
		$criteria->compare('id_correlation',$this->id_correlation);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
