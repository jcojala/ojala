<?php

/**
 * This is the model class for table "correlation_tag".
 *
 * The followings are the available columns in table 'correlation_tag':
 * @property integer $id_correlation
 * @property double $value
 *
 * The followings are the available model relations:
 * @property CourseTag[] $courseTags
 * @property UserTag[] $userTags
 */
class CorrelationTag extends CActiveRecord
{
	public function tableName()
	{
		return 'correlation_tag';
	}

	public function rules()
	{
		return array(
			array('value', 'numerical'),
			array('id_correlation, name, value', 'safe', 'on'=>'search'),
		);
	}

	public function relations()
	{
		return array(
			'courseTags' => array(self::HAS_MANY, 'CourseTag', 'id_correlation'),
			'userTags' => array(self::HAS_MANY, 'UserTag', 'id_correlation'),
		);
	}

	public function attributeLabels()
	{
		return array(
			'id_correlation' => 'Código',
			'value' => 'Valor',
		);
	}

	public function search()
	{
		$criteria=new CDbCriteria;

		$criteria->compare('id_correlation',$this->id_correlation);
		$criteria->compare('value',$this->value);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
