<?php

/**
 * This is the model class for table "study_plan".
 *
 * The followings are the available columns in table 'study_plan':
 * @property integer $id_user
 * @property integer $hours
 * @property integer $unit
 * @property string $start_date
 *
 * The followings are the available model relations:
 * @property User $idUser
 * @property Course[] $courses
 */
class StudyPlan extends CActiveRecord
{
	const MAX_COURSES = 5;
	public $planCourses;

	public function tableName()
	{
		return 'study_plan';
	}

	public function rules()
	{
		return array(
			array('id_user, hours, unit, start_date', 'required'),
			array('id_user, hours, unit', 'numerical', 'integerOnly'=>true),
			array('id_user, hours, unit, start_date', 'safe', 'on'=>'search'),
		);
	}

	public function relations()
	{
		return array(
			'idUser' => array(self::BELONGS_TO, 'User', 'id_user'),
			'courses' => array(self::MANY_MANY, 'Course', 'study_plan_course(id_user, id_course)'),
		);
	}

	public function attributeLabels()
	{
		return array(
			'id_user' => 'Id User',
			'hours' => 'Hours',
			'unit' => 'Unit',
			'start_date' => 'Start Date',
		);
	}

	public function search()
	{
		$criteria=new CDbCriteria;

		$criteria->compare('id_user',$this->id_user);
		$criteria->compare('hours',$this->hours);
		$criteria->compare('unit',$this->unit);
		$criteria->compare('start_date',$this->start_date,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * Función para crear la población inicial 
	 * 
	 * @return Array	Arreglo compuesto de arreglos con listados de cursos compatibles con los tags del usuario
	 */
	public static function createPopulation($userTags)
	{
		$condition = sprintf("tag.tag IN (%s)", "'" . implode("','", $userTags) ."'");
		$allCourses = Course::model()->with('courseTags','courseTags.tag')->findAll($condition);

		$population = array();
		$groupIndex = -1;

		$databaseSize = count($allCourses);
		$iterations = $databaseSize - ($databaseSize%self::MAX_COURSES);

		for ($i=0; $i < $iterations ; $i++) { 
			if($i % self::MAX_COURSES == 0 || $i===0){
				$groupIndex++;
				$population[$groupIndex] = new self;
				$population[$groupIndex]->planCourses = array();
			}
						
			$population[$groupIndex]->planCourses[] = $allCourses[$i];
		}

		return $population;
	}
}
