<?php 
class AdvisorForm extends CFormModel
{
	public $dataSent = false;
	public $invalidPayment;
	public $country;
	public $region;
	public $phoneNumber;
	public $phoneType;

  public function rules()
	{
		return array(
			array('phoneNumber', 'required','message'=>'Debes escribir tu número de teléfono'),
			array('country', 'required','message'=>'Debes seleccionar tu país'),
			array('region', 'required','message'=>'Debes seleccionar tu región'),
			array('phoneType', 'required', 'message'=>'Debes seleccionar un tipo de número'),
			array('phoneNumber', 'length', 'max'=>20),
			array('country, region', 'length', 'max'=>100),
		);
	}

	/**
	 * Declares attribute labels.
	 */
	public function attributeLabels()
	{
		return array(
			'country'=>'País',
			'region'=>'Región',
			'phoneNumber'=>'Número telefónico',
			'phoneType'=>'Tipo de número',
		);
	}

	public function getType()
	{
		return array(
			'Celular'=>'Celular',
			'Fijo'=>'Fijo'
		);
	}
}