<!DOCTYPE html>
<html lang="es">
	<head>
		<meta content='text/html; charset=utf-8' http-equiv='Content-Type'>
		<meta content='width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1' name='viewport'>
		<meta content='203807993010072' property='fb:app_id'>
		
		<title><?php echo CHtml::encode(ucfirst($this->pageTitle)); ?> | Ojala</title>

		<meta content='es_LA' property='og:locale'>
		<meta content='Oja.la' property='og:site_name'>
		<meta content='school' property='og:type'>
		<meta content='Ojala Edu Inc' name='author'>
		<meta content='Oja.la es una biblioteca online de cursos especializados que te ayudaran a obtener nuevas habilidades y crecer profesionalmente en temas de tecnología.' name='description'>
		<link rel="apple-touch-icon" href="<?php echo Yii::app()->request->baseUrl; ?>/apple-touch-icon.png" />
		<link href="<?php echo Yii::app()->request->baseUrl; ?>/favicon.ico" rel="icon" type="image/x-icon" />

		<!-- APIs que usamos -->
		<link href='http://fast.wistia.com' rel='dns-prefetch'>
		<link href='http://ajax.googleapis.com' rel='dns-prefetch'>
		<link href='http://graph.facebook.com' rel='dns-prefetch'>
		<link href='http://embed.wistia.com' rel='dns-prefetch'>

		<?php 
			Yii::app()->clientScript->registerCssFile(Yii::app()->request->baseUrl . '/css/styles-publicos.css?v=2','screen');
			Yii::app()->clientScript->registerCssFile(Yii::app()->request->baseUrl . '/css/styles-landings.css?v=2','screen');

			if(!Yii::app()->user->isGuest){
		    	Yii::app()->clientScript->registerCssFile(Yii::app()->request->baseUrl . '/css/styles-privados.css','screen');
		  	}
		  	
			Yii::app()->clientScript->registerCoreScript('jquery');
			OjalaScripts::registerKissmetrics();
			OjalaScripts::registerZopim();
			OjalaScripts::registerSegumiento();
		?>
		<script>var baseUrl = '<?php echo Yii::app()->request->baseUrl; ?>';</script>
	</head>

	<body>
		<?php OjalaScripts::registerRemarketing(); ?>
		<?php echo $content; ?>
	</body>
</html>