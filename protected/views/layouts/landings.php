<?php $this->beginContent('//layouts/bodylandings'); ?>
	<div class="container-fluid">
		<div class="row">
			<?php echo $content; ?>
		</div>

		<footer>
			<div class="row">
				<div class="col-md-2 first">
					<img alt="Ojala EDU Inc" src="<?php echo Yii::app()->request->baseUrl; ?>/images/brand-footer.png">
					<small>© Ojala EDU Inc. 2014</small>
				</div>
				<div class="col-md-2 second">
					<h4>Nuestros cursos:</h4>
					<ul>
						<li><a href="<?php echo Yii::app()->request->baseUrl; ?>/cursos/ios">iPhone apps y iOS</a></li>
						<li><a href="<?php echo Yii::app()->request->baseUrl; ?>/cursos/android">Android apps</a></li>
						<li><a href="<?php echo Yii::app()->request->baseUrl; ?>/cursos/desarrollo-web">Desarrollo web</a></li>
						<li><a href="<?php echo Yii::app()->request->baseUrl; ?>/cursos/mercadeo">Mercadeo y ventas</a></li>
						<li><a href="<?php echo Yii::app()->request->baseUrl; ?>/cursos/social-media">Social Media</a></li>
					</ul>
				</div>
				<div class="col-md-3 third">
					<h4>Nuestros diplomados:</h4>
					<ul>
						<li><a href="<?php echo Yii::app()->request->baseUrl; ?>/diplomado/diplomado-de-desarrollo-de-apps-para-iphone-y-ipad-en-ios7?landing=diplomadoios">Desarrollo de apps para iPhone y iPad en iOS7</a></li>
						<li><a href="<?php echo Yii::app()->request->baseUrl; ?>/diplomado/diplomado-de-desarrollo-de-apps-para-android?landing=diplomadoAndroid">Desarrollo de apps para Android con Eclipse</a></li>
						<li><a href="<?php echo Yii::app()->request->baseUrl; ?>/diplomado/diplomado-de-desarrollo-de-paginas-web?landing=diplomadoweb">Desarrollo de páginas y productos web</a></li>
						<li><a href="<?php echo Yii::app()->request->baseUrl; ?>/diplomado/diplomado-on-line-de-gestion-de-redes-sociales?landing=diplomadosm">Community Management y gestión de redes</a></li>
					</ul>
				</div>
				<div class="col-md-2 fourth">
					<h4>Nosotros:</h4>
					<ul>
						<li><a href="<?php echo Yii::app()->urlManager->createUrl('site/ayuda'); ?>">Preguntas frecuentes</a></li>
						<li><a href="<?php echo Yii::app()->urlManager->createUrl('site/terminos'); ?>">Términos y condiciones</a></li>
						<li><a href="<?php echo Yii::app()->urlManager->createUrl('site/privacidad'); ?>">Políticas de privacidad</a></li>
						<li><a href="<?php echo Yii::app()->urlManager->createUrl('site/blog'); ?>">Nuestro blog</a></li>
					</ul>
				</div>
				<div class="col-md-3 fifth">
					<h4>Contáctanos</h4>
					<ul>
						<li><a href="https://twitter.com/Oja_la" target="_blank" class="twitter">Twitter</a></li>
						<li><a href="https://www.facebook.com/OjalaLatam" target="_blank" class="facebook">Facebook</a></li>
						<li><a href="mailto:m@oja.la?cc=fr%40oja.la&amp;subject=Hola%20tengo%2C%20una%20pregunta%21%21" class="mail">Mail</a></li>
						<!--<li><a href="#" target="_blank" class="youtube">Youtube</a></li>-->
						<li class="address">
							<p><strong>USA:</strong> 364 University Ave, Palo Alto, CA, 94301</p>
							<p><strong>Bogotá:</strong> Carrera 7 #69 - 17</p>
						</li>
					</ul>
				</div>
			</div>

		</footer>
		
	</div>
<?php $this->endContent(); ?>