<!DOCTYPE html>
<html lang="es">
	<head>
		<meta content='text/html; charset=utf-8' http-equiv='Content-Type'>
		<meta content='width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1' name='viewport'>
		<meta content='203807993010072' property='fb:app_id'>

		<title>Administración | Ojala</title>

		<meta content='es_LA' property='og:locale'>
		<meta content='Oja.la' property='og:site_name'>
		<meta content='school' property='og:type'>
		<meta content='' name='author'>
		<meta content='Oja.la es una biblioteca online de cursos especializados que te ayudar&aacute;n a obtener nuevas habilidades y crecer profesionalmente en temas de tecnolog&iacute;a.' name='description'>

		<?php
			Yii::app()->clientScript->registerCssFile(Yii::app()->request->baseUrl . '/css/styles-publicos.css','screen');
			
			if(!Yii::app()->user->isGuest){
				Yii::app()->clientScript->registerCssFile(Yii::app()->request->baseUrl . '/css/styles-privados.css?v=2','screen');
				Yii::app()->clientScript->registerCssFile(Yii::app()->request->baseUrl . '/css/styles-admin.css?v=2','screen');
			}
			Yii::app()->clientScript->registerCoreScript('jquery');
			Yii::app()->clientScript->registerCoreScript('jquery.ui');
			Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl . '/js/core.js', CClientScript::POS_HEAD);
		?>
		
		<script>var baseUrl = '<?php echo Yii::app()->request->baseUrl ?>';</script>
	</head>

	<body>
		<?php echo $content; ?>
		<footer>
			<div class="address">
				<p><strong>USA:</strong> 364 University Ave, Palo Alto, CA, 94301</p>
				<i>© Copyright <?php echo date('Y') ?> Oja.la Edu Inc.</i>
			</div>
		</footer>
	</body>
</html>