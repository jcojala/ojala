<?php $this->beginContent('//layouts/bodylandings'); ?>
<div class="menu-princ">
<?php $this->widget('MainMenu', array('show'=>'byRole')); ?>
</div>
	<div class="container-fluid">
		<div class="row">
			<?php echo $content; ?>
		</div>
		<footer>
			<div class="address">
				<p><strong>USA:</strong> 364 University Ave, Palo Alto, CA, 94301</p>
				<i>© Copyright <?php echo date('Y') ?> Oja.la Edu Inc.</i>
			</div>
		</footer>
	</div>
<?php $this->endContent(); ?>