<?php 
	$this->beginContent('//layouts/bodyadmin'); 
	$this->widget('MainMenu', array('show'=>'side'));
?>


<div class="container-fluid main-container">
	<div id="content">
		<?php echo $content; ?>
	</div>
</div>

<?php $this->endContent(); ?>