<div class='incmty' role='main'>

    <nav class='navbar navbar-default navbar-fixed-top' role='navigation'>
      <div class='navbar-header'>
        <p class="navbar-brand">Oja.la</p>
      </div>
    </nav>

	<div class='container'>
		<div class='col-md-7'>
			<img class="img" alt="INCmty - Innovate - Network - Create" src="<?php echo Yii::app()->request->baseUrl; ?>/images/brand-incmty.png" />
			<h1>
				<strong>INCmty y Oja.la te regalan un curso</strong>
				<strong class="pink">completamente gratis</strong>
			</h1>
			<h2>selecciona el curso que quieras</br> para que desarrolles tus habilidades</h2>
			<p>Más de 2200 clases en español, Más de 70 cursos en temas de tecnología</br> como desarrollo de iPhone apps, Desarrollo web, Mercadeo, etc.</p>
		</div>
		
		<div class='col-md-4'>
			<div class='form'>
				<p>Toma tu curso!</p>

				<?php $form=$this->beginWidget('CActiveForm', array(
					'id'=>'register-form',
					'action'=>Yii::app()->createUrl('site/registro'),
					'htmlOptions' => array(
						'class'=>"form-inline",
					),
				)); ?>
							
				<input name="redirect" type="hidden" value="cursos" />
				<div class='form-group'>
					<?php echo $form->textField($model,'name', array('size'=>'30', 'class'=>'form-control input-lg', 'placeholder'=>"Digita tu nombre:")); ?>
					<?php echo $form->error($model,'name'); ?>
				</div>
				<div class='form-group'>
					<?php echo $form->textField($model,'email1', array('size'=>'30', 'class'=>'form-control input-lg', 'placeholder'=>"Digita tu e-mail:")); ?>
					<?php echo $form->error($model,'email1'); ?>
				</div>
				<input class="btn btn-lg btn-block" name="commit" type="submit" value="Quiero mi curso ahora" />
				<?php $this->endWidget(); ?>

				<img class="arrow" src="<?php echo Yii::app()->request->baseUrl; ?>/images/arrow.png" />
			</div>
		</div>

		<div class="col-md-12">
			<p>INCmty - FESTIVAL PARA EL EMPRENDEDOR INNOVADOR DE IMPACTO - 15, 16 y 17 de Noviembre - Monterrey, México - www.incmty.com</p>
			<img alt="INCmty - Innovate - Network - Create" src="<?php echo Yii::app()->request->baseUrl; ?>/images/brand-instituto.gif" />
			<img alt="INCmty - Innovate - Network - Create" src="<?php echo Yii::app()->request->baseUrl; ?>/images/brand-techmonterrey.gif">
			<img alt="INCmty - Innovate - Network - Create" src="<?php echo Yii::app()->request->baseUrl; ?>/images/brand-altaventures.gif" />
			<small>© Copyright 2013 Oja.la Edu Inc.</small>
		</div>
	</div>

</div>