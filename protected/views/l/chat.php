<!-- begin olark code -->
<script data-cfasync="false" type='text/javascript'>/*<![CDATA[*/window.olark||(function(c){var f=window,d=document,l=f.location.protocol=="https:"?"https:":"http:",z=c.name,r="load";var nt=function(){
f[z]=function(){
(a.s=a.s||[]).push(arguments)};var a=f[z]._={
},q=c.methods.length;while(q--){(function(n){f[z][n]=function(){
f[z]("call",n,arguments)}})(c.methods[q])}a.l=c.loader;a.i=nt;a.p={
0:+new Date};a.P=function(u){
a.p[u]=new Date-a.p[0]};function s(){
a.P(r);f[z](r)}f.addEventListener?f.addEventListener(r,s,false):f.attachEvent("on"+r,s);var ld=function(){function p(hd){
hd="head";return["<",hd,"></",hd,"><",i,' onl' + 'oad="var d=',g,";d.getElementsByTagName('head')[0].",j,"(d.",h,"('script')).",k,"='",l,"//",a.l,"'",'"',"></",i,">"].join("")}var i="body",m=d[i];if(!m){
return setTimeout(ld,100)}a.P(1);var j="appendChild",h="createElement",k="src",n=d[h]("div"),v=n[j](d[h](z)),b=d[h]("iframe"),g="document",e="domain",o;n.style.display="none";m.insertBefore(n,m.firstChild).id=z;b.frameBorder="0";b.id=z+"-loader";if(/MSIE[ ]+6/.test(navigator.userAgent)){
b.src="javascript:false"}b.allowTransparency="true";v[j](b);try{
b.contentWindow[g].open()}catch(w){
c[e]=d[e];o="javascript:var d="+g+".open();d.domain='"+d.domain+"';";b[k]=o+"void(0);"}try{
var t=b.contentWindow[g];t.write(p());t.close()}catch(x){
b[k]=o+'d.write("'+p().replace(/"/g,String.fromCharCode(92)+'"')+'");d.close();'}a.P(2)};ld()};nt()})({
loader: "static.olark.com/jsclient/loader0.js",name:"olark",methods:["configure","extend","declare","identify"]});
/* custom configuration goes here (www.olark.com/documentation) */
olark.identify('5119-735-10-3270');/*]]>*/</script><noscript><a href="https://www.olark.com/site/5119-735-10-3270/contact" title="Contact us" target="_blank">Questions? Feedback?</a> powered by <a href="http://www.olark.com?welcome" title="Olark live chat software">Olark live chat software</a></noscript>
<!-- end olark code -->
<script type="text/javascript">
    olark('api.visitor.updateEmailAddress', {emailAddress: '<?php echo $_GET["email"]; ?>'});
    olark('api.visitor.updateFullName', {fullName: '<?php echo $_GET["nombre"]; ?>'});
		olark('api.visitor.currentPage', function(currentPage){
});
</script>

<style type="text/css">
	body{
		background: #2f5fa6 url("<?php echo Yii::app()->request->baseUrl; ?>/images/back-chat.jpg") 50% -100px no-repeat !important;
		position: relative;
	}
	.navbar{
		background-color: transparent;
		border: 0px;
		text-align: center;
		padding: 30px 0 0 0
	}
	.navbar .container{ padding-top: 0;}
	#habla_window_div{
		width: 60% !important;
		margin: 25% 20% 50px 20% !important;
		height: 75% !important;
		z-index: 0 !important;
		background-color: #ffffff;
		border-radius: 10px 10px 0 0;
	}
	#habla_expanded_div{
		background: none !important;
		background-color: #f9f9f9 !important;
		height: 500px !important;
	}
	#habla_both_div,
	#habla_topbar_div,
	#olrk_tab_closure_span{
		background: none !important;
	}
	#habla_middle_div{
		width: 80% !important;
		margin: 20px 10% 0 10% !important;
		height: 100% !important;
	}
	#habla_topbar_div{
		width: 100% !important;
		text-align: center !important;
	}
	#habla_topbar_div a{
		font-size: 16px;
		margin: 15px 0 0 0;
		width: 100%;
	}
	#habla_middle_div textarea{
		padding: 15px 3%;
		min-height: 50px !important;
		max-height: 50px !important;
		height: 50px !important;
		border: 1px solid #aaaaaa;
		font-size: 16px;
		width: 94%;
	}
	#habla_middle_div textarea:focus{
		border-color: rgba(82,168,236,.8);
		outline: 0;
		outline: thin dotted \9;
		-moz-box-shadow: 0 0 8px rgba(82,168,236,.6);
		box-shadow: 0 0 8px rgba(82,168,236,.6);
	}
	#habla_offline_message_div textarea{
		height: 25px !important;
		max-height: 25px !important;
		min-height: 25px !important;
	}
	#habla_offline_message_div .hbl_txt_wrapper #habla_offline_body_input{
		height: 100px !important;
		max-height: 100px !important;
		min-height: 100px !important;
	}
	#habla_offline_submit_input{
		display: inline-block;
		font-weight: 400;
		text-align: center;
		vertical-align: middle;
		cursor: pointer;
		background-image: none;
		border: 1px solid #4cae4c;
		white-space: nowrap;
		padding: 10px 50px;
		font-size: 16px;
		line-height: 1.42857143;
		border-radius: 4px;
		-webkit-user-select: none;
		-moz-user-select: none;
		-ms-user-select: none;
		user-select: none;
		color: #fff;
		background-color: #5cb85c;
	}
	#habla_sizebutton_a{
		display: none;
	}
	#habla_middle_wrapper_div{
		height: 60% !important;
	}
	.habla_conversation_p_item {
		border-top: 1px dotted #ccc;
		font-size: 15px;
	}
	.habla_conversation_person2{
		color: #ff6600;
	}
	#habla_conversation_div{
		height: 100% !important;
	}
	footer{
		position: absolute;
		bottom: 0;
		z-index: 100 !important;
		width: 100%;
		padding-right: 8%;
	}
	footer .address, footer i {
		width: 100% !important;
		display: block;
	}
	footer p{display: none;}
	footer i {text-align: center !important;}
</style>

    <div class="navbar navbar-inverse navbar-fixed-top" role="navigation">
      <div class="container">
        <div class="navbar-collapse collapse">
          <img src="<?php echo Yii::app()->request->baseUrl; ?>/images/simbolo.png">
        </div>
      </div>
    </div>
