<?php
	$paygateway = new StripeWrapper();
	$paygateway->registerScripts('#payment-form');
?>
<style type="text/css">
	body {
		background: #505355 url("../images/pulso/back.png") 50% 0 repeat-x !important;
		background-attachment:fixed !important;
	}
</style>
<div class="pulso acceso">

	<div class="header"><div class="brand">Pulso Institute</div></div>

	<div class='row'>
		<div class='container'>
			
			<div class="page-header">
				<h2>Ya estas muy cerca de iniciar el programa, selecciona tu forma de pago</h2>
			</div>

		</div>
	</div>

	<div class="row">
		<div class="container course-pay">

			<div class="col-md-8">					
				
				<div class="credit-card">
					<form accept-charset="UTF-8" action="<?php echo Yii::app()->urlManager->createUrl('site/confirmacion'); ?>" id="payment-form" method="get">
							
							<input type="hidden" id="amount" name="amount" value="300"/>
							<input type="hidden" id="id" name="id" value="1"/>

							<div style="margin:0;padding:0;display:inline">
								<input name="utf8" type="hidden" value="✓">
								<input name="authenticity_token" type="hidden" value="YrGtRmzm30BFgIkgbPspWnMKpAtV9mURmx/5UOQZbNc=">
							</div>

							<div class="payment-errors" style="color: red;"></div>
							<input id="user_new_email" name="user[new_email]" type="hidden">
							
							<div class="form-group card-number">
								<label>Número de tú tarjeta de crédito:</label>
								<input autocomplete="off" class="form-control stripe" maxlength="16" size="16" type="text" data-stripe="number">
							</div>
							
							<div class="form-group card-expiry-date clearfix" style = "width:100%">
								<label>Fecha de expiración:</label>
								<select class="card-expiry-month form-control stripe" data-stripe="exp-month">
								<option value="1">1 - Enero</option>
								<option value="2">2 - Febrero</option>
								<option value="3">3 - Marzo</option>
								<option value="4">4 - Abril</option>
								<option value="5">5 - Mayo</option>
								<option value="6">6 - Junio</option>
								<option value="7">7 - Julio</option>
								<option value="8">8 - Agosto</option>
								<option value="9">9 - Septiembre</option>
								<option value="10">10 - Octubre</option>
								<option value="11">11 - Noviembre</option>
								<option value="12">12 - Diciembre</option>
								</select>
								<select class="card-expiry-year form-control stripe" data-stripe="exp-year">
								<option value="2013">2013</option>
								<option value="2014">2014</option>
								<option value="2015">2015</option>
								<option value="2016">2016</option>
								<option value="2017">2017</option>
								<option value="2018">2018</option>
								<option value="2019">2019</option>
								<option value="2020">2020</option>
								<option value="2021">2021</option>
								<option value="2022">2022</option>
								<option value="2023">2023</option>
								<option value="2024">2024</option>
								<option value="2025">2025</option>
								<option value="2026">2026</option>
								<option value="2027">2027</option>
								<option value="2028">2028</option>
								</select>
							</div>
							<div class="form-group card-cvc" style = "float:left">
								<label>Cód de verificación:</label>
								<input autocomplete="off" class="form-control stripe" maxlength="4" size="4" type="text" data-stripe="cvc">
							</div>
							<input id="button" class="btn btn-warning btn-lg btn-block" name="commit" type="submit" value="Acceso inmediato, comprar ahora">
						</form>

					</div>
					<form accept-charset="UTF-8" action="<?php echo Yii::app()->urlManager->createUrl('site/buy'); ?>" id="payment-form-paypal" method="get">
						<div class="paypal">
							<input type="hidden" id="amount" name="amount" value="300">
							<input type="hidden" id="desc" name="desc" value="300"/>
							<input type="hidden" id="id" name="id" value="300"/>
	            <img alt="" border="0" height="1" src="<?php echo PaypalWrapper::getDomain() ?>es_XC/i/scr/pixel.gif" width="1">
							<input id="button" class="btn btn-lg btn-warning" type="submit" value="o usa tu cuenta de Paypal">
	          </div>
          </form>
				</div>
				<div class="col-md-4 course-details">
					<h4>Acceso inmediato al programa</h4>
					<div class="pay-total alert alert-success col-md-12">
						<p>
						Total a pagar:
						<span>Dólares américanos</span>
						<strong><sup>$</sup>300</strong>
						</p>
					</div>
					<p class="resume">
					Recibirás acceso ilimitado a la biblioteca de cursos; recuerda muy bien el correo y la contraseña con la que adquieres el acceso. También
					tendrás disponible materiales de descarga y al equipo de Oja.la
					</p>
				</div>
			</div>
		</div>
	</div>
</div>
</div>
<footer>
	<div class='row'>
		<div class='col-md-12 about'>
			<p><strong>USA:</strong>364 University Ave, Palo Alto, CA, 94301</p>
			<i>© Copyright 2013 Oja.la Edu Inc.</i>
		</div>
	</div>
</footer>

<?php
	Yii::app()->clientScript->registerScript('stripeChange', 
	    '$(function(){' .
			'$(\'.stripe\').change(function(){' .
				'if($("#button").attr("disabled") == "disabled"){' .
					'$("#button").removeAttr("disabled");' .
					'$(".payment-errors").empty();' .
				'}' .
			'});' .
	    '});', CClientScript::POS_END );
?>