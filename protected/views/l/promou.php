<?php $this->pageTitle = "Conviertete en un creador de apps para iPhone, iPad, y Android desde 0 (Diplomado completo)"; ?>
<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/jquery.countdown.js"></script>
<script type="text/javascript">
$(document).ready(function() {
	var endDate = "March 13, 2014 23:59:59";
	$('.countdown.simple').countdown({ until: +300 });
	$('.countdown.styled').countdown({
		date: endDate,
		render: function(data) {
			$(this.el).html("<div>" + this.leadingZeros(data.days, 2) + " <span>Días</span></div><div>" + this.leadingZeros(data.hours, 2) + " <span>Hrs</span></div><div>" + this.leadingZeros(data.min, 2) + " <span>Min</span></div><div>" + this.leadingZeros(data.sec, 2) + " <span>Seg</span></div>");
		}
	});
});
</script>

<div class="promou">

	<!-- Main header -->
	<nav class='navbar navbar-default' role='navigation'>
		<img alt="Oja.la" src="<?php echo Yii::app()->request->baseUrl; ?>/images/brand-full.png" />
	</nav>

	<div class="container">
		<div class="row">
			
			<div class="col-md-3 left">

				<div class="promo-video">
					<a href="#" data-toggle="modal" data-target=".bs-example-modal-lg"><img src="<?php echo Yii::app()->request->baseUrl; ?>/images/video-landing.png"></a>
						<div class="modal fade bs-example-modal-lg" id="video" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  						<div class="modal-dialog modal-lg">
    						<div class="modal-content">
    							<div class="modal-header" style="border:0px;height:40px;">
        						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
      						</div>
      					<iframe style="margin:0 20px 20px 20px;" src="//fast.wistia.net/embed/iframe/nuucgo75o7" allowtransparency="true" frameborder="0" scrolling="no" class="wistia_embed" name="wistia_embed" allowfullscreen mozallowfullscreen webkitallowfullscreen oallowfullscreen msallowfullscreen width="800" height="450"></iframe>
						    </div>
						  </div>
						</div>
				</div>

				<div class="mod contiene">
					<h5>QUÉ CONTIENE</h5>
					<ul>
						<li>Acceso Ilimitado a 541 clases</li>
						<li>+73 horas de contenido</li>
						<li>+53 archivos descargables</li>
					</ul>
				</div>

				<div class="instructor">
					<h5>IMPARTIDO POR</h5>
					<div class="user-data">
						<img alt="" src="https://pbs.twimg.com/profile_images/2426114030/profile_bigger.jpg">
						<div class="name">
							<p>Juan Carlos Robles</p>
							<span>iOS Developer</span>
						</div>
					</div>

					<div class="about">
						<p>Desarrollador, Instructor & "Geek" asumido,  es un experto en la enseñanza de máxima calidad en programación y desarrollo para dispositivos iOS, Cocoa Touch, Objective C, o eso es lo que dicen sus alumnos.</p>
					</div>
				</div>

			</div>


			<div class="col-md-6 center">
				<div class="mod basic-info">
					<h2>Conviertete en un creador de apps para iPhone, iPad, y Android,  desde 0 (Diplomado completo)</h2>
					<p>Diplomado de desarrollo desde nivel principiante hasta avanzado</p>
					<div class="stars">
						<span class="glyphicon glyphicon-star"></span>
						<span class="glyphicon glyphicon-star"></span>
						<span class="glyphicon glyphicon-star"></span>
						<span class="glyphicon glyphicon-star"></span>
						<span class="glyphicon glyphicon-star-empty"></span>
						<p><b>33</b> comentarios </p>
					</div>
					<div class="users">
						<ul>
							<li><img src="https://udemyimages-a.akamaihd.net/user/50x50/593731_b3d2_3.jpg"></li>
							<li><img src="https://udemyimages-a.akamaihd.net/user/50x50/206329_97c5_2.jpg"></li>
							<li><img src="https://udemyimages-a.akamaihd.net/user/50x50/73952_404a_2.jpg"></li>
							<li><img src="https://udemyimages-a.akamaihd.net/user/50x50/anonymous.jpg"></li>
						</ul>
						<p><b>56</b>estudiantes<br> apuntados</p>
					</div>
				</div>

				<div class="mod descripcion">
					<p>En este diplomado publicarás proyectos reales para teléfonos iPhone y Android y recibirás certificación al finalizar. El diplomado es online, con un entrenador que te armará el calendario de estudio, a tu propio ritmo; dependiendo del tiempo que más se te ajuste, con un primer nivel de 30 días para lo más básico donde iremos paso a paso para lanzar una aplicación simple, luego un programa acelerado de 3 meses (despendiendo de las horas que le puedas dedicar a la semana) ó hasta 6 meses si optas por el programa desacelerado para niveles intermedios y avanzados, incluye también, 6 meses extra para especializaciones (Modelo de Negocio, Mercadeo, como conseguir usuarios, inversiones, etc).</p>
				</div>

				<div class="mod questions">
					<ul>
						<li>
							<b><span class="glyphicon glyphicon-chevron-right"></span>¿Cuáles son los requisitos?</b>
							<ul>
								<li>Necesitas un computador Windows o Mac</li>
								<li>Ganas de aprender</li>
							</ul>
						</li>
						<li>
							<b><span class="glyphicon glyphicon-chevron-right"></span>¿Qué voy a aprender en este curso?</b>
							<ul>
								<li>¡Más de 541 clases y 73 horas de contenido!</li>
								<li>Dominar el lenguaje de programación Objective-C</li>
								<li>Comprenderás el lenguaje de programación Java</li>
								<li>Gestionar Base de datos con aplicaciones para iOS7</li>
								<li>Aplicar el conocimiento adquirido en el desarrollo de futuros proyectos</li>
								<li>Aprender a utilizar la herramienta Xcode y Eclipse</li>
								<li>Aprenderás a crear aplicaciones usando APIs externas ocmo la google maps</li>
								<li>Aprender a utilizar la herramienta Parse</li>
							</ul>
						</li>
						<li>
							<b><span class="glyphicon glyphicon-chevron-right"></span>¿A quién está dirigido?</b>
							<ul>
								<li>Programadores que deseen dar su primer paso en desarrollo para móviles</li>
								<li>Diseñadores que conocen HTML y desean desarrollar con lenguajes de programación</li>
							</ul>
						</li>
					</ul>
				</div>

				<div class="mod reviews">
					<div class="stars">
						<div class="calification">
							<span class="glyphicon glyphicon-star"></span>
							<span class="glyphicon glyphicon-star"></span>
							<span class="glyphicon glyphicon-star"></span>
							<span class="glyphicon glyphicon-star"></span>
							<span class="glyphicon glyphicon-star-empty"></span>
							<p>(12 reviews)</p>
							<small>Total: 4.5 de 5 estrellas</small>
						</div>
						<div class="bars">
							<div class="each">
								<p>5 Estrellas</p>
								<div class="total"><div class="votos" style="width: 60%;"></div></div>
								<span>7</span>
							</div>
							<div class="each">
								<p>4 Estrellas</p>
								<div class="total"><div class="votos" style="width: 10%;"></div></div>
								<span>2</span>
							</div>
							<div class="each">
								<p>3 Estrellas</p>
								<div class="total"><div class="votos" style="width: 5%;"></div></div>
								<span>1</span>
							</div>
							<div class="each">
								<p>2 Estrellas</p>
								<div class="total"><div class="votos" style="width: 5%;"></div></div>
								<span>1</span>
							</div>
							<div class="each">
								<p>1 Estrellas</p>
								<div class="total"><div class="votos" style="width: 5%;"></div></div>
								<span>1</span>
							</div>
						</div>
					</div>
					<div class="star-reviews">
						<div class="each">
							<p>
								<strong>“</strong>
								Recomiendo este diplomado para iniciarse en el  desarrollo de iOS y Android”
							</p>
							<small>Nelson Castro | 10 reviewes similares a esta</small>
						</div>
						<div class="each">
							<p>
								<strong>“</strong>
								Ya hice mi primer aplicación para iOS y ya está en el app store y además tengo a un cliente contento.”
							</p>
							<small>Carlos Guerrero | 8 reviewes similares a esta</small>
						</div>
					</div>
					<hr>
					<div class="each-review">
						<span class="glyphicon glyphicon-star"></span>
						<span class="glyphicon glyphicon-star"></span>
						<span class="glyphicon glyphicon-star"></span>
						<span class="glyphicon glyphicon-star"></span>
						<span class="glyphicon glyphicon-star-empty"></span>
						<p>Genial, me encanto<span>Diciembre 10 de 2013</span><small>Por: Jessika Alonzo Marquez</small></p>
						<div class="info">
							<p>Te enseñan los conceptos muy bien, y me encantaron los ejercicios, fueron muy importantes para aplicar lo que aprendí.</p>
						</div>
					</div>
					<div class="each-review">
						<span class="glyphicon glyphicon-star"></span>
						<span class="glyphicon glyphicon-star"></span>
						<span class="glyphicon glyphicon-star"></span>
						<span class="glyphicon glyphicon-star"></span>
						<span class="glyphicon glyphicon-star-empty"></span>
						<p>El mejor diplomado para aprender a crear apps<span>Diciembre 12 de 2013</span><small>Por: Pablo (España)</small></p>
						<div class="info">
							<p>He estado tomando  muchos tutoriales gratis en internet sobre programación de creacion de apps en los últimos años. Este diplomado es, de lejos, el mejor forma de aprender que he encontrado. Está claro, al punto, y sigue un buen objetivo. También obliga a las profundidades internas del código, SDK, y el funcionamiento interno de una aplicación para iOS sin que te vuelvas loco.</p>
						</div>
					</div>
					<div class="each-review">
						<span class="glyphicon glyphicon-star"></span>
						<span class="glyphicon glyphicon-star"></span>
						<span class="glyphicon glyphicon-star"></span>
						<span class="glyphicon glyphicon-star"></span>
						<span class="glyphicon glyphicon-star-empty"></span>
						<p>gran diplomado si empiezas de 0<span>Noviembre 06 de 2013</span><small>Por: Hernan (Coro, Venezuela)</small></p>
						<div class="info">
							<p>De los cerca de 3 cursos que he tomado y 4 libros que me he leido, este es la mejor forma de aprender a crear apps pero por lejos.  Aprendí nuevos principios importantes que otros materiales nunca cubrieron. El soporte 5 estrellas.</p>
						</div>
					</div>
					<div class="each-review">
						<span class="glyphicon glyphicon-star"></span>
						<span class="glyphicon glyphicon-star"></span>
						<span class="glyphicon glyphicon-star"></span>
						<span class="glyphicon glyphicon-star"></span>
						<span class="glyphicon glyphicon-star-empty"></span>
						<p>Más más más<span>Enero 20 de 2014</span><small>Por: Natali Mendoza</small></p>
						<div class="info">
							<p>No puedo esperar a que termine esta primera etapa del diplomado para poder  pasar a el más avanzada porque siento que el proceso va muy acelerado pero aprendiendo mucho.</p>
						</div>
					</div>
					<div class="each-review">
						<span class="glyphicon glyphicon-star"></span>
						<span class="glyphicon glyphicon-star"></span>
						<span class="glyphicon glyphicon-star"></span>
						<span class="glyphicon glyphicon-star"></span>
						<span class="glyphicon glyphicon-star-empty"></span>
						<p>¿Perdido creando un app para iphone y ipad? este es el diplomado<span>Enero 20 de 2014</span><small>Por: Carlos García</small></p>
						
						<div class="info">
							<p><strong>Antes de comprar</strong></p>
							<p>Aprender a crear apps,  para iphone o android. pero en serio. Hazlo . Saber o tener un sitio web ya no es suficiente. Si no sabes nada sobre como crear un app (mas que usarlas) , comienza con este diplomado.</p>
							<p><strong>Información General</strong></p>
							<p>Traté de usar varios materiales educativos para crear apps en el internet-  pagos y gratuitos - y luché con todas ellas. O bien esperaba mucho de ellos, o fueron simplemente mal estructurados para el aprendizaje “desde 0”. Solo mirando codigo sin saber lo que realmente estaba haciendo no es una buena forma de aprender.</p>
							<p>¿Por que me gusto el diplomado ? la primera parte fue extremadamente básica y vi como cada cosa que agregaba se veía reflejada en mi proyecto final, la primera parte del diplomado no es tanto lo tecnico sino en cómo familiarizarme y hacer cosas lo más rápido posible, se ve que esta hecho no solo por programadores si no personas que entienden el enfoque de negocio (se nota). Mientras pasaba las clases iba profundizando más y entrando mas en los aspectos tecnicos y si tenia alguna duda,  preguntaba y recibía una respuesta. La estructura de este diplomado me hizo el aprendizaje fácil, agradable, POSIBLE.</p>
							<p>No sólo estás mirando  código, sino que describen lo que están haciendo, y por qué , y luego te muestran cómo se construye y llenar los espacios y los errores con los que te puedes ver.</p>
							<p>También establecieron un linea de comunicacion por correo electronico en caso de tener algun problema y la respuesta fue rápida. Tuve un acompañamiento, que en lo personal fue un recurso muy útil.</p>
							<p>Allí, puedes hablar con personas que están haciendo el mismo diplomado que tu! puedes comparar las respuestas etc.</p>
							<p><strong>Necesitas constancia</strong></p>
							<p>Para que tengas una idea de los tiempos, me tomó cerca de 25 días completar la primera parte (3 dias a la semana 2 horas por sección) y luego lo continué por 3 meses y medio con el intermedio avanzado, ahora estoy haciendo una especialización para entender la parte de negocios (está incluido en el diplomado, tienes 12 meses para tomar cualquier especialización sin costo extra)  . No estoy seguro si el tiempo es el típico, o lento, o rápido. Pero estaba motivado y deseaba regresar a casa y retormarlo creo que me ayudo mucho el hecho de que ya tenia como hábito cada vez que regresaba del trabajo sin pensarlo me sentaba a  mis dos horas de clase. Algo extra fue configurar mi calendario para que me mandara un email a la misma hora para recordarme.</p>
							<p><strong>Resultados</strong></p>
							<p>Cuando empecé este diplomado tenía básicamente 0 experiencia en programación. Sabía algo de HTML, para armar páginas web, pero muy muy básico , sin embargo , nada de lo que sabía en html me ayudo para crear apps.</p>
							<p>Pero hacia el final, me sentí como un verdadero creador de apps (aunque novato) . Me siento de esa manera debido a dos cosas:</p>
							<p>[1] - Después de hacer este diplomado , puedo descargar casi cualquier aplicación de la App Store y explicar cómo se podría construir . Y luego construir una igual.</p>
							<p>[2] - Un buen amigo mío ha sido un desarrollador de iOS por 2 años, y después este diplomado, él y yo podemos tener conversaciones - conversaciones reales - donde discutimos las mejores prácticas y así sucesivamente. No se equivoquen , para él escribir código para apps es como escribir en español, mientras que yo todavía tengo que pensar como inventarme algunas cosas. El ya a pasado por TONELADAS de verdadera creación de Apps, mientras que a mi todavía me toca continuar practicando, ¡pero es divertido!</p>
							<p><strong>Conclusión</strong></p>
							<p>¿Tienen las energías para desafíos?  ¡Piérdete en ellos y logra tu objetivo!. Este diplomado puede ser el comienzo de una gran aventura.</p>
							<p>Muy recomendable . 5 estrellas . Si siempre has querido hacer realidad las cosas que se te ocurren , este diplomado podría ser un gran cambio en tu vida.</p>
						</div>
					</div>
					<div class="each-review">
						<span class="glyphicon glyphicon-star"></span>
						<span class="glyphicon glyphicon-star"></span>
						<span class="glyphicon glyphicon-star"></span>
						<span class="glyphicon glyphicon-star"></span>
						<span class="glyphicon glyphicon-star"></span>
						<p>Funciona<span>Febrero 02 de 2014</span><small>Por: Jacobo Arenas</small></p>
						<div class="info">
							<p>Soy un programador oxidado que decidió ensuciarse las manos algo nuevo. Yo había intentado con cursos en la universidad previamente, pero los encontraba muy deficientes. En la universidad no me gustaban los ejemplos y las muestras eran inútiles nada practicas, la forma de presentar los conceptos no eran intuitivo se sentía complicado. Estos cursos presenciales asumían que sabía cosas y me perdían. Unos meses más tarde , decidí probar de nuevo pero esta vez quería un diplomado en línea, vi este curso y los comentarios y decidí darle una oportunidad. Inmediatamente me gustó como empezó y me encontré mucho más comprometido. El ritmo es bueno, los ejemplos son interesantes y que hacen un gran trabajo de la presentación de los conceptos clave. Realmente aprecio el hecho de que en un corto período de tiempo, no sólo estoy aprendiendo más de cerca de la fundación de apps , sino que también estoy explorando el uso de características clave , como la ubicación y mapas. A pesar de que solía programar, yo considero que este diplomado lo  puede empezar hasta una persona de negocios. Realmente siento que este diplomado fue el puente entre lo conocido y algo nuevo y real.</p>
						</div>
					</div>
					<div class="each-review">
						<span class="glyphicon glyphicon-star"></span>
						<span class="glyphicon glyphicon-star"></span>
						<span class="glyphicon glyphicon-star"></span>
						<span class="glyphicon glyphicon-star"></span>
						<span class="glyphicon glyphicon-star"></span>
						<p>Excelente Diplomado<span>Febrero 08 de 2014</span><small>Por: Jonathan Velazquez</small></p>
						<div class="info">
							<p>Realmente excelente manera de aprender a programar Apps .El  Diplomado tiene éxito en la enseñanza de los conceptos detrás de la construcción de Apps modificando constantemente aplicaciones y  muestra lo que sucede cuando haces cada cambio (incluso lo que sucede cuando haces algo mal).</p>
							<p>Lo principal me gustaría añadir a las otras críticas positivas es que necesitas un iPad y un Mac, y debes utilizarlos juntos . Cómo configuro mi iPad sentado junto a mi Mac , y veo el diplomado en en el ipad , escribo el código en modo de pantalla completa en Xcode en el MAC. Eso fácilita la puesta a punto. La  version de este diplomado en el iPad se ve hermoso y fácil de usar con el soporte necesario , y puedo crear una aplicación en Xcode y ejecutarla de inmediato en mi  teléfono ( con la consola mostrando el Xcode ) , y luego cambiar fácilmente de nuevo al diplomado  para seguir viendo mi clase cuando termino de probar mi aplicación . Es mucho más cómodo que hacer diplomados presenciales.</p>
							<p>Mi única crítica es que por alguna razón no  usan git con sus proyectos de Xcode , cuando en realidad es muy fácil de usar.</p>
						</div>
					</div>
					<div class="each-review">
						<span class="glyphicon glyphicon-star"></span>
						<span class="glyphicon glyphicon-star"></span>
						<span class="glyphicon glyphicon-star"></span>
						<span class="glyphicon glyphicon-star"></span>
						<span class="glyphicon glyphicon-star"></span>
						<p>Mucho más claro que cualquier otra opción en internet<span>Febrero 12 de 2014</span><small>Por: Esteban</small></p>
						<div class="info">
							<p>He encontrado que este diplomado es  más fácil  de entender que muchas opciones online que he tomado antes. Puede ser debido al hecho de que utilizan la última versión de creacion de apps  de Apple que es mas simple. Pero en general, creo que realmente se enfocaron y trataron de explicar las cosas de la manera más clara posible y al menos a través de la primera etapa del diplomado, todo ha tenido un montón de sentido. Es difícil para la gente no muy familiarizados con la programación (como yo)  captar algunos de los conceptos , pero me pareció que la las clases van lentamente y trabajar con ejemplos es indispensable para la construcción de una base sólida. Yo recomiendo este diplomado para comenzar a crear Apps.</p>
						</div>
					</div>
					<div class="each-review">
						<span class="glyphicon glyphicon-star"></span>
						<span class="glyphicon glyphicon-star"></span>
						<span class="glyphicon glyphicon-star"></span>
						<span class="glyphicon glyphicon-star"></span>
						<span class="glyphicon glyphicon-star-empty"></span>
						<p>El mejor diplomado para crear apps , pero ...<span>Enero 13 de 2014</span><small>Por: José Carlos Guzman</small></p>
						<div class="info">
							<p>Me la  pase la mayor parte de 2013 tratando de conseguir una forma práctica de aprender a crear apps. Trate varios tutoriales, cursos y más de 15 libros, y casi todos fueron un desastre y por supuesto dinero tirado. Este diplomado es ,hasta ahora,la mejor forma que  que he encontrado para aprender a crear apps , pero eso no significa que si te inscribes en este diplomado te vas a convertir en programador de apps automáticamente, es este diplomado con la práctica constante lo que hizo la diferencia. He estado en el campo de desarrollo de software para más de 25 años , pero me tomó tres intentos y con el apoyo del seguimiento que tenía en el diplomado hacer todo lo que me presentaban en el diplomado . Los dos primeros etapas comenzaron bien, pero en la parte más avanzada me empeze a perder un poco ya que se movió un poco rápido con las explicaciones (aquí es donde el acompañamiento del diplomado  fue fundamental. Antes del acompañamiento me llevaba horas pasar las clases. Pero con el soporte por correo  finalmente fui capaz de abrir camino a través de todo el diplomado. Nótese que no estoy hablando ver el diplomado como si fuera una película tienes que practicar constantemente y tener una rutina diaria hacer un calendario casi inamovible bien sea una hora específica o después de alguna actividad que siempre hagas como después de la cena. Así fue como logre familiarizarse con lo que los temas que me ayudaron a crear las apps. Este diplomado con la práctica, búsquedas por google y soporte son la mezcla perfecta que me ayudaron a lograr hacer mi idea realidad..</p>
							<p>Yo se que suena como mucho pero no deje que mi opinión te desanime a hacer el diplomado finalmente es algo que tienes que probar por ti mismo. Después de haber pasado varios cursos libros y consultores para hacer un app ( y creanme fueron muchos ) ,este diplomado es el mejor que logre encontrar . Sólo ten en cuenta de que solo conseguir algo que funcione como este diplomado ya toma bastante trabajo osea que deberías estar en buen camino así que no te desanimes.</p>
						</div>
					</div>
					<div class="each-review" style="border: 0px;">
						<span class="glyphicon glyphicon-star"></span>
						<span class="glyphicon glyphicon-star"></span>
						<span class="glyphicon glyphicon-star"></span>
						<span class="glyphicon glyphicon-star"></span>
						<span class="glyphicon glyphicon-star-empty"></span>
						<p>Este tiene que ser una de las mejores formas de aprender a crear apps que he tomado<span>Diciembre 19 de 2013</span><small>Por: Victor (Madrid, España)</small></p>
						<div class="info">
							<p>Este diplomado  es brillante  por dos cosas: Es muy práctico, y es a fondo. Hay un montón de ejemplos, y el por qué y cómo de se explican con los suficiente detalles - lo suficiente para que puedas controlar todo fácilmente, pero no aburrirte con más de las mismas repeticiones.</p>
							<p>Para mí como novato en creaciones de apps este diplomado es simplemente perfecto. Muy recomendado.</p>
						</div>
					</div>
				</div>
			</div>


			<div class="col-md-3 right">

				<div class="mod">
					<div class="descuento">
						<span>Descuento</span>
						<div class="precio">
							<span>PRECIO:</span>
							<span class="value">$25 / mes</span>
						</div>
						<ul>
							<li class="one"><b>ORIGINAL</b><i>$99</i></li>
							<li class="two"><b>DESCUENTO</b><i>- 75%</i></li>
							<li class="three"><b>AHORRAS</b><i>$74</i></li>
						</ul>
						<div class="discount">
							<h4>Este descuento expira en:</h4>
							<div class="countdown styled"></div>
							<a class="btn btn-success btn-shadowed btn-large ud-popup " data-requirelogin="true" data-signuppopupidentifier="button-enroll-b" data-padding="0" href="<?php echo Yii::app()->request->baseUrl; ?>/pagoSuscripcion?plan=25-st">
								Toma este diplomado </a>
							</div>
						</div>
						<div class="info">
							<ul>
								<li><i class="glyphicon glyphicon-usd"></i>30 días de garantía de devolución</li>
								<li><i class="glyphicon glyphicon-time"></i>¡Acceso ilimitado!</li>
								<li><i class="glyphicon glyphicon-phone"></i>Accesible desde iPhone, iPad y Android</li>
								<li><i class="glyphicon glyphicon-file"></i>Certificado de cumplimiento</li>
							</ul>
						</div>
					</div>

					<div class="comments">
						<h5>Comentarios</h5>
						<div class="each-review">
							<span class="glyphicon glyphicon-star"></span>
							<span class="glyphicon glyphicon-star"></span>
							<span class="glyphicon glyphicon-star"></span>
							<span class="glyphicon glyphicon-star"></span>
							<span class="glyphicon glyphicon-star"></span>
							<p>Excelente Diplomado<span>Febrero 17 de 2014</span><small>Por: Jonathan Velazquez</small></p>
							<div class="info">
								<p>Realmente excelente manera de aprender a programar Apps .El  Diplomado tiene éxito en la enseñanza de los conceptos detrás de la construcción de ... <a href="#" data-toggle="modal" data-target="#first">Leer más</a></p>
							</div>
							<div class="modal fade" id="first" tabindex="-1" role="dialog" aria-labelledby="first" aria-hidden="true">
								<div class="modal-dialog">
									<div class="modal-content">
										<div class="modal-body">
											<p class="fecha">Febrero 17 de 2014</p>
											<h3>Excelente Diplomado</h3>
											<p class="by">Por: Jonathan Velazquez</p>
											<p class="comment">Realmente excelente manera de aprender a programar Apps .El  Diplomado tiene éxito en la enseñanza de los conceptos detrás de la construcción de Apps modificando constantemente aplicaciones y  muestra lo que sucede cuando haces cada cambio (incluso lo que sucede cuando haces algo mal).</p>
											<p class="comment">Lo principal me gustaría añadir a las otras críticas positivas es que necesitas un iPad y un Mac, y debes utilizarlos juntos . Cómo configuro mi iPad sentado junto a mi Mac , y veo el diplomado en en el ipad , escribo el código en modo de pantalla completa en Xcode en el MAC. Eso facilita la puesta a punto. La  versión de este diplomado en el iPad se ve hermoso y fácil de usar con el soporte necesario , y puedo crear una aplicación en Xcode y ejecutarla de inmediato en mi  teléfono ( con la consola mostrando el Xcode ) , y luego cambiar fácilmente de nuevo al diplomado  para seguir viendo mi clase cuando termino de probar mi aplicación . Es mucho más cómodo que hacer diplomados presenciales.</p>
											<p class="comment">Mi única crítica es que por alguna razón no  usan git con sus proyectos de Xcode , cuando en realidad es muy fácil de usar.</p>
										</div>
										<div class="modal-footer">
											<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar ventana</button>
										</div>
									</div>
								</div>
							</div>
						</div>
<div class="each-review">
							<span class="glyphicon glyphicon-star"></span>
							<span class="glyphicon glyphicon-star"></span>
							<span class="glyphicon glyphicon-star"></span>
							<span class="glyphicon glyphicon-star"></span>
							<span class="glyphicon glyphicon-star"></span>
							<p>Funciona<span>Febrero 02 de 2014</span><small>Por: Jacobo Arenas</small></p>
							<div class="info">
								<p>Soy un programador oxidado que decidió ensuciarse las manos algo nuevo. Yo había intentado con cursos en la universidad previamente, pero los encontraba ... <a href="#" data-toggle="modal" data-target="#second">Leer más</a></p>
							</div>
							<div class="modal fade" id="second" tabindex="-1" role="dialog" aria-labelledby="second" aria-hidden="true">
								<div class="modal-dialog">
									<div class="modal-content">
										<div class="modal-body">
											<p class="fecha">Febrero 02 de 2014</p>
											<h3>Funciona</h3>
											<p class="by">Por: Jacobo Arenas</p>
											<p class="comment">Soy un programador oxidado que decidió ensuciarse las manos algo nuevo. Yo había intentado con cursos en la universidad previamente, pero los encontraba muy deficientes. En la universidad no me gustaban los ejemplos y las muestras eran inútiles nada practicas, la forma de presentar los conceptos no eran intuitivo se sentía complicado. Estos cursos presenciales asumían que sabía cosas y me perdían. Unos meses más tarde , decidí probar de nuevo pero esta vez quería un diplomado en línea, vi este curso y los comentarios y decidí darle una oportunidad. Inmediatamente me gustó como empezó y me encontré mucho más comprometido. El ritmo es bueno, los ejemplos son interesantes y que hacen un gran trabajo de la presentación de los conceptos clave. Realmente aprecio el hecho de que en un corto período de tiempo, no sólo estoy aprendiendo más de cerca de la fundación de apps , sino que también estoy explorando el uso de características clave , como la ubicación y mapas. A pesar de que solía programar, yo considero que este diplomado no lo  puede empezar hasta una persona de negocios. Realmente siento que este diplomado fue el puente entre lo nuevo y algo lo real.</p>
										</div>
										<div class="modal-footer">
											<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar ventana</button>
										</div>
									</div>
								</div>
							</div>
						</div>
<div class="each-review">
							<span class="glyphicon glyphicon-star"></span>
							<span class="glyphicon glyphicon-star"></span>
							<span class="glyphicon glyphicon-star"></span>
							<span class="glyphicon glyphicon-star"></span>
							<span class="glyphicon glyphicon-star"></span>
							<p>Recomendado para personas que estan empezando ...<span>Febrero 27 de 2014</span><small>Por: Juan Zapata</small></p>
							<div class="info">
								<p>Poco a poco voy encaminado en este diplomado. Hasta el momento, el contenido ha sido fácil de... <a href="#" data-toggle="modal" data-target="#third">Leer más</a></p>
							</div>
							<div class="modal fade" id="third" tabindex="-1" role="dialog" aria-labelledby="third" aria-hidden="true">
								<div class="modal-dialog">
									<div class="modal-content">
								
										<div class="modal-body">
											<p class="fecha">Febrero 27 de 2014</p>
											<h3>Recomendado para personas que estan empezando ...</h3>
											<p class="by">Por: Juan Zapata</p>
											<p class="comment">Poco a poco voy encaminado en este diplomado. Hasta el momento, el contenido ha sido fácil de seguir y para alguien como yo, que está empezando, es la diferencia entre continuarlo y dejarlo. Creo que es un gran forma de empezar</p>
										</div>
										<div class="modal-footer">
											<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar ventana</button>
										</div>
									</div>
								</div>
							</div>
						</div>
<div class="each-review">
							<span class="glyphicon glyphicon-star"></span>
							<span class="glyphicon glyphicon-star"></span>
							<span class="glyphicon glyphicon-star"></span>
							<span class="glyphicon glyphicon-star-empty"></span>
							<span class="glyphicon glyphicon-star-empty"></span>
							<p>Conclusión<span>Enero 5 de 2014</span><small>Por: Carlos</small></p>
							<div class="info">
								<p>¿Tienes  energías para desafíos?  ¡Piérdete en ellos y logra tu objetivo!. Este diplomado puede ser el comienzo de una gran aventura.</p>
							</div>
						</div>
<div class="each-review">
							<span class="glyphicon glyphicon-star"></span>
							<span class="glyphicon glyphicon-star"></span>
							<span class="glyphicon glyphicon-star"></span>
							<span class="glyphicon glyphicon-star"></span>
							<span class="glyphicon glyphicon-star"></span>
							<p>Muy recomendable<span>Febrero 13 de 2014</span><small>Por: Lourdes (California, United States)</small></p>
							<div class="info">
								<p>Muy recomendable . 5 estrellas . Si siempre has querido hacer realidad las cosas que se te ocurren , este diplomado podría ser un gran cambio en tu vida.</p>
							</div>
						</div>
<div class="each-review">
							<span class="glyphicon glyphicon-star"></span>
							<span class="glyphicon glyphicon-star"></span>
							<span class="glyphicon glyphicon-star"></span>
							<span class="glyphicon glyphicon-star"></span>
							<span class="glyphicon glyphicon-star"></span>
							<p>Mucho más claro que cualquier otra opción en internet<span>Enero 12 de 2014</span><small>Por: Esteban Medina</small></p>
							<div class="info">
								<p>He encontrado que este diplomado es  más fácil de entender que muchas opciones online que he tomado antes... <a href="#" data-toggle="modal" data-target="#five">Leer más</a></p>
							</div>
							<div class="modal fade" id="five" tabindex="-1" role="dialog" aria-labelledby="five" aria-hidden="true">
								<div class="modal-dialog">
									<div class="modal-content">
								
										<div class="modal-body">
											<p class="fecha">Enero 12 de 2014</p>
											<h3>Mucho más claro que cualquier otra opción en internet</h3>
											<p class="by">Por: Esteban Medina</p>
											<p class="comment">He encontrado que este diplomado es  más fácil  de entender que muchas opciones online que he tomado antes. Puede ser debido al hecho de que utilizan la última versión de creacion de apps  de Apple que es mas simple. Pero en general, creo que realmente se enfocaron y trataron de explicar las cosas de la manera más clara posible y al menos a través de la primera etapa del diplomado, todo ha tenido un montón de sentido. Es difícil para la gente no muy familiarizados con la programación (como yo)  captar algunos de los conceptos , pero me pareció que la las clases van lentamente y trabajar con ejemplos es indispensable para la construcción de una base sólida. Yo recomiendo este diplomado para comenzar a crear Apps</p>
										</div>
										<div class="modal-footer">
											<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar ventana</button>
										</div>
									</div>
								</div>
							</div>
						</div>
<div class="each-review">
							<span class="glyphicon glyphicon-star"></span>
							<span class="glyphicon glyphicon-star"></span>
							<span class="glyphicon glyphicon-star"></span>
							<span class="glyphicon glyphicon-star"></span>
							<span class="glyphicon glyphicon-star-empty"></span>
							<p>Recomendado para personas que estan empezando<span>Febrero 27 de 2014</span><small>Por: Juan Carlos Posada</small></p>
							<div class="info">
								<p>Poco a poco voy encaminado en este diplomado. Hasta el momento, el contenido ha sido... <a href="#" data-toggle="modal" data-target="#seven">Leer más</a></p>
							</div>
							<div class="modal fade" id="seven" tabindex="-1" role="dialog" aria-labelledby="seven" aria-hidden="true">
								<div class="modal-dialog">
									<div class="modal-content">
								
										<div class="modal-body">
											<p class="fecha">Febrero 27 de 2014</p>
											<h3>Recomendado para personas que estan empezando</h3>
											<p class="by">Por: Juan Carlos Posada</p>
											<p class="comment">Poco a poco voy encaminado en este diplomado. Hasta el momento, el contenido ha sido fácil de seguir y para alguien como yo, que está empezando, es la diferencia entre continuarlo y dejarlo. Creo que es un gran forma de empezar.</p>
										</div>
										<div class="modal-footer">
											<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar ventana</button>
										</div>
									</div>
								</div>
							</div>
						</div>
<div class="each-review">
							<span class="glyphicon glyphicon-star"></span>
							<span class="glyphicon glyphicon-star"></span>
							<span class="glyphicon glyphicon-star"></span>
							<span class="glyphicon glyphicon-star"></span>
							<span class="glyphicon glyphicon-star-empty"></span>
							<p>La mejor guía de programación iOS que he probado<span>Septiembre 28 de 2013</span><small>Por: Fernando Martinez (Puerto Rico)</small></p>
							<div class="info">
								<p>Estoy en la parte intermedia del diplomado y estoy emocionado de que esta segunda parte es tan... <a href="#" data-toggle="modal" data-target="#eight">Leer más</a></p>
							</div>
							<div class="modal fade" id="eight" tabindex="-1" role="dialog" aria-labelledby="eight" aria-hidden="true">
								<div class="modal-dialog">
									<div class="modal-content">
								
										<div class="modal-body">
											<p class="fecha">Septiembre 28 de 2013</p>
											<h3>La mejor guía de programación iOS que he probado</h3>
											<p class="by">Por: Fernando Martinez (Puerto Rico)</p>
											<p class="comment">Estoy en la parte intermedia del diplomado y estoy emocionado de que esta segunda parte es tan ilustrativa como la primera. Yo había tratado en otras fuentes, pero quedé frustrado por la cantidad de teoria sin practica. Los que trate antes del diplomado se enfocan en detalles aburridos sobre algunas cosas y se saltaban cosas que ellos pensaban que eran evidentes, pero que yo no sabia. Este diplomado es la mezcla entre la teoría pero lo más importante la práctica crear algo real, y no quedarme varado cuando no entienda a alguien si no saber que existe una persona allí en el diplomado que me va a ayudar.</p>
										</div>
										<div class="modal-footer">
											<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar ventana</button>
										</div>
									</div>
								</div>
							</div>
						</div>
<div class="each-review">
							<span class="glyphicon glyphicon-star"></span>
							<span class="glyphicon glyphicon-star"></span>
							<span class="glyphicon glyphicon-star"></span>
							<span class="glyphicon glyphicon-star"></span>
							<span class="glyphicon glyphicon-star"></span>
							<p>excelente ! <span>Enero 27 de 2014</span><small>Por: Luis F (San José, Costa Rica)</small></p>
							<div class="info">
								<p>Soy un programador , pero nuevo en todo lo de creaciones de apps Objective-C, XCode , y la programación de iOS... <a href="#" data-toggle="modal" data-target="#nine">Leer más</a></p>
							</div>
							<div class="modal fade" id="nine" tabindex="-1" role="dialog" aria-labelledby="nine" aria-hidden="true">
								<div class="modal-dialog">
									<div class="modal-content">
								
										<div class="modal-body">
											<p class="fecha">Enero 27 de 2014</p>
											<h3>excelente !</h3>
											<p class="by">Por: Luis F (San José, Costa Rica)</p>
											<p class="comment">Soy un programador , pero nuevo en todo lo de creaciones de apps Objective-C, XCode , y la programación de iOS</p>
											<p class="comment">Actualmente estoy en la parte avanzada del diplomado y este diplomado me recuerda talleres patrocinados en la empresa que eran extremadamente costosos.</p>
											<p class="comment">Empieza de una forma muy amigable , paso a paso , a la programación de apps usando Objective -C y XCode . Los ejemplos son muy bien hechos y me parece en general se puede completar mucho en aproximadamente 1-2 horas al día que se le dediquen . Al final de cada clase hay actividades para practicar lo que vas aprendiendo.</p>
											<p class="comment">En este momento creo que tengo una buena comprensión general de desarrollo Apps y me siento cómodo escribiendo código para la creacion de apps. Además en el diplomado se cubre una serie de temas importantes relacionados con la programación de iOS que probablemente me ahorrará mucho tiempo cuando llega el momento de hacer un poco de trabajo de desarrollo real.</p>
											<p class="comment">Por cierto, yo ya estaba familiarizado con más de media docena de otros lenguajes de programación , así que pude entender de Objective- C de una forma simple con el diplomado. Dependiendo de tu nivel de experiencia, es posible que tengas que dedicarle un poco mas de tiempo de lo normal para ponerte al dia.</p>
										</div>
										<div class="modal-footer">
											<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar ventana</button>
										</div>
									</div>
								</div>
							</div>
						</div>

<div class="each-review">
							<span class="glyphicon glyphicon-star"></span>
							<span class="glyphicon glyphicon-star"></span>
							<span class="glyphicon glyphicon-star"></span>
							<span class="glyphicon glyphicon-star"></span>
							<span class="glyphicon glyphicon-star"></span>
							<p>No estoy seguro que sea para principiantes pero es MUY bueno!<span>Noviembre 19 de 2013</span><small>Por: Frank (Buenos Aires, Argentina)</small></p>
							<div class="info">
								<p>Un diplomado q tiene una buena estructura y esta bien organizado pero creo... <a href="#" data-toggle="modal" data-target="#ten">Leer más</a></p>
							</div>
							<div class="modal fade" id="ten" tabindex="-1" role="dialog" aria-labelledby="ten" aria-hidden="true">
								<div class="modal-dialog">
									<div class="modal-content">
								
										<div class="modal-body">
											<p class="fecha">Noviembre 19 de 2013</p>
											<h3>No estoy seguro que sea para principiantes pero es MUY bueno!</h3>
											<p class="by">Por: Frank (Buenos Aires, Argentina)</p>
											<p class="comment">Un diplomado q tiene una buena estructura y esta bien organizado pero creo  que está orientado para personas que tienen experiencia en programación. Este diplomado me lo recomendaron, y después que me  inscribi , me di cuenta de que debería quizás tener un poco más de experiencia en lo basico de programacion. Pero a pesar que empecé un poco lento, no tan rápido como pensé, me dieron instrucciones más detalladas para un principiante como yo y ya despues de alli todo fue más simple. Mientras voy adelantando el diplomado se vuelve mucho mas facil y aveces me toca dedicarle tiempo extra para captar el concepto.</p>
											<p class="comment">Si tienes el tiempo necesario y la constancia entonces es una inversión segura en mi opinión personal pero necesita practica y constancia no puedes parar. por eso le doy 4 estrellas.</p>
										</div>
										<div class="modal-footer">
											<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar ventana</button>
										</div>
									</div>
								</div>
							</div>
						</div>
					
<div class="each-review">
							<span class="glyphicon glyphicon-star"></span>
							<span class="glyphicon glyphicon-star"></span>
							<span class="glyphicon glyphicon-star"></span>
							<span class="glyphicon glyphicon-star"></span>
							<span class="glyphicon glyphicon-star"></span>
							<p>La forma más útil para aprender a crear apps sin tener ninguna experiencia previa<span>Enero 20 de 2014</span><small>Por: Oscar (Valencia, Venezuela)</small></p>
							<div class="info">
								<p>No soy un ingeniero de software profesional , pero , soy apasionado por la computación. Como... <a href="#" data-toggle="modal" data-target="#doce">Leer más</a></p>
							</div>
							<div class="modal fade" id="doce" tabindex="-1" role="dialog" aria-labelledby="doce" aria-hidden="true">
								<div class="modal-dialog">
									<div class="modal-content">
								
										<div class="modal-body">
											<p class="fecha">Enero 20 de 2014</p>
											<h3>La forma más útil para aprender a crear apps sin tener ninguna experiencia previa</h3>
											<p class="by">Por: Oscar (Valencia, Venezuela)</p>
											<p class="comment">No soy un ingeniero de software profesional , pero , soy apasionado por la computación. Como trabajo / freelance hago paginas web y he incursionado en aprender otros campos para tener más oportunidades, Aún así, cuando me decidí a aprender el desarrollo de aplicaciones me sentí algo así como hormiga en baile gallina. Este curso fue la luz al final del túnel. Le puso fin a la locura y coloco todo en un contexto en que lo pude entender.</p>
											<p class="comment">Aunque todo en el desarrollo de iOS sigue una lógica coherente , que es una parte lógica seleccionada por Apple.  Por lo menos, es necesaria alguna explicación de por qué estas cosas son como son ( legibilidad ) .Este diplomado da una introducción simple pero concreta  que explica las opciones de diseño y te lleva de la mano paso a paso , una vez que te acostumbras a los estándares para crear apps vas a amar todo el mundo de Apple.</p>
											<P class="comment">Quizás para programadores más experimentados puede parecer un poco lento o básico. Sin embargo , de todos los cursos y tutoriales que he tomado , este tiene el mejor equilibrio entre básico y avanzado . Los fundamentos se entremezclan de una manera que te frustras si no entiendes algo muy básico.</P>
											<p class="comment">Le voy a dar cuatro estrellas porque estoy justo al principio , así que no puedo decir como va a terminar. Sin embargo , mi impresión inicial es muy positiva y yo se lo recomendaría a varios amigos que se quieren iniciar en crear apps.</p>
										</div>
										<div class="modal-footer">
											<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar ventana</button>
										</div>
									</div>
								</div>
							</div>
						</div>

<div class="each-review">
							<span class="glyphicon glyphicon-star"></span>
							<span class="glyphicon glyphicon-star"></span>
							<span class="glyphicon glyphicon-star"></span>
							<span class="glyphicon glyphicon-star"></span>
							<span class="glyphicon glyphicon-star-empty"></span>
							<p>Realmente es un gran diplomado<span>Enero 12 de 2014</span><small>Por: Rodolfo Castañeda (La Paz, Bolivia )</small></p>
							<div class="info">
								<p>Me encanta el estilo del diplomado. Un gran diplomado, soy nuevo creando apps para iphone y android</p>
							</div>
						</div>
<div class="each-review">
							<span class="glyphicon glyphicon-star"></span>
							<span class="glyphicon glyphicon-star"></span>
							<span class="glyphicon glyphicon-star"></span>
							<span class="glyphicon glyphicon-star"></span>
							<span class="glyphicon glyphicon-star"></span>
							<p>Muy útil<span>Enero 15 de 2014</span><small>Por: Susana Milestrone (Los Lagos, Chile)</small></p>
							<div class="info">
								<p>Claro como el agua, ejemplos excelentes, muy buen sitio amigos. Los ejercicios... <a href="#" data-toggle="modal" data-target="#modal1">Leer más</a></p>
							</div>
							<div class="modal fade" id="modal1" tabindex="-1" role="dialog" aria-labelledby="modal1" aria-hidden="true">
								<div class="modal-dialog">
									<div class="modal-content">
								
										<div class="modal-body">
											<p class="fecha">Enero 15 de 2014</p>
											<h3>Muy útil</h3>
											<p class="by">Por: Susana Milestrone (Los Lagos, Chile)</p>
											<p class="comment">Claro como el agua, ejemplos excelentes, muy buen sitio amigos. Los ejercicios están bien pensados ​​y desafiantes, que en realidad aumentan la eficacia del diplomado. Este es el diplomado para empezar.</p>
										</div>
										<div class="modal-footer">
											<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar ventana</button>
										</div>
									</div>
								</div>
							</div>
						</div>
<div class="each-review">
							<span class="glyphicon glyphicon-star"></span>
							<span class="glyphicon glyphicon-star"></span>
							<span class="glyphicon glyphicon-star"></span>
							<span class="glyphicon glyphicon-star"></span>
							<span class="glyphicon glyphicon-star-empty"></span>
							<p>La mejor forma de aprender a crear apps<span>diciembre 5 de 2013</span><small>Por: Ricardo Grisales ( Francisco Morazan, Honduras)</small></p>
							<div class="info">
								<p>Este es la mejor forma de aprender a crear apps. Todos los conceptos se explican con claridad y fácil de seguir. No deja ningún detalle y esta actualizado.</p>
							</div>
						</div>

<div class="each-review">
							<span class="glyphicon glyphicon-star"></span>
							<span class="glyphicon glyphicon-star"></span>
							<span class="glyphicon glyphicon-star"></span>
							<span class="glyphicon glyphicon-star"></span>
							<span class="glyphicon glyphicon-star"></span>
							<p>Buena estructura, es exactamente lo que se necesita para empezar a crear apps<span>Noviembre 27 de 2013</span><small>Por: José Ignacio Montalban (Distrito federal, Mexico)</small></p>
							<div class="info">
								<p>El diplomado esta Excelente, muy bien organizado. Es exactamente lo que necesita para empezar a aprender a crear apps. Casi todos los temas... <a href="#" data-toggle="modal" data-target="#modal">Leer más</a></p>
							</div>
							<div class="modal fade" id="modal" tabindex="-1" role="dialog" aria-labelledby="modal" aria-hidden="true">
								<div class="modal-dialog">
									<div class="modal-content">
								
										<div class="modal-body">
											<p class="fecha">Noviembre 27 de 2013</p>
											<h3>Buena estructura, es exactamente lo que se necesita para empezar a crear apps</h3>
											<p class="by">Por: José Ignacio Montalban (Distrito federal, Mexico)</p>
											<p class="comment">El diplomado esta Excelente, muy bien organizado . Es exactamente lo que necesita para empezar a aprender a crear apps  . Casi todos los temas están cubiertos en un buen nivel . No ahondan en detalles muy complicados , pero si está buscando un diplomado para iniciar la creacion de apps, entonces este es la mejor opcion. Hay varios otros cursos disponibles , pero nadie es capaz llevar la clases de tal forma limpia y fácil como lo hace este diplomado. Yo recomendaría este diplomado a cualquier persona que tenga conocimiento escaso o medio de programación o creacion de apps.</p>
										</div>
										<div class="modal-footer">
											<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar ventana</button>
										</div>
									</div>
								</div>
							</div>
						</div>

<div class="each-review">
							<span class="glyphicon glyphicon-star"></span>
							<span class="glyphicon glyphicon-star"></span>
							<span class="glyphicon glyphicon-star"></span>
							<span class="glyphicon glyphicon-star"></span>
							<span class="glyphicon glyphicon-star"></span>
							<p>Gran diplomado<span>Febrero 03 de 2014</span><small>Por: José Angel (California, United States)</small></p>
							<div class="info">
								<p>	Muy útil para un programador con experiencia, pero que aún esté buscando aprender a programar para... <a href="#" data-toggle="modal" data-target="#uju">Leer más</a></p>
							</div>
							<div class="modal fade" id="uju" tabindex="-1" role="dialog" aria-labelledby="uju" aria-hidden="true">
								<div class="modal-dialog">
									<div class="modal-content">
								
										<div class="modal-body">
											<p class="fecha">Noviembre 27 de 2013</p>
											<h3>Gran diplomado</h3>
											<p class="by">Por: José Angel (California, United States)</p>
											<p class="comment">Muy útil para un programador con experiencia, pero que aún esté buscando aprender a programar para IOS o Android. Estoy muy contento que tome este diplomado, ya que te guía a través de una gran cantidad de código y explica las clases pertinentes, los métodos, las funciones, etc. Hacen un buen trabajo de no entrar demasiado a fondo con la explicación y simplemente crear y tirar código una forma muy intuitiva de aprender. Es un buen equilibrio que mantiene el diplomado.</p>
										</div>
										<div class="modal-footer">
											<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar ventana</button>
										</div>
									</div>
								</div>
							</div>
						</div>

<div class="each-review">
							<span class="glyphicon glyphicon-star"></span>
							<span class="glyphicon glyphicon-star"></span>
							<span class="glyphicon glyphicon-star"></span>
							<span class="glyphicon glyphicon-star"></span>
							<span class="glyphicon glyphicon-star-empty"></span>
							<p>No es para personas que son absolutamente nuevas en la programación<span>Febrero 03 de 2014</span><small>Por: Ivan Benites (Región Metropolitana, Chile)</small></p>
							<div class="info">
								<p>Soy programador y tenían poca (o ninguna) experiencia en el desarrollo de iOS, este diplomado excelente  pero creo que para una  personas nueva en programación le va a tocar dura  ... pero si puedes dedicarle tiempo y trabajo entonces es cuestión de dedicarle.</p>
							</div>
						</div>

<div class="each-review">
							<span class="glyphicon glyphicon-star"></span>
							<span class="glyphicon glyphicon-star"></span>
							<span class="glyphicon glyphicon-star"></span>
							<span class="glyphicon glyphicon-star"></span>
							<span class="glyphicon glyphicon-star"></span>
							<p>Buen diplomado para principiantes en plataforma móviles.<span>Octubre 29 de 2013</span><small>Por: Diego Montenegro (Sonora, Mexico)</small></p>
							<div class="info">
								<p>	Me parece que el diplomado tiene una muy buena introducción a Obj C y en conceptos. No he tenido problemas con las clases  y encontrar el material fácil de entender. Creo que es útil contar con conocimiento en programación pero no es necesario Muy recomendado.</p>
							</div>
						</div>

<div class="each-review">
							<span class="glyphicon glyphicon-star"></span>
							<span class="glyphicon glyphicon-star"></span>
							<span class="glyphicon glyphicon-star"></span>
							<span class="glyphicon glyphicon-star-empty"></span>
							<span class="glyphicon glyphicon-star-empty"></span>
							<p>Curso decente<span>Enero 20 de 2014</span><small>Por: JPG (Cordoba, Colombia)</small></p>
							<div class="info">
								<p>Mi opinión está dividida sobre este diplomado, porque veo que tiene muy buen material y esta muy bien organizado, pero... <a href="#" data-toggle="modal" data-target="#twotwo">Leer más</a></p>
							</div>
							<div class="modal fade" id="twotwo" tabindex="-1" role="dialog" aria-labelledby="twotwo" aria-hidden="true">
								<div class="modal-dialog">
									<div class="modal-content">
								
										<div class="modal-body">
											<p class="fecha">Enero 20 de 2014</p>
											<h3>Curso decente</h3>
											<p class="by">Por: JPG (Cordoba, Colombia)</p>
											<p class="comment">Mi opinión está dividida sobre este diplomado, porque veo que tiene muy buen material y esta muy bien organizado, pero tuve que luchar para empezar desde 0 con la creacion de apps. Creo que existen mejores forma de aprender programacion aunque quisas no es caso para una persona que quiere aprender a crear apps y luego profundizar. Pero para una persona que empieza de 0 toca dedicarle para ir profundizando.</p>
										</div>
										<div class="modal-footer">
											<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar ventana</button>
										</div>
									</div>
								</div>
							</div>
						</div>

<div class="each-review">
							<span class="glyphicon glyphicon-star"></span>
							<span class="glyphicon glyphicon-star"></span>
							<span class="glyphicon glyphicon-star"></span>
							<span class="glyphicon glyphicon-star-empty"></span>
							<span class="glyphicon glyphicon-star-empty"></span>
							<p>Creacion de apps para personas con ideas<span>Enero 22 de 2014</span><small>Por: Paul S (Lima, Peru)</small></p>
							<div class="info">
								<p>Estilo de diplomado esta. Bien estructurado y hasta ahora a sido una de mis formas favoritas de aprender algo me atreveria a decir que fue divertido</p>
							</div>
						</div>

<div class="each-review">
							<span class="glyphicon glyphicon-star"></span>
							<span class="glyphicon glyphicon-star"></span>
							<span class="glyphicon glyphicon-star"></span>
							<span class="glyphicon glyphicon-star"></span>
							<span class="glyphicon glyphicon-star"></span>
							<p>Me encanto este diplomado<span>Diciembre 10 de 2013</span><small>Por: ManUel (Región Metropolitana, Chile)</small></p>
							<div class="info">
								<p>Estoy en la segunda parte del diplomado. Ni te imaginas las nuevas técnicas y... <a href="#" data-toggle="modal" data-target="#oneone">Leer más</a></p>
							</div>
							<div class="modal fade" id="oneone" tabindex="-1" role="dialog" aria-labelledby="oneone" aria-hidden="true">
								<div class="modal-dialog">
									<div class="modal-content">
								
										<div class="modal-body">
											<p class="fecha">Diciembre 10 de 2013</p>
											<h3>Me encanto este diplomado</h3>
											<p class="by">Por: ManUel (Región Metropolitana, Chile)</p>
											<p class="comment">Estoy en la segunda parte del diplomado. Ni te imaginas las nuevas técnicas y conceptos que se vienen mucho mejor que la primera parte. Me encantan los ejemplos incrementalmente complejos y la forma en que se presentan.</p>
										</div>
										<div class="modal-footer">
											<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar ventana</button>
										</div>
									</div>
								</div>
							</div>
						</div>

					</div>					
				</div>
			</div>
		</div>
	</div>
