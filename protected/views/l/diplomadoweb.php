<div class="container diplomado-l web">
  <div class="row fold">
    <div class="col-sm-12">
      <p class="pre">Aprende paso a paso a crear cualquier aplicación desde cero</p>
      <h1>Diplomado de desarrollo <br>de páginas web</h1>
      <p class="post">garantizado el mejor dinero que puedes gastar en educación on-line.</p>
      <small>*No necesitas experiencia</small>
    </div>
  </div>

  <div class="row programa">
    <div class="mod-header">
      <h2>Programa del Diplomado</h2>
      <p>El programa comprende módulos teórico/prácticos y proyectos completos con códigos descargables; <br> este Diplomado se completará de forma on-line a lo largo de 12 meses.</p>
    </div>


    <div class="col-sm-3 mod1">
      <p><strong>Módulo 1</strong></p>
      <h4>Aprende HTML paso a paso para crear tu página web</h4>
      <small>46 clases  ·  8 archivos</small>
    </div>


    <div class="col-sm-3 mod2">
      <p><strong>Módulo 2</strong></p>
      <h4>Diseña y desarrolla tu página web con Bootstrap 3</h4>
      <small>33 clases  ·  23 archivos</small>
    </div>


    <div class="col-sm-3 mod3">
      <p><strong>Módulo 3</strong></p>
      <h4>Aprende a usar Adobe Fireworks en tus desarrollos web</h4>
      <small>51 clases  ·  38 archivos</small>
    </div>


    <div class="col-sm-3 mod4">
      <p><strong>Módulo 4</strong></p>
      <h4>Fundamentos de Javascript para mejorar tu trabajo web</h4>
      <small>36 clases  ·  34 archivos</small>
    </div>


    <div class="col-sm-3 mod5">
      <p><strong>Módulo 5</strong></p>
      <h4>Aprende a mejorar tus desarrollos web siendo un ninja de Jquery</h4>
      <small>40 clases  ·  28 archivos</small>
    </div>


    <div class="col-sm-3 mod6">
      <p><strong>Módulo 6</strong></p>
      <h4>Aprende a usar responsive design en tus diseños web</h4>
      <small>39 clases  ·  35 archivos</small>
    </div>


    <div class="col-sm-3 mod6">
      <p><strong>Módulo 7 / Proyecto 1</strong></p>
      <h4>Aprende a crear un TODO list en HTML5</h4>
      <small>10 clases  ·  7 archivos</small>
    </div>


    <div class="col-sm-3 mod7">
      <p><strong>Módulo 8 / Proyecto 2</strong></p>
      <h4>Crea tu propia tienda online con PHP</h4>
      <small>38 clases  ·  34 archivos</small>
    </div>


    <div class="col-sm-3 mod8">
      <p><strong>Módulo 9 / Proyecto 2</strong></p>
      <h4>Expande y optimiza tu tienda online con PHP</h4>
      <small>46 clases  ·  38 archivos</small>
    </div>


    <div class="col-sm-3 mod9">
      <p><strong>Módulo 10 / Proyecto 2</strong></p>
      <h4>Potencia y mejora tu tienda on-line usando PHP y MySQL</h4>
      <small>27 clases  ·  20 archivos</small>
    </div>


    <div class="col-sm-3 mod10">
      <p><strong>Módulo 11</strong></p>
      <h4>Tutorial en español paso a paso sobre MySQL</h4>
      <small>26 clases  ·  7 archivos</small>
    </div>


    <div class="col-sm-3 mod11">
      <p><strong>Módulo 12</strong></p>
      <h4>Tutorial paso a paso sobre fundamentos de bases de datos relacionales</h4>
      <small>34 clases</small>
    </div>


    <h5>Módulo final: Exámen de finalización con un instructor especializado</h5>
    <a  class="btn btn-success btn-lg" href="<?php echo Yii::app()->request->baseUrl; ?>/suscripciones" >¡Inicia tu diplomado ahora!</a> 
    <small>*No necesitas experiencia</small>
  </div>

  <div class="row include">
    <div class="col-sm-12">
    <div class="mod-header">
      <h2>Recuerda el Diplomado incluye:</h2>
      <p>Todo lo que necesitas para crear cualquier tipo de app para iOS</p>
    </div>
        
      <div class="col-sm-4 mod1">
        <img alt="" src="<?php echo Yii::app()->request->baseUrl; ?>/images/downloads.png">
        <p>Acceso a más de 251 archivos descargables, disponibles todo el tiempo, guías, códigos, etc</p>
      </div>
      <div class="col-sm-4 mod2">
        <img alt="" src="<?php echo Yii::app()->request->baseUrl; ?>/images/certificado.png">
        <p>Certificación de terminación al terminar el Diplomado Validada por nuestros instructores</p>
      </div>
      <div class="col-sm-4 mod3">
        <img alt="" src="<?php echo Yii::app()->request->baseUrl; ?>/images/servicio.png">
        <p>Nunca estarás solo, sí necesitas ayuda tu asesor estará atento vía chat, mail o Skype.</p>
      </div>
    <h5>Además de forma inmediata recibes acceso a más de 200 cursos de nuestro biblioteca</h5>
    <a  class="btn btn-success btn-lg" href="<?php echo Yii::app()->request->baseUrl; ?>/suscripciones" >¡Inicia tu diplomado ahora!</a> 
    <small>*No necesitas experiencia</small>
    </div>
  </div>
  <div class="row apoyo">
    <div class="mod-header">
      <h2>¿Quién apoya Oja.la?</h2>
    </div>
    <img alt="" src="<?php echo Yii::app()->request->baseUrl; ?>/images/marcas.gif">
    <div class="row kudos">
      <div class="mod1">
        <h5>“Una atención por demás personalizada muy humana, y en total confianza, como siempre resolviendo hasta las dudas mas pequeñas. excelente atencion!</h5>
        <img alt="" src="<?php echo Yii::app()->request->baseUrl; ?>/images/avatar-estudiante.jpg">
        <p><strong>Jabama Madrid</strong></p>
        <a href="https://www.facebook.com/jabama" target="_blank">Ver post</a>
      </div>
      <div class="mod2">
        <h5>“Excelente Plataforma, excelente soporte siempre en línea y con los cambios que se vienen será mejor :D Soy programador y en el momento llevo tres cursos empece con introduccion a phonegap build server y ese me llevo a otro y otro :D. Son cosas que no sabían y me ayudan a mejorar profesionalmente."</h5>
        <img alt="" src="<?php echo Yii::app()->request->baseUrl; ?>/images/avatar-estudiante-2.jpg">
        <p><strong>Oscar Sán</strong></p>
        <a href="https://twitter.com/haleando/status/486925917340835840" target="_blank">Ver post</a>
      </div>
      <div class="mod3">
        <h5>“Lo que me gusto de Oja.la, es la excelente calidad de enseñanza que brinda en diferentes cursos, tienen un soporte técnico excelente, y que siempre tienen un curso nuevo que llama mucho la atención :)"</h5>
        <img alt="" src="https://graph.facebook.com/100000518251121/picture">
        <p><strong>Jose Antonio Pulido Murga</strong></p>
        <a href="https://www.facebook.com/OjalaLatam/posts/924296680930932" target="_blank">Ver post</a>
      </div>
      <div class="clearfix"></div>
      <div class="mod1">
        <h5>“Lo rápido y directo para resolver el problema, muy buen servicio felicitaciones."</h5>
        <img alt="" src="<?php echo Yii::app()->request->baseUrl; ?>/images/avatar-estudiante-3.jpg">
        <p><strong>Marcelo Aravena Boza</strong></p>
        <a href="https://twitter.com/marceloaravenab/status/492699238275751937" target="_blank">Ver post</a>
      </div>
      <div class="mod2">
        <h5>“atención bastante personalizada y con buenos consejos para iniciar la educación"</h5>
        <img alt="" src="<?php echo Yii::app()->request->baseUrl; ?>/images/avatar-estudiante-4.jpg">
        <p><strong>Lucio Salinas</strong></p>
        <a href="https://twitter.com/luci0s/status/482573789231054848" target="_blank">Ver post</a>
      </div>
      <div class="mod3">
        <h5>“Genial!, Respuestas oportunas e inmediatas, Recomendado"</h5>
        <img alt="" src="<?php echo Yii::app()->request->baseUrl; ?>/images/avatar-estudiante-5.jpg">
        <p><strong>Diego Fernando Lara</strong></p>
        <a href="https://twitter.com/DevFerLara/status/486252362508795904" target="_blank">Ver post</a>
      </div>
    </div>
  </div>
  <div class="row final">
    <div class="mod-header">
      <h2>¿Listo para empezar?</h2>
    </div>
    <a  class="btn btn-success btn-lg" href="<?php echo Yii::app()->request->baseUrl; ?>/suscripciones">¡Inicia tu Diplomado ahora!</a> 
    <p>No lo olvides, acceso ilimitado al Diplomado y más de 200 cursos sin que cueste más.</p>
  </div>
</div>