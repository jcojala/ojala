<script type="text/javascript" src="../js/jquery.countdown.js"></script>
<script type="text/javascript">
    $(document).ready(function() {
        var endDate = "<?php echo OjalaUtils::getFechaLanding(Yii::app()->controller->action->id); ?> 23:59:59";
        $('.countdown.simple').countdown({ date: endDate });
        $('.countdown.styled').countdown({
          date: endDate,
          render: function(data) {
            $(this.el).html("<div>" + this.leadingZeros(data.days, 2) + " <span>Días</span></div><div>" + this.leadingZeros(data.hours, 2) + " <span>hrs</span></div><div>" + this.leadingZeros(data.min, 2) + " <span>min</span></div><div>" + this.leadingZeros(data.sec, 2) + " <span>sec</span></div>");
          }
        });
    });
</script>

<style type="text/css">
.amigos{
  background: #f1f1f1 url('../images/back.jpg') center -60px no-repeat !important;
  text-align: center;
  font-family: "Arial Narrow", Arial, sans-serif !important;  
}

/* Header  */
.amigos header{
  text-align: center;
  padding: 10px 0;
  background:rgba(0,97,191,1);
  border-bottom: 3px solid #003a73;
}

/* Contenidos */ 
.amigos .container{
  padding-top: 0px;
}
.amigos .main-msg{ 
  margin-top: 30px;
}
.amigos .main-msg p{
  font-size: 40px;
  line-height: 40px;
  color: #e00;
  margin: 0 0 5px 0;
}
.amigos .main-msg h1{
  font-size: 60px;
  line-height: 60px;
  font-weight: bold;
  margin: 0 0 15px 0; 
  color: #e00;
}
.amigos .main-msg h5{
  font-size: 30px;
  line-height: 30px;
  margin: 0 0 30px 0;
}

/* Combo box */
.amigos .container .cupon-box{
  border: 4px dashed #999999;
  width: 500px;
  margin: 0 auto 40px auto;
  border-radius:20px;
  padding: 10px 20px;
  position: relative;
}
.amigos .container .cupon-box img{
  position: absolute;
  left: -40px;
  top: 90px;
}
.amigos .container .cupon-box small{
  font-size: 18px;
  display: block;
  margin: 0 0 10px 0;
}
.amigos .container .cupon-box a,
.amigos .container .cupon-box button{
  font-size: 24px;
  padding: 14px 50px;
  background-color: #e00;
  border-bottom: 5px solid #a00;
  margin: 0 5% 15px 5%;
  border-radius: 0 0 5px 5px;
  text-shadow: 0 2px 2px #000;
  width: 80%;
}
.amigos .container .cupon-box a:hover,
.amigos .container .cupon-box button:hover{ background-color: #c00;}
.amigos .container .cupon-box a:active,
.amigos .container .cupon-box button:active{background-color: #a00;}
.amigos .container .cupon-box p{ font-size: 20px;}

.amigos .container .callback, 
.amigos .container .simple {
  font-size: 20px;
  background: #000;
  padding: 0;
  color: #fff;
  -webkit-transition: background 0.5s ease-out;
  transition: background 0.5s ease-out;
}
.amigos .container .callback{
  cursor: pointer;
}
.amigos .container .ended {
  background: #c0392b;
}
.amigos .container .styled{
  margin-bottom: 20px;
  background: #666;
  border-radius: 5px 5px 0 0;
  margin: 0 10%;
  padding: 10px 0;
  color: #fff;
  width: 80%;
  height: 95px;
}
.amigos .container .styled div {
  display: inline-block;
  font-size: 40px;
  font-weight: bold;
  line-height: 1;
  text-align: center;
  background: #333;
  margin: 0 3px;
  padding: 10px;
  border-radius: 5px;
  height: 75px;
}
*+html .amigos .container .styled div{
  display: inline;
  zoom: 1;
}
.amigos .container .styled div:first-child {
  margin-left: 0;
}
.amigos .container .styled div span {
  display: block;
  padding-top: 3px;
  font-size: 12px;
  font-weight: normal;
  text-transform: uppercase;
  text-align: center;
}

/* Listado de lo que se incluye */
.amigos .includes{
  width: 520px;
  margin: 0 auto 20px auto;
  text-align: left;
}
.amigos .includes h4{
  font-weight: bold;
  margin: 0 0 20px 15px;
}
.amigos .includes ul{
  margin: 0 0 20px 0;
}
.amigos .includes li{
  font-size: 18px;
  margin: 0 0 10px 0;
}
.amigos .includes a,
.amigos .includes a:hover{
  color: #e00;
  font-weight: bold;
}

/* Listado de cursos */
.amigos .mod-courses h3{
  color:#000; 
  font-size:36px;
  margin-bottom: 30px;
}
.amigos .mod-course{
  margin-bottom: 40px;
}

</style>
<div class="menu-princ">
<?php $this->widget('MainMenu', array('show'=>'byRole')); ?>
</div>
<div class="amigos">

  <div class="row">
    <div class="container">

      <div class="main-msg">
        <h1>2 amigos en 1 suscripción</h1>
        <h5>Acceso ilimitado para los 2 a TODOS los cursos y Diplomados en Oja.la</h5>
      </div>
      
      <div class="cupon-box">
        <img src="../images/BF-arrow.png">
        <small>Esta oferta terminará en:</small>
        
        <div class="countdown styled"></div>

        <a href="<?php echo Yii::app()->urlManager->createUrl('site/suscripciones'); ?>" class="btn btn-lg btn-danger">Activa el acceso ahora</a>
        <p>¡Al final del proceso activarémos a tu amigo personalmente!</p>
      </div>

      <div class="includes">
        <h4>ESTA OFERTA INCLUYE:</h4>
        <ul>
          <li>Acompañamiento personalizado</li>
          <li>Los 2 reciben asesoria de un asesor personal</li>
          <li>Reciben acceso ilimitado a más de 200 cursos</li>
          <li>Acceso a nuevos cursos y clases en la biblioteca cada semana</li>
          <li>Descarga de todos los archivos usados por el instructor</li>
          <li>Disponible 100% a tu propio tiempo, 24Hrs, los 7 días de la semana</li>
          <li>Clases basadas 100% en proyectos y desarrollos reales</li>
          <li>Acceso a certificación al terminar cada curso</li>
          <li>Soporte inmediato vía Skype, Chat ó E-mail</li>
          <li>Descarga de códigos fuente de cada proyecto</li>
        </ul>
      </div>


      <div class="container mod-courses">

        <h3>Algunos de los 200 cursos en la biblioteca:</h3>

        <div class="col-md-3 mod-course">
          <div class="thumbnail">
            <div class="img">
              <img src="https://oja.la/images/covers/ios-twitter-580x41572.jpg">  
            </div>
            <div class="caption">
              <h6>200 personas interesadas en:</h6>
              <h4>Aprende como crear un Twitter para iOS con todo y base de datos usando Parse</h4>
            </div>
          </div>
        </div>
        
        <div class="col-md-3 mod-course">
          <div class="thumbnail">
            <div class="img">
              <img src="https://oja.la/images/covers/android-tools-simple-580x415.png">
            </div>
            <div class="caption">
              <h6>202 personas interesadas en:</h6>
              <h4>Herramientas de desarrollo en Android para principiantes</h4>
            </div>
          </div>
        </div>
        
        <div class="col-md-3 mod-course">
          <div class="thumbnail">
            <div class="img">
              <img src="https://oja.la/images/covers/sublime-580x415.jpg">
            </div>
            <div class="caption">
              <h6>138 personas interesadas en:</h6>
              <h4>Aprende a usar Sublime Text, la mejor herramienta para escribir el código de tus proyectos</h4>
            </div>
          </div>
        </div>

        <div class="col-md-3 mod-course">
          <div class="thumbnail">
            <div class="img">
              <img src="https://oja.la/images/covers/social-media-580x41529.jpg">
            </div>
            <div class="caption">
              <h6>315 personas interesadas en:</h6>
              <h4>Curso de Marketing Digital: Administración de comunidades online</h4>
            </div>
          </div>
        </div>
        


        <div class="col-md-3 mod-course">
          <div class="thumbnail">
            <div class="img">
              <img src="https://oja.la/images/covers/google-analytics-580x415.jpg">
            </div>
            <div class="caption">
              <h6>214 personas interesadas en:</h6>
              <h4>Aprende cómo usar Google Analytics de forma simple para tu página web</h4>
            </div>
          </div>
        </div>
        
        <div class="col-md-3 mod-course">
          <div class="thumbnail">
            <div class="img">
              <img src="https://oja.la/images/covers/ios-jetpack-580x415.jpg">
            </div>
            <div class="caption">
              <h6>279 personas interesadas en:</h6>
              <h4>Aprende a crear un juego como Jetpack Joyride en Unity 2D</h4>
            </div>
          </div>
        </div>

        <div class="col-md-3 mod-course">
          <div class="thumbnail">
            <div class="img">
              <img src="https://oja.la/images/covers/google-tools-2-580x41555.jpg">
            </div>
            <div class="caption">
              <h6>150 personas interesadas en:</h6>
              <h4>Aprende a optimizar tu sitio web con las herramientas de Google</h4>
            </div>
          </div>
        </div>

        <div class="col-md-3 mod-course">
          <div class="thumbnail">
            <div class="img">
              <img src="https://oja.la/images/covers/ios-swift-580x415.jpg">
            </div>
            <div class="caption">
              <h6>897 personas interesadas en:</h6>
              <h4>Curso básico de Swift, nuevo lenguaje de programación para iPhone </h4>
            </div>
          </div>
        </div>

        <div class="col-md-3 mod-course">
          <div class="thumbnail">
            <div class="img">
              <img src="https://oja.la/images/covers/ios-candy-crush-580x415.jpg">
            </div>
            <div class="caption">
              <h6>410 personas interesadas en:</h6>
              <h4>Aprende a crear un juego como Candy Crush para iPhone</h4>
            </div>
          </div>
        </div>
        
        <div class="col-md-3 mod-course">
          <div class="thumbnail">
            <div class="img">
              <img src="https://oja.la/images/covers/fally-nyancat-580x415.jpg">
            </div>
            <div class="caption">
              <h6>331 personas interesadas en:</h6>
              <h4>Usa Sprite Kit para desarrollar un juego como Flappy NyanCat</h4>
            </div>
          </div>
        </div>
        
        <div class="col-md-3 mod-course">
          <div class="thumbnail">
            <div class="img">
              <img src="https://oja.la/images/covers/css-compass-580x415.jpg">
            </div>
            <div class="caption">
              <h6>268 personas interesadas en:</h6>
              <h4>Aprende como usar Compass en tus desarrollos web</h4>
            </div>
          </div>
        </div>

        <div class="col-md-3 mod-course">
          <div class="thumbnail">
            <div class="img">
              <img src="https://oja.la/images/covers/html5-basics-2-580x415.jpg">
            </div>
            <div class="caption">
              <h6>259 personas interesadas en:</h6>
              <h4>Aprende las bases del HTML paso a paso para crear tu página web [Parte II]</h4>
            </div>
          </div>
        </div>

        <div class="col-md-3 mod-course">
          <div class="thumbnail">
            <div class="img">
              <img src="https://oja.la/images/covers/objective.jpg">
            </div>
            <div class="caption">
              <h6>727 personas interesadas en:</h6>
              <h4>Curso de Objective C Básico</h4>
            </div>
          </div>
        </div>

        <div class="col-md-3 mod-course">
          <div class="thumbnail">
            <div class="img">
              <img src="https://oja.la/images/covers/5252034a0eb1ae959f0009ee.jpg">
            </div>
            <div class="caption">
              <h6>231 personas interesadas en:</h6>
              <h4>Crea aplicaciones para teléfonos con Windows Phone 8 desde cero</h4>
            </div>
          </div>
        </div>
        

        <div class="col-md-3 mod-course">
          <div class="thumbnail">
            <div class="img">
              <img src="https://oja.la/images/covers/4e9ff4ff0eb1ae0d5a00004f.png">
            </div>
            <div class="caption">
              <h6>448 personas interesadas en:</h6>
              <h4>Cómo conseguir dinero para un proyecto en Internet desde 0</h4>
            </div>
          </div>
        </div>
        

        <div class="col-md-3 mod-course">
          <div class="thumbnail">
            <div class="img">
              <img src="https://oja.la/images/covers/5245b5b50eb1ae283a000312.jpg">
            </div>
            <div class="caption">
              <h6>1324 personas interesadas en:</h6>
              <h4>Aprende a programar en 30 días paso a paso, con Ruby on Rails</h4>
            </div>
          </div>
        </div>

        <div class="col-md-3 mod-course">
          <div class="thumbnail">
            <div class="img">
              <img src="https://oja.la/images/covers/mysql-avanzado-580x41545.jpg">
            </div>
            <div class="caption">
              <h6>360 personas interesadas en:</h6>
              <h4>MySQL avanzado, todo lo que no sabias sobre Bases de datos</h4>
            </div>
          </div>
        </div>

        <div class="col-md-3 mod-course">
          <div class="thumbnail">
            <div class="img">
              <img src="https://oja.la/images/covers/mercadeo-basico-580x415.jpg">
            </div>
            <div class="caption">
              <h6>291 personas interesadas en:</h6>
              <h4>Aprende a manejar las herramientas más básicas de mercadeo online</h4>
            </div>
          </div>
        </div>

        <div class="col-md-3 mod-course">
          <div class="thumbnail">
            <div class="img">
              <img src="https://oja.la/images/covers/copy-580x415.jpg">
            </div>
            <div class="caption">
              <h6>175 personas interesadas en:</h6>
              <h4>Estrategias de copywriting para fundadores y vendedores</h4>
            </div>
          </div>
        </div>

        <div class="col-md-3 mod-course">
          <div class="thumbnail">
            <div class="img">
              <img src="https://oja.la/images/covers/producto.jpg">
            </div>
            <div class="caption">
              <h6>1324 personas interesadas en:</h6>
              <h4>¿Tienes una idea? Crea algo que la gente quiera</h4>
            </div>
          </div>
        </div>
      </div>
    </div>    
  </div>
</div>

<script src="https://checkout.stripe.com/checkout.js"></script>
<?php $key = YII_DEBUG ? 'pk_test_PquLfiHIRJDiZXSLhNtDAl3a' : 'pk_live_A8Ri0rI6Ux0McPCK4H6lLT24'; ?>
<script>
  var handler = StripeCheckout.configure({
    key: '<?php echo $key; ?>',
    image: '//d9hhrg4mnvzow.cloudfront.net/el.oja.la/cargar/1av03g-logomail.png',
    token: function(token, args) {
      $.ajax({
             'url': '<?php echo Yii::app()->urlManager->createUrl('site/crearUsuario'); ?>?email=' + token.email,
             'cache': false,
              'success': function(){
                $.ajax({
                  url: '<?php echo Yii::app()->urlManager->createUrl('site/hookLanding'); ?>',
                  type: 'POST',
                  cache: false,
                  data: { 
                      token: token.id, 
                      email: token.email,
                      plan: '<?php echo isset($_GET['plan']) ? $_GET['plan'] : '37-st' ?>'
                  },
                  success: function(data){
                      window.location = '<?php echo Yii::app()->createAbsoluteUrl("site/RedireccionLanding"); ?>?email=' + token.email + '&url=cursos';
                  }
                });
              }
       });
    }
  });

  document.getElementById('btn-checkout').addEventListener('click', function(e) {
    handler.open({
      name: '2x1 Para ti y un amigo',
      amount: <?php echo isset($_GET['plan']) ? substr($_GET['plan'],0,2) : 37 ?>00,
      currency: 'USD',  
      panelLabel: 'Tomar por',
      label: 'Iniciar Ahora',
      description: 'Acceso ilimitado por USD $<?php echo isset($_GET['plan']) ? substr($_GET['plan'],0,2) : 37 ?> al mes',
      allowRememberMe: false,
    });
    e.preventDefault();
  });
</script>
</div>