<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/jquery.countdown.js"></script>

<script type="text/javascript">
    $(document).ready(function() {
        var $header = $(".header-sticky"),
            $clone = $header.before($header.clone().addClass("clone"));
        
        $(window).on("scroll", function() {
            var fromTop = $(window).scrollTop();
            console.log(fromTop);
            $("body").toggleClass("down", (fromTop > 200));
        });

        var endDate = "January 31, 2014 23:59:59";  
        $('.countdown.simple').countdown({ date: endDate });
        $('.countdown.styled').countdown({
          date: endDate,
          render: function(data) {
            $(this.el).html("<div>" + this.leadingZeros(data.days, 2) + " <span>Días</span></div><div>" + this.leadingZeros(data.hours, 2) + " <span>hrs</span></div><div>" + this.leadingZeros(data.min, 2) + " <span>min</span></div><div>" + this.leadingZeros(data.sec, 2) + " <span>sec</span></div>");
          }
        });
    });
</script>



<div class="wordpress web">
    
    <!-- Main nav -->
    <nav class="navbar navbar-default" role="navigation">
      <div class="navbar-header">
          <p class="navbar-brand">Oja.la</p>
      </div>
    </nav>


    <div class="header-sticky">
      <div class="brand">Oja.la</div>
      <div class="cupon-box">
        <small>Inicia en:</small>      
        <div class="countdown styled"></div>
        <a href="<?php echo Yii::app()->createUrl('site/suscripciones'); ?>" class="btn btn-lg btn-success">Empieza hoy mismo →</a>
      </div>
    </div>


    <section class="fold">

      <?php if(!Yii::app()->user->isGuest){ ?>
      <div class="regards">
        <h3>Hola <?php echo Yii::app()->user->name; ?>, ¿Listo para iniciar?</h3> 
      </div>
      <?php } ?>

      <h1>Diplomado online de <br> HTML5 y CSS3</h1>
      <p>100% en español - Certificación al completar el diplomado</p>

      <div class="cupon-box">
          <small>Este diplomado inicia en:</small>      
          <div class="countdown styled"></div>
          <a href="<?php echo Yii::app()->createUrl('site/suscripciones'); ?>" class="btn btn-lg btn-success">Empieza hoy mismo →</a>
        </div>
    </section>
  
    <section class="cursos">
      <h2>¿Qué aprenderás en este diplomado?</h2>
      
      <article class="col-md-4 mod-course">
          <h3>Nivel I</h3>
          <div class="thumbnail">
              <div class="img">
                <img src="https://oja.la/images/covers/5022d5bb0eb1ae0dca000043.png">
              </div>
              <div class="caption">
                  <h6>3365 personas interesadas en:</h6>
                  <h4>Aprende las bases del HTML 5 <br> y CSS3 fácil y rápido</h4>
              </div>
            </div>
      </article>
      
      <article class="col-md-4 mod-course">
          <h3>Nivel II</h3>
          <div class="thumbnail">
              <div class="img">
                <img src="https://oja.la/images/covers/5063d9d00eb1ae73d3000f5b.jpg">
              </div>
              <div class="caption">
                  <h6>2158 personas interesadas en:</h6>
                  <h4>Aprende a analizar quienes visitan tu página web paso a paso con Google Analytics</h4>
              </div>
            </div>
      </article>
      
      <article class="col-md-4 mod-course">
          <h3>Nivel III</h3>
          <div class="thumbnail">
              <div class="img">
                <img src="https://oja.la/images/covers/joomla.jpg">
              </div>
              <div class="caption">
                  <h6>2985 personas interesadas en:</h6>
                  <h4>Construyendo plantillas para Joomla 3 con Bootstrap</h4>
              </div>
            </div>
      </article>

      <article class="col-md-4 mod-course">
          <h3>Nivel IV</h3>
          <div class="thumbnail">
              <div class="img">
                <img src="https://oja.la/images/covers/520476da0eb1ae97df00061a.jpg">
              </div>
              <div class="caption">
                  <h6>1678 personas interesadas en:</h6>
                  <h4>Aprende a mejorar tus desarrollos web siendo un ninja de Jquery</h4>
              </div>
            </div>
      </article>


      <article class="col-md-4 mod-course">
          <h3>Nivel V</h3>
          <div class="thumbnail">
              <div class="img">
                <img src="https://oja.la/images/covers/519ec9610eb1ae9b2800027c.jpg">
              </div>
              <div class="caption">
                  <h6>1325 personas interesadas en:</h6>
                  <h4>Aprende a crear tu propia página web con Wordpress</h4>
              </div>
            </div>
      </article>


      <article class="col-md-4 mod-course">
          <h3>Nivel VI</h3>
          <div class="thumbnail">
              <div class="img">
                <img src="https://oja.la/images/covers/5245b5b50eb1ae283a000312.jpg">
              </div>
              <div class="caption">
                  <h6>4123 personas interesadas en:</h6>
                  <h4>Aprende a programar en 30 días paso a paso, con Ruby on Rails</h4>
              </div>
            </div>
      </article>      
    </section>

    <section class="que-mas">
      <h2>¿Qué más vas a recibir en este diplomado?</h2>
      <article class="col-md-6">
        <img src="<?php echo Yii::app()->request->baseUrl; ?>/images/wordpress/certificado.png">
      </article>
      <article class="col-md-6">
        <ul>
          <li>Acceso ilimitado a más de 300 clases</li>
          <li>Certificación al terminar</li>
          <li>Soporte inmediato vía Skype, Chat ó E-mail</li>
          <li>Desde cualquier dispositivo, computador, tablet</li>
          <li>Descarga de todos los códigos de los proyectos</li>
          <li>Clases 100% en tu idioma, 100% español</li>
          <li>Acceso 7dias de la semana, 24hrs del día</li>
          <li>Desde cualquier lugar</li>
        </ul>
      </article>
    </section>

    <section class="duracion">
      <h2>¿Cúanto tiempo dura el diplomado y que horarios tiene?</h2>
      <article class="col-md-7 col-md-offset-1">
        <ul>
          <li>La duración total del Diplomado son 6 meses</li>
          <li>Cada nivel tiene una duración de 1 mes apróximadamente</li>
          <li>La duración puede variar según el desempeño del estudiante</li>
          <li>No tenemos horarios o restricciones de fechas</li>
          <li>Puedes acceder en el momento que desees, desde donde desees</li>
          <li>Recomendamos que seas constante para conseguir un mejor resultado</li>
        </ul>
      </article>
      <article class="col-md-3">
        <img src="<?php echo Yii::app()->request->baseUrl; ?>/images/wordpress/calendar.png">
      </article>
    </section>

    <section class="necesitas">
      <h2>¿Qué necesitas para iniciar este Diplomado?</h2>
      <article class="col-md-6">
        <img src="<?php echo Yii::app()->request->baseUrl; ?>/images/wordpress/computadores.png">
      </article>
      <article class="col-md-6">
        <ul>
          <li>Un computador con cualquier tipo de sistema operativo</li>
          <li>No necesitas ningún tipo de software en particular</li>
          <li>Conexión a Internet</li>
          <li>Ningún tipo de experiencia, te explicamos paso a paso</li>
          <li><strong>No necesitas ser experto, sólo necesitas interés</strong></li>
        </ul>
      </article>
    </section>


  <footer>
      <div class='row'>
        <div class='col-md-12'>
          <p>Powered by: Oja.la Edu Inc - © Copyright 2013 Oja.la</p>
        </div>
      </div>
    </footer>
</div>
