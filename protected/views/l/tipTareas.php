<style type="text/css">
.tips{
	margin: 0;
	padding-bottom: 20px;
	overflow: hidden;
}
footer{
	text-align: center !important;
}
footer .address{
	width: 100%;
}
.tips header{
	text-align: center;
	padding: 10px 0;
	background:rgba(0,97,191,1);
	border-bottom: 3px solid #003a73;
}
.tips .video{
	padding: 5px 0 0 0;
	margin: 0;
	text-align: center;
}
.tips .info{
	overflow: hidden;
	text-align: center;
}
.tips .info h1{
	font-weight: bold;
	font-size: 32px;
	line-height: 36px;
	margin: 30px 0;
}
.tips .info button{
    background-color: #f7941d !important;
    background: -webkit-linear-gradient(#f7941d,#d75305)!important;
    background: -moz-linear-gradient(#f7941d,#d75305)!important;
    background: -ms-linear-gradient(#f7941d,#d75305)!important;
    background: -o-linear-gradient(#f7941d,#d75305)!important;
    background: linear-gradient(#f7941d,#d75305)!important;
    box-shadow: inset 0px 1px 0px #ffbb6a,inset 0 -1px 2px #a33f03;
    text-shadow: 0px 1px 3px #521601;
    color: #fff;
    border: 1px solid #521601 !important;
    font-size: 22px;
    letter-spacing: 1px;
    font-family: Arial;
    border-radius: 5px; 
    padding: 13px 40px;  
}
</style>
<div class="tips">
	
	<header>
		<img src="<?php echo Yii::app()->request->baseUrl; ?>/images/brand-full.png">
	</header>
	
	<div class="container">
		<div class="row">
  		
	  		<div class="col-md-7 video">
	  		  <iframe src="//fast.wistia.net/embed/iframe/8zumig58sj" allowtransparency="true" frameborder="0" scrolling="no" class="wistia_embed" name="wistia_embed" allowfullscreen mozallowfullscreen webkitallowfullscreen oallowfullscreen msallowfullscreen width="670" height="377"></iframe>
        </div>
	  		
	  		<div class="col-md-4 info">
	  			<h1>Cómo pedirle a tu iPhone que organice tus tareas diarias</h1>
	  			<button id="btn-checkout" class="btn btn-warning btn-lg">Empieza tu curso ahora</button>
	  		</div>

		</div>
	</div>

</div>


<script src="https://checkout.stripe.com/checkout.js"></script>
<script>
  var handler = StripeCheckout.configure({
    key: 'pk_live_A8Ri0rI6Ux0McPCK4H6lLT24',
    image: '//d9hhrg4mnvzow.cloudfront.net/el.oja.la/cargar/1av03g-logomail.png',
    token: function(token, args) {
      $.ajax({
             'url': '<?php echo Yii::app()->urlManager->createUrl('site/crearUsuario'); ?>?email=' + token.email,
             'cache': false,
              'success': function(){
                $.ajax({
                  url: '<?php echo Yii::app()->urlManager->createUrl('site/hookLanding'); ?>',
                  type: 'POST',
                  cache: false,
                  data: { 
                      token: token.id, 
                      email: token.email,
                      plan: '<?php echo isset($_GET['plan']) ? $_GET['plan'] : '37-st' ?>'
                  },
                  success: function(data){
                      window.location = '<?php echo Yii::app()->createAbsoluteUrl("site/RedireccionLanding"); ?>?email=' + token.email;
                  }
                });
              }
       });
    }
  });

  document.getElementById('btn-checkout').addEventListener('click', function(e) {
    handler.open({
      name: '<?php echo isset($_GET['plan']) ? substr($_GET['plan'],0,2) : 37 ?> clases nuevas al mes',
      amount: <?php echo isset($_GET['plan']) ? substr($_GET['plan'],0,2) : 37 ?>00,
      currency: 'USD',  
      panelLabel: 'Tomar por',
      label: 'Iniciar Ahora',
      description: 'por USD $<?php echo isset($_GET['plan']) ? substr($_GET['plan'],0,2) : 37 ?>, incluye preguntas',
      //description: 'Plan basico mensual $<?php echo isset($_GET['plan']) ? substr($_GET['plan'],0,2) : 37 ?>',
      allowRememberMe: false,
    });
    e.preventDefault();
  });
</script>
