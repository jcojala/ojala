<div class="container diplomado-l ios">
  <div class="row fold">
    <div class="col-sm-12">
      <p class="pre">Aprende paso a paso a crear cualquier aplicación desde cero</p>
      <h1>Diplomado de desarrollo de apps para iPhone y iPad en iOS7</h1>
      <p class="post">garantizado el mejor dinero que puedes gastar en educación on-line.</p>
      <small>*No necesitas experiencia</small>
      <img src="<?php echo Yii::app()->request->baseUrl; ?>/images/iphone-app.png">
    </div>
  </div>

  <div class="row programa">
    <div class="mod-header">
      <h2>Programa del Diplomado</h2>
      <p>El programa comprende módulos teórico/prácticos y proyectos completos con códigos descargables; <br> este Diplomado se completará de forma on-line a lo largo de 12 meses.</p>
    </div>


    <div class="col-sm-3 mod1">
      <p><strong>Módulo 1</strong></p>
      <h4>Iniciación al desarrollo de iPhone apps con Objective C Básico</h4>
      <small>28 clases  ·  25 archivos</small>
    </div>


    <div class="col-sm-3 mod2">
      <p><strong>Módulo 2</strong></p>
      <h4>Para no programadores: Iníciate en el desarrollo de iPhone apps paso a paso</h4>
      <small>38 clases  ·  13 archivos</small>
    </div>


    <div class="col-sm-3 mod3">
      <p><strong>Módulo 3</strong></p>
      <h4>Crea tu primera aplicación en iOS 7 paso a paso</h4>
      <small>28 clases  ·  23 archivos</small>
    </div>


    <div class="col-sm-3 mod4">
      <p><strong>Módulo 4</strong></p>
      <h4>Crea un lector de blogs para iPhone en iOS7</h4>
      <small>23 clases  ·  19 archivos</small>
    </div>


    <div class="col-sm-3 mod5">
      <p><strong>Módulo 5 / Proyecto 1</strong></p>
      <h4>Como usar Cocoapods para mejorar tus proyectos en iOS</h4>
      <small>16 clases  ·  13 archivos</small>
    </div>


    <div class="col-sm-3 mod6">
      <p><strong>Módulo 6 / Proyecto 2</strong></p>
      <h4>Crea paso a paso una aplicación de fotos para el iPhone5</h4>
      <small>16 clases  ·  14 archivos</small>
    </div>


    <div class="col-sm-3 mod7">
      <p><strong>Módulo 7 / Proyecto 3</strong></p>
      <h4>Aprende como crear tu propio Snapchat para iOS7</h4>
      <small>27 clases  ·  22 archivos</small>
    </div>


    <div class="col-sm-3 mod8">
      <p><strong>Módulo 8 / Proyecto 4</strong></p>
      <h4>Mejorando el diseño en aplicaciones para iPhone con iOS7</h4>
      <small>18 clases  ·  15 archivos</small>
    </div>


    <div class="col-sm-3 mod9">
      <p><strong>Módulo 9 / Proyecto 5</strong></p>
      <h4>Crea tu propio Foursquare para iPhone y iPad</h4>
      <small>25 clases  ·  16 archivos</small>
    </div>


    <div class="col-sm-3 mod10">
      <p><strong>Módulo 10</strong></p>
      <h4>Aprende Origami paso a paso 100% en español y aplícalo a tus iPhone apps</h4>
      <small>12 clases  ·  9 archivos</small>
    </div>


    <div class="col-sm-3 mod11">
      <p><strong>Módulo 11 / Proyecto 6</strong></p>
      <h4>Básico de desarrollo de juegos con Sprite Kit, el framework de Apple</h4>
      <small>36 clases  ·  32 archivos</small>
    </div>


    <div class="col-sm-3 mod12">
      <p><strong>Módulo 12 / Proyecto 8</strong></p>
      <h4>Curso básico de Swift, nuevo lenguaje de programación para iPhone</h4>
      <small>18 clases  ·  17 archivos</small>
    </div>


    <h5>Módulo final: Exámen de finalización con un instructor especializado</h5>
    <a  class="btn btn-success btn-lg" href="<?php echo Yii::app()->request->baseUrl; ?>/suscripciones" >¡Inicia tu diplomado ahora!</a> 
    <small>*No necesitas experiencia</small>


  </div>

  <div class="row include">
    <div class="col-sm-12">
    <div class="mod-header">
      <h2>Recuerda el Diplomado incluye:</h2>
      <p>Todo lo que necesitas para crear cualquier tipo de app para iOS</p>
    </div>
        
      <div class="col-sm-4 mod1">
        <img alt="" src="<?php echo Yii::app()->request->baseUrl; ?>/images/downloads.png">
        <p>Acceso a más de 251 archivos descargables, disponibles todo el tiempo, guías, códigos, etc</p>
      </div>
      <div class="col-sm-4 mod2">
        <img alt="" src="<?php echo Yii::app()->request->baseUrl; ?>/images/certificado.png">
        <p>Certificación de terminación al terminar el Diplomado Validada por nuestros instructores</p>
      </div>
      <div class="col-sm-4 mod3">
        <img alt="" src="<?php echo Yii::app()->request->baseUrl; ?>/images/servicio.png">
        <p>Nunca estarás solo, sí necesitas ayuda tu asesor estará atento vía chat, mail o Skype.</p>
      </div>
    <h5>Además de forma inmediata recibes acceso a más de 200 cursos de nuestro biblioteca</h5>
    <a  class="btn btn-success btn-lg" href="<?php echo Yii::app()->request->baseUrl; ?>/suscripciones" >¡Inicia tu diplomado ahora!</a> 
    <small>*No necesitas experiencia</small>
    </div>
  </div>
  <div class="row apoyo">
    <div class="mod-header">
      <h2>¿Quién apoya Oja.la?</h2>
    </div>
    <img alt="" src="<?php echo Yii::app()->request->baseUrl; ?>/images/marcas.gif">
    <div class="row kudos">
      <div class="mod1">
        <h5>“Una atención por demás personalizada muy humana, y en total confianza, como siempre resolviendo hasta las dudas mas pequeñas. excelente atencion!</h5>
        <img alt="" src="<?php echo Yii::app()->request->baseUrl; ?>/images/avatar-estudiante.jpg">
        <p><strong>Jabama Madrid</strong></p>
        <a href="https://www.facebook.com/jabama" target="_blank">Ver post</a>
      </div>
      <div class="mod2">
        <h5>“Excelente Plataforma, excelente soporte siempre en línea y con los cambios que se vienen será mejor :D Soy programador y en el momento llevo tres cursos empece con introduccion a phonegap build server y ese me llevo a otro y otro :D. Son cosas que no sabían y me ayudan a mejorar profesionalmente."</h5>
        <img alt="" src="<?php echo Yii::app()->request->baseUrl; ?>/images/avatar-estudiante-2.jpg">
        <p><strong>Oscar Sán</strong></p>
        <a href="https://twitter.com/haleando/status/486925917340835840" target="_blank">Ver post</a>
      </div>
      <div class="mod3">
        <h5>“Lo que me gusto de Oja.la, es la excelente calidad de enseñanza que brinda en diferentes cursos, tienen un soporte técnico excelente, y que siempre tienen un curso nuevo que llama mucho la atención :)"</h5>
        <img alt="" src="https://graph.facebook.com/100000518251121/picture">
        <p><strong>Jose Antonio Pulido Murga</strong></p>
        <a href="https://www.facebook.com/OjalaLatam/posts/924296680930932" target="_blank">Ver post</a>
      </div>
      <div class="clearfix"></div>
      <div class="mod1">
        <h5>“Lo rápido y directo para resolver el problema, muy buen servicio felicitaciones."</h5>
        <img alt="" src="<?php echo Yii::app()->request->baseUrl; ?>/images/avatar-estudiante-3.jpg">
        <p><strong>Marcelo Aravena Boza</strong></p>
        <a href="https://twitter.com/marceloaravenab/status/492699238275751937" target="_blank">Ver post</a>
      </div>
      <div class="mod2">
        <h5>“atención bastante personalizada y con buenos consejos para iniciar la educación"</h5>
        <img alt="" src="<?php echo Yii::app()->request->baseUrl; ?>/images/avatar-estudiante-4.jpg">
        <p><strong>Lucio Salinas</strong></p>
        <a href="https://twitter.com/luci0s/status/482573789231054848" target="_blank">Ver post</a>
      </div>
      <div class="mod3">
        <h5>“Genial!, Respuestas oportunas e inmediatas, Recomendado"</h5>
        <img alt="" src="<?php echo Yii::app()->request->baseUrl; ?>/images/avatar-estudiante-5.jpg">
        <p><strong>Diego Fernando Lara</strong></p>
        <a href="https://twitter.com/DevFerLara/status/486252362508795904" target="_blank">Ver post</a>
      </div>
    </div>
  </div>
  <div class="row final">
    <div class="mod-header">
      <h2>¿Listo para empezar?</h2>
    </div>
    <a  class="btn btn-success btn-lg" href="<?php echo Yii::app()->request->baseUrl; ?>/suscripciones">¡Inicia tu Diplomado ahora!</a> 
    <p>No lo olvides, acceso ilimitado al Diplomado y más de 200 cursos sin que cueste más.</p>
  </div>
</div>