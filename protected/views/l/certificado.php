<!doctype html>
<html>
	<style type="text/css">
		@font-face {
			font-family:"EScriptITC";
			src: url("<?php echo Yii::app()->request->baseUrl; ?>/fonts/edwardian_script_itc.ttf") /* TTF file for CSS3 browsers */
		}

		html { 
			font: 16px/1 'Open Sans', sans-serif; 
			overflow: auto; 
			padding: 0.5in; 
		}

		html { 
			background: #999; 
			cursor: default; 
		}

		body {
			width: 11in;
			margin: 0 auto; 
			overflow: hidden; 
			height: 8.5in; 
		}

		body { 
			background: #FFF; 
			border-radius: 1px; 
			box-shadow: 0 0 1in -0.25in rgba(0, 0, 0, 0.5); 
		}

		img{
			height: 100%;
			width: 100%;
		}

		.nombre
		{
			font-family: 'EScriptITC', sans-serif;
			position: absolute;
			text-align: center;
			width: 11in;
			margin-top: 350px;
			font-size: 50px;
			color: rgb(41, 89, 134);
		}

		.curso
		{
			font-family: 'EScriptITC', sans-serif;
			position: absolute;
			text-align: center;
			width: 11in;
			margin-top: 478px;
			font-size: 32px;
		}

		.fecha1
		{
			font-family: 'EScriptITC', sans-serif;
			position: absolute;
			text-align: center;
			width: 11in;
			margin-top: 506px;
			margin-left: 166px;
			font-size: 30px;
			color: rgb(134, 134, 135);
		}

		.fecha2
		{
			position: absolute;
			text-align: center;
			width: 11in;
			margin-top: 633px;
			margin-left: -180px;
			font-size: 24px;
			font-family: serif;
			font-style: italic;
			color: rgb(41,89, 134);
		}

		.numero
		{
			position: absolute;
			text-align: center;
			width: 11in;
			margin-top: 716px;
			font-size: 16px;
			margin-left: 26px;
		}
	</style>
	<head>
		<meta charset="utf-8">
		<title>Certificado</title>
	</head>
	<body>
		<p class="nombre">Juan Carlos Robles Zambrano</p>
		<p class="curso">Iníciate en el diseño centrado en el usuario con Design Thinking</p>
		<p class="fecha1">12 de Marzo de 2014</p>
		<p class="fecha2">12-03-2014</p>
		<p class="numero">030201022014</p>
		<img alt="" src="<?php echo Yii::app()->request->baseUrl; ?>/images/certificado.jpg">
	</body>
</html>