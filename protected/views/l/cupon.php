<script type="text/javascript">
    $(document).ready(function() {
        var $header = $(".header-sticky"),
            $clone = $header.before($header.clone().addClass("clone"));
        
        $(window).on("scroll", function() {
            var fromTop = $(window).scrollTop();
            console.log(fromTop);
            $("body").toggleClass("down", (fromTop > 200));
        });
    });
</script>


	<div class='cupon' role='main'>

	<div class="header-sticky">
		<img alt="Oja.la" src="<?php echo Yii::app()->request->baseUrl; ?>/images/brand-plain.png" />
		<p>Ahorra el 50% en acceso ilimitado, con el cupón<strong>YAMISMO50</strong><a href="<?php echo Yii::app()->createUrl('site/suscripciones'); ?>" class="btn btn-success">Acceso ya mismo →</a></p>
	</div>
  
    <nav class='navbar navbar-default' role='navigation'>
      <img alt="Oja.la" src="<?php echo Yii::app()->request->baseUrl; ?>/images/brand-plain.png" />
    </nav>

		<section class="fold">
  		<div class="col-md-6 col-md-offset-1 cupon-info">
	  		<h1>Oja.la tiene más de <strong>3000</strong> clases <span>para ayudarte con tus proyectos</span></h1>
  			<p>Por tiempo limitado, ahorra el 50% en cualquier suscripción <br> con el cupón: <strong>YAMISMO50</strong></p>
  			<a href="<?php echo Yii::app()->createUrl('site/suscripciones'); ?>">Quiero acceso ya mismo</a>
  		</div>  		
  		<div class="col-md-5 img">
  			<img class="img" alt="#" src="<?php echo Yii::app()->request->baseUrl; ?>/images/mac.png" />
  		</div>
  	</section>


	<div class='container'>

  	<div class="cursos">
  		<h1>Ahorra el 50% en acceso ilimitado</h1>
  		<p>Usa el cupón YAMISMO50 y ahora el 50% instantaneamente, acceso ilimitado a TODOS los cursos.</p>

  		<div class='row'>

  			<!-- CURSO 1 -->
  			<div class='col-md-3 mod-course'>
  				<div class='thumbnail'>
  					<div class='img'>
							<a href="<?php echo Yii::app()->createUrl('site/suscripciones'); ?>" alt="Aprende las bases del HTML paso a paso para crear tu página web" class="img-rounded">
								<img alt="Aprende las bases del HTML paso a paso para crear tu página web" src="<?php echo Yii::app()->request->baseUrl; ?>/images/cupon/html5.jpg" width="262" height="149" />
							</a>
						</div>
						<div class='caption'>
							<h6>1567 personas interesadas en:</h6>
							<h4>
								<a href="<?php echo Yii::app()->createUrl('site/suscripciones'); ?>">Aprende HTML 5 y CSS3 fácil y rápido</a>
							</h4>
						</div>
  				</div>
  			</div>

  			<!-- CURSO 2 -->
  			<div class='col-md-3 mod-course'>
  				<div class='thumbnail'>
  					<div class='img'>
							<a href="<?php echo Yii::app()->createUrl('site/suscripciones'); ?>" alt="Aprende a usar responsive design en tus diseños web" class="img-rounded">
								<img alt="Aprende a usar responsive design en tus diseños web" src="<?php echo Yii::app()->request->baseUrl; ?>/images/cupon/responsive.jpg" width="262" height="149" />
							</a>
						</div>
						<div class='caption'>
							<h6>1354 personas interesadas en:</h6>
							<h4>
								<a href="<?php echo Yii::app()->createUrl('site/suscripciones'); ?>">Aprende a usar responsive design en tus diseños web</a>
							</h4>
						</div>
  				</div>
  			</div>

  			<!-- CURSO 3 -->
  			<div class='col-md-3 mod-course'>
  				<div class='thumbnail'>
  					<div class='img'>
							<a href="<?php echo Yii::app()->createUrl('site/suscripciones'); ?>" alt="Expande y optimiza tu tienda online con PHP" class="img-rounded">
								<img alt="Expande y optimiza tu tienda online con PHP" src="<?php echo Yii::app()->request->baseUrl; ?>/images/cupon/tienda.jpg" width="262" height="149" />
							</a>
						</div>
						<div class='caption'>
							<h6>2373 personas interesadas en:</h6>
							<h4>
								<a href="<?php echo Yii::app()->createUrl('site/suscripciones'); ?>">Expande y optimiza tu tienda online con PHP</a>
							</h4>
						</div>
  				</div>
  			</div>

  			<!-- CURSO 4 -->
  			<div class='col-md-3 mod-course'>
  				<div class='thumbnail'>
  					<div class='img'>
							<a href="<?php echo Yii::app()->createUrl('site/suscripciones'); ?>" alt="Aprende a mejorar tus desarrollos web siendo un ninja de Jquery" class="img-rounded">
								<img alt="Aprende a mejorar tus desarrollos web siendo un ninja de Jquery" src="<?php echo Yii::app()->request->baseUrl; ?>/images/cupon/jquery.jpg" width="262" height="149" />
							</a>
						</div>
						<div class='caption'>
							<h6>583 personas interesadas en:</h6>
							<h4>
								<a href="<?php echo Yii::app()->createUrl('site/suscripciones'); ?>">Aprende a mejorar tus desarrollos web siendo un ninja de Jquery</a>
							</h4>
						</div>
  				</div>
  			</div>

  			<!-- CURSO 1 -->
  			<div class='col-md-3 mod-course'>
  				<div class='thumbnail'>
  					<div class='img'>
							<a href="<?php echo Yii::app()->createUrl('site/suscripciones'); ?>" alt="Aprende a crear tu propia página web con Wordpress" class="img-rounded">
								<img alt="Aprende a crear tu propia página web con Wordpress" src="<?php echo Yii::app()->request->baseUrl; ?>/images/cupon/wordpress.jpg" width="262" height="149" />
							</a>
						</div>
						<div class='caption'>
							<h6>2567 personas interesadas en:</h6>
							<h4>
								<a href="<?php echo Yii::app()->createUrl('site/suscripciones'); ?>">Aprende a crear tu propia página web con Wordpress</a>
							</h4>
						</div>
  				</div>
  			</div>

  			<!-- CURSO 2 -->
  			<div class='col-md-3 mod-course'>
  				<div class='thumbnail'>
  					<div class='img'>
							<a href="<?php echo Yii::app()->createUrl('site/suscripciones'); ?>" alt="[Parte I] Aprende a crear páginas web con Joomla 3 paso a paso" class="img-rounded">
								<img alt="Aprende a crear tu propia página web con Wordpress" src="<?php echo Yii::app()->request->baseUrl; ?>/images/cupon/joomla.jpg" width="262" height="149" />
							</a>
						</div>
						<div class='caption'>
							<h6>763 personas interesadas en:</h6>
							<h4>
								<a href="<?php echo Yii::app()->createUrl('site/suscripciones'); ?>">[Parte I] Aprende a crear páginas web con Joomla 3 paso a paso</a>
							</h4>
						</div>
  				</div>
  			</div>

  			<!-- CURSO 3 -->
  			<div class='col-md-3 mod-course'>
  				<div class='thumbnail'>
  					<div class='img'>
							<a href="<?php echo Yii::app()->createUrl('site/suscripciones'); ?>" alt="Aprende las bases del HTML paso a paso para crear tu página web" class="img-rounded">
								<img alt="Aprende las bases del HTML paso a paso para crear tu página web" src="<?php echo Yii::app()->request->baseUrl; ?>/images/cupon/html52.jpg" width="262" height="149" />
							</a>
						</div>
						<div class='caption'>
							<h6>2145 personas interesadas en:</h6>
							<h4>
								<a href="<?php echo Yii::app()->createUrl('site/suscripciones'); ?>">Aprende las bases del HTML paso a paso para crear tu página web</a>
							</h4>
						</div>
  				</div>
  			</div>

  			<!-- CURSO 4 -->
  			<div class='col-md-3 mod-course'>
  				<div class='thumbnail'>
  					<div class='img'>
							<a href="<?php echo Yii::app()->createUrl('site/suscripciones'); ?>" alt="Diseña y desarrolla tu página web con Bootstrap 3" class="img-rounded">
								<img alt="Diseña y desarrolla tu página web con Bootstrap 3" src="<?php echo Yii::app()->request->baseUrl; ?>/images/cupon/bootstrap.jpg" width="262" height="149" />
							</a>
						</div>
						<div class='caption'>
							<h6>2145 personas interesadas en:</h6>
							<h4>
								<a href="<?php echo Yii::app()->createUrl('site/suscripciones'); ?>">Diseña y desarrolla tu página web con Bootstrap 3</a>
							</h4>
						</div>
  				</div>
  			</div>

  			<!-- CURSO 1 -->
  			<div class='col-md-3 mod-course'>
  				<div class='thumbnail'>
  					<div class='img'>
							<a href="<?php echo Yii::app()->createUrl('site/suscripciones'); ?>" alt="Diseño de Emails con HTML" class="img-rounded">
								<img alt="Diseño de Emails con HTML" src="<?php echo Yii::app()->request->baseUrl; ?>/images/cupon/email.jpg" width="262" height="149" />
							</a>
						</div>
						<div class='caption'>
							<h6>478 personas interesadas en:</h6>
							<h4>
								<a href="<?php echo Yii::app()->createUrl('site/suscripciones'); ?>">Diseño de Emails con HTML</a>
							</h4>
						</div>
  				</div>
  			</div>

  			<!-- CURSO 2 -->
  			<div class='col-md-3 mod-course'>
  				<div class='thumbnail'>
  					<div class='img'>
							<a href="<?php echo Yii::app()->createUrl('site/suscripciones'); ?>" alt="Construyendo plantillas para Joomla 3 con Bootstrap" class="img-rounded">
								<img alt="Construyendo plantillas para Joomla 3 con Bootstrap" src="<?php echo Yii::app()->request->baseUrl; ?>/images/cupon/joomlap.jpg" width="262" height="149" />
							</a>
						</div>
						<div class='caption'>
							<h6>3567 personas interesadas en:</h6>
							<h4>
								<a href="<?php echo Yii::app()->createUrl('site/suscripciones'); ?>">Construyendo plantillas para Joomla 3 con Bootstrap</a>
							</h4>
						</div>
  				</div>
  			</div>

  			<!-- CURSO 3 -->
  			<div class='col-md-3 mod-course'>
  				<div class='thumbnail'>
  					<div class='img'>
							<a href="<?php echo Yii::app()->createUrl('site/suscripciones'); ?>" alt="Aprende a programar en 30 días paso a paso, con Ruby on Rails" class="img-rounded">
								<img alt="Aprende a programar en 30 días paso a paso, con Ruby on Rails" src="<?php echo Yii::app()->request->baseUrl; ?>/images/cupon/ror.jpg" width="262" height="149" />
							</a>
						</div>
						<div class='caption'>
							<h6>3567 personas interesadas en:</h6>
							<h4>
								<a href="<?php echo Yii::app()->createUrl('site/suscripciones'); ?>">Aprende a programar en 30 días paso a paso, con Ruby on Rails</a>
							</h4>
						</div>
  				</div>
  			</div>

  			<!-- CURSO 4 -->
  			<div class='col-md-3 mod-course'>
  				<div class='thumbnail'>
  					<div class='img'>
							<a href="<?php echo Yii::app()->createUrl('site/suscripciones'); ?>" alt="Aprende paso a paso a desarrollar aplicaciones para Facebook" class="img-rounded">
								<img alt="Aprende paso a paso a desarrollar aplicaciones para Facebook" src="<?php echo Yii::app()->request->baseUrl; ?>/images/cupon/facebook.jpg" width="262" height="149" />
							</a>
						</div>
						<div class='caption'>
							<h6>3567 personas interesadas en:</h6>
							<h4>
								<a href="<?php echo Yii::app()->createUrl('site/suscripciones'); ?>">Aprende paso a paso a desarrollar aplicaciones para Facebook</a>
							</h4>
						</div>
  				</div>
  			</div>

  			<!-- CURSO 1 -->
  			<div class='col-md-3 mod-course'>
  				<div class='thumbnail'>
  					<div class='img'>
							<a href="<?php echo Yii::app()->createUrl('site/suscripciones'); ?>" alt="Crea tu primera aplicación en iOS 7 paso a paso" class="img-rounded">
								<img alt="Crea tu primera aplicación en iOS 7 paso a paso" src="https://oja.la/images/covers/simple-app.jpg" />
							</a>
						</div>
						<div class='caption'>
							<h6>1643 personas interesadas en:</h6>
							<h4>
								<a href="<?php echo Yii::app()->createUrl('site/suscripciones'); ?>">Crea tu primera aplicación en iOS 7 paso a paso</a>
							</h4>
						</div>
  				</div>
  			</div>

  			<!-- CURSO 2 -->
  			<div class='col-md-3 mod-course'>
  				<div class='thumbnail'>
  					<div class='img'>
							<a href="<?php echo Yii::app()->createUrl('site/suscripciones'); ?>" alt="Aprende a construir un plan de mercadeo" class="img-rounded">
								<img alt="Aprende a construir un plan de mercadeo" src="https://oja.la/images/covers/mercadeo.jpg" />
							</a>
						</div>
						<div class='caption'>
							<h6>431 personas interesadas en:</h6>
							<h4>
								<a href="<?php echo Yii::app()->createUrl('site/suscripciones'); ?>">Aprende a construir un plan de mercadeo</a>
							</h4>
						</div>
  				</div>
  			</div>

  			<!-- CURSO 3 -->
  			<div class='col-md-3 mod-course'>
  				<div class='thumbnail'>
  					<div class='img'>
							<a href="<?php echo Yii::app()->createUrl('site/suscripciones'); ?>" alt="Aprende a planear tu propia tienda en línea con Wordpress" class="img-rounded">
								<img alt="Aprende a planear tu propia tienda en línea con Wordpress" src="https://oja.la/images/covers/ecommerce.jpg" />
							</a>
						</div>
						<div class='caption'>
							<h6>2351 personas interesadas en:</h6>
							<h4>
								<a href="<?php echo Yii::app()->createUrl('site/suscripciones'); ?>">Aprende a planear tu propia tienda en línea con Wordpress</a>
							</h4>
						</div>
  				</div>
  			</div>

  			<!-- CURSO 4 -->
  			<div class='col-md-3 mod-course'>
  				<div class='thumbnail'>
  					<div class='img'>
							<a href="<?php echo Yii::app()->createUrl('site/suscripciones'); ?>" alt="Desarrolla aplicaciones para todos los dispositivos móviles con Phonegap" class="img-rounded">
								<img alt="Desarrolla aplicaciones para todos los dispositivos móviles con Phonegap" src="https://oja.la/images/covers/52167fa80eb1aed98e00046e.jpg" />
							</a>
						</div>
						<div class='caption'>
							<h6>2562 personas interesadas en:</h6>
							<h4>
								<a href="<?php echo Yii::app()->createUrl('site/suscripciones'); ?>">Desarrolla aplicaciones para todos los dispositivos móviles con Phonegap</a>
							</h4>
						</div>
  				</div>
  			</div>

  			<!-- CURSO 1 -->
  			<div class='col-md-3 mod-course'>
  				<div class='thumbnail'>
  					<div class='img'>
							<a href="<?php echo Yii::app()->createUrl('site/suscripciones'); ?>" alt="Para no programadores: Iníciate en el desarrollo de iPhone apps paso a paso" class="img-rounded">
								<img alt="Para no programadores: Iníciate en el desarrollo de iPhone apps paso a paso" src="https://oja.la/images/covers/52571a6a0eb1ae5b10000993.jpg" />
							</a>
						</div>
						<div class='caption'>
							<h6>1432 personas interesadas en:</h6>
							<h4>
								<a href="<?php echo Yii::app()->createUrl('site/suscripciones'); ?>">Para no programadores: Iníciate en el desarrollo de iPhone apps paso a paso</a>
							</h4>
						</div>
  				</div>
  			</div>

  			<!-- CURSO 2 -->
  			<div class='col-md-3 mod-course'>
  				<div class='thumbnail'>
  					<div class='img'>
							<a href="<?php echo Yii::app()->createUrl('site/suscripciones'); ?>" alt="Como crear revistas y catálogos digitales para el ipad" class="img-rounded">
								<img alt="Como crear revistas y catálogos digitales para el ipad" src="https://oja.la/images/covers/5237d3110eb1aeda26000ca2.jpg" />
							</a>
						</div>
						<div class='caption'>
							<h6>1429 personas interesadas en:</h6>
							<h4>
								<a href="<?php echo Yii::app()->createUrl('site/suscripciones'); ?>">Como crear revistas y catálogos digitales para el ipad</a>
							</h4>
						</div>
  				</div>
  			</div>

  			<!-- CURSO 3 -->
  			<div class='col-md-3 mod-course'>
  				<div class='thumbnail'>
  					<div class='img'>
							<a href="<?php echo Yii::app()->createUrl('site/suscripciones'); ?>" alt="Desarrolla tu propia aplicación para Android paso a paso hasta publicarla en el Google Play" class="img-rounded">
								<img alt="Desarrolla tu propia aplicación para Android paso a paso hasta publicarla en el Google Play" src="https://oja.la/images/covers/522ff5da0eb1ae307b000060.jpg" />
							</a>
						</div>
						<div class='caption'>
							<h6>3567 personas interesadas en:</h6>
							<h4>
								<a href="<?php echo Yii::app()->createUrl('site/suscripciones'); ?>">Desarrolla tu propia aplicación para Android paso a paso hasta publicarla en el Google Play</a>
							</h4>
						</div>
  				</div>
  			</div>

  			<!-- CURSO 4 -->
  			<div class='col-md-3 mod-course'>
  				<div class='thumbnail'>
  					<div class='img'>
							<a href="<?php echo Yii::app()->createUrl('site/suscripciones'); ?>" alt="Aprende a usar Facebook en tu negocio o empresa" class="img-rounded">
								<img alt="Aprende a usar Facebook en tu negocio o empresa" src="https://oja.la/images/covers/51cb63ad0eb1aef19f00018a.jpg" />
							</a>
						</div>
						<div class='caption'>
							<h6>1657 personas interesadas en:</h6>
							<h4>
								<a href="<?php echo Yii::app()->createUrl('site/suscripciones'); ?>">Aprende a usar Facebook en tu negocio o empresa</a>
							</h4>
						</div>
  				</div>
  			</div>

  			<!-- CURSO 1 -->
  			<div class='col-md-3 mod-course'>
  				<div class='thumbnail'>
  					<div class='img'>
							<a href="<?php echo Yii::app()->createUrl('site/suscripciones'); ?>" alt="Aprende Microsoft PowerPoint 2010 paso a paso" class="img-rounded">
								<img alt="Aprende Microsoft PowerPoint 2010 paso a paso" src="https://oja.la/images/covers/522e03280eb1aeeb7b000294.jpg" />
							</a>
						</div>
						<div class='caption'>
							<h6>3045 personas interesadas en:</h6>
							<h4>
								<a href="<?php echo Yii::app()->createUrl('site/suscripciones'); ?>">Aprende Microsoft PowerPoint 2010 paso a paso</a>
							</h4>
						</div>
  				</div>
  			</div>

  			<!-- CURSO 2 -->
  			<div class='col-md-3 mod-course'>
  				<div class='thumbnail'>
  					<div class='img'>
							<a href="<?php echo Yii::app()->createUrl('site/suscripciones'); ?>" alt="Aprende a construir el nombre y marca de tu negocio" class="img-rounded">
								<img alt="Aprende a construir el nombre y marca de tu negocio" src="https://oja.la/images/covers/50456f8a0eb1ae0e0c003157.jpg" />
							</a>
						</div>
						<div class='caption'>
							<h6>729 personas interesadas en:</h6>
							<h4>
								<a href="<?php echo Yii::app()->createUrl('site/suscripciones'); ?>">Aprende a construir el nombre y marca de tu negocio</a>
							</h4>
						</div>
  				</div>
  			</div>

  			<!-- CURSO 3 -->
  			<div class='col-md-3 mod-course'>
  				<div class='thumbnail'>
  					<div class='img'>
							<a href="<?php echo Yii::app()->createUrl('site/suscripciones'); ?>" alt="Curso clave de estrategias de marketing en Internet" class="img-rounded">
								<img alt="Curso clave de estrategias de marketing en Internet" src="https://oja.la/images/covers/502b5f910eb1ae22d2000385.jpg" />
							</a>
						</div>
						<div class='caption'>
							<h6>2178 personas interesadas en:</h6>
							<h4>
								<a href="<?php echo Yii::app()->createUrl('site/suscripciones'); ?>">Curso clave de estrategias de marketing en Internet</a>
							</h4>
						</div>
  				</div>
  			</div>

  			<!-- CURSO 4 -->
  			<div class='col-md-3 mod-course'>
  				<div class='thumbnail'>
  					<div class='img'>
							<a href="<?php echo Yii::app()->createUrl('site/suscripciones'); ?>" alt="Aprende a usar photoshop CS6 paso a paso desde 0" class="img-rounded">
								<img alt="Aprende a usar photoshop CS6 paso a paso desde 0" src="https://oja.la/images/covers/502ad0e60eb1ae1dd5000113.jpg" />
							</a>
						</div>
						<div class='caption'>
							<h6>569 personas interesadas en:</h6>
							<h4>
								<a href="<?php echo Yii::app()->createUrl('site/suscripciones'); ?>">Aprende a usar photoshop CS6 paso a paso desde 0</a>
							</h4>
						</div>
  				</div>
  			</div>

  			<!-- CURSO 1 -->
  			<div class='col-md-3 mod-course'>
  				<div class='thumbnail'>
  					<div class='img'>
							<a href="<?php echo Yii::app()->createUrl('site/suscripciones'); ?>" alt="Cómo conseguir dinero para un proyecto en Internet desde 0" class="img-rounded">
								<img alt="Cómo conseguir dinero para un proyecto en Internet desde 0" src="https://oja.la/images/covers/4e9ff4ff0eb1ae0d5a00004f.png" />
							</a>
						</div>
						<div class='caption'>
							<h6>982 personas interesadas en:</h6>
							<h4>
								<a href="<?php echo Yii::app()->createUrl('site/suscripciones'); ?>">Cómo conseguir dinero para un proyecto en Internet desde 0</a>
							</h4>
						</div>
  				</div>
  			</div>

  			<!-- CURSO 2 -->
  			<div class='col-md-3 mod-course'>
  				<div class='thumbnail'>
  					<div class='img'>
							<a href="<?php echo Yii::app()->createUrl('site/suscripciones'); ?>" alt="Pasos para llegar a ser un eficiente Community manager" class="img-rounded">
								<img alt="Pasos para llegar a ser un eficiente Community manager" src="https://oja.la/images/covers/4f17159c0eb1ae6789000064.png" />
							</a>
						</div>
						<div class='caption'>
							<h6>2451 personas interesadas en:</h6>
							<h4>
								<a href="<?php echo Yii::app()->createUrl('site/suscripciones'); ?>">Pasos para llegar a ser un eficiente Community manager</a>
							</h4>
						</div>
  				</div>
  			</div>

  			<!-- CURSO 3 -->
  			<div class='col-md-3 mod-course'>
  				<div class='thumbnail'>
  					<div class='img'>
							<a href="<?php echo Yii::app()->createUrl('site/suscripciones'); ?>" alt="Desarrollo básico de aplicaciones para iPhone / iPad" class="img-rounded">
								<img alt="Desarrollo básico de aplicaciones para iPhone / iPad" src="https://oja.la/images/covers/4f30caf00eb1ae469900048e.png" />
							</a>
						</div>
						<div class='caption'>
							<h6>536 personas interesadas en:</h6>
							<h4>
								<a href="<?php echo Yii::app()->createUrl('site/suscripciones'); ?>">Desarrollo básico de aplicaciones para iPhone / iPad</a>
							</h4>
						</div>
  				</div>
  			</div>

  			<!-- CURSO 4 -->
  			<div class='col-md-3 mod-course'>
  				<div class='thumbnail'>
  					<div class='img'>
							<a href="<?php echo Yii::app()->createUrl('site/suscripciones'); ?>" alt="Aprende Excel, ahorra tiempo y dinero" class="img-rounded">
								<img alt="Aprende Excel, ahorra tiempo y dinero" src="https://oja.la/images/covers/50070ab40eb1ae76fc000eb5.png" />
							</a>
						</div>
						<div class='caption'>
							<h6>1352 personas interesadas en:</h6>
							<h4>
								<a href="<?php echo Yii::app()->createUrl('site/suscripciones'); ?>">Aprende Excel, ahorra tiempo y dinero</a>
							</h4>
						</div>
  				</div>
  			</div>


  			<!-- CURSO 1 -->
  			<div class='col-md-3 mod-course'>
  				<div class='thumbnail'>
  					<div class='img'>
							<a href="<?php echo Yii::app()->createUrl('site/suscripciones'); ?>" alt="Aprende Word y crea cualquier documento que necesites para tu empresa" class="img-rounded">
								<img alt="Aprende Word y crea cualquier documento que necesites para tu empresa" src="https://oja.la/images/covers/503540ce0eb1ae6351000229.jpg" />
							</a>
						</div>
						<div class='caption'>
							<h6>486 personas interesadas en:</h6>
							<h4>
								<a href="<?php echo Yii::app()->createUrl('site/suscripciones'); ?>">Aprende Word y crea cualquier documento que necesites para tu empresa</a>
							</h4>
						</div>
  				</div>
  			</div>

  			<!-- CURSO 2 -->
  			<div class='col-md-3 mod-course'>
  				<div class='thumbnail'>
  					<div class='img'>
							<a href="<?php echo Yii::app()->createUrl('site/suscripciones'); ?>" alt="Entiende cómo medir los resultados de tu trabajo con Social Media" class="img-rounded">
								<img alt="Entiende cómo medir los resultados de tu trabajo con Social Media" src="https://oja.la/images/covers/5032bd260eb1ae49540003ba.jpg" />
							</a>
						</div>
						<div class='caption'>
							<h6>730 personas interesadas en:</h6>
							<h4>
								<a href="<?php echo Yii::app()->createUrl('site/suscripciones'); ?>">Entiende cómo medir los resultados de tu trabajo con Social Media</a>
							</h4>
						</div>
  				</div>
  			</div>

  			<!-- CURSO 3 -->
  			<div class='col-md-3 mod-course'>
  				<div class='thumbnail'>
  					<div class='img'>
							<a href="<?php echo Yii::app()->createUrl('site/suscripciones'); ?>" alt="Cómo crear tu currículum vitae y lograr el trabajo que siempre has querido" class="img-rounded">
								<img alt="Cómo crear tu currículum vitae y lograr el trabajo que siempre has querido" src="https://oja.la/images/covers/5031a3dc0eb1ae68a7000392.jpg" />
							</a>
						</div>
						<div class='caption'>
							<h6>2541 personas interesadas en:</h6>
							<h4>
								<a href="<?php echo Yii::app()->createUrl('site/suscripciones'); ?>">Cómo crear tu currículum vitae y lograr el trabajo que siempre has querido</a>
							</h4>
						</div>
  				</div>
  			</div>

  			<!-- CURSO 4 -->
  			<div class='col-md-3 mod-course'>
  				<div class='thumbnail'>
  					<div class='img'>
							<a href="<?php echo Yii::app()->createUrl('site/suscripciones'); ?>" alt="Google Docs y cómo usarlo en tus proyectos" class="img-rounded">
								<img alt="Google Docs y cómo usarlo en tus proyectos" src="https://oja.la/images/covers/5023df1f0eb1ae70260000fd.png" />
							</a>
						</div>
						<div class='caption'>
							<h6>539 personas interesadas en:</h6>
							<h4>
								<a href="<?php echo Yii::app()->createUrl('site/suscripciones'); ?>">Google Docs y cómo usarlo en tus proyectos</a>
							</h4>
						</div>
  				</div>
  			</div>

  			<!-- CURSO 1 -->
  			<div class='col-md-3 mod-course'>
  				<div class='thumbnail'>
  					<div class='img'>
							<a href="<?php echo Yii::app()->createUrl('site/suscripciones'); ?>" alt="Aprende a mejorar la gestión de tus proyectos" class="img-rounded">
								<img alt="Aprende a mejorar la gestión de tus proyectos" src="https://oja.la/images/covers/503ec94f0eb1ae143a0001cf.jpg" />
							</a>
						</div>
						<div class='caption'>
							<h6>2765 personas interesadas en:</h6>
							<h4>
								<a href="<?php echo Yii::app()->createUrl('site/suscripciones'); ?>">Aprende a mejorar la gestión de tus proyectos</a>
							</h4>
						</div>
  				</div>
  			</div>

  			<!-- CURSO 2 -->
  			<div class='col-md-3 mod-course'>
  				<div class='thumbnail'>
  					<div class='img'>
							<a href="<?php echo Yii::app()->createUrl('site/suscripciones'); ?>" alt="Aprende de mercadeo por correo electrónico paso a paso" class="img-rounded">
								<img alt="Aprende de mercadeo por correo electrónico paso a paso" src="https://oja.la/images/covers/505033780eb1ae4a3400031d.jpg" />
							</a>
						</div>
						<div class='caption'>
							<h6>4351 personas interesadas en:</h6>
							<h4>
								<a href="<?php echo Yii::app()->createUrl('site/suscripciones'); ?>">Aprende de mercadeo por correo electrónico paso a paso</a>
							</h4>
						</div>
  				</div>
  			</div>

  			<!-- CURSO 3 -->
  			<div class='col-md-3 mod-course'>
  				<div class='thumbnail'>
  					<div class='img'>
							<a href="<?php echo Yii::app()->createUrl('site/suscripciones'); ?>" alt="Aprende a analizar quienes visitan tu página web paso a paso con Google Analytics" class="img-rounded">
								<img alt="Aprende a analizar quienes visitan tu página web paso a paso con Google Analytics" src="https://oja.la/images/covers/5063d9d00eb1ae73d3000f5b.jpg" />
							</a>
						</div>
						<div class='caption'>
							<h6>619 personas interesadas en:</h6>
							<h4>
								<a href="<?php echo Yii::app()->createUrl('site/suscripciones'); ?>">Aprende a analizar quienes visitan tu página web paso a paso con Google Analytics</a>
							</h4>
						</div>
  				</div>
  			</div>

  			<!-- CURSO 4 -->
  			<div class='col-md-3 mod-course'>
  				<div class='thumbnail'>
  					<div class='img'>
							<a href="<?php echo Yii::app()->createUrl('site/suscripciones'); ?>" alt="Aprende cómo mejorar tu trabajo actual y administrar tu carrera profesional" class="img-rounded">
								<img alt="Aprende cómo mejorar tu trabajo actual y administrar tu carrera profesional" src="https://oja.la/images/covers/50515e860eb1ae764d0001e8.jpg" />
							</a>
						</div>
						<div class='caption'>
							<h6>143 personas interesadas en:</h6>
							<h4>
								<a href="<?php echo Yii::app()->createUrl('site/suscripciones'); ?>">Aprende cómo mejorar tu trabajo actual y administrar tu carrera profesional</a>
							</h4>
						</div>
  				</div>
  			</div>
  		</div>
  	</div>
	</div>

	<footer>
    <div class='row'>
      <div class='col-md-12'>
        <p>Powered by: Oja.la Edu Inc - © Copyright 2013 Oja.la</p>
      </div>
    </div>
  </footer>
</div>
