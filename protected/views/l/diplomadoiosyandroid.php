<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/jquery.countdown.js"></script>
<script type="text/javascript">
    $(document).ready(function() {
        var $header = $(".header-sticky"),
            $clone = $header.before($header.clone().addClass("clone"));
        
        $(window).on("scroll", function() {
            var fromTop = $(window).scrollTop();
            console.log(fromTop);
            $("body").toggleClass("down", (fromTop > 200));
        });

        var endDate = "May 11, 2014 23:59:59";  
        $('.countdown.simple').countdown({ date: endDate });
        $('.countdown.styled').countdown({
          date: endDate,
          render: function(data) {
            $(this.el).html("<div>" + this.leadingZeros(data.days, 2) + " <span>Días</span></div><div>" + this.leadingZeros(data.hours, 2) + " <span>hrs</span></div><div>" + this.leadingZeros(data.min, 2) + " <span>min</span></div><div>" + this.leadingZeros(data.sec, 2) + " <span>seg</span></div>");
          }
        });
    });
</script>



<div class="landing-diplomado">

	<nav class='navbar navbar-default' role='navigation'>
		<img alt="Oja.la" src="<?php echo Yii::app()->request->baseUrl; ?>/images/brand-full.png" />
	</nav>

	<div class="container-fluid fold">
		<h1 itemprop="name">Conviertete en un creador de apps para iPhone, iPad, y Android desde 0</h1>
		<p>Diplomado completo - 100% en español - Certificación al completar el curso</p>
		<div class="buttons">
			<p>Esta promoción expira en:</p>
			<div class="countdown styled"></div>	
			<a href="https://oja.la/pagoSuscripcion?plan=37-st" class="btn btn-success btn-block btn-lg">Toma este diplomado ahora por solo USD$37</a>
		</div>
	</div>




	<div class="container">
		<div class="row">

			<!-- LEFT BAR -->
			<div class="col-md-3 left">

				<!-- Descripción del diplomado -->
				<div class="mod descripcion">
					<h4>Sobre este diplomado</h4>
					<div class="promo-video">
					<a href="#" data-toggle="modal" data-target=".bs-example-modal-lg"><img src="<?php echo Yii::app()->request->baseUrl; ?>/images/video-landing.png"></a>
						<div class="modal fade bs-example-modal-lg" id="video" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  						<div class="modal-dialog modal-lg">
    						<div class="modal-content">
    							<div class="modal-header" style="border:0px;height:40px;">
        						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
      						</div>
      					<iframe style="margin:0 20px 20px 20px;" src="//fast.wistia.net/embed/iframe/nuucgo75o7" allowtransparency="true" frameborder="0" scrolling="no" class="wistia_embed" name="wistia_embed" allowfullscreen mozallowfullscreen webkitallowfullscreen oallowfullscreen msallowfullscreen width="800" height="450"></iframe>
						    </div>
						  </div>
						</div>
				</div>

					<dl>
						<dt><span class="glyphicon glyphicon-signal"></span>Nivel:</dt>
						<dd>Básico a Avanzado</dd>
					</dl>
					<dl>
						<dt><span class="glyphicon glyphicon-time"></span>Duración:</dt>
						<dd>42hrs 32mins en video</dd>
					</dl>
					<dl>
						<dt><span class="glyphicon glyphicon-tag"></span>Categoría:</dt>
						<dd>Desarrollo móvil</dd>
					</dl>
					<dl>
						<dt><span class="glyphicon glyphicon glyphicon-info-sign"></span>Clases:</dt>
						<dd>11 Módulos - 306 Clases</dd>
					</dl>
				</div>
				<!-- **** -->
				<?php $this->widget('ShareThis'); ?>

				<!-- Mod de requisitos -->
				<div class="mod">
					<h4>¿Cuáles son los requisitos?</h4>
					<p>Para desarrollar aplicaciones para iOS necesitas un computador puede ser Win o Mac (Preferiblemente Mac), disposición y tiempo para desarrollar todo el programa.</p>
				</div>
				<!-- *** -->

				<!-- Mod que vas a lograr -->
				<div class="mod">
					<h4>¿Qué voy a aprender?</h4>
					<p>Aprenderás como desarrollar cualquier tipo de aplicación para iOS y Android, desde la más simple hasta la más compleja, el límite está en tus ideas.</p>
				</div>
				<!-- *** -->

				<!-- Mod para quien -->
				<div class="mod">
					<h4>¿A quién va dirigido?</h4>
					<p>Este diploamdo está dirigido a todo tipo de personas que tengan una idea y la quieran llevar a cabo; Este diplomado lo pueden tomar personas con o sin experiencia previa en programación.</p>
				</div>
				<!-- *** -->

				<div class="mod recibes">
<h4>¿Qué recibirás?</h4>
					<div class="each">
						<img alt="Faq-videos" src="<?php echo Yii::app()->request->baseUrl; ?>/images/faq-videos-b547ed0a34cfedacc1b4e3c229eab249.png">
						<strong>Videocursos paso a paso</strong>
						<p>Contenido 100% Online, videos de alta calidad, que puedes ver cuantas veces quieras a la hora que prefieras.</p>
					</div>
					<div class="each">
						<img alt="Faq-expire" src="<?php echo Yii::app()->request->baseUrl; ?>/images/faq-expire-dafebe946557b41ffedc11867b329f34.png">
						<strong>Acceso ilimitado</strong>
						<p>Tus cursos son tuyos, nunca expirarán, disponibles las 24hrs del día sin restricciones de ningún tipo.</p>
					</div>
					<div class="each">
						<img alt="Faq-dispositivos" src="<?php echo Yii::app()->request->baseUrl; ?>/images/faq-dispositivos-192843e5e8532ec4e879c88865338260.png">
						<strong>Disponibilidad total</strong>
						<p>Puedes tomar tus cursos desde cualquier dispositivo, teléfono, tablet, o computador, incluso desde varios a la vez.</p>
					</div>
					<div class="each">
						<img alt="Faq-certificate" src="<?php echo Yii::app()->request->baseUrl; ?>/images/faq-certificate-0445ea4296df636f0915d64816fe66f0.png">
						<strong>Certificación al terminar</strong>
						<p>Cuando completes el 100% de tu curso, el sistema te dará acceso a tu certificación para que la descargues e imprimas.</p>
					</div>
				</div>

<div class="mod questions">
			<h4>¿Alguna pregunta?</h4>
			<p>
				Escríbenos a
				<a href="mailto:fr@oja.la?subject=Tengo%20una%20pregunta%20sobre%20este%20diplomado">fr@oja.la</a>
				y revisaremos lo que sea q haga falta, preguntas sobre el diplomado, sobre Oja.la, sobre lo que sea que haga falta, incluso un simple hola sería perfecto ;).
			</p>
		</div>

			</div>
			<!-- *** -->



			<!--  -->
			<div class="col-md-6 center">
				<div class="mod programa">
					<h4>Modulos del Diplomado</h4>
					
					<div class="mod-diplomado">
						<div class="title">Modulo I</div>
						<h3>Iníciate en el desarrollo de iPhone apps</h3>
						<p>El desarrollo de iOS es realmente más sencillo de lo que piensas, incluso si eres o no un programador experimentado. En este modulo, aprenderás los conceptos más importantes de iOS de forma sencilla y al grano, explicando el proceso de desarrollo de una manera simple, para que cualquier persona lo pueda entender.</p>
						<div class="details">
							<dl>
								<dt><span class="glyphicon glyphicon-signal"></span>Nivel:</dt>
								<dd>Principiante</dd>
							</dl>
							<dl>
								<dt><span class="glyphicon glyphicon glyphicon-info-sign"></span>Clases:</dt>
								<dd>38 Clases</dd>
							</dl>
						</div>
						
						<h6><strong>Temas tratados en este módulo:</strong></h6>
						<ul>
							<li>Conoce Xcode y su interfaz.</li>
							<li>Aprende a usar el emulador de dispositivos.</li>
							<li>Conoce las diferentes Variables y funciones disponibles.</li>
							<li>Reconoce como crear y definir sentencias condicionales.</li>
							<li>Crea tu primera aplicación.</li>
						</ul>
					</div>

					<div class="mod-diplomado">
						<div class="title">Modulo II</div>
						<h3>Conoce paso a paso Objective-C el lenguaje de programación para iPhone apps</h3>
						<p>Objective-C es la mejor opción para los desarrolladores que quieren desarrollar para iOS y OS X aplicaciones. Con este módulo tendrás las herramientas que necesarias, para desarrollar cualquier app para iPhone y conseguir tu primer código funcionando.</p>
						<div class="details">
							<dl>
								<dt><span class="glyphicon glyphicon-signal"></span>Nivel:</dt>
								<dd>Principiante</dd>
							</dl>
							<dl>
								<dt><span class="glyphicon glyphicon glyphicon-info-sign"></span>Clases:</dt>
								<dd>28 Clases</dd>
							</dl>
						</div>
						
						<h6><strong>Temas tratados en este módulo:</strong></h6>
						<ul>
							<li>Fundamentos y báses de C.</li>
							<li>Programación básica en C.</li>
							<li>Introducción y primeros pasos a Objective-C.</li>
							<li>Reconoce el framework sobre el lenguaje.</li>
							<li>Funciones complejas en Onjective-C.</li>
						</ul>
					</div>

					<div class="mod-diplomado">
						<div class="title">Modulo III</div>
						<h3>Tu primera aplicación simple en iOS 7</h3>
						<p>La construcción de esta aplicación sencilla te enseñará cómo combinar el lenguaje Objective-C y las herramientas de Xcode5. También aprenderás los conceptos fundamentales, con un patrón de diseño llamado Modelo-Vista-Controlador. aprenderás cómo conectar vistas y controles para codificar, crear animaciones, responder al tacto y movimiento del teléfono y como estos afectan tu aplicación.</p>
						<div class="details">
							<dl>
								<dt><span class="glyphicon glyphicon-signal"></span>Nivel:</dt>
								<dd>Principiante</dd>
							</dl>
							<dl>
								<dt><span class="glyphicon glyphicon glyphicon-info-sign"></span>Clases:</dt>
								<dd>28 Clases</dd>
							</dl>
						</div>
						
						<h6><strong>Temas tratados en este módulo:</strong></h6>
						<ul>
							<li>Uso de la interfaz de Xcode5 para tu app.</li>
							<li>MVC con Xcode, Modelo-Vista-Controlador</li>
							<li>Planeando un diseño de aplicación.</li>
							<li>Animando e interpretando eventos.</li>
							<li>Implementando iconos e imagenes de lanzamiento.</li>
						</ul>
					</div>					

					<div class="mod-diplomado">
						<div class="title">Modulo IV</div>
						<h3>Crea un lector de blogs para iPhone en iOS7</h3>
						<p>En este modulo aprenderás una de las necesidades más comunes al crear una aplicación: la descarga de datos desde Internet y mostrar esa información en una lista dentro de tu app. Vamos pedir los datos de la web, analizar y utilizar la información en formato JSON. También veremos cómo mostrar una página web dentro de nuestra aplicación.</p>
						<div class="details">
							<dl>
								<dt><span class="glyphicon glyphicon-signal"></span>Nivel:</dt>
								<dd>Intermedio</dd>
							</dl>
							<dl>
								<dt><span class="glyphicon glyphicon glyphicon-info-sign"></span>Clases:</dt>
								<dd>23 Clases</dd>
							</dl>
						</div>
						
						<h6><strong>Temas tratados en este módulo:</strong></h6>
						<ul>
							<li>Aprendiendo a usar plantillas master como MasterDetail.</li>
							<li>Iniciando el trabajo y referencia al TableView.</li>
							<li>Conociendo los NSDictionary.</li>
							<li>Parseando datos en JSON.</li>
							<li>Creando modelos de datos para apps.</li>
							<li>Modificar data de JSON para aplicarla en vistas.</li>
							<li>Implementando los NavigationController y UIWebView.</li>
						</ul>
					</div>

					<div class="mod-diplomado">
						<div class="title">Modulo V</div>
						<h3>Crear una app de mensajes que se "destruyen" en 10segs</h3>
						<p>Aplicarás los conceptos aprendidos en proyectos anteriores de iOS para crear una aplicación que permitirá a los usuarios enviar mensajes de fotos o videos a otros usuarios que se eliminarán una vez vean su contenido.</p>
						<div class="details">
							<dl>
								<dt><span class="glyphicon glyphicon-signal"></span>Nivel:</dt>
								<dd>Experto</dd>
							</dl>
							<dl>
								<dt><span class="glyphicon glyphicon glyphicon-info-sign"></span>Clases:</dt>
								<dd>27 Clases</dd>
							</dl>
						</div>
						
						<h6><strong>Temas tratados en este módulo:</strong></h6>
						<ul>
							<li>La aplicación estará basada en pestañas con UITabBarController.</li>
							<li>Profundizarémos en Storyboards, Segues, Table Views</li>
							<li>Trabajarémos con View Controller lifecycle.</li>
							<li>UIImagePickerController estándar para capturar fotos o videos.</li>
							<li>Usarémos MPMoviePlayerController del marco MediaPlayer.</li>
							<li>Back-end y almacenamiento en la nube proveido por Parse.com</li>
						</ul>
					</div>

					<div class="mod-diplomado">
						<div class="title">Modulo VI</div>
						<h3>Desarrollo de juegos con Cocos2D</h3>
						<p>En este módulo verás Cocos2D, el framework para desarrollar juegos y conocerás las clases fundamentales necesarias para la creación de un juego. También muestra cómo incorporar capacidades multi-touch, personalizar la apariencia de un juego, el uso e Implementando spritessheets en Cocos2D y todas las funcionalidades que Cocos2D tiene para el desarrollo de juegos.</p>
						<div class="details">
							<dl>
								<dt><span class="glyphicon glyphicon-signal"></span>Nivel:</dt>
								<dd>Experto</dd>
							</dl>
							<dl>
								<dt><span class="glyphicon glyphicon glyphicon-info-sign"></span>Clases:</dt>
								<dd>24 Clases</dd>
							</dl>
						</div>
						
						<h6><strong>Temas tratados en este módulo:</strong></h6>
						<ul>
							<li>Inatalación e implementación de Cocos2D.</li>
							<li>Stripes y uso de imagenes en Cocos2D.</li>
							<li>Uso e implementación de opciones TouchScreen.</li>
							<li>Grupos, escenas y capas para el desarrollo de juegos.</li>
							<li>Usando el cronómetro como parte del juego.</li>
							<li>Puntajes y tabla de ganadores.</li>
						</ul>
					</div>

					<div class="mod-diplomado">
						<div class="title">Modulo VII</div>
						<h3>Tu primera aplicación para Android</h3>
						<p>Vamos a crear aplicaciones que funcionen en Android programando en Java. Entre otras cosas, aprenderás a utilizar una serie de librerías y componentes propios de la plataforma. El desarrollo móvil es accesible a cualquier persona a través de este curso. Todos estos detalles los encontrarás en las diferentes clases. Es la oportunidad de aprender, a crear diversas aplicaciones para teléfonos de baja gama y Smartphone que funcionan con Android, el sistema operativo de Google.</p>
						<div class="details">
							<dl>
								<dt><span class="glyphicon glyphicon-signal"></span>Nivel:</dt>
								<dd>Principiante</dd>
							</dl>
							<dl>
								<dt><span class="glyphicon glyphicon glyphicon-info-sign"></span>Clases:</dt>
								<dd>16 Clases</dd>
							</dl>
						</div>
						
						<h6><strong>Temas tratados en este módulo:</strong></h6>
						<ul>
							<li>Conociendo Eclipse y conociendo el entorno de desarrollo.</li>
							<li>Adaptando el canvas de la aplicación a diferentes dispositivos.</li>
							<li>Creando animaciones con fotogramas.</li>
							<li>aplicando sonidos a la aplicación.</li>
						</ul>
					</div>

					<div class="mod-diplomado">
						<div class="title">Modulo VIII</div>
						<h3>Crea una aplicación de notas para Android</h3>
						<p>Esta aplicación sencilla te ayudará a afianzar conocimientos básicos en el desarrollo de apps para Android. Verás como trabajar la información en tu aplicación, guardarla y procesarla para servirla a traves de las vistas de la app.</p>
						<div class="details">
							<dl>
								<dt><span class="glyphicon glyphicon-signal"></span>Nivel:</dt>
								<dd>Principiante</dd>
							</dl>
							<dl>
								<dt><span class="glyphicon glyphicon glyphicon-info-sign"></span>Clases:</dt>
								<dd>26 Clases</dd>
							</dl>
						</div>
						
						<h6><strong>Temas tratados en este módulo:</strong></h6>
						<ul>
							<li>Creación de dispositivos virtuales.</li>
							<li>Optimización del rendimiento de máquinas virtuales.</li>
							<li>Definición y modeloado de datos para Android.</li>
							<li>Opteniendo e integrando ActionBarSherlock.</li>
							<li>Soportando versiones anteriores de Android.</li>
						</ul>
				</div>

<div class="mod-diplomado">
						<div class="title">Modulo IX</div>
						<h3>Lector de blogs en tus dispositivos Android</h3>
						<p>Vamos a crear una aplicación sencilla para avanzar en el desarrollo de apps para Android. Desarrollaremos una app q se conecta con contenidos en Web y los despliega en tu aplicación.</p>
						<div class="details">
							<dl>
								<dt><span class="glyphicon glyphicon-signal"></span>Nivel:</dt>
								<dd>Intermedio</dd>
							</dl>
							<dl>
								<dt><span class="glyphicon glyphicon glyphicon-info-sign"></span>Clases:</dt>
								<dd>29 Clases</dd>
							</dl>
						</div>
						
						<h6><strong>Temas tratados en este módulo:</strong></h6>
						<ul>
							<li>Trabajando con plantillas, MasterDetail.</li>
							<li>Usando el listActivity en nuestra aplicación.</li>
							<li>Entendiendo de arrays con el archivo strings.xml.</li>
							<li>Trabajando con bloques try-catch.</li>
							<li>Manipulando el contenido del blog con objetos JSON.</li>
							<li>Usando intents para mostrar y compartir publicaciones.</li>
						</ul>
					</div>

<div class="mod-diplomado">
						<div class="title">Modulo X</div>
						<h3>Aprende a guardar datos en tu aplicación sin conectarse a Internet</h3>
						<p>Lleva tus habilidades de desarrollo en Android al próximo nivel, aprendiendo cómo desarrollar aplicaciones con almacenamiento local, basado en archivos de texto y en bases de datos relacionales con SQLite, lo cual permitirá que la App pueda guardar y mostrar datos anteriormente almacenados sin necesidad de conectarte a internet.</p>
						<div class="details">
							<dl>
								<dt><span class="glyphicon glyphicon-signal"></span>Nivel:</dt>
								<dd>Experto</dd>
							</dl>
							<dl>
								<dt><span class="glyphicon glyphicon glyphicon-info-sign"></span>Clases:</dt>
								<dd>27 Clases</dd>
							</dl>
						</div>
						
						<h6><strong>Temas tratados en este módulo:</strong></h6>
						<ul>
							<li>Trabajo con oyentes y preferencias compartidas.</li>
							<li>Creando y leyendo archivos de data JSON.</li>
							<li>Analizando archivos XML para trabajar la data de app.</li>
							<li>Trabajando con bases de datos SQLite.</li>
							<li>Definiendo una base de datos con SQLiteOpenHelper.</li>
							<li>Importando data desde XML hasta SQLite.</li>
						</ul>
					</div>

<div class="mod-diplomado">
						<div class="title">Modulo XI</div>
						<h3>Desarrolla juegos para Android con Cocos2D</h3>
						<p>Descubre cómo construir un juego para Android y distribuirlo en la tienda de aplicaciones de Google, utilizando Cocos2d y Eclipse. Además, verás cómo monetizar su juego aún más con anuncios de Google Mobile. Verás cómo instalar y configurar el entorno de codificación; construir escenas del juego básicas, capas y sprites, acelerómetro de datos, crear clases basadas en un flujo de juego, y gestionar accidentes, victorias, y las puntuaciones.</p>
						<div class="details">
							<dl>
								<dt><span class="glyphicon glyphicon-signal"></span>Nivel:</dt>
								<dd>Experto</dd>
							</dl>
							<dl>
								<dt><span class="glyphicon glyphicon glyphicon-info-sign"></span>Clases:</dt>
								<dd>40 Clases</dd>
							</dl>
						</div>
						
						<h6><strong>Temas tratados en este módulo:</strong></h6>
						<ul>
							<li>Preparando eclipse para usar C++.</li>
							<li>Ajustando propiedades básicas de Sprites en Cocos2D.</li>
							<li>Trabajando y ajustando el Acelerómetro del dispositivo.</li>
							<li>Definiendo el diagrama de flujo del juego.</li>
							<li>Ajustando intervalos de tiempo en la animación del juego.</li>
						</ul>
					</div>

				</div>	
			</div>
			<!-- *** -->



			<div class="col-md-3 right">

				<div class="mod comments">
						<h4>Comentarios de estudiantes</h4>
						<div class="each-review">
							<span class="glyphicon glyphicon-star"></span>
							<span class="glyphicon glyphicon-star"></span>
							<span class="glyphicon glyphicon-star"></span>
							<span class="glyphicon glyphicon-star"></span>
							<span class="glyphicon glyphicon-star"></span>
							<p>Excelente Diplomado<span>Febrero 17 de 2014</span><small>Por: Jonathan Velazquez</small></p>
							<div class="info">
								<p>Realmente excelente manera de aprender a programar Apps .El  Diplomado tiene éxito en la enseñanza de los conceptos detrás de la construcción de ... <a href="#" data-toggle="modal" data-target="#first">Leer más</a></p>
							</div>
							<div class="modal fade" id="first" tabindex="-1" role="dialog" aria-labelledby="first" aria-hidden="true">
								<div class="modal-dialog">
									<div class="modal-content">
										<div class="modal-body">
											<p class="fecha">Febrero 17 de 2014</p>
											<h3>Excelente Diplomado</h3>
											<p class="by">Por: Jonathan Velazquez</p>
											<p class="comment">Realmente excelente manera de aprender a programar Apps .El  Diplomado tiene éxito en la enseñanza de los conceptos detrás de la construcción de Apps modificando constantemente aplicaciones y  muestra lo que sucede cuando haces cada cambio (incluso lo que sucede cuando haces algo mal).</p>
											<p class="comment">Lo principal me gustaría añadir a las otras críticas positivas es que necesitas un iPad y un Mac, y debes utilizarlos juntos . Cómo configuro mi iPad sentado junto a mi Mac , y veo el diplomado en en el ipad , escribo el código en modo de pantalla completa en Xcode en el MAC. Eso facilita la puesta a punto. La  versión de este diplomado en el iPad se ve hermoso y fácil de usar con el soporte necesario , y puedo crear una aplicación en Xcode y ejecutarla de inmediato en mi  teléfono ( con la consola mostrando el Xcode ) , y luego cambiar fácilmente de nuevo al diplomado  para seguir viendo mi clase cuando termino de probar mi aplicación . Es mucho más cómodo que hacer diplomados presenciales.</p>
											<p class="comment">Mi única crítica es que por alguna razón no  usan git con sus proyectos de Xcode , cuando en realidad es muy fácil de usar.</p>
										</div>
										<div class="modal-footer">
											<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar ventana</button>
										</div>
									</div>
								</div>
							</div>
						</div>
<div class="each-review">
							<span class="glyphicon glyphicon-star"></span>
							<span class="glyphicon glyphicon-star"></span>
							<span class="glyphicon glyphicon-star"></span>
							<span class="glyphicon glyphicon-star"></span>
							<span class="glyphicon glyphicon-star"></span>
							<p>Funciona<span>Febrero 02 de 2014</span><small>Por: Jacobo Arenas</small></p>
							<div class="info">
								<p>Soy un programador oxidado que decidió ensuciarse las manos algo nuevo. Yo había intentado con cursos en la universidad previamente, pero los encontraba ... <a href="#" data-toggle="modal" data-target="#second">Leer más</a></p>
							</div>
							<div class="modal fade" id="second" tabindex="-1" role="dialog" aria-labelledby="second" aria-hidden="true">
								<div class="modal-dialog">
									<div class="modal-content">
										<div class="modal-body">
											<p class="fecha">Febrero 02 de 2014</p>
											<h3>Funciona</h3>
											<p class="by">Por: Jacobo Arenas</p>
											<p class="comment">Soy un programador oxidado que decidió ensuciarse las manos algo nuevo. Yo había intentado con cursos en la universidad previamente, pero los encontraba muy deficientes. En la universidad no me gustaban los ejemplos y las muestras eran inútiles nada practicas, la forma de presentar los conceptos no eran intuitivo se sentía complicado. Estos cursos presenciales asumían que sabía cosas y me perdían. Unos meses más tarde , decidí probar de nuevo pero esta vez quería un diplomado en línea, vi este curso y los comentarios y decidí darle una oportunidad. Inmediatamente me gustó como empezó y me encontré mucho más comprometido. El ritmo es bueno, los ejemplos son interesantes y que hacen un gran trabajo de la presentación de los conceptos clave. Realmente aprecio el hecho de que en un corto período de tiempo, no sólo estoy aprendiendo más de cerca de la fundación de apps , sino que también estoy explorando el uso de características clave , como la ubicación y mapas. A pesar de que solía programar, yo considero que este diplomado no lo  puede empezar hasta una persona de negocios. Realmente siento que este diplomado fue el puente entre lo nuevo y algo lo real.</p>
										</div>
										<div class="modal-footer">
											<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar ventana</button>
										</div>
									</div>
								</div>
							</div>
						</div>
<div class="each-review">
							<span class="glyphicon glyphicon-star"></span>
							<span class="glyphicon glyphicon-star"></span>
							<span class="glyphicon glyphicon-star"></span>
							<span class="glyphicon glyphicon-star"></span>
							<span class="glyphicon glyphicon-star"></span>
							<p>Recomendado para personas que estan empezando ...<span>Febrero 27 de 2014</span><small>Por: Juan Zapata</small></p>
							<div class="info">
								<p>Poco a poco voy encaminado en este diplomado. Hasta el momento, el contenido ha sido fácil de... <a href="#" data-toggle="modal" data-target="#third">Leer más</a></p>
							</div>
							<div class="modal fade" id="third" tabindex="-1" role="dialog" aria-labelledby="third" aria-hidden="true">
								<div class="modal-dialog">
									<div class="modal-content">
								
										<div class="modal-body">
											<p class="fecha">Febrero 27 de 2014</p>
											<h3>Recomendado para personas que estan empezando ...</h3>
											<p class="by">Por: Juan Zapata</p>
											<p class="comment">Poco a poco voy encaminado en este diplomado. Hasta el momento, el contenido ha sido fácil de seguir y para alguien como yo, que está empezando, es la diferencia entre continuarlo y dejarlo. Creo que es un gran forma de empezar</p>
										</div>
										<div class="modal-footer">
											<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar ventana</button>
										</div>
									</div>
								</div>
							</div>
						</div>
<div class="each-review">
							<span class="glyphicon glyphicon-star"></span>
							<span class="glyphicon glyphicon-star"></span>
							<span class="glyphicon glyphicon-star"></span>
							<span class="glyphicon glyphicon-star"></span>
							<span class="glyphicon glyphicon-star"></span>
							<p>Muy recomendable<span>Febrero 13 de 2014</span><small>Por: Lourdes (California, United States)</small></p>
							<div class="info">
								<p>Muy recomendable . 5 estrellas . Si siempre has querido hacer realidad las cosas que se te ocurren , este diplomado podría ser un gran cambio en tu vida.</p>
							</div>
						</div>
<div class="each-review">
							<span class="glyphicon glyphicon-star"></span>
							<span class="glyphicon glyphicon-star"></span>
							<span class="glyphicon glyphicon-star"></span>
							<span class="glyphicon glyphicon-star"></span>
							<span class="glyphicon glyphicon-star"></span>
							<p>Mucho más claro que cualquier otra opción en internet<span>Enero 12 de 2014</span><small>Por: Esteban Medina</small></p>
							<div class="info">
								<p>He encontrado que este diplomado es  más fácil de entender que muchas opciones online que he tomado antes... <a href="#" data-toggle="modal" data-target="#five">Leer más</a></p>
							</div>
							<div class="modal fade" id="five" tabindex="-1" role="dialog" aria-labelledby="five" aria-hidden="true">
								<div class="modal-dialog">
									<div class="modal-content">
								
										<div class="modal-body">
											<p class="fecha">Enero 12 de 2014</p>
											<h3>Mucho más claro que cualquier otra opción en internet</h3>
											<p class="by">Por: Esteban Medina</p>
											<p class="comment">He encontrado que este diplomado es  más fácil  de entender que muchas opciones online que he tomado antes. Puede ser debido al hecho de que utilizan la última versión de creacion de apps  de Apple que es mas simple. Pero en general, creo que realmente se enfocaron y trataron de explicar las cosas de la manera más clara posible y al menos a través de la primera etapa del diplomado, todo ha tenido un montón de sentido. Es difícil para la gente no muy familiarizados con la programación (como yo)  captar algunos de los conceptos , pero me pareció que la las clases van lentamente y trabajar con ejemplos es indispensable para la construcción de una base sólida. Yo recomiendo este diplomado para comenzar a crear Apps</p>
										</div>
										<div class="modal-footer">
											<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar ventana</button>
										</div>
									</div>
								</div>
							</div>
						</div>
<div class="each-review">
							<span class="glyphicon glyphicon-star"></span>
							<span class="glyphicon glyphicon-star"></span>
							<span class="glyphicon glyphicon-star"></span>
							<span class="glyphicon glyphicon-star"></span>
							<span class="glyphicon glyphicon-star-empty"></span>
							<p>Recomendado para personas que estan empezando<span>Febrero 27 de 2014</span><small>Por: Juan Carlos Posada</small></p>
							<div class="info">
								<p>Poco a poco voy encaminado en este diplomado. Hasta el momento, el contenido ha sido... <a href="#" data-toggle="modal" data-target="#seven">Leer más</a></p>
							</div>
							<div class="modal fade" id="seven" tabindex="-1" role="dialog" aria-labelledby="seven" aria-hidden="true">
								<div class="modal-dialog">
									<div class="modal-content">
								
										<div class="modal-body">
											<p class="fecha">Febrero 27 de 2014</p>
											<h3>Recomendado para personas que estan empezando</h3>
											<p class="by">Por: Juan Carlos Posada</p>
											<p class="comment">Poco a poco voy encaminado en este diplomado. Hasta el momento, el contenido ha sido fácil de seguir y para alguien como yo, que está empezando, es la diferencia entre continuarlo y dejarlo. Creo que es un gran forma de empezar.</p>
										</div>
										<div class="modal-footer">
											<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar ventana</button>
										</div>
									</div>
								</div>
							</div>
						</div>
<div class="each-review">
							<span class="glyphicon glyphicon-star"></span>
							<span class="glyphicon glyphicon-star"></span>
							<span class="glyphicon glyphicon-star"></span>
							<span class="glyphicon glyphicon-star"></span>
							<span class="glyphicon glyphicon-star-empty"></span>
							<p>La mejor guía de programación iOS que he probado<span>Septiembre 28 de 2013</span><small>Por: Fernando Martinez (Puerto Rico)</small></p>
							<div class="info">
								<p>Estoy en la parte intermedia del diplomado y estoy emocionado de que esta segunda parte es tan... <a href="#" data-toggle="modal" data-target="#eight">Leer más</a></p>
							</div>
							<div class="modal fade" id="eight" tabindex="-1" role="dialog" aria-labelledby="eight" aria-hidden="true">
								<div class="modal-dialog">
									<div class="modal-content">
								
										<div class="modal-body">
											<p class="fecha">Septiembre 28 de 2013</p>
											<h3>La mejor guía de programación iOS que he probado</h3>
											<p class="by">Por: Fernando Martinez (Puerto Rico)</p>
											<p class="comment">Estoy en la parte intermedia del diplomado y estoy emocionado de que esta segunda parte es tan ilustrativa como la primera. Yo había tratado en otras fuentes, pero quedé frustrado por la cantidad de teoria sin practica. Los que trate antes del diplomado se enfocan en detalles aburridos sobre algunas cosas y se saltaban cosas que ellos pensaban que eran evidentes, pero que yo no sabia. Este diplomado es la mezcla entre la teoría pero lo más importante la práctica crear algo real, y no quedarme varado cuando no entienda a alguien si no saber que existe una persona allí en el diplomado que me va a ayudar.</p>
										</div>
										<div class="modal-footer">
											<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar ventana</button>
										</div>
									</div>
								</div>
							</div>
						</div>
<div class="each-review">
							<span class="glyphicon glyphicon-star"></span>
							<span class="glyphicon glyphicon-star"></span>
							<span class="glyphicon glyphicon-star"></span>
							<span class="glyphicon glyphicon-star"></span>
							<span class="glyphicon glyphicon-star"></span>
							<p>excelente ! <span>Enero 27 de 2014</span><small>Por: Luis F (San José, Costa Rica)</small></p>
							<div class="info">
								<p>Soy un programador , pero nuevo en todo lo de creaciones de apps Objective-C, XCode , y la programación de iOS... <a href="#" data-toggle="modal" data-target="#nine">Leer más</a></p>
							</div>
							<div class="modal fade" id="nine" tabindex="-1" role="dialog" aria-labelledby="nine" aria-hidden="true">
								<div class="modal-dialog">
									<div class="modal-content">
								
										<div class="modal-body">
											<p class="fecha">Enero 27 de 2014</p>
											<h3>excelente !</h3>
											<p class="by">Por: Luis F (San José, Costa Rica)</p>
											<p class="comment">Soy un programador , pero nuevo en todo lo de creaciones de apps Objective-C, XCode , y la programación de iOS</p>
											<p class="comment">Actualmente estoy en la parte avanzada del diplomado y este diplomado me recuerda talleres patrocinados en la empresa que eran extremadamente costosos.</p>
											<p class="comment">Empieza de una forma muy amigable , paso a paso , a la programación de apps usando Objective -C y XCode . Los ejemplos son muy bien hechos y me parece en general se puede completar mucho en aproximadamente 1-2 horas al día que se le dediquen . Al final de cada clase hay actividades para practicar lo que vas aprendiendo.</p>
											<p class="comment">En este momento creo que tengo una buena comprensión general de desarrollo Apps y me siento cómodo escribiendo código para la creacion de apps. Además en el diplomado se cubre una serie de temas importantes relacionados con la programación de iOS que probablemente me ahorrará mucho tiempo cuando llega el momento de hacer un poco de trabajo de desarrollo real.</p>
											<p class="comment">Por cierto, yo ya estaba familiarizado con más de media docena de otros lenguajes de programación , así que pude entender de Objective- C de una forma simple con el diplomado. Dependiendo de tu nivel de experiencia, es posible que tengas que dedicarle un poco mas de tiempo de lo normal para ponerte al dia.</p>
										</div>
										<div class="modal-footer">
											<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar ventana</button>
										</div>
									</div>
								</div>
							</div>
						</div>

<div class="each-review">
							<span class="glyphicon glyphicon-star"></span>
							<span class="glyphicon glyphicon-star"></span>
							<span class="glyphicon glyphicon-star"></span>
							<span class="glyphicon glyphicon-star"></span>
							<span class="glyphicon glyphicon-star"></span>
							<p>No estoy seguro que sea para principiantes pero es MUY bueno!<span>Noviembre 19 de 2013</span><small>Por: Frank (Buenos Aires, Argentina)</small></p>
							<div class="info">
								<p>Un diplomado q tiene una buena estructura y esta bien organizado pero creo... <a href="#" data-toggle="modal" data-target="#ten">Leer más</a></p>
							</div>
							<div class="modal fade" id="ten" tabindex="-1" role="dialog" aria-labelledby="ten" aria-hidden="true">
								<div class="modal-dialog">
									<div class="modal-content">
								
										<div class="modal-body">
											<p class="fecha">Noviembre 19 de 2013</p>
											<h3>No estoy seguro que sea para principiantes pero es MUY bueno!</h3>
											<p class="by">Por: Frank (Buenos Aires, Argentina)</p>
											<p class="comment">Un diplomado q tiene una buena estructura y esta bien organizado pero creo  que está orientado para personas que tienen experiencia en programación. Este diplomado me lo recomendaron, y después que me  inscribi , me di cuenta de que debería quizás tener un poco más de experiencia en lo basico de programacion. Pero a pesar que empecé un poco lento, no tan rápido como pensé, me dieron instrucciones más detalladas para un principiante como yo y ya despues de alli todo fue más simple. Mientras voy adelantando el diplomado se vuelve mucho mas facil y aveces me toca dedicarle tiempo extra para captar el concepto.</p>
											<p class="comment">Si tienes el tiempo necesario y la constancia entonces es una inversión segura en mi opinión personal pero necesita practica y constancia no puedes parar. por eso le doy 4 estrellas.</p>
										</div>
										<div class="modal-footer">
											<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar ventana</button>
										</div>
									</div>
								</div>
							</div>
						</div>
					
<div class="each-review">
							<span class="glyphicon glyphicon-star"></span>
							<span class="glyphicon glyphicon-star"></span>
							<span class="glyphicon glyphicon-star"></span>
							<span class="glyphicon glyphicon-star"></span>
							<span class="glyphicon glyphicon-star"></span>
							<p>La forma más útil para aprender a crear apps sin tener ninguna experiencia previa<span>Enero 20 de 2014</span><small>Por: Oscar (Valencia, Venezuela)</small></p>
							<div class="info">
								<p>No soy un ingeniero de software profesional , pero , soy apasionado por la computación. Como... <a href="#" data-toggle="modal" data-target="#doce">Leer más</a></p>
							</div>
							<div class="modal fade" id="doce" tabindex="-1" role="dialog" aria-labelledby="doce" aria-hidden="true">
								<div class="modal-dialog">
									<div class="modal-content">
								
										<div class="modal-body">
											<p class="fecha">Enero 20 de 2014</p>
											<h3>La forma más útil para aprender a crear apps sin tener ninguna experiencia previa</h3>
											<p class="by">Por: Oscar (Valencia, Venezuela)</p>
											<p class="comment">No soy un ingeniero de software profesional , pero , soy apasionado por la computación. Como trabajo / freelance hago paginas web y he incursionado en aprender otros campos para tener más oportunidades, Aún así, cuando me decidí a aprender el desarrollo de aplicaciones me sentí algo así como hormiga en baile gallina. Este curso fue la luz al final del túnel. Le puso fin a la locura y coloco todo en un contexto en que lo pude entender.</p>
											<p class="comment">Aunque todo en el desarrollo de iOS sigue una lógica coherente , que es una parte lógica seleccionada por Apple.  Por lo menos, es necesaria alguna explicación de por qué estas cosas son como son ( legibilidad ) .Este diplomado da una introducción simple pero concreta  que explica las opciones de diseño y te lleva de la mano paso a paso , una vez que te acostumbras a los estándares para crear apps vas a amar todo el mundo de Apple.</p>
											<P class="comment">Quizás para programadores más experimentados puede parecer un poco lento o básico. Sin embargo , de todos los cursos y tutoriales que he tomado , este tiene el mejor equilibrio entre básico y avanzado . Los fundamentos se entremezclan de una manera que te frustras si no entiendes algo muy básico.</P>
											<p class="comment">Le voy a dar cuatro estrellas porque estoy justo al principio , así que no puedo decir como va a terminar. Sin embargo , mi impresión inicial es muy positiva y yo se lo recomendaría a varios amigos que se quieren iniciar en crear apps.</p>
										</div>
										<div class="modal-footer">
											<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar ventana</button>
										</div>
									</div>
								</div>
							</div>
						</div>

<div class="each-review">
							<span class="glyphicon glyphicon-star"></span>
							<span class="glyphicon glyphicon-star"></span>
							<span class="glyphicon glyphicon-star"></span>
							<span class="glyphicon glyphicon-star"></span>
							<span class="glyphicon glyphicon-star-empty"></span>
							<p>Realmente es un gran diplomado<span>Enero 12 de 2014</span><small>Por: Rodolfo Castañeda (La Paz, Bolivia )</small></p>
							<div class="info">
								<p>Me encanta el estilo del diplomado. Un gran diplomado, soy nuevo creando apps para iphone y android</p>
							</div>
						</div>
<div class="each-review">
							<span class="glyphicon glyphicon-star"></span>
							<span class="glyphicon glyphicon-star"></span>
							<span class="glyphicon glyphicon-star"></span>
							<span class="glyphicon glyphicon-star"></span>
							<span class="glyphicon glyphicon-star"></span>
							<p>Muy útil<span>Enero 15 de 2014</span><small>Por: Susana Milestrone (Los Lagos, Chile)</small></p>
							<div class="info">
								<p>Claro como el agua, ejemplos excelentes, muy buen sitio amigos. Los ejercicios... <a href="#" data-toggle="modal" data-target="#modal1">Leer más</a></p>
							</div>
							<div class="modal fade" id="modal1" tabindex="-1" role="dialog" aria-labelledby="modal1" aria-hidden="true">
								<div class="modal-dialog">
									<div class="modal-content">
								
										<div class="modal-body">
											<p class="fecha">Enero 15 de 2014</p>
											<h3>Muy útil</h3>
											<p class="by">Por: Susana Milestrone (Los Lagos, Chile)</p>
											<p class="comment">Claro como el agua, ejemplos excelentes, muy buen sitio amigos. Los ejercicios están bien pensados ​​y desafiantes, que en realidad aumentan la eficacia del diplomado. Este es el diplomado para empezar.</p>
										</div>
										<div class="modal-footer">
											<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar ventana</button>
										</div>
									</div>
								</div>
							</div>
						</div>
<div class="each-review">
							<span class="glyphicon glyphicon-star"></span>
							<span class="glyphicon glyphicon-star"></span>
							<span class="glyphicon glyphicon-star"></span>
							<span class="glyphicon glyphicon-star"></span>
							<span class="glyphicon glyphicon-star-empty"></span>
							<p>La mejor forma de aprender a crear apps<span>diciembre 5 de 2013</span><small>Por: Ricardo Grisales ( Francisco Morazan, Honduras)</small></p>
							<div class="info">
								<p>Este es la mejor forma de aprender a crear apps. Todos los conceptos se explican con claridad y fácil de seguir. No deja ningún detalle y esta actualizado.</p>
							</div>
						</div>

<div class="each-review">
							<span class="glyphicon glyphicon-star"></span>
							<span class="glyphicon glyphicon-star"></span>
							<span class="glyphicon glyphicon-star"></span>
							<span class="glyphicon glyphicon-star"></span>
							<span class="glyphicon glyphicon-star"></span>
							<p>Buena estructura, es exactamente lo que se necesita para empezar a crear apps<span>Noviembre 27 de 2013</span><small>Por: José Ignacio Montalban (Distrito federal, Mexico)</small></p>
							<div class="info">
								<p>El diplomado esta Excelente, muy bien organizado. Es exactamente lo que necesita para empezar a aprender a crear apps. Casi todos los temas... <a href="#" data-toggle="modal" data-target="#modal">Leer más</a></p>
							</div>
							<div class="modal fade" id="modal" tabindex="-1" role="dialog" aria-labelledby="modal" aria-hidden="true">
								<div class="modal-dialog">
									<div class="modal-content">
								
										<div class="modal-body">
											<p class="fecha">Noviembre 27 de 2013</p>
											<h3>Buena estructura, es exactamente lo que se necesita para empezar a crear apps</h3>
											<p class="by">Por: José Ignacio Montalban (Distrito federal, Mexico)</p>
											<p class="comment">El diplomado esta Excelente, muy bien organizado . Es exactamente lo que necesita para empezar a aprender a crear apps  . Casi todos los temas están cubiertos en un buen nivel . No ahondan en detalles muy complicados , pero si está buscando un diplomado para iniciar la creacion de apps, entonces este es la mejor opcion. Hay varios otros cursos disponibles , pero nadie es capaz llevar la clases de tal forma limpia y fácil como lo hace este diplomado. Yo recomendaría este diplomado a cualquier persona que tenga conocimiento escaso o medio de programación o creacion de apps.</p>
										</div>
										<div class="modal-footer">
											<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar ventana</button>
										</div>
									</div>
								</div>
							</div>
						</div>

<div class="each-review">
							<span class="glyphicon glyphicon-star"></span>
							<span class="glyphicon glyphicon-star"></span>
							<span class="glyphicon glyphicon-star"></span>
							<span class="glyphicon glyphicon-star"></span>
							<span class="glyphicon glyphicon-star"></span>
							<p>Gran diplomado<span>Febrero 03 de 2014</span><small>Por: José Angel (California, United States)</small></p>
							<div class="info">
								<p>	Muy útil para un programador con experiencia, pero que aún esté buscando aprender a programar para... <a href="#" data-toggle="modal" data-target="#uju">Leer más</a></p>
							</div>
							<div class="modal fade" id="uju" tabindex="-1" role="dialog" aria-labelledby="uju" aria-hidden="true">
								<div class="modal-dialog">
									<div class="modal-content">
								
										<div class="modal-body">
											<p class="fecha">Noviembre 27 de 2013</p>
											<h3>Gran diplomado</h3>
											<p class="by">Por: José Angel (California, United States)</p>
											<p class="comment">Muy útil para un programador con experiencia, pero que aún esté buscando aprender a programar para IOS o Android. Estoy muy contento que tome este diplomado, ya que te guía a través de una gran cantidad de código y explica las clases pertinentes, los métodos, las funciones, etc. Hacen un buen trabajo de no entrar demasiado a fondo con la explicación y simplemente crear y tirar código una forma muy intuitiva de aprender. Es un buen equilibrio que mantiene el diplomado.</p>
										</div>
										<div class="modal-footer">
											<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar ventana</button>
										</div>
									</div>
								</div>
							</div>
						</div>

<div class="each-review">
							<span class="glyphicon glyphicon-star"></span>
							<span class="glyphicon glyphicon-star"></span>
							<span class="glyphicon glyphicon-star"></span>
							<span class="glyphicon glyphicon-star"></span>
							<span class="glyphicon glyphicon-star-empty"></span>
							<p>No es para personas que son absolutamente nuevas en la programación<span>Febrero 03 de 2014</span><small>Por: Ivan Benites (Región Metropolitana, Chile)</small></p>
							<div class="info">
								<p>Soy programador y tenían poca (o ninguna) experiencia en el desarrollo de iOS, este diplomado excelente  pero creo que para una  personas nueva en programación le va a tocar dura  ... pero si puedes dedicarle tiempo y trabajo entonces es cuestión de dedicarle.</p>
							</div>
						</div>

<div class="each-review">
							<span class="glyphicon glyphicon-star"></span>
							<span class="glyphicon glyphicon-star"></span>
							<span class="glyphicon glyphicon-star"></span>
							<span class="glyphicon glyphicon-star"></span>
							<span class="glyphicon glyphicon-star"></span>
							<p>Buen diplomado para principiantes en plataforma móviles.<span>Octubre 29 de 2013</span><small>Por: Diego Montenegro (Sonora, Mexico)</small></p>
							<div class="info">
								<p>	Me parece que el diplomado tiene una muy buena introducción a Obj C y en conceptos. No he tenido problemas con las clases  y encontrar el material fácil de entender. Creo que es útil contar con conocimiento en programación pero no es necesario Muy recomendado.</p>
							</div>
						</div>

<div class="each-review">
							<span class="glyphicon glyphicon-star"></span>
							<span class="glyphicon glyphicon-star"></span>
							<span class="glyphicon glyphicon-star"></span>
							<span class="glyphicon glyphicon-star-empty"></span>
							<span class="glyphicon glyphicon-star-empty"></span>
							<p>Curso decente<span>Enero 20 de 2014</span><small>Por: JPG (Cordoba, Colombia)</small></p>
							<div class="info">
								<p>Mi opinión está dividida sobre este diplomado, porque veo que tiene muy buen material y esta muy bien organizado, pero... <a href="#" data-toggle="modal" data-target="#twotwo">Leer más</a></p>
							</div>
							<div class="modal fade" id="twotwo" tabindex="-1" role="dialog" aria-labelledby="twotwo" aria-hidden="true">
								<div class="modal-dialog">
									<div class="modal-content">
								
										<div class="modal-body">
											<p class="fecha">Enero 20 de 2014</p>
											<h3>Curso decente</h3>
											<p class="by">Por: JPG (Cordoba, Colombia)</p>
											<p class="comment">Mi opinión está dividida sobre este diplomado, porque veo que tiene muy buen material y esta muy bien organizado, pero tuve que luchar para empezar desde 0 con la creacion de apps. Creo que existen mejores forma de aprender programacion aunque quisas no es caso para una persona que quiere aprender a crear apps y luego profundizar. Pero para una persona que empieza de 0 toca dedicarle para ir profundizando.</p>
										</div>
										<div class="modal-footer">
											<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar ventana</button>
										</div>
									</div>
								</div>
							</div>
						</div>

<div class="each-review">
							<span class="glyphicon glyphicon-star"></span>
							<span class="glyphicon glyphicon-star"></span>
							<span class="glyphicon glyphicon-star"></span>
							<span class="glyphicon glyphicon-star-empty"></span>
							<span class="glyphicon glyphicon-star-empty"></span>
							<p>Creacion de apps para personas con ideas<span>Enero 22 de 2014</span><small>Por: Paul S (Lima, Peru)</small></p>
							<div class="info">
								<p>Estilo de diplomado esta. Bien estructurado y hasta ahora a sido una de mis formas favoritas de aprender algo me atreveria a decir que fue divertido</p>
							</div>
						</div>

<div class="each-review" style="border:0px;">
							<span class="glyphicon glyphicon-star"></span>
							<span class="glyphicon glyphicon-star"></span>
							<span class="glyphicon glyphicon-star"></span>
							<span class="glyphicon glyphicon-star"></span>
							<span class="glyphicon glyphicon-star"></span>
							<p>Me encanto este diplomado<span>Diciembre 10 de 2013</span><small>Por: ManUel (Región Metropolitana, Chile)</small></p>
							<div class="info">
								<p>Estoy en la segunda parte del diplomado. Ni te imaginas las nuevas técnicas y... <a href="#" data-toggle="modal" data-target="#oneone">Leer más</a></p>
							</div>
							<div class="modal fade" id="oneone" tabindex="-1" role="dialog" aria-labelledby="oneone" aria-hidden="true">
								<div class="modal-dialog">
									<div class="modal-content">
								
										<div class="modal-body">
											<p class="fecha">Diciembre 10 de 2013</p>
											<h3>Me encanto este diplomado</h3>
											<p class="by">Por: ManUel (Región Metropolitana, Chile)</p>
											<p class="comment">Estoy en la segunda parte del diplomado. Ni te imaginas las nuevas técnicas y conceptos que se vienen mucho mejor que la primera parte. Me encantan los ejemplos incrementalmente complejos y la forma en que se presentan.</p>
										</div>
										<div class="modal-footer">
											<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar ventana</button>
										</div>
									</div>
								</div>
							</div>
						</div>

				</div>
						

				
		</div>
	</div>

</div>