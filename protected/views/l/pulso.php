<style type="text/css">
	body {
		background: #505355 url("../images/pulso/back.png") 50% 0 repeat-x !important;
		background-attachment:fixed !important;
	}
</style>
<div class="pulso">
	<div class="header"><div class="brand">Pulso Institute</div></div>

	<div class='row'>
		<div class='container'>
			<div class='col-md-12'>
				<h1> Cursos paso a paso de tecnología para</br>  <strong>personas que hacen cosas increíbles</strong></h1>
				

				<div class='form'>
						<div class='msg'>
							Regístrate gratis!
							<img alt="Arrow-tooltip" src="<?php echo Yii::app()->request->baseUrl; ?>/images/arrow-tooltip.png" />
						</div>


						<?php $form=$this->beginWidget('CActiveForm', array(
							'id'=>'register-form',
							'action'=>Yii::app()->createUrl('l/registro'),
							'htmlOptions' => array(
								'class'=>"form-inline",
							),
						)); ?>
							
							<input name="redirect" type="hidden" value="l/pulsoinstitute" />
							<div class='form-group'>
								<?php echo $form->textField($model,'email1', array('size'=>'40', 'class'=>'form-control input-lg', 'placeholder'=>"Digita tu e-mail:")); ?>
								<?php echo $form->error($model,'email1'); ?>
							</div>
							<input class="btn btn-danger btn-lg" name="commit" type="submit" value="Iniciate hoy mismo" />
						<?php $this->endWidget(); ?>
					</div>
			</div>
		</div>
	</div>

	<footer>
      <div class='row'>
        <div class='col-md-12'>
          <p>Powered by: Oja.la Edu Inc - © Copyright 2013 Oja.la</p>
          <small>Hecho con <img src="../images/coin/withlove.png" alt="With Love"> para Latam</small>
        </div>
      </div>
    </footer>
</div>