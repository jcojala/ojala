<nav class='navbar navbar-default navbar-fixed-top' role='navigation'>
	<div class='navbar-header'>
		<p class="navbar-brand">Oja.la</p>
	</div>
</nav>

<div class='home' role='main'>
	<div class='row'>
		<div class='row landing iphone'>
			<div class='container'>
				<div class='col-md-3 img'><img alt="Iphone" src="<?php echo Yii::app()->request->baseUrl; ?>/images/iPhoneiOS7.png" /></div>

				<div class='col-md-8 info'>
					<h1> Crea aplicaciones para <strong>iOS7</strong></h1>
					<h4>Curso desde 0 para no-programadores</h4>

					<div class='player'>
					<a data-toggle="modal" href="#video" class="btn btn-primary btn-lg">Play</a>

					<div class="modal fade" id="video" tabindex="-1" role="dialog" aria-labelledby="video" aria-hidden="true">
						<div class="modal-dialog">
							<div class="modal-content">
								<div class="modal-header" style="padding:5px 10px 20px 10px; border: 0px;">
                					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
								</div>
								<div class="modal-body" id="modalbody" style="padding-top:0;">
								<iframe src="https://fast.wistia.net/embed/iframe/bf569173e5?controlsVisibleOnLoad=false" allowtransparency="true" frameborder="0" scrolling="no" class="wistia_embed" name="wistia_embed" allowfullscreen mozallowfullscreen webkitallowfullscreen oallowfullscreen msallowfullscreen width="840" height="468" video_quality="hd-only"></iframe>
								</div>
							</div>
						</div>
					</div>
					</div>

					<div class='form'>
						<div class='msg'>
							Regístrate gratis!
							<img alt="Arrow-tooltip" src="<?php echo Yii::app()->request->baseUrl; ?>/images/arrow-tooltip.png" />
						</div>


						<?php $form=$this->beginWidget('CActiveForm', array(
							'id'=>'register-form',
							'action'=>Yii::app()->createUrl('site/registro'),
							'htmlOptions' => array(
								'class'=>"form-inline",
							),
						)); ?>
							
							<input name="redirect" type="hidden" value="portada?id=153" />
							<div class='form-group'>
								<?php echo $form->textField($model,'name', array('size'=>'30', 'class'=>'form-control input-lg', 'placeholder'=>"Digita tu nombre:")); ?>
								<?php echo $form->error($model,'name'); ?>
							</div>
							<div class='form-group'>
								<?php echo $form->textField($model,'email1', array('size'=>'30', 'class'=>'form-control input-lg', 'placeholder'=>"Digita tu e-mail:")); ?>
								<?php echo $form->error($model,'email1'); ?>
							</div>
							<input class="btn btn-success btn-lg" name="commit" type="submit" value="Unirse" />
						<?php $this->endWidget(); ?>


					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<footer>
      <div class='row'>
        <div class='col-md-12 about'>
          <p>
          <strong>USA:</strong>
          364 University Ave, Palo Alto, CA, 94301
          </p>
          <i>© Copyright 2013 Oja.la Edu Inc.</i>
        </div>
      </div>
    </footer>

<script type="text/javascript">
	$(document).ready(function()
	{
		$('#video').on('hidden.bs.modal', function () {
			$('#modalbody').html('');
		});
		$('#video').on('show.bs.modal', function () {
			$('#modalbody').html('<iframe src="https://fast.wistia.net/embed/iframe/bf569173e5?controlsVisibleOnLoad=false" allowtransparency="true" frameborder="0" scrolling="no" class="wistia_embed" name="wistia_embed" allowfullscreen mozallowfullscreen webkitallowfullscreen oallowfullscreen msallowfullscreen width="840" height="478" video_quality="hd-only"></iframe>');
		});
	});
</script>