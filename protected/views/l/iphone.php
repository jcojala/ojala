<!DOCTYPE html>
<html>
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>Ojala</title>
    <meta name="keywords" content="">
    <meta name="description" content="">
    <link href="<?php echo Yii::app()->request->baseUrl; ?>/landing/reset-81c62fcc415bd2d6fa009d66c47174b6.css" media="screen" rel="stylesheet" type="text/css">
    <link href="<?php echo Yii::app()->request->baseUrl; ?>/landing/page_defaults-2f3cb785ec396f69d695b37548de7fa0.css" media="screen" rel="stylesheet" type="text/css">
    <link rel="stylesheet" media="screen" href="<?php echo Yii::app()->request->baseUrl; ?>/landing/jquery.ubpoverlay.css" type="text/css">
    
    <script src="<?php echo Yii::app()->request->baseUrl; ?>/landing/jquery.min.js" type="text/javascript"></script>
    <script src="<?php echo Yii::app()->request->baseUrl; ?>/landing/jquery.validate.min.js" type="text/javascript"></script>
    <script src="<?php echo Yii::app()->request->baseUrl; ?>/landing/additional-methods.js" type="text/javascript"></script>
    <script src="<?php echo Yii::app()->request->baseUrl; ?>/landing/jquery.ubpoverlay.js" type="text/javascript"></script>
    <script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/landing/unbounce.js"></script>
    <script src="<?php echo Yii::app()->request->baseUrl; ?>/landing/jquery.min.js" type="text/javascript"></script>

    <script type="text/javascript">var _kmq = _kmq || [];
      var _kmk = _kmk || '180d38c17b41ceefca2da1e0a4391465d66998b5';
      function _kms(u){
        setTimeout(function(){
          var d = document, f = d.getElementsByTagName('script')[0],
          s = d.createElement('script');
          s.type = 'text/javascript'; s.async = true; s.src = u;
          f.parentNode.insertBefore(s, f);
        }, 1);
      }
      _kms('//i.kissmetrics.com/i.js');
      _kms('//doug1izaerwt3.cloudfront.net/' + _kmk + '.1.js');
    </script>

    <link href="<?php echo Yii::app()->request->baseUrl; ?>/landing/jquery.fancybox-1.3.4.css" media="screen" rel="stylesheet" type="text/css">
    <script src="<?php echo Yii::app()->request->baseUrl; ?>/landing/jquery.fancybox-1.3.4.js"></script>
    
    <style title="page-styles" type="text/css">
      body { color:#000; background-color: #fff !important }
      a {
        color:#0000ff;
        text-decoration:none;
      }
      #lp-pom-text-35 {
        background:rgba(255,255,255,0);
        -pie-background:rgba(255,255,255,0);
        position:absolute;
        left:0px;
        top:146px;
        z-index:1;
        width:1036px;
        height:0px;
        behavior:url(/PIE.htc);
      }
      #lp-pom-text-45 {
        background:rgba(255,255,255,0);
        -pie-background:rgba(255,255,255,0);
        position:absolute;
        left:606px;
        top:384px;
        z-index:2;
        width:300px;
        height:74px;
        behavior:url(/PIE.htc);
      }
      #lp-pom-text-47 {
        background:rgba(255,255,255,0);
        -pie-background:rgba(255,255,255,0);
        position:absolute;
        left:923px;
        top:215px;
        z-index:3;
        width:300px;
        height:0px;
        behavior:url(/PIE.htc);
      }
      #lp-pom-button-59 {
        position:absolute;
        left:292px;
        top:-4px;
        z-index:5;
        width:148px;
        height:40px;
        behavior:url(/PIE.htc);
        border-radius:5px;
        background-color:#fe0000;
        background:-webkit-linear-gradient(#fe0000,#e40000);
        background:-moz-linear-gradient(#fe0000,#e40000);
        background:-ms-linear-gradient(#fe0000,#e40000);
        background:-o-linear-gradient(#fe0000,#e40000);
        background:linear-gradient(#fe0000,#e40000);
        box-shadow:inset 0px 1px 0px #ff4c4c,inset 0 -1px 2px #b10000;
        text-shadow:1px 1px #5b0000;
        -pie-background:linear-gradient(#fe0000,#e40000);
        color:#fff;
        border-style:solid;
        border-width:1px;
        border-color:#333333;
        font-size:16px;
        line-height:19px;
        font-weight:bold;
        font-family:arial;
        text-align:center;
        background-repeat:no-repeat;
      }
      #lp-pom-button-59:hover {
        background-color:#f10000;
        background:-webkit-linear-gradient(#f10000,#d90000);
        background:-moz-linear-gradient(#f10000,#d90000);
        background:-ms-linear-gradient(#f10000,#d90000);
        background:-o-linear-gradient(#f10000,#d90000);
        background:linear-gradient(#f10000,#d90000);
        box-shadow:inset 0px 1px 0px #ff4c4c,inset 0 -1px 2px #a50000;
        -pie-background:linear-gradient(#f10000,#d90000);
        color:#fff;
      }
      #lp-pom-button-59:active {
        background-color:#e50000;
        background:-webkit-linear-gradient(#e40000,#e40000);
        background:-moz-linear-gradient(#e40000,#e40000);
        background:-ms-linear-gradient(#e40000,#e40000);
        background:-o-linear-gradient(#e40000,#e40000);
        background:linear-gradient(#e40000,#e40000);
        box-shadow:inset 0px 2px 4px #650000;
        -pie-background:linear-gradient(#e40000,#e40000);
        color:#fff;
      }
      #lp-pom-form-58 .lp-pom-form-field label {
        margin-top:10px;
        font-family:"arial";
        font-weight:bold;
        font-size:14px;
        line-height:15px;
        color:#000;
      }
      #lp-pom-form-58 .lp-pom-form-field .option label {
        font-family:"arial";
        font-weight:normal;
        font-size:13px;
        line-height:15px;
        left:18px;
        color:#000;
      }
      #lp-pom-form-58 .lp-pom-form-field .option input { top:2px; }
      #lp-pom-form-58 .lp-pom-form-field input.text {
        box-shadow:inset 0px 2px 3px #dddddd;
        -webkit-box-shadow:inset 0px 2px 3px #dddddd;
        -moz-box-shadow:inset 0px 2px 3px #dddddd;
        border-radius:5px;
      }
      #lp-pom-form-58 .lp-pom-form-field textarea {
        box-shadow:inset 0px 2px 3px #dddddd;
        -webkit-box-shadow:inset 0px 2px 3px #dddddd;
        -moz-box-shadow:inset 0px 2px 3px #dddddd;
        border-style:solid;
        border-width:1px;
        border-color:#bbbbbb;
        border-radius:5px;
      }
      #lp-pom-form-58 .lp-pom-form-field input[type=text] {
        border-style:solid;
        border-width:1px;
        border-color:#bbbbbb;
      }
      #lp-pom-form-58 .lp-pom-form-field select {
        border-style:solid;
        border-width:1px;
        border-color:#bbbbbb;
      }
      #lp-pom-form-58 {
        position:absolute;
        left:422px;
        top:283px;
        z-index:4;
        width:280px;
        height:34px;
        behavior:url(/PIE.htc);
      }
      #lp-pom-image-60 {
        position:absolute;
        left:5px;
        top:8px;
        z-index:6;
      }
      #lp-pom-image-60 .lp-pom-image-container {
        width:57px;
        height:57px;
        behavior:url(/PIE.htc);
      }
      #lp-pom-image-61 {
        position:absolute;
        top:-139px;
        z-index:7;
      }
      #lp-pom-image-61 .lp-pom-image-container {
        width:520px;
        height:92px;
        behavior:url(/PIE.htc);
      }
      #lp-pom-block-10 {
        background:rgba(255,255,255,1);
        -pie-background:rgba(255,255,255,1);
        margin:auto;
        position:relative;
        width:1036px;
        height:523px;
        behavior:url(/PIE.htc);
        border-radius:5px;
      }
      #lp-pom-block-25 {
        background:rgba(255,255,255,0);
        -pie-background:rgba(255,255,255,0);
        margin-left:auto;
        margin-right:auto;
        margin-bottom:0px;
        position:relative;
        width:1036px;
        height:139px;
        behavior:url(/PIE.htc);
      }
      #lp-pom-root {
        background:rgba(255,255,255,1);
        -pie-background:rgba(255,255,255,1);
        margin:auto;
        padding-top:0px;
        behavior:url(/PIE.htc);
      }
      #lp-pom-form-22{
        left: 0 !important;
        top: 300px !important;
      }
      #lp-pom-button-23{
        right: 0;
      }
      .lp-pom-form-field{
        width: 320px !important;
        top: 0px !important;
      }
      .lp-pom-form-field:nth-child(2){
        left: 300px !important;
      }
      .lp-pom-form-field:nth-child(3){
        left: 600px !important;
      }
      .lp-pom-form-field input{
        width: 260px !important;
      }
      .lp-pom-form-field input:active{
        border-color: #d75305;
      }
      div.lp-pom-root .lp-pom-button span{
        
      }
    </style>
    <script src="<?php echo Yii::app()->request->baseUrl; ?>/landing/webfont.js"></script>

    <!--[if IE 7]>
    <script>
    (function() {
      var sheet = document.styleSheets[1];
      var rules = sheet.rules;
      var rule;
      var index = -1;
      for (var i=0, l=rules.length; i<l; i++){
        rule = rules[i];
        if (rule.selectorText.toLowerCase() == 'div.lp-pom-root .lp-pom-text span') {
          index = i;
          break;
        }
      }
      if (index > -1) {
        sheet.removeRule(index);
        sheet.addRule('div.lp-pom-root .lp-pom-text span', 'line-height:inherit');
      }
    })();
    </script>
    <![endif]-->

  </head>
  <body class="lp-pom-body ">
    <div class="conteiner">
      <div class="lp-element lp-pom-root" id="lp-pom-root" style="min-width: 1036px;">
      <div class="lp-positioned-content" style="top: 0px; width: 1036px; margin-left: -518px;">
      <div class="lp-element lp-pom-text" id="lp-pom-text-35" style="height: auto;"></div>
      <div class="lp-element lp-pom-text nlh" id="lp-pom-text-45" style="height: auto;">
      <p xmlns="" class="lplh-29">
      	&nbsp;</p>
      <p class="lplh-29">
      	&nbsp;</p>
      </div>
      <div class="lp-element lp-pom-text nlh" id="lp-pom-text-47" style="height: auto;"></div>
      <div class="lp-element lp-pom-form" id="lp-pom-form-58">

        <?php if(Yii::app()->user->hasFlash('mensaje')){ ?>

          <div class="alert alert-success" style="font-size: 25px;text-align: center;line-height: 30px;margin-left: -100px;margin-top: -100px;">
          <?php echo Yii::app()->user->getFlash('mensaje'); ?>
          </div>

        <?php }else{ ?>

        <?php $form=$this->beginWidget('CActiveForm', array(
            'id'=>'register-form',
            'action'=>Yii::app()->createUrl('l/registroIphone'),
        )); ?>

          <input name="landing" value="iphone" type="hidden">
          <fieldset class="clearfix" style="width: 280px; height: 34px;">
            <div class="lp-pom-form-field clearfix" style="width: 280px; height: 36px; top: 0px;">
              <label for="email" class="main" style="top: 0px; width: 42px; height: auto;">E-mail</label>
                <?php echo $form->textField($model,'email1', array('size'=>'30', 'class'=>'form-control input-lg', 'placeholder'=>"Digita tu e-mail:", 'style'=>"top: 0px; left: 54px; width: 208px; font-size: 15px; height: 32px; padding-left: 8px; padding-right: 8px; lineheight: 15px;")); ?>
            </div>
          </fieldset>
        
          <a class="lp-element lp-pom-button" id="lp-pom-button-59" id="registro">
            <span class="label" style="margin-top: -10px;">Registrarme</span>
          </a>

        <?php $this->endWidget(); ?>


        <div class="lp-element lp-pom-image" id="lp-pom-image-61">
          <div class="lp-pom-image-container" style="overflow: hidden;">
          <img src="<?php echo Yii::app()->request->baseUrl; ?>/images/1yxi17n-creaunapp.gif" alt=""></div>
        </div>

      <?php } ?>
      </div>

      <div class="lp-element lp-pom-image" id="lp-pom-image-60">
        <div class="lp-pom-image-container" style="overflow: hidden;">
        <img src="<?php echo Yii::app()->request->baseUrl; ?>/images/15igcbd-logo_01l01l01l01l000000.gif" alt=""></div>
      </div>


      </div>
      <div class="lp-element lp-pom-block" id="lp-pom-block-10" style="position: relative;">
        <div class="lp-pom-block-content" style="margin: auto; width: 100%; height: 100%;"></div>
      </div>
      <div class="lp-element lp-pom-block" id="lp-pom-block-25" style="position: relative;">
        <div class="lp-pom-block-content" style="margin: auto; width: 100%; height: 100%;"></div>
      </div>
      </div>
    </div>
  </body>
</html>
<script type="text/javascript">
  $(document).ready(function()
  {
    $('#lp-pom-button-59').click(function()
    {
      $("#register-form").submit();
    });
    
  });
</script>