<!-- Vero Library Snippet -->
<script type="text/javascript">
  var _veroq = _veroq || [];
  _veroq.push(['init', { api_key: '76049769c3004bc00f813623ce0e68cb45ab3748' } ]);
  (function() {var ve = document.createElement('script'); ve.type = 'text/javascript'; ve.async = true; ve.src = '//d3qxef4rp70elm.cloudfront.net/m.js'; var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ve, s);})();
</script>
<!-- End of Vero Library Snippet -->

<?php

require_once(Yii::getPathOfAlias('ext.vero') . '/vero.php');

$v = new Vero("NzYwNDk3NjljMzAwNGJjMDBmODEzNjIzY2UwZTY4Y2I0NWFiMzc0ODphMDY0YTg0NTMzMDk2Mjg0NTU3NmI4M2U2YWJjOTlkM2YzOWIwY2Nm");

$v->identify("1234567890", "jc@oja.la", array('First name' => 'Juan', 'Last name' => 'Robles'));
$v->update("1234567890", array('First name' => 'Juan Carlos', 'Last name' => 'Robles', 'job_title' => 'Developer'));
$v->tags('1234567890', array('Yii Framework'), array());

$v->track("Se Suuscribio", array('id' => '1234567890'), array('Plan' => 'Plan Platino', 'Price' => 80));

$v->unsubscribe("1234567890");

?>