<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=1024, user-scalable=no">

    <title>Oja.la</title>
    <link href='https://fonts.googleapis.com/css?family=Open+Sans+Condensed:300,700' rel='stylesheet' type='text/css'>
    <link href='<?php echo Yii::app()->request->baseUrl; ?>/css/landing.css' rel='stylesheet' type='text/css'>

    <link href="<?php echo Yii::app()->request->baseUrl; ?>/css/animate.css" media="screen" rel="stylesheet" type="text/css" />

    <?php Yii::app()->clientScript->registerCoreScript('jquery'); ?>
    <?php Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl . '/js/landing/landing.js', CClientScript::POS_HEAD); ?>
    <script>var baseUrl = '<?php echo Yii::app()->request->baseUrl; ?>';</script>

    <?php
      OjalaScripts::registerKissmetrics();
      OjalaScripts::registerSegumiento();
    ?>
  </head>

  <body>
    <?php OjalaScripts::registerRemarketing(); ?>

    <div class="keys-nav hide">
      <a href="#" class="back">back</a>
      <a href="#" class="next">next</a>
    </div>

    <div id="background" class="negro slide"></div>

    <div id="main" class="slide slide0">
      <div class="info">
        <h1>Aprendizaje personalizado</h1>
        <h2>Encontramos las clases correctas <strong>para ti</strong></h2>
        <p><strong>¿Quieres saber todo sobre <?php echo $campaign->name ?>?</strong></p>
        <div class="btns">
          <a href="#" class="start">Empecemos</a>
          <a href="#" class="how">¿Cómo funciona?</a>
        </div>
      </div>
    </div>

    <section id="how-works" class="slide hide">
      <section class="slide slide1 gris hide">
        <h1><strong>El problema</strong> con los cursos on-line …</h1>
      </section>

      <section class="slide slide2 gris hide">
        <h1>es que nunca sabes donde conseguir <strong>el curso correcto</strong></h1>
      </section>

      <section class="slide slide3 gris hide">
        <h1>¿Cómo sé, si el curso <strong>es de calidad?</strong></h1>
      </section>

      <section class="slide slide4 gris hide">
        <h1><strong>¿Qué voy a lograr</strong> cuando termine el curso?</h1>
      </section>

      <section class="slide slide5 azul hide">
        <h1><strong>¿Qué pasaría sí alguien te ayuda</strong><br> a conseguir el curso perfecto para ti?</h1>
      </section>

      <section class="slide slide6 azul hide">
        <h1><strong>¡Empecemos!</strong></h1>
      </section>

      <div class="slide slide7 azul hide">
        <h1><strong>Oja.la es la forma más personalizada</strong><br> de aprendizaje que vayas a experimentar</h1>
      </div>

      <div class="slide slide8 azul hide">
        <h1><strong>Vas a chatear en vivo con un asesor real</strong><br> quién te ayudará a encontrar el curso correcto</h1>
      </div>
    </section>

    <div id="questions-wrapper" class="slide hide">
      <?php 
        $i = 1;
        foreach ($campaign->questions as $qstn): 
      ?>
      <div id="question-<?php echo $i ?>" class="slide slide<?php echo 9 + $i ?> azul <?php echo $i > 1 ? 'hide' : '' ?>">
        <h1><?php echo $qstn->question ?></h1>
        <div class="btns">
          <?php 
            foreach ($qstn->answers as $answer): 
                $function = 'ojaLanding.save(' . $i . ',\'' . $qstn->question . '\',\'' . $answer->value .'\','. $qstn->id_question . ',' . $answer->id_answer . ')';
          ?>
          <button onclick="<?php echo $function ?>" class="btn btn4">
            <?php echo $answer->value ?>
          </button>
        <?php endforeach ?>
      </div>
    </div>
    <?php $i++; ?>
  <?php endforeach ?>   
  </div>

  <div id="registration-form" class="slide azul hide form">
      <form id="registration" name="login" method="get" accept-charset="utf-8">  
        <h1><strong>Deja tus datos para hablar con un asesor</strong></br>¿Cuál es tu nombre?</h1>
        <ul>  
          <li><input id="username" type="text" name="username" placeholder="Escribe tu nombre aquí" autocomplete="off"></li>
          <li><input id="usermail" type="email" name="usermail" placeholder="y ¿Cuál es tu correo?" autocomplete="off" class="hide" required></li>
          <li><input id="register" type="submit" value="Entrar" class="hide"></li>
        </ul>  
      </form>
    </div>

  <div id="loading-chat" class="slide slide15 azul hide">
    <h1>Te estamos conectando con <strong>nuestro asesor …</strong>
      <span class="cargando"></span></h1>
  </div>

  <div id="chat" class="slide slide16 hide">
    <div class="loading-wrapper hide">Cargando&hellip;</div>
    <div class="video video-basico">        
      <div class="video-data">
        <input 
          type="hidden" 
          id="mediumCourse" 
          data-title="<?php echo $campaign->mediumCourse->name ?>"
          data-content="<?php echo htmlspecialchars($campaign->mediumCourse->content) ?>"
          value="<?php echo $campaign->mediumCourse->mainVideo ?>"
        >
        <input 
          type="hidden" 
          id="advancedCourse" 
          data-title="<?php echo $campaign->advancedCourse->name ?>"
          data-content="<?php echo htmlspecialchars($campaign->advancedCourse->content) ?>"
          value="<?php echo $campaign->advancedCourse->mainVideo ?>"
        >
      </div>
      <div class="info-basica">
          <h1><?php echo $campaign->basicCourse->name ?></h1>

          <?php 
            $video = $campaign->basicCourse->mainVideo;
           ?>
          <iframe id="wistia_embed" src="//fast.wistia.net/embed/iframe/<?php echo $video; ?>?videoFoam=true" 
            allowtransparency="true" frameborder="0" scrolling="no" class="wistia_embed" name="wistia_embed"
            allowfullscreen mozallowfullscreen webkitallowfullscreen oallowfullscreen msallowfullscreen width="670" height="419">
          </iframe>

          <div class="programa">
            <?php echo $campaign->basicCourse->content ?>
          </div>

          <a href="#" id="show-programa">Ver más información de este curso</a>

        <div class="certificaciones">
          <div class="col-1">
            <p><strong>Certificación al terminar</strong></p>
            <p>Al completar el curso o diplomado solicita tu certificado</p>
          </div>
          <div class="col-2">
            <p><strong>Acompañamiento permanente</strong></p>
            <p>No estarás solo, siempre tendrás a quien preguntar por ayuda</p>
          </div>
        </div>


        <div class="pagos">
          <div class="costo">
            <h3>$37</h3>
            <p>Dólares américanos</p>
            <hr>
            <form>
              <label><input type="radio" name="mensual" value="mensual" class="month" checked>Mensual</label>
              <label><input type="radio" name="anual" value="anual" class="year">Anual</label>
              <small>La anualidad incluye 2 meses gratis</small>
            </form>
            <hr>
            <div class="btns">
              <p>Puedes pagar usando...</p>
              <a href="#" class="tarjeta">Tu tarjeta de crédito</a>
              <a href="#" class="paypal">ó tu cuenta de Paypal</a>
            </div>
          </div>
          <div class="recibiras">
            <p>Recibirás acceso a:</p>
            <ul>
              <li>Más de 150 cursos y 20 nuevos cada mes.</li>
              <li>3000 Clases paso a paso sobre los temas que más te interesan.</li>
              <li>Todo el Material descargable, como libros y guias.</li>
              <li>Códigos de los proyectos desarrollados.</li>
              <li>Podrás descargar el material usado por el instructor.</li>
              <li>Soporte personalizado vía chat, mail o Skype.</li>
              <li>La certificación de cada curso o diplomado que completes.</li>
            </ul>
          </div>
        </div>
      </div>
    </div>

      <div class="chat"><?php OjalaScripts::registerOlark() ?></div>
    </div>
    <script src="//fast.wistia.net/assets/external/iframe-api-v1.js"></script>
  </body>
</html>