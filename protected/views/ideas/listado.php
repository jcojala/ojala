<?php

$this->pageTitle = "Listado de próximos cursos";

?>

<div class="page-header row">
    <div class="col-md-8">
        <h1>Próximos cursos</h1>
        <p>Este listado muestra todos futuros cursos que Oja.la tendrá para ti... Si tienes alguna sugerencia, por favor infórmanos.</p>
    </div>
    <div class="col-md-4 buscador">
        <form role="search" method="get" action="<?php echo Yii::app()->urlManager->createUrl('ideas/proxcursos'); ?>">			
            <div class="input-group input-group-lg">
                <input type="text" class="form-control" placeholder="¿Qué información buscas?" name="buscar">
            </div>
            <button type="submit" class="input-group-addon glyphicon glyphicon-search"></button>
        </form>
    </div>
</div>

<div class="row">

    <div class="col-md-2 sidebar-filters">

        <!-- Filtros de seccion -->		
        <div class="filters">
            <h4>Secciones</h4>
            <ul class="nav nav-pills nav-stacked" role="navigation">

                <?php foreach ($categorias as $cat) { ?>
                    <?php if ($cat->na_cathegory == $categoria) { ?>

                        <li class="active">

                        <?php } else { ?>

                        <li>

                        <?php } ?>

                        <a href="<?php echo Yii::app()->urlManager->createUrl('ideas/proxcursos', array('cat' => $cat->na_cathegory)); ?>"> <?php echo $cat->na_cathegory; ?></a>

                    </li>

                <?php } ?>

            </ul>

        </div>

        <!-- Filtros de orden -->
        <div class="order">

            <h4>Filtros</h4>
            <ul class="nav nav-pills nav-stacked">
                <li><a href="<?php echo Yii::app()->urlManager->createUrl('ideas/proxcursos', array('orden' => 'alfabetico')); ?>">Orden alfabético</a></li>
                <li><a href="<?php echo Yii::app()->urlManager->createUrl('ideas/proxcursos', array('orden' => 'dificultad')); ?>">Dificultad</a></li>
                <li><a href="<?php echo Yii::app()->urlManager->createUrl('ideas/proxcursos', array('orden' => 'fecha')); ?>">Fecha de publicación</a></li>
            </ul>

        </div>
        <!-- * * * * * * -->						
    </div>


    <div class="col-md-10 col-md-offset-2">

        <?php if (count($list) == 0) { ?>

            <div class="blankslate cursos">
                <h5>Ya estamos trabajando en el curso que buscas, ahora mismo no está disponible. Mientras, aquí hay algunos temas que te pueden ser útiles</h5>

                <div class="links">
                    <?php foreach ($categorias as $cat) { ?>

                        <a href="<?php echo Yii::app()->urlManager->createUrl('ideas/proxcursos', array('cat' => $cat->na_cathegory)); ?>" class="<?php echo $cat->na_cathegory ?>"><?php echo $cat->na_cathegory ?></a>

                    <?php } ?>

                </div>

                <div class="alert alert-info">
                    <h4><a href="#" data-toggle="modal" data-target="#sugerenciaCurso">¿Ninguno de estos temas te interesa? Cuéntanos que curso quieres que hagamos<strong>Escribe aquí</strong></a></h4>
                </div>

                <div class="modal fade" id="sugerenciaCurso" tabindex="-1" role="dialog" aria-labelledby="sugerenciaCurso" aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-body">
                                <p><strong>Hola <?php echo Yii::app()->user->name; ?></strong></p>
                                <p>Cuéntanos, ¿Cuál curso quieres que hagamos para ti? o cuáles temas te interesan; de esta forma podemos planear nuevos cursos que te sean útiles.</p>
                                <form>
                                    <textarea class="form-control" placeholder="Escribe aquí tu sugerencia ..." rows="5" name="curso" id="curso"></textarea>
                                </form>
                            </div>
                            <div class="modal-footer">
                                <div class="btn-group">
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar ventana</button>
                                    <button type="button" class="btn btn-primary enviar">Enviar sugerencia</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="modal fade" id="gracias" tabindex="-1" role="dialog" aria-labelledby="gracias" aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-body">
                                <h4 style="text-align: center;margin-top: 30px;"><strong>Gracias <?php echo Yii::app()->user->name; ?> por tu sugerencia</strong></h4>
                                <p id="mensaje"></p>
                            </div>
                            <div class="modal-footer">
                                <div class="btn-group">
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar ventana</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        <?php } else if ($buscar != "") { ?>
            <div class="resultado alert alert-info">
                <p>Estos son los cursos que tenemos sobre: <strong>"<?php echo $buscar; ?>"</strong></p>
            </div>

        <?php } ?>

        <section class="ib-container row" id="ib-container">

            <!-- Modulo de cada uno de los cursos en los listados -->
            <?php foreach ($list as $item) { ?>

                <?php
                $cover = "";

                if ($item->cover != null) {

                    $cover = $item->cover;
                } else {

                    $cover = 'default.jpg';
                }
                ?>

                <div class="col-md-4">
                    <article class='mod-course'>

                        <div class='thumbnail'>
                            <div class='img'>
                                <a href="#" alt="..." class="img-rounded">
                                    <img class="img-thumbnail" alt="<?php echo $item->name; ?>" src="<?php echo Proposal::coverUrl($cover, 'medium') ?>">
                                </a>
                            </div>	

                            <div class='caption'>
                                <h6><?php echo count($item->linkProposals) ?>  personas interesadas en:</h6>
                                <h4>
                                    <a href="#">
                                        <?php echo $item->name; ?>
                                    </a>
                                </h4>

                                <hr>

                                <a id="botonMeInteresa" class="btn btn-warning pull-right" href="#" >Me interesa</a>

                            </div>
                        </div>
                    </article>
                </div>

            <?php } ?>


        </section>

    </div>

</div>








<div class="modal fade" id="meInteresaCurso" tabindex="-1" role="dialog" aria-labelledby="meInteresaCurso" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                <p>Hola, <strong><?php echo Yii::app()->user->name; ?></strong></p>
                <p>Por favor, escribe tu correo electrónico para informarte cuando el curso esté disponible.</p>
                <form>
                    <input type="text" class="form-control" name="correoElectronico">
                </form>
            </div>
            <div class="modal-footer">
                <div class="btn-group">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar ventana</button>
                    <button type="button" class="btn btn-primary enviar">Enviar</button>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="gracias" tabindex="-1" role="dialog" aria-labelledby="gracias" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                <h4 style="text-align: center;margin-top: 30px;"><strong>¡Gracias, <?php echo Yii::app()->user->name; ?> por tu interés!</strong></h4>
                <p id="mensaje"></p>
            </div>
            <div class="modal-footer">
                <div class="btn-group">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar ventana</button>
                </div>
            </div>
        </div>
    </div>
</div>






<script type="text/javascript">
    $(function () {
        $(".enviar").click(function ()
        {
            var curso = $("#curso").val();
            $.ajax({
                'url': '<?php echo Yii::app()->urlManager->createUrl("site/sugerenciaCurso"); ?>' + '?curso=' + curso,
                'cache': false,
                success: function (data)
                {
                    $('#sugerenciaCurso').modal('hide');
                    $('#gracias').modal('show');
                },
            });
        });
    });
</script>

<!-- Start of GetKudos Script -->
<script>
    (function (w, t, gk, d, s, fs) {
        if (w[gk])
            return;
        d = w.document;
        w[gk] = function () {
            (w[gk]._ = w[gk]._ || []).push(arguments)
        };
        s = d.createElement(t);
        s.async = !0;
        s.src = '//static.getkudos.me/widget.js';
        fs = d.getElementsByTagName(t)[0];
        fs.parentNode.insertBefore(s, fs)
    })(window, 'script', 'getkudos');

    getkudos('create', 'ojala');
</script>
<!-- End of GetKudos Script -->