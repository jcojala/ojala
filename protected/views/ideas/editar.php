<?php
Yii::app()->clientScript->registerCssFile(Yii::app()->baseUrl . '/css/jquery.tagsinput.css', 'screen');
Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl . '/js/scholarships/jquery.tagsinput.min.js', CClientScript::POS_END);
Yii::app()->clientScript->registerScript('taginput', "$(document).ready(function(){
			$('#tags').tagsInput({
			width: '100%',
			height: '45px',
			defaultText: ''
		});
	});"
        , CClientScript::POS_END);
?>

<div class="page-header">
    <h1>Editar Propuesta</h1>
    <p class="panel-title" style="font-size: 24px;">Completa el formulario para actualizar los datos de la propuesta. </p>
</div>

<div class="row">
    <div class="col-md-12">
        <a class="btn btn-default" href="<?php echo Yii::app()->urlManager->createUrl('admin/index') ?>">« Regresar al Administrador</a>
    </div>
</div>

<br>

<div class="row">
    <div class="col-md-8">

        <?php
        $form = $this->beginWidget('CActiveForm', array(
            'id' => 'register-form',
            'htmlOptions' => array(
                'class' => 'form-horizontal',
            )
        ));
        ?>

        <div class="form-group">
            <label class="col-lg-3" for="proposal_name">Nombre:</label>
            <div class="col-lg-7">
                <?php echo $form->textField($proposal, 'name', array('size' => '500', 'class' => 'form-control')); ?>
                <?php echo $form->error($proposal, 'name', array('style' => 'color: red;')); ?>
            </div>
        </div>

        <div class="form-group">
            <label class="col-lg-3">¿Qué voy a aprender?:</label>
            <div class="col-lg-9">
                <?php echo $form->textarea($proposal, 'learn', array('rows' => '4', 'class' => 'form-control')); ?>
                <?php echo $form->error($proposal, 'learn', array('style' => 'color: red;')); ?>
            </div>
        </div>

        <div class="form-group">

            <label class="col-lg-3" for="user_email">Tags:</label>

            <div class="col-lg-9">

                <input class="form-control" id="tags" name="tags" value="<?php
                if (count($tags) > 0) {
                    foreach ($tags as $key => $value) {
                        echo $value->idTag->tag . ', ';
                    }
                }
                ?>" />
            </div>
        </div>

        <div class="form-group">
            <label class="col-lg-3" for="id_cathegory">Categoría</label>
            <div class="col-lg-9">
                <select class="combobox form-control" name="id_cathegory">
                    <option value>Seleccione una Categoría</option>
                    <?php foreach ($listCat as $cathegory) { ?>
                        <?php if ($proposal->id_cathegory == $cathegory->id_cathegory) { ?>
                            <option selected value="<?php echo $cathegory->id_cathegory; ?>"><?php echo $cathegory->na_cathegory; ?></option>
                        <?php } else { ?>
                            <option value="<?php echo $cathegory->id_cathegory; ?>"><?php echo $cathegory->na_cathegory; ?></option>
                        <?php } ?>
                    <?php } ?>
                </select>
            </div>
        </div> 

        <div class="form-group">
            <label class="col-lg-3" for="id_cathegory">Nivel</label>
            <div class="col-lg-9">
                <select class="combobox form-control" name="id_level">
                    <option value>Seleccione un Nivel</option>
                    <?php foreach ($listNiv as $level) { ?>
                        <?php if ($proposal->id_level == $level->id_level) { ?>
                            <option selected value="<?php echo $level->id_level; ?>"><?php echo $level->na_level; ?></option>
                        <?php } else { ?>
                            <option value="<?php echo $level->id_level; ?>"><?php echo $level->na_level; ?></option>
                        <?php } ?>
                    <?php } ?>
                </select>
            </div>
        </div> 

        <hr>

        <div class="form-group">
            <label class="col-lg-3" for="user_nickname">Cover:</label>
            <div class="col-lg-9">
                <?php
                $this->widget('ext.EAjaxUpload.EAjaxUpload', array(
                    'id' => 'uploadFile',
                    'config' => array(
                        'action' => Yii::app()->createUrl('ideas/upload'),
                        'allowedExtensions' => array("jpg", "jpeg", "png", "gif"), //array("jpg","jpeg","gif","exe","mov" and etc...
                        'debug' => false,
                        'sizeLimit' => 5 * 1024 * 1024, // maximum file size in bytes
                        'minSizeLimit' => 10 * 1024, // minimum file size in bytes
                        'onComplete' => "js:function(id, fileName, responseJSON){
				                        $('#picture img').attr('src','" . Yii::app()->request->baseUrl . "/images/coversProposals/'+fileName);
				                        $('#picture').css('display','block');
                                                        $('#picture p').text('');
				                   }",
                    )
                ));

                $imgPath = getcwd() . '/' . Yii::app()->params['coverProposalPath'];
                $mediumPath = $imgPath . substr($proposal->cover, 0, -4) . Proposal::COVER_MEDIUM_SUFFIX;
                $imagen = file_exists($mediumPath) ? $proposal->getCoverURL('small') : NULL;
                ?>
                <div id='picture' class='picture' style='display:block;float:left;'>
                    <?php if ($imagen) : ?>
                        <img style='width:35%; border-radius:5px;' src='<?php echo $imagen; ?>' />
                    <?php else: ?>
                        <img style='width:35%; border-radius:5px;'  />
                        <p>No se ha cargado una portada</p>
                    <?php endif; ?>
                </div>
            </div>						
        </div>				
        <hr>

        <input class="btn btn-warning btn-lg col-lg-offset-3" type="submit" value="Guardar">

        <?php $this->endWidget(); ?>
    </div>
    <div class="col-md-3">
        <?php
        if($proposal->active == 1){
            echo CHtml::link(
                'Desactivar esta propuesta', array('ideas/desactivarPropuesta', 'id' => $proposal->id_proposal), array('confirm' => '¿Estas completamente seguro de desactivar la propuesta?', 'class' => 'btn btn-danger', 'style' => 'background-color: #FF0000 !important')
        );
        }else{
            echo CHtml::link(
                'Activar esta propuesta', array('ideas/ActivarPropuesta', 'id' => $proposal->id_proposal), array('confirm' => '¿Estas completamente seguro de activar la propuesta?', 'class' => 'btn btn-danger', 'style' => 'background-color: #FF0000 !important')
        );
        }
        
        ?>
    </div>
</div>
<hr>