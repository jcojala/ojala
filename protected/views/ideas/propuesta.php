<div class="page-header">
    <h1>Próximo curso</h1>
    <p class="panel-title" style="font-size: 24px;">Completa el formulario para ingresar una nueva propuesta. </p>
</div>

<div class="row">
    <div class="col-md-12">
        <a class="btn btn-default" href="<?php echo Yii::app()->urlManager->createUrl('admin/index') ?>">« Regresar al Administrador</a>
    </div>
</div>

<br>

<div class="row">
    <div class="col-md-12 page-layout">

        <?php
        $form = $this->beginWidget('CActiveForm', array(
            'id' => 'register-form',
            'htmlOptions' => array(
                'class' => 'form-horizontal',
            )
        ));
        ?>

        <div class="form-group">
            <label class="col-lg-3" for="proposal_name">Nombre:</label>
            <div class="col-lg-7">
                <?php echo $form->textField($proposal, 'name', array('size' => '500', 'class' => 'form-control')); ?>
                <?php echo $form->error($proposal, 'name', array('style' => 'color: red;')); ?>
            </div>
        </div>

        <div class="form-group">
            <label class="col-lg-3">¿Qué voy a aprender?:</label>
            <div class="col-lg-9">
                <?php echo $form->textarea($proposal, 'learn', array('rows' => '4', 'class' => 'form-control')); ?>
                <?php echo $form->error($proposal, 'learn', array('style' => 'color: red;')); ?>
            </div>
        </div>


        <div class="form-group">
            <label class="col-lg-3" for="id_cathegory">Categoría</label>
            <div class="col-lg-9">
                <select class="combobox form-control" name="id_cathegory">
                    <option value>Seleccione una Categoría</option>
                    <?php foreach ($listCat as $cathegory) { ?>

                        <option value="<?php echo $cathegory->id_cathegory; ?>"><?php echo $cathegory->na_cathegory; ?></option>

                    <?php } ?>
                </select>
            </div>
        </div>  

        <div class="form-group">
            <label class="col-lg-3" for="id_cathegory">Nivel</label>
            <div class="col-lg-9">
                <select class="combobox form-control" name="id_level">
                    <option value>Seleccione un Nivel</option>
                    <?php foreach ($listNiv as $level) { ?>

                        <option value="<?php echo $level->id_level; ?>"><?php echo $level->na_level; ?></option>

                    <?php } ?>
                </select>
            </div>
        </div> 


        <hr>

        <div class="form-group">
            <label class="col-lg-3" for="user_nickname">Cover:</label>
            <div class="col-lg-7">
                <?php
                $this->widget('ext.EAjaxUpload.EAjaxUpload', array(
                    'id' => 'uploadFile',
                    'config' => array(
                        'action' => Yii::app()->createUrl('ideas/upload'),
                        'allowedExtensions' => array("jpg", "jpeg", "png", "gif"), //array("jpg","jpeg","gif","exe","mov" and etc...
                        'debug' => false,
                        'sizeLimit' => 5 * 1024 * 1024, // maximum file size in bytes
                        'minSizeLimit' => 10 * 1024, // minimum file size in bytes
                        'onComplete' => "js:function(id, fileName, responseJSON){						                        
				                        $('#picture img').attr('src','" . Yii::app()->request->baseUrl . "/images/coversProposals/'+fileName);
				                        $('#picture').css('display','block');
				                   }",
                    )
                ));
                ?>
                <div id='picture' class='picture' style='display:none;float:left;margin-left:20px;'>
                    <img style='width:200px;height:200px' />
                </div>
            </div>						
        </div>	

        <input class="btn btn-warning btn-lg col-lg-offset-3" type="submit" value="Guardar">

<?php $this->endWidget(); ?>
    </div>
</div>
<hr>