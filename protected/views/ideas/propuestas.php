<div class="page-header">
    <h1>Listado de Propuestas</h1>
    <p class="panel-title" style="font-size: 24px;">Listado de próximos cursos.</p>
</div>

<div class="row">
    <div class="col-md-12">
        <form class="form-inline" style="margin-left: 5px;">
            <div class="form-group col-xs-4">
                <input class="form-control " id="exampleInputEmail2" placeholder="Nombre de la propuesta" name="dato" value="<?php
                if (isset($dato)) {
                    echo $dato;
                }
                ?>">
            </div>
            <button type="submit" class="btn btn-default" style="margin-left: -20px;">Buscar</button>
        </form>

        <hr>

        <div style="text-align: center;">
            <a class="btn btn-success" href="<?php echo Yii::app()->urlManager->createUrl('ideas/propuesta'); ?>">Nueva Propuesta</a>
        </div>
        <h2 style="text-align: center;">Propuestas (<?php echo count($list1) ?>)</h2>
        <table class="table table-bordered" style="margin: 2px 0 20px 20px; width: 98%">
            <thead>
                <tr>
                    <th>Categoría</th>
                    <th>Propuesta</th>
                    <th>Contenido</th>
                    <th>Nivel</th>
                    <th>Creación</th>
                    <th style="width: 14%;">Opción</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($list1 as $proposal) { ?>
                    <?php
                    $tags = "";
                    foreach ($proposal->ProposalTags as $tag) {
                        $tags = $tags.", ".$tag->idTag->tag;
                    }
                    ?>
                    <tr>
                        <td>
                            <?php if ($proposal->id_cathegory != null) { ?>
                                <button type="button" class="btn btn-default tags" data-toggle="tooltip" data-placement="top" title="<?php echo trim($tags,",");?>"><?php
                                            echo $proposal->idCathegory->na_cathegory;
                                            ?>
                                </button>
                            <?php } ?>

                        </td>

                        <td><?php echo $proposal->name; ?></td>
                        <td><?php echo $proposal->learn; ?></td>
                        <td>
                            <?php
                            if ($proposal->id_level != null) {

                                echo $proposal->idLevel->na_level;
                            }
                            ?>
                        </td>
                        <td>
                            <?php echo substr($proposal->create, 0, 10); ?>
                        </td>
                        <td>
                            <a type="button" class="btn btn-warning btn-xs" href="<?php echo Yii::app()->urlManager->createUrl('ideas/editarPropuesta', array('id'=>$proposal->id_proposal)); ?>">Editar</a>
                        </td>
                    </tr>
                <?php } ?>
            </tbody>
        </table>

    </div>

    <script type="text/javascript">
        $('.tags').tooltip();
    </script>