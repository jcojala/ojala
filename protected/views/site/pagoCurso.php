<?php
	$this->pageTitle = "Si quieres aprender, te lo enseñamos";
	$paygateway = new StripeWrapper();
	$paygateway->registerScripts('#payment-form');
?>

<div class="home" role="main">
	<div class="row" style="padding-top: 40px;">
		<div class="container course-pay">
			<div class="page-header">
				<h2>Ya estas muy cerca de empezar tu curso, revisa tu compra</h2>
			</div>
			<form accept-charset="UTF-8" action="<?php echo Yii::app()->urlManager->createUrl('site/confirmacion'); ?>" id="payment-form" method="get">
				<div class="col-md-8">
					<!-- -if current_user.becas > 0 || @process.credits > 0 -->
					<!-- %fieldset.credits.alert.alert-success -->
					<!-- %h4 -->
					<!-- Tienes -->
					<!-- %span.amount -->
					<!-- $#{current_user.becas.round} -->
					<!-- USD en cr&eacute;ditos disponibles -->
					<!-- .field -->
					<!-- .input-prepend.input-append -->
					<!-- %span.add-on $ -->
					<!-- = form.number_field :credits -->
					<!-- = form.submit "Usar creditos", :class => "btn btn-success" -->
					<!-- %small.help-block Escribe un número según los cr&eacute;ditos que tengas disponibles. -->
					<!-- %small.help-block -->
					<!-- o puedes -->
					<!-- = link_to "cancelar el uso de tus cr&eacute;ditos", "#", :id => "reset-becas" -->
					<!-- y reservarlos para una pr&oacute;xima vez. -->


					<div class="form-group email-princ" id="formemail">
						<label for="email">&iquest;Cu&aacute;l es tu email principal? <span>Lo necesitamos para el acceso a tu curso</span></label>
			   			<input type="email" class="form-control input-lg" id="email" name="email" value="<?php echo (!Yii::app()->user->isGuest) ? Yii::app()->user->email : ''; ?>">
			  			<p id="error" style="color: red; display: none;">Necesitamos tu correo para confirmar tu acceso.</p>
					</div>

					<h3>Selecciona tu forma de pago</h3>
					<hr>
					<div class="credit-card">
						<input type="hidden" name="id" value="<?php echo $_GET['id']; ?>"/>
		   				<input type="hidden" id="emails" name="email" value="<?php echo (!Yii::app()->user->isGuest) ? Yii::app()->user->email : ''; ?>">

						<div class="payment-errors" style="color: red;">
							<?php if(isset($_GET['error'])) echo $_GET['error'] ?>
						</div>
						<div class="form-group card-number">
							<label for="number">Número de tú tarjeta de cr&eacute;dito:</label>
							<input id="number" autocomplete="off" class="form-control stripe" maxlength="16" size="16" type="text">
						</div>
						<div class="form-group card-expiry-date clearfix" style = "width: 59% !important;">
							<label>Fecha de expiraci&oacute;n:</label>
							<?php echo OjalaUtils::getDropdownMonths('exp-month',array('id'=>'exp-month', 'class'=>'card-expiry-month form-control stripe')) ?>	
							<?php echo OjalaUtils::getExpireYears('exp-year', array('id'=>'exp-year', 'class'=>'card-expiry-year form-control stripe')) ?>	
						</div>
						<div class="form-group card-cvc" style = "float:left">
							<label for="cvc">C&oacute;d de verificaci&oacute;n:</label>
							<input id="cvc" autocomplete="off" class="form-control stripe" maxlength="4" size="4" type="text" >
						</div>
						<input id="button" class="btn btn-warning btn-lg btn-block" name="commit" type="submit" value="Acceso inmediato, comprar ahora">

					</div>
				</form>
				<form accept-charset="UTF-8" action="<?php echo Yii::app()->urlManager->createUrl('site/compraPaypal'); ?>" id="formpaypal" method="get">
					<div class="paypal">
							<input type="hidden" id="amount" name="amount" value="<?php echo ($course->cost / 100); ?>"/>
							<input type="hidden" id="desc" name="desc" value="<?php echo $course->name; ?>"/>
							<input type="hidden" id="id" name="id" value="<?php echo $course->id_course; ?>"/>
                            <img alt="" border="0" height="1" src="<?php echo PaypalWrapper::getDomain() ?>es_XC/i/scr/pixel.gif" width="1">
							<input id="button2" class="btn btn-lg btn-warning" type="submit" value="o usa tu cuenta de Paypal">
                    </div>
            	</form>
			</div>
			<div class="col-md-4 course-details">
				<h6>Estas comprando el curso:</h6>
				<h4><?php echo $course->name; ?></h4>
				<div class="pay-total alert alert-success col-md-5">
					<p>
					Total a pagar:
					<span><?php echo Yii::app()->session['currencyName']; ?></span>
					<strong><i>$</i><?php echo $costo; ?></strong>
					</p>
				</div>
				<img alt="<?php echo $course->name; ?>" src="<?php echo Yii::app()->request->baseUrl; ?>/images/covers/<?php echo $course->cover; ?>" />
				<p class="description">
				Estas apunto de adquirir ingreso ilimitado a este curso, recuerda que no caduca nunca, lo tendr&aacute;s 
				disponible por siempre en tu cuenta; recuerda muy bien el correo y la contrase&ntilde;a con la que adquieres 
				este curso. Tambi&eacute;n tendr&aacute;s disponible el material de descarga y consulta; adem&aacute;s tendr
				&aacute;s al equipo de <strong>Oja.la</strong> listo a ayudarte.
				</p>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
	$("#email").change(function() 
	{
		if($("#email").val()!="")
		{
			$("#email").attr('style', '');
			$("#error").attr('style', 'display: none;');
			$("#emails").attr('value', $("#email").val());
	        $.ajax({
	            'url': '<?php echo Yii::app()->urlManager->createUrl('site/crearUsuario'); ?>?email='+$("#email").val(),
	            'cache': false,
	        });
		}
	});

	$('#button2').click(function(event) 
    {
        if($("#email").val() == '')
        {
			$("#email").attr('style', 'border-color: red;');
			$("#error").attr('style', 'color: red;');
        	return false;
        }
   	});
</script>