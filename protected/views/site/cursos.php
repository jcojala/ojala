<?php
	if(!is_null($cathegory)) {
		$this->pageTitle = $cathegory->na_cathegory=="Otros" ?  "Otros cursos de Tecnología" : "Cursos de ".$cathegory->na_cathegory ;
	} else {
		$this->pageTitle = "Biblioteca de Cursos en Línea";
	}
?>

<!-- Script para el efecto hover en cada curso -->
<script type="text/javascript">
	$(function() {
		var $container	= $('#ib-container .col-md-3'),
		$articles	= $container.children('article'),
		timeout;				
		$articles.on( 'mouseenter', function( event ) {
			var $article	= $(this);
			clearTimeout( timeout );
			timeout = setTimeout( function() {
				if( $article.hasClass('active') ) return false;						
				$articles.not( $article.removeClass('blur').addClass('active') )
				.removeClass('active')
				.addClass('blur');
			}, 40 );
		});

		$container.on( 'mouseleave', function( event ) {
			clearTimeout( timeout );
			$articles.removeClass('active blur');
		});

	});
</script>
<!-- * * * * * * * * * * * -->

<script type="text/javascript">
$(document).ready(function () {
    var length = $('.side').height() - $('.sidebar-filters').height() + $('.side').offset().top;

    $(window).scroll(function () {
        var scroll = $(this).scrollTop();
        var height = $('.sidebar-filters').height() + 'px';
        if (scroll < $('.side').offset().top) {
            $('.sidebar-filters').css({
                'position': 'absolute',
                'top': '0'
            });
        } else if (scroll > length) {
            $('.sidebar-filters').css({
                'position': 'absolute',
                'bottom': '20px',
                'top': 'auto'
            });
        } else {
            $('.sidebar-filters').css({
                'position': 'fixed',
                'top': '20px',
                'height': height
            });
        }
    });
});
</script>

<div class="page-header row">
  <div class="col-md-8">
		<h1>Biblioteca de cursos</h1>
		<p>Este listado muestra todos los cursos que tienes disponible ahora para tomar.</p>
  </div>
  <div class="col-md-4 buscador">
		<form role="search" method="get" action="<?php echo Yii::app()->urlManager->createUrl('site/cursos'); ?>">			
			<div class="input-group input-group-lg">
				<input type="text" class="form-control" placeholder="¿Qué curso buscas?" name="buscar">
			</div>
			<button type="submit" class="input-group-addon glyphicon glyphicon-search"></button>
		</form>
  </div>
</div>


<div class="row wraper">

	<?php if(isset(Yii::app()->session['mexico'])){ ?>
	<!-- Modulo para el lead de Mexcio po rla INCmty -->		
		<div class="alert alert-incmty">
			<h1>Selecciona el curso que quieras</h1>
			<p><strong>Recuerda que tienes acceso a un curso completamente gratis</strong><br> si quieres tener acceso a TODOs nuestros cursos, <a href="https://oja.la/suscripciones?utm_source=website&utm_medium=flyers&utm_content=free-course&utm_campaign=INCmty">revisa nuestros planes de suscripción.</a></p>
			<img class="arrow" src="<?php echo Yii::app()->request->baseUrl; ?>/images/arrow2.png" />
		</div>
	<!-- * * * * * * * -->
	<?php } ?>


	<div class="col-md-2 side">

	<div class="sidebar-filters">

		<!-- Filtros de seccion -->		
		<div class="filters">
			<h4>Secciones</h4>
			<ul class="nav nav-pills nav-stacked" role="navigation">
				<li <?php if(is_null($cathegory) && $buscar==""){ echo 'class="active"'; } ?>><a href="<?php echo Yii::app()->urlManager->createUrl('site/cursos'); ?>">Todos los cursos</a></li>
				<li <?php if(!is_null($cathegory) && $cathegory->slug=="mercadeo"){ echo 'class="active"'; } ?>><a href="<?php echo Yii::app()->urlManager->createUrl('site/cursos', array('cat'=>'mercadeo')); ?>">Mercadeo</a></li>
				<li <?php if(!is_null($cathegory) && $cathegory->slug=="social-media"){ echo 'class="active"'; } ?>><a href="<?php echo Yii::app()->urlManager->createUrl('site/cursos', array('cat'=>'social-media')); ?>">Social Media</a></li>
				<li <?php if(!is_null($cathegory) && $cathegory->slug=="desarrollo-web"){ echo 'class="active"'; } ?>><a href="<?php echo Yii::app()->urlManager->createUrl('site/cursos', array('cat'=>'desarrollo-web')); ?>">Desarrollo web</a></li>
				<li <?php if(!is_null($cathegory) && $cathegory->slug=="ios"){ echo 'class="active"'; } ?>><a href="<?php echo Yii::app()->urlManager->createUrl('site/cursos', array('cat'=>'ios')); ?>">iPhone apps</a></li>
				<li <?php if(!is_null($cathegory) && $cathegory->slug=="android"){ echo 'class="active"'; } ?>><a href="<?php echo Yii::app()->urlManager->createUrl('site/cursos', array('cat'=>'android')); ?>">Android apps</a></li>
				<li <?php if(!is_null($cathegory) && $cathegory->slug=="otros"){ echo 'class="active"'; } ?>><a href="<?php echo Yii::app()->urlManager->createUrl('site/cursos', array('cat'=>'otros')); ?>">Otros</a></li>
			</ul>
		</div>

		<!-- Filtros de orden -->
		<div class="order">
			<?php 
				if(!is_null($cathegory) && $cathegory->slug!="")
					{ $acat=array('cat'=>$cathegory->slug);}
				else
					{ $acat=array();}
			?>
			<h4>Filtros</h4>
			<ul class="nav nav-pills nav-stacked">
				<li><a href="<?php echo Yii::app()->urlManager->createUrl('site/cursos', $acat + array('orden'=>'alfabetico')); ?>">Orden alfabético</a></li>
				<li><a href="<?php echo Yii::app()->urlManager->createUrl('site/cursos', $acat + array('orden'=>'dificultad')); ?>">Dificultad</a></li>
				<li><a href="<?php echo Yii::app()->urlManager->createUrl('site/cursos', $acat + array('orden'=>'fecha')); ?>">Fecha de publicación</a></li>
			</ul>
		</div>
		<!-- * * * * * * -->
		
		<div class="recomendacion alert alert-info">
			<a href="#" data-toggle="modal" data-target="#sugerenciaCurso">¿Quiéres que hagamos un curso? ¿Cuál?</a>
		</div>
		<!-- * * * * * * -->
						
	</div>

	</div>






	<div class="col-md-10 col-md-offset-2">

		<!-- Blankslate cuando una busqueda no tiene resultados -->
		<?php if(count($list)==0){ ?>

			<div class="blankslate cursos">
				<h5>Ya estamos trabajando en el curso que buscas, ahora mismo no está disponible. Mientras, aquí hay algunos temas que te pueden ser útiles</h5>
	
				<div class="links">
					<a href="<?php echo Yii::app()->urlManager->createUrl('site/cursos', array('cat'=>'mercadeo')); ?>" class="mercadeo">Mercadeo</a>
					<a href="<?php echo Yii::app()->urlManager->createUrl('site/cursos', array('cat'=>'social-media')); ?>" class="socialmedia">Social Media</a>
					<a href="<?php echo Yii::app()->urlManager->createUrl('site/cursos', array('cat'=>'desarrollo-web')); ?>" class="web">Desarrollo Web</a>
					<a href="<?php echo Yii::app()->urlManager->createUrl('site/cursos', array('cat'=>'ios')); ?>" class="ios">iOS / iPhone</a>
					<a href="<?php echo Yii::app()->urlManager->createUrl('site/cursos', array('cat'=>'android')); ?>" class="android">Android</a>
					<a href="<?php echo Yii::app()->urlManager->createUrl('site/cursos', array('cat'=>'otros')); ?>" class="otros">Otros</a>
				</div>
				
				<!--<div class="alert alert-info">
						<h4><a href="#" data-toggle="modal" data-target="#sugerenciaCurso">¿Ninguno de estos temas te interesa? Cuéntanos que curso quieres que hagamos<strong>Usa el</strong></a></h4>
				</div>-->
			</div>

		<?php }else if($buscar!=""){ ?>
			<div class="resultado alert alert-info">
				<p>Estos son los cursos que tenemos sobre: <strong>"<?php echo $buscar; ?>"</strong></p>
			</div>

		<?php } ?>
	
		<section class="ib-container row" id="ib-container">
			
			<!-- Modulo de cada uno de los cursos en los listados -->
			<?php foreach($list as $item){ ?>

			<?php $cover = isset($item['cover']) ? $item['cover'] : 'default.jpg'; ?>
				<div class="col-md-4">
					<article class='mod-course'>
						
						<div class='thumbnail'>
							<div class='img'>
								<a href="<?php echo Yii::app()->urlManager->createUrl('site/curso', array('slug'=>$item['slug'], 'cat'=>$item['cat'])); ?>" alt="..." class="img-rounded">
									<img class="img-thumbnail" alt="<?php echo $item['name']; ?>" src="<?php echo Course::coverUrl($cover, 'medium') ?>">
								</a>
							</div>	

							<div class='caption'>
								<h6><?php echo $item['interesados']; ?> personas interesadas en:</h6>
								<h4>
									<a href="<?php echo Yii::app()->urlManager->createUrl('site/curso', array('slug'=>$item['slug'], 'cat'=>$item['cat'])); ?>">
										<?php echo $item['name']; ?>
									</a>
								</h4>
								
								<hr>
								<a href="<?php echo Yii::app()->urlManager->createUrl('site/curso', array('slug'=>$item['slug'], 'cat'=>$item['cat'])); ?>" class="btn btn-warning pull-right">Más información →</a>
							</div>
						</div>
					</article>
				</div>
			<?php } ?>

			<?php if($paginas>1){ 
				if(isset($orden) && $orden!="")
				{ $orden=array('orden'=>$orden); }
				else
				{ $orden=array(); }
			?>
				<ul class="pagination pagination-lg">
		  		<li <?php if($paginaactual==1){echo 'class="disabled"'; } ?>>
		  			<a href="<?php echo Yii::app()->urlManager->createUrl('site/cursos', $acat); ?>">&laquo;</a>
		  		</li>
		  		<?php for ($i=$desde; $i <= $hasta; $i++) { ?>
		  			<?php if($i==1){ ?>
		  				<li <?php if($i==$paginaactual){echo 'class="active"'; } ?>>
		  					<a href="<?php echo Yii::app()->urlManager->createUrl('site/cursos', $acat + $orden); ?>"><?php echo $i; ?></a>
		  				</li>
		  			<?php }else{ ?>
		  				<li <?php if($i==$paginaactual){echo 'class="active"'; } ?>>
		  					<a href="<?php echo Yii::app()->urlManager->createUrl('site/cursos', $acat + $orden + array('p'=>$i)); ?>"><?php echo $i; ?></a>
		  				</li>
		  			<?php } ?>
		  		<?php } ?>
		  		<li <?php if($paginaactual==$paginas){echo 'class="disabled"'; } ?>>
		  			<a href="<?php echo Yii::app()->urlManager->createUrl('site/cursos', $acat + $orden + array('p'=>$paginas)); ?>">&raquo;</a>
		  		</li>
				</ul>
			<?php } ?>
		</section>

	</div>

</div>



				<div class="modal fade" id="sugerenciaCurso" tabindex="-1" role="dialog" aria-labelledby="sugerenciaCurso" aria-hidden="true">
					<div class="modal-dialog">
						<div class="modal-content">
							<div class="modal-body">
								<p><strong>Hola <?php //echo Yii::app()->user->name; ?></strong></p>
								<p>Cuéntanos cuál curso quisieras que hicieramos para ti? o cuáles temas te interesan; de esta forma podemos planear nuevos cursos que te sean útiles.</p>
								<form>
									<textarea class="form-control" placeholder="Escribe aquí tu sugerencia ..." rows="5" name="curso" id="curso"></textarea>
								</form>
							</div>
							<div class="modal-footer">
								<div class="btn-group">
									<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar ventana</button>
									<button type="button" class="btn btn-primary enviar">Enviar sugerencia</button>
								</div>
							</div>
						</div>
					</div>
				</div>

				<div class="modal fade" id="gracias" tabindex="-1" role="dialog" aria-labelledby="gracias" aria-hidden="true">
					<div class="modal-dialog">
						<div class="modal-content">
							<div class="modal-body">
								<h4 style="text-align: center;margin-top: 30px;"><strong>Gracias <?php if(!Yii::app()->user->isGuest){ echo Yii::app()->user->name.' '; } ?>por tu sugerencia</strong></h4>
								<p id="mensaje"></p>
							</div>
							<div class="modal-footer">
								<div class="btn-group">
									<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar ventana</button>
								</div>
							</div>
						</div>
					</div>
				</div>

<script type="text/javascript">
    $(function() {
        $(".enviar").click(function()
        {
            var curso=$("#curso").val();
            $.ajax({
                'url': '<?php echo Yii::app()->urlManager->createUrl("site/sugerenciaCurso"); ?>'+'?curso='+curso,
                'cache': false,
                success: function (data) 
                {
                 	$('#sugerenciaCurso').modal('hide');
                 	$('#gracias').modal('show');
                },
            });
        });
    });
</script>

<!-- Start of GetKudos Script -->
<script>
(function(w,t,gk,d,s,fs){if(w[gk])return;d=w.document;w[gk]=function(){
(w[gk]._=w[gk]._||[]).push(arguments)};s=d.createElement(t);s.async=!0;
s.src='//static.getkudos.me/widget.js';fs=d.getElementsByTagName(t)[0];
fs.parentNode.insertBefore(s,fs)})(window,'script','getkudos');

getkudos('create', 'ojala');
</script>
<!-- End of GetKudos Script -->