<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
	</head>
	<body>

	<div class="invoice">
			<div class="container">				
				<div class="top">
					<div class="col-8">
						<h1><span>Factura N°: </span><?php echo $pay->invoice_number; ?></h1>					
						<p> Emisión: <?php echo ucwords(Yii::app()->dateFormatter->format('EEEE dd MMMM yyyy',$pay->date)) ?></p>
					</div>
					<div class="pull-right">
						<img alt="Oja.la" src="<?php echo Yii::app()->request->baseUrl; ?>/images/brand-invoice.png">
					</div>
				</div>
				<div class="data">
					<div class="col">
						<h2>PARA:</h2>
						<p><?php if(isset($payinfo->id))echo $payinfo->id; ?></p>					
						<p><strong><?php echo $payinfo->name ?></strong></p>
						<p><?php echo $payinfo->email; ?></p>
						<p><?php echo nl2br($payinfo->address); ?></p>
						<p><?php echo $payinfo->phone; ?></p>
					</div>
					<div class="pull-right" style="padding: 20px">
						<h2>DE:</h2>
						<p><strong>OJA.LA EDU INC.</strong></p>
						<p>951 Brickell Ave</p>
						<p>Miami, FL33131</p>
						<p>United States</p>
					</div>
				</div>
				<div class="products">							
					<table>
						<tr>
							<th>Cantidad</th>
							<th>Descripción</th>
							<th>Precio Unidad</th>
							<th>TOTAL</th>
						</tr>
						<tr>
							<td>1</td>
							<td><?php echo $st->na_stype ?>
								( <?php echo ucwords(Yii::app()->dateFormatter->format('MMMM dd / yyyy',strtotime($pay->date."-1 month"))) ?>
								- <?php echo ucwords(Yii::app()->dateFormatter->format('MMMM dd / yyyy',strtotime($pay->date))) ?> )</td>
							<td>
								<?php 
									switch($pay->id_ptype){
										case 3: echo "MXN";
												break;
										default: echo "USD";
												break;
									} 
								?>
								$<?php echo $pay->amount; ?>
							</td>
							<td>
								<?php 
									switch($pay->id_ptype){
										case 3: echo "MXN";
												break;
										default: echo "USD";
												break;
									} 
								?>
								$<?php echo $pay->amount; ?>
							</td>
						</tr>
						<tr>
							<td></td>
							<td></td>
							<td></td>
							<td></td>
						</tr>
						<tr>
							<td></td>
							<td></td>
							<td></td>
							<td></td>
						</tr>
						<tr>
							<td></td>
							<td></td>
							<td></td>
							<td></td>
						</tr>
						<tr>
							<td></td>
							<td></td>
							<td></td>
							<td></td>
						</tr>						
						<tr>
							<td></td>
							<td></td>
							<td></td>
							<td></td>
						</tr>
					</table>

				</div>
				<div class="bottom">
					<div class="col-4 pull-right">
						<table>
							<tr>
								<th></th>
								<th>TOTAL</th>
							</tr>
							<tr>
								<td>Subtotal</td>
								<td>
									<?php 
										switch($pay->id_ptype){
											case 3: echo "MXN";
													break;
											default: echo "USD";
													break;
										} 
									?>
									$<?php echo $pay->amount; ?>
								</td>
							</tr>
							<tr>
								<td>TAX</td>
								<td>$00.00</td>
							</tr>
							<tr class="total">
								<td>TOTAL</td>
								<td>
									<?php 
										switch($pay->id_ptype){
											case 3: echo "MXN";
													break;
											default: echo "USD";
													break;
										} 
									?>
									$<?php echo $pay->amount; ?>
								</td>
							</tr>
						</table>
					</div>				
					<div class="col-8">					
						<h6><i>Observaciones:</i></h6>
						<p>Dentro de la suscripción, se tendrá acceso a cursos y diplomados de desarrollo para teléfonos en iOS, Android, Phonegap, desarrollo y programación para web desde el inicio, Wordpress, Joomla, nuevos cursos sobre aplicaicones y ejercicios prácticos en iOS7, Kikat de Android, etc
						<br/> <br/>
						<small >Oja.la Edu Inc. - 951 Brickell Ave Miami, FL 33131</small>
						</p>					
					</div>

				</div>

			</div>
		</div>

	</body>
</html>