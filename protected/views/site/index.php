<?php $this->pageTitle = "Si quieres aprender, te lo enseñamos"; ?>

<?php
	//OjalaScripts::registerRemarketing();
	//OjalaScripts::registerAdroll();
	$menu = isset($menu) ? $menu : 'byRole';
	$this->widget('MainMenu', array('show'=>$menu));
	//OjalaScripts::registerIntercom();
?>

<div class="row home">
	<div class="container-fluid">
	
		<div class="fold">
			<h1>Aprende paso a paso <br>nuevas habilidades para tu carrera</h1>
			<p>Mejora tus oportunidades con la mejor librería de video cursos y diplomados 100% en español</p>
			<a href="<?php echo Yii::app()->request->baseUrl; ?>/cursos" class="btn btn-lg btn-bibloteca">Conoce nuestros cursos</a>
		</div>


		<div class="details">
			<div class="col-md-4 clases">
				<div class="each">
					<h2>+5885 Clases</h2>
					<p>Clases paso a paso, a tu propio ritmo. Aprenderás cada detalle necesario para lograr tu objetivo</p>
				</div>
			</div>
			<div class="col-md-4 cursos">
				<div class="each">
					<h2>+212 Cursos en video</h2>
					<p>Encuentra el que necesitas, en nuestra biblioteca. Agregamos nuevos cursos todas las semanas</p>
				</div>
			</div>
			<div class="col-md-4 chats">
				<div class="each">
					<h2>+5614 Chats</h2>
					<p>No estarás solo.<br>Siempre habrá alguien que te ayude con tus preguntas y sugerencias</p>
				</div>
			</div>
		</div>


		<div class="profundizar">
			<h2>Puedes profundizar en un tema en específico</h2>

			<div class="diplomados">
			<div class="col-md-3">
			<article class="mod-course">
				<div class="thumbnail">
					<div class="img">
						<a href="<?php echo Yii::app()->request->baseUrl; ?>/diplomado/diplomado-de-desarrollo-de-apps-para-iphone-y-ipad-en-ios7?landing=diplomadoios" class="img-rounded">
							<img class="img-thumbnail" alt="Diplomado de desarrollo de apps para iPhone y iPad en iOS7" src="<?php echo Yii::app()->request->baseUrl; ?>/images/covers/Ewq8xpJ92.png">
						</a>
					</div>

					<div class="caption">
						<h4>
							<a href="<?php echo Yii::app()->request->baseUrl; ?>/diplomado/diplomado-de-desarrollo-de-apps-para-iphone-y-ipad-en-ios7?landing=diplomadoios">
								Diplomado de desarrollo de apps para iPhone y iPad en iOS7</a>
						</h4>
						<a href="<?php echo Yii::app()->request->baseUrl; ?>/diplomado/diplomado-de-desarrollo-de-apps-para-iphone-y-ipad-en-ios7?landing=diplomadoios" class="btn btn-warning pull-right">Más información →</a>
					</div>
				</div>
			</article>
			</div>
			<div class="col-md-3">
			<article class="mod-course">
				<div class="thumbnail">
					<div class="img">
						<a href="<?php echo Yii::app()->request->baseUrl; ?>/diplomado/diplomado-de-desarrollo-de-apps-para-android?landing=diplomadoAndroid" class="img-rounded">
							<img class="img-thumbnail" alt="Aprende a diseñar como prefesional una revista digital" src="<?php echo Yii::app()->request->baseUrl; ?>/images/covers/android-diplo.jpg">
						</a>
					</div>

					<div class="caption">
						<h4>
							<a href="<?php echo Yii::app()->request->baseUrl; ?>/diplomado/diplomado-de-desarrollo-de-apps-para-android?landing=diplomadoAndroid">
								Diplomado de desarrollo de apps para Android con Eclipse</a>
						</h4>
						<a href="<?php echo Yii::app()->request->baseUrl; ?>/diplomado/diplomado-de-desarrollo-de-apps-para-android?landing=diplomadoAndroid" class="btn btn-warning pull-right">Más información →</a>
					</div>
				</div>
			</article>
			</div>
			<div class="col-md-3">
			<article class="mod-course">
				<div class="thumbnail">
					<div class="img">
						<a href="<?php echo Yii::app()->request->baseUrl; ?>/diplomado/diplomado-de-desarrollo-de-paginas-web?landing=diplomadoweb" alt="..." class="img-rounded">
							<img class="img-thumbnail" alt="Aprende a diseñar como prefesional una revista digital" src="<?php echo Yii::app()->request->baseUrl; ?>/images/covers/web76.jpg">
						</a>
					</div>

					<div class="caption">
						<h4>
							<a href="<?php echo Yii::app()->request->baseUrl; ?>/diplomado/diplomado-de-desarrollo-de-paginas-web?landing=diplomadoweb">
								Diplomado paso a paso de desarrollo de páginas y productos web</a>
						</h4>
						<a href="<?php echo Yii::app()->request->baseUrl; ?>/diplomado/diplomado-de-desarrollo-de-paginas-web?landing=diplomadoweb" class="btn btn-warning pull-right">Más información →</a>
					</div>
				</div>
			</article>
			</div>
			<div class="col-md-3">
			<article class="mod-course">
				<div class="thumbnail">
					<div class="img">
						<a href="<?php echo Yii::app()->request->baseUrl; ?>/diplomado/diplomado-on-line-de-gestion-de-redes-sociales?landing=diplomadosm" alt="..." class="img-rounded">
							<img class="img-thumbnail" alt="Aprende a diseñar como prefesional una revista digital" src="<?php echo Yii::app()->request->baseUrl; ?>/images/covers/nuevo-socialm.jpg">
						</a>
					</div>

					<div class="caption">
						<h4>
							<a href="<?php echo Yii::app()->request->baseUrl; ?>/diplomado/diplomado-on-line-de-gestion-de-redes-sociales?landing=diplomadosm">
								Diplomado on-line de Community Management y gestión de redes</a>
						</h4>
						<a href="<?php echo Yii::app()->request->baseUrl; ?>/diplomado/diplomado-on-line-de-gestion-de-redes-sociales?landing=diplomadosm" class="btn btn-warning pull-right">Más información →</a>
					</div>
				</div>
			</article>
			</div>
		</div>
		</div>


		<div class="trainer">
			<h2>Tendrás un asesor personal tiempo completo</h2>
			<div class="ayuda">
				<div class="col-md-6"><img src="<?php echo Yii::app()->request->baseUrl; ?>/images/home-asesor.jpg"></div>
				<div class="col-md-6">
					<h3>Nunca estarás solo. Estarémos en contácto cuando necesites ayuda, vía teléfono, chat, e-mail o Skype.</h3>
					<div class="each">
						<img src="<?php echo Yii::app()->request->baseUrl; ?>/images/icon-certificado.gif">
						<p>Certificación digital, con numeración única, al terminar cada curso y diplomado, validada por nuestros instructores.</p>
					</div>
					<div class="each">
						<img src="<?php echo Yii::app()->request->baseUrl; ?>/images/icon-material.gif">
						<p>Podrás descargar los archivos de los proyectos, y los usados por los instructores, en cada curso.</p>
					</div>
				</div>
			</div>
		</div>

		<div class="videos">
			<h2>Clases 100% en español, no necesitas experiencia</h2>
			<div class="clases">
				<iframe src="//fast.wistia.net/embed/iframe/smgq3cfnza" allowtransparency="true" frameborder="0" scrolling="no" class="wistia_embed" name="wistia_embed" allowfullscreen mozallowfullscreen webkitallowfullscreen oallowfullscreen msallowfullscreen width="700" height="394"></iframe>
			</div>
		</div>

		<div class="estudiantes">
			<h2>Más de 1500 estudiantes ya están mejorando sus habilidades</h2>
			<div class="brands">
				<img alt="BBC News" class="bbc" src="images/brand-bbc.png" />
				<img alt="Enter.co" class="enter" src="images/brand-enter.png" />
				<img alt="The NextWeb" class="nxtweb" src="images/brand-nextweb.png" />
				<img alt="PulsoSocial" class="pulso" src="images/brand-pulso.png" />
				<img alt="Red Innova" class="innova" src="images/brand-innova.png" />
				<img alt="Emol" class="emol" src="images/brand-emol.png" />
				<br>
				<img alt="Colombia.Inn" class="colombia-inn" src="images/Colombia-Inn.png" />
				<img alt="El Economista Mx" class="economista" src="images/eleconomista.gif"/>
				<img alt="Ae Tecno" class="aetecno" src="images/aetecno.gif"/>
				<img alt="It/Users" class="itusers" src="images/itusers.gif"/>
				<img alt="StartUp Chile" class="startupchile" src="images/startupchile.gif"/>

			</div>
	    <div class="row kudos">
			
			<!-- Start of GetKudos Inline Script -->
			<div class="getkudos-inline" data-site-name="ojala" data-layout="inline"></div>

			<script>
				(function(w,t,gk,d,s,fs){if(w[gk])return;d=w.document;w[gk]=function(){
				(w[gk]._=w[gk]._||[]).push(arguments)};s=d.createElement(t);s.async=!0;
				s.src='//static.getkudos.me/widget.js';fs=d.getElementsByTagName(t)[0];
				fs.parentNode.insertBefore(s,fs)})(window,'script','getkudos');

				getkudos('parse');
			</script>
			<!-- End of GetKudos Inline Script -->

    	</div>
		</div>

		<div class="final">
			<h2>¿Listo para empezar?</h2>
			<a href="<?php echo Yii::app()->request->baseUrl; ?>/cursos" class="btn btn-lg btn-bibloteca">Conoce nuestros cursos</a>
			<p>¿Aún tienes preguntas? usa nuestro chat. Un asesor está listo para ayudarte</p>
		</div>
	</div>
</div>