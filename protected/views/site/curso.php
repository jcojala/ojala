<?php $this->pageTitle=$course->name; ?>

<script type="text/javascript">
$(document).ready(function(){
    $('.fold-course').mousemove(function(e){
        var x = -(e.pageX + this.offsetLeft) / 60;
        var y = -(e.pageY + this.offsetTop) / 60;
        $(this).css('background-position', x + 'px ' + y + 'px');
    });
});
</script>

<!-- Info basica del curso -->
<div class="fold-course">
	<h6><a href="<?php echo Yii::app()->urlManager->createUrl('site/cursos', array('cat'=>$cathegory->slug)); ?>"><span class="glyphicon glyphicon-chevron-left"></span><?php echo $cathegory->na_cathegory; ?></a></h6>
	<h1 itemprop="name"><?php echo $course->name; ?></h1>
	<small>¿Cómo vas con tu curso?</small>

	<div class="row" style="margin:0 !important;">
		<div class="progress col-md-9" style="padding:0;">
	  		<div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="<?php echo $cs->progress; ?>" aria-valuemin="0" aria-valuemax="100" style="width: <?php echo $cs->progress; ?>%">
	    		<span class="sr-only"><?php echo $cs->progress; ?>% Complete (success)</span>
	  		</div>
		</div>
		<div class="col-md-3">
			<?php if(is_null(($cs->certificate))){ ?>
				<a href="#" class="btn btn-block btn-success btn-certificado solicitar" data-toggle="modal" data-target="#certificado" <?php if($cs->progress < 90){ echo 'disabled="disabled"'; } ?>>Descargar tu Certificado</a>
			<?PHP }else{ ?>
				<a href="#" class="btn btn-block btn-success btn-certificado solicitar" data-toggle="modal" data-target="#certificado">Descargar tu Certificado</a>
			<?PHP } ?>
			
			<div class="modal fade" id="certificado" tabindex="-1" role="dialog" aria-labelledby="certificado" aria-hidden="true">
			  	<div class="modal-dialog">
			    	<div class="modal-content">
			      		<div class="modal-body">
			        		<h2>¡¡¡¡ FELICITACIONES !!!!</h2>
							<p><?php echo Yii::app()->user->name; ?>, vamos a revisar los datos de tu certificado y te lo enviaremos en las próximas 24hrs, debes estar atento a tu correo electrónico. <br><strong>(Recuerda tu correo es: <?php echo Yii::app()->user->email; ?>)</strong></p>
			      			<input type="hidden" id="curso" value="<?php echo $course->name; ?>" />
			      			<input type="hidden" id="idcs" value="<?php echo $cs->id_csuscription; ?>" />
			      			<img src="<?php echo Yii::app()->request->baseUrl; ?>/images/aplausos.gif">
			      		</div>
			      		<div class="modal-footer">
			        		<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar ventana</button>
			      		</div>
			    	</div>
			  	</div>
			</div>
		</div>
	</div>
</div>
<!-- **** -->

<script type="text/javascript">
    $(function() {
        $(".solicitar").click(function()
        {
            var idcs=$("#idcs").attr('value');
            $.ajax({
                'url': '<?php echo Yii::app()->urlManager->createUrl("site/solicitarCertificado"); ?>'+'?idcs='+idcs,
                'cache': false,
                success: function (data) 
                {
                 	$('.solicitar').attr('style', 'display: none;');
                },
            });
        });
    });
</script>

<div class="row">
	<div class="container" style="padding-top:0;">

		<div class="col-md-9">

			<!-- BTN proxima clase o de inicio -->
			<?php if($cs->progress < 98){ ?>
				<div class="next-class">
					<?php if(!$last){ $last = $list[0]; ?>
						<div class="status">
							<a class="btn btn-lg btn-block btn-warning" href="<?php echo Yii::app()->urlManager->createUrl('site/clase', array('slugc'=>$last['slugc'], 'slug'=>$course->slug, 'cat'=>$cathegory->slug)); ?>">
								<i></i>
								<strong>Comienza tu primera clase:</strong>
								<span><?php echo $last['na_topic']; ?></span>
							</a>
						</div>
			 		<?php }else{ ?>
						<div class="status">
							<a class="btn btn-lg btn-block btn-warning" href="<?php echo Yii::app()->urlManager->createUrl('site/clase', array('slugc'=>$last['slugc'], 'slug'=>$course->slug, 'cat'=>$cathegory->slug)); ?>">
								<strong>Continúa donde te quedaste, próxima clase:</strong>
								<span><?php echo $last['na_topic']; ?></span>
							</a>
						</div>
			  		<?php } ?>
				</div>
		  	<?php } ?>
			<!-- **** -->

			<!-- Programa del curso -->
			<div class="program">
				<div class="row">

					<div class="col-md-12 agenda">
						<h3>Programa del curso:</h3>
						<ul class="lessons">
							<?php $lesson=""; foreach ($list as $topic) { ?>
								<li class="each item">
									<?php if($lesson !== $topic['na_lesson']){ $lesson = $topic['na_lesson']; ?>
										<div class="name">
											<h5><?php echo $topic['na_lesson']; ?></h5>
										</div>
									<?php } ?>
									<ul class="topics each-topic">
										<li class="item">
											<div class="col-md-9">
												<h4>
													<a class="topic-name" href="<?php echo Yii::app()->urlManager->createUrl('site/clase', array('slugc'=>$topic['slugc'], 'slug'=>$course->slug, 'cat'=>$cathegory->slug)); ?>">
														<?php echo $topic['na_topic']; ?>
	                    								<span datetime="">Duración: <?php echo Dates::secondsToTime($topic['duration']); ?></span>
													</a>
												</h4>
											</div>
											<div class="col-md-3">
												<div class="advance">
													<p>Has visto el <?php echo $topic['progress']; ?>%</p>
													<div class="full">
														<div class="done" style="width: <?php echo $topic['progress']; ?>%"></div>
													</div>
												</div>
											</div>
										</li>
									</ul>
								</li>
							<?php } ?>
						</ul>
					</div>

				</div>
			</div>
			<!-- **** -->
		</div>

		<!-- Sidebar del curso -->
		<div class="col-md-3 sidebar">
			<div class="mod info">
				<?php if(isset(Yii::app()->user->utype) && (Yii::app()->user->utype==='Administrador' || Yii::app()->user->utype==='AAC')){ ?>
					<h4>Administración</h4>
					<a type="button" class="btn btn-danger btn-block" href="<?php echo Yii::app()->urlManager->createUrl('admin/editarCurso', array('id'=>$course->id_course)); ?>">Editar Curso</a>
				<?php } ?>
				<h4>Información de Este curso</h4>
				<dl>
					<dt><span class="glyphicon glyphicon-signal"></span>Nivel:</dt>
					<dd><?php echo $level->na_level; ?></dd>
				</dl>
				<dl>
					<dt><span class="glyphicon glyphicon-time"></span>Duración:</dt>
					<dd><?php echo Dates::secondsToTime2($course->duration); ?></dd>
				</dl>
				<dl>
					<dt><span class="glyphicon glyphicon-tag"></span>Categoría:</dt>
					<dd><a href="<?php echo Yii::app()->urlManager->createUrl('site/cursos', array('cat'=>$cathegory->slug)); ?>"><?php echo $cathegory->na_cathegory; ?></a></dd>
				</dl>
				<dl>
					<dt><span class="glyphicon glyphicon glyphicon-info-sign"></span>Clases:</dt>
					<dd><?php echo count($list); ?> Clases</dd>
				</dl>
				<h5>Sobre este curso:</h5>
				<p><?php echo $course->description; ?></p>
			</div>
			<?php $this->widget('ShareThis'); ?>


<!-- Start of GetKudos Script -->
<script>
(function(w,t,gk,d,s,fs){if(w[gk])return;d=w.document;w[gk]=function(){
(w[gk]._=w[gk]._||[]).push(arguments)};s=d.createElement(t);s.async=!0;
s.src='//static.getkudos.me/widget.js';fs=d.getElementsByTagName(t)[0];
fs.parentNode.insertBefore(s,fs)})(window,'script','getkudos');

getkudos('create', 'ojala');
</script>
<!-- End of GetKudos Script -->