<?php
	$this->pageTitle = $course->name;
	$payments = new StripeWrapper;
?>
<style type="text/css">body{overflow-x: hidden !important;}</style>

<div class="portada">

	<!-- Primer fold -->
	<div class="fold">
		<div class="content">
		<h1 itemprop="name"><?php echo $course->name; ?></h1>
		<p>100% en español - Certificación al completar el curso</p>
		<div class="buttons">
			<a href="<?php echo Yii::app()->urlManager->createUrl('site/suscripciones'); ?>" class="btn btn-success btn-block btn-lg">Toma este curso ahora!</a>
		</div>
		</div>
	</div>
	<!-- Final del primer fold -->

	


	<!-- Columna izquierda -->
	<div class="col-md-3 left">
		
		<div class="mod descripcion">
			<h4>Sobre este curso</h4>
			<img alt="<?php echo $course->name; ?>" src="<?php echo Yii::app()->request->baseUrl; ?>/images/covers/<?php echo $course->cover; ?>" />
			<dl>
				<dt><span class="glyphicon glyphicon-signal"></span>Nivel:</dt>
				<dd><?php if($level != null ){ echo $level->na_level; } ?></dd>
			</dl>
			<dl>
				<dt><span class="glyphicon glyphicon-time"></span>Duración:</dt>
				<dd><?php echo Dates::secondsToTime2($course->duration); ?></dd>
			</dl>
			<dl>
				<dt><span class="glyphicon glyphicon-tag"></span>Categoría:</dt>
				<dd><a href="<?php echo Yii::app()->urlManager->createUrl('site/cursos', array('cat'=>$cathegory->slug)); ?>"><?php echo $cathegory->na_cathegory; ?></a></dd>
			</dl>
			<dl>
				<dt><span class="glyphicon glyphicon glyphicon-info-sign"></span>Clases:</dt>
				<dd><?php echo count($list); ?> Clases</dd>
			</dl>
			<p><?php echo $course->description; ?></p>
		</div>

		<?php if($course->requirements!=""){ ?>
			<div class="mod">
				<h4>¿Cuáles son los requisitos?</h4>
				<p><?php echo $course->requirements; ?></p>
			</div>
		<?php } ?>
		<?php if($course->learn!=""){ ?>
			<div class="mod">
				<h4>¿Qué voy a aprender?</h4>
				<p><?php echo $course->learn; ?></p>
			</div>
		<?php } ?>
		<?php if($course->directed!=""){ ?>
			<div class="mod">
				<h4>¿A quién va dirigido?</h4>
				<p><?php echo $course->directed; ?></p>
			</div>
		<?php } ?>
	</div>
	<!-- Final columna izquierda -->


	<!-- Columna central -->
	<div class="col-md-6 center">
		<div class="mod programa">
			<h4>Programa del curso</h4>
			<div class="lessons">
				<?php $lesson=""; foreach ($list as $topic) { ?>

				<?php if($lesson !== $topic['na_lesson']){ $lesson = $topic['na_lesson']; ?>
				<div class="title">
					<h5><?php echo $topic['na_lesson']; ?></h5>
				</div>
				<?php } ?>

				<?php if($topic['public']=="1"){ ?> <a href="<?php echo Yii::app()->urlManager->createUrl('site/clase', array('slugc'=>$topic['slugc'], 'slug'=>$course->slug, 'cat'=>$cathegory->slug)); ?>" class="publico"> <?php } ?>
				<div class="topic">
					<p><?php echo $topic['na_topic']; ?></p>
					<ul>
						<li><span class="glyphicon glyphicon-time"></span>Duración: <?php echo Dates::secondsToTime($topic['duration']); ?></li>
						<?php if($topic['package']!=""){ ?>
						<li><span class="glyphicon glyphicon-folder-open"></span>Con material de descarga</li>
						<?php } ?>
					</ul>
				</div>
				<?php if($topic['public']=="1"){ ?> </a> <?php } ?>

				<?php } ?>
			</div>
		</div>
	</div>
	<!-- Final columna central -->



	<!-- Final columna derecha -->
	<div class="col-md-3 right">

		<div class="mod recibes">
			
			<h4>¿Qué recibirás?</h4>
			<div class="each">
				<img alt="Faq-videos" src="<?php echo Yii::app()->request->baseUrl; ?>/images/faq-videos-b547ed0a34cfedacc1b4e3c229eab249.png">
				<strong>Videocursos paso a paso</strong>
				<p>Contenido 100% Online, videos de alta calidad, que puedes ver cuantas veces quieras a la hora que prefieras.</p>
			</div>
			<div class="each">
				<img alt="Faq-expire" src="<?php echo Yii::app()->request->baseUrl; ?>/images/faq-expire-dafebe946557b41ffedc11867b329f34.png">
				<strong>Acceso ilimitado</strong>
				<p>Tus cursos son tuyos, nunca expirarán, disponibles las 24hrs del día sin restricciones de ningún tipo.</p>
			</div>
			<div class="each">
				<img alt="Faq-dispositivos" src="<?php echo Yii::app()->request->baseUrl; ?>/images/faq-dispositivos-192843e5e8532ec4e879c88865338260.png">
				<strong>Disponibilidad total</strong>
				<p>Puedes tomar tus cursos desde cualquier dispositivo, teléfono, tablet, o computador, incluso desde varios a la vez.</p>
			</div>
			<div class="each">
				<img alt="Faq-certificate" src="<?php echo Yii::app()->request->baseUrl; ?>/images/faq-certificate-0445ea4296df636f0915d64816fe66f0.png">
				<strong>Certificación al terminar</strong>
				<p>Cuando completes el 100% de tu curso, el sistema te dará acceso a tu certificación para que la descargues e imprimas.</p>
			</div>
		</div>
		
		<?php $this->widget('ShareThis'); ?>

		<div class="mod questions">
			<h4>¿Alguna pregunta?</h4>
			<p>
				Escríbenos a
				<a href="mailto:m@oja.la?cc=fr%40oja.la&amp;subject=Tengo%20una%20pregunta%20sobre%20este%20curso">m@oja.la</a>
				y revisaremos lo que sea que haga falta, preguntas sobre el curso, sobre Oja.la, incluso un simple hola sería perfecto ;).
			</p>
		</div>
	</div>
</div>


</div>
<?php 
	Yii::app()->clientScript->registerScript('switchStripe', 
		"$(function(){" .
			"$('#switchForeign').on('click', function(e){".
				"e.preventDefault();" .
				"document.cookie = 'currency=foreign;';" .
				"window.location.reload(); " .
			"});" . 
			"$('#switchLocal').on('click', function(e){".
				"e.preventDefault();" .
				"document.cookie = 'currency=; expires=Thu, 01 Jan 1970 00:00:01 GMT;';" .
				"window.location.reload(); " .
			"});" .
		"});"
	, CClientScript::POS_END);
?>

<!-- Start of GetKudos Script -->
<script>
(function(w,t,gk,d,s,fs){if(w[gk])return;d=w.document;w[gk]=function(){
(w[gk]._=w[gk]._||[]).push(arguments)};s=d.createElement(t);s.async=!0;
s.src='//static.getkudos.me/widget.js';fs=d.getElementsByTagName(t)[0];
fs.parentNode.insertBefore(s,fs)})(window,'script','getkudos');

getkudos('create', 'ojala');
</script>
<!-- End of GetKudos Script -->