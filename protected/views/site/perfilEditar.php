<?php
  $this->pageTitle = "Tu Perfil";
?>
<div class="page-header">
	<h1>Hola <?php echo $user->name; ?></h1>
	<p>Bienvenido a tu zona de usuario, aqui podrás encontrar toda la información sobre tu cuenta.</p>
</div>

<div class="row">
	<div class='container' style="padding-top:0;">
		<div class="col-md-2" itemscope="" itemtype="http://schema.org/Person">
			<nav>
				<ul class="nav nav-pills nav-stacked">
					<li class="active"><a href="">Mi información</a></li>
					<?php if($suscripcion==1){ ?>
					<li><a href="<?php echo Yii::app()->urlManager->createUrl('site/perfilSuscripcion'); ?>">Mi suscripción</a></li>
					<?php } ?>
					<?php if($compras==1){ ?>
					<li><a href="<?php echo Yii::app()->urlManager->createUrl('site/perfilCompras'); ?>">Mis compras</a></li>
					<?php } ?>
          			<li><a href="<?php echo Yii::app()->urlManager->createUrl('site/perfilFacturacion'); ?>">Mis datos de facturación</a></li>					
					<li><a href="<?php echo Yii::app()->urlManager->createUrl('site/CambiarClave'); ?>">Cambiar Contraseña</a></li>
				</ul>
			</nav>
		</div>

		<div class="col-md-9 col-md-offset-1">
			
			<div class="page-header" style="padding-top:0 !important;border:0px !important;">
				<h2 style="font-weight:lighter;">Actualiza tus datos</h2>
				<p>La información de este formulario solo será editada por ti, pero podrá ser vista por toda la comunidad. Cuentanos por que eres experto en tu campo.</p>
			</div>
			

			<div>
				<?php $form=$this->beginWidget('CActiveForm', array(
					'id'=>'register-form',
					'htmlOptions' => array('class'=>'form-horizontal','enctype'=>'multipart/form-data'),
					)); ?>


					<div class="form-group avatar-input">
						<label class="col-lg-3" for="user_image">Avatar:</label>
						<div class="col-lg-7">
							<?php $this->widget('ext.EAjaxUpload.EAjaxUpload',
								array(
									'id'=>'uploadFile',
									'config'=>array(
										'action'=>Yii::app()->createUrl('site/upload'),
							          'allowedExtensions'=>array("jpg", "jpeg", "png", "gif"),//array("jpg","jpeg","gif","exe","mov" and etc...
							          	'debug'=>false,
								        'sizeLimit'=>5*1024*1024,// maximum file size in bytes
								        'minSizeLimit'=>1*1024,// minimum file size in bytes
								        'onComplete'=>"js:function(id, fileName, responseJSON){						                        
								        	$('#picture img').attr('src','".Yii::app()->request->baseUrl."/images/users/'+fileName);
								        	$('#picture').css('display','block');
								        }",)
								)); ?>
							<?php if($user->image != ""){ ?>
								<img  style='width:200px;height:200px' alt="<?php echo $user->image; ?>" src="<?php echo Yii::app()->request->baseUrl; ?>/images/users/<?php echo $user->image; ?>" />
							<?php }else{ ?>

								<div id='picture' class='picture' style='display:none;float:left;margin-left:20px;'>
								  	<img style='width:200px;height:200px' />
								</div>
							<?php } ?>
						</div>
					</div>

					<div class="form-group">
						<?php echo $form->label($user, 'name', array('class'=>'col-lg-3 control-label')) ?>
						<div class="col-lg-7">
							<?php echo $form->textField($user,'name', array('size'=>'30', 'class'=>'form-control')); ?>
							<?php echo $form->error($user,'name', array('style'=>'color: red;')); ?>
						</div>
					</div>

					<div class="form-group">
						<?php echo $form->label($user, 'lastname', array('class'=>'col-lg-3 control-label')) ?>
						<div class="col-lg-7">
							<?php echo $form->textField($user,'lastname', array('size'=>'30', 'class'=>'form-control')); ?>
							<?php echo $form->error($user,'lastname', array('style'=>'color: red;')); ?>
						</div>
					</div>

					<div class="form-group">
						<label class="col-lg-3" for="user_email">Correo Electrónico:</label>
						<div class="col-lg-7">
							<?php echo $form->textField($user,'email1', array('size'=>'30', 'class'=>'form-control', 'disabled'=>'disabled')); ?>
							<?php echo $form->error($user,'email1', array('style'=>'color: red;')); ?>
						</div>
					</div>

					<div class="form-group">
						<?php echo $form->label($user, 'phone', array('class'=>'col-lg-3 control-label')) ?>
						<div class="col-lg-7">
							<?php echo $form->textField($user,'phone', array('size'=>'15', 'class'=>'form-control')); ?>
							<?php echo $form->error($user,'phone', array('style'=>'color: red;')); ?>
						</div>
					</div>

					<div class="form-group">
						<?php echo $form->label($user, 'nickname', array('class'=>'col-lg-3 control-label')) ?>
						<div class="col-lg-7">
							<?php echo $form->textField($user,'nickname', array('size'=>'30', 'class'=>'form-control')); ?>
							<?php echo $form->error($user,'nickname', array('style'=>'color: red;')); ?>
						</div>
					</div>

					<div class="form-group">
						<?php echo $form->label($user, 'about', array('class'=>'col-lg-3 control-label')) ?>
						<div class="col-lg-7">
							<?php echo $form->textArea($user, 'about', array('rows'=>'6', 'class'=>'form-control')); ?>
							<?php echo $form->error($user, 'about', array('style'=>'color: red;')); ?>
						</div>
					</div>

					<div class="form-group">
						<?php echo $form->label($user, 'occupation', array('class'=>'col-lg-3 control-label')) ?>
						<div class="col-lg-7">
							<?php echo $form->textField($user,'occupation', array('size'=>'30', 'class'=>'form-control')); ?>
							<?php echo $form->error($user,'occupation', array('style'=>'color: red;')); ?>
						</div>
					</div>
					<div class="form-group">
						<?php echo $form->label($user, 'link', array('class'=>'col-lg-3 control-label')) ?>
						<div class="col-lg-7">
							<?php echo $form->textField($user,'link', array('size'=>'30', 'class'=>'form-control')); ?>
							<?php echo $form->error($user,'link', array('style'=>'color: red;')); ?>
						</div>
					</div>
					<hr>
					<input class="btn btn-warning btn-lg col-lg-offset-3" type="submit" value="Actualizar mis datos">
					<?php $this->endWidget(); ?>

			</div>
		</div>
	</div>
</div>

