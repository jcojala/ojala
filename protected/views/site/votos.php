<?php
  $this->pageTitle = "Votos";
?>
<div class="home" role="main">
<div class="row">

<div class="row">
<section class="container idea unvoted" itemscope="itemscope" itemtype="http://schema.org/CreativeWork">
<meta content="UserPageVisits:787.0" itemprop="interactionCount">
<meta content="¿quieres, aprender, diagnosticar, idea, negocio?" itemprop="keywords">
<meta content="es_LA" itemprop="inLanguage">
<meta content="Educativo" itemprop="genre">
<ol class="breadcrumb" itemprop="breadcrumb">
<li><a href="https://oja.la/courses">Cursos</a></li>
<li><a href="https://oja.la/ideas" class="active">Próximos</a></li>
<li class="active">¿Quieres aprender a diagnos...</li>
<div class="btn-group pull-right">
</div>
</ol>
<div class="col-md-4 col-md-offset-2 img"><img alt="caratula" itemprop="image" src="https://oja.la/uploads/idea/cover/501115680eb1ae49f50003b6/blue2.jpg"></div>
<div class="col-md-4">
<h6>Sugerido por: <a href="/users/experto.en.viabilidad.de.negocios.y.estrategias" rel="author">Juan Jose Barbero Dossin</a></h6>
<h2 itemprop="name">¿Quieres aprender a diagnosticar una idea de negocio?</h2>
<p>
Votos por este curso:
515
</p>
<div class="votes"><a class="btn btn-lg btn-warning good" data-method="post" data-require-login="Debes ser un miembro de Oja.la para votar." href="/ideas/quieres-aprender-a-diagnosticar-una-idea-de-negocio/vote?value=1">
<div class="txt-voted">
<div class="glyphicon glyphicon-thumbs-up"></div>
Gracias por tu voto
</div>
<div class="txt">Vota por este curso</div>
</a>
</div>
<div class="share-btns">
<h5>Comparte este curso:</h5>
<div class="addthis_toolbox addthis_default_style addthis_24x24_style">
  <a class="addthis_button_facebook addthis_button_preferred_1 at300b" title="Facebook" href="#"><span class="at16nc at300bs at15nc at15t_facebook at16t_facebook"><span class="at_a11y">Share on facebook</span></span></a>
  <a class="addthis_button_twitter addthis_button_preferred_2 at300b" title="Tweet" href="#"><span class="at16nc at300bs at15nc at15t_twitter at16t_twitter"><span class="at_a11y">Share on twitter</span></span></a>
  <a class="addthis_button_email addthis_button_preferred_3 at300b" target="_blank" title="Email" href="#"><span class="at16nc at300bs at15nc at15t_email at16t_email"><span class="at_a11y">Share on email</span></span></a>
  <a class="addthis_button_favorites addthis_button_preferred_7 at300b" title="Favorites" href="#"><span class="at16nc at300bs at15nc at15t_favorites at16t_favorites"><span class="at_a11y">Share on favorites</span></span></a>
  <a class="addthis_button_pinterest_share addthis_button_preferred_9 at300b" target="_blank" title="Pinterest" href="#"><span class="at16nc at300bs at15nc at15t_pinterest_share at16t_pinterest_share"><span class="at_a11y">Share on pinterest_share</span></span></a>
  <a class="addthis_button_google addthis_button_preferred_10 at300b" href="https://www.addthis.com/bookmark.php?v=300&amp;winname=addthis&amp;pub=ra-4ee8218669730a74&amp;source=tbx-300&amp;lng=es-419&amp;s=google&amp;url=https%3A%2F%2Foja.la%2Fideas%2Fquieres-aprender-a-diagnosticar-una-idea-de-negocio&amp;title=Oja.la%20-%20%C2%BFQuieres%20aprender%20a%20diagnosticar%20una%20idea%20de%20negocio%3F&amp;ate=AT-ra-4ee8218669730a74/-/-/5261e49421535e6a/2/5254018fbb1bb98a&amp;frommenu=1&amp;uid=5254018fbb1bb98a&amp;ct=1&amp;pre=https%3A%2F%2Foja.la%2Fideas&amp;tt=0&amp;captcha_provider=nucaptcha" target="_blank" title="Google"><span class="at16nc at300bs at15nc at15t_google at16t_google"><span class="at_a11y">Share on google</span></span></a>
  <a class="addthis_button_compact at300m" href="#"><span class="at16nc at300bs at15nc at15t_compact at16t_compact"><span class="at_a11y">More Sharing Services</span></span></a>
  <a class="addthis_counter addthis_bubble_style" href="#" style="display: inline-block;"><a class="addthis_button_expanded" target="_blank" title="View more services" href="#">1</a><a class="atc_s addthis_button_compact"><span></span></a></a>
<div class="atclear"></div></div>
</div>
</div>
<div class="col-md-8 col-md-offset-2" id="disqus_thread"><iframe id="dsq2" data-disqus-uid="2" allowtransparency="true" frameborder="0" role="complementary" width="100%" src="https://disqus.com/embed/comments/?f=ojalaedu&amp;t_u=https%3A%2F%2Foja.la%2Fideas%2Fquieres-aprender-a-diagnosticar-una-idea-de-negocio&amp;t_d=Oja.la%20-%20%C2%BFQuieres%20aprender%20a%20diagnosticar%20una%20idea%20de%20negocio%3F&amp;t_t=Oja.la%20-%20%C2%BFQuieres%20aprender%20a%20diagnosticar%20una%20idea%20de%20negocio%3F&amp;s_o=popular&amp;disqus_version=1382133232#2" style="width: 100% !important; border: none !important; overflow: hidden !important; height: 339px !important;" scrolling="no" horizontalscrolling="no" verticalscrolling="no"></iframe></div>
<script type="text/javascript">
  /* * * CONFIGURATION VARIABLES: EDIT BEFORE PASTING INTO YOUR WEBPAGE * * */
  var disqus_shortname = 'ojalaedu'; // required: replace example with your forum shortname
  /* * * DON'T EDIT BELOW THIS LINE * * */
  (function() {
      var dsq = document.createElement('script'); dsq.type = 'text/javascript'; dsq.async = true;
      dsq.src = '//' + disqus_shortname + '.disqus.com/embed.js';
      (document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(dsq);
  })();
</script>
<noscript>Please enable JavaScript to view the &lt;a href="http://disqus.com/?ref_noscript"&gt;comments powered by Disqus.&lt;/a&gt;</noscript>

</section>
</div>

</div>
</div>