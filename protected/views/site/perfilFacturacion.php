<?php
  $this->pageTitle = "Tu perfil";
?>
<div class="page-header">
	<h1>Información de facturación</h1>
	<p>Aquí podrás editar la información que incluiremos en tus facturas.</p>
</div>

<div class="row">
	<div class='container' style="padding-top:0;">
		<div class="col-md-2" itemscope="" itemtype="http://schema.org/Person">
			<nav>
				<ul class="nav nav-pills nav-stacked">
                    <li><a href="<?php echo Yii::app()->urlManager->createUrl('site/perfilEditar'); ?>">Mi información</a></li>
                    <?php if(isset($suscripcion) && $suscripcion){ ?>
					<li><a href="<?php echo Yii::app()->urlManager->createUrl('site/perfilSuscripcion'); ?>">Mi suscripción</a></li>
					<?php } ?>
                    <?php if(isset($compras) && $compras){ ?>
					<li><a href="<?php echo Yii::app()->urlManager->createUrl('site/perfilCompras'); ?>">Mis compras</a></li>
					<?php } ?>
          			<li class="active"><a href="<?php echo Yii::app()->urlManager->createUrl('site/perfilFacturacion'); ?>">Mis datos de facturación</a></li> 					
					<li><a href="<?php echo Yii::app()->urlManager->createUrl('site/CambiarClave'); ?>">Cambiar Contraseña</a></li>
				</ul>
			</nav>
		</div>

		<div class="col-md-9 col-md-offset-1">
			
			<div class="page-header" style="padding-top:0 !important;border:0px !important;">
				<h2 style="font-weight:lighter;">Actualiza tus datos</h2>
				<p>Incluiremos esta información en los detalles de tu factura.</p>
				<?php if(isset($_GET['redirect'])){ ?>
					<div class="alert alert-danger"> Debes tener tus datos de facturación actualizados para descargar facturas. </div>
					<div class="alert alert-warning">Por favor actualiza tu información de facturación para descargar facturas. </div>				
				<?php } ?>
			</div>
			

			<div>
				<?php $form=$this->beginWidget('CActiveForm', array(
					'id'=>'register-form',
					'htmlOptions' => array('class'=>'form-horizontal')
					)); ?>

					<?php if(!empty($payinfo->id_pinfo)){ ?>
					<input type="hidden" name="id_pinfo" value="<?php echo $payinfo->id_pinfo; ?>" />
					<?php } ?>

					<div class="form-group">
						<label class="col-lg-3" for="user_last_name">Identificador (Opcional):</label>
						<div class="col-lg-7">
							<?php echo $form->textField($payinfo,'id', array('size'=>'30', 'class'=>'form-control')); ?>
							<?php echo $form->error($payinfo,'id', array('style'=>'color: red;')); ?>
						</div>
					</div>					

					<div class="form-group">
						<label class="col-lg-3" for="user_first_name">Nombre o Razón Fiscal:</label>
						<div class="col-lg-7">
							<?php echo $form->textField($payinfo,'name', array('size'=>'30', 'class'=>'form-control')); ?>
							<?php echo $form->error($payinfo,'name', array('style'=>'color: red;')); ?>
						</div>
					</div>


<!-- 
					<div class="form-group">
						<label class="col-lg-3" for="user_email">Correo Electrónico:</label>
						<div class="col-lg-7">
							<?php echo $form->textField($payinfo,'email', array('size'=>'30', 'class'=>'form-control')); ?>
							<?php echo $form->error($payinfo,'email', array('style'=>'color: red;')); ?>
						</div>
					</div>

					<div class="form-group">
						<label class="col-lg-3" for="user_nickname">Teléfono:</label>
						<div class="col-lg-7">
							<?php echo $form->textField($payinfo,'phone', array('size'=>'30', 'class'=>'form-control')); ?>
							<?php echo $form->error($payinfo,'phone', array('style'=>'color: red;')); ?>
						</div>
					</div>
 -->
					<div class="form-group">
						<label class="col-lg-3" for="user_bio">Datos Adicionales:</label>
						<div class="col-lg-7">
							<?php echo $form->textArea($payinfo, 'address', array('rows'=>'6', 'class'=>'form-control')); ?>
							<?php echo $form->error($payinfo, 'address', array('style'=>'color: red;')); ?>
						</div>
					</div>
					<hr>
					<input class="btn btn-warning btn-lg col-lg-offset-3" type="submit" value="Actualizar mis datos">
					<?php $this->endWidget(); ?>

			</div>
		</div>
	</div>
</div>

