<?php
	$this->pageTitle=$topic->na_topic;
?>
<div class="row back-class">
	<div class="class">

	<div class="video" id="ojala-video">
		<?php $this->widget('Wistia', array(
		        'videoId' => $topic->wistia_uid
		)); ?>

		<div id="thepopover" class="popover" style="position: absolute; margin-top: 14%; margin-left: 27%; width: 25%;">
			<h3 class="popover-title">Clase Terminada</h3>
			<div class="popover-content">
				<p style="text-align: center;">
					<a id="reiniciar" class="btn btn-default btn-sm" href="#" style="margin: 10px;"><span class="glyphicon glyphicon-repeat"></span> Ver de Nuevo</a>
					<?php if(isset($next)){ ?>
					<a class="btn btn-primary" href="<?php echo Yii::app()->urlManager->createUrl('site/clase', array('slugc'=>$next['slug'], 'slug'=>$slug, 'cat'=>$cat)); ?>">Siguiente <span class="glyphicon glyphicon-chevron-right"></span></a>
					<?php } ?>
				</p>
			</div>
		</div>

	</div>

	<div class="info">
		<p>Curso: <?php echo $course->name; ?></p>
		<h1><?php echo $topic->na_topic; ?></h1>
		
		<div class="addons">
			<?php if(isset($topic->package) && $topic->package != ""){ ?>
				<?php if(($descargas==1 OR Yii::app()->user->utype == 'Administrador' OR Yii::app()->user->utype == 'AAC') AND !isset(Yii::app()->session['admin'])) { ?>
					
					<a href="#" class="btn-default btn btn-block" target="_blank" data-toggle="modal" data-target="#downloads">
						<span class="glyphicon glyphicon-folder-open"></span>Descarga archivos
					</a>
			
					<div class="modal fade" id="downloads" tabindex="-1" role="dialog" aria-labelledby="downloads" aria-hidden="true">
						<div class="modal-dialog">
							<div class="modal-content">
								<div class="modal-header">
									<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
									<h4 class="modal-title" id="myModalLabel">Material descargable</h4>
								</div>
								<div class="modal-body" style="text-align:center;">
									<span class="glyphicon glyphicon-cloud-download" style="font-size:100px;color:#e5e5e5;"></span>
									</br>
									<a href="<?php echo Yii::app()->urlManager->createUrl('site/descargarArchivo', array('id_topic'=>$topic->id_old, 'package'=>$topic->package, 'uid'=>$course->wistia_uid)); ?>" class="btn btn-lg btn-primary">Descargar archivos de esta clase</a>
									<p>Al picar el botón descargarás un archivo tipo .ZIP, donde se encuentran todos los archivos usados y sugeridos para esta clase, si no tienes un programa para abrir este tipo de archivos, sigue <a href="http://www.winzip.com/es/downwz.html" target="_blank">este enlace y descarga completamente gratis</a> la versión que necesites.</p>
								</div>
							</div>
						</div>
					</div>

				<?php }else{ ?>

					<a href="#" class="btn-default btn btn-block" target="_blank" data-toggle="modal" data-target="#downloadsOff">
						<span class="glyphicon glyphicon-folder-open"></span>Descarga archivos
					</a>
			
				<?php } ?>
			<?php } ?>

			<div class="modal fade" id="downloadsOff" tabindex="-1" role="dialog" aria-labelledby="downloadsOff" aria-hidden="true">
				<div class="modal-dialog">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
							<h4 class="modal-title" id="downloadsOff">Material descargable</h4>
						</div>
						<div class="modal-body" style="text-align:center;">
							<div class="alert alert-danger">
								<h5>No tienes acceso al material, debes cambiar tu plan de suscripción</h5>
								<a href="<?php echo Yii::app()->urlManager->createUrl('site/perfilSuscripcion'); ?>" class="btn btn-danger">Cambiar mi plan</a>
							</div>
							<span class="glyphicon glyphicon-cloud-download" style="font-size:100px;color:#e5e5e5;"></span>
							</br>
							<a href="#" class="btn btn-lg btn-default" disabled="disabled">Descargar archivos de esta clase</a>
							<p>Al picar el botón descargarás un archivo tipo .ZIP, donde se encuentran todos los archivos usados y sugeridos para esta clase, si no tienes un programa para abrir este tipo de archivos, sigue <a href="http://www.winzip.com/es/downwz.html" target="_blank">este enlace y descarga completamente gratis</a> la versión que necesites.</p>
						</div>
					</div>
				</div>
			</div>

			<a href="<?php echo Yii::app()->urlManager->createUrl('site/foro', array('slugc'=>$topic->slug, 'slug'=>$slug, 'cat'=>$cat)); ?>" class="btn-default btn btn-block">
				<span class="glyphicon glyphicon-cloud"></span>Opina en el foro
			</a>
			<a href="#" class="btn-default btn btn-block btn-soporte">
				<span class="glyphicon glyphicon-envelope"></span>¿Necesitas ayuda?
			</a>
		</div>
		<div class="btn-group">
			<?php if(isset($previous)){ ?>
				<a href="<?php echo Yii::app()->urlManager->createUrl('site/clase', array('slugc'=>$previous['slug'], 'slug'=>$slug, 'cat'=>$cat)); ?>" class="back btn btn-primary btn-sm pjax"><span class="glyphicon glyphicon-chevron-left"></span> Anterior</a>
			<?php } ?>
		
			<a class="btn btn-primary btn-sm btn-programa" data-toggle="modal" href="#classes">
				<span class="glyphicon glyphicon-list-alt"></span> 
			</a>
		
			<?php if(isset($next)){ ?>
				<a href="<?php echo Yii::app()->urlManager->createUrl('site/clase', array('slugc'=>$next['slug'], 'slug'=>$slug, 'cat'=>$cat)); ?>" class="next btn btn-primary btn-sm pjax" id="next-topic">Siguiente <span class="glyphicon glyphicon-chevron-right"></span></a>
			<?php } ?>
		</div>

	</div>
</div>

<div class="modal fade" id="classes">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header" style="height:60px;">
				<button aria-hidden="true" class="close" data-dismiss="modal" type="button">×</button>
				<h4 class="modal-title" style="float:left;">Más clases de este curso</h4>
				<a href="<?php echo Yii::app()->urlManager->createUrl('site/curso', array('slug'=>$slug, 'cat'=>$cat)); ?>" class="btn btn-info btn-xs pull-right" style="margin:0 20px 0 0;">Regresar al home del curso</a>
			</div>
			<div class="modal-body">
				<div id="classes">
					<div class="lists">
						<ul>
							<ol>
							<?php $lesson=""; foreach ($list as $item) { ?>
							
								<?php if($lesson !== $item['na_lesson']){ $lesson = $item['na_lesson']; ?>
									<h5><?php echo $item['na_lesson']; ?></h5>
								<?php } ?>

								<?php if($item['id_topic'] == $topic->id_topic){ ?>

									<li class="active">
										<i>Estas viendo:</i>
										<?php echo $item['na_topic']; ?>
									</li>

								<?php }else{ ?>

									<li>
										<a href="<?php echo Yii::app()->urlManager->createUrl('site/clase', array('slugc'=>$item['slug'], 'slug'=>$slug, 'cat'=>$cat)); ?>">
										<div class="progress progress-success">
										<?php if(isset($item['progress'])){ ?>
										<div class="bar" style="width: <?php echo $item['progress']; ?>%"></div>
										<?php } ?>
										</div>
										<?php echo $item['na_topic']; ?>
										</a>
									</li>

								<?php } ?>

							<?php } ?>
							<ol>
						</ul>
					</div>
				</div>

			</div>
			<div class="modal-footer">
				<a href="<?php echo Yii::app()->urlManager->createUrl('site/curso', array('slug'=>$slug, 'cat'=>$cat)); ?>" class="btn btn-info pull-left">Regresar al home del curso</a>
				<button class="btn btn-default" data-dismiss="modal" type="button">Cerrar</button>
			</div>
		</div>
	</div>
</div>
<script charset='ISO-8859-1' src='//fast.wistia.net/static/iframe-api-v1.js'></script>

<script>
    $(function() 
    {
		wistiaEmbed = document.getElementById("wistia_<?php echo $topic->wistia_uid; ?>").wistiaApi;
		<?php if(isset($idcs)){ ?>
			wistiaEmbed.email('<?php echo $email; ?>');
			if('<?php echo substr_count(Yii::app()->request->urlReferrer, '/', strpos(Yii::app()->request->urlReferrer, "curso")); ?>' > '2') //Si viene de afuera
			{
				wistiaEmbed.play();
			}

			wistiaEmbed.bind("secondchange", function(second) {
				if ((second % 3) == 0) 
				{
					$.ajax({
						'url': '<?php echo Yii::app()->urlManager->createUrl('site/guardarTiempo'); ?>?id=<?php echo $topic->id_topic; ?>&idcs=<?php echo $idcs; ?>&second='+second,
						'cache': false,
					});
				}
			});

			wistiaEmbed.bind('end', function()
			{
			  	$.ajax({
					'url': '<?php echo Yii::app()->urlManager->createUrl('site/guardarTiempo'); ?>?id=<?php echo $topic->id_topic; ?>&idcs=<?php echo $idcs; ?>&second=<?php echo $topic->duration; ?>&final=true',
					'cache': false,
					success: function(data) 
					{
					  	//Enviar URL al siguiente Video
						if('<?php echo isset($next); ?>' == '1')
						{
							$('#thepopover').show();

							//window.location.href="<?php echo Yii::app()->urlManager->createUrl('site/clase', array('slugc'=>$next['slug'], 'slug'=>$slug, 'cat'=>$cat)); ?>";
						}
						else
						{
						  	$.ajax({
								'url':'<?php echo Yii::app()->urlManager->createUrl('site/finCurso'); ?>?id=<?php echo $topic->id_topic; ?>&idcs=<?php echo $idcs; ?>',
								'cache': false,
						        success: function (data) {
						        	if(data!='')
										window.location.href="<?php echo Yii::app()->urlManager->createUrl('site/cursoDesbloqueado', array('idcs'=>$idcs)); ?>&id="+data;
						        },
							});
						}
					},
				});
			});
		<?php } ?>

		$( "#reiniciar" ).click(function() 
		{
			$('#thepopover').hide();
			wistiaEmbed.time(0).play();
		});

        $(".btn-soporte").click(function(event) 
        {
          	$zopim.livechat.window.show();
        });
	});
</script>