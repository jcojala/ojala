<?php $this->pageTitle = "Privacy Policy"; ?>


<div class="row">
	<div class="container-fluid">
		<div class="page-header row">
  		<div class="col-md-12">
				<h1>Políticas de privacidad</h1>
			</div>
		</div>

		<div class="col-md-8 col-md-offset-2">

		<p>This web site is owned and operated by oja.la, Inc. ("we", "our", or "us" or the "Company"). We understand you value your privacy. We want to make your experience online satisfying and safe. This privacy policy (the "Policy") governs information you provide to us or we learn from your use of this web site (the "Site") and tells you how we may collect, use, and in some instances share this information. Our policies may differ from other service offerings and you should carefully review the terms of service and this privacy notice before using these services.</p>
		<hr>
		<h4>Collected Information</h4>
		<p>In general, you can visit the Site without telling us who you are or revealing any personally identifiable information about yourself. If you choose to register, and are 13 years of age or older, we will collect your name, date of birth, email address, and street address along with other`billing and shipping addresses ("Credit Card Information"). By providing your Personal Information to us, you explicitly agree to our collection and use of such information as described in this Policy.</p>
		<hr>
		<h4>Children's Privacy</h4>
		<p>We are committed to protecting children's privacy online and meeting the requirements of the federal Children's Online Privacy Protection Act. This Site is intended for users above the age of 13. We do not knowingly collect information from children.</p>
		<hr>
		<h4>Billing</h4>
		<p>If you use or provide services on the Site for which we implement a billing system for you, we will collect additional information from you so that we can process and collect billing information. For example, we may collect your mailing address to remit payments, and we may collect your Social Security number if necessary and to help us verify your identity.</p>
		<hr>
		<h4>Cookies</h4>
		<p>The Site uses software tags called "cookies" to identify customers when they visit our Site. Cookies are used to remember user preferences and maximize performance of our services. The information we collect with cookies is not sold, rented, or shared with any outside parties. Users who disable their Web browser's ability to accept cookies will be able to browse our Site but will not be able to successfully use our Service.</p>
		<p>We use both session ID cookies and persistent cookies. A session ID cookie expires when you close you browser. A persistent cookie remains on your hard drive for an extended period of time. You can remove persistent cookies by following directions provided in your Internet browser's "help" file. Persistent cookies enable us to track and target the interests of our users to enhance the experience on our Site.</p>
		<p>This privacy policy covers the use of cookies by our Site only and does not cover the use of cookies by any advertisers.</p>
		<p>Third Party Cookies: We may from time to time engage third parties to track and analyze non-personally identifiable usage and volume statistical information from visitors to our Site to help us administer our Site and improve its quality. Such third parties may use cookies to help track visitor behavior. Such cookies will not be used to associate individual Site visitors to any Personal Information. All data collected by such third parties on our behalf is used only to provide us with information on Site usage and is not shared with any other third parties.</p>
		<hr>
		<h4>Web Beacons</h4>
		<p>Web pages may contain an electronic file called a web beacon that allows a web site to count users who have visited that page or to access certain cookies. We may use web beacons in the following ways:</p>
		<ul>
			<li>We use web beacons within the Site in order to count users and to recognize users by accessing our cookies.</li>
			<li>Being able to access our cookies allows us to personalize your user experience.</li>
			<li>In general, any file served as part of a web page, including an ad banner, can act as a web beacon. We may also include web beacons from other companies within pages we serve so that our advertisers may receive auditing, and reporting.</li>
		</ul>
		<hr>
		<h4>Log Files</h4>
		<p>We maintain log files of the traffic that visits our Site. We do not link any information gathered in these log files to Personal Information. Log files are used to manage traffic loads and information technology requirements for providing reliable service. Information collected includes IP addresses and browser types.</p>
		<hr>
		<h4>Security</h4>
		<p>We take steps to protect the Personal Information submitted to us, both during transmission and once we receive it. No method of transmission over the Internet, or method of electronic storage, is 100% secure, however. Therefore, while we strive to use commercially reasonable means to protect your Personal Information, we cannot guarantee its absolute security. If we learn of a security systems breach we may attempt to notify you electronically so that you can take appropriate protective steps. By using this Site or providing personal information to us you agree that we can communicate with you electronically regarding security, privacy, and administrative issues relating to your use of this site. We may post a notice on our Site if a security breach occurs. We may also send an email to you at the email address you have provided to us in these circumstances. Depending on where you live, you may have a legal right to receive notice of a security breach in writing. To receive free written notice of a security breach (or to withdraw your consent from receiving electronic notice) you should notify us at privacy@oja.la.com.</p>
		<hr>
		<h4>Retention of Information</h4>
		<p>We will keep Personal Information of our users for as long as they are registered subscribers or users of our products and services, and as permitted by law.</p>
		<hr>
		<h4>Linking</h4>
		<p>This Site may contain links to other web sites. The Company cannot and does not control the privacy practices of any such sites. You should review the privacy policy on any web site where you may submit personal information before providing it to any web site.</p>
		<hr>
		<h4>Emails</h4>
		<p>If you choose to register for our products and services, we will send you certain promotional emails. Promotional emails advertise our products and services and/or the products and services of our Users and Affiliates. If you do not want to receive promotional emails from us, you may elect to opt-out of receiving promotional emails at any time after registering by e-mailing us at fr@oja.la.com, by writing to us at the address contained herein, or by hitting the "unsubscribe" button at the bottom of any of our e-mails.</p>
		<hr>
		<h4>Legal Disclaimer</h4>
		<p>We reserve the right to disclose your Personal Information as required by law and when we believe that disclosure doing so in the Company's interest to protect its property or other legal rights or the rights or property of others.</p>
		<hr>
		<h4>International Users</h4>
		<p>This Site is hosted and operated in the United States. If you are visiting from the European Union or other regions with laws governing data collection and use that may differ from U.S. law, please note that you are transferring your personal data to the United States, which does not have the same data protection laws as the EU and by providing your personal data you consent to:</p>
		<ul>
			<li>The use of your personal data for the uses identified above in accordance with this Privacy Policy; and The transfer of your personal data to the United States as indicated above.</li>
			
			<h5>Merger, Sale or Insolvency</h5>
			<li>If the Company should ever file for bankruptcy or have its assets sold to or merge with another entity, information the Company receives from you from this Site is a Company asset that may be transferred in connection with these types of corporate events.</li>
			
			<h5>Change in this Privacy Policy</h5>
			<li>We may occasionally update this Privacy Policy. When we do, we will also revise the "last updated" date on the Privacy Policy. For changes to this Privacy Policy that may be materially less restrictive on our use or disclosure of personal information you have provided to us, we will attempt to obtain your consent before implementing the change. We encourage you to periodically review this Privacy Policy to stay informed about how we are protecting the personal information we collect. Your continued use of this Site constitutes your agreement to this Privacy Policy and any updates.</li>
		</ul>
		<hr>
		<h4>Contact Us</h4>
		<p>If you have questions or concerns regarding this Policy, please contact us by emailing h@oja.la</p>
		
		<hr>
		
		<p style="margin-bottom:50px;"><i>Effective: April 16, 2010</i></p>
	</div>
</div>