
<script type="text/javascript">
    /* * * CONFIGURATION VARIABLES: EDIT BEFORE PASTING INTO YOUR WEBPAGE * * */
    var disqus_shortname = 'ojalaedu'; // required: replace example with your forum shortname

    /* * * DON'T EDIT BELOW THIS LINE * * */
    (function () {
        var s = document.createElement('script'); s.async = true;
        s.type = 'text/javascript';
        s.src = '//' + disqus_shortname + '.disqus.com/count.js';
        (document.getElementsByTagName('HEAD')[0] || document.getElementsByTagName('BODY')[0]).appendChild(s);
    }());
</script>


<div class="row link-container">
	<ul class="pager">
  	<li class="previous"><a href="#">&larr; Regresar a la clase</a></li>
  	<li class="next"><a href="#" data-toggle="modal" data-target="#links">Más enlaces</a></li>
	</ul>

	<div class="col-md-12 content">
		<h1>Oja.la cierra ronda De USD$500K encabezada por AltaVentures</h1>
		<p>Oja.la la plataforma de aprendizaje de informática más grande de América Latina, ha cerrado una ronda de USD 500,000 liderada por el fondo mexicano Alta Ventures.  En la ronda de inversión también forman parte  Angel Ventures México , Naranya Labs, NXTP Labs( follow on), además de los inversionistas ángeles Ian Noel, Patrick Wakeham y Ariel Poler.</p>
		<p>Fundada en 2012, Oja.la es parte de la 4ta edición de startups invertidas por NXTP Labs y cuyo proceso de alceleración fue realizado en asocio con Socialatom Ventures de Colombia.</p>
		<p>Previamente fueron inversores del startup  Wenceslao Casares y Meyer Malka</p>
		<p>Con una oferta de 1,500 clases Oja.la ha lanzado hasta el momento un total de 70 programas educativos de informática y ha sumado en promedio 16 programas cada mes para satisfacer la demanda de sus 120,000 estudiantes registrados, de los cuales el 57% son de México.</p>
		<p>“Agregando a estos líderes de capital de emprendedor en México, nos aseguramos grandes socios que nos apoyarán con nuestro crecimiento en todo el país” indicó Hernán Aracena, Director General de Oja.la.</p>
		<p>Y continúa Aracena: “La tecnología es  fundamental en nuestra generación como saber leer y escribir, es una forma de amplificar nuestras posibilidades. Nosotros creemos que con estas habilidades podemos dar mayor competitividad laboral a la clase media en México a través del conocimiento más importante del siglo XXI: La Informática”.</p>
		<p>Por su parte, Diego Serebrisky, socio director de Alta Ventures señalo: “En Alta Ventures estamos muy entusiasmados con la inversión en Oja.la, una empresa que está buscando revolucionar la educación online de contenidos técnicos en Latinoamérica. El acceso eficiente a educación es uno de los cambios transformadores que requiere la región.”</p>
		<p>“Creemos que es importante la señal que le das al mercado como inversor, cuando después de una inversión semilla, acompañas el crecimiento de una empresa de alto potencial de crecimiento como Oja.la, con un seguimiento de esa inversión” indicó Ariel Arrieta, fundador y director de NXTP Labs.</p>
		<p>Hechos:</p>
		<ul>
			<li>Oja.la es la plataforma de aprendizaje en línea más grande para los conocimientos de informática en América Latina.</li>
			<li>Cuenta con una base estudiantil de 120,000 alumnos.</li>
			<li>Recaudó un total de USD$500,000.</li>
			<li>Lidereados por Alta Ventures.</li>
			<li>Otros inversores incluyen:
				<ul>
					<li>Ángel Ventures México</li>
					<li>Ariel Poler</li>
					<li>Naranya Labs</li>
					<li>Ian Noel</li>
					<li>Patrick Wakeham</li>
					<li>NXTP</li>
				</ul>
			</li>
			<li>Inversionistas anteriores incluyen:
				<ul>
					<li>Socialatom Ventures</li>
					<li>Wenceslao Casares</li>
					<li>Meyer Malka</li>
					<li>Startup Chile</li>
				</ul>
			</li>
			<li>1,500 clases.</li>
			<li>70 programas educativos, lanzaron 16 nuevos programas del mes pasado y tienen previsto duplicar el lanzamiento de nuevos cursos mensualmente.</li>
			<li>Los estudiantes toman cursos en todos los países de América Latina.</li>
			<li>Han sumado inversores estratégicos en Sillicon Valley, México, España, Argentina, Colombia y Perú.</li>
		</ul>

		<div id="disqus_thread"></div>
    <script type="text/javascript">
        /* * * CONFIGURATION VARIABLES: EDIT BEFORE PASTING INTO YOUR WEBPAGE * * */
        var disqus_shortname = 'ojalaedu'; // required: replace example with your forum shortname

        /* * * DON'T EDIT BELOW THIS LINE * * */
        (function() {
            var dsq = document.createElement('script'); dsq.type = 'text/javascript'; dsq.async = true;
            dsq.src = '//' + disqus_shortname + '.disqus.com/embed.js';
            (document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(dsq);
        })();
    </script>
    <noscript>Por favor habilita JavaScript para que puedas usar  <a href="http://disqus.com/?ref_noscript">este foro.</a></noscript>    
	</div>

				<div class="modal fade" id="links" tabindex="-1" role="dialog" aria-labelledby="links" aria-hidden="true">
				<div class="modal-dialog">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
							<h4 class="modal-title" id="links">Enlaces recomendados</h4>
						</div>
						<div class="modal-body">
							<ul>
								<li>
									<h5><a href="<?php echo Yii::app()->urlManager->createUrl('site/enlaces'); ?>"><span class="glyphicon glyphicon-new-window"></span>Aprende como generar archivos .XML del lado del servidor</a></h5>
									<p>En este enlace verás como desarrollar fácilemnete y paso a paso como una reseta de cocina cada una de las lineas de código.</p>
								</li>
								<li>
									<h5><a href="<?php echo Yii::app()->urlManager->createUrl('site/enlaces'); ?>"><span class="glyphicon glyphicon-new-window"></span>Aprende como generar archivos .XML del lado del servidor</a></h5>
									<p>En este enlace verás como desarrollar fácilemnete y paso a paso como una reseta de cocina cada una de las lineas de código.</p>
								</li>
								<li>
									<h5><a href="<?php echo Yii::app()->urlManager->createUrl('site/enlaces'); ?>"><span class="glyphicon glyphicon-new-window"></span>Aprende como generar archivos .XML del lado del servidor</a></h5>
									<p>En este enlace verás como desarrollar fácilemnete y paso a paso como una reseta de cocina cada una de las lineas de código.</p>
								</li>
								<li>
									<h5><a href="<?php echo Yii::app()->urlManager->createUrl('site/enlaces'); ?>"><span class="glyphicon glyphicon-new-window"></span>Aprende como generar archivos .XML del lado del servidor</a></h5>
									<p>En este enlace verás como desarrollar fácilemnete y paso a paso como una reseta de cocina cada una de las lineas de código.</p>
								</li>
							</ul>
					</div>
				</div>
			</div>
</div>