<?php
  $this->pageTitle = "Tu Perfil";
?>
<div class='home' role='main'>
	<div class='row'>
		<div class='row'>
			<div class='container user'>
				
				<div class="col-md-3" itemscope="" itemtype="http://schema.org/Person">
					<div class="avatar">
						<div class="img">
							<?php if($user->image!=""){ ?>
								<img alt="<?php echo $user->name; ?>" src="<?php echo Yii::app()->request->baseUrl; ?>/images/users/<?php echo $user->image; ?>" />
							<?php }else{ ?>
								<img style="height: 103px;; width: 103px;" src="<?php echo Yii::app()->request->baseUrl; ?>/images/placeholder.gif" />
							<?php } ?>
						</div>
						<h3 itemprop='name'><?php echo $user->name; ?></h3>
						<!--<p itemprop="address" itemscope="" itemtype="http://schema.org/Place"><?php //echo $user->locale; ?></p>-->
						<!--<p><?php //echo $user->email1; ?></p>-->
						<nav>
							<ul class="nav nav-pills nav-stacked">
								<li class="active"><a href="#">Mis cursos</a></li>
								<li><a href="<?php echo Yii::app()->urlManager->createUrl('site/perfilEditar'); ?>">Mi información</a></li>
								<?php if($suscripcion==1){ ?>
								<li><a href="<?php echo Yii::app()->urlManager->createUrl('site/perfilSuscripcion'); ?>">Mi suscripción</a></li>
								<?php } ?>
								<?php if($compras==1){ ?>
								<li><a href="<?php echo Yii::app()->urlManager->createUrl('site/perfilCompras'); ?>">Mis compras</a></li>
								<?php } ?>
								<li><a href="<?php echo Yii::app()->urlManager->createUrl('site/CambiarClave'); ?>">Cambiar Contraseña</a></li>
							</ul>
						</nav>
					</div>
				</div>

				
				<div class='col-md-9'>
					<?php if(count($listDiplo)>0){ ?>
					<div id='courses'>
						<div class="page-header">
							<h2>Diplomados</h2>
						</div>
						<div class="row">
							<?php foreach($listDiplo as $item){ ?>
							<div class='col-md-4 mod-course'>
								<div class='thumbnail'>
									<div class='img'>
										<a href="<?php echo Yii::app()->urlManager->createUrl('site/diplomado', array('id'=>$item['id_group_susc'])); ?>" alt="..." class="img-rounded">
											<img alt="<?php echo $item['nb_group']; ?>" src="<?php echo Yii::app()->request->baseUrl; ?>/images/covers/<?php echo $item['image']; ?>" />
										</a>
									</div>
									<div class='caption'>
										<h4>
											<a href="<?php echo Yii::app()->urlManager->createUrl('site/diplomado', array('id'=>$item['id_group_susc'])); ?>">
												<?php echo $item['nb_group']; ?>
											</a>
										</h4>
										<hr>
										<a href="<?php echo Yii::app()->urlManager->createUrl('site/diplomado', array('id'=>$item['id_group_susc'])); ?>" class="btn btn-primary pull-right">Abrir Diplomado</a>
									</div>
								</div>
							</div>
							<?php } ?>
						</div>
					</div>
					<?php } ?>
					<div class='page-header'>
						<h2>Mis cursos</h2>
					</div>
					<div id='courses'>
						<div class='row'>
							
							<?php if(count($listaCursos)>0){ ?>

								<?php foreach($listaCursos as $item){ ?>
								<div class='col-md-4 mod-course'>
									<div class='thumbnail'>
										<div class='img'>
											<a href="<?php echo Yii::app()->urlManager->createUrl('site/curso', array('idcs'=>$item['id_csuscription'])); ?>" alt="..." class="img-rounded">
												<img alt="<?php echo $item['name']; ?>" src="<?php echo Yii::app()->request->baseUrl; ?>/images/covers/<?php echo $item['cover']; ?>" />
												<div class="study">
													<div class="progress">
														<div class="progress-bar progress-bar-success" style="width: <?php echo ''; ?>%">
											              <span> <?php echo ''; ?>% Completado</span>
											            </div>
													</div>
												</div>
											</a>
										</div>
										<div class='caption'>
											<h4><a href="<?php echo Yii::app()->urlManager->createUrl('site/curso', array('idcs'=>$item['id_csuscription'])); ?>"><?php echo $item['name']; ?></a></h4>
											<hr>
											<a href="<?php echo Yii::app()->urlManager->createUrl('site/curso', array('idcs'=>$item['id_csuscription'])); ?>" class="btn btn-warning pull-right">Abrir curso →</a>
										</div>
									</div>
								</div>
								<?php } // Fin del foreach ?>
							<?php } else { ?>
								<a class="btn btn-lg btn-primary pull-right" href="<?php echo Yii::app()->urlManager->createUrl('site/cursos')?>" style="margin-bottom: 5px"> Buscar cursos </a>
							<?php } ?>
						</div>
					</div>
				</div>

			</div>
		</div>
	</div>
</div>