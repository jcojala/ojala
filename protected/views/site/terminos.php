<?php $this->pageTitle = "Términos de Uso";?>

<div class="row">
	<div class="container-fluid">
		<div class="page-header row">
  		<div class="col-md-12">
				<h1>Términos y condiciones de Uso</h1>
			</div>
		</div>

		<div class="col-md-8 col-md-offset-2">
		<ol style="margin-bottom:50px;">
			<li>
				<h4>Introduction</h4>
				<p>The Oja.la web site (the "Site"), the services provided (the "Services") and the software available for download from Oja.la or the Site in connection with the Services (the "Software") are owned, operated and maintained, as applicable, by Oja.la, Inc. ("we", "our" , "us", or the "Company"). By (1) using or accessing the Site or the Services; (2) downloading, accessing, installing or using the Software; or (3) paying for someone else to use or access the Site or the Services or download, access, install or use the Software, you agree to the terms and conditions set forth below (the "Terms"). If you do not agree with these Terms, you should not use the Site. For the purposes of this agreement, "you" means a parent who pays for access to the Services as well as the student or Educator (as defined below) who accesses or uses the Services. If you are a parent, guardian, or other person who enables a child to access the Services, you agree to stand in the shoes of such child for the purposes of making us whole in case of damages or indemnification that could properly lie against a child, if not for his or her age. This Site is intended for use by you only if you are above the age of 13.</p>
			</li>
			<li>
				<h4>Privacy</h4>
				<p>Any personal information submitted in connection with your use of the Site is subject to our Privacy Policy, the terms of which are incorporated into the Terms. Please review our Privacy Policy to understand our practices.</p>
			</li>
			<li>
				<h4>Services</h4>
				<p>Through our Services, Site and Software, we enable users to connect with independent contractor educators (the "Educators") who provide live and recorded instruction, tutoring and learning services in our proprietary online classrooms (the "Sessions"). The Services include, without limitation, facilitating and hosting Sessions, and taking feedback from users.</p>
			</li>
			<li>
				<h4>Connectivity Costs and Equipment</h4>
				<p>You are solely responsible for all service, telephony and/or other fees and costs associated with your access to and use of the Services and for obtaining and maintaining all telephone, computer hardware and other equipment required for such access and use.</p>
			</li>
			<li>
				<h4>Educator Fees and Taxes</h4>
				<p>Joining the Site, browsing for Courses and publishing content is free. Educators will determine a fee for a course. We do charge a commission on every completed transaction using the Site that you agree to for using the Services. We may choose to temporarily change the fees for our Services, and such changes are effective when you consent to the changes.</p>
				<p>Unless otherwise stated, all fees are quoted in U.S. Dollars. You are responsible for paying all fees and applicable taxes associated with the Site in a timely manner with a valid payment method. If your payment method fails or your account is past due, we may collect fees owed using other collection mechanisms. (This includes charging other payment methods on file with us and retaining collection agencies and legal counsel)</p>
			</li>
			<li>
				<h4>General Disclaimer</h4>
				<p>The Site is only a marketplace of Educators, students, parents, schools and other interested organizations. We do not hire or employ Educators nor are we responsible or liable for any interactions involved between the Educators and their respective clients. We are not responsible for disputes, claims, losses, injuries, or damage of any kind that might out of or relate to conduct of Educators or users.</p>
				<p>We do not control Submitted Content (as defined below) posted on the Site and, as such, do not guarantee in any manner the reliability, validity, accuracy or truthfulness of such Submitted Content. You also understand that by using the Site you may be exposed to Submitted Content that is offensive, indecent, or objectionable. By using the Site, you hereby represent and warrant that you are located in the U.S. or are otherwise in a country where it is lawful to do so.</p>
			</li>
			<li>
				<h4>Conduct</h4>
				<p>You may only access the Site and use the Services for lawful purposes. You are solely responsible for the knowledge of and adherence to any and all laws, rules, and regulations pertaining to your use of the Services. You agree not to use the Site, the Services or the Company Content (as defined below) to recruit, solicit, or contact in any form Educators or potential users for employment or contracting for a business not affiliated with us without our advance written permission.</p>
			</li>
			<li>
				<h4>Specific Obligations of Educators</h4>
				<p>As a Educators registered with the Site, you agree that:</p>
				<ul>
					<li>You will read and abide by the pricing information (see Services section above) before using the Site.</li>
					<li>You are fully responsible for the content you provide, and accuracy of the information; In connection with the content you provide, you affirm, represent, and/or warrant that: you own or have the necessary licenses, rights, consents, and permissions to use and authorize us to use all patent, trademark, trade secret, copyright or other proprietary rights in and to any and all of your content to enable inclusion and use of the content in the manner contemplated by the Site and these Terms of Service.</li>
					<li>You have the required qualifications, credentials and expertise, including without limitation, education, training, knowledge and skill sets, to teach and offer the services you mention; * You will not post any inappropriate, offensive, racist, hateful, sexist, sex-related, false, defamatory or libelous content.</li>
					<li>You will not distribute viruses, post spam, chain mails or other such data.</li>
					<li>You will not use the Site for any business other than for providing tutoring, teaching and instructional services.</li>
					<li>You will not copy, modify or distribute Company Content (as defined below) from our Site, including trademarks and copyrights.</li>
					<li>You will not interfere with or otherwise prevent other Educators from providing their services.</li>
					<li>You will keep Your Data (as defined below) up-to-date, and respond to users seeking your services in a timely fashion, so as to ensure quality of service provided to students, parents and other users of the Site.</li>
					<li>If you are under the age of 18, but are qualified to be a Educator, you will obtain parent or legal guardian permission as and when needed, and before registering with the Site.</li>
					<li>If you are in a state or locale where any form of educating requires a license or other form of governmental permission, you shall not use this Site or act as a Educator until such license and permission is obtained.</li>
				</ul>
			</li>
			<li>
				<h4>Specific Obligations of Users (students, parents, schools and other organizations) using the Site</h4>
				<p>As a user in search of or engaging Educators, you agree that you will:</p>
				<ul>
					<li>Read and abide by the pricing information (see Services section above) before using the Site;</li>
					<li>If you are under the age of 18, get parent or legal guardian consent before using the Site and before contacting the Educators; only contact the Educators if you are looking for an Educator. You also agree that you will NOT:
						<ul>
							<li>Not distribute or post spam, chain mails or any such data.</li>
							<li>Not distribute or post any inappropriate, offensive, racist, hateful, sexist, sex-related, false, defamatory or libelous content to the site.</li>
							<li>Not manipulate or interfere with the site.</li>
							<li>Not disclose any information to a Educator that could be considered personally identifiable information including your full name, address, telephone number, email address, social security number, password or any other information that could be used to identify or locate you; and not solicit personal information from any Educator, and agree that if any Educator ever discloses such information to you, asks you for any personal information, or suggests any offline meeting or conversation, you agree to immediately report this to us by phone and in writing.</li>
						</ul>
					</li>
				</ul>
			</li>
			<li>
				<h4>Registration and Identity Protection</h4>
				<p>To use the Services, you will need to register on the Site and obtain an account, username and password. When you register, the information you provide to us during the registration process will help us in offering content, customer service, and network management. You are solely responsible for maintaining the confidentiality of your account(s), username(s) and password(s) (collectively, the "Account") and for all activities and liabilities associated with or occurring under your Account. You must notify us immediately of any unauthorized use of your Account and any other breach of security, and (b) ensure that you exit from your Account at the end of each session. We cannot and will not be responsible for any loss or damage arising from your failure to comply with this requirement or as a result of use of your Account, either with or without your knowledge. However, you could be held liable for losses incurred by us or another party due to someone else using your Account.</p>
				<p>You may not transfer your Account and you may not use anyone else's Account at any time without the permission of the account holder. In cases where you have authorized or registered another individual, including a minor, to use your Account, you are fully responsible for (i) the online conduct of such user; (ii) controlling the user's access to and use of the Services; and (iii) the consequences of any misuse For additional information on how we use your information, please see our Privacy Policy.</p>
			</li>
			<li>
				<h4>Accuracy of Account Information</h4>
				<p>In consideration of your use of the Services, you agree to (a) provide true, accurate, current and complete information about yourself as prompted by the Service's registration form (such information being "Your Data"), (b) maintain and promptly update Your Data to keep it true, accurate, current and complete; and (c) comply with these Terms. If you provide any information that is untrue, inaccurate, not current or incomplete, or if we believe that such information is untrue, inaccurate, not current or incomplete, we reserve the right to suspend or terminate your account(s) and refuse or restrict any and all current or future use of the Services.</p>
			</li>
			<li>
				<h4>User and Educator Submitted Content</h4>
				<p>Any materials, information, communications or ideas that you upload, communicate or otherwise transmit or post to us, the Site, or the Services by any means (the "Submitted Content") will be treated as non-confidential and subject to the license below, and may be disseminated or used by us for any purpose whatsoever, including, but not limited to, quality control, redistribution, professional development, as well as our developing, manufacturing, and marketing our current and/or future Services. Notwithstanding the foregoing, you have the right to remove your Submitted Content and/or mark it as "private" such that the Submitted Content will be inaccessible to other users.</p>
				<p>You agree that we may record all or any part of any Sessions (including voice chat communications) for quality control, redistribution, advertising and other purposes. We reserve the right to review the Sessions for any purpose.</p>
			</li>
			<li>
				<h4>Licensing Submitted Content</h4>
				<p>While you retain any and all rights in any Submitted Content you make available through the Service, we need certain rights to the Submitted Content and in that Submitted Content in order to make the Service available. By uploading or otherwise making available any Submitted Content, you automatically grant and/or warrant that the owner has granted to us a non-exclusive, royalty-free, perpetual, world-wide, irrevocable, transferable license with the right to grant sublicenses through multiple tiers of sublicenses to publicly display, publicly perform, distribute, store, transcode, syndicate, broadcast, reproduce, edit, modify, create derivative works, and otherwise use and reuse your Submitted Content (or any portion or derivative works thereof) in any manner, in any medium, for any purpose to the extent required to modify or deliver the Services. This license enables us to provide the Services and provide access to the Submitted Content and is not intended to otherwise limit your rights to the Content.</p>
				<p>You hereby waive any and all rights of privacy, publicity, or any other rights of a similar nature in connection with the exploitation of the Submitted Content, or any portion thereof, or of your name, personality, likeness, image or voice in connection with the Submitted Content, or any advertising or publicity relating thereto.</p>
			</li>
			<li>
				<h4>Removal of Submitted Content by Us</h4>
				<p>You acknowledge that we may screen Submitted Content, and that we shall have the right (but not the obligation), in our sole discretion, to remove any Submitted Content, including terminating Sessions. Without limiting the foregoing, we have the right to remove any Submitted Content that violates these Terms or is otherwise objectionable. You agree and acknowledge that we may preserve Submitted Content and may disclose Submitted Content if required to do so by law or in the good faith belief that any such preservation or disclosure is reasonably necessary to comply with legal process, enforce these Terms, respond to claims that any Submitted Content violates the rights of third parties or protect our rights, property or personal safety or that of our users and the public.</p>
			</li>
			<li>
				<h4>Copyright</h4>
				<p>You acknowledge that the Software, the technology underlying the Services, and all other software, designs, materials, information, communications, text, graphics, links, electronic art, animations, illustrations, artwork, audio clips, video clips, photos, images, and other data or copyrightable materials, including the selection and arrangements thereof, provided or made available to you in connection with the Site, the Software or the Services (collectively, the "Company Content") are the proprietary works of us and/or our affiliated and/or third party providers and suppliers (the "Third Parties") and are protected, without limitation, pursuant to U.S. and foreign copyright laws.</p>
			</li>
			<li>
				<h4>Prohibited Use of Company Content</h4>
				<p>Except as expressly authorized by us or in these Terms, you may not copy, reproduce, publish, perform, distribute, disseminate, broadcast, circulate, modify, create derivative works of, rent, lease, sell, assign, sublicense, otherwise transfer, display, transmit, compile or collect in a database, or in any manner commercially exploit the Site, Company Content or the Services, in whole or in part. You will not, in any manner, without our prior written approval, decompile, disassemble, reverse engineer, reverse assemble or otherwise attempt to discover any source code of, the Software or any other Company Content, the Site or the Services. You may not store any significant portion of any Company Content or the Services in any form, whether archival files, computer-readable files or any other medium. You may not "mirror" any Company Content or the Services on any server. Any unauthorized or prohibited use of the Software, other Company Content, the Site or the Services may subject the offender to civil liability and criminal prosecution under applicable federal and state laws.</p>
			</li>
			<li>
				<h4>Permitted Use of Company Content</h4>
				<p>You may download and print a reasonable number of copies of documentation provided or available in connection with the Company Content for noncommercial personal or educational use only and we grant you a limited, non-perpetual, revocable, nontransferable, non-assignable, non-exclusive, royalty-free license to access and utilize the Services, the Software and the other Company Content for noncommercial personal or educational purposes while these Terms are in full force and effect; provided that (i) any permitted copies of documentation provided or available in connection with the Company Content contain, in an unmodified form, (a) all language designations contained in the materials originally provided to you by us indicating the confidential nature thereof and (b) all copyright or other proprietary rights notices contained in the materials originally provided to you by us and an original source attribution to us and/or the applicable Third Parties; and (ii) you will not modify of any of the Company Content except as approved by us in advance in writing. You acknowledge that we and/or Third Parties, as applicable, hold all right, title and interest in and to all tangible and intangible aspects of the Company Content, the Site and the Services, including without limitation, all patents, copyrights and trade secrets pertaining thereto, and that, except for the limited rights set forth above, you do not acquire any intellectual property right or license in any of the foregoing by downloading or printing the Company Content or otherwise, including without limitation, by accessing or using the Site, the Company Content or the Services. These rights granted to you are revocable by us in accordance with these Terms.</p>
			</li>
			<li>
				<h4>Copyright Infringement</h4>
				<p>We do not promote, foster or condone the copying of copyrighted material or any other infringing activity. Any unauthorized use of the Site or its contents will terminate the limited license granted by us.</p>
				<p>If you believe that your work has been copied in a way that constitutes copyright infringement, or your intellectual property rights have been otherwise violated, please provide us our Copyright Agent (identified below) a notice with the following information:</p>
				<ol type="A">
					<li>An electronic or physical signature of the person authorized to act on behalf of the owner of the copyright or other intellectual property interest.</li>
					<li>A description of the copyrighted work or other intellectual property that you claim has been infringed.</li>
					<li>A description of where the material that you claim is infringing is located on the Site.</li>
					<li>Your address, telephone number, and email address.</li>
					<li>A statement by you that you have a good faith belief that the disputed use is not authorized by the copyright owner, its agent, or the law; and</li>
				</ol>
				<p>Our Copyright Agent for notice of claims of copyright or other intellectual property infringement can be reached by e-mail at copyright@Oja.la.com</p>
			</li>
			<li>
				<h4>Confidential Information</h4>
				<p>You agree to safeguard the Company Content and the Services (collectively, "Proprietary Information") and to prevent the unauthorized, negligent or inadvertent use or disclosure of such Proprietary Information. You will not, without our prior written approval, directly or indirectly, use or disclose the Proprietary Information to any person or business entity except for a limited number of your employees who are on a need-to-know basis and who agree in writing to be bound by the restrictions on use and disclosure set forth in these Terms or restrictions no less restrictive than these Terms. You agree to promptly notify us in writing of any use or disclosure of Proprietary Information in violation of these Terms. You acknowledge that the use or disclosure of the Proprietary Information in any manner inconsistent with these Terms will cause us irreparable damage and that we will have the right to (i) equitable and injunctive relief to prevent such prohibited use or disclosure, and (ii) recover the amount of all damages (including attorneys fees and expenses) in connection with such prohibited use or disclosure.</p>
			</li>
			<li>
				<h4>Links</h4>
				<p>The Site or the Services may provide links to non-Company web sites or resources (the "Third Party Sites"). This may include Educators sending links to Third Party Sites and/or causing Third Party Sites (such as study resources or online education pages) to pop-up for your review. Because we have no control over Third Party Sites, you acknowledge and agree that we are not responsible for the availability of Third Party Sites, and do not endorse and are not responsible or liable for any content, advertising, products, services or other materials on or available from Third Party Sites. You further acknowledge and agree that we shall not be responsible or liable, directly or indirectly, for any damage or loss caused or alleged to be caused by or in connection with use of or reliance on any such content, advertising, products, services or other materials available on or through any Third Party Sites or for any mistakes, defamation, libel, slander, omissions, falsehoods, obscenity, pornography, or profanity they may contain.</p>
			</li>
			<li>
				<h4>Trademarks</h4>
				<p>The trademarks, service marks, and logos (the "Trademarks") used and displayed on the Site or in any Company Content are registered and unregistered Trademarks of us and others and are protected, without limitation, pursuant to U.S. and foreign trademark laws. Nothing on the Site, the Services or otherwise should be construed as granting, by implication, estoppel, or otherwise, any license or right to use any Trademark displayed on the Site or in connection with the Services, Company Content or Software, without the written permission of the applicable Trademark owner. We aggressively enforce our intellectual property rights to the fullest extent of the law. You may not use the Trademarks, either ours or others, in any way without the prior written permission of the applicable Trademark owner. We prohibit use of our logo as a "hot" link to any other World Wide Web site unless approved by us in advance in writing.</p>
			</li>
			<li>
				<h4>Warranty Disclaimer</h4>
				<p>THE COMPANY CONTENT, THE SITE, THE SERVICES AND EACH PORTION THEREOF ARE PROVIDED "AS IS" WITHOUT WARRANTIES OF ANY KIND EITHER EXPRESS OR IMPLIED. TO THE FULLEST EXTENT POSSIBLE PURSUANT TO APPLICABLE LAW, WE DISCLAIM ALL WARRANTIES, EXPRESS OR IMPLIED, WITH RESPECT TO THE SITE, THE COMPANY CONTENT, THE SERVICES AND EACH PORTION THEREOF, INCLUDING, BUT NOT LIMITED TO, IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE, NON-INFRINGEMENT OR OTHER VIOLATION OF RIGHTS. WE DO NOT WARRANT OR MAKE ANY REPRESENTATIONS REGARDING THE USE, VALIDITY, ACCURACY, OR RELIABILITY OF, OR THE RESULTS OF THE USE OF, OR OTHERWISE RESPECTING, THE COMPANY CONTENT, THE SITE, THE SERVICES, EACH PORTION THEREOF OR ANY THIRD PARTY SITES.</p>
			</li>
			<li>
				<h4>Limitation of Liability</h4>
				<p>UNDER NO CIRCUMSTANCES, INCLUDING, BUT NOT LIMITED TO, NEGLIGENCE, SHALL WE OR THIRD PARTIES BE LIABLE FOR ANY DIRECT, INDIRECT, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES, INCLUDING, BUT NOT LIMITED TO, LOSS OF DATA OR PROFIT, ARISING OUT OR RELATING TO THE USE, OR THE INABILITY TO USE, THE COMPANY CONTENT, THE SITE, THE SERVICES OR ANY PORTION THEREOF, EVEN IF WE OR OUR AUTHORIZED REPRESENTATIVE HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES. IF YOUR USE OF THE SITE, THE COMPANY CONTENT, THE SERVICES OR ANY PORTION THEREOF RESULTS IN THE NEED FOR SERVICING, REPAIR OR CORRECTION OF EQUIPMENT OR DATE, YOU ASSUME ANY COSTS THEREOF. SOME STATES DO NOT ALLOW THE EXCLUSION OR LIMITATION OF INCIDENTAL OR CONSEQUENTIAL DAMAGES, SO THE ABOVE LIMITATION OR EXCLUSION MAY NOT APPLY TO YOU. NEITHER WE, NOR THIRD PARTIES WARRANT THE ACCURACY OR COMPLETENESS OF THE INFORMATION, TEXT, GRAPHICS, LINKS OR OTHER ITEMS CONTAINED IN THE COMPANY CONTENT, THE SITE, THE SERVICES OR ANY PORTION THEREOF OR IN ANY REPORTS OF VERIFICATION SERVICES. YOU AGREE NOT TO HOLD US (OR OUR AGENTS, EMPLOYEES OR EDUCATORS) LIABLE FOR ANY INSTRUCTION, ADVICE OR SERVICES DELIVERED WHICH ORIGINATED THROUGH THE SITE, THROUGH ANY VERIFICATION SERVICE OR IN CONNECTION WITH THE COMPANY CONTENT, THE SERVICES OR ANY PORTION THEREOF. WE ARE NOT RESPONSIBLE FOR DISPUTES, CLAIMS, LOSSES, INJURIES, OR DAMAGE OF ANY KIND THAT MIGHT ARISE OUT OF OR RELATE TO CONDUCT OF EDUCATORS OR USERS.</p>
			</li>
			<li>
				<h4>Indemnification</h4>
				<p>You agree to indemnify, defend and hold harmless us, and our affiliates, officers, directors, agents, partners, employees, licensors, representatives and third party providers (including our affiliates' respective officers, directors, agents, partners, employees, licensors, representatives, and third party providers), from and against all losses, expenses, damages, costs, claims and demands, including reasonable attorney's fees and related costs and expenses, due to or arising out of any Submitted Content you submit, post to, email, or otherwise transmit to us or through the Services, your use of the Services, the Company Content or any portion thereof, your connection to the Services, or your breach of these Terms. We reserve the right, at our own expense, to assume the exclusive defense and control of any matter otherwise subject to indemnification by you, and in such case, you agree to fully cooperate with such defense and in asserting any available defenses.</p>
			</li>
			<li>
				<h4>Modification of Services</h4>
				<p>We may add, change or eliminate features, pricing, nomenclature and other aspects of the Services and make other changes at any time and these Terms will continue to apply to the Services as modified. We reserve the right at any time and from time to time to modify or discontinue, temporarily or permanently, the Site or the Services (or any part thereof) with or without notice. You agree that we will not be liable to you or to any third party for any such modification, suspension, or discontinuance of the Site or the Services.</p>
			</li>
			<li>
				<h4>Changes to Terms of Service</h4>
				<p>We reserve the right, from time to time, with or without notice to you, to change these Terms in our sole and absolute discretion. The most current version of these Terms can be reviewed by clicking on the "Terms of Service" located at the bottom of the pages of the Site. The most current version of the Terms will supersede all previous versions. Your use of the Site or continued use of our service after changes are made means that you agree to be bound by such changes.</p>
			</li>
			<li>
				<h4>Termination of Services</h4>
				<p>We may terminate your use of the Site or Services immediately without notice for any breach by you of these Terms or any of our applicable policies, as posted on the Site from time to time. Furthermore, we may terminate your rights to use the Site or the Services for any reason or no reason. In the event of termination or expiration, the following sections of these Terms shall survive: all provisions regarding ownership of intellectual property, indemnification, disclaimer of warranties and limitations of liability, the provisions of this section which, by their nature apply after termination, and the general provisions below. You agree that upon the termination, we may delete all information related to you on the Services and may bar your access to the Site and use of the Services. Upon the termination you will immediately destroy any downloaded or printed Company Content.</p>
			</li>
			<li>
				<h4>Entire Agreement</h4>
				<p>These Terms and any policies applicable to you posted on the Site constitute the entire agreement between the parties with respect to the subject matter hereof, and supersede all previous written or oral agreements between the parties with respect to such subject matter. All rights not expressly granted in the Terms are expressly reserved. These Terms shall inure to our benefit and to the benefit of our agents, licensors, licensees, successors, and assigns.</p>
			</li>
			<li>
				<h4>Severability</h4>
				<p>If any provision of these Terms is found to be illegal or unenforceable, these Terms will be deemed curtailed to the extent necessary to make the Terms legal and enforceable and will remain, as modified, in full force and effect.</p>
			</li>
			<li>
				<h4>Governing Law and Forum Selection</h4>
				<p>These Terms and all matters or issues collateral thereto will be governed by, construed and enforced in accordance with the laws of the State of California applicable to contracts executed and performed entirely therein (without regard to any principles of conflict of laws), you agree that any dispute arising out of or relating to the Site or Services or use of the Site or Services shall be heard exclusively in the courts of the County of Santa Clara, California and you covenant and agree not to bring suit in any other forum.</p>
			</li>
			<li>
				<h4>Notice</h4>
				<p>Any notice or other communication to be given hereunder will be in writing and given by facsimile, postpaid registered or certified mail return receipt requested, or electronic mail.</p>
			</li>
			<li>
				<h4>No Agency</h4>
				<p>Nothing in these Terms shall be construed as making either party the partner, joint venture, agent, legal representative, employer, contractor or employee of the other. Neither the Company nor any other party to this Agreement shall have, or hold itself out to any third party as having, any authority to make any statements, representations or commitments of any kind, or to take any action that shall be binding on the other except as provided for herein or authorized in writing by the party to be bound.</p>
			</li>
		</ol>
		</div>

	</div>
</div>