<?php
$this->pageTitle = 'Conoce a tu asesor';
?>

<div class="row">
    <div class="col-sm-12 asesor">

        <h1>¡Bienvenido a Oja.la!</h1>

        <img alt="" src="<?php echo Yii::app()->request->baseUrl; ?>/images/avatar-asesor-ben.jpg">
        <p>Tú asesor será Benjamín,<br>
            él necesita tu teléfono para contactarte y contarte más sobre este curso</p>
        <?php
        $form = $this->beginWidget('CActiveForm', array(
            'id' => 'advisor-form',
            'enableClientValidation' => true,
            'clientOptions' => array('validateOnSubmit' => true),
            'htmlOptions' => array('class' => 'yiiform')
        ));
        ?>
        <div class="form-group">

            <?php
            echo $form->textField($modelUser, 'name', array(
                'class' => 'form-control input-lg',
                'placeholder' => 'Nombre',
                'name'=>'nombreInteresado'
            ));
            echo $form->error($modelUser, 'name');
            ?>

        </div>

        <div class="form-group">

            <?php
            echo $form->textField($modelUser, 'email1', array(
                'class' => 'form-control input-lg',
                'placeholder' => 'Correo Electrónico',
                'name'=>'emailInteresado'
            ));
            echo $form->error($modelUser, 'email1');
            ?>

        </div>

        <?php
        $this->widget('CountrySelection', array(
            'form' => $form,
            'model' => $modelAdvisor,
            'countryAttribute' => 'country',
            'regionAttribute' => 'region'
        ));
        ?>

        <div class="js-hide-city hidden">
            <label class="radio-inline input-lg">
                <input name="AdvisorForm[phoneType]" type="radio" id="inlineCheckbox2" value="Fijo"> Teléfono Fijo 
            </label>

            <label class="radio-inline input-lg">
                <input name="AdvisorForm[phoneType]" type="radio" id="inlineCheckbox1" value="Celular"> Celular
            </label>					
        </div>

<?php echo $form->error($modelAdvisor, 'phoneType') ?>

        <div class="form-group phone-group">
<?php
echo $form->textField($modelAdvisor, 'phoneNumber', array(
    'class' => 'form-control input-lg',
    'disabled' => true,
    'placeholder' => 'xxx xxxx',
));
echo $form->error($modelAdvisor, 'phoneNumber');
?>
        </div>



<?php echo CHtml::submitButton('Enviar tu información a Benjamín', array('class' => 'btn btn-primary btn-lg btn-block')); ?>

        <?php $this->endWidget() ?>
    </div>
</div>

<?php
Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl . '/js/advisor/intlTelInput.min.js'); //, CClientScript::POS_END);
Yii::app()->clientScript->registerCssFile(Yii::app()->request->baseUrl . '/css/intlTelInput.css');
?>
<script>
    var phoneNumber = '',
            phoneInput = $('#AdvisorForm_phoneNumber');

    phoneInput.intlTelInput({
        autoFormat: true,
        defaultCountry: 'ar',
        onlyCountries: ['ar', 'bo', 'br', 'cl', 'co', 'cr', 'ec', 'sv', 'es', 'us', 'hn', 'mx', 'ni', 'pa', 'py', 'pe', 'pr', 'do', 'uy', 've'],
        utilsScript: "<?php echo Yii::app()->request->baseUrl . '/js/advisor/utils.js' ?>"
    });

    $('#inlineCheckbox2').on('change', function () {
        if ($(this).is(':checked')) {
            var areaCode = '(' + $('#region-list option:selected').data('code') + ') ';
            phoneInput.prop('disabled', false).delay(1100).intlTelInput('setNumber', phoneNumber + areaCode);
            $('#AdvisorForm_phoneNumber').focus();
        }
    });

    $('#inlineCheckbox1').on('change', function () {
        if ($(this).is(':checked')) {
            phoneNumber = '+' + $('#country-list option:selected').data('code');
            phoneInput.prop('disabled', false).delay(1100).intlTelInput('setNumber', phoneNumber);
            $('#AdvisorForm_phoneNumber').focus();
        }
    });

    $('#country-list').on('change', function () {
        phoneNumber = '+' + $('#country-list option:selected').data('code');
        $("#AdvisorForm_phoneNumber").intlTelInput('setNumber', phoneNumber);
        phoneInput.prop('disabled', true);
        $('#inlineCheckbox1, #inlineCheckbox2').prop('checked', false);
        $('.js-hide-city').addClass('hidden');
    });

    $('form').on('change', '#region-list', function () {
        console.log($('#region-list').val())
        if ($('#region-list').val() == '') {
            $('.js-hide-city').addClass('hidden');
        } else {
            $('.js-hide-city').removeClass('hidden');
        }
    });

    $('form div').off();

    $('form').on('submit', function (e) {
        e.preventDefault();

        if ($.trim(phoneInput.val())) {
            if (phoneInput.intlTelInput("isValidNumber")) {
                $('#AdvisorForm_phoneNumber_em_').removeClass("errorMessage");
                $('#AdvisorForm_phoneNumber_em_').hide();
                document.getElementById('advisor-form').submit();
            } else {
                $('#AdvisorForm_phoneNumber_em_').text("Por favor escribe correctamente tu número");
                $('#AdvisorForm_phoneNumber_em_').addClass('errorMessage');
                $('#AdvisorForm_phoneNumber_em_').show();
            }
        }
    });

    // on keydown: reset
    phoneInput.keydown(function () {
        $('#AdvisorForm_phoneNumber_em_').removeClass("errorMessage");
        $('#AdvisorForm_phoneNumber_em_').hide();
    });

</script>