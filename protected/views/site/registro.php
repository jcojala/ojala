<?php
	$this->pageTitle = "Crea una Cuenta";
?>
		<div class='row devise-view'>
			<div class='col-md-4 col-md-offset-4 mod-devise'>

				<div class='page-header' style="border:0px !important;">
					<h3>¿Quiéres una nueva cuenta?</h3>
				</div>

				<?php $form=$this->beginWidget('CActiveForm', array(
					'id'=>'register-form',
					'htmlOptions' => array('enctype'=>'multipart/form-data'),
				)); ?>

					<div class='form-group'>
						<label for="user_Tu correo electrónico:">Tu correo electrónico:</label>
						<?php echo $form->textField($model,'email1', array('size'=>'30', 'class'=>'form-control input-lg')); ?>
						<?php echo $form->error($model,'email1'); ?>
					</div>
					<div class='form-group'>
						<label for="user_name">Tu nombre:</label>
						<?php echo $form->textField($model,'name', array('size'=>'30', 'class'=>'form-control input-lg')); ?>
						<?php echo $form->error($model,'name'); ?>
					</div>
					<div class='form-group'>
						<?php 
							echo $form->label($model,'pass', array('class'=>'control-label'));
							echo $form->passwordField($model,'pass', array('size'=>'30', 'class'=>'form-control input-lg'));
							echo $form->error($model,'pass');
						?>
					</div>
					<div class='form-group'>
						<?php
							echo $form->label($model,'repeatpass', array('class'=>'control-label'));
							echo $form->passwordField($model,'repeatpass', array('size'=>'30', 'class'=>'form-control input-lg'));
							echo $form->error($model,'repeatpass');
						?>
					</div>
					<br>
					<input class="btn btn-warning btn-lg btn-block" name="commit" type="submit" value="Crear mi cuenta ahora!" />

				<?php $this->endWidget(); ?>

			</div>

</div>