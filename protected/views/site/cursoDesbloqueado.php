<div class="home" role="main">
	<div class="row">
		<div class="jumbotron">
		  <h1>¡Curso Desbloqueado!</h1>
		  <p>Felicidades, terminaste el curso <b><?php echo $curso1; ?></b>, ahora desbloqueaste el siguiente nivel <b><?php echo $curso2; ?></b></p>
		  <p><a class="btn btn-primary btn-lg" role="button" href="<?php echo Yii::app()->urlManager->createUrl('site/diplomado', array('slug'=>$slug)); ?>#<?php echo $cid ?>">Empezar siguiente nivel</a></p>
		</div>
	</div>
</div>