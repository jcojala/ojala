<?php
$this->pageTitle = $course->name;
$payments = new StripeWrapper;
?>

<div class="portada">

    <!-- Primer fold -->
    <div class="fold">
        <div class="content">

            <h1 itemprop="name" style="margin-top: 30px"><?php echo $course->name; ?></h1>
            <p>100% en español - Certificación al completar el curso</p>
            <div class="buttons">
                <div class="btn-group">

                    <?php
                    echo CHtml::link('Más información', Yii::app()->urlManager->createUrl('site/estoyInteresado', array('id' => $course->id_course)), array(
                        'class' => 'btn btn-primary',
                        'style' => ''
                            )
                    );

                    echo CHtml::link('Más cursos de '.$cathegory->na_cathegory, Yii::app()->urlManager->createUrl('site/masCursos', array('id' => $course->id_course)), array(
                        'class' => 'btn btn-success',
                        'style' => ''
                            )
                    );
                    
                    ?>
                    
                </div>

                <div class="modal fade" id="gracias" tabindex="-1" role="dialog" aria-labelledby="gracias" aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content" style="padding-bottom: 10%">
                            <div class="modal-header"><span class="big" style="font-size: 20px">¡Hola<?php if (!Yii::app()->user->isGuest) echo ", " . Yii::app()->user->name ?>!</span> <a data-dismiss="modal" class="glyphicon glyphicon-remove pull-right" id="cerrarModal" style="color:#CCC!important;"></a></div>
                            <div class="modal-body">
                                <?php if (!Yii::app()->user->isGuest) { ?>
                                    <p>El curso aún no se encuentra disponible, pero lo estará en un lapso corto de tiempo. Te enviaremos un correo cuando el curso se encuentre disponible.</p>

                                <?php } else { ?>

                                    <p style="color:#888!important;padding-bottom: 5%">Gracias por interesarte en Oja.la, el curso no se encuentra disponible por los momentos, pero lo tendremos en el menor tiempo posible.
                                        Por favor, déjanos tu número de teléfono para avisarte.</p>

                                    <input type="hidden" id="cursoId" value="<?php echo $course->id_course; ?>" />
                                    <div class="col-md-12" style="">

                                        <?php
                                        $form = $this->beginWidget('CActiveForm', array(
                                            'id' => 'advisor-form',
                                            'enableClientValidation' => true,
                                            'clientOptions' => array('validateOnSubmit' => true),
                                            'htmlOptions' => array('class' => 'yiiform')
                                        ));
                                        ?>

                                        <div class="col-md-8 left">
                                            <div class="form-group phone-group">
                                                <?php
                                                echo $form->textField($model, 'phone', array(
                                                    'class' => 'form-control',
                                                    'placeholder' => 'xxx xxxx',
                                                    'id' => 'emailInteresado'
                                                ));
                                                echo $form->error($model, 'phoneNumber');
                                                ?>
                                            </div>
                                        </div>

                                        <div class="col-md-4 right">

                                            <?php
                                            echo CHtml::ajaxSubmitButton('Enviar', array('site/insertarInteresado'), array(
                                                'type' => 'post',
                                                'data' => array(
                                                    'course' => $course->id_course,
                                                    'email' => 'js:$("#emailInteresado").val()'
                                                ),
                                                'success' => 'function(data) { $("#cerrarModal").click(); $("#emailInteresado").val("");}'
                                                    ), array('class' => 'btn btn-info', 'style' => 'padding:6px 12px !important;font-size:14px!important;'));
                                            ?>

                                        </div>
                                        <?php $this->endWidget() ?>
                                    </div>

                                <?php } ?>

                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
    <!-- Final del primer fold -->

    <!-- Columna izquierda -->
    <div class="col-md-3 left">

        <div class="mod descripcion">
            <h4>Sobre este curso</h4>
            <?php
            $cover = $course->cover;
            if ($cover == "" || $cover == null) {

                $cover = "default.jpg";
            }
            ?>

            <img alt="<?php echo $course->name; ?>" src="<?php echo Yii::app()->request->baseUrl; ?>/images/covers/<?php echo $cover; ?>" />

            <?php
            if (!Yii::app()->user->isGuest) {
                if (Yii::app()->user->utype === 'Administrador') {
                    ?>
                    <a class="btn btn-success center" href="<?php echo Yii::app()->urlManager->createUrl('admin/editarPropuesta', array('id' => $course->id_course)); ?>">Editar Propuesta</a>
                    <?php
                }
            }
            ?>

            <dl>
                <dt><span class="glyphicon glyphicon-signal"></span>Nivel:</dt>
                <dd><?php
                    if ($level != null) {
                        echo $level->na_level;
                    }
                    ?></dd>
            </dl>

            <dl>
                <dt><span class="glyphicon glyphicon-tag"></span>Categoría:</dt>
                <dd><a href="<?php echo Yii::app()->urlManager->createUrl('site/cursos', array('cat' => $cathegory->slug)); ?>"><?php echo $cathegory->na_cathegory; ?></a></dd>
            </dl>
            <dl>
                <dt><span class="glyphicon glyphicon glyphicon-info-sign"></span>Clases:</dt>
                <dd><?php echo count($list); ?> Capitulos</dd>
            </dl>
            <p style="text-align: justify"><?php echo $course->description; ?></p>
        </div>

        <?php if ($course->requirements != "") { ?>
            <div class="mod">
                <h4>¿Cuáles son los requisitos?</h4>
                <p><?php echo $course->requirements; ?></p>
            </div>
        <?php } ?>
        <?php if ($course->learn != "") { ?>
            <div class="mod">
                <h4>¿Qué voy a aprender?</h4>
                <p style="text-align: justify"><?php echo $course->learn; ?></p>
            </div>
        <?php } ?>
        <?php if ($course->directed != "") { ?>
            <div class="mod">
                <h4>¿A quién va dirigido?</h4>
                <p><?php echo $course->directed; ?></p>
            </div>
        <?php } ?>
    </div>
    <!-- Final columna izquierda -->

    <!-- Columna central -->
    <div class="col-md-6 center">
        <div class="mod programa">
            <h4>Programa del curso</h4>
            <div class="lessons">
                <?php foreach ($list as $lesson) { ?>

                    <div class="title">
                        <h5><?php echo $lesson->na_lesson; ?></h5>
                    </div>

                    <?php foreach ($lesson->topics as $topic) { ?>

                        <?php if ($topic->public == 1) { ?> <a class="publico"><?php } ?>

                            <div class="topic">
                                <p><?php echo $topic->na_topic; ?></p>
                                <ul>
                                    <!--<li><span class="glyphicon glyphicon-time"></span>Duración: <?php // echo Dates::secondsToTime($topic->duration);          ?></li>-->
                                    <?php if ($topic->package != "") { ?>
                                        <li><span class="glyphicon glyphicon-folder-open"></span>Con material de descarga</li>
                                    <?php } ?>
                                </ul>
                            </div>

                            <?php if ($topic->public == 1) { ?> </a><?php } ?>


                    <?php } ?>

                <?php } ?>
            </div>
        </div>
    </div>
    <!-- Final columna central -->

    <!-- Final columna derecha -->
    <div class="col-md-3 right">

        <div class="mod recibes">

            <h4>¿Qué recibirás?</h4>
            <div class="each">
                <img alt="Faq-videos" src="<?php echo Yii::app()->request->baseUrl; ?>/images/faq-videos-b547ed0a34cfedacc1b4e3c229eab249.png">
                <strong>Videocursos paso a paso</strong>
                <p>Contenido 100% Online, videos de alta calidad, que puedes ver cuantas veces quieras a la hora que prefieras.</p>
            </div>
            <div class="each">
                <img alt="Faq-expire" src="<?php echo Yii::app()->request->baseUrl; ?>/images/faq-expire-dafebe946557b41ffedc11867b329f34.png">
                <strong>Acceso ilimitado</strong>
                <p>Tus cursos son tuyos, nunca expirarán, disponibles las 24hrs del día sin restricciones de ningún tipo.</p>
            </div>
            <div class="each">
                <img alt="Faq-dispositivos" src="<?php echo Yii::app()->request->baseUrl; ?>/images/faq-dispositivos-192843e5e8532ec4e879c88865338260.png">
                <strong>Disponibilidad total</strong>
                <p>Puedes tomar tus cursos desde cualquier dispositivo, teléfono, tablet, o computador, incluso desde varios a la vez.</p>
            </div>
            <div class="each">
                <img alt="Faq-certificate" src="<?php echo Yii::app()->request->baseUrl; ?>/images/faq-certificate-0445ea4296df636f0915d64816fe66f0.png">
                <strong>Certificación al terminar</strong>
                <p>Cuando completes el 100% de tu curso, el sistema te dará acceso a tu certificación para que la descargues e imprimas.</p>
            </div>
        </div>

        <?php // $this->widget('ShareThis');       ?>

        <div class="mod questions">
            <h4>¿Alguna pregunta?</h4>
            <p>
                Escríbenos a
                <a href="mailto:m@oja.la?cc=fr%40oja.la&amp;subject=Tengo%20una%20pregunta%20sobre%20este%20curso">m@oja.la</a>
                y revisaremos lo que sea q haga falta, preguntas sobre el curso, sobre Oja.la, sobre lo que sea que haga falta, incluso un simple hola sería perfecto ;).
            </p>
        </div>
    </div>
</div>


</div>
<?php
Yii::app()->clientScript->registerScript('switchStripe', "$(function(){" .
        "$('#switchForeign').on('click', function(e){" .
        "e.preventDefault();" .
        "document.cookie = 'currency=foreign;';" .
        "window.location.reload(); " .
        "});" .
        "$('#switchLocal').on('click', function(e){" .
        "e.preventDefault();" .
        "document.cookie = 'currency=; expires=Thu, 01 Jan 1970 00:00:01 GMT;';" .
        "window.location.reload(); " .
        "});" .
        "});"
        , CClientScript::POS_END);
?>

<!-- Start of GetKudos Script -->
<script>
    (function (w, t, gk, d, s, fs) {
        if (w[gk])
            return;
        d = w.document;
        w[gk] = function () {
            (w[gk]._ = w[gk]._ || []).push(arguments)
        };
        s = d.createElement(t);
        s.async = !0;
        s.src = '//static.getkudos.me/widget.js';
        fs = d.getElementsByTagName(t)[0];
        fs.parentNode.insertBefore(s, fs)
    })(window, 'script', 'getkudos');

    getkudos('create', 'ojala');
</script>
<!-- End of GetKudos Script -->