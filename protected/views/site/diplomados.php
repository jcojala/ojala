<?php 
	$this->pageTitle = "Diplomados"; 
?>

<div class="page-header row">
  <div class="col-md-8">
		<h1>Biblioteca de diplomados</h1>
		<p>Este listado muestra los diplomados disponibles para tomar.</p>
  </div>
</div>

<?php 
$urlIOS = Yii::app()->request->baseUrl.'/diplomado/diplomado-de-desarrollo-de-apps-para-iphone-y-ipad-en-ios7?landing=diplomadoios'; 
//$urlIOS = Yii::app()->request->baseUrl.'/diplomado/diplomado-profesional-online-de-diseno-y-desarrollo-de-aplicaciones-para-iphone?landing=diplomadoios'; //URL de prueba ya que los diplomados no existen en esta bd 
?>

<div class="row">
	<div class="col-md-12">
		<div class="row" style="margin:0 0 0 -5px;">
	<div class="col-md-6 diplomado">
	<article class='mod-course'>
		<div class='thumbnail'>

			<div class='img'>
				<a href="<?php echo $urlIOS; ?>" class="img-rounded">
					<img class="img-thumbnail" alt="name" src="https://oja.la/images/covers/Ewq8xpJ92.png">
				</a>
			</div>

			<div class='caption'>
				<h4>
					<a href="<?php echo $urlIOS ?>">
						Diplomado de desarrollo de apps para iPhone y iPad en iOS7
					</a>
				</h4>
				<hr>
				<a href="<?php echo $urlIOS ?>" class="btn btn-warning pull-right">
					Más información →
				</a>
			</div>
		</div>
	</article>
	</div>
	<?php $urlAndroid = Yii::app()->request->baseUrl.'/diplomado/diplomado-de-desarrollo-de-apps-para-android?landing=diplomadoAndroid'; ?>
	<div class="col-md-6 diplomado">
	<article class='mod-course'>
		<div class='thumbnail'>

			<div class='img'>
				<a href="<?php echo $urlAndroid ?>">
					<img class="img-thumbnail" alt="name" src="https://oja.la/images/covers/android-diplo.jpg">
				</a>
			</div>

			<div class='caption'>
				<h4>
					<a href="<?php echo $urlAndroid ?>">
						Diplomado de desarrollo de apps para Android
					</a>
				</h4>
				<hr>
				<a href="<?php echo $urlAndroid ?>" class="btn btn-warning pull-right">Más información →</a>
			</div>
		</div>
	</article>
	</div>
</div>
	</div>
</div>


<?php $urlWeb = Yii::app()->request->baseUrl.'/diplomado/diplomado-de-desarrollo-de-paginas-web?landing=diplomadoweb'; ?>
<div class="row" style="margin:0 0 0 -5px;">
	<div class="col-md-6 diplomado">
	<article class='mod-course'>
		<div class='thumbnail'>

			<div class='img'>
				<a href="<?php echo $urlWeb ?>" class="img-rounded">
					<img class="img-thumbnail" alt="name" src="https://oja.la/images/covers/web76.jpg">
				</a>
			</div>

			<div class='caption'>
				<h4>
					<a href="<?php echo $urlWeb ?>">
						Diplomado de desarrollo de páginas web
					</a>
				</h4>
				<hr>
				<a href="<?php echo $urlWeb ?>" class="btn btn-warning pull-right">Más información →</a>
			</div>
		</div>
	</article>
	</div>
	<?php $smWeb = Yii::app()->request->baseUrl.'/diplomado/diplomado-on-line-de-gestion-de-redes-sociales?landing=diplomadosm'; ?>
	<div class="col-md-6 diplomado">
	<article class='mod-course'>
		<div class='thumbnail'>

			<div class='img'>
				<a href="<?php echo $smWeb ?>" class="img-rounded">
					<img class="img-thumbnail" alt="name" src="https://oja.la/images/covers/nuevo-socialm.jpg">
				</a>
			</div>

			<div class='caption'>
				<h4>
					<a href="<?php echo $smWeb ?>">
						Diplomado on-line de Community Management y gestión de redes sociales
					</a>
				</h4>
				<hr>
				<a href="<?php echo $smWeb ?>" class="btn btn-warning pull-right">Más información →</a>
			</div>
		</div>
	</article>
	</div>
</div>

