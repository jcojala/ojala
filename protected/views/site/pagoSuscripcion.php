<?php
	$this->pageTitle = "Si quieres aprender, te lo enseñamos";
	$paygateway = new StripeWrapper;
	$paygateway->registerScripts('#subscriptions-form');

	$anualMensual = round(($anual/12),2);
	$isPaypalTabActive = $gw==='paypal';
?>

<?php if($plan=="P-310"){ ?>
<style type="text/css">
  .mod-christmas{
    border-radius: 5px;
    margin: 0 0 30px 0;
    padding-top: 30px;
    overflow: hidden;
  }
  .mod-christmas.basico{background:url("images/back-nav-gray.jpg");}
  .mod-christmas.premium{background:url("images/back-nav.jpg");}
  .mod-christmas h1{
    margin: 0 30% 15px 30%;
    color: #ffffff;
    text-shadow: 1px 2px 3px #000;
  }
  .mod-christmas h1 a{
    background: url("images/icon-info-nav.png") 0 0 no-repeat;
    height: 27px;
    width: 27px;
    margin: 10px 0 0 0;
    display: block;
    float: right;
    text-indent: -900000px;
    overflow: hidden;
  }
  .receipt *{margin: 0;padding: 0;}
  .receipt .row{border-bottom: 1px solid #f1f1f1;}
  .receipt .row.last{background-color: #f90;border:0px;color:#fff;border-radius: 0 0 5px 5px;padding: 10px 20px;}
  .receipt h3{font-size: 18px; margin:10px 0 0 0;}
  .receipt p{font-size: 11px; margin:0 0 10px 0;color: #ccc;}
  /*.receipt .col-md-9{background-color:;}*/
  .receipt .col-md-3{font-size: 20px; text-align: right;font-weight: normal;padding-top: 15px;padding-right: 20px}
  .receipt .row.last .col-md-3{padding: 0;}
  .receipt .row.last h3{margin: 0; padding: 5px 0 0 0;}
</style>
<?php } ?>

<?php if($plan=="Promo-370")
{
	$this->layout="simpleCentrado";
?>
	<div class="promou pago">
		<div class="container">
			<h1>¡No pierdas la oportunidad!</h1>
			<div class="details row">
				<div class="col-md-3 valor">
					<p>Precio</p>
					<h2><strong>$370</strong></h2>
				</div>
				<div class="col-md-3">
					<p>Precio Original</p>
					<h2 style="text-decoration: line-through;">$930</h2>
				</div>
				<div class="col-md-3">
					<p>Descuento</p>
					<h2>-60%</h2>
				</div>
				<div class="col-md-3 last">
					<p>Ahorras</p>
					<h2>$530</h2>
				</div>
			</div>
		</div>
	</div>

	<div class="row" style="background-color:#f4f4f4 !important;">
		<div class="container payments page-layout" style="background-color:f4f4f4 !important;">

<?php }else{ ?>

	<div class="row">
		<div class="container payments page-layout">
			<div class="page-header" style="padding-top:0 !important; margin-top:-30px !important;">
				<h1>Selecciona la forma de pago, que mejor te convenga</h1>
			</div>

			<div class="col-md-3 col-md-offset-3 monthly">
				<h4>Mensual</h4>
				<label class="active">
					<input <?php if($st->interval=="month"){ ?> checked="checked" <?php } ?> class="comboplan" name="plant" id="monthly_plan" type="radio" value="<?php echo $plan; ?>">
					<p>
						<strong>$<?php echo number_format($mensual, 0, ',', '.'); ?></strong>
						<span>/ mes</span>
					</p>
					<p>Pago mes a mes, sin contratos.</p>
				</label>
			</div>

			<div class="col-md-3 yearly">
				<h4>Anual</h4>
				<label>
					<small>2 meses gratis</small>
					<input <?php if($st->interval=="year"){ ?> checked="checked" <?php } ?> class="comboplan" name="plant" id="yearly_plan" type="radio" value="<?php echo $plana; ?>">
					<p>
						<strong>$<?php echo number_format($anualMensual, 0, ',', '.'); ?></strong>
						<span>/ mes</span>
					</p>

					<p>Un pago anual de $ <?php echo strtoupper(Yii::app()->session['currency']). ' ' . number_format($anual, 0, ',', '.'); ?></p>
				</label>
			</div>
			<hr>
			<?php } ?>

		
			<div class="col-md-12">
				<div class="payment-tabs">
				<ul id="paymentTabs" class="nav nav-tabs">
					<li <?php if (!$isPaypalTabActive) echo  'class="active"' ?>><a href="#home" data-toggle="tab" class="credit-card">Tarjeta de cr&eacute;dito</a></li>
					<li <?php if ($isPaypalTabActive) echo  'class="active"' ?>><a href="#profile" data-toggle="tab" class="paypal">Paypal</a></li>
				</ul>
				<!-- Tarjeta de crédito -->
				<div class="tab-content">
					<div class="tab-pane <?php if (!$isPaypalTabActive) echo  'active' ?>" id="home">
						<div class="cc">
							<form accept-charset="UTF-8" action="<?php echo Yii::app()->urlManager->createUrl('site/confirmacion'); ?>" id="subscriptions-form" method="get">
								<input type="hidden" id="plan" class="plan" name="plan" value="<?php echo $plan; ?>"/>
								<div class="form-group email-princ <?php echo ($emailerror!='') ? 'error' : ''; ?>" id="formemail">
									<label for="email">&iquest;Cu&aacute;l es tu email principal?</label>
				    					<input type="email" 
				    						class="form-control input-lg" 
				    						id="email" 
				    						name="email" 
				    						value="<?php echo $email; ?>" 
				    						<?php echo (!Yii::app()->user->isGuest) ? 'readonly="readonly"' : ''; ?> 
			    						>
									<small class="eerror"><?php echo $emailerror; ?></small>
				    					<p class="help-block">Necesitamos tu correo para confirmar tu acceso.</p>
				  				</div>


								<div class="alert alert-danger payment-errors"><p><?php echo $error; ?></p></div>
								
								<div class="form-group field card-number">
									<label for="number">N&uacute;mero de t&uacute; tarjeta:</label>
									<input id="number" autocomplete="off" class="form-control stripe input-lg" type="text" maxlength="16" size="16" value="">
								</div>

								<div class="form-group field ccdate">
									<label>Fecha de expiraci&oacute;n:</label>
									<?php echo OjalaUtils::getDropdownMonths('exp-month',array('id'=>'exp-month', 'class'=>'card-expiry-month form-control stripe')) ?>	
									<?php echo OjalaUtils::getExpireYears('exp-year', array('id'=>'exp-year', 'class'=>'card-expiry-year form-control stripe')) ?>
								</div>

								<div class="field card-cvc form-group">
									<label for="cvc">C&oacute;d de verificaci&oacute;n:</label>
									<input id="cvc" autocomplete="off" class="form-control stripe" maxlength="4" size="4" type="text">
								</div>

								<input id="button" class="btn btn-warning btn-lg btn-block" type="submit" value="Inicia tu suscripci&oacute;n ahora">
							</form>
						</div>
					</div>
					<div class="tab-pane <?php if ($isPaypalTabActive) echo  'active' ?>" id="profile">
						<form action="<?php echo PaypalWrapper::getDomain() ?>cgi-bin/webscr" method="post" target="_top" id="paypal-form">
							<input type="hidden" name="cmd" value="_s-xclick">
							<input id="idPaypal" type="hidden" name="hosted_button_id" value="<?php echo $st->name; ?>">
							<input type="hidden" name="on0" value="email">
							<input type="hidden" name="on1" value="plan">
							<input type="hidden" name="rm" value="1">
							<input type="hidden" name="return" value="<?php echo Yii::app()->controller->createAbsoluteUrl('site/confirm') . '?' . htmlentities(Yii::app()->request->queryString) ?>">
							<div class="form-group email-princ <?php echo ($emailerror!='') ? 'error' : ''; ?>" id="formemail2">
								<label for="email2">&iquest;Cu&aacute;l es tu email principal?</label>
								<input type="email" id="email2" class="form-control input-lg" name="os0" value="<?php echo $email; ?>">
									<small class="eerror"><?php echo $emailerror; ?></small>
								<p class="help-block" id="email2m">Necesitamos tu correo para confirmar tu acceso.</p>
							</div>
							<input type="hidden" class="plan" name="os1" value="<?php echo Yii::app()->session['tipoPlan']; ?>">
							<div class="paypal">
								<div class="monthly paypal-box side">
									<input id="button2" class="btn btn-warning btn-lg btn-block" type="submit" value="Inicia tu suscripci&oacute;n ahora">
								</div>
							</div>
						</form>
					</div>
				</div>
				<span id="siteseal"><script type="text/javascript" src="https://seal.godaddy.com/getSeal?sealID=ie7tluqRrzuESsbvrgDYwo8hHFvHTMx00PX16n1KiK1qqaDJeCpAZwPnk60"></script></span>
			</div>
			</div>
		
		<hr>

		<div class="bottom">
			<h4>Para tener en cuenta</h4>
			<ul>
				<li class="questions">
				&iquest;Preguntas? - (+57) 318-686 2999 - 7 dias a la semana, 8 a.m.–4 p.m. &oacute;
				<a href="mailto:fr@oja.la?cc=h%40oja.la&amp;subject=Tengo%20preguntas%20con%20mi%20pago" target="_blank">Escríbenos un correo</a>
				</li>
				<li class="monthly">
				Al completar el proceso, autom&aacute;ticamente quedar&aacute;s matriculado en la suscripci&oacute;n, se cargar&aacute; a tu tarjeta el monto respectivo al plan seleccionado.
				</li>
				<li class="monthly">
				Tu fecha de corte, ser&aacute; la misma cada mes, si inicias el 1ro de Enero, el cobro se har&aacute;n todos los 1ros de cada mes.
				</li>
				<li class="security">
				Tu informaci&oacute;n personal est&aacute; segura y nunca ser&aacute; compartida con nadie, ni ser&aacute; usada de forma indebida.
				<a href="<?php echo Yii::app()->urlManager->createUrl('site/privacidad'); ?>">Nuestra pol&iacute;tica de privacidad.</a>
				</li>
			</ul>
		</div>
	</div>
</div>

<script>
	$('.comboplan').click(function()
	{
		$(".plan").attr('value', $(this).val());
		var plan = $(this).val();
		$.ajax({
			'url':'<?php echo Yii::app()->urlManager->createUrl("site/valorPlan"); ?>?plan='+plan,
			'cache': false,
			success: function(data){$("#idPaypal").val(data)},
		});
	});

	$.ajax({
		url: '<?php echo Yii::app()->urlManager->createUrl("becas/guardarReferido"); ?>',
		cache: false,
		success: function(data){return data} 
	});

	$('#paypal-form').submit(function(event) 
	{		
		event.preventDefault();
		if($("#email2").val() ==='')
		{
			$("#email2").addClass('error');
			$("#email2m").attr('style', 'color: #FF0000;');
		}
		else
		{
			$("#email2").removeClass('error');
			$("#email2m").attr('style', 'color: #737373;');
			$('#button2').prop('disabled', true);
			$.ajax({
			    'url': '<?php echo Yii::app()->urlManager->createUrl('site/crearUsuario'); ?>?email='+$("#email2").val(),
			    'cache': false,
			    success: function () {
					$('#paypal-form').unbind().submit();
			    }
			});
		}
	});
</script>
