<?php
	//Yii::app()->clientScript->registerScriptFile('if(window.location == window.parent.location) window.location = "https://oja.la/login";', CClientScript::POS_HEAD);
	
	$this->pageTitle = "Login";

	$referrer =  Yii::app()->request->getUrlReferrer(true) ;
	$referrerEmail = OjalaUtils::extractParamFromString($referrer, 'email');
	
	if(empty($model->username) && !empty($referrerEmail)) {
		$model->username = $referrerEmail;
	}

	Yii::app()->facebook->run();
	$facebookUser = Yii::app()->facebook->getUser();
?>

<div class='row devise-view'>
	<div class='col-md-4 col-md-offset-4 mod-devise'>
		<div class='page-header' style="border:0px !important;">
		<?php if ((strpos($referrer, 'suscripciones' ) !== false && strpos($referrer, 'email=') !== false ) || strpos($referrer, 'paypal.com' )!==false):  ?>
			<div class="alert alert-warning">
				Ya se estamos procesando tu pago, para acceder a nuestros cursos
				solo necesitas entrar a tu cuenta  y listo.
			</div>
			<h3>Entrar a Oja.la</h3>
		<?php else: ?>
			<h3>¿Quiéres entrar a tu cuenta?</h3>
		<?php endif; ?>
		</div>

		<?php $form=$this->beginWidget('CActiveForm', array(
			'id'=>'login-form',
			'enableClientValidation'=>true,
			'clientOptions'=>array(
				'validateOnSubmit'=>true,
			),
			'htmlOptions'=>array(
				'class'=>'new_user',
			),
		)); ?>
		
		<input name="utf8" type="hidden" value="&#x2713;" />
		<input name="authenticity_token" type="hidden" value="oFWBdJPsW9Vl1YkrWRiraIGNHQXNcWj8PQQCbPs3JzE=" />
		
		<div class='form-group'>
			<label>Tu correo electrónico:</label>
			<?php echo $form->textField($model,'username', array('type'=>'email', 'size'=>'30', 'class'=>'form-control input-lg')); ?>
			<?php echo $form->error($model,'username'); ?>
		</div>
		
		<div class='form-group'>
			<label for="user_Tu contraseña:">Tu contraseña:</label>
			<?php echo $form->passwordField($model,'password', array('type'=>'password', 'size'=>'30', 'class'=>'form-control input-lg')); ?>
			<?php echo $form->error($model,'password'); ?>
		</div>
		
		<div class='checkbox'>
			<label for="user_remember_me">
				<input name="user[remember_me]" type="hidden" value="0" />
				<input id="user_remember_me" name="user[remember_me]" type="checkbox" value="1" />
				Recordarme en este computador
			</label>
		</div>
		
		<input class="btn btn-warning btn-lg btn-block" name="commit" type="submit" value="Entrar a mi cuenta en Oja.la" />
		
		<?php if(isset(Yii::app()->request->cookies['becado'])){ ?>
			<hr>
			<div class='btn-group links'>
				<a href="<?php echo Yii::app()->urlManager->createUrl('site/registro'); ?>" class="btn btn-default btn-sm">¿Aún no tienes cuenta?</a>
				<a href="<?php echo Yii::app()->urlManager->createUrl('site/recuperarclave'); ?>" class="btn btn-default btn-sm">¿Olvidaste tu contraseña? </a>
			</div>
		<?php }else{ ?>
			<p><span>ó puedes usar</span></p>
			
			<?php 
				if (!$facebookUser) { 
					$redirect_uri="https://oja.la/index.php/site/login"
					?> <a href="<?php echo Yii::app()->facebook->getLoginUrl($redirect_uri); ?>" class="btn btn-info facebook">Tu cuenta de Facebook</a> <?php
				}
				else{
					$logoutUrl = Yii::app()->facebook->getLogoutUrl(array(
						'next'	=> 'http://oja.la', // URL to which to redirect the user after logging out
					));
				?> 
					<a href="<?php echo $logoutUrl; ?>" class="btn btn-info facebook">Salir de tu cuenta de Facebook</a> 
				<?php
					$fb_user_profile = Yii::app()->facebook->getUserProfile();
						print_r($fb_user_profile['email']);
					}
				?>
				
			<div class='btn-group links'>
				<a href="<?php echo Yii::app()->urlManager->createUrl('site/registro'); ?>" class="btn btn-default btn-sm">¿Aún no tienes cuenta?</a>
				<a href="<?php echo Yii::app()->urlManager->createUrl('site/recuperarclave'); ?>" class="btn btn-default btn-sm">¿Olvidaste tu contraseña? </a>
			</div>
		<?php } ?>
		<?php $this->endWidget(); ?>
	</div>
</div>
