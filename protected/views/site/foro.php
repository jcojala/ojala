<?php
    $this->pageTitle = "Foro ".$course->name;
?>
<script type="text/javascript">
    /* * * CONFIGURATION VARIABLES: EDIT BEFORE PASTING INTO YOUR WEBPAGE * * */
    var disqus_shortname = 'ojalaedu'; // required: replace example with your forum shortname

    /* * * DON'T EDIT BELOW THIS LINE * * */
    (function () {
        var s = document.createElement('script'); s.async = true;
        s.type = 'text/javascript';
        s.src = '//' + disqus_shortname + '.disqus.com/count.js';
        (document.getElementsByTagName('HEAD')[0] || document.getElementsByTagName('BODY')[0]).appendChild(s);
    }());
</script>

<div class="row link-container">
	<ul class="pager">
  	<li class="previous"><a href="<?php echo Yii::app()->urlManager->createUrl('site/clase', array('slug'=>$course->slug, 'slugc'=>$topic->slug, 'cat'=>$cat)); ?>">&larr; Regresar a la clase</a></li>
  </ul>
	<div class="info-foro">
		<h6>Curso: <?php echo $course->name; ?></h6>
		<h1><?php echo $topic->na_topic; ?></h1>
	</div>
		<div id="disqus_thread"></div>
    <script type="text/javascript">
        /* * * CONFIGURATION VARIABLES: EDIT BEFORE PASTING INTO YOUR WEBPAGE * * */
        var disqus_shortname = 'ojalaedu'; // required: replace example with your forum shortname

        /* * * DON'T EDIT BELOW THIS LINE * * */
        (function() {
            var dsq = document.createElement('script'); dsq.type = 'text/javascript'; dsq.async = true;
            dsq.src = '//' + disqus_shortname + '.disqus.com/embed.js';
            (document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(dsq);
        })();
    </script>
    <noscript>Por favor habilita JavaScript para que puedas usar  <a href="http://disqus.com/?ref_noscript">este foro.</a></noscript>    
</div>