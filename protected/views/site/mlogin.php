<?php
	Yii::app()->facebook->run();
	$facebookUser = Yii::app()->facebook->getUser();
?>

<style type="text/css">
.modal-backdrop {
	position: fixed;
	top: 0;
	right: 0;
	bottom: 0;
  	left: 0;
  	z-index: 1040;
  	background-color: #000000;
  	border: 0px !important;
}
.modal-backdrop.fade { opacity: 0;}
.modal-backdrop,
.modal-backdrop.fade.in {
  opacity: 0.8;
  filter: alpha(opacity=80);
}
.modal {
  position: fixed;
  top: 10%;
  left: 50%;
  z-index: 1050;
  width: 450px;
  border: 0px !important;
  max-height: 480px !important;
  margin-left: -225px;
  background-color: #ffffff;
  *border: 1px solid #999;
  -webkit-border-radius: 6px;
     -moz-border-radius: 6px;
          border-radius: 6px;
  outline: none;
  -webkit-box-shadow: 0 3px 7px rgba(0, 0, 0, 0.3);
     -moz-box-shadow: 0 3px 7px rgba(0, 0, 0, 0.3);
          box-shadow: 0 3px 7px rgba(0, 0, 0, 0.3);
  -webkit-background-clip: padding-box;
     -moz-background-clip: padding-box;
          background-clip: padding-box;
}
.modal.fade {
  	top: -25%;
  	-webkit-transition: opacity 0.3s linear, top 0.3s ease-out;
       -moz-transition: opacity 0.3s linear, top 0.3s ease-out;
       	 -o-transition: opacity 0.3s linear, top 0.3s ease-out;
        	transition: opacity 0.3s linear, top 0.3s ease-out;
}
.modal.fade.in { top: 10%;}
.modal-header, .modal-footer {display: none;}
.modal-body {
  position: relative;
  padding: 15px 50px;
  overflow-y: auto;
}
.modal-body .facebook{
	padding-top: 10px;
	text-align: center;
}
.modal-body h3{
	border-bottom: 1px solid #e5e5e5;
	text-align: center;
	padding: 0 0 15px 0;
	margin-bottom: 30px;
}
</style>

<div class="login-body">
	<h3>Ingresa con tu cuenta a Oja.la</h3>
	
	<?php $form=$this->beginWidget('CActiveForm', array(
		'id'=>'login-form',
		'enableClientValidation'=>true,
		'clientOptions'=>array(
			'validateOnSubmit'=>true,
		),
		'htmlOptions'=>array(
			'class'=>'new_user',
		),
	)); ?>
		
		<input name="url" type="hidden" value="<?php echo $url; ?>" />
		<div class='form-group'>
			<label for="user_Tu correo electrónico:">Tu correo electrónico:</label>
			<?php echo $form->textField($model,'username', array('type'=>'email', 'size'=>'30', 'class'=>'form-control input-lg')); ?>
			<?php echo $form->error($model,'username'); ?>
		</div>
		<div class='form-group'>
			<label for="user_Tu contraseña:">Tu contraseña:</label>
			<?php echo $form->passwordField($model,'password', array('type'=>'password', 'size'=>'30', 'class'=>'form-control input-lg')); ?>
			<?php echo $form->error($model,'password'); ?>
		</div>
		<div class='checkbox'>
			<label>
				<input name="user[remember_me]" type="hidden" value="0" /><input id="user_remember_me" name="user[remember_me]" type="checkbox" value="1" />
				Recordarme en este computador
			</label>
		</div>
		<input class="btn btn-warning btn-lg btn-block" name="commit" type="submit" value="Entrar a mi cuenta en Oja.la" />
		
		<div class="facebook">
			<p><span>ó</span></p>
		<?php 
			if (!$facebookUser) 
			{ 
				$redirect_uri="https://oja.la/index.php/site/mlogin"
				?> <a href="<?php echo Yii::app()->facebook->getLoginUrl($redirect_uri, Yii::app()->urlManager->createUrl('site/pagoSuscripcion')); ?>" class="btn btn-info facebook">Ingresa con tu cuenta de Facebook</a> <?php
			}
			else
			{
				$logoutUrl = Yii::app()->facebook->getLogoutUrl(array(
				'next'	=> 'http://oja.la', // URL to which to redirect the user after logging out
				));
				?> 

				<a href="<?php echo $logoutUrl; ?>" class="btn btn-info facebook">Salir de tu cuenta de Facebook</a> <?php

				$fb_user_profile = Yii::app()->facebook->getUserProfile();
				print_r($fb_user_profile['email']);
			}
		?>		
		</div>
	<?php $this->endWidget(); ?>

</div>