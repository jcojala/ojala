<?php
	$this->pageTitle = $course->name;
?>

<div class="portada" role="main" style="margin-top: -27px;">
	<div class="row">

		<div itemscope="itemscope" itemtype="http://schema.org/CreativeWork">
			<meta content="UserPageVisits:1092" itemprop="interactionCount">
			<meta content="para, programadores:, iníciate, desarrollo, iphone" itemprop="keywords">
			<meta content="es_LA" itemprop="inLanguage">
			<meta content="Educativo" itemprop="genre">

			<div class="row course-fold">
				<div class="container">
			
					<div class="col-md-7">
						<div class="course-image">
							<img alt="<?php echo $course->name; ?>" src="<?php echo Yii::app()->request->baseUrl; ?>/images/covers/<?php echo $course->cover; ?>" />
							<link href="<?php echo Yii::app()->request->baseUrl; ?>/images/52571a6a0eb1ae5b10000993.jpg" itemprop="thumbnailUrl">
						</div>
					</div>
			
					<div class="col-md-5">
						<h1 itemprop="name"><?php echo $course->name; ?></h1>
						
						<div class="promo-mod" itemprop="offers" itemscope="" itemtype="http://schema.org/Offer">
							<div class="info-box">
								<div class="redeem-box">					
									<?php if(isset(Yii::app()->session['mexico'])){ ?>
										<div class="buttons btns-incmty">
											<a class="btn btn-warning btn-lg" href="<?php echo Yii::app()->urlManager->createUrl('site/asignarCurso', array('id'=>$course->id_course)); ?>">
												Quiero tomar este curso ahora →
											<small>recuerda un curso gratis de nuestra librería</small>
										</a>
										<a class="btn btn-link" href="<?php echo Yii::app()->urlManager->createUrl('site/pagoSuscripcion', array('curso'=>$course->name)); ?>">
											Ó puedes tener acceso a TODA la biblioteca por solo USD$25
										</a>
									</div>
								<?php }else{ ?>
									<div class="buttons">
										<a class="btn btn-warning btn-lg" href="<?php echo Yii::app()->urlManager->createUrl('site/suscripciones'); ?>" data-original-title="" title="">
										Acceso a TODOS los cursos →
										<small>por solo USD$25 al mes</small>
										</a>
										<br>
										<a class="btn btn-default btn-block" data-require-login="Regístrate gratis" href="<?php echo Yii::app()->urlManager->createUrl('site/pagoCurso', array('id'=>$course->id_course)); ?>" data-original-title="" title="">
										ó compra este curso por solo USD$<?php echo $course->cost/100; ?>
										</a>
									</div>
								<?php } ?>
								
							</div>
							</div>
						</div>

					</div>
				</div>
			</div>

			<div class="row course-details">
				<div class="container">
					<div class="col-md-7 program">
						<div class="row">
							<div class="col-md-12 description">
								<h3>¿Qué verás en el curso?</h3>
								<p><?php echo $course->description; ?></p>
							</div>
						</div>
						<div class="row">
							<div class="col-md-12 agenda">
								<h3>Temario del curso:</h3>
								<ul class="lessons">
									<?php $lesson=""; foreach ($list as $topic) { ?>
	                                    <li class="each item" id="lesson-525726bd0eb1ae5b10000a7c">
	                                        <?php if($lesson !== $topic['na_lesson']){ $lesson = $topic['na_lesson']; ?>
												<div class="name" style="margin-bottom:10px;">
													<h5 style="float: left; margin: 8px 0 0 0"><?php echo $topic['na_lesson']; ?></h5>
												</div>
											<?php } ?>
	                                        
	                                        <ul class="topics each-topic">
	                                            <li class="item" id="topic-525727450eb1ae5b10000a8c" name="topic-525727450eb1ae5b10000a8c">
	                                                <div class="position-controls"></div>
	                                                <div class="col-md-12">
	                                                	<h4>
	                                                		<?php echo $topic['na_topic']; ?>
	                                                		<span datetime="">Duración: <?php echo Dates::secondsToTime($topic['duration']); ?></span>
	                                                    </h4>
	                                                </div>
	                                            </li>
	                                        </ul>

	                                    </li>
									<?php } ?>
	                            </ul>
							</div>
						</div>
					</div>
					<div class="col-md-5 pull-right">
						<div class="mod faq">
							<div class="each">
								<img alt="Faq-videos" src="<?php echo Yii::app()->request->baseUrl; ?>/images/faq-videos-b547ed0a34cfedacc1b4e3c229eab249.png">
								<h5>Videocursos paso a paso</h5>
								<p>Contenido 100% Online, videos de alta calidad, que puedes ver cuantas veces quieras a la hora que prefieras.</p>
							</div>
							<div class="each">
								<img alt="Faq-expire" src="<?php echo Yii::app()->request->baseUrl; ?>/images/faq-expire-dafebe946557b41ffedc11867b329f34.png">
								<h5>Acceso ilimitado</h5>
								<p>Tus cursos son tuyos, nunca expirarán, disponibles las 24hrs del día sin restricciones de ningún tipo.</p>
							</div>
							<div class="each">
								<img alt="Faq-dispositivos" src="<?php echo Yii::app()->request->baseUrl; ?>/images/faq-dispositivos-192843e5e8532ec4e879c88865338260.png">
								<h5>Disponibilidad total</h5>
								<p>Puedes tomar tus cursos desde cualquier dispositivo, teléfono, tablet, o computador, incluso desde varios a la vez.</p>
							</div>
							<div class="each">
								<img alt="Faq-certificate" src="<?php echo Yii::app()->request->baseUrl; ?>/images/faq-certificate-0445ea4296df636f0915d64816fe66f0.png">
								<h5>Certificación al terminar</h5>
								<p>Cuando completes el 100% de tu curso, el sistema te dará acceso a tu certificación para que la descargues e imprimas.</p>
							</div>
						</div>
						<?php $this->widget('ShareThis'); ?>

						<div class="mod questions">
							<h4>¿Alguna pregunta?</h4>
							<p>
							Escríbenos a
							<a href="mailto:m@oja.la?cc=fr%40oja.la&amp;subject=Tengo%20una%20pregunta%20sobre%20este%20curso">m@oja.la</a>
							y revisaremos lo que sea q haga falta, preguntas sobre el curso, sobre Oja.la, sobre lo que sea que haga falta, incluso un simple hola sería perfecto ;).
							</p>
						</div>
					</div>
				</div>
			</div>
		</div>

	</div>
</div>