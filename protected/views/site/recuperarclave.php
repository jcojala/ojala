<?php
	$this->pageTitle = "Recuperar Contraseña";
?>
<div class="row devise-view">
	<div class="col-md-4 col-md-offset-4 mod-devise">
		<div class="page-header" style="border:0px !important;">
			<h3>¿Olvidaste tu contraseña?</h3>
		</div>

		<?php if(Yii::app()->user->hasFlash('recoveryMessage')): ?>
		<div class="success">
		<?php echo Yii::app()->user->getFlash('recoveryMessage'); ?>
		</div>
		<?php else: ?>


		<?php if(isset($mensaje)){ ?>
		<div class="alert alert-danger" style="height: 50px;text-align: center;"><?php echo $mensaje; ?></div>
		<?php } ?>
		<?php $form=$this->beginWidget('CActiveForm', array(
			'id'=>'new-user',
			'enableClientValidation'=>true,
			'clientOptions'=>array(
				'validateOnSubmit'=>true,
			),
			'htmlOptions'=>array(
				'class'=>'new_user',
			),
		)); ?>
			<div class="errors"></div>
			<div class="form-group">
				<label>Tu correo electrónico:</label>
				<?php echo $form->textField($user, 'email1', array('type'=>'email', 'size'=>'30', 'class'=>'form-control input-lg')); ?>
				<small class="help-block">A este correo te enviaremos toda la información necesaria para que recuperes tu contraseña.</small>

			</div>
			<input class="btn btn-warning btn-lg btn-block" style="margin-bottom:20px;" name="commit" type="submit" value="Actualizar contraseña">
		<?php $this->endWidget(); ?>

		<?php endif; ?>
		
		<div class="btn-group links">
			<a href="<?php echo Yii::app()->urlManager->createUrl('site/login'); ?>" class="btn btn-default btn-sm">Ingresa con tu cuenta</a>
			<a href="<?php echo Yii::app()->urlManager->createUrl('site/registro'); ?>" class="btn btn-default btn-sm">¿Aún no tienes cuenta?</a>
		</div>

	</div>
</div>