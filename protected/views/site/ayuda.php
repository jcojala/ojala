<?php
$this->pageTitle = "Preguntas Frecuentes";
?>
<div class="row">
	<div class="container-fluid">

<div class="page-header row">
  <div class="col-md-9">
		<h1>Preguntas frecuentes</h1>
		<p>En este listado encontrarás todas las preguntas que podrías tener con respecto a cualquier aspecto de Oja.la, si la pregunta que tienes no está aqui, por favor escribenos a fr@oja.la.</p>
  </div>
</div>

<div class="row">
	
	<div class="col-md-2 col-md-offset-1">
			<div class="filters">
				<h4>Temas</h4>
				<ul class="nav nav-pills nav-stacked">
					<li><a href="#info-general">Información general</a></li>
					<li><a href="#suscripciones">Suscripciones</a></li>
					<li><a href="#buy">Sobre los cursos</a></li>
					<li><a href="#product">Sobre la plataforma</a></li>
				</ul>
			</div>
	</div>

	<div class="col-md-7">
		
		<div class="block" id="info-general">
			<h2>Información General</h2>
			<div class="question">
				<h4>¿Qué es Oja.la?</h4>
				<div class="respond">
					<p>Oja.la es una plataforma de cursos en linea donde puedes aprender desde como crear tu propia aplicación para iPhone hasta como hacer tu propio negocio, te explicamos paso a paso como lograr esto.</p>
				</div>
			</div>
			<hr>
			<div class="question">
			
			<h4>¿Cómo puedo registrarme en Oja.la?</h4>
			<div class="respond">
				<p>Para crear una cuenta o registrarte, puedes usar uno de dos métodos disponibles, puedes usar tu cuenta de Facebook (un click, aceptar permisos y listo) o puedes crear tu cuenta con un correo y una contraseña, en
					<a href="https://oja.la/registro">este link</a>
					puedes hacerlo.
				</p>
			</div>
			</div>
			<hr>
				<div class="question">
					<h4>¿Lo que ofrecen, son clases presenciales?</h4>
					<div class="respond">
						<p>No, nosotros no hcemos cursos fisicos, no te vamos a pedir que vayas a algun lugar, pierdas tiempo y dinero y además intentes aprender en 2hrs todo lo que te podamos enseñar, al contrario, somos cursos virtuales que te ayudaran a asimilar y repasar nuevos y viejos conocimientos.</p>
					</div>
				</div>
				<hr>
				<div class="question">
					<h4>¿Cómo puedes colaborar?</h4>
					<div class="respond">
						<p>
							Si quieres ser parte de esta gran familia solo tienes que escribirnos a
							<a href="mailto:h@oja.la?cc=fr%40oja.la&amp;subject=Quiero%20colaborar%2C%20q%20debo%20hacer%21%21">h@oja.la</a>
							o
							<a href="mailto:fr@oja.la?cc=h%40oja.la&amp;subject=Quiero%20colaborar%2C%20q%20debo%20hacer%21%21">fr@oja.la</a>
							y veremos como podemos trabajar juntos.
						</p>
					</div>
				</div>
				<hr>
				<div class="question">
					<h4>¿Tengo sugerencias, donde puedo enviarlas?</h4>
					<p>
						Nos encanta conocer todas las opiniones que pudieras tener así simplemente enviando un correo a
						<a href="mailto:fr@oja.la?cc=h%40oja.la&amp;subject=Tengo%20una%20pregunta...">fr@oja.la</a>
						o
						<a href="mailto:h@oja.la?cc=fr%40oja.la&amp;subject=Tengo%20una%20pregunta...">h@oja.la</a>
						estarémos al tanto sea para mejorar o para ayudarte.
					</p>
				</div>
				<hr>
				<div class="question">
					<h4>¿Cómo puedo contactarles?</h4>
					<p>
						Oja.la tiene oficinas en 364 University Ave, Palo Alto, CA, 94301, en Estados Unidos, puedes escribirnos a
						<a href="mailto:fr@oja.la?cc=h%40oja.la&amp;subject=Contacto%20%3B%29">fr@oja.la</a>
					</p>
				</div>
				<hr>
				<div class="question">
					<h4>¿Quiénes hacen Oja.la?</h4>
					<p>
						Somos 4 seres humanos dirigiendo un equipo de mas 15 personas que queremos llevar la educación en su propio idioma a cualquier lugar del mundo y a un precio razonable; Oja.la esta conformada por
						<a href="http://www.linkedin.com/profile/view?id=235562448&amp;locale=en_US&amp;trk=tyah" target="_blank">Hernan Aracena</a>
						,
						<a href="http://ve.linkedin.com/in/jcoaks" target="_blank">Juan Robles</a>
						,
						<a href="http://ve.linkedin.com/in/ciscoinc" target="_blank">Jose Pagola</a>
						y
						<a href="http://www.linkedin.com/profile/view?id=18001861&amp;trk=tab_pro" target="_blank">Francisco Rodriguez</a>
					</p>
				</div>
			</div>
			<hr>
			<div class="block" id="suscripciones">
				<h2>Suscripciones</h2>
				<div class="question">
					<h4>¿Si adquiero una suscripción, estoy atado a un contrato?</h4>
					<div class="respond">
						<p>No!, no hay ningún tipo de contrato con Oja.la. Puedes cancelar, modificar o detener tu cuenta en cualquier momento, depende 100% de ti.</p>
					</div>
				</div>
				<hr>
				<div class="question">
					<h4>¿Con una suscripción, a que cursos puedo ingresar?</h4>
					<div class="respond">
						<p>A TODOS, con una suscripción consigues acceso a toda nuestra biblioteca de cursos y sus respectivos materiales de apoyo, y herramientas de aprendizaje como quizzes, exámenes o foros.</p>
					</div>
				</div>
				<hr>
				<div class="question">
					<h4>¿Por cuanto tiempo tengo acceso a estos cursos con mi suscripción?</h4>
					<div class="respond">
						<p>El acceso es ilimitado, siempre que tengas tu cuenta activa en Oja.la puedes acceder a toda tu biblioteca, ingresar las 24 horas del dia, los 7 días de la semana y no te costará mas.</p>
					</div>
				</div>
				<hr>
				<div class="question">
					<h4>¿Qué tan amenudo agregan nuevo material?</h4>
					<div class="respond">
						<p>
							Estamos agregando nuevos videos o nuevos cursos o nuevas ayudas cada semana, si te interesan los próximos cursos puedes revisarlos
							<a href="mailto:jp@oja.la?cc=fr%40oja.la&amp;subject=Sugerencia%20%3B%29">aquí</a>
						</p>
					</div>
				</div>
				<hr>
				<div class="question">
					<h4>¿Cómo funcionan los certificados teniendo una suscripción?</h4>
					<div class="respond">
						<p>De la misma forma que tener acceso a un solo curso, al terminar el curso podrás descargar la certificación de ese curso que has terminado, si tienes membresia podrías tener certificación por todos los cursos.</p>
					</div>
				</div>
				<hr>
				<div class="question">
					<h4>¿Cómo puedo cancelar mi suscripción?</h4>
					<div class="respond">
						<p>
							Desde la aplicación, en
							<a href="<?php echo Yii::app()->urlManager->createUrl('site/perfilEditar'); ?>">la sección de tu perfil</a>
							puedes cancelar directamente y un asesor se contactará contigo para confirmar el proceso,
							<b>esto se debe hacer 3 dias antes de la fecha de corte de tu suscripción.</b>
						</p>
					</div>
				</div>
			</div>
			<hr>
			<div class="block" id="buy">
				<h2>Sobre los cursos</h2>
				<div class="question">
					<h4>¿Ya pagué el curso, ahora cómo accedo a éste?</h4>
					<div class="respond">
						<p>Muy sencillo, tienes varios caminos y todos te llevan a Roma:</p>
						<ul>
							<li>Puedes ingresar desde el menu principal, opción mis cursos.</li>
							<li>Puedes entrar desde el enlace a tu perfil en el menu principal, opción Mi perfil.</li>
							<li>O puedes usar el mail que te enviamos a tu correo con el enlace directo a la primera clase de tu curso.</li>
						</ul>
					</div>
				</div>
				<hr>
				<div class="question">
					<h4>¿Cuál es el tiempo de vigencia de los cursos?</h4>
					<div class="respond">
						<p>Los cursos nunca caducan, tendrás acceso a ellos por siempre, si compras un curso de forma individual tendrás acceso a el siempre q exista tu cuenta con Oja.la, si lo adquieres por medio de una membresia depende 100% de ti.</p>
					</div>
				</div>
				<hr>
				<div class="question">
					<h4>¿Cómo puedo aportar ideas para un curso?</h4>
					<div class="respond">
						<p>Puedes dirigirte desde el menu principal a la opción Próximos, allí estarán todos los cursos que uds han propuesto para que realicemos.</p>
					</div>
				</div>
				<hr>
				<div class="question">
					<h4>¿Uds dan alguna certificación?</h4>
					<div class="respond">
						<p>Si, cuando completes el 100% de tu curso, tendrás acceso a la certificación, esta certificación demuestra que has cumplido con el tiempo necesario para completar los logros de tu curso.</p>
					</div>
				</div>
			</div>
			<hr>
			<div class="block" id="product">
				<h2>Sobre la plataforma</h2>
				<div class="question">
					<h4>¿Además de los videos qué otras herramientas tiene Oja.la?</h4>
					<div class="respond">
						<p>La principal herramienta de educación son los videos de alta calidad, con conocimientos e instructores de expertos, estos videos los apoyamos con:</p>
						<ul>
							<li>Materiales de descarga como lecturas, libros, enlaces a herramientas o información.</li>
							<li>Archivos de cada clase, muchos cursos tecnicos necesitan del paquete de archivos que el experto usa como ejemplo, ese material se puede descargar.</li>
							<li>Exámenes y quizzes para validar el conocimiento adquirido.</li>
							<li>Sección de notas o modo de recordatorios que cada estudiante puede usar como separadores en su curso.</li>
							<li>Foros de discusión por cada curso logrando tanto discusiones como respuestas a dudas comunes</li>
						</ul>
					</div>
				</div>
				<hr>
				<div class="question" style="margin-bottom:50px;">
					<h4>¿Tienen algún sistema de afiliados?</h4>
					<div class="respond">
						<p>
							Tenemos facilidades para que nos ayudes a ofrecer nuestros cursos, puedes escribirnos a
							<a href="mailto:fr@oja.la?cc=h%40oja.la&amp;subject=Afiliados%3F">fr@oja.la</a>
							y revisamos como podemos colaborar.
						</p>
					</div>
				</div>
			</div>
		</div>
</div>
	</div>
</div>
