<?php
  $this->pageTitle = "Tu Perfil";
?>
<div class="page-header">
  <h1>Información de tus Compras</h1>
  <p>Aquí podrás ver toda la información de tus compras, pagos y facturaciones.</p>
</div>

<div class='row'>
  <div class='container' style="padding-top:0;">

    <div class="col-md-2" itemscope="" itemtype="http://schema.org/Person">
      <nav>
        <ul class="nav nav-pills nav-stacked">
          <li><a href="<?php echo Yii::app()->urlManager->createUrl('site/perfilEditar'); ?>">Mi información</a></li>
          <?php if(isset($suscripcion) && $suscripcion){ ?>
          <li><a href="<?php echo Yii::app()->urlManager->createUrl('site/perfilSuscripcion'); ?>">Mi suscripción</a></li>
          <?php } ?>
          <?php if(isset($compras) && $compras){ ?>
          <li class="active"><a href="#">Mis compras</a></li>
          <?php } ?>
          <li><a href="<?php echo Yii::app()->urlManager->createUrl('site/perfilFacturacion'); ?>">Mis datos de facturación</a></li>                    
          <li><a href="<?php echo Yii::app()->urlManager->createUrl('site/CambiarClave'); ?>">Cambiar Contraseña</a></li>
        </ul>
      </nav>
    </div>

    <div class="col-md-9 col-md-offset-1">
      <?php if($compras){ ?>
        <legend>Historial de compras</legend>
        <table class="table table-hover">
          <thead>
            <tr>
              <th>Curso</th>
              <th>Plan adquirido</th>
              <th>Fecha de compra</th>
            </tr>
          </thead>
          <tbody>
            <?php foreach ($compras as $compra) { ?> 
            <tr>
              <td><?php echo $compra['name']; ?></td>
              <td><?php echo $compra['na_stype']; ?></td>
              <td><?php echo date('d/m/Y', strtotime($compra['date'])); ?></td>
            </tr>
            <?php }?>
          </tbody>
          <tbody>
        </table>

      <?php } ?>
      <hr>
      <legend>Historial de facturación</legend>
      <?php if(count($pagos)){ ?>
      <table class="table table-hover">
        <thead>
          <tr>
            <th>Id. Pago</th>
            <th>Plan pagado</th>
            <th>Fecha de pago</th>
            <th>Monto</th>
          </tr>
        </thead>
        <tbody>
          <?php foreach ($pagos as $factura) { ?> 
          <tr>
            <td><?php echo $factura['id_pay']; ?></td>
            <td><?php echo $factura['na_stype']; ?></td>
            <td><?php echo date('d/m/Y', strtotime($factura['date'])); ?></td>
            <td><?php echo $factura['amount']; ?></td>
          </tr>
          <?php }?>
        </tbody>
      </table>
      <?php }else{ ?>
        <span class="label label-info">Por ahora no tenemos registros de esta información</span>
      <?php } ?>

    </div>
  </div>
</div>


<!-- Modal hacer upgrade -->
<div class="modal fade" id="mod-upgrade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      
      <div class="modal-header">
        <button aria-hidden="true" class="close" data-dismiss="modal" type="button">×</button>
        <h4 class="modal-title">¿Quiéres cambiar tu plan suscripción?</h4>
      </div>

      <div class="modal-body plan-upgrade">
        <p>
          <strong>Ahora tienes el Plan Básico activo</strong>, puedes activar el plan Premium ahora:
        </p>

          <div class="col-md-4 active-plan alert alert-info" style="margin: 10px 50px 0 30px;"> 
            <small>Plan <?php echo $suscripcion['na_stype']; ?></small>
            <h1><span>$</span> <?php echo $suscripcion['amount']; ?></h1>
          </div>
          
          <div class="lista">
            <ul>
              <li>Acceso ilimitado a más de 50 cursos, con más de 1,102 video tutoriales.</li>
              <li>Nuevos contenidos agregados cada semana sin costo adicional.</li>
              <li><strong>Tendrás acceso a: Material de descarga, como archivos usados por los instructores, guías, etc.</strong></li>
            </ul>
          </div>

      </div>

      <div class="modal-footer">
        <a type="button" class="btn btn-default" data-dismiss="modal">Cancelar</a>
        <a href="<?php echo Yii::app()->urlManager->createUrl('site/cambiarPlan'); ?>" type="button" class="btn btn-primary">Solicitar Cambiar de plan</a>
      </div>

    </div>
  </div>
</div>


<!-- Modal hacer downgrade -->
<div class="modal fade" id="mod-downgrade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      
      <div class="modal-header">
        <button aria-hidden="true" class="close" data-dismiss="modal" type="button">×</button>
        <h4 class="modal-title">¿Quiéres cambiar tu plan suscripción?</h4>
      </div>

      <div class="modal-body plan-downgrade">
        <p>
          <strong>Ahora tienes el Plan Premium activo</strong>, puedes cambiar al Plan Básico ahora:
        </p>
        

          <div class="col-md-4 active-plan alert alert-info" style="margin: 10px 50px 0 30px;"> 
            <p><strong>Plan <?php echo $suscripcion['na_stype']; ?></strong></p>
            <h1><span>$</span> 25<?php echo $suscripcion['amount']; ?></h1>
            <small>Dólares</small>
          </div>
          
          <div class="lista">
            <ul>
              <li>Acceso ilimitado a más de 50 cursos, con más de 1,102 video tutoriales.</li>
              <li>Nuevos contenidos agregados cada semana sin costo adicional.</li>
              <li><strong>No tendrás más acceso a: Material de descarga, como archivos usados por los instructores, guías, etc.</strong></li>
            </ul>
          </div>

      </div>

      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
        <button type="button" class="btn btn-primary">Cambiar de plan</button>
      </div>

    </div>
  </div>
</div>

<!-- Modal para enviar el mensaje de que el estudiante quiere cancelar, este mensaje lo envia a un correo fr@oja.la -->
<div class="modal fade" id="unsuscribe_dialog" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      
      <div class="modal-header">
        <button aria-hidden="true" class="close" data-dismiss="modal" type="button">×</button>
        <h4 class="modal-title">¿Quiéres cancelar tu suscripción?</h4>
      </div>

      <div class="modal-body" style="padding: 30px 40px 50px 40px; text-align: center;">
        <form action="<?php echo Yii::app()->urlManager->createUrl('site/cancelarSuscripcion'); ?>" method="post" id="contact-form">
          <input type="hidden" name="cancelar" value="cancelar">
          <input class="btn btn-danger" type="submit" value="Quiero iniciar el proceso de cancelación">
          <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar Ventana</button>
        </form>
      </div>
    </div>
  </div>
</div>

<!-- Modal para enviar el mensaje de que el estudiante quiere cancelar, este mensaje lo envia a un correo fr@oja.la -->
<div class="modal fade" id="dialog-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      
      <div class="modal-header">
        <button aria-hidden="true" class="close" data-dismiss="modal" type="button">×</button>
        <h4 class="modal-title">Solicitud Procesada</h4>
      </div>

      <div class="modal-body" style="padding: 30px 40px 50px 40px;">
        <p style="margin: 0"><strong></strong></p>
        <p>Te contactaremos a la brevedad posible, por favor revisa tu correo electrónico.</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
      </div>

    </div>
  </div>
</div>

<!-- Modal para el cambio de la forma de pago -->
<div class="modal fade" id="payment-change" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      
      <div class="modal-header">
        <button aria-hidden="true" class="close" data-dismiss="modal" type="button">×</button>
        <h4 class="modal-title">¿Quiéres cambiar tu forma de pago?</h4>
      </div>

      <div class="modal-body payments" style="padding: 30px 40px;">
        <div class="payment-tabs">
            <h4>Selecciona el nuevo método de pago</h4>
          <ul id="paymentTabs" class="nav nav-tabs">
            <li class="active"><a href="#home" data-toggle="tab" class="credit-card">Tarjeta de crédito</a></li>
            <li><a href="#profile" data-toggle="tab" class="paypal">Paypal</a></li>
          </ul>
        

          <!-- Tarjeta de crédito -->
          <div class="tab-content">
            <div class="tab-pane active" id="home">

            <div class="cc">
              <span class="payment-errors" style="color: red;"><?php //echo $error; ?></span>
              <div class="form-group field card-number">
                <label>Número de tú tarjeta:</label>
                <input autocomplete="off" class="form-control stripe input-lg" type="text" maxlength="16" size="16" data-stripe="number">
              </div>
              <div class="form-group field ccdate">
                <label>Fecha de expiración:</label>
                <select class="card-expiry-month form-control stripe" data-stripe="exp-month">
                  <option value="1">1 - Enero</option>
                  <option value="2">2 - Febrero</option>
                  <option value="3">3 - Marzo</option>
                  <option value="4">4 - Abril</option>
                  <option value="5">5 - Mayo</option>
                  <option value="6">6 - Junio</option>
                  <option value="7">7 - Julio</option>
                  <option value="8">8 - Agosto</option>
                  <option value="9">9 - Septiembre</option>
                  <option value="10">10 - Octubre</option>
                  <option value="11">11 - Noviembre</option>
                  <option value="12">12 - Diciembre</option>
                </select>
                <select class="card-expiry-year form-control stripe" data-stripe="exp-year">
                  <option value="2013">2013</option>
                  <option value="2014">2014</option>
                  <option value="2015">2015</option>
                  <option value="2016">2016</option>
                  <option value="2017">2017</option>
                  <option value="2018">2018</option>
                  <option value="2019">2019</option>
                  <option value="2020">2020</option>
                  <option value="2021">2021</option>
                  <option value="2022">2022</option>
                  <option value="2023">2023</option>
                  <option value="2024">2024</option>
                  <option value="2025">2025</option>
                  <option value="2026">2026</option>
                  <option value="2027">2027</option>
                  <option value="2028">2028</option>
                </select>
              </div>
              <div class="field card-cvc form-group">
                <label>Cód de verificación:</label>
                <input autocomplete="off" class="form-control stripe" maxlength="4" size="4" type="text" data-stripe="cvc">
              </div>
              <input id="button" class="btn btn-warning btn-lg btn-block" name="commit" type="submit" value="Cambia tu tarjeta de crédito ahora">
            </div>
          </div>



          <div class="tab-pane" id="profile">

            <div class="paypal">
              <div class="monthly paypal-box side">
                <p>
                  Plan <span class="plan"></span> de $<span class="monto"></span> incluye
                  <br>
                  7 días de prueba completamente gratis
                </p>
                <form action="<?php echo PaypalWrapper::getDomain() ?>cgi-bin/webscr" method="post" target="_top">
                  <input type="hidden" name="cmd" value="_s-xclick">
                  <input id = "idPaypal" type="hidden" name="hosted_button_id" value="LGH8DXLEEBPZ2">
                  <input id="button" class="btn btn-warning btn-lg btn-block" name="commit" type="submit" value="Cambia tu método de pago ahora">
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<script type="text/javascript">
  $(function() {
      $('#contact-form').submit(function(event) 
      {
          event.preventDefault();

          
          $('#dialog-modal').modal('show');

          $('#unsuscribe_dialog').modal('hide');

          $.ajax({
              type: "POST",
              url: $(this).attr('action'),
              data: $(this).serialize()
          });

          return false;
      });
  });
</script>