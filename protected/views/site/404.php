<?php
$this->pageTitle = "Página de Error";
?>

<div class="row">
	<div class="container error404">
		<div class="col-md-12 page-layout clearfix">
			<div class="row" style="text-align: center;">
				<h2>Al parecer cambiamos la ubicación del curso que buscas</h2>
			</div>
			
			<div class="fourth fold">
				<div class="content">
					<h3>Algunos de nuestros cursos</h3>
					<div class="row">

						<?php foreach ($list as $item) { ?>
						<div class='col-md-3 mod-course'>
							<div class='thumbnail'>
								<div class='img'>
									<a href="<?php echo Yii::app()->urlManager->createUrl('site/curso', array('slug'=>$item['slug'], 'cat'=>$item['cat'])); ?>" class="img-rounded">
										<img src="<?php echo Yii::app()->request->baseUrl; ?>/images/covers/<?php echo $item['cover']; ?>" alt="Portada" />
									</a>
								</div>
								<div class='caption'>
									<h4><a href="<?php echo Yii::app()->urlManager->createUrl('site/curso', array('slug'=>$item['slug'], 'cat'=>$item['cat'])); ?>"><?php echo $item['name']; ?></a></h4>
								</div>
							</div>
						</div>
						<?php }?>

					</div>
					<hr>
					<div class="row" style="text-align: center;">
						<?php if(Yii::app()->user->isGuest){ ?>
						<a href="<?php echo Yii::app()->urlManager->createUrl('site/login'); ?>" class="btn btn-lg btn-success btn-bibloteca">Iniciar Sesión</a>
						<?php } ?>
						<a href="<?php echo Yii::app()->urlManager->createUrl('site/cursos'); ?>" class="btn btn-lg btn-primary btn-bibloteca">Conoce nuestra biblioteca</a>
					</div>
				</div>
			</div>
			<br>
			<br>
			<br>
			<br>
			<div class="row" style="text-align: center;">
				<small><?php echo $code.', '.CHtml::encode($message); ?></small>
			</div>
		</div>
	</div>
</div>