<?php
$this->pageTitle = "Listado de próximos cursos";
?>

<div class="page-header row">
    <div class="col-md-8">
        <h1>Próximos cursos</h1>
        <p>Este listado muestra todos los cursos que Oja.la tendrá para ti próximamente... Si tienes alguna sugerencia, por favor infórmanos.</p>
    </div>
    <div class="col-md-4 buscador">
        <form role="search" method="get" action="<?php echo Yii::app()->urlManager->createUrl('site/proximoscursos'); ?>">			
            <div class="input-group input-group-lg">
                <input type="text" class="form-control" placeholder="¿Qué información buscas?" name="buscar">
            </div>
            <button type="submit" class="input-group-addon glyphicon glyphicon-search"></button>
        </form>
    </div>
</div>

<div class="row">

    <div class="col-md-2 sidebar-filters">

        <!-- Filtros de seccion -->		
        <div class="filters">
            <h4>Secciones</h4>
            <ul class="nav nav-pills nav-stacked" role="navigation">

                <?php foreach ($categorias as $cat) { ?>
                    <?php if ($cat->na_cathegory == $categoria) { ?>

                        <li class="active">

                        <?php } else { ?>

                        <li>

                        <?php } ?>

                        <a href="<?php echo Yii::app()->urlManager->createUrl('site/proximoscursos', array('cat' => $cat->na_cathegory)); ?>"> <?php echo $cat->na_cathegory; ?></a>

                    </li>

                <?php } ?>

            </ul>

        </div>

        <!-- Filtros de orden -->
        <div class="order">

            <h4>Filtros</h4>
            <ul class="nav nav-pills nav-stacked">
                <li><a href="<?php echo Yii::app()->urlManager->createUrl('site/proximoscursos', array('orden' => 'alfabetico')); ?>">Orden alfabético</a></li>
                <li><a href="<?php echo Yii::app()->urlManager->createUrl('site/proximoscursos', array('orden' => 'dificultad')); ?>">Dificultad</a></li>
                <li><a href="<?php echo Yii::app()->urlManager->createUrl('site/proximoscursos', array('orden' => 'fecha')); ?>">Fecha de publicación</a></li>
            </ul>

        </div>
        <!-- * * * * * * -->						
    </div>

    <div class="col-md-10">

        <?php if ($dataProvider->totalItemCount == 0) { ?>

            <div class="blankslate cursos">

                <div class="modal-body">
                    <p><strong>¡Hola<?php
                            if (!Yii::app()->user->isGuest) {
                                echo ' ' . Yii::app()->user->name;
                            }
                            ?>!</strong></p>
                    <p>Cuéntanos, ¿Cuál curso quieres que hagamos para ti? o ¿Cuáles temas te interesan?... De esta forma, podemos planear nuevos cursos que te sean útiles.</p>
                    <form>
                        <textarea class="form-control" placeholder="Escribe aquí tu sugerencia ..." rows="5" name="textosugerencia" id="textosugerencia"></textarea>
                    </form>
                </div>
                <div class="modal-footer">
                    <div class="btn-group">
                        <button type="button" class="btn btn-primary enviar">Enviar sugerencia</button>
                    </div>
                </div>

                <!--comentado-->

                <!--                <h5>Ya estamos trabajando en el curso que buscas, ahora mismo no está disponible. Mientras, aquí hay algunos temas que te pueden ser útiles</h5>
                
                                <div class="links">
                <?php
//                    foreach ($categorias as $cat) {
//                        $nombre = strtolower(str_replace(' ', '', $cat->na_cathegory));
//                        if ($nombre == 'desarrolloweb') {
//
//                            $nombre = 'web';
//                        }
                ?>
                
                                        <a href="<?php // echo Yii::app()->urlManager->createUrl('site/proximoscursos', array('cat' => $cat->na_cathegory));             ?>" class="<?php // echo $nombre             ?>"><?php // echo $cat->na_cathegory             ?></a>
                
                <?php // }
                ?>
                
                                </div>-->

                <!--final de comentado-->

                <div class="modal fade" id="gracias" tabindex="-1" role="dialog" aria-labelledby="gracias" aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-body">
                                <h4 style="text-align: center;margin-top: 30px;"><strong>¡Gracias <?php
                                        if (!Yii::app()->user->isGuest) {
                                            echo Yii::app()->user->name;
                                        }
                                        ?> por tu sugerencia!</strong></h4>
                                <p id="mensaje"></p>
                            </div>
                            <div class="modal-footer">
                                <div class="btn-group">
                                    <button type="button" id="borrarTexto" class="btn btn-default" data-dismiss="modal">Cerrar ventana</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        <?php } else if ($buscar != "") { ?>
            <div class="resultado alert alert-info">
                <p>Estos son los cursos que tenemos sobre: <strong>"<?php echo $buscar; ?>"</strong></p>
            </div>

        <?php } ?>

        <section class="ib-container row" id="ib-container">

            <!-- Modulo de cada uno de los cursos en los listados -->

            <!--probando con un clistview-->

            <?php
            $this->widget('zii.widgets.CListView', array(
                'id' => 'list-auth-items',
                'dataProvider' => $dataProvider,
                'itemView' => '_curso',
                'summaryText' => '', //'Result {start} - {end} of {count} results'
                'pagerCssClass' => 'result-list',
                'pager' => array(
                    'header' => '',
                    'prevPageLabel' => '&lt;',
                    'nextPageLabel' => '&gt;',
                    'firstPageLabel' => '&laquo;',
                    'lastPageLabel' => '&raquo;',
                    'htmlOptions'=>array(
                        'class'=>'pagination pagination-lg',
                        'id'=>'otro'
                    ),
                    // css class        
                    'firstPageCssClass' => '«', //default "first"
                    'lastPageCssClass' => '»', //default "last"
                    'nextPageCssClass' => 'hide', //default "first"
                    'previousPageCssClass' => 'hide', //default "last"
                    'internalPageCssClass' => 'pager1', //default "page"
                    'selectedPageCssClass' => 'active', //default "selected"
                    'hiddenPageCssClass' => 'pager_hidden_li'//default "hidden"                                   
                ),
            ));
            ?>
            <hr>


            <!--fin de probando con clistview-->


        </section>

    </div>

</div>

<div class="modal fade" id="meInteresaCurso" tabindex="-1" role="dialog" aria-labelledby="meInteresaCurso" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                <p>Hola, <strong><?php echo Yii::app()->user->name; ?></strong></p>
                <p>Por favor, escribe tu correo electrónico para informarte cuando el curso esté disponible.</p>
                <form>
                    <input type="text" class="form-control" name="correoElectronico">
                </form>
            </div>
            <div class="modal-footer">
                <div class="btn-group">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar ventana</button>
                    <button type="button" class="btn btn-primary enviar">Enviar</button>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="gracias" tabindex="-1" role="dialog" aria-labelledby="gracias" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                <h4 style="text-align: center;margin-top: 30px;"><strong>¡Gracias, <?php echo Yii::app()->user->name; ?> por tu interés!</strong></h4>
                <p id="mensaje"></p>
            </div>
            <div class="modal-footer">
                <div class="btn-group">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar ventana</button>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(function () {
        $(".enviar").click(function ()
        {
            var curso = $("#curso").val();
            $.ajax({
                'url': '<?php echo Yii::app()->urlManager->createUrl("site/sugerenciaCurso"); ?>' + '?curso=' + curso,
                'cache': false,
                success: function (data)
                {
                    $('#gracias').modal('show');
                }
            });
        });

        $("#borrarTexto").click(function ()
        {
            $("#textosugerencia").val("");
        });
    });
</script>

<!-- Start of GetKudos Script -->
<script>
    (function (w, t, gk, d, s, fs) {
        if (w[gk])
            return;
        d = w.document;
        w[gk] = function () {
            (w[gk]._ = w[gk]._ || []).push(arguments)
        };
        s = d.createElement(t);
        s.async = !0;
        s.src = '//static.getkudos.me/widget.js';
        fs = d.getElementsByTagName(t)[0];
        fs.parentNode.insertBefore(s, fs)
    })(window, 'script', 'getkudos');

    getkudos('create', 'ojala');
</script>
<!-- End of GetKudos Script -->