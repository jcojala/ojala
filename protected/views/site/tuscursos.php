<?php $this->pageTitle = "Tus Cursos"; ?>


<?php if($showMsg) :?>
	<div class="alert alert-success alert-dismissible alert-sucess-payment">
		<button type="button" class="close" data-dismiss="alert">
			<span aria-hidden="true">&times;</span><span class="sr-only">Cerrar</span>
		</button>
		¡Gracias por reactivar tu cuenta! Ya tienes acceso a tus cursos,
		si tienes preguntas no dudes en usar nuestro chat.
	</div>
<?php endif; ?>


<div class="page-header">
	<h1>Tus cursos</h1>
	<p>Este listado muestra todos los cursos que tienes disponibles.</p>
</div>


<div class="content-fluid">
	<div class="row filters-private">
		
		<nav class="navbar navbar-default" role="navigation">
			<ul class="nav navbar-nav navbar-left" id="myTab">
				<li><a href="#todos" data-toggle="tab">Todos</a></li>
				<li><a href="#activos" data-toggle="tab">Activos</a></li>
				<li><a href="#terminados" data-toggle="tab">Terminados</a></li>
				<li><a href="#revisados" data-toggle="tab">Revisados</a></li>
			</ul> 

			<ul class="nav navbar-nav navbar-right">
				<li><a href="#" class="btn" data-toggle="modal" data-target="#sugerenciaCurso">¿Necesitas un curso? ¿Cuál?</a></li> 
			</ul>
		</nav>

	</div>
</div>


<div class='row'>
	<div class="content-fluid">
		
			<div class="modal fade" id="sugerenciaCurso" tabindex="-1" role="dialog" aria-labelledby="sugerenciaCurso" aria-hidden="true">
				<div class="modal-dialog">
					<div class="modal-content">
						<div class="modal-body">
							<p><strong>Hola <?php echo Yii::app()->user->name; ?></strong></p>
							<p>¿Cuéntanos cuál curso quisieras que hicieramos para ti? ¿O cuáles temas te interesan; de esta forma podemos planear nuevos cursos que te sean útiles.</p>
							<form>
								<textarea class="form-control" placeholder="Escribe aquí tu sugerencia ..." rows="5" name="curso" id="curso"></textarea>
							</form>
						</div>
						<div class="modal-footer">
							<div class="btn-group">
								<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar ventana</button>
								<button type="button" class="btn btn-primary enviar">Enviar sugerencia</button>
							</div>
						</div>
					</div>
				</div>
			</div>

			<div class="modal fade" id="gracias" tabindex="-1" role="dialog" aria-labelledby="gracias" aria-hidden="true">
				<div class="modal-dialog">
					<div class="modal-content">
						<div class="modal-body">
							<h4 style="text-align: center;margin-top: 30px;"><strong>Gracias <?php echo Yii::app()->user->name; ?> por tu sugerencia</strong></h4>
							<p id="mensaje"></p>
						</div>
						<div class="modal-footer">
							<div class="btn-group">
								<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar ventana</button>
							</div>
						</div>
					</div>
				</div>
			</div>

			<section class="ib-container row main" id="ib-container">
				<div class="content-fluid">
			<!-- Tab panes -->
			<div class="tab-content">
				<div class="tab-pane fade" id="todos">

					<?php if(count($list)==0){ ?>
						<div class="alert alert-danger">
							<h4>¡Aún te has inscrito en ningún curso! ¿Qué esperas? tenemos uno para ti.</h4>
							<br>
							<a href="<?php echo Yii::app()->urlManager->createUrl('site/cursos'); ?>" class="btn btn-success">Biblioteca de Cursos</a>
						</div>
					<?php }?>



					<?php 
					$revisados=0;
					$terminados=0;
					$activos=0;
					foreach($list as $item)
					{ 
						$cover = !empty($item['cover']) ? $item['cover'] : 'default.jpg';
					?>
						<div class="each">
							<div class="col-md-3 thumbnail"><div class='img'>
								<a href="<?php echo Yii::app()->urlManager->createUrl('site/curso', array('slug'=>$item['slug'], 'cat'=>$item['cat'])); ?>">
									<img src="<?php echo Course::coverUrl($cover, 'medium'); ?>" alt="<?php echo $item['name']; ?>" />	
								</a>
							</div></div>
					
							<div class="col-md-6 caption">
								<h4><a href="<?php echo Yii::app()->urlManager->createUrl('site/curso', array('slug'=>$item['slug'], 'cat'=>$item['cat'])); ?>"><?php echo $item['name']; ?></a></h4>
								
								<?php if($item['progress'] == 0 ){ $revisados++; ?>

									<small>¿Listo para empezar tu curso?</small>
									<div class="row" style="margin:0 !important;">
										<div class="progress" style="padding:0;">
											<div class="progress-bar progress-bar-warning" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="0" style="width: 0%">
				    						<span class="sr-only">0% Complete (success)</span>
				  						</div>
										</div>
									</div>
									<a href="<?php echo Yii::app()->urlManager->createUrl('site/curso', array('slug'=>$item['slug'], 'cat'=>$item['cat'])); ?>" class="btn btn-info">Inicia este curso ahora!  <span class="glyphicon glyphicon-chevron-right"></span></a>

								<?php }else if($item['progress'] > 98 ){ $terminados++; ?>

									<small>Terminaste tu curso, ¡Felicitaciones!</small>
									<div class="row" style="margin:0 !important;">
										<div class="progress" style="padding:0;">
											<div class="progress-bar progress-bar-warning" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%">
				    						<span class="sr-only">100% Complete (success)</span>
				  						</div>
										</div>
									</div>

									<a href="#" class="btn btn-warning solicitar" idcs="<?php echo $item['id_csuscription']; ?>" curso="<?php echo $item['name']; ?>" data-toggle="modal" data-target="#certificado"><span class="glyphicon glyphicon-file"></span>Descargar tu certificado</a>

								<?php }else{ $activos++; ?>

									<small>¿Cómo vas con tu curso?</small>
									<div class="row" style="margin:0 !important;">
										<div class="progress" style="padding:0;">
											<div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="<?php echo $item['progress']; ?>" aria-valuemin="0" aria-valuemax="100" style="width: <?php echo $item['progress']; ?>%">
				    						<span class="sr-only"><?php echo $item['progress']; ?>% Complete (success)</span>
				  						</div>
										</div>
									</div>
									<h5>Tu próxima clase es:<a href="#"><?php echo $item['na_topic']; ?></a></h5>
									<a href="<?php echo Yii::app()->urlManager->createUrl('site/curso', array('slug'=>$item['slug'], 'cat'=>$item['cat'])); ?>" class="btn btn-success">Tomar próxima clase  <span class="glyphicon glyphicon-chevron-right"></span></a>
								
								<?php } ?>
							</div>
					
							<div class="col-md-3 info">
								<h4>Información de Este curso</h4>
								<dl>
									<dt><span class="glyphicon glyphicon-signal"></span>Nivel:</dt>
									<dd><?php echo $item['na_level']; ?></dd>
								</dl>
								<dl>
									<dt><span class="glyphicon glyphicon-time"></span>Duración:</dt>
									<dd><?php echo Dates::secondsToTime2($item['duracion']); ?></dd>
								</dl>
								<dl>
									<dt><span class="glyphicon glyphicon-tag"></span>Categoría:</dt>
									<dd><a href="#"><?php echo $item['na_cathegory']; ?></a></dd>
								</dl>
								<dl>
									<dt><span class="glyphicon glyphicon glyphicon-info-sign"></span>Clases:</dt>
									<dd><?php echo $item['clases']; ?> Clases</dd>
								</dl>
								<?php if($item['progress'] == 0 ){ ?>
									<a href="<?php echo Yii::app()->urlManager->createUrl('site/EliminarCurso', array('id'=>$item['id_csuscription'])); ?>" class="btn btn-danger btn-xs">Eliminar de mi lista</a>
								<?php } ?>
							</div>
						</div>
					<?php } ?>
				</div>
				<div class="tab-pane fade" id="activos">
					<?php foreach($list as $item) { 
						$cover = isset($item['cover']) ? $item['cover'] : 'default.jpg';
					?>
						<?php if($item['progress'] > 0 && $item['progress'] < 98){ ?>
						<div class="each">
							<div class="col-md-3 thumbnail"><div class='img'>
								<a href="<?php echo Yii::app()->urlManager->createUrl('site/curso', array('slug'=>$item['slug'], 'cat'=>$item['cat'])); ?>">
									<img src="<?php echo Course::coverUrl($cover, 'medium'); ?>" alt="<?php echo $item['name']; ?>" />
								</a>
							</div></div>
					
							<div class="col-md-6 caption">
								<h4><a href="<?php echo Yii::app()->urlManager->createUrl('site/curso', array('slug'=>$item['slug'], 'cat'=>$item['cat'])); ?>"><?php echo $item['name']; ?></a></h4>
									<small>¿Cómo vas con tu curso?</small>
									<div class="row" style="margin:0 !important;">
										<div class="progress" style="padding:0;">
											<div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="<?php echo $item['progress']; ?>" aria-valuemin="0" aria-valuemax="100" style="width: <?php echo $item['progress']; ?>%">
				    						<span class="sr-only"><?php echo $item['progress']; ?>% Complete (success)</span>
				  						</div>
										</div>
									</div>
									<h5>Tu próxima clase es:<a href="#"><?php echo $item['na_topic']; ?></a></h5>
									<a href="<?php echo Yii::app()->urlManager->createUrl('site/curso', array('slug'=>$item['slug'], 'cat'=>$item['cat'])); ?>" class="btn btn-success">Tomar próxima clase  <span class="glyphicon glyphicon-chevron-right"></span></a>
							</div>
					
							<div class="col-md-3 info">
								<h4>Información de Este curso</h4>
								<dl>
									<dt><span class="glyphicon glyphicon-signal"></span>Nivel:</dt>
									<dd><?php echo $item['na_level']; ?></dd>
								</dl>
								<dl>
									<dt><span class="glyphicon glyphicon-time"></span>Duración:</dt>
									<dd><?php echo Dates::secondsToTime2($item['duracion']); ?></dd>
								</dl>
								<dl>
									<dt><span class="glyphicon glyphicon-tag"></span>Categoría:</dt>
									<dd><a href="#"><?php echo $item['na_cathegory']; ?></a></dd>
								</dl>
								<dl>
									<dt><span class="glyphicon glyphicon glyphicon-info-sign"></span>Clases:</dt>
									<dd><?php echo $item['clases']; ?> Clases</dd>
								</dl>
							</div>
						</div>
						<?php } ?>
					<?php } ?>
				</div>
				<div class="tab-pane fade" id="terminados">
					<?php foreach($list as $item) { 
						$cover = isset($item['cover']) ? $item['cover'] : 'default.jpg';
					?>
						<?php if($item['progress'] > 98 ){ ?>
						<div class="each">
							<div class="col-md-3 thumbnail"><div class='img'>
								<a href="<?php echo Yii::app()->urlManager->createUrl('site/curso', array('slug'=>$item['slug'], 'cat'=>$item['cat'])); ?>">
									<img src="<?php echo Course::coverUrl($cover, 'medium'); ?>" alt="<?php echo $item['name']; ?>" />
								</a>
							</div></div>
					
							<div class="col-md-6 caption">
								<h4><a href="<?php echo Yii::app()->urlManager->createUrl('site/curso', array('slug'=>$item['slug'], 'cat'=>$item['cat'])); ?>"><?php echo $item['name']; ?></a></h4>
							
									<small>Terminaste tu curso, ¡Felicitaciones!</small>
									<div class="row" style="margin:0 !important;">
										<div class="progress" style="padding:0;">
											<div class="progress-bar progress-bar-warning" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%">
				    						<span class="sr-only">100% Complete (success)</span>
				  						</div>
										</div>
									</div>
									<a href="#" class="btn btn-warning solicitar" idcs="<?php echo $item['id_csuscription']; ?>" curso="<?php echo $item['name']; ?>" data-toggle="modal" data-target="#certificado"><span class="glyphicon glyphicon-file"></span>Descargar tu certificado</a>
							</div>
					
							<div class="col-md-3 info">
								<h4>Información de Este curso</h4>
								<dl>
									<dt><span class="glyphicon glyphicon-signal"></span>Nivel:</dt>
									<dd><?php echo $item['na_level']; ?></dd>
								</dl>
								<dl>
									<dt><span class="glyphicon glyphicon-time"></span>Duración:</dt>
									<dd><?php echo Dates::secondsToTime2($item['duracion']); ?></dd>
								</dl>
								<dl>
									<dt><span class="glyphicon glyphicon-tag"></span>Categoría:</dt>
									<dd><a href="#"><?php echo $item['na_cathegory']; ?></a></dd>
								</dl>
								<dl>
									<dt><span class="glyphicon glyphicon glyphicon-info-sign"></span>Clases:</dt>
									<dd><?php echo $item['clases']; ?> Clases</dd>
								</dl>
							</div>
						</div>
						<?php } ?>
					<?php } ?>	
				</div>
				<div class="tab-pane fade" id="revisados">
					<?php foreach($list as $item) { 
						$cover = isset($item['cover']) ? $item['cover'] : 'default.jpg';
					?>
						<?php if($item['progress'] == 0 ){ ?>
						<div class="each">
							<div class="col-md-3 thumbnail"><div class='img'>
								<a href="<?php echo Yii::app()->urlManager->createUrl('site/curso', array('slug'=>$item['slug'], 'cat'=>$item['cat'])); ?>">
									<img src="<?php echo Course::coverUrl($cover, 'medium'); ?>" alt="<?php echo $item['name']; ?>" />
								</a>
							</div></div>
					
							<div class="col-md-6 caption">
								<h4><a href="<?php echo Yii::app()->urlManager->createUrl('site/curso', array('slug'=>$item['slug'], 'cat'=>$item['cat'])); ?>"><?php echo $item['name']; ?></a></h4>
								
									<small>Listo para empezar tu curso?</small>
									<div class="row" style="margin:0 !important;">
										<div class="progress" style="padding:0;">
											<div class="progress-bar progress-bar-warning" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="0" style="width: 0%">
				    						<span class="sr-only">0% Complete (success)</span>
				  						</div>
										</div>
									</div>
									<a href="<?php echo Yii::app()->urlManager->createUrl('site/curso', array('slug'=>$item['slug'], 'cat'=>$item['cat'])); ?>" class="btn btn-info">Inicia este curso ahora!  <span class="glyphicon glyphicon-chevron-right"></span></a>

							</div>
					
							<div class="col-md-3 info">
								<h4>Información de Este curso</h4>
								<dl>
									<dt><span class="glyphicon glyphicon-signal"></span>Nivel:</dt>
									<dd><?php echo $item['na_level']; ?></dd>
								</dl>
								<dl>
									<dt><span class="glyphicon glyphicon-time"></span>Duración:</dt>
									<dd><?php echo Dates::secondsToTime2($item['duracion']); ?></dd>
								</dl>
								<dl>
									<dt><span class="glyphicon glyphicon-tag"></span>Categoría:</dt>
									<dd><a href="#"><?php echo $item['na_cathegory']; ?></a></dd>
								</dl>
								<dl>
									<dt><span class="glyphicon glyphicon glyphicon-info-sign"></span>Clases:</dt>
									<dd><?php echo $item['clases']; ?> Clases</dd>
								</dl>
								<a href="<?php echo Yii::app()->urlManager->createUrl('site/EliminarCurso', array('id'=>$item['id_csuscription'])); ?>" class="btn btn-danger btn-xs">Eliminar de mi lista</a>
							</div>
						</div>
						<?php } ?>
					<?php } ?>
				</div>
			</div>
		</div>
	</section>
</div></div>

<?php if($terminados > 0): ?>
<div class="modal fade" id="certificado" tabindex="-1" role="dialog" aria-labelledby="certificado" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
	  		<div class="modal-body">
	    		<h2>¡¡¡¡ FELICITACIONES !!!!</h2>
				<p><?php echo Yii::app()->user->name; ?>, has terminado el curso, se esta descargando tu certificado.</p>
	  			<img src="<?php echo Yii::app()->request->baseUrl; ?>/images/aplausos.gif">
	  		</div>
	  		<div class="modal-footer">
	    		<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar ventana</button>
	  		</div>
		</div>
	</div>
</div>
<?php
	endif;

	$activo= "";

	if( $revisados > 0 ) $activo="revisados";
	if( $terminados > 0 ) $activo="terminados";
	if( $activos > 0 ) $activo="activos";

	if($revisados < 1 && $terminados < 1 && $activos < 1) 
		$activo ="todos";
?>

<script>
	$(function() {
		$('#myTab a[href="#<?php echo $activo ?>"]').tab('show');

		$(".enviar").click(function() {
			var curso=$("#curso").val();
			$.ajax({
				'url': '<?php echo Yii::app()->urlManager->createUrl("site/sugerenciaCurso"); ?>'+'?curso='+curso,
				'cache': false,
				success: function (data) 
				{
					$('#sugerenciaCurso').modal('hide');
					$('#gracias').modal('show');
				},
			});
		});

		$(".solicitar").click(function(){
			var idcs=$(this).attr('idcs');
			window.location.href="<?php echo Yii::app()->urlManager->createUrl('site/solicitarCertificado'); ?>"+"?idcs="+idcs;
		});


		var $container	= $('#ib-container .col-md-3'),
			$articles	= $container.children('article'),
			timeout;				

		$articles.on( 'mouseenter', function( event ) {
			var $article	= $(this);
			clearTimeout( timeout );
			timeout = setTimeout( function() {
				if( $article.hasClass('active') ) return false;						
				$articles.not( $article.removeClass('blur').addClass('active') )
				.removeClass('active')
				.addClass('blur');
			}, 40 );
		});

		$container.on( 'mouseleave', function( event ) {
			clearTimeout( timeout );
			$articles.removeClass('active blur');
		});
	});
</script>