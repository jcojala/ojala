<div class="row">
	<div class="container certificado">
		<div class="col-md-10 page-layout clearfix">

			<h1>¡Felicitaciones has completado tu curso!</h1>
			<p>Tu certificado está en proceso, lo recibiras en tu correo y lo podrás imprimir en el momento que desees.</p>
			<a href="<?php echo Yii::app()->urlManager->createUrl('site/sendUserData'); ?>" class="btn btn-danger btn-lg" data-toggle="modal" data-target="#certificado">Solicitar certificado</a>				
			</br>
			</br>
			<img alt="Mi certificado!" class="img" src="<?php echo Yii::app()->request->baseUrl; ?>/images/certificado-demo.jpg" />			
		</div>
	</div>
</div>


<!-- Modal -->
<div class="modal fade" id="certificado" tabindex="-1" role="dialog" aria-labelledby="certificado" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-body">
        <h3>Perfecto! estamos procesando tu certificado</h3>
        <img src="<?php echo Yii::app()->request->baseUrl; ?>/images/graduation.gif" />
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar ventana</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
