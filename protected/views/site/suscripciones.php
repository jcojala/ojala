<?php
	$this->pageTitle = "Financia tu educación";
	$isPaypalTabActive = $gw==='paypal';
?>

<script type="text/javascript">
$(document).ready(function () {
    var length = $('.side-price').height() - $('.price').height() + $('.side-price').offset().top;

    $(window).scroll(function () {
        var scroll = $(this).scrollTop();
        var height = $('.price').height() + 'px';
        if (scroll < $('.side-price').offset().top) {
            $('.price').css({
                'position': 'absolute',
                'top': '0'
            });
        } else if (scroll > length) {
            $('.price').css({
                'position': 'absolute',
                'bottom': '0',
                'top': 'auto'
            });
        } else {
            $('.price').css({
                'position': 'fixed',
                'top': '0',
                'height': height
            });
        }
    });
});
</script>


<div class="menu-princ menu-suscripcion">
<?php $this->widget('MainMenu', array('show'=>'byRole')); ?>
</div>


<div class="row info-suscripcion">
	<div class="container">
		
		<div class="page-header">
			<h1>Acceso ilimitado a todos los cursos y diplomados</h1>
			<h4>Únete y recibe acceso inmediato a todos los cursos y diplomados en la biblioteca de Oja.la.</h4>
		</div>
		

		<div class="col-md-2 side-price">
			<div class="price">
				<div class="monthly">
					<h6>Pago mensual</h6>
					<h1>$<?php echo number_format($amount, 0,',','.')  ?></h1>
					<p><?php echo OjalaPayments::getForeignCurrency(); ?></p>
					<?php $focusForm = strpos(Yii::app()->request->getRequestUri(), 'error=') !== false ?>
					<a id="btnActivate" href="#payments" class="btn btn-lg <?php echo $focusForm ? 'btn-default' : 'btn-success' ?>">Activar acceso ahora</a>
				</div>
				<small class="disclaimer">Sólo recibimos tarjetas de crédito no debito.<img src="<?php echo Yii::app()->request->baseUrl; ?>/images/cardicons.jpg"></small>
				<p><a href="mailto:fr@oja.la?cc=h%40oja.la&amp;subject=Quiero%20acceso%20para%20mi%20equipo%21%21&amp;body=Hola%0AQuiero%20acceso%20para%20mi%20equipo%20y%20quiero%20saber%20que%20debo%20hacer%20entonces%2C%20mi%20equipo%20esta%20compuesto%20por%20____%20personas%20y%20quiero%20q%20tengan%20acceso%20a%20_______%0A%0AGracias%0A%0A">¿Buscas acceso para tu equipo?</a></p>			
			</div>
		</div>

		<div class="col-md-6 col-md-offset-2 side-razones">
			<div class="razones">
			<h2>La mensualidad incluye:</h2>

			<div class="reason">
				<h4><span>✔</span>Acceso a cada curso</h4>
				<p>Solo por el pago mensual puedes tomar todos y cada uno de los cursos en la biblioteca. Tienes acceso a los ya publicados o los nuevos por publicar. Agregamos nuevos cursos semana tras semana.</p>
			</div>

			<div class="reason">
				<h4><span>✔</span>Profundiza en el tema que necesites</h4>
				<p>El acceso te da la oportunidad de profundizar en cualquier tema que necesites por medio de nuestros diplomados. De la misma forma que los cursos tendrás acceso a todos los diplomados disponibles.</p>
			</div>

			<div class="reason">
				<h4><span>✔</span>Asesor personal tiempo completo</h4>
				<p>Vía teléfono, chat, mail o skype, podrás coordinar tus clases y ayuda con un instructor asigando a ti, nunca estarás solo.</p>
			</div>

			<div class="reason">
				<h4><span>✔</span>Ayuda casi en tiempo real</h4>
				<p>Respondemos a tus preguntas en menos de 60 segundos, vía teléfono, chat, mail o skype, de la forma que te sea más sencillo.</p>
			</div>

			<div class="reason">
				<h4><span>✔</span>Certificación al terminar</h4>
				<p>Después de tomar cada curso recibes una certificación y al terminar un Diplomado recibes una certificación física en tu casa.</p>
			</div>

			<div class="reason">
				<h4><span>✔</span>Material de descarga</h4>
				<p>El acceso incluye disponibilidad el material usado por los instructores, de los códigos de cada proyecto desarrollado en los cursos y material de apoyo.</p>
			</div>

			<div class="reason">
				<h4><span>✔</span>100% a tu propio ritmo</h4>
				<p>Tendrás acceso 24hrs del día, 7 dias a la semana, sin ningún tipo de restricción, incluso desde cualquier dispositivo.</p>
			</div>

			<hr id="payments">
			<div class="payment-form">
				<h3>Selecciona tu forma de pago</h3>
				<div>
					<div class="payment-tabs">
						<ul id="paymentTabs" class="nav nav-tabs">
							<li <?php if (!$isPaypalTabActive) echo  'class="active"' ?>><a href="#home" data-toggle="tab" class="credit-card">Tarjeta de cr&eacute;dito</a></li>
							<li <?php if ($isPaypalTabActive) echo  'class="active"' ?>><a href="#profile" data-toggle="tab" class="paypal">Cuenta de Paypal</a></li>
						</ul>
						
						<!-- Tarjeta de crédito -->
						<div class="tab-content">
							<div class="tab-pane <?php if (!$isPaypalTabActive) echo  'active' ?>" id="home">
								<div class="cc">
								<form accept-charset="UTF-8" action="<?php echo Yii::app()->urlManager->createUrl('site/confirmacion'); ?>" id="subscriptions-form" method="get">
									<input type="hidden" id="plan" class="plan" name="plan" value="<?php echo $plan; ?>"/>
									<div class="form-group email-princ <?php echo ($emailerror!='') ? 'error' : ''; ?>" id="formemail">
									<label for="email">&iquest;Cu&aacute;l es tu email principal?</label>
				    					<input type="email" 
				    						class="form-control input-lg" 
				    						id="email" 
				    						name="email" 
				    						value="<?php echo $email; ?>" 
				    						<?php echo (!Yii::app()->user->isGuest) ? 'readonly="readonly"' : ''; ?> 
			    						>
									<small class="eerror"><?php echo $emailerror; ?></small>
				    					<p class="help-block">Necesitamos tu correo para enviarte la confirmación de tu acceso.</p>
				  				</div>


								<div class="alert alert-danger payment-errors"><p><?php echo $error; ?></p></div>
								
								<div class="form-group field card-number">
									<label for="number">N&uacute;mero de t&uacute; tarjeta:</label>
									<input id="number" autocomplete="off" class="form-control stripe input-lg" type="text" maxlength="16" size="16" value="">
								</div>

								<div class="form-group field ccdate">
									<label>Fecha de expiraci&oacute;n:</label>
									<?php echo OjalaUtils::getDropdownMonths('exp-month',array('id'=>'exp-month', 'class'=>'card-expiry-month form-control stripe')) ?>	
									<?php echo OjalaUtils::getExpireYears('exp-year', array('id'=>'exp-year', 'class'=>'card-expiry-year form-control stripe')) ?>
								</div>

								<div class="field card-cvc form-group">
									<label for="cvc">C&oacute;d de verificaci&oacute;n:</label>
									<input id="cvc" autocomplete="off" class="form-control stripe" maxlength="4" size="4" type="text">
								</div>

								<input id="button" class="btn btn-success btn-lg btn-block" type="submit" value="Activa tu acceso ahora">
							</form>
						</div>
					</div>

					<div class="tab-pane <?php if ($isPaypalTabActive) echo  'active' ?>" id="profile">
						<form action="<?php echo PaypalWrapper::getDomain() ?>cgi-bin/webscr" method="post" target="_top" id="paypal-form">
							<input type="hidden" name="cmd" value="_s-xclick">
							<input id="idPaypal" type="hidden" name="hosted_button_id" value="<?php echo $paypalBtnCode ?>">
							<input type="hidden" name="on0" value="email">
							<input type="hidden" name="on1" value="plan">
							<input type="hidden" name="rm" value="1">
							<input type="hidden" name="return" value="<?php echo Yii::app()->controller->createAbsoluteUrl('site/confirm') . '?' . htmlentities(Yii::app()->request->queryString) ?>">
							<div class="form-group email-princ <?php echo ($emailerror!='') ? 'error' : ''; ?>" id="formemail2">
								<label for="email2">&iquest;Cu&aacute;l es tu email principal?</label>
								<input type="email" id="email2" class="form-control input-lg" name="os0" value="<?php echo $email; ?>">
									<small class="eerror"><?php echo $emailerror; ?></small>
								<p class="help-block" id="email2m">Necesitamos tu correo para confirmar tu acceso.</p>
							</div>
							<!--If you change this, it won't affect the price dear reader !-->
							<input type="hidden" name="os1" value="80">
							<div class="paypal">
								<div class="monthly paypal-box">
									<input id="button2" class="btn btn-success btn-lg btn-block" type="submit" value="Activa tu acceso ahora">
								</div>
							</div>
						</form>
					</div>
				</div>
				<div class="text-center">
					<span id="siteseal">
						<script src="https://seal.godaddy.com/getSeal?sealID=ie7tluqRrzuESsbvrgDYwo8hHFvHTMx00PX16n1KiK1qqaDJeCpAZwPnk60"></script>
					</span>
				</div>
			</div>
			</div>

			</div>
			<div class="detalles">
				<h3>Para tener en cuenta:</h3>
				<ul>
				<li class="questions">
					¿Preguntas? - Llama al <a href="tel:+573186862999">(+57) 318-686 2999</a> los 7 dias a la semana, de 8 a.m. a 4 p.m. (UTC -5:00) o
					<a href="mailto:fr@oja.la?cc=h%40oja.la&amp;subject=Tengo%20preguntas%20con%20mi%20pago" target="_blank">escríbenos un correo</a>.
					Respondemos todos los correos personalmente.
				</li>
				<li class="monthly">Al completar el proceso, quedarás matriculado en la suscripción, se cargará automáticamente mes a mes o anualmente a tu tarjeta el monto respectivo.</li>
				<li class="monthly">Tu fecha de corte, será la misma cada mes, si inicias el 1ro de Enero, el cobro se harán todos los 1ros de cada mes.</li>
				<li class="security">Tu información personal está segura y nunca será compartida con nadie, ni será usada de forma indebida.
				</li>
			</ul>
			</div>
			</div>
		</div>

	</div>
</div>

<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl . '/js/jquery-scrollto.js', CClientScript::POS_HEAD); ?>

<script>
	$(document).ready(function(){
		var scrolling = false;

		$('#btnActivate').on('click', function(e){
			e.preventDefault();

			if(!$(this).hasClass('btn-default')){
				scrolling = true;
				$($(this).attr('href')).ScrollTo({
					duration: 800,
					callback: function(){
						scrolling = false;
					}
					//onlyIfOutside: true
				});
				$(this).addClass('btn-default');
				$(this).removeClass('btn-success').addClass('btn-default');
			}
		});

		$(window).on('scroll', function() {
			if(!scrolling){
			    var curTop = $('#payments').offset().top,
			    	screenHeight = $(window).height(),
			    	winTop = $(window).scrollTop();

			    	//console.log('curPos: ' + curTop + ' window: ' + screenHeight);

				if(curTop > screenHeight){
					$('#btnActivate').removeClass('btn-default').addClass('btn-success');
				}

				if( winTop >= curTop && winTop < (curTop + $('.payment-tabs').height())) {
					$('#btnActivate').removeClass('btn-success').addClass('btn-default');	
				}
			}
		});
	});

	$.ajax({
		url: '<?php echo Yii::app()->urlManager->createUrl("becas/guardarReferido"); ?>',
		cache: false,
		success: function(data){return data} 
	});

	$('#paypal-form').submit(function(event) 
	{		
		event.preventDefault();
		if($("#email2").val() ==='')
		{
			$("#email2").addClass('error');
			$("#email2m").attr('style', 'color: #FF0000;');
		}
		else
		{
			$("#email2").removeClass('error');
			$("#email2m").attr('style', 'color: #737373;');
			$('#button2').prop('disabled', true);
			$.ajax({
			    'url': '<?php echo Yii::app()->urlManager->createUrl('site/crearUsuario'); ?>?email='+$("#email2").val(),
			    'cache': false,
			    success: function () {
					$('#paypal-form').unbind().submit();
			    }
			});
		}
	});


    $(function() 
    {
		$('#aplicar-cupon').click(function(event) 
		{
			event.preventDefault();

		  	$.ajax({
				'url':'<?php echo Yii::app()->urlManager->createUrl('site/aplicarCupon'); ?>?cupon='+$("#cupon").val(),
				'cache': false,
		        success: function (data) {
		        	if(data=='no')
		        	{
		        		$("#error-cupon").attr('style', 'color: #c00;text-align: center;display: block;font-weight: bold;font-size: 13px;margin-top: 5px;');
		        	}
		        	else
		        	{
		        		$("#error-cupon").attr('style', 'display: none;');
		        		$("#metodo-pago").html(data);
		        		$("#cupon").attr('style', 'display: none;');
		        		$("#aplicar-cupon").attr('style', 'display: none;');
		        		$("#mensaje-cupon").html("<h3 style='font-size: 40px;margin: 0 0 -15px 0 !important;color: #0a0;'>:)<h3/><p style='margin:0'><strong>Cupón agregado correctamente</strong></p>");
		        		
		        	}
		        },
			});

			return false;
		});

    });
</script>
<?php 
	Yii::app()->clientScript->registerScript('switchStripe', 
		"$(function(){".
			"$('#switchForeign').on('click', function(e){".
				"e.preventDefault();".
				"document.cookie = 'currency=foreign;';".
				"window.location.reload(); ".
			"});".
			"$('#switchLocal').on('click', function(e){".
				"e.preventDefault();".
				"document.cookie = 'currency=local;';".
				"window.location.reload(); ".
			"});".
		"});"
	, CClientScript::POS_END);
?>