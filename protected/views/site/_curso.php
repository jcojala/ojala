
<?php
$cover = "";

if ($data->cover != null) {

    $cover = $data->cover;
} else {

    $cover = 'default.jpg';
}
?>

<div class="col-md-4">
    <article class='mod-course'>

        <div class='thumbnail'>
            <div class='img'>
                <a href="#" alt="..." class="img-rounded">
                    <img class="img-thumbnail" title="<?php echo $data->name; ?>" alt="<?php echo $data->name; ?>" src="<?php echo Course::coverUrl($cover, 'medium') ?>">
                </a>
            </div>	

            <div class='caption'>
                <h6><?php echo count($data->links) ?>  personas interesadas en:</h6>
                <h4>
                    <a href="#">
                        <?php echo $data->name; ?>
                    </a>
                </h4>

                <hr>

                <a id="botonMeInteresa" class="btn btn-warning pull-right" href="<?php echo Yii::app()->urlManager->createUrl('site/propuesta', array('slug' => $data->slug)); ?>" >Más información →</a>

            </div>
        </div>
    </article>
</div>