<?php
$this->pageTitle = "Cambiar Contraseña";
?>
<div class="page-header">
	<h1>Cambiar contraseña </h1>
	<p>Aqui puedes cambiar tu contraseña, lo puedes hacer en el momento que desees y cuantas veces quieras.</p>
</div>

<div class="row">

  <div class='container' style="padding-top:0;margin-bottom:60px;">

    <div class="col-md-2" itemscope="" itemtype="http://schema.org/Person">
      <nav>
        <ul class="nav nav-pills nav-stacked">
          <li><a href="<?php echo Yii::app()->urlManager->createUrl('site/perfilEditar'); ?>">Mi información</a></li>
          <?php if(!empty($suscripcion)){ ?>
          <li><a href="<?php echo Yii::app()->urlManager->createUrl('site/perfilSuscripcion'); ?>">Mi suscripción</a></li>
          <?php } ?>
          <?php if(!empty($compras)){ ?>
          <li><a href="<?php echo Yii::app()->urlManager->createUrl('site/perfilCompras'); ?>">Mis compras</a></li>
          <?php } ?>
          <li><a href="<?php echo Yii::app()->urlManager->createUrl('site/perfilFacturacion'); ?>">Mis datos de facturación</a></li>					          
          <li class="active"><a href="<?php echo Yii::app()->urlManager->createUrl('site/CambiarClave'); ?>">Cambiar Contraseña</a></li>
        </ul>
      </nav>
    </div>

    <div class="col-md-4 col-md-offset-2">

    	<?php if(Yii::app()->user->hasFlash('recoveryMessage')): ?>
				<div class="alert alert-success">
					<?php echo Yii::app()->user->getFlash('recoveryMessage'); ?>
				</div>
			<?php endif; ?>

			<?php 
				$form=$this->beginWidget('CActiveForm', array('id'=>'change-pass')); 

				$form->hiddenField($model,'id_user');
			?>

			<div class="form-group">
				<?php 
					echo $form->label($model,'pass', array('class'=>'control-label'));
					echo $form->passwordField($model,'pass', array('maxlength'=>'30', 'class'=>'form-control input-lg'));
					echo $form->error($model,'pass'); 
				?>
			</div>
		
			<div class="form-group">
				<?php 
					echo $form->label($model,'repeatpass', array('class'=>'control-label'));
					echo $form->passwordField($model,'repeatpass', array('maxlength'=>'30', 'class'=>'form-control input-lg'));
					echo $form->error($model,'repeatpass');
				?>
			</div>
		
			<div class="row submit">
				<input class="btn btn-warning btn-lg btn-block" name="commit" type="submit" value="Guardar Contraseña">
			</div>

		<?php $this->endWidget() ?>

    </div>
  </div>
</div>

