<?php
  $this->pageTitle = "Tus Diplomados";
?>
<script type="text/javascript">
$(function() {
	var $container	= $('#ib-container'),
	$articles	= $container.children('article'),
	timeout;				
	$articles.on( 'mouseenter', function( event ) {
		var $article	= $(this);
		clearTimeout( timeout );
		timeout = setTimeout( function() {
			if( $article.hasClass('active') ) return false;						
			$articles.not( $article.removeClass('blur').addClass('active') )
			.removeClass('active')
			.addClass('blur');
		}, 65 );
	});

	$container.on( 'mouseleave', function( event ) {
		clearTimeout( timeout );
		$articles.removeClass('active blur');
	});

});
</script>

<div class="page-header">
	<h1>Tus Diplomados</h1>
	<p>Este listado muestra los diplomados que tienes activos ahora.</p>
</div>

<div class="row content">
	<?php if(count($listDip)==0){ ?>
		<div class="alert alert-danger">
			<h4>¡Aún no te has inscrito en ningún diplomado! ¿Qué esperas? tienes que estar atento a las próximas inscripciones.</h4> 
		</div>
	<?php }?>
	<section class="ib-container row conatiner" id="ib-container">
		<?php foreach($listDip as $item){ ?>
		<article class='col-md-3 mod-course'>
			<div class='thumbnail'>
				<div class='img'>
					<a href="<?php echo Yii::app()->urlManager->createUrl('site/diplomado', array('slug'=>$item['slug'])); ?>" alt="..." class="img-rounded">
						<img alt="<?php echo $item['nb_group']; ?>" src="<?php echo Yii::app()->request->baseUrl; ?>/images/covers/<?php echo $item['image']; ?>" />
					</a>
				</div>
				<div class='caption'>
					<h4>
						<a href="<?php echo Yii::app()->urlManager->createUrl('site/diplomado', array('slug'=>$item['slug'])); ?>">
							<?php echo $item['nb_group']; ?>
						</a>
					</h4>
					<hr>
					<a href="<?php echo Yii::app()->urlManager->createUrl('site/diplomado', array('slug'=>$item['slug'])); ?>" class="btn btn-primary pull-right">Abrir Diplomado</a>
				</div>
			</div>
		</article>
		<?php } ?>
	</section>
</div>