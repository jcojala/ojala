<div class="row">
  <div class="col-sm-12 validacion">

  	<img alt="Estamos validando tu pago" src="<?php echo Yii::app()->request->baseUrl; ?>/images/validacion.gif">
  	<h1>Estamos confirmando la transacción</h1>
  	<p>Estas a un paso de estar activo en Oja.la, solo un par de segundos más ;)</p>
  </div>
</div>

<?php
	$url = Yii::app()->urlManager->createUrl('site/hasActiveSubscription');
	Yii::app()->clientScript->registerScript('checkStatus',
		"$(function () {
			var counter = 1;

			function checkStatus() {
				counter++;
				if(counter == 85){
					window.location.href = 'confirmacionAsesor?invalidPayment';
				} else {
					$.ajax({
						type:  'GET',
						url: '${url}',
						success: function (data) {
							if(data.trim()==='active') {
								window.location.href = 'confirmacionAsesor';
							} else {
								setTimeout(function () {checkStatus();}, 500);
							}
						},
						error: function () {
							setTimeout(function () {checkStatus();}, 500);
						}
					});
				}
    	}
			setTimeout(function () {checkStatus();}, 3000);
		});",
		CClientScript::POS_END);
?>

<!-- Facebook Conversion Code for Pago -->
<script>(function() {
	var _fbq = window._fbq || (window._fbq = []);
	if (!_fbq.loaded) {
	var fbds = document.createElement('script');
	fbds.async = true;
	fbds.src = '//connect.facebook.net/en_US/fbds.js';
	var s = document.getElementsByTagName('script')[0];
	s.parentNode.insertBefore(fbds, s);
	_fbq.loaded = true;
	}
	})();
	window._fbq = window._fbq || [];
	window._fbq.push(['track', '6015673443095', {'value':'<?php echo $amount; ?>','currency':'<?php echo $currency; ?>'}]);
</script>
<noscript><img height="1" width="1" alt="" style="display:none" src="https://www.facebook.com/tr?ev=6015673443095&amp;cd[value]=<?php echo $amount; ?>&amp;cd[currency]=<?php echo $currency; ?>&amp;noscript=1" /></noscript>