<?php
	$this->pageTitle = "Ideas";
?>
<div class='home' role='main'>
	<div class='row'>
		<div class='row'>
			<div class='container'>
				<div class='page-header'>
					<a href="ideas/new.html" class="btn btn-success" data-remote="true" id="new_idea" style="float: right;">Recomendar curso</a>
					<h2>Próximos cursos</h2>
				</div>
				<div class='courses ideas' id='courses'>
					<div class='row courses-row'>
						<div class='col-md-3 mod-course' id='idea-5017eaf70eb1ae704f00076c'>
							<div class='thumbnail'>
								<div class='img'>
									<a href="<?php echo Yii::app()->urlManager->createUrl('site/votos'); ?>" alt="Diseño de Aplicaciones para Facebook" class="img">
										<img alt="Caras" src="http://www.placehold.it/255x182&text=Curso" />
									</a>
								</div>
								<div class='caption'>
									<h6>
									Por:
									<a href="users/lennin.patricio.html">Lennin Quiñones</a>
									</h6>
									<h4><a href="ideas/diseno-de-aplicaciones-para-facebook.html">Diseño de Aplicaciones para Facebook</a></h4>
									<hr>
									<a href="ideas/diseno-de-aplicaciones-para-facebook.html" class="btn btn-warning pull-right">Ver más de este curso →</a>
								</div>
							</div>
						</div>

						<div class='col-md-3 mod-course' id='idea-501114840eb1ae49f50003ae'>
							<div class='thumbnail'>
								<div class='img'>
									<a href="ideas/aprende-a-realizar-un-estudio-de-mercado.html" alt="Aprende a realizar un Estudio de Mercado" class="img">
										<img alt="Study" src="http://www.placehold.it/255x182&text=Curso" />
									</a>
								</div>
								<div class='caption'>
								<h6>
								Por:
								<a href="users/experto.en.viabilidad.de.negocios.y.estrategias.html">Juan Jose Barbero Dossin</a>
								</h6>
								<h4><a href="ideas/aprende-a-realizar-un-estudio-de-mercado.html">Aprende a realizar un Estudio de Mercado</a></h4>
								<hr>
								<a href="ideas/aprende-a-realizar-un-estudio-de-mercado.html" class="btn btn-warning pull-right">Ver más de este curso →</a>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>