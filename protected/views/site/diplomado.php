<?php $this->pageTitle = $group->nb_group; ?>

<div class="diplomado-l-ui">
  <div class="row fold">
    <div class="col-sm-12">
      <p class="pre">Aprende paso a paso a crear cualquier aplicación desde cero</p>
      <h1><?php echo $group->nb_group ?></h1>
      <small>*No necesitas experiencia</small>
    </div>
  </div>
</div>



<div id="content" class="diplomado">
  <div class="row">
    <div class="container col-md-12">
      <?php 
        if(count($listCourses)>0){
          $course = ""; 
          $lesson = "";
          $count=0;
          $count2=0; 
          $count3=1; 
          $progreso=0;
          $idc=0;
          foreach($listCourses as $topic) { 
            $idc=$topic['id_course'];
            if($count!=0 && $course != $topic['name']){ 
              $count2=0;          
      ?> 
          </div>
          </div>

          <?php } 
            if($course != $topic['name']){
              $course = $topic['name'];
          ?>

          <div id="<?php echo $topic['id_course']; ?>" class="level">
            <div class="info-level clearfix">
              <div class="description">
                <span class="label label-success">Activo</span>
                <h5> Módulo <?php echo $count3; $count3++; ?></h5>
                <h4><?php echo $topic['name']; ?></h4>
              </div>
            </div>
            <?php } ?>

            <?php if($count2!=0 && $lesson != $topic['na_lesson']){  ?>              
          </div>
          <?php } ?>


          <?php if($lesson != $topic['na_lesson']) {      
           $lesson = $topic['na_lesson']; ?>
           <div class="lessons">  
            <div class="name"><h5 style="float: left; margin: 8px 0 0 0"><?php echo $topic['na_lesson']; ?></h5></div>
            <?php }  ?>                   

            <ul class="topics each-topic">
              <li class="item">
                <div class="col-md-9">
                  <h4>
                    <a class="topic-name" href="<?php echo Yii::app()->urlManager->createUrl('site/clase', array('slugc'=>$topic['slug'], 'slug'=>$topic['slugc'], 'cat'=>$topic['cat'])); ?>">
                      <?php echo $topic['na_topic']; ?>
                      <span>Duración: <?php echo Dates::secondsToTime($topic['duration']); ?></span>
                    </a>
                  
                  </h4>
                </div>
                <div class="col-md-3">
                  <div class="advance">
                    <p>Has visto el <?php echo $topic['progress']; ?>%</p>
                    <div class="full"><div class="done" style="width: <?php echo $topic['progress']; ?>%"></div></div>
                    <?php $progreso+=$topic['progress']; ?>
                  </div>
                </div>
              </li>
            </ul>  

            <?php
            $count++;
            $count2++;
          } //Fin del for para el despliegue de cursos del diplomado ?>
        </div>
      </div>

      <?php
      $progreso=($progreso/$count2);

      if($progreso>95 && count($inactiveCourses)>0){ ?>
        <div style="text-align: center;"><a class="btn btn-large btn-success btn-block" href="<?php echo Yii::app()->urlManager->createUrl('site/desbloquearCurso', array('id_course'=>$idc, 'id_gs'=>$gs->id_group_susc)); ?>" style="padding: 10px; font-size: 18px;">Click para Desbloquear el siguiente Nivel</a></div>
      <?php } ?>

      
    <?php } ?>

    <?php if(count($inactiveCourses)>0){ ?>
      <h4 class="reason alert alert-info"><strong>Los siguientes niveles los podrás tomar cuando completes el nivel previo.</strong></h4>
    <?php } ?>
      <?php foreach($inactiveCourses as $inactivo){ ?>
        <div class="level inactivo">
          <div class="info-level clearfix">
            <div class="description">
              <span class="label label-default">Inactivo</span>
              <h5> Módulo <?php echo $inactivo['nivel']; ?></h5>
              <h4><?php echo $inactivo['name']; ?></h4>
            </div>
          </div> 
        </div>
      <?php } ?>

    </div>
  </div>
</div>

<!-- Start of GetKudos Script -->
<script>
(function(w,t,gk,d,s,fs){if(w[gk])return;d=w.document;w[gk]=function(){
(w[gk]._=w[gk]._||[]).push(arguments)};s=d.createElement(t);s.async=!0;
s.src='//static.getkudos.me/widget.js';fs=d.getElementsByTagName(t)[0];
fs.parentNode.insertBefore(s,fs)})(window,'script','getkudos');

getkudos('create', 'ojala');
</script>
<!-- End of GetKudos Script -->