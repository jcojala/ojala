<div class='home' role='main'>
	<div class='row'>
		<div class='container'>

			<?php if(isset(Yii::app()->session['mexico'])){ ?>

			<div class="alert alert-incmty">
				<h1>Selecciona el curso que quieras</h1>
				<p><strong>Recuerda que tienes acceso a un curso completamente gratis</strong><br> si quieres tener acceso a TODOs nuestros cursos, <a href="https://oja.la/suscripciones?utm_source=website&utm_medium=flyers&utm_content=free-course&utm_campaign=INCmty">revisa nuestros planes de suscripción.</a></p>
				<img class="arrow" src="<?php echo Yii::app()->request->baseUrl; ?>/images/arrow2.png" />
			</div>

			<?php } ?>

			<div class='page-header'>
				<h2>Todos nuestros cursos</h2>
			</div>

			<div id='courses'>
				<div class='row'>

					<?php foreach($list as $item){ ?>
						<div class='col-md-3 mod-course'>
							<div class='thumbnail'>
								<div class='img'>
									<a href="<?php echo Yii::app()->urlManager->createUrl('site/tomarCurso', array('id'=>$item['id_course'])); ?>" alt="..." class="img-rounded">
										<img alt="<?php echo $item['name']; ?>" src="<?php echo Yii::app()->request->baseUrl; ?>/images/covers/<?php echo $item['cover']; ?>" />
									</a>
								</div>
								<div class='caption'>
									<h6><?php echo rand(50, 300); ?> personas interesadas en:</h6>
									<h4>
										<a href="<?php echo Yii::app()->urlManager->createUrl('site/tomarCurso', array('id'=>$item['id_course'])); ?>">
											<?php echo $item['name']; ?>
										</a>
									</h4>
									<hr>
									<a href="<?php echo Yii::app()->urlManager->createUrl('site/tomarCurso', array('id'=>$item['id_course'])); ?>" class="btn btn-warning pull-right">Tomar curso →</a>
								</div>
						</div>
					</div>
					<?php } ?>
				</div>
			</div>
		</div>
	</div>
</div>