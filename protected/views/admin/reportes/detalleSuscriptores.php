<?php $dias = array("Domingo","Lunes","Martes","Miercoles","Jueves","Viernes","Sábado"); ?>


<?php if(isset($ini) && isset($fin)){
		$ps=0; 
		$pc=0; 
		$ss=0; 
		$sc=0; 
		$cs=0; 
		$cc=0; 
		foreach($list as $item) 
		{
			if($item['na_event']=="suscriptor")
			{
				if($item['na_ptype']=="Paypal")
  					$ps++; 
  				elseif($item['na_ptype']=="Stripe")
					$ss++;
  				elseif($item['na_ptype']=="Conekta")
					$cs++;
	  		}
	  		elseif($item['na_event']=="cancelado")
			{
				if($item['na_ptype']=="Paypal")
	  				$pc++; 
  				elseif($item['na_ptype']=="Stripe")
					$sc++; 
  				elseif($item['na_ptype']=="Conekta")
					$cc++; 
	  		}
		}
?>
<div class="page-header">
	<h1>Suscriptores desde <?php echo $ini.' hasta '.$fin; ?></h1>
</div>

<div class="row">
	<div class="col-md-4">
		<div class="well" style="margin: 20px 0 0 20px;">
			<span class="label label-warning" style="font-size: 14px;">Stripe</span>
			<hr>
			<span>Nuevos <span class="label label-success" style="font-size: 18px;"><?php echo $ss; ?></span><span>  /  </span></span>
			<span>Cancelados <span class="label label-danger" style="font-size: 18px;"><?php echo $sc; ?></span></span>
		</div>
	</div>
	<div class="col-md-4">
		<div class="well" style="margin: 20px 20px 0 0;">
			<span class="label label-primary" style="font-size: 14px;">Paypal</span>
			<hr>
  			<span>Nuevos <span class="label label-success" style="font-size: 18px;"><?php echo $ps; ?></span><span>  /  </span></span>
  			<span>Cancelados <span class="label label-danger" style="font-size: 18px;"><?php echo $pc; ?></span></span>
		</div>
	</div>
	<div class="col-md-4">
		<div class="well" style="margin: 20px 20px 0 0;">
			<span class="label label-default" style="font-size: 14px;">Conekta</span>
			<hr>
  			<span>Nuevos <span class="label label-success" style="font-size: 18px;"><?php echo $cs; ?></span><span>  /  </span></span>
  			<span>Cancelados <span class="label label-danger" style="font-size: 18px;"><?php echo $cc; ?></span></span>
		</div>
	</div>
</div>

<hr>

<a class="btn btn-default" href="<?php echo Yii::app()->urlManager->createUrl('admin/reporteSuscriptores'); ?>">« Regresar al Reporte</a>

	<?php }elseif(isset($busqueda)){ ?>
  			


		<div class="page-header">
			<h1>Eventos de <?php echo $busqueda; ?></h1>
		</div>

		<div class="row">
			<div class="col-md-4">
     		<a class="btn btn-default" href="<?php echo Yii::app()->urlManager->createUrl('admin/reporteSuscriptores'); ?>">« Regresar al Reporte</a>
				<a class="btn btn-primary" href="<?php echo Yii::app()->urlManager->createUrl('admin/nuevoEvento'); ?>">Nuevo Evento</a>
			</div>
		
			<div class="col-md-4">
  			<form class="form-inline" action="<?php echo Yii::app()->urlManager->createUrl('admin/detalleSuscriptores'); ?>" method="get">
					<input type="text" class="form-control" name="busqueda" placeholder="Buscar por Correo" style="width: 75%;"/>
					<input type="submit" value="Buscar" class="btn btn-default" style="margin-left: -8px;" />
  			</form>
			</div>
		</div>

	<?php } ?>

<hr>

<div class="col-md-12 page-layout">
	<div class="panel panel-default">
  		<div class="panel-body">
    			<div class="row">
      				<div class="col-md-12">
				        <table class="table table-bordered" style="margin: 30px 0 20px 20px; width: 98%">
				            <thead>
				              <tr>
				              	<th>Numero</th>
				              	<th>Fecha</th>
				            	<th>Estado</th>
				            	<th>Pago</th>
				            	<th>Suscriptor</th>
				            	<th>Plan</th>
				            	<th>Opcion</th>
				              </tr>
				            </thead>
				            <tbody>
								<?php $i=0; foreach($list as $item) { $i++; ?>
									<?php if(isset($busqueda) OR $item['na_event']=="suscriptor" OR $item['na_event']=="cancelado" OR $item['na_event']=="deudor"){ ?>
									<tr>
										<td><?php echo $i; ?></td>
										<td><?php echo $dias[date('w', strtotime($item['created_at']))].', '.date('d-m-Y', strtotime($item['created_at'])); ?></td>

										<?php if($item['na_event']=="suscriptor"){ ?>
										<td><span class="label label-success"><?php echo $item['na_event']; ?></span></td>
										<?php }elseif($item['na_event']=="cancelado"){ ?>
										<td><span class="label label-danger"><?php echo $item['na_event']; ?></span></td>
										<?php }else{ ?>
										<td><span class="label label-default"><?php echo $item['na_event']; ?></span></td>
										<?php } ?>

										<?php if($item['na_ptype']=="Paypal"){ ?>
										<td><span class="label label-primary"><?php echo $item['na_ptype']; ?></span></td>
										<?php }elseif($item['na_ptype']=="Stripe"){ ?>
										<td><span class="label label-warning"><?php echo $item['na_ptype']; ?></span></td>
										<?php }else{ ?>
										<td><span class="label label-default"><?php echo $item['na_ptype']; ?></span></td>
										<?php }?>

										<td><a href="<?php echo Yii::app()->urlManager->createUrl('admin/detalleSuscriptores', array('busqueda'=>$item['email1'])); ?>"><?php echo $item['email1']; ?></a></td>
										<?php if(isset($item['plan'])) { ?>
										<td><?php echo $item['plan']; ?></td>
										<?php }else{ ?>
										<td></td>
										<?php } ?>
										<td style="text-align: center;">
											<a class="btn btn-default btn-xs" href="<?php echo Yii::app()->urlManager->createUrl('admin/nuevoEvento', array('id'=>$item['id_happen'])); ?>">Editar</a>
											<a class="btn btn-default btn-xs" href="<?php echo Yii::app()->urlManager->createUrl('admin/buscadorEstudiantes', array('dato'=>$item['email1'])); ?>">Ver Perfil Ojala</a>
										</td>
									</tr>
									<?php }?>
								<?php } ?>
				            </tbody>
			          </table>
          			</div>
        		</div>
  		</div>
	</div>
</div>