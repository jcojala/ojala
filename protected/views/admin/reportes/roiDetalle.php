<div class="page-header">
	<h1>ROI Detalle</h1>
</div>

<hr>

<a class="btn btn-default" href="<?php echo Yii::app()->urlManager->createUrl('admin/roi'); ?>">« Regresar al Reporte</a>

<h2>Suscriptores <?php if(isset($fecha_suscripcion)){ echo $fecha_suscripcion; } ?> Registrados en <?php echo $fecha_registro ?></h2>

<div class="panel panel-primary">
    <table class="table table-bordered" style="margin: auto;">
        <thead>
          <tr>
        	<th>Numero</th>
        	<th>Email</th>
            <th>Registro</th>
            <th>Suscripcion</th>
            <th>Plan</th>
            <th>Metodo</th>
          </tr>
        </thead>
        <tbody>
        	<?php $i=1; foreach ($suscriptores as $key => $value) { ?>
            <tr>
                <td><?php echo $i; $i++; ?></td>
                <td><?php echo $value['email']; ?> <?php if($value['email2']!=""){ echo '* '.$value['email2'].' *'; } ?>
                	<a class="btn btn-link btn-xs" href="<?php echo Yii::app()->urlManager->createUrl('admin/estudiante', array('id'=>$value['id_user'])); ?>"> Abrir</a>
                	<?php if($value['na_ptype']=="STRIPE"){ ?>
                	<a class="btn btn-link btn-xs" href="https://dashboard.stripe.com/customers/<?php echo $value['id_service']; ?>"> Stripe</a>
                	<?php } ?>
                </td>
                <td><?php $registro = new DateTime($value['registro']); echo $registro->format('F d, Y'); ?></td>
                <td><?php $suscripcion = new DateTime($value['suscripcion']); echo $suscripcion->format('F d, Y'); ?>
				<?php //if(Yii::app()->user->name=="jcoaks"){ ?>
					<form action="<?php echo Yii::app()->urlManager->createUrl('admin/actualizarFechaSuscripcion'); ?>" method="get">
                        <?php
                            $this->widget('zii.widgets.jui.CJuiDatePicker',array(
                                'name'=>'fecha',
                                'language'=>'es',
                                'options'=>array(        
                                    'showButtonPanel'=>true,
                                    'dateFormat'=>'dd/mm/yy',
                                    'maxDate'=>'0',
                                ),
                                'htmlOptions'=>array(
                                    'id'=>'fecha-'.$i,
                                    'placeholder'=>'Fecha de suscripcion',
                                    'class'=>'form-control'
                                ),
                            ));
                         ?>
						<input type="hidden" name="id_suscription" value="<?php echo $value['id_suscription']; ?>" />
						<input type="submit" class="btn btn-success btn-xs" value="Guardar">
					</form>
				<?php //}?>
                </td>
                <td><?php echo $value['na_stype']; ?></td>
                <td><?php echo $value['na_ptype']; ?></td>
            </tr>
            <?php } ?>
        </tbody>
    </table>
</div>