<div class="page-header">
	<h1>Reporte <?php echo $tipo; ?></h1>
	<p class="panel-title" style="font-size: 24px;">Reporte de Aquisición Semanal / Mensual.</p>
</div>

<a class="btn btn-default" href="<?php echo Yii::app()->urlManager->createUrl('admin/index') ?>">« Regresar al Administrador</a>

<hr>

<?php
	$totales=0;
	$ingreso=0;
	$crate=0;
	foreach($list as $item) 
	{ 
	 	$totales=$item['acumulado'];
	 	$crate=$item['crate'];
	 	break;
	}
?>
<div class="row">
	<p class="panel-title" style="font-size: 24px;">Resumen Total.</p>
	<div class="col-lg-3 col-sm-6 col-xs-6 col-xxs-12">
		<div class="box">
			<div class="boxicon"><span class="glyphicon glyphicon-thumbs-up"></span></div>
			<div class="boxinfo">
				<span class="title">Suscriptores Totales</span>
				<span class="value"><?php echo $totales; ?></span>
			</div>
		</div>
	</div>
	<div class="col-lg-3 col-sm-6 col-xs-6 col-xxs-12">
		<div class="box">
			<div class="boxicon"><span class="glyphicon glyphicon-thumbs-up"></span></div>
			<div class="boxinfo">
				<span class="title">Ingresos</span>
				<span class="value"><?php echo $ingreso; ?></span>
			</div>
		</div>
	</div>
	<div class="col-lg-3 col-sm-6 col-xs-6 col-xxs-12">
		<div class="box">
			<div class="boxicon"><span class="glyphicon glyphicon-thumbs-up"></span></div>
			<div class="boxinfo">
				<span class="title">Churn Rate</span>
				<span class="value"><?php echo $crate; ?></span>
				<small STYLE="margin-left: 10px;">Ultima Semana</small>
			</div>
		</div>
	</div>
	<div class="col-lg-3 col-sm-6 col-xs-6 col-xxs-12">
		<div class="box">
			<div class="boxicon"><span class="glyphicon glyphicon-thumbs-up"></span></div>
			<div class="boxinfo">
				<span class="title">Leads</span>
				<span class="value"><?php echo $leads; ?></span>
				<small STYLE="margin-left: 10px;">Ultima Semana</small>
			</div>
		</div>
	</div>
</div>

<hr>


<div class="row">
	<div class="col-md-12 page-layout">
		<div class="row">
			<div class="col-md-4">
  			<a class="btn btn-primary" href="<?php echo Yii::app()->urlManager->createUrl('admin/reporteSuscriptores', array('mensual'=>'true')); ?>">Mensual</a>
  			<a class="btn btn-primary" href="<?php echo Yii::app()->urlManager->createUrl('admin/reporteSuscriptores', array('semanal'=>'true')); ?>">Semanal</a>
  			<a class="btn btn-default" href="<?php echo Yii::app()->urlManager->createUrl('admin/nuevoEvento'); ?>">Nuevo Evento</a>
			</div>
			<div class="col-md-4">
  			<form class="form-inline" action="<?php echo Yii::app()->urlManager->createUrl('admin/detalleSuscriptores'); ?>" method="get">
					<input type="text" class="form-control" name="busqueda" placeholder="Buscar por Correo" style="width: 75%;"/>
					<input type="submit" value="Buscar" class="btn btn-default" style="margin-left: -8px;" />
  			</form>
			</div>
			<div class="col-md-4">
				<div class="pull-right">
					Exportar por: 
      			<a class="btn btn-info" id="btnMail" href="#sendMail">Correo</a>
      			<a class="btn btn-success" href="<?php echo Yii::app()->urlManager->createUrl('admin/excelSuscriptores'); ?>">Excel</a>
      			<a class="btn btn-danger" href="<?php echo Yii::app()->urlManager->createUrl('admin/pdfSuscriptores'); ?>">PDF</a>
				</div>
			</div>
		</div>
		<div class="row">
	  		<div id="mail-form" class="panel-body form-inline hidden">
	  			<div class="pull-right">
	      			<div class="form-group">
	      				<input type="text" class="form-control" placeholder="Escriba los destinatarios separados por coma" size="38"/>
	      			</div>
		      		<button class=" btn btn-default" id="btnSendMail">Enviar</button>
	  			</div>
	  		</div>
	  		<div class="text-center">
	      		<div id="alertSuccess" 	class="alert alert-success hidden col-md-10 col-md-offset-1">Correo(s) enviado(s) correctamente</div>
	      		<div id="alertDanger" 	class="alert alert-danger  hidden col-md-10 col-md-offset-1">Problema al enviar el correo, intente de nuevo</div>
	      		<div class="clearfix"></div>
	  		</div>
  		</div>


		<div class="row">
			<div class="col-md-12">
	        	<table class="table table-bordered" style="margin: 30px 0 20px 20px; width: 98%">
					<thead>
						<tr>
							<th>#</th>
							<th>Dia de Inicio</th>
							<th>Dia de Fin</th>
							<th>Totales</th>
							<th>Nuevos</th>
							<th>Cancelados</th>
							<th>Churn Rate</th>
							<th>Opcion</th>
						</tr>
					</thead>
					<tbody>
						<?php foreach($list as $item) { ?>
						<tr>
							<td><?php echo $item['numero']; ?></td>
							<td><?php echo $item['inicio']; ?></td>
							<td><?php echo $item['fin']; ?></td>
							<td><?php echo $item['acumulado']; ?></td>
							<td><?php echo $item['nuevos']; ?></td>
							<td><?php echo $item['cancelados']; ?></td>
							<td><?php echo $item['crate']; ?></td>
							<td><a href="<?php echo Yii::app()->urlManager->createUrl('admin/detalleSuscriptores', array('ini'=>$item['inibd'], 'fin'=>$item['finbd'])); ?>">Listado</a></td>
						</tr>
						<?php } ?>
					</tbody>
	      		</table>
			</div>
	    </div>
	</div>
</div>


<?php
	Yii::app()->clientScript->registerScript('mailForm',
		"$().ready(function(){".
			"$('#btnMail').on('click',function(){".
				"$('#mail-form').toggleClass('hidden');".
				"if($('#mail-form').is(':hidden')){".
				"$('#mail-form input').val('');".
				"}".
			"});".

			"$('#btnSendMail').on('click', function(){".
			 	"var correos = $('#mail-form input').val();".
			 	"if (correos!==''){".
				 	"$.ajax({".
				 		"type: 'POST',".
				 		"data: {mails: correos}," .
				 		"url: '" . Yii::app()->createUrl('admin/mailSuscriptores') . "',".
				 		"beforeSend: function(){".
			 			"	$('.alert').addClass('hidden');".
			 			"	$('#btnSendMail').html('Enviando...');".
			 			"	$('#btnSendMail').prop('disabled',true);".
				 		"},".
				 		"success: function(data){".
			 			" $('#alertSuccess').removeClass('hidden');".
			 			" $('#mail-form input').val('');".
			 			" $('#mail-form').addClass('hidden');".
			 			" $('#alertSuccess').delay(4000).fadeOut('slow',function(){ $(this).addClass('hidden').css('display','block');});".
				 		"},".
				 		"error: function(){".
						" $('#alertDanger').removeClass('hidden');".
			 			" $('#alertDanger').delay(4000).fadeOut('slow',function(){ $(this).addClass('hidden').css('display','block');});".
				 		"},".
				 		"complete: function(){".
			 			"	$('#btnSendMail').html('Enviar');".
			 			"	$('#btnSendMail').prop('disabled',false);".
				 		"}" .
			 		"});".
		 		"}".
			"});" .
		"});"
		,CClientScript::POS_END
	);
