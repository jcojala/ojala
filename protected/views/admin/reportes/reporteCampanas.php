<div class="page-header">
	<h1>Reporte de Campañas</h1>
	<p class="panel-title" style="font-size: 24px;">Listado de ultimas suscripciones por campañas.</p>
</div>

<a class="btn btn-default" href="<?php echo Yii::app()->urlManager->createUrl('admin/index') ?>">« Regresar al Administrador</a>

<hr>

<div class="panel panel-primary">
    <div class="panel-heading">
		<h4>Ultimas Campañas</h4>
    </div>
    <div class="panel-footer">
		<?php if(count($campanas)>0){ foreach($campanas as $campana) { if($campana['campana']!=""){ ?>
			<a type="button" class="btn btn-primary" href="<?php echo Yii::app()->urlManager->createUrl('admin/campana', array('utm_campaign'=>$campana['campana'])); ?>"><?php echo $campana['campana']; ?></a>
		<?php }}} ?>
    </div>
</div>

<div class="panel panel-primary">
    <div class="panel-heading">
		<h3>Ultimos Suscriptores por Campañas</h3>
    </div>
	    <table class="table table-bordered" style="margin: auto;">
	        <thead>
	          <tr>
	          	<th>Plan</th>
	        	<th>Suscriptor</th>
	          	<th>Email</th>
	        	<th>Registro</th>
	        	<th>Registro</th>
	        	<th>Suscripcion</th>
	        	<th>Suscripcion</th>
	        	<th></th>
	          </tr>
	        </thead>
	        <tbody>
				<?php if(count($suscriptores)>0){ foreach($suscriptores as $suscriptor) { ?>
					<tr>
						<td><?php echo $suscriptor['na_stype']; ?></td>
						<td><?php echo $suscriptor['name']; ?></td>
						<td><?php echo $suscriptor['email1']; ?></td>
						<td><?php echo $suscriptor['date']; ?></td>
						<td><a type="button" class="btn btn-default btn-xs" href="<?php echo Yii::app()->urlManager->createUrl('admin/campana', array('utm_campaign'=>$suscriptor['utm_campaign'])); ?>"><?php echo $suscriptor['utm_campaign']; ?></a></td>
						<td><?php echo $suscriptor['date']; ?></td>
						<td><a type="button" class="btn btn-default btn-xs" href="<?php echo Yii::app()->urlManager->createUrl('admin/campana', array('utm_campaign'=>$suscriptor['utm_campaign'])); ?>"><?php echo $suscriptor['utm_campaign']; ?></td>
						<td><a type="button" class="btn btn-primary btn-xs" href="<?php echo Yii::app()->urlManager->createUrl('admin/estudiante', array('id'=>$suscriptor['id_user'])); ?>">Abrir</a></td>
					</tr>
				<?php }} ?>
	        </tbody>
	  	</table>
</div>
