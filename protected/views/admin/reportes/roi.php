<?php $meses = array("Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"); ?>

<div class="page-header">
	<h1>ROI</h1>
</div>
<h2>Suscriptores</h2>
<div class="panel panel-primary">
	<table class="table table-bordered" style="margin: auto; font-size: 12px;">
		<thead>
		  <tr>
			<th>Mes</th>
			<th>Gastado</th>
			<th>Leads</th>
			<th>Costo/lead</th>
			<th>Suscritos</th>
			<th>Costo/Suscriptor</th>
			<th>Acumulado</th>
			<th>Mes 1</th>
			<th>Mes 2</th>
			<th>Mes 3</th>
			<th>Mes 4</th>
			<th>Mes 5</th>
			<th>Mes 6</th>
			<th>Mes 7</th>
			<th>Mes 8</th>
			<th>Mes 9</th>
			<th>Mes 10</th>
		  </tr>
		</thead>
		<tbody>
			<?php foreach ($lista as $key => $value) { ?>
				<tr>
					<td><?php echo $meses[$value['mes']-1].' de '.$value['ano']; ?></td>
					<td><?php echo $value['gasto']; ?></td>
					<td><?php echo $value['leads']; ?></td>
					<td><?php echo $value['cleads']; ?></td>
					<td><a href="<?php echo Yii::app()->urlManager->createUrl('admin/roiDetalle', array('mes'=>$value['mes'], 'ano'=>$value['ano'])); ?>"><?php echo $value['suscriptores']; ?></a></td>
					<td><?php echo $value['csuscriptor']; ?></td>
					<td><?php echo $value['suscriptores']; ?></td>
					<td><a href="<?php echo Yii::app()->urlManager->createUrl('admin/roiDetalle', array('cont'=>0, 'mes'=>$value['mes'], 'ano'=>$value['ano'])); ?>"><?php echo $value['0']; ?></a></td>
					<td><a href="<?php echo Yii::app()->urlManager->createUrl('admin/roiDetalle', array('cont'=>1, 'mes'=>$value['mes'], 'ano'=>$value['ano'])); ?>"><?php echo $value['1']; ?></a></td>
					<td><a href="<?php echo Yii::app()->urlManager->createUrl('admin/roiDetalle', array('cont'=>2, 'mes'=>$value['mes'], 'ano'=>$value['ano'])); ?>"><?php echo $value['2']; ?></a></td>
					<td><a href="<?php echo Yii::app()->urlManager->createUrl('admin/roiDetalle', array('cont'=>3, 'mes'=>$value['mes'], 'ano'=>$value['ano'])); ?>"><?php echo $value['3']; ?></a></td>
					<td><a href="<?php echo Yii::app()->urlManager->createUrl('admin/roiDetalle', array('cont'=>4, 'mes'=>$value['mes'], 'ano'=>$value['ano'])); ?>"><?php echo $value['4']; ?></a></td>
					<td><a href="<?php echo Yii::app()->urlManager->createUrl('admin/roiDetalle', array('cont'=>5, 'mes'=>$value['mes'], 'ano'=>$value['ano'])); ?>"><?php echo $value['5']; ?></a></td>
					<td><a href="<?php echo Yii::app()->urlManager->createUrl('admin/roiDetalle', array('cont'=>6, 'mes'=>$value['mes'], 'ano'=>$value['ano'])); ?>"><?php echo $value['6']; ?></a></td>
					<td><a href="<?php echo Yii::app()->urlManager->createUrl('admin/roiDetalle', array('cont'=>7, 'mes'=>$value['mes'], 'ano'=>$value['ano'])); ?>"><?php echo $value['7']; ?></a></td>
					<td><a href="<?php echo Yii::app()->urlManager->createUrl('admin/roiDetalle', array('cont'=>8, 'mes'=>$value['mes'], 'ano'=>$value['ano'])); ?>"><?php echo $value['8']; ?></a></td>
					<td><a href="<?php echo Yii::app()->urlManager->createUrl('admin/roiDetalle', array('cont'=>9, 'mes'=>$value['mes'], 'ano'=>$value['ano'])); ?>"><?php echo $value['9']; ?></a></td>
				</tr>
			<?php } ?>
		</tbody>
	</table>
</div>

<h2>Porcentaje</h2>
<div class="panel panel-primary">
	<table class="table table-bordered" style="margin: auto; font-size: 12px;">
		<thead>
		  <tr>
			<th>Mes</th>
			<th>Gastado</th>
			<th>Leads</th>
			<th>Costo/lead</th>
			<th>Suscritos</th>
			<th>Costo/Suscriptor</th>
			<th></th>
			<th>Mes 1</th>
			<th>Mes 2</th>
			<th>Mes 3</th>
			<th>Mes 4</th>
			<th>Mes 5</th>
			<th>Mes 6</th>
			<th>Mes 7</th>
			<th>Mes 8</th>
			<th>Mes 9</th>
			<th>Mes 10</th>
		  </tr>
		</thead>
		<tbody>
			<?php foreach ($lista as $key => $value) { ?>
				<tr>
					<td><?php echo $meses[$value['mes']-1].' de '.$value['ano']; ?></td>
					<td><?php echo $value['gasto']; ?></td>
					<td><?php echo $value['leads']; ?></td>
					<td><?php echo $value['cleads']; ?></td>
					<td><?php echo $value['suscriptores']; ?></td>
					<td><?php echo $value['csuscriptor']; ?></td>
					<td><?php echo $value['conversion']; ?></td>
					<td><?php echo $value['10']; ?></td>
					<td><?php echo $value['11']; ?></td>
					<td><?php echo $value['12']; ?></td>
					<td><?php echo $value['13']; ?></td>
					<td><?php echo $value['14']; ?></td>
					<td><?php echo $value['15']; ?></td>
					<td><?php echo $value['16']; ?></td>
					<td><?php echo $value['17']; ?></td>
					<td><?php echo $value['18']; ?></td>
					<td><?php echo $value['19']; ?></td>
				</tr>
			<?php } ?>
		</tbody>
	</table>
</div>

<h2>Costo de Suscriptores</h2>
<div class="panel panel-primary">
	<table class="table table-bordered" style="margin: auto; font-size: 12px;">
		<thead>
		  <tr>
			<th>Mes</th>
			<th>Gastado</th>
			<th>Leads</th>
			<th>Costo/lead</th>
			<th>Suscritos</th>
			<th>Costo/Suscriptor</th>
			<th>Acumulado</th>
			<th>Mes 1</th>
			<th>Mes 2</th>
			<th>Mes 3</th>
			<th>Mes 4</th>
			<th>Mes 5</th>
			<th>Mes 6</th>
			<th>Mes 7</th>
			<th>Mes 8</th>
			<th>Mes 9</th>
			<th>Mes 10</th>
		  </tr>
		</thead>
		<tbody>
			<?php foreach ($lista as $key => $value) { ?>
				<tr>
					<td><?php echo $meses[$value['mes']-1].' de '.$value['ano']; ?></td>
					<td><?php echo $value['gasto']; ?></td>
					<td><?php echo $value['leads']; ?></td>
					<td><?php echo $value['cleads']; ?></td>
					<td><?php echo $value['suscriptores']; ?></td>
					<td><?php echo $value['csuscriptor']; ?></td>
					<td><?php echo ''; ?></td>
					<td><?php echo $value['20']; ?></td>
					<td><?php echo $value['21']; ?></td>
					<td><?php echo $value['22']; ?></td>
					<td><?php echo $value['23']; ?></td>
					<td><?php echo $value['24']; ?></td>
					<td><?php echo $value['25']; ?></td>
					<td><?php echo $value['26']; ?></td>
					<td><?php echo $value['27']; ?></td>
					<td><?php echo $value['28']; ?></td>
					<td><?php echo $value['29']; ?></td>
				</tr>
			<?php } ?>
		</tbody>
	</table>
</div>