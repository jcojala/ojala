<div class="page-header">
    <h1>Top 10 de usuarios que mas entran a la plataforma.</h1>
</div>

<a class="btn btn-default" href="<?php echo Yii::app()->urlManager->createUrl('admin/index') ?>">« Regresar al Administrador</a>

<hr>

<div class="panel panel-primary">
    <?php if(count($list)>0){ ?>
    <table class="table table-bordered table-striped">
        <thead>
          <tr>
            <th>Posición</th>
            <th>Usuario</th>
            <th>Email</th>
            <!-- <th>Fecha de Registro</th> -->
            <th>Numero de Accesos</th>
          </tr>
        </thead>
        <tbody>
            <?php $i=1; foreach($list as $item) { ?>
            <tr>
                <td><?php echo $i; $i++; ?></td>
                <td><?php echo $item['name']; ?></td>
                <td><a class="btn btn-link" href="<?php echo Yii::app()->urlManager->createUrl('admin/estudiante', array('id'=>$item['id_user'])); ?>"><?php echo $item['email1']; ?></a></td>
                <!-- <td><?php echo $item['create']; ?></td> -->
                <td><?php echo $item['login']; ?></td>
            </tr>
            <?php } ?>
        </tbody>
    </table>
    <?php }else{ ?>
        <span class="label label-info">No hay listado</span>
    <?php } ?>
</div>