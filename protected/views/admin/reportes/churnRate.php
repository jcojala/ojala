<?php $meses = array("Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"); ?>

<div class="page-header">
	<h1>Churn Rate Mensual</h1>
	<p class="panel-title" style="font-size: 20px;">Porcentaje de suscriptores que han cancelado en los primeros 10 meses en oja.la.</p>
</div>

<div class="panel">
    <table id="report" class="table table-bordered">
        <thead>
          <tr>
        	<th>Mes</th>
            <th>Suscriptores</th>
            <th>Nuevos</th>
            <?php 
                for ($i=0; $i < $monthToStudy; $i++) { 
                    echo '<th>Mes' . $i . '</th>';
                }
            ?>
          </tr>
        </thead>
        <tbody>
        	<?php foreach ($listaCohort as $key => $value) { ?>
			<tr>
				<td><a class="btn btn-link" href="<?php echo Yii::app()->urlManager->createUrl('admin/detalleMes', array('deudores'=>$deudores, 'mes'=>$value['mes'], 'ano'=>$value['ano'])); ?>"><?php echo $meses[$value['mes']-1].' de '.$value['ano']; ?></a></td>
                <td><?php echo $value['acumulado']; ?></td>
                <td><?php echo $value['suscriptores']; ?></td>
                <?php
                    for ($i=0; $i < $monthToStudy; $i++) { 
                        echo '<td ';
                        if($value['Mes' . $i] != '0' ){ 
                            echo 'style="background-color: rgba(255, 0, 0, '.($value['PMes' . $i]/100).')"'; 
                            echo sprintf(">%s (%s%%)",$value['Mes'.$i], $value['PMes'.$i]);
                        } else {
                            echo '>&nbsp;';
                        }    
                        echo '</td>';
                    }
                ?>
            </tr>
            <?php }?>
        </tbody>
    </table>
</div>