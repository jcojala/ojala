<?php $meses = array("Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"); ?>

<div class="page-header">
	<h1>Detalle de los Suscriptores del Mes</h1>
	<p class="panel-title" style="font-size: 24px;">Suscriptores que han cancelado o deudores en los primeros 10 meses en oja.la.</p>
</div>

<div class="panel panel-primary">
    <table class="table table-bordered" style="margin: auto;">
        <thead>
          <tr>
            <th>Fecha (S/C/D)</th>
        	<th>Email</th>
            <th>Estado</th>
            <th>Plan</th>
            <th>Servicio</th>
          </tr>
        </thead>
        <tbody>
        	<?php foreach ($lista as $key => $value) { ?>
                <tr class="success">
                    <td colspan="6"><?php echo $value['titulo'].' '.$meses[$value['mes']-1].' de '.$value['ano'].' - '.$value['cuantos']; ?></td>
                </tr>
                <?php foreach ($value['lista'] as $estudiante) { 
                    $user2=User::model()->findbyAttributes(array('email1'=>$estudiante['email']));
                    $email2='';
                    if($user2 && $user2->email2!="")
                        $email2=' *'.$user2->email2.'*';
                    ?>
                    <?php if(isset($estudiante['activo']) && $estudiante['activo']!=""){ ?>
                        <tr class="danger">
                    <?php }else{ ?>
                        <tr>
                    <?php } ?>
                        <td><?php echo $estudiante['fecha']; ?></td>
                        <td><?php echo $estudiante['email'].''.$email2; ?></td>
                        <td><a class="btn btn-link" target="_blank" href="<?php echo Yii::app()->urlManager->createUrl('admin/detalleSuscriptores', array('busqueda'=>$estudiante['email'])); ?>"> Abrir</a></td>
                        <td><?php echo $estudiante['estado']; ?></td>
                        <td><?php if($estudiante['plan']){ echo $estudiante['plan']; }else{ ?>
                        <select idev="<?php echo $estudiante['id_happen']; ?>" class="form-control combo" name="id_stype">
                            <option value>Seleccione un Plan</option>
                            <?php foreach ($listaPlanes as $plan){ ?>
                            <option value="<?php echo $plan['id_stype']; ?>"><?php echo $plan['na_stype']; ?></option>
                            <?php } ?>
                            <div id="c<?php echo $estudiante['id_happen']; ?>"></div>
                        </select>
                        <?php } ?></td>
                        <?php if($estudiante['servicio']=="Paypal"){ ?>
                            <td><span class="label label-primary"><?php echo $estudiante['servicio']; ?></span></td>
                        <?php }elseif($estudiante['servicio']=="Stripe"){ ?>
                            <td><span class="label label-warning"><?php echo $estudiante['servicio']; ?></span></td>
                        <?php }else{ ?>
                            <td><span class="label label-default"><?php echo $estudiante['servicio']; ?></span></td>
                        <?php }?>
                    </tr>
                <?php }?>
            <?php }?>
        </tbody>
    </table>
</div>


<script type="text/javascript">
    $(".combo").change(function() 
    {
        var idev=$(this).attr('idev');
        var stype=$(this).val();

        $.ajax({
            'url': '<?php echo Yii::app()->urlManager->createUrl('admin/editarStype'); ?>?idev='+idev+'&stype='+stype,
            'cache': false,
            success: function (data) 
            {
                $("#c"+idev).html(data);
            },
        });

        //alert(str+' '+idev);
    });
</script>