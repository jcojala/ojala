<div class="container" style="padding-top: 15px;">
	<h2><?php echo count($list); ?> Suscriptores Nuevos</h2>
	<div class="list-group">
	<?php foreach ($list as $item) { ?>
	  <a href="#" class="list-group-item">
	    <h4 class="list-group-item-heading"><?php echo $item['name'].' - '.$item['email1']; ?></h4>
	    <p class="list-group-item-text"><?php echo $item['curso']; ?></p>
	  </a>
	<?php } ?>
	</div>
</div>