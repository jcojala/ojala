<?php $meses = array("Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"); ?>

<div class="page-header">
	<h1>Suscriptores de la Semana <?php echo $semana; ?> del Año <?php echo $ano; ?></h1>
</div>

<div class="panel panel-primary">
    <table class="table table-bordered" style="margin: auto;">
        <thead>
          <tr>
            <th>X</th>
            <th>Fecha Suscripcion</th>
            <th>Nombre</th>
            <th>Telefono</th>
            <th>Email</th>
            <th>Intercom</th>
            <th>Ultima Clase Vista</th>
          </tr>
        </thead>
        <tbody>
            <?php $i=0; foreach ($lista as $estudiante) { $i++; ?>
            <tr>
                <td><?php echo $i; ?></td>
                <td><?php echo $estudiante['suscripcion']; ?></td>
                <td><?php echo $estudiante['name']; ?></td>
                <td <?php if(isset($mode)){ ?> style="font-size: 18px; border: 1px solid black;" <?php } ?>><a href="skype:<?php echo $estudiante['phone']; ?>?call"><?php echo $estudiante['phone']; ?></a></td>
                <td><a href="<?php echo Yii::app()->urlManager->createUrl('admin/perfil', array('id'=>$estudiante['id_user'])); ?>"><?php echo $estudiante['email1']; ?></a></td>
                <td>
                    <a target="_blank" href="https://app.intercom.io/apps/wmlo15pq/users/<?php echo OjalaUtils::getIntercomUrl($estudiante['email1']); ?>">Intercom</a>
                </td>
                <?php $valor = User::lastLogin($estudiante['email1']); ?>
                <td><?php echo $valor; ?></td>
            </tr>
            <?php }?>
        </tbody>
    </table>
</div>