<div class="page-header">
    <h1>Resumen de Reporte Administrativo</h1>
    <p class="panel-title" style="font-size: 24px;">
        Detalle de los suscriptores, cancelados y regreso del dinero del 
        <strong><?php echo substr($mes_anterior, 4) ?></strong> al <strong> <?php echo substr($mes_actual, 4); ?></strong>
    </p>
</div>

<a class="btn btn-default" href="<?php echo Yii::app()->urlManager->createUrl('admin/index') ?>">« Regresar al Administrador</a>

<hr>
<div class="panel panel-primary">
    <div class="panel-heading">
        <h3>Resumen</h3>
    </div>
    <div class="panel-body">
        <ul style="margin: 30px 0 0 0; font-size:16px;">
            <li><strong>Revenue:</strong> $<?php echo $revenue; ?> ( <?php echo OjalaUtils::getIncrementPercentaje($revenue, $revenue_anterior); ?>% [$<?php echo $revenue_anterior; ?>] compared to last Month)</li>
            <li><strong>Subscribers :</strong> <?php echo $suscriptores; ?> active subscribers ( <?php echo OjalaUtils::getIncrementPercentaje($suscriptores, $suscriptores_pasados); ?>% [<?php echo $suscriptores_pasados; ?>] compared to last Month)</li>
            <li><strong>New Subscribers :</strong> <?php echo $suscriptores_nuevos; ?> New  ( <?php echo OjalaUtils::getIncrementPercentaje($suscriptores_nuevos, $suscriptores_nuevos_anterior); ?>% [<?php echo $suscriptores_nuevos_anterior ?>] compared to last Month)</li>
            <!-- <li><strong>Subscribers (deudores en 90 dias como cancelados):</strong> <?php echo $suscriptores_90; ?> subscribers [ <?php echo $suscriptores_90_nuevos; ?> New ]  ( +0.00% [00] compared to last Month)</li> -->
            <li><strong>Subscribers (with debtors):</strong> <?php echo $suscriptores_deudores; ?> subscribers [ <?php echo $suscriptores_deudores_nuevos; ?> New ]  ( <?php echo OjalaUtils::getIncrementPercentaje($suscriptores_deudores_nuevos, $suscriptores_deudores_nuevos_anterior); ?>% [<?php echo $suscriptores_deudores_nuevos_anterior ?>] compared to last Month)</li>
            <li><strong>Total # courses:</strong> <?php echo $cursos; ?> courses [ <?php echo $cursos_nuevos; ?> New ] ( <?php echo OjalaUtils::getIncrementPercentaje($cursos_nuevos, $cursos_nuevos_anterior); ?>% [<?php echo $cursos_nuevos_anterior; ?>] compared to last Month)</li>
            <li><strong>Churn rate:</strong> <a href="<?php echo Yii::app()->urlManager->createUrl('admin/churnRate') ?>">Imagen de table</a></li>
            <li><strong>Churn rate (debtors with 90 days as canceled):</strong> <a href="<?php echo Yii::app()->urlManager->createUrl('admin/churnRate', array('deudores'=>'90')) ?>">Table image</a></li>
            <li><strong>Churn rate (with debtors):</strong> <a href="<?php echo Yii::app()->urlManager->createUrl('admin/churnRate', array('deudores'=>'todos')) ?>">Table image</a></li>
            <li><strong>ROI (Return of investment):</strong> <a href="<?php echo Yii::app()->urlManager->createUrl('admin/roi') ?>">Table image</a></li>
        </ul>
    </div>
</div>


<div class="panel-group" id="accordion">
    <div class="panel panel-default">
        <div class="panel-heading">
          <h4 class="panel-title">
            <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne">
              Cursos
            </a>
          </h4>
        </div>
        <div id="collapseOne" class="panel-collapse collapse">
          <div class="panel-body">
            <?php if(count($lista_cursos)>0){ ?>
            <table class="table table-bordered table-striped">
                <thead>
                  <tr>
                    <th>ID</th>
                    <th>Curso</th>
                    <th>Fecha</th>
                  </tr>
                </thead>
                <tbody>
                    <?php foreach($lista_cursos as $item) { ?>
                    <tr>
                        <td><?php echo $item['id_course']; ?></td>
                        <td><?php echo $item['name']; ?></td>
                        <td><?php echo $item['create']; ?></td>
                    </tr>
                    <?php } ?>
                </tbody>
            </table>
            <?php }else{ ?>
                <span class="label label-info">No hay Suscriptores</span>
            <?php } ?>
            <h2>Lista Mes anterior</h2>
            <?php if(count($lista_cursos_anterior)>0){ ?>
            <table class="table table-bordered table-striped">
                <thead>
                  <tr>
                    <th>ID</th>
                    <th>Curso</th>
                    <th>Fecha</th>
                  </tr>
                </thead>
                <tbody>
                    <?php foreach($lista_cursos_anterior as $item) { ?>
                    <tr>
                        <td><?php echo $item['id_course']; ?></td>
                        <td><?php echo $item['name']; ?></td>
                        <td><?php echo $item['create']; ?></td>
                    </tr>
                    <?php } ?>
                </tbody>
            </table>
            <?php }else{ ?>
                <span class="label label-info">No hay Suscriptores</span>
            <?php } ?>
            </div>
        </div>
    </div>
    <div class="panel panel-default">
        <div class="panel-heading">
          <h4 class="panel-title">
            <a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo">
              Pagos
            </a>
          </h4>
        </div>
        <div id="collapseTwo" class="panel-collapse collapse">
          <div class="panel-body">
            <?php if(count($lista_revenue)>0){ ?>
            <table class="table table-bordered table-striped">
                <thead>
                  <tr>
                    <th>ID</th>
                    <th>Estudiante</th>
                    <th>Monto</th>
                    <th>Fecha</th>
                    <th>Tipo</th>
                  </tr>
                </thead>
                <tbody>
                    <?php foreach($lista_revenue as $item) { ?>
                    <tr>
                        <td><?php echo $item['id_pay']; ?></td>
                        <td><?php echo $item['email1'].' - '.$item['name'].' '.$item['lastname']; ?></td>
                        <td><?php echo $item['amount']; ?></td>
                        <td><?php echo $item['date']; ?></td>
                        <td><?php echo $item['na_ptype']; ?></td>
                    </tr>
                    <?php } ?>
                </tbody>
            </table>
            <?php }else{ ?>
                <span class="label label-info">No hay Suscriptores</span>
            <?php } ?>
            </div>
        </div>
    </div>
    <div class="panel panel-default">
        <div class="panel-heading">
          <h4 class="panel-title">
            <a data-toggle="collapse" data-parent="#accordion" href="#collapseThree">
              Suscriptores Nuevos
            </a>
          </h4>
        </div>
        <div id="collapseThree" class="panel-collapse collapse">
          <div class="panel-body">
          
                <?php if(count($lista_suscriptores_nuevos)>0){ ?>
                <table class="table table-bordered table-striped">
                    <thead>
                      <tr>
                        <th>Estudiante</th>
                        <th>Plan</th>
                        <th>Fecha</th>
                        <th>Opcion</th>
                      </tr>
                    </thead>
                    <tbody>
                        <?php foreach($lista_suscriptores_nuevos as $item) { ?>
                        <tr>
                            <td><?php echo $item['email1']; ?></td>
                            <td><?php echo $item['na_stype']; ?></td>
                            <td><?php echo $item['date']; ?></td>
                            <td><a class="btn btn-link btn-xs" href="<?php echo Yii::app()->urlManager->createUrl('admin/estudiante', array('id'=>$item['id'])); ?>">Abrir</a></td>
                        </tr>
                        <?php } ?>
                    </tbody>
                </table>
                <?php }else{ ?>
                    <span class="label label-info">No hay Suscriptores</span>
                <?php } ?>

          </div>
        </div>
    </div>
    <div class="panel panel-default">
        <div class="panel-heading">
          <h4 class="panel-title">
            <a data-toggle="collapse" data-parent="#accordion" href="#collapseFour">
              Suscriptores Nuevos Mes Anterior
            </a>
          </h4>
        </div>
        <div id="collapseFour" class="panel-collapse collapse">
          <div class="panel-body">
          
                <?php if(count($lista_suscriptores_nuevos_anterior)>0){ ?>
                <table class="table table-bordered table-striped">
                    <thead>
                      <tr>
                        <th>Estudiante</th>
                        <th>Plan</th>
                        <th>Fecha</th>
                        <th>Opcion</th>
                      </tr>
                    </thead>
                    <tbody>
                        <?php foreach($lista_suscriptores_nuevos_anterior as $item) { ?>
                        <tr>
                            <td><?php echo $item['email1']; ?></td>
                            <td><?php echo $item['na_stype']; ?></td>
                            <td><?php echo $item['date']; ?></td>
                            <td><a class="btn btn-link btn-xs" href="<?php echo Yii::app()->urlManager->createUrl('admin/estudiante', array('id'=>$item['id'])); ?>">Abrir</a></td>
                        </tr>
                        <?php } ?>
                    </tbody>
                </table>
                <?php }else{ ?>
                    <span class="label label-info">No hay Suscriptores</span>
                <?php } ?>

          </div>
        </div>
    </div>
</div>