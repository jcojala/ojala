<div class="page-header">
    <h1>Top 5 de Cursos</h1>
</div>

<a class="btn btn-default" href="<?php echo Yii::app()->urlManager->createUrl('admin/index') ?>">« Regresar al Administrador</a>

<hr>
<h1>Top 5 de los ultimos cursos empezados</h1>
<div class="panel panel-primary">
    <?php if(count($ultimosEmpezados)>0){ ?>
    <table class="table table-bordered table-striped">
        <thead>
          <tr>
            <th>Curso</th>
            <th>Cantidad de Estudiantes</th>
          </tr>
        </thead>
        <tbody>
            <?php foreach($ultimosEmpezados as $item) { ?>
            <tr>
                <td><?php echo $item['curso']; ?></td>
                <td><?php echo $item['cantidad']; ?></td>
            </tr>
            <?php } ?>
        </tbody>
    </table>
    <?php }else{ ?>
        <span class="label label-info">No hay data</span>
    <?php } ?>
</div>
<hr>
<h1>Top 5 de los ultimos cursos completados</h1>
<div class="panel panel-primary">
    <?php if(count($ultimosCompletados)>0){ ?>
    <table class="table table-bordered table-striped">
        <thead>
          <tr>
            <th>Curso</th>
            <th>Cantidad de Estudiantes</th>
          </tr>
        </thead>
        <tbody>
            <?php foreach($ultimosCompletados as $item) { ?>
            <tr>
                <td><?php echo $item['curso']; ?></td>
                <td><?php echo $item['cantidad']; ?></td>
            </tr>
            <?php } ?>
        </tbody>
    </table>
    <?php }else{ ?>
        <span class="label label-info">No hay data</span>
    <?php } ?>
</div>
<hr>
<h1>Top 5 de los ultimos cursos empezados de usuarios cancelados</h1>
<div class="panel panel-primary">
    <?php if(count($ultimosEmpezadosCancelados)>0){ ?>
    <table class="table table-bordered table-striped">
        <thead>
          <tr>
            <th>Curso</th>
            <th>Cantidad de Estudiantes</th>
          </tr>
        </thead>
        <tbody>
            <?php foreach($ultimosEmpezadosCancelados as $item) { ?>
            <tr>
                <td><?php echo $item['curso']; ?></td>
                <td><?php echo $item['cantidad']; ?></td>
            </tr>
            <?php } ?>
        </tbody>
    </table>
    <?php }else{ ?>
        <span class="label label-info">No hay data</span>
    <?php } ?>
</div>
<hr>
<h1>Top 5 de los ultimos cursos completados de usuarios cancelados</h1>
<div class="panel panel-primary">
    <?php if(count($ultimosCompletadosCancelados)>0){ ?>
    <table class="table table-bordered table-striped">
        <thead>
          <tr>
            <th>Curso</th>
            <th>Cantidad de Estudiantes</th>
          </tr>
        </thead>
        <tbody>
            <?php foreach($ultimosCompletadosCancelados as $item) { ?>
            <tr>
                <td><?php echo $item['curso']; ?></td>
                <td><?php echo $item['cantidad']; ?></td>
            </tr>
            <?php } ?>
        </tbody>
    </table>
    <?php }else{ ?>
        <span class="label label-info">No hay data</span>
    <?php } ?>
</div>