<div class="page-header">
    <h1>Campaña: <?php echo $campana['utm_campaign']; ?></h1>
    <p class="panel-title" style="font-size: 24px;">Detalle de la campaña.</p>
</div>

<a class="btn btn-default" href="<?php echo Yii::app()->urlManager->createUrl('admin/reporteCampanas') ?>">« Regresar al Reporte</a>

<hr>

<div class="panel panel-primary">
    <div class="panel-heading">
        <h3>Resumen de la Campañas</h3>
    </div>
    <div class="panel-body">
        <ul style="margin: 30px 0 0 0; font-size:16px;">
            <li><strong>Nombre de la Campaña: </strong><?php echo $campana['utm_campaign']; ?></li>
            <li><strong>Registros: </strong><?php echo $campana['registros']; ?></li>
            <li><strong>Conversiones: </strong><?php echo $campana['conversiones']; ?></li>
            <li><strong>Uso de Cupones: </strong><?php echo $campana['cupones']; ?></li>
            <li><strong>Porcentaje de Conversión: </strong><span class="label label-info"><?php echo round((($campana['conversiones']*100)/$campana['registros']), 2); ?>%</span></li>
        </ul>
    </div>
</div>

<div class="panel panel-primary">
    <div class="panel-heading">
        <h3>Detalle por Campaña</h3>
    </div>

    <?php if(count($campanas)>0){ ?>
    <table class="table table-bordered table-striped">
        <thead>
          <tr>
            <th>utm_source</th>
            <th>utm_medium</th>
            <th>utm_term</th>
            <th>utm_content</th>
            <th>utm_campaign</th>
            <th>Registros</th>
            <th>Conversiones</th>
            <th>Porcentaje</th>
          </tr>
        </thead>
        <tbody>
            <?php foreach($campanas as $campana) { ?>
            <tr>
                <td><?php echo $campana['utm_source']; ?></td>
                <td><?php echo $campana['utm_medium']; ?></td>
                <td><?php echo $campana['utm_term']; ?></td>
                <td><?php echo $campana['utm_content']; ?></td>
                <td><?php echo $campana['utm_campaign']; ?></td>
                <td><?php echo $campana['registros']; ?></td>
                <td><?php echo $campana['conversiones']; ?></td>
                <td><span class="label label-info"><?php echo round((($campana['conversiones']*100)/$campana['registros']),2); ?>%</span></td>
            </tr>
            <?php } ?>
        </tbody>
    </table>
    <?php }else{ ?>
        <span class="label label-info">No hay detalle de Campañas</span>
    <?php } ?>
</div>
<div class="panel panel-primary">
    <div class="panel-heading">
        <h3>Listado de Conversiones</h3>
    </div>

    <?php if(count($conversiones)>0){ ?>
    <table class="table table-bordered table-striped">
        <thead>
          <tr>
            <th>Fecha</th>
            <th>Usuario</th>
            <th>Email</th>
            <th>Suscripcion</th>
          </tr>
        </thead>
        <tbody>
            <?php foreach($conversiones as $conversion) { ?>
            <tr>
                <td><?php echo $conversion['date']; ?></td>
                <td><?php echo $conversion['name']; ?></td>
                <td><?php echo $conversion['email1']; ?></td>
                <td><?php echo $conversion['na_stype']; ?></td>
            </tr>
            <?php } ?>
        </tbody>
    </table>
    <?php }else{ ?>
        <span class="label label-info">No hay Conversiones</span>
    <?php } ?>
</div>

<div class="panel panel-primary">
    <div class="panel-heading">
        <h3>Comportamiento Mensual</h3>
    </div>
    <table class="table table-bordered" style="margin: auto;">
        <thead>
          <tr>
            <th>Campaña</th>
            <th>Mes1</th>
            <th>Mes2</th>
            <th>Mes3</th>
            <th>Mes4</th>
            <th>Mes5</th>
            <th>Mes6</th>
          </tr>
        </thead>
        <tbody>
            <?php foreach($campanas as $campana) { ?>
            <tr>
                <td><?php echo $campana['utm_content']; ?></td>
                <td><?php echo '0'; ?></td>
                <td><?php echo '0'; ?></td>
                <td><?php echo '0'; ?></td>
                <td><?php echo '0'; ?></td>
                <td><?php echo '0'; ?></td>
                <td><?php echo '0'; ?></td>
            </tr>
            <?php } ?>
        </tbody>
    </table>
</div>