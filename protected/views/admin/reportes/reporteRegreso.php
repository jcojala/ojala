<div class="page-header">
	<h1>Regreso Semanal de Suscriptores</h1>
	<p class="panel-title" style="font-size: 24px;">Suscriptores que regresan a la plataforma por semanas en los utimos 2 meses.</p>
</div>

    <table class="table table-bordered" style="margin: auto;" data-toggle="table">
        <thead>
          <tr>
            <th	style="width:10%">Semana</th>
            <th>Suscriptores</th>
            <th>Semana 1</th>
            <th>Semana 2</th>
            <th>Semana 3</th>
            <th>Semana 4</th>
            <th>Semana 5</th>
            <th>Semana 6</th>
            <th>Semana 7</th>
            <th>Semana 8</th>
            <th>Semana 9</th>
            <th>Semana 10</th>
            <th>Semana 11</th>
            <th>Semana 12</th>
            <th>Semana 13</th>
            <th>Semana 14</th>
            <th>Semana 15</th>
          </tr>
        </thead>
        <tbody>
            <?php foreach ($lista as $key => $value) { ?>
            <tr>
                <td><?php echo 'Semana '.$value['semana'].' de '.$value['mes'].'  '.$value['ano']; ?></td>
                <td><a href="<?php echo Yii::app()->urlManager->createUrl('admin/detalleRegreso', array('semana'=>$value['semana'], 'ano'=>$value['ano'])); ?>"><?php echo $value['suscriptores']; ?></a></td>
                <td><?php echo $value['semana1']; ?></td>
                <td><?php echo $value['semana2']; ?></td>
                <td><?php echo $value['semana3']; ?></td>
                <td><?php echo $value['semana4']; ?></td>
                <td><?php echo $value['semana5']; ?></td>
                <td><?php echo $value['semana6']; ?></td>
                <td><?php echo $value['semana7']; ?></td>
                <td><?php echo $value['semana8']; ?></td>
                <td><?php echo $value['semana9']; ?></td>
                <td><?php echo $value['semana10']; ?></td>
                <td><?php echo $value['semana11']; ?></td>
                <td><?php echo $value['semana12']; ?></td>
                <td><?php echo $value['semana13']; ?></td>
                <td><?php echo $value['semana14']; ?></td>
                <td><?php echo $value['semana15']; ?></td>
            </tr>
            <?php }?>
        </tbody>
    </table>
<hr>
    <table class="table table-bordered" style="margin: auto;" data-toggle="table">
        <thead>
          <tr>
            <th style="width:10%">Semana</th>
            <th>Suscriptores</th>
            <th>Semana 1</th>
            <th>Semana 2</th>
            <th>Semana 3</th>
            <th>Semana 4</th>
            <th>Semana 5</th>
            <th>Semana 6</th>
            <th>Semana 7</th>
            <th>Semana 8</th>
            <th>Semana 9</th>
            <th>Semana 10</th>
            <th>Semana 11</th>
            <th>Semana 12</th>
            <th>Semana 13</th>
            <th>Semana 14</th>
            <th>Semana 15</th>
          </tr>
        </thead>
        <tbody>
            <?php foreach ($lista as $key => $value) { ?>
            <tr>
                <td><?php echo 'Semana '.$value['semana'].' de '.$value['mes'].' '.$value['ano']; ?></td>
                <td><?php echo $value['suscriptores']; ?></td>
                <td <?php if(round(($value['semana1']*100)/$value['suscriptores'], 2)!='0'){ echo 'style="background-color: rgba(255, 0, 0, '.(round(($value['semana1']*100)/$value['suscriptores'], 2)/100).')"'; } ?> ><?php echo round(($value['semana1']*100)/$value['suscriptores'], 2).' %'; ?></td>
                <td <?php if(round(($value['semana2']*100)/$value['suscriptores'], 2)!='0'){ echo 'style="background-color: rgba(255, 0, 0, '.(round(($value['semana2']*100)/$value['suscriptores'], 2)/100).')"'; } ?> ><?php echo round(($value['semana2']*100)/$value['suscriptores'], 2).' %'; ?></td>
                <td <?php if(round(($value['semana3']*100)/$value['suscriptores'], 2)!='0'){ echo 'style="background-color: rgba(255, 0, 0, '.(round(($value['semana3']*100)/$value['suscriptores'], 2)/100).')"'; } ?> ><?php echo round(($value['semana3']*100)/$value['suscriptores'], 2).' %'; ?></td>
                <td <?php if(round(($value['semana4']*100)/$value['suscriptores'], 2)!='0'){ echo 'style="background-color: rgba(255, 0, 0, '.(round(($value['semana4']*100)/$value['suscriptores'], 2)/100).')"'; } ?> ><?php echo round(($value['semana4']*100)/$value['suscriptores'], 2).' %'; ?></td>
                <td <?php if(round(($value['semana5']*100)/$value['suscriptores'], 2)!='0'){ echo 'style="background-color: rgba(255, 0, 0, '.(round(($value['semana5']*100)/$value['suscriptores'], 2)/100).')"'; } ?> ><?php echo round(($value['semana5']*100)/$value['suscriptores'], 2).' %'; ?></td>
                <td <?php if(round(($value['semana6']*100)/$value['suscriptores'], 2)!='0'){ echo 'style="background-color: rgba(255, 0, 0, '.(round(($value['semana6']*100)/$value['suscriptores'], 2)/100).')"'; } ?> ><?php echo round(($value['semana6']*100)/$value['suscriptores'], 2).' %'; ?></td>
                <td <?php if(round(($value['semana7']*100)/$value['suscriptores'], 2)!='0'){ echo 'style="background-color: rgba(255, 0, 0, '.(round(($value['semana7']*100)/$value['suscriptores'], 2)/100).')"'; } ?> ><?php echo round(($value['semana7']*100)/$value['suscriptores'], 2).' %'; ?></td>
                <td <?php if(round(($value['semana8']*100)/$value['suscriptores'], 2)!='0'){ echo 'style="background-color: rgba(255, 0, 0, '.(round(($value['semana8']*100)/$value['suscriptores'], 2)/100).')"'; } ?> ><?php echo round(($value['semana8']*100)/$value['suscriptores'], 2).' %'; ?></td>
                <td <?php if(round(($value['semana9']*100)/$value['suscriptores'], 2)!='0'){ echo 'style="background-color: rgba(255, 0, 0, '.(round(($value['semana9']*100)/$value['suscriptores'], 2)/100).')"'; } ?> ><?php echo round(($value['semana9']*100)/$value['suscriptores'], 2).' %'; ?></td>
                <td <?php if(round(($value['semana10']*100)/$value['suscriptores'], 2)!='0'){ echo 'style="background-color: rgba(255, 0, 0, '.(round(($value['semana10']*100)/$value['suscriptores'], 2)/100).')"'; } ?> ><?php echo round(($value['semana10']*100)/$value['suscriptores'], 2).' %'; ?></td>
                <td <?php if(round(($value['semana11']*100)/$value['suscriptores'], 2)!='0'){ echo 'style="background-color: rgba(255, 0, 0, '.(round(($value['semana11']*100)/$value['suscriptores'], 2)/100).')"'; } ?> ><?php echo round(($value['semana11']*100)/$value['suscriptores'], 2).' %'; ?></td>
                <td <?php if(round(($value['semana12']*100)/$value['suscriptores'], 2)!='0'){ echo 'style="background-color: rgba(255, 0, 0, '.(round(($value['semana12']*100)/$value['suscriptores'], 2)/100).')"'; } ?> ><?php echo round(($value['semana12']*100)/$value['suscriptores'], 2).' %'; ?></td>
                <td <?php if(round(($value['semana13']*100)/$value['suscriptores'], 2)!='0'){ echo 'style="background-color: rgba(255, 0, 0, '.(round(($value['semana13']*100)/$value['suscriptores'], 2)/100).')"'; } ?> ><?php echo round(($value['semana13']*100)/$value['suscriptores'], 2).' %'; ?></td>
                <td <?php if(round(($value['semana14']*100)/$value['suscriptores'], 2)!='0'){ echo 'style="background-color: rgba(255, 0, 0, '.(round(($value['semana14']*100)/$value['suscriptores'], 2)/100).')"'; } ?> ><?php echo round(($value['semana14']*100)/$value['suscriptores'], 2).' %'; ?></td>
                <td <?php if(round(($value['semana15']*100)/$value['suscriptores'], 2)!='0'){ echo 'style="background-color: rgba(255, 0, 0, '.(round(($value['semana15']*100)/$value['suscriptores'], 2)/100).')"'; } ?> ><?php echo round(($value['semana15']*100)/$value['suscriptores'], 2).' %'; ?></td>
            </tr>
            <?php }?>
        </tbody>
    </table>
<hr>