<div class="page-header">
	<h1>Listado de Cursos</h1>
	<p class="panel-title" style="font-size: 24px;">Listado por estatus de publicación.</p>
</div>

<ol class="breadcrumb" style="margin-top:-20px; margin-bottom:30px;">
  <li><a href="<?php echo Yii::app()->urlManager->createUrl('admin/index'); ?>">Home admin</a></li>
  <li class="active">Cursos</li>
</ol>


<div class="row">
	<div class="content-fluid">
		<div class="col-md-3">
	<div class="btn-group">
		  <button type="button" class="btn btn-primary btn-lg dropdown-toggle" data-toggle="dropdown">
    		Filtrar cursos por ...<span class="caret" style="margin-left:10px;"></span>
 	 		</button>
  		<ul class="dropdown-menu" role="menu">
   			<li><a href="<?php echo Yii::app()->urlManager->createUrl('admin/cursos', array('state'=>1, 'publish'=>1)); ?>">Publicados</a></li>
    		<li><a href="<?php echo Yii::app()->urlManager->createUrl('admin/cursos', array('state'=>1, 'publish'=>0)); ?>">Programados</a></li>
    		<li><a href="<?php echo Yii::app()->urlManager->createUrl('admin/cursos', array('state'=>2)); ?>">Borradores</a></li>
    		<li><a href="<?php echo Yii::app()->urlManager->createUrl('admin/cursos', array('state'=>3)); ?>">Ocultos</a></li>
    		<li class="divider"></li>
    		<li><a href="<?php echo Yii::app()->urlManager->createUrl('admin/cursos'); ?>">Todos</a></li>
  		</ul>
		</div>
		</div>
		<div class="col-md-7">

			<form role="form" class="">
        <div class="form-group col-md-8">
          <input type="text" class="form-control input-lg " placeholder="Nombre del curso" name="dato" value="<?php if(isset($dato)){echo $dato;} ?>">
        </div>
        <button type="submit" class="col-md-3 btn btn-primary btn-lg">Buscar curso</button>
      </form>


		</div>
		<div class="col-md-2">
	    <a class="btn btn-success btn-lg" href="<?php echo Yii::app()->urlManager->createUrl('admin/curso'); ?>">Nuevo Curso</a>
		</div>
	</div>
</div>


<div class="row">
	<div class="col-md-12">

<div class="page-header">
	<h1><?php echo $titulo; ?></h1>
</div>
		<table class="table table-bordered" style="margin: 2px 0 20px 20px; width: 98%">
			<thead>
			  <tr>
			  	<?php if(isset($dato)){ ?><th class="col-md-1">Estado</th><?php } ?>
			  	<!--<th class="col-md-2">Categoría</th>-->
				<th class="col-md-5">Curso</th>
				<th class="col-md-1">Cód. Wistia</th>
			  	<th class="col-md-2">Duración</th>
			  	<th class="col-md-1">Creación</th>
			  	<th class="col-md-1">Publicado</th>
			  	<th class="col-md-2">Opción</th>
			  </tr>
			</thead>
			
			<tbody>
				<?php foreach($list1 as $curso) { ?>
				<tr>
			  	<?php if(isset($dato)){ ?>
						<?php if($curso['status']=="Publicado"){ ?>
							<td><span class="label label-success"><?php echo $curso['status']; ?></span></td>
						<?php }elseif($curso['status']=="Borrador"){ ?>
							<td><span class="label label-primary"><?php echo $curso['status']; ?></span></td>
						<?php }elseif($curso['status']=="Oculto"){ ?>
							<td><span class="label label-info"><?php echo $curso['status']; ?></span></td>
						<?php } ?>
			  	<?php } ?>
			  	<!--<td><?php //echo $curso['cat']; ?></td>-->
					<td><?php echo $curso['name']; ?></td>
					<td><?php echo $curso['wistia']; ?></td>
					<td><?php echo Dates::secondsToTime2($curso['duration']); ?></td>
					<td><?php echo OjalaUtils::convertDateFromDb($curso['create'], false); ?></td>
					<td><?php echo empty($curso['publish']) ? '' : OjalaUtils::convertDateFromDb($curso['publish'], false); ?></td>
					<td>
						<div class="btn-group">
						<a type="button" class="btn btn-primary btn-xs" href="<?php echo Yii::app()->urlManager->createUrl('site/curso', array('slug'=>$curso['slug'], 'cat'=>$curso['cat'])); ?>">Ver curso</a>
						<a type="button" class="btn btn-success btn-xs" href="https://oja.wistia.com/projects/<?php echo $curso['wistia']; ?>">Wistia</a>
						<a type="button" class="btn btn-warning btn-xs" href="<?php echo Yii::app()->urlManager->createUrl('admin/editarCurso', array('id'=>$curso['id'])); ?>">Editar</a>
						</div>
					</td>
				</tr>
				<?php } ?>
			</tbody>
		</table>

</div>

<script type="text/javascript">
	$('.tags').tooltip();
</script>