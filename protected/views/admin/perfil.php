<div class="page-header">
    <h1>Perfil de: <b><?php echo $user->name.' '.$user->lastname; ?></b></h1>
</div>
<div class="row">
    <div class="col-md-6">
        <a class="btn btn-default" href="<?php echo Yii::app()->urlManager->createUrl('admin/seguimiento') ?>">« Regresar a la Tabla</a>
    </div>
</div>
<br>
<div class="row">
    <div class="col-md-12 page-layout">
                <div class="row">
                    <div class="col-md-12">
                        <div class="row">
                            <div class="col-md-7">
                                <div class="row">
                                    <div class="col-md-4">
                                        <img src="http://placehold.it/210x280&text=Foto de Perfil">
                                    </div>
                                    <div class="col-md-8">
                                        <ul style="margin: 30px 0 0 0; font-size:16px;">
                                            <p><strong>Nombre: </strong> <?php echo $user->name.' '.$user->lastname; ?></p>
                                            <p><strong>Email: </strong> <?php echo $user->email1; ?></p>
                                            <p><strong>Fecha de registro: </strong><?php echo $user->create; ?></p>
                                            <p><strong>Último Login: </strong><?php echo $user->last_sign; ?></p>
                                            <?php if($user->email2) :?>
                                            <p><strong>Email Sec: </strong> <?php echo $user->email2; ?></p>
                                            <?php endif; ?>
                                            <p><strong>País: </strong> <?php if(isset($user->region)){ echo $user->region->country->name.' - '.$user->region->name; }else{ echo OjalaUtils::getIntercomPais($user->email1); } ?>
                                            <p><strong>Teléfono: </strong> <a href="skype:<?php echo $user->phone; ?>?call"><?php echo $user->phone; ?></a> 
                                                <form class="form-inline" method="get" action="<?php echo Yii::app()->urlManager->createUrl('admin/editarTelefono'); ?>">
                                                    <input type="hidden" name="id" value="<?php echo $user->id_user; ?>"/>
                                                    <input class="form-control bfh-phone" type="text" name="phone" style="width: 70%;" data-format="+dd (ddd) ddd-dddd" value="<?php echo $user->phone; ?>"/>
                                                    <input class="btn btn-warning" type="submit" value="Editar"/>
                                                </form>
                                            </p>
                                            <p><strong>Cursos Vistos: </strong> <?php echo count($cursos); ?> <strong>Clases Vistas: </strong> </p>
                                            <p><strong>Tecnologías preferidas: </strong> </p>
                                        </ul>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <hr>
                                        <legend><b>Estado de la Suscripción</b></legend>
                                        <?php if($suscripcion){ ?>
                                        <ul style="font-size:16px;">
                                            <p><strong>Estado:</strong>
                                                <?php if($suscripcion['estado']=='1'){ ?>
                                                    <span class="label label-success">Activo</span>
                                                <?php }elseif($suscripcion['estado']=='2'){ ?>
                                                    <span class="label label-warning">Deudor</span>
                                                <?php }else{ ?>
                                                    <span class="label label-danger">Cancelado</span>
                                                <?php } ?>
                                            </p>
                                            <p><strong>Fecha de Suscripcion: </strong><?php echo $suscripcion['date']; ?></p>
                                            <p><strong>Tipo Suscripcion: </strong><?php echo $suscripcion['na_stype']; ?></p>
                                        </ul>
                                        <?php }else{ ?>
                                            <span class="label label-info">No tiene ninguna Suscripcion Recurrente</span>
                                        <?php } ?>
                                        <br>
                                        <br>
                                        <legend><b>Ultimo Curso y Ultimas Clases Vista</b></legend>
                                        <div class="alert alert-success" role="alert">
                                            <?php if(isset($cursos[0])){ ?>
                                                <strong><?php echo $cursos[0]['name']; ?></strong>
                                                <p><?php echo $cursos[0]['current']; ?></p>
                                            <?php } ?>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-5">
                                <legend>Crea un Evento</legend>
                                <form method="get" action="<?php echo Yii::app()->urlManager->createUrl('admin/guardarContacto'); ?>">
                                    <input type="hidden" name="id_user" value="<?php echo $user->id_user; ?>"/>
                                    <?php
                                        $this->widget('zii.widgets.jui.CJuiDatePicker',array(
                                            'name'=>'date',
                                            'language'=>'es',
                                            'options'=>array(        
                                                'showButtonPanel'=>true,
                                                'dateFormat'=>'dd/mm/yy'
                                            ),
                                            'htmlOptions'=>array(
                                                'id'=>'datetimepicker1',
                                                'placeholder'=>'Fecha de publicación',
                                                'class'=>'form-control'
                                            ),
                                        ));
                                    ?>
                                    <br>
                                    <textarea class="form-control" name="text" rows="2"></textarea>
                                    <br>
                                    <input type="submit" class="btn btn-xs btn-success" value="Programar Evento"/>
                                </form>
                                <hr>
                                <legend>Proximos Contactos</legend>
                                <div class="bs-example">
                                    <?php foreach ($listProximos as $item) {
                                        $color="success";
                                        if($item['tiempo']!="OK"){
                                            $color="danger";
                                        }
                                     ?>

                                    <div class="alert alert-<?php echo $color; ?>" role="alert">
                                        <strong><?php echo $item['text']; ?></strong> <?php echo OjalaUtils::convertDateFromDB($item['date_contact']); ?>
                                        <br>
                                        <form method="get" action="<?php echo Yii::app()->urlManager->createUrl('admin/cambiarEstadoContacto'); ?>">
                                          <textarea class="form-control" rows="1" name="text" placeholder="Detalle (opcional)"></textarea>
                                          <br>
                                          <input type="hidden" name="id_user" value="<?php echo $user->id_user; ?>"/>
                                          <input type="hidden" name="id_contact" value="<?php echo $item['id_contact']; ?>"/>
                                          <input type="hidden" name="id_state" value="2"/>
                                          <input type="submit" class="btn btn-xs btn-success" value="Hecho"/>
                                        </form>
                                        <form method="get" action="<?php echo Yii::app()->urlManager->createUrl('admin/cambiarEstadoContacto'); ?>">
                                          <input type="hidden" name="id_user" value="<?php echo $user->id_user; ?>"/>
                                          <input type="hidden" name="id_contact" value="<?php echo $item['id_contact']; ?>"/>
                                          <input type="hidden" name="id_state" value="3"/>
                                          <input type="submit" class="btn btn-xs btn-danger" value="Cancelar"/>
                                        </form>
                                    </div>
                                    <?php } ?>
                                </div>
                                <legend>Ultimos Eventos</legend>
                                <div class="bs-example">
                                    <?php foreach ($listPasados as $item) { ?>
                                    <div class="alert alert-warning" role="alert">
                                        <strong><?php echo $item['text']; ?></strong> <?php echo OjalaUtils::convertDateFromDB($item['date_contact']); ?>
                                    </div>
                                    <?php } ?>
                                </div>
                            </div>
                        </div>
                        <hr>
                        <div class="row">
                            <div class="col-md-12">

                                <legend>Diplomados Inscritos</legend>
                                <?php if(count($diplomados)>0){ ?>
                                <table class="table table-bordered table-striped">
                                    <thead>
                                      <tr>
                                        <th>Diplomado</th>
                                        <th>Nivel Actual</th>
                                      </tr>
                                    </thead>
                                    <tbody>
                                        <?php foreach($diplomados as $diplomado) { ?>
                                        <tr>
                                            <td><?php echo $diplomado['nb_group']; ?></td>
                                            <td><?php echo $diplomado['nivel']; ?></td>
                                        </tr>
                                        <?php } ?>
                                    </tbody>
                                </table>
                                <?php }else{ ?>
                                    <span class="label label-info">No se ha inscrito en ninun diplomado</span>
                                <?php } ?>

                                <hr>

                                <legend>Otros Cursos Realizados</legend>
                                <?php if(count($cursos)>0){ ?>
                                <table class="table table-bordered table-striped">
                                    <thead>
                                      <tr>
                                        <th>Progreso</th>
                                        <th>Curso</th>
                                      </tr>
                                    </thead>
                                    <tbody>
                                        <?php foreach($cursos as $curso) { ?>
                                        <tr>
                                            <td><span class="label label-<?php if($curso['progress']<90){ echo 'info'; }else{ echo 'success'; }?>"><?php if($curso['progress']==""){ echo '0'; }else{ echo $curso['progress']; } ?> %</span></td>
                                            <td><strong>Título:</strong> <?php echo $curso['name'].' </br> <strong>Clase:</strong> '.$curso['current']; ?></td>
                                        </tr>
                                        <?php } ?>
                                    </tbody>
                                </table>
                                <?php }else{ ?>
                                    <span class="label label-info">No ha visto ningun curso</span>
                                <?php } ?>
                            </div>
                        </div>
                    </div>
        </div>
    </div>
</div>
<hr>
<?php 
    Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl . '/js/admin/bootstrap-formhelpers-phone.js', CClientScript::POS_HEAD);
?>