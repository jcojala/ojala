<div class="page-header">
	<h1>Agregar curso al diplomado</h1>
	<p><?php echo $group->nb_group; ?></p>
</div>

<div class="row">
	<div class="col-md-12">
		<a class="btn btn-default" href="<?php echo Yii::app()->urlManager->createUrl('admin/diplomado', array('id'=>$group->id_group)) ?>">« Regresar al Diplomado</a>
	</div>
</div>

<div class="row">
	<div class="container user">

		<div class="col-md-12 page-layout">
			<div class="div">

				<?php $form=$this->beginWidget('CActiveForm', array(
					'id'=>'register-form',
					'htmlOptions' => array('class'=>'form-horizontal'),
				)); ?>


					<?php if($groupc->id_gcourse!=""){ ?>
					<input type="hidden" name="id_gcourse" value="<?php echo $groupc->id_gcourse; ?>" />
					<?php } ?>

					<input type="hidden" name="id_group" value="<?php echo $group->id_group; ?>">
					<div class="form-group">
						<label class="col-lg-3" for="course_name">Seleccione el curso:</label>
						<div class="col-lg-7">
							<?php echo $form->dropDownList($groupc, 'id_course', CHtml::listData($lista, 'id_course', 'name'), array('class'=>'combobox')); ?>
							<?php echo $form->error($groupc,'nb_group', array('style'=>'color: red;')); ?>
						</div>
					</div>

					<div class="form-group">
						<label class="col-lg-3" for="course_name">Nivel:</label>
						<div class="col-lg-7">
							<?php echo $form->textField($groupc, 'nivel', array('size'=>'500', 'class'=>'form-control')); ?>
							<?php echo $form->error($groupc, 'nivel', array('style'=>'color: red;')); ?>
						</div>
					</div>

					<input class="btn btn-success btn-lg col-lg-offset-3" type="submit" value="Agregar Curso">

				<?php $this->endWidget(); ?>

			</div>
		</div>
	</div>
</div>