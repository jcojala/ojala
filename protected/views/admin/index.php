<div class="page-header">
	<h1>Administrador Oja.la</h1>
	<p>Panel de administración de Oja.la</p>
</div>

<ol class="breadcrumb" style="margin-top:-40px; margin-bottom:30px;">
  <li class="active">Home admin</li>
</ol>

<div class="admin">
		<h2>Estudiantes</h2>
		<div class="btn-group">
			<a class="btn btn-info btn-lg" href="<?php echo Yii::app()->urlManager->createUrl('admin/buscadorEstudiantes'); ?>">
				<i class="glyphicon glyphicon-search"></i> Buscar
			</a>
			<?php if(Yii::app()->user->utype==='Administrador'):?>
				<a class="btn btn-info btn-lg" href="<?php echo Yii::app()->urlManager->createUrl('admin/seguimiento'); ?>">
					<i class="glyphicon glyphicon-earphone"></i> Seguimiento
				</a>
				<a class="btn btn-info btn-lg" href="<?php echo Yii::app()->urlManager->createUrl('admin/suscriptores'); ?>">
					<i class="glyphicon glyphicon-list-alt"></i> Nuevos suscriptores
				</a>
			<?php endif;?>
		</div>
	
		<hr>

		<h2>Cursos</h2>
		<div class="btn-group">
			<a href="<?php echo Yii::app()->urlManager->createUrl('admin/cursos');	?>" class="btn btn-info btn-lg">
				Todos los cursos: <span class="label label-danger"><?php echo $activeCourses + $hiddenCourses + $draftCourses; ?></span>
			</a>
			<a href="<?php echo Yii::app()->urlManager->createUrl('admin/cursos', array('state'=>1));	?>" class="btn btn-info btn-lg">
				Publicados: <span class="label label-danger"><?php echo $activeCourses ?></span>
			</a>
			<a href="<?php echo Yii::app()->urlManager->createUrl('admin/cursos', array('state'=>2));	?>" class="btn btn-info btn-lg">
				En Borrador: <span class="label label-danger"><?php echo $draftCourses; ?></span>
			</a>
			<a href="<?php echo Yii::app()->urlManager->createUrl('admin/cursos', array('state'=>3));	?>" class="btn btn-info btn-lg">
				Ocultos: <span class="label label-danger"><?php echo $hiddenCourses; ?></span>
			</a>
			<a class="btn btn-success btn-lg" href="<?php echo Yii::app()->urlManager->createUrl('admin/curso'); ?>">
				Nuevo curso
			</a>

		</div>
	
		<hr>
                
        <h2>Propuestas</h2>
		<div class="btn-group">
			<a href="<?php echo Yii::app()->urlManager->createUrl('admin/propuestas',array('filtro'=>0));	?>" class="btn btn-info btn-lg">Todas las propuestas: <span class="label label-danger"><?php echo $proposalUnpublished + $proposalPublished ?></span></a>
            <a href="<?php echo Yii::app()->urlManager->createUrl('admin/propuestas',array('filtro'=>1));	?>" class="btn btn-info btn-lg">Publicadas <span class="label label-danger"><?php echo $proposalPublished ?></span></a>
            <a href="<?php echo Yii::app()->urlManager->createUrl('admin/propuestas',array('filtro'=>2));	?>" class="btn btn-info btn-lg">No Publicadas <span class="label label-danger"><?php echo $proposalUnpublished ?></span></a>
            <a class="btn btn-success btn-lg" href="<?php echo Yii::app()->urlManager->createUrl('admin/nuevaPropuesta'); ?>">Nueva propuesta</a>
		</div>
                
        <hr>

		<h2>Diplomados</h2>
		<div class="btn-group">
			<a href="<?php echo Yii::app()->urlManager->createUrl('admin/diplomados');	?>" class="btn btn-info btn-lg">Todos los diplomados: <span class="label label-danger"><?php echo $diplomados; ?></span></a>
			<a class="btn btn-success btn-lg" href="<?php echo Yii::app()->urlManager->createUrl('admin/crearDiplomado'); ?>">Nuevo diplomado</a>
			<a href="#" class="btn btn-default btn-lg">Estudiantes en los diplomados: <span class="label label-default"><?php echo $suscritos_diplomados; ?></span></a>
		</div>

		<hr>

		<?php if(isset(Yii::app()->user->utype) && (Yii::app()->user->utype==='Administrador')){ ?>

		<div class="page-header"><h1>Reportes de Administración</h1></div>

		<h2>Preguntas para Onboarding:</h2>
		<div class="btn-group">
			<a href="<?php echo Yii::app()->urlManager->createUrl('admin/campanas');	?>" class="btn btn-info btn-lg">Todas las campañas: <span class="label label-danger"><?php echo $onboarding ?></span></a>
			<a class="btn btn-success btn-lg" href="<?php echo Yii::app()->urlManager->createUrl('admin/crearCampana'); ?>">Nueva campaña</a>
		</div>
	
		<hr>

		<h2>Reportes:</h2>
		<div class="btn-group">
			<a class="btn btn-info btn-lg" href="<?php echo Yii::app()->urlManager->createUrl('admin/resumen'); ?>" class="btn btn-info btn-lg">Resumen mensual</a>
			<a class="btn btn-info btn-lg" href="<?php echo Yii::app()->urlManager->createUrl('admin/cursosTrends'); ?>">Trends Cursos</a>
			<a class="btn btn-info btn-lg" href="<?php echo Yii::app()->urlManager->createUrl('admin/landings'); ?>">Landings</a>
			<a class="btn btn-info btn-lg" href="<?php echo Yii::app()->urlManager->createUrl('admin/reporteSuscriptores'); ?>">Suscriptores Semanal</a>
		</div>
		<br><br>
		<div class="btn-group">
			<a class="btn btn-info btn-lg" href="<?php echo Yii::app()->urlManager->createUrl('admin/topClases'); ?>">Top 10 Clases</a>
			<a class="btn btn-info btn-lg" href="<?php echo Yii::app()->urlManager->createUrl('admin/topLogin'); ?>">Top 10 Acceso</a>
			<a class="btn btn-info btn-lg" href="<?php echo Yii::app()->urlManager->createUrl('admin/topFrecuencia'); ?>">Top 10 Frecuencia</a>
		</div>
	</div>
	
	<hr>

	<h2>Reportes de Churn Rate:</h2>
	<div class="btn-group">
		<a class="btn btn-info btn-lg" href="<?php echo Yii::app()->urlManager->createUrl('admin/reportes/churnRate'); ?>">Churn Rate mensual</a>
		<a class="btn btn-info btn-lg" href="<?php echo Yii::app()->urlManager->createUrl('admin/reportes/churnDeudores'); ?>">Churn Rate + Deudores</a>
		<a class="btn btn-info btn-lg" href="<?php echo Yii::app()->urlManager->createUrl('admin/reportes/churnDeudores90'); ?>">Churn Rate + Deudores 90 Dias</a>
	</div>

	<hr>

	<h2>Administración:</h2>
	<div class="btn-group">
		<a class="btn btn-info btn-lg" href="<?php echo Yii::app()->urlManager->createUrl('admin/newsletters'); ?>">Newsletters</a>
		<a class="btn btn-info btn-lg" href="<?php echo Yii::app()->urlManager->createUrl('admin/planes'); ?>">Planes y Botones</a>
		<a class="btn btn-info btn-lg" href="<?php echo Yii::app()->urlManager->createUrl('admin/usuarios'); ?>">Usuarios y Roles</a>
	</div>

	<div class="spacer"></div>
	<?php } ?>
</div>
