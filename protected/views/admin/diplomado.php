<div class="page-header">
	<h1><?php echo $group->nb_group; ?></h1>
	<p class="panel-title" style="font-size: 24px;">Editar Diplomado. </p>
</div>

<div class="row">
	<div class="col-md-12">
		<a class="btn btn-default" href="<?php echo Yii::app()->urlManager->createUrl('admin/diplomados') ?>">« Listado de Diplomados</a>
		<a class="btn btn-success" href="<?php echo Yii::app()->urlManager->createUrl('admin/agregarCursoDiplomado', array('id'=>$group->id_group)) ?>">Agregar Curso</a>
		<a class="btn btn-warning" href="<?php echo Yii::app()->urlManager->createUrl('admin/crearDiplomado', array('id'=>$group->id_group)) ?>">Editar Datos</a>
	</div>
</div>

<h2>Listado de Cursos del Diplomado</h2>

<br>

<div class="row">
	<div class="col-md-12">
        <table class="table table-bordered table-hover">
            <thead>
              <tr>
            	<th>Nivel</th>
            	<th>Titulo</th>
              	<th style="width: 20%;">Opciones</th>
              </tr>
            </thead>
			<tbody>
				<?php foreach ($list as $curso) { ?>
					<tr>
						<td><?php echo $curso['nivel']; ?></td>
						<td><?php echo $curso['name']; ?></td>
						<td style="text-align: center;">
							<a type="button" class="btn btn-warning btn-xs" href="<?php echo Yii::app()->urlManager->createUrl('admin/agregarCursoDiplomado', array('id'=>$group->id_group, 'gc'=>$curso['id_gcourse'])) ?>">Editar</a>
							<a type="button" class="btn btn-danger btn-xs"  href="<?php echo Yii::app()->urlManager->createUrl('admin/eliminarCursoDiplomado', array('id'=>$group->id_group, 'gc'=>$curso['id_gcourse'])) ?>">Eliminar</a>
						</td>
					</tr>
				<?php } ?>
			</tbody>
		</table>
	</div>
</div>