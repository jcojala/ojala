<div class="page-header">
	<h1>Crea un nuevo curso</h1>
	<p class="panel-title" style="font-size: 24px;">Completa la información de éste formulario para la creación de un nuevo curso. </p>
</div>

<ol class="breadcrumb" style="margin-top:-20px; margin-bottom:30px;">
  <li><a href="<?php echo Yii::app()->urlManager->createUrl('admin/index'); ?>">Home admin</a></li>
  <li><a href="<?php echo Yii::app()->urlManager->createUrl('admin/cursos'); ?>">Cursos</a></li>
  <li class="active">Nuevo Curso</li>
</ol>


<br>

<div class="row">
	<div class="col-md-12 page-layout">

		<?php 
			$form=$this->beginWidget('CActiveForm', array(
				'id'=>'register-form',
				'htmlOptions' => array(
					'class'=>'form-horizontal',
				)
			)); 
		?>

			<div class="form-group">
				<label class="col-lg-3" for="course_name">Nombre del Nuevo Curso:</label>
				<div class="col-lg-7">
					<?php echo $form->textField($course,'name', array('size'=>'500', 'class'=>'form-control')); ?>
					<?php echo $form->error($course,'name', array('style'=>'color: red;')); ?>
				</div>
			</div>

			<div class="form-group">
				<label class="col-lg-3">ID del Curso en Wistia:</label>
				<div class="col-lg-7">
					<?php echo $form->textField($course,'wistia_uid', array('size'=>'100', 'class'=>'form-control')); ?>
					<?php echo $form->error($course,'wistia_uid', array('style'=>'color: red;')); ?>
				</div>
			</div>

			<div class="form-group">
				<label class="col-lg-3" for="user_last_name">Costo del Curso:</label>
				<div class="col-lg-7">
					<?php echo $form->textField($course,'cost', array('size'=>'10', 'class'=>'form-control')); ?>
					<?php echo $form->error($course,'cost', array('style'=>'color: red;')); ?>
				</div>
			</div>					

			<div class="form-group">
				<label class="col-lg-3">¿Cuáles son los requisitos?:</label>
				<div class="col-lg-9">
					<?php echo $form->textarea($course,'requirements', array('rows'=>'4', 'class'=>'form-control')); ?>
					<?php echo $form->error($course,'requirements', array('style'=>'color: red;')); ?>
				</div>
			</div>

			<div class="form-group">
				<label class="col-lg-3">¿Qué voy a aprender?:</label>
				<div class="col-lg-9">
					<?php echo $form->textarea($course,'learn', array('rows'=>'4', 'class'=>'form-control')); ?>
					<?php echo $form->error($course,'learn', array('style'=>'color: red;')); ?>
				</div>
			</div>

			<div class="form-group">
				<label class="col-lg-3">¿A quién va dirigido?:</label>
				<div class="col-lg-9">
					<?php echo $form->textarea($course,'directed', array('rows'=>'4', 'class'=>'form-control')); ?>
					<?php echo $form->error($course,'directed', array('style'=>'color: red;')); ?>
				</div>
			</div>

	        <div class="form-group">
				<label class="col-sm-3 control-label" for="id_cathegory">Categoría</label>
				<div class="col-lg-9">
		            	<select class="combobox form-control" name="id_cathegory">
						<option value>Seleccione una Categoría</option>
						<?php foreach ($listCat as $cathegory){ ?>
							<?php if(isset($course) && $course->id_cathegory==$cathegory['id_cathegory']){ ?>
								<option selected value="<?php echo $cathegory['id_cathegory']; ?>"><?php echo $cathegory['na_cathegory']; ?></option>
							<?php }else{ ?>
								<option value="<?php echo $cathegory['id_cathegory']; ?>"><?php echo $cathegory['na_cathegory']; ?></option>
							<?php } ?>
						<?php } ?>
		            	</select>
				</div>
	        </div>  

	        <div class="form-group">
				<label class="col-sm-3 control-label" for="id_level">Nivel</label>
				<div class="col-lg-9">
		            	<select class="combobox form-control" name="id_level">
						<option value>Seleccione el Nivel</option>
						<?php foreach ($listNiv as $level){ ?>
							<?php if(isset($course) && $course->id_level==$level['id_level']){ ?>
								<option selected value="<?php echo $level['id_level']; ?>"><?php echo $level['na_level']; ?></option>
							<?php }else{ ?>
								<option value="<?php echo $level['id_level']; ?>"><?php echo $level['na_level']; ?></option>
							<?php } ?>
						<?php } ?>
		            	</select>
				</div>
	        </div>
	        
			<hr>

			<div class="form-group">
				<label class="col-lg-3" for="user_nickname">Cover:</label>
				<div class="col-lg-7">
					<?php $this->widget('ext.EAjaxUpload.EAjaxUpload',
				    array(
				            'id'=>'uploadFile',
				            'config'=>array(
				                   'action'=>Yii::app()->createUrl('admin/upload'),
				                   'allowedExtensions'=>array("jpg", "jpeg", "png", "gif"),//array("jpg","jpeg","gif","exe","mov" and etc...
				                   'debug'=>false,
				                   'sizeLimit'=>5*1024*1024,// maximum file size in bytes
				                   'minSizeLimit'=>10*1024,// minimum file size in bytes
				                   'onComplete'=>"js:function(id, fileName, responseJSON){						                        
				                        $('#picture img').attr('src','".Yii::app()->request->baseUrl."/images/covers/'+fileName);
				                        $('#picture').css('display','block');
				                   }",
				            )
				    )); ?>
				    <div id='picture' class='picture' style='display:none;float:left;margin-left:20px;'>
				    	<img style='width:200px;height:200px' />
				    </div>
				</div>						
			</div>				

			<input class="btn btn-warning btn-lg col-lg-offset-3" type="submit" value="Guardar">

		<?php $this->endWidget(); ?>
	</div>
</div>
<hr>