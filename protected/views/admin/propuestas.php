<div class="page-header">
    <h1>Listado de Propuestas</h1>
    <p class="panel-title" style="font-size: 24px;">Listado de próximos cursos.</p>
</div>

<div class="row">
    <div class="col-md-12">
        <form class="form-inline" style="margin-left: 5px;">
            <div class="form-group col-xs-4">
                <input class="form-control " id="busqueda" placeholder="Nombre de la propuesta" name="dato" value="">
            </div>
            <button type="submit" class="btn btn-default" style="margin-left: -20px;">Buscar</button>
        </form>

        <hr>

        <div style="text-align: center;">

            <a class="btn btn-default" href="<?php echo Yii::app()->urlManager->createUrl('admin/convertirTodas'); ?>">Convertir a CURSO</a>
            <a class="btn btn-success" href="<?php echo Yii::app()->urlManager->createUrl('admin/nuevaPropuesta'); ?>">Nueva Propuesta</a>

        </div>
        <h2 style="text-align: center;">Propuestas (<?php echo $dataProvider->totalItemCount; ?>)</h2>

        <?php
        $this->widget('zii.widgets.grid.CGridView', array(
            'dataProvider' => $dataProvider,
            'id' => 'listadoPropuestas',
            'template' => "{items}\n{pager}",
            'columns' => array(
                array(
                    'type' => 'raw',
					'value' => '(isset($data->category)) ? "<button class=boton-tags>".$data->category->na_cathegory."</button>" : ""',
                    //'value' => '"<button class=boton-tags>".$data->category->na_cathegory."</button>"',
                    'header' => 'Categoría'
                )
                ,
                'name',
                array(
                    'value' => '(strlen($data->description) >= 200) ? substr($data->description,0,200)."..." : $data->description',
                    'header' => 'Descripción'
                ),
                array(
                    'value' => 'OjalaUtils::convertDateFromDb($data->create, false)',
                    'header' => 'Creación'
                ),
                array(
                    'type' => 'raw',
                    'value' => '($data->id_state == 4) ? "<span class=label-no-revisado>No Revisado</span>" : "<span class=label-revisado>Revisado</span>"',
                    'header' => 'Estado'
                ),
                array
                    (
                    'class' => 'CButtonColumn',
                    'template' => '{show} {edit}',
                    'header' => 'Opción',
                    'buttons' => array
                        (
                        'edit' => array
                            (
                            'label' => 'Editar',
                            'options' => array('class' => 'btn btn-warning btn-xs', 'style' => 'margin:3px'),
                            'url' => 'Yii::app()->urlManager->createUrl("admin/editarPropuesta", array("id" => $data->id_course))',
                        ),
                        'show' => array
                            (
                            'label' => 'Abrir',
                            'options' => array('class' => 'btn btn-info btn-xs', 'style' => 'margin:3px'),
                            'url' => 'Yii::app()->urlManager->createUrl("site/propuesta", array("slug" => $data->slug))',
                        )
                    ),
                ),
            ),
            'itemsCssClass' => 'table table-bordered espacio',
        ));
        ?>

    </div>

    <script type="text/javascript">
        $('.tags').tooltip();
    </script>