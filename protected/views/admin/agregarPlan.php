<div class="page-header">
	<h1>Plan</h1>
	<p class="panel-title" style="font-size: 24px;">Completa la información de éste formulario para la inclusión de un nuevo plan de pago. </p>
</div>

<div class="row">
    <div class="col-md-12">
        <a class="btn btn-default" href="<?php echo Yii::app()->urlManager->createUrl('admin/index') ?>">« Regresar al Administrador</a>
    </div>
</div>

<br>

<div class="row">
	<div class="container user">

		<div class="col-md-12 page-layout">
			<div class="div">

				<?php $form=$this->beginWidget('CActiveForm', array(
					'id'=>'register-form',
					'htmlOptions' => array('class'=>'form-horizontal','enctype'=>'multipart/form-data'),
				)); ?>

					<?php if($plan->id_stype!=""){ ?>
					<input type="hidden" name="id_stype" value="<?php echo $plan->id_stype; ?>" />
					<?php } ?>

					<div class="form-group">
						<label class="col-sm-3 control-label" for="recurrence">Tipo de suscripción</label>
						<div class="col-lg-4">
							<?php echo $form->dropDownList($plan, 'recurrence', 
								array(
									'0' =>'No recurrente',
									'1' =>'Recurrente'
								), 
								array('class' => 'combobox form-control')
						    ); 
						    ?>
						</div>
					</div>

					<div class="form-group">
						<label class="col-sm-3 control-label" for="recurrence">Intervalo de cobro</label>
						<div class="col-lg-4">
							<?php echo $form->dropDownList($plan, 'interval', 
								array(
									'month' =>'Mensual',
									'year' =>'Anual'
								), 
								array('class' => 'combobox form-control')
						    ); 
						    ?>
						</div>
					</div>			        
					<div class="form-group">
						<label class="col-lg-3" for="course_name">Nombre:</label>
						<div class="col-lg-7">
							<?php echo $form->textField($plan,'na_stype', array('class'=>'form-control')); ?>
							<?php echo $form->error($plan,'na_stype', array('style'=>'color: red;')); ?>
						</div>
					</div>					

					<div class="form-group">
						<label class="col-lg-3" for="course_name">Costo:</label>
						<div class="col-lg-7">
							<?php echo $form->textField($plan,'amount', array('class'=>'form-control')); ?>
							<?php echo $form->error($plan,'amount', array('style'=>'color: red;')); ?>
						</div>
					</div>	

					<div class="form-group">
						<label class="col-lg-3" for="course_name">ID. en servicios de pago:</label>
						<div class="col-lg-7">
							<?php echo $form->textField($plan,'id_service', array('class'=>'form-control')); ?>
							<?php echo $form->error($plan,'id_service', array('style'=>'color: red;')); ?>
						</div>
					</div>				

					<div class="form-group">
						<label class="col-lg-3" for="course_name">Días de prueba:</label>
						<div class="col-lg-7">
							<?php echo $form->textField($plan,'trial_period_days', array('class'=>'form-control')); ?>
							<?php echo $form->error($plan,'trial_period_days', array('style'=>'color: red;')); ?>
						</div>
					</div>

					<div class="form-group">
						<label class="col-lg-3" for="course_name">Permitir descargas:</label>
						<div class="col-lg-7">
							<?php echo $form->radioButtonList($plan,'with_downloads', array('0'=>'No','1'=>'Si'), array('template'=>'<div class="radio-inline"> {input} {label} </div>')); ?>
							<?php echo $form->error($plan,'with_downloads', array('style'=>'color: red;')); ?>
						</div>
					</div>						

					<?php if($plan->id_stype==""){ ?>
					<input class="btn btn-success btn-lg col-lg-offset-3" type="submit" value="Agregar plan">
					<?php }else{?>
					<input class="btn btn-success btn-lg col-lg-offset-3" type="submit" value="Actualizar plan">					
					<?php } ?>
				<?php $this->endWidget(); ?>

			</div>
		</div>
	</div>

</div>