<div class="page-header">
	<h1>Preguntas de las campañas</h1>
</div>

<div class="row">
	<form class="form-inline">
		<div class="col-md-10">
		    <a class="btn btn-default" href="<?php echo Yii::app()->urlManager->createUrl('admin/index') ?>">« Regresar al Administrador</a>
		</div>
		
		<div class="form-group col-md-2">
			<a class="btn btn-success btn-block" href="<?php echo Yii::app()->urlManager->createUrl('admin/crearCampana'); ?>">
				Nueva Campaña
			</a>
		</div>
	</form>
</div>

<?php if(isset($_GET['msg'])): ?>
	<div id="alert-bar" class="alert alert-success alert-wrapper">
		<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
		<?php echo htmlspecialchars($_GET['msg']) ?>
	</div>
<?php endif;?>

<div class="row">
	<div class="col-md-12">
	<?php
	$this->widget('zii.widgets.grid.CGridView', array(
	    'id' => 'campanas-grid',
		'cssFile'=>'',
		'dataProvider'=>$model->search(),
		'filter'=>$model,
		'filterCssClass'=>'filter-wrapper',
		'itemsCssClass'=>'table table-bordered table-hover table-striped',
		'columns' => array('name',
		array(
			'name'=>'id_campaign',
			'value' => 'CHtml::link($data->url,$data->url, array(\'target\'=>\'_blank\'))',
			'type'=>'html'
		),
		array(
		            'class'=>'zii.widgets.grid.CButtonColumn',
		            'template'=>'{update} {delete}',
		            'header'=>'Opción',
		            'buttons'=>array(
		            		'delete'=>array('visible'=>'$data->deletable')
		            	),
		            'updateButtonUrl' => 'Yii::app()->urlManager->createUrl("admin/editarCampana/".$data->primaryKey)',
		            'updateButtonLabel' => 'Editar',
		            'updateButtonImageUrl' => '',
		            'updateButtonOptions' => array('class'=>'btn btn-xs btn-warning'),

		            'deleteConfirmation' => '¿Desea eliminar este campaña? (no se puede deshacer)',
		            'deleteButtonUrl' => 'Yii::app()->urlManager->createUrl("admin/eliminarCampana/".$data->primaryKey)',
		            'deleteButtonLabel' => 'Eliminar',
		            'deleteButtonImageUrl' => '',
		            'deleteButtonOptions' => array('class'=>'btn btn-xs btn-danger'),
		        ),
	    ),
	)); ?>
	</div>
</div>

<div class="alert alert-info">
	Las campañas con respuestas de suscriptores, no pueden ser eliminadas.
</div>