<?php
Yii::app()->clientScript->registerCssFile(Yii::app()->baseUrl . '/css/jquery.tagsinput.css', 'screen');
Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl . '/js/scholarships/jquery.tagsinput.min.js', CClientScript::POS_END);
Yii::app()->clientScript->registerScript('taginput', "$(document).ready(function(){
			$('#tags').tagsInput({
			width: '100%',
			height: '45px',
			defaultText: ''
		});
	});"
        , CClientScript::POS_END);
?>

<div class="page-header">
	<h1>Edita los datos del curso para publicarlo</h1>
	<p>Completa la información de éste formulario para la creación de un nuevo curso. </p>
</div>

<ol class="breadcrumb" style="margin-top:-40px; margin-bottom:30px;">
  <li><a href="<?php echo Yii::app()->urlManager->createUrl('admin/index'); ?>">Home admin</a></li>
  <li><a href="<?php echo Yii::app()->urlManager->createUrl('admin/cursos'); ?>">Cursos</a></li>
  <li class="active">Curso</li>
</ol>

<?php if(isset($_GET['msg'])) :?>
	<div class="alert alert-success ">
		<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
		<?php echo $_GET['msg'] ?>
	</div>
<?php endif; ?>

<div class="content-fluid">
	<div class="col-md-7">

		<?php $form=$this->beginWidget('CActiveForm', array(
			'id'=>'register-form',
			'htmlOptions' => array('class'=>'form-horizontal','enctype'=>'multipart/form-data'),
		)); ?>

		<div class="form-group">
			<label for="course_name">Nombre del Nuevo Curso:</label>
			<?php echo $form->textField($course,'name', array('size'=>'500', 'class'=>'form-control input-lg')); ?>
			<?php echo $form->error($course,'name', array('style'=>'color: red;')); ?>
		</div>

		<div class="form-group">
			<?php echo $form->label($course,'slug', array())?>
			<?php echo $form->textField($course,'slug', array('size'=>'500', 'class'=>'form-control')); ?>
			<?php echo $form->error($course,'slug', array('style'=>'color: red;')); ?>
		</div>

		<div class="form-group">
			<?php echo $form->label($course,'id_level', array())?>
			<?php 
				$options = CHtml::listData($listNiv,'id_level','na_level');
				echo $form->dropDownList($course, 'id_level', $options, array('class'=>'form-control'));
			?>
		</div>

		<div class="form-group">
			<?php echo $form->label($course,'id_cathegory', array())?>
			<select class="form-control" name="id_cathegory">
				<option value>Seleccione una Categoría</option>
				<?php foreach ($listCat as $cathegory){ ?>
					<?php if(isset($course) && $course->id_cathegory==$cathegory['id_cathegory']){ ?>
						<option selected value="<?php echo $cathegory['id_cathegory']; ?>">
							<?php echo $cathegory['na_cathegory']; ?>
						</option>
					<?php }else{ ?>
						<option value="<?php echo $cathegory['id_cathegory']; ?>"><?php echo $cathegory['na_cathegory']; ?></option>
					<?php } ?>
				<?php } ?>
			</select>
		</div>

		<div class="form-group">
			<?php echo $form->label($course,'cost', array())?>
			<?php echo $form->textField($course,'cost', array('size'=>'10', 'class'=>'form-control')); ?>
			<?php echo $form->error($course,'cost', array('style'=>'color: red;')); ?>
		</div>

		<div class="form-group">
			<?php echo $form->label($course,'description', array())?>
			<?php echo $form->textarea($course,'description', array('rows'=>'5', 'class'=>'form-control')); ?>
			<?php echo $form->error($course,'description', array('style'=>'color: red;')); ?>
		</div>

		<div class="form-group">
			<?php echo $form->label($course,'requirements', array())?>
			<?php echo $form->textarea($course,'requirements', array('rows'=>'5', 'class'=>'form-control')); ?>
			<?php echo $form->error($course,'requirements', array('style'=>'color: red;')); ?>
		</div>

		<div class="form-group">
			<label for="user_email">¿Qué voy a aprender?:</label>
			<?php echo $form->textarea($course,'learn', array('rows'=>'5', 'class'=>'form-control')); ?>
			<?php echo $form->error($course,'learn', array('style'=>'color: red;')); ?>
		</div>

		<div class="form-group">
			<?php echo $form->label($course,'directed', array())?>
			<?php echo $form->textarea($course,'directed', array('rows'=>'5', 'class'=>'form-control')); ?>
			<?php echo $form->error($course,'directed', array('style'=>'color: red;')); ?>
		</div>

		<div class="form-group">
			<?php echo $form->label($course,'trailer', array())?>
			<?php echo $form->textField($course,'trailer', array('class'=>'form-control')); ?>
			<?php echo $form->error($course,'trailer', array('style'=>'color: red;')); ?>
		</div>
		
		<!--<div class="form-group">
			<?php echo $form->label($course,'duration', array())?>
			<?php echo $form->textField($course,'duration', array('size'=>'500', 'class'=>'form-control')); ?>
			<?php echo $form->error($course,'duration', array('style'=>'color: red;')); ?>
		</div>-->

		<div class="form-group">
			<label for="user_email">Tags:</label>
			<input class="form-control" id="tags" name="tags" value="<?php if(count($tags)>0){ foreach ($tags as $key => $value) { echo $value->tag->tag.', '; }} ?>" />
		</div>

		<div class="form-group">
			<label for="user_nickname">Cover:</label>
			<?php 
				$this->widget('ext.EAjaxUpload.EAjaxUpload',
				array(
					'id'=>'uploadFile',
					'config'=>array(
						'action'=>Yii::app()->createUrl('admin/upload'),
						'allowedExtensions'=>array("jpg", "jpeg", "png", "gif"),
						'debug'=>false,
						'sizeLimit'=>5*1024*1024,
						'minSizeLimit'=>10*1024,
						'onComplete'=>"js:function(id, fileName, responseJSON){
							$('#picture img').attr('src','".Yii::app()->request->baseUrl."/images/covers/'+fileName);
							$('#picture').css('display','block');
						}",
					)
				)); 
				$imgPath = getcwd() . '/' . Yii::app()->params['coverPath'];
				$mediumPath = $imgPath . substr($course->cover, 0, -4) . Course::COVER_MEDIUM_SUFFIX;
				$imagen = file_exists($mediumPath) ? $course->getCoverURL('small') : NULL;
			?>
			<div id='picture' class='picture'>
				<?php if($imagen) :?>
					<img style=' border-radius:5px;' src='<?php echo $imagen; ?>' />
				<?php else: ?>
					<h4>No se ha cargado ninguna imagen</h4>
				<?php endif; ?>
			</div>
		</div>

		<div class="form-group">
			<input class="btn btn-success btn-lg btn-block" type="submit" value="Guardar cambios hechos">
		</div>
		<?php $this->endWidget(); ?>	
	</div>

	<div class="col-md-4 col-md-offset-1">
		
		<?php if($state->na_state!="Publicado"){ ?>
			<form class="form-inline" action="<?php echo Yii::app()->urlManager->createUrl('admin/cambiarEstado'); ?>" method="get">
				<?php
					$this->widget('zii.widgets.jui.CJuiDatePicker',array(
						'name'=>'date',
						'language'=>'es',
						'options'=>array(        
							'showButtonPanel'=>true,
							'dateFormat'=>'dd/mm/yy',										
						),
						'htmlOptions'=>array(
							'id'=>'datetimepicker1',
				    	'placeholder'=>'Fecha de publicación',
							'class'=>'form-control col-md-5'
						),
					));?>
					<input type="hidden" name="id" value="<?php echo $course->id_course; ?>" />
					<input type="hidden" name="id_state" value="1" />
					<input type="submit" class="btn btn-success" value="Publicar">
			</form>
			<hr>
		<?php } ?>

		<h4>Estado del curso:
			<?php if($state->na_state=="Publicado"){ ?>
				<span>Está publicado!</span>
			<?php }else if($state->na_state=="Oculto"){ ?>
				<span>Está Oculto</span>
			<?php }else if($state->na_state=="Borrador"){ ?>
				<span>Es un Borrador</span>
			<?php } ?>
		</h4>

		<hr>

		<div class="btn-group">
  		<a href="https://oja.wistia.com/projects/<?php echo $course->wistia_uid; ?>" class="btn btn-success" target="_blank">Ver curso en Wistia </a>
  		<a href="<?php echo Yii::app()->urlManager->createUrl('admin/editarPorWistia', array('id'=>$course->id_course)); ?>" class="btn btn-primary">Editar según Wistia</a>
		</div>

		<hr>

		<div class="btn-group">
			<a class="btn btn-success" href="<?php echo Yii::app()->urlManager->createUrl('admin/escanearMaterial', array('id_course'=>$course->id_course)); ?>">Escanear Descargables</a>
			<a class="btn btn-primary" href="<?php echo Yii::app()->urlManager->createUrl('admin/escanearMaterial', array('id_course'=>$course->id_course)); ?>">Subir material</a>
		</div>

		<hr>

		<?php if($state->na_state!="Borrador"){ ?>
			<a class="btn btn-default btn-block" style="" href="<?php echo Yii::app()->urlManager->createUrl('admin/cambiarEstado', array('id'=>$course->id_course, 'id_state'=>2)); ?>">Dejar como borrador este curso</a>
		<?php } ?>
		<?php if($state->na_state!="Oculto"){ ?>
			<a class="btn btn-primary btn-block" style="" href="<?php echo Yii::app()->urlManager->createUrl('admin/cambiarEstado', array('id'=>$course->id_course, 'id_state'=>3)); ?>">Ocultar este curso de los listados</a>
		<?php } ?>

		<hr>
		<?php 	echo CHtml::link(
			'Eliminar este curso',
			array('admin/eliminarCurso','id_course'=>$course->id_course),
			array('confirm' => '¿Estas completamente seguro de eliminar el curso?, Los cambios no se podran revertir', 'class' => 'btn btn-danger btn-block')									     
		);?>
	</div>
</div>

<div class="clearfix"></div>
<div class="content-fluid">
	<div class="page-header"><h1>Programa del curso</h1></div>
	<div class="row">
		<table class="table table-bordered">
			
			<thead>
				<th style="width: 30%;">Titulo</th>
				<th style="width: 30%;">Slug</th>
				<th style="width: 15%;">Wistia</th>
				<th style="width: 5%;">Material</th>
				<!--<th style="width: 10%;">Posición</th>-->
				<th style="width: 10%;">Publico</th>
			</thead>
			
			<tbody>
				<?php $lesson=""; foreach ($list as $class) { ?>
					
					<!-- Titulo de la seccion del programa -->
					<?php if($lesson!=$class['na_lesson']){
						$lesson=$class['na_lesson']; ?>
						<tr>
							<td class="success" colspan="6">
								<form  class="form-inline">
									<input class="form-control" type="text" name="nombre" id="txl<?php echo $class['id_lesson']; ?>" value="<?php echo $class['na_lesson']; ?>" style="width: 57%;" />
									<div class="btn-group">
										<a class="btn btn-success editarlesson" idl="<?php echo $class['id_lesson']; ?>" id="idc<?php echo $class['id_lesson']; ?>" /><span class="glyphicon glyphicon-pencil"></span></a>
										<a class="btn btn-danger borrarlesson" idl="<?php echo $class['id_lesson']; ?>" id="idd<?php echo $class['id_lesson']; ?>" /><span class="glyphicon glyphicon-remove"></span></a>
									</div>
								</form>
							</td> 
						</tr>
					<?php } ?>
					<!-- * * * * * * * * * * * * -->

					<tr>

						<!-- Titulo de la clase -->
						<td>
							<form  class="form-inline">
								<input class="form-control" type="text" name="nombre" id="txc<?php echo $class['id_topic']; ?>" value="<?php echo $class['na_topic']; ?>" style="width: 85%;" />
								<a  class="btn btn-default editarclase" idt="<?php echo $class['id_topic']; ?>" id="idc<?php echo $class['id_topic']; ?>" /><span class="glyphicon glyphicon-pencil"></span></a>
							</form>
						</td> 
						<!-- * * * * * * * * * * * * -->
						
						<!-- Slug de la clase -->
						<td>
							<form  class="form-inline">
								<input class="form-control" type="text" name="slug"   id="txs<?php echo $class['id_topic']; ?>"  value="<?php echo $class['slug']; ?>" style="width: 85%;" />
								<a class="btn btn-default editarslug"   idt="<?php echo $class['id_topic']; ?>" id="ids<?php echo $class['id_topic']; ?>" /><span class="glyphicon glyphicon-pencil"></span></a>
							</form>
						</td> 
						<!-- * * * * * * * * * * * * -->
						
						<!-- Editar clase según wistia -->
						<td>
							<form  class="form-inline">
								<input class="form-control" type="text" name="slug"   id="txw<?php //echo $class['id_topic']; ?>"  value="<?php echo $class['wistia_uid']; ?>" style="width: 70%;" />
								<a class="btn btn-default editarwistia"   idt="<?php //echo $class['id_topic']; ?>" id="idw<?php echo $class['id_topic']; ?>" /><span class="glyphicon glyphicon-pencil"></span></a>
							</form>
						</td> 
						<!-- * * * * * * * * * * * * -->

						<!-- Tiene o no material descargable -->
						<td>
							<?php if($class['package']!=""){ ?>
								<span class="label label-success" style="width: 20px;">Si tiene</span> 
							<?php }else{ ?>
								<span class="label label-danger" style="width: 20px;">No tiene</span> 
							<?php } ?>
						</td> 
						<!-- * * * * * * * * * * * * -->
						
						<!--
						<td>
							<span class="label label-default"><?php echo $class['positiont']; ?></span>
							<a href="#" class="btn btn-primary subirClase" idt="<?php echo $class['id_topic']; ?>"><span class="glyphicon glyphicon-upload"></span></a>
							<a href="#" class="btn btn-primary bajarClase" idt="<?php echo $class['id_topic']; ?>"><span class="glyphicon glyphicon-download"></span></a>
						</td>-->
						
						<td>
							<span class="label label-default"><?php echo $class['public']; ?></span>
							<a href="#" class="btn btn-primary publico" idt="<?php echo $class['id_topic']; ?>"><span class="glyphicon glyphicon-eye-close"></span></a>
						</td>
					</tr>
				<?php } ?>
			</tbody>
		</table>
	</div>
</div>

<script type="text/javascript">
    $('.editarwistia').click(function (event)
    {
        event.preventDefault();
        var idt = $(this).attr('idt');
        if ($("#txw" + idt).val() == '')
        {
            $("#txw" + idt).attr('style', 'border-color: red;');
        }
        else
        {
            $("#idw" + idt).attr('disabled', 'disabled');
            $.ajax({
                'url': '<?php echo Yii::app()->urlManager->createUrl('admin/EditarWistiaClase'); ?>?id=' + idt + '&wistia=' + $("#txw" + idt).val(),
                'cache': false,
                success: function (data)
                {
                    $("#txw" + idt).attr('value', data);
                    $("#idw" + idt).removeAttr('disabled');

                    $("#txw" + idt).attr('style', 'border-color: rgb(134, 221, 0); width: 85%;');
                    $("#txw" + idt).focus();
                    setTimeout(function () {
                        $("#txw" + idt).attr('style', 'width: 85%;');
                    }, 2000);
                }
            });
        }
    });

    $('.editarslug').click(function (event)
    {
        event.preventDefault();
        var idt = $(this).attr('idt');
        if ($("#txs" + idt).val() == '')
        {
            $("#txs" + idt).attr('style', 'border-color: red;');
        }
        else
        {
            $("#ids" + idt).attr('disabled', 'disabled');
            $.ajax({
                'url': '<?php echo Yii::app()->urlManager->createUrl('admin/EditarSlugClase'); ?>?id=' + idt + '&slug=' + $("#txs" + idt).val(),
                'cache': false,
                success: function (data)
                {
                    $("#txs" + idt).attr('value', data);
                    $("#ids" + idt).removeAttr('disabled');

                    $("#txs" + idt).attr('style', 'border-color: rgb(134, 221, 0); width: 85%;');
                    $("#txs" + idt).focus();
                    setTimeout(function () {
                        $("#txs" + idt).attr('style', 'width: 85%;');
                    }, 2000);
                },
            });
        }
    });

    $('.editarclase').click(function (event)
    {
        event.preventDefault();
        var idt = $(this).attr('idt');
        if ($("#txc" + idt).val() == '')
        {
            $("#txc" + idt).attr('style', 'border-color: red;');
        }
        else
        {
            $("#idc" + idt).attr('disabled', 'disabled');
            $.ajax({
                'url': '<?php echo Yii::app()->urlManager->createUrl('admin/EditarClase'); ?>?id=' + idt + '&nombre=' + $("#txc" + idt).val(),
                'cache': false,
                success: function (data)
                {
                    $("#txc" + idt).attr('value', data);
                    $("#idc" + idt).removeAttr('disabled');

                    $("#txc" + idt).attr('style', 'border-color: rgb(134, 221, 0); width: 85%;');
                    $("#txc" + idt).focus();
                    setTimeout(function () {
                        $("#txc" + idt).attr('style', 'width: 85%;');
                    }, 2000);
                },
            });
        }
    });

    $('.editarlesson').click(function (event)
    {
        event.preventDefault();
        var idl = $(this).attr('idl');
        if ($("#txl" + idl).val() == '')
        {
            $("#txl" + idl).attr('style', 'border-color: red;');
        }
        else
        {
            $("#idl" + idl).attr('disabled', 'disabled');
            $.ajax({
                'url': '<?php echo Yii::app()->urlManager->createUrl('admin/EditarLesson'); ?>?id=' + idl + '&nombre=' + $("#txl" + idl).val(),
                'cache': false,
                success: function (data)
                {
                    $("#txl" + idl).attr('value', data);
                    $("#idl" + idl).removeAttr('disabled');

                    $("#txl" + idl).attr('style', 'border-color: rgb(134, 221, 0); width: 85%;');
                    $("#txl" + idl).focus();
                    setTimeout(function () {
                        $("#txl" + idl).attr('style', 'width: 85%;');
                    }, 2000);
                },
            });
        }
    });

    $('.borrarlesson').click(function (event)
    {
        event.preventDefault();
        var idl = $(this).attr('idl');
        $("#idc" + idl).attr('disabled', 'disabled');
        $.ajax({
            'url': '<?php echo Yii::app()->urlManager->createUrl('admin/borrarLesson'); ?>?id=' + idl,
            'cache': false,
            success: function (data)
            {
                location.reload();
            },
        });
    });

	$('.publico').click(function(event) 
    {		
    	event.preventDefault();	
        var idt=$(this).attr('idt');
        $.ajax({
            'url': '<?php echo Yii::app()->urlManager->createUrl('admin/publicoClase'); ?>?id='+idt,
            'cache': false,
	        success: function (data) 
	        {
	        	alert(data);
				//location.reload();
			},
        });
   	});

	$('.subirClase').click(function(event) 
    {		
    	event.preventDefault();	
        var idt=$(this).attr('idt');
        $.ajax({
            'url': '<?php echo Yii::app()->urlManager->createUrl('admin/subirClase'); ?>?id=' + idt,
            'cache': false,
            success: function (data)
            {
                location.reload();
            },
        });
    });

    $('.bajarClase').click(function (event)
    {
        event.preventDefault();
        var idt = $(this).attr('idt');
        $.ajax({
            'url': '<?php echo Yii::app()->urlManager->createUrl('admin/bajarClase'); ?>?id=' + idt,
            'cache': false,
            success: function (data)
            {
                location.reload();
            },
        });
    });
</script>