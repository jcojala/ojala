<div class="page-header">
	<h1>Diplomado</h1>
	<p class="panel-title" style="font-size: 24px;">Completa la información de éste formulario para la creación de un nuevo diplomado. </p>
</div>

<div class="row">
    <div class="col-md-12">
        <a class="btn btn-default" href="<?php echo Yii::app()->urlManager->createUrl('admin/index') ?>">« Regresar al Administrador</a>
    </div>
</div>

<br>

<div class="row">
	<div class="container user">

		<div class="col-md-12 page-layout">
			<div class="div">

				<?php $form=$this->beginWidget('CActiveForm', array(
					'id'=>'register-form',
					'htmlOptions' => array('class'=>'form-horizontal','enctype'=>'multipart/form-data'),
				)); ?>

					<?php if($group->id_group!=""){ ?>
					<input type="hidden" name="id_group" value="<?php echo $group->id_group; ?>" />
					<?php } ?>

					<div class="form-group">
						<label class="col-lg-3" for="course_name">Nombre del Diplomado:</label>
						<div class="col-lg-7">
							<?php echo $form->textField($group,'nb_group', array('class'=>'form-control')); ?>
							<?php echo $form->error($group,'nb_group', array('style'=>'color: red;')); ?>
						</div>
					</div>

					<div class="form-group">
						<label class="col-lg-3" for="course_name">Descripcion del Diplomado:</label>
						<div class="col-lg-7">
							<?php echo $form->textArea($group,'description', array('class'=>'form-control', 'rows'=>'8')); ?>
							<?php echo $form->error($group,'description', array('style'=>'color: red;')); ?>
						</div>
					</div>

					<div class="form-group">
						<label class="col-lg-3" for="user_last_name">Costo del Diplomado:</label>
						<div class="col-lg-7">
							<?php echo $form->textField($group,'cost', array('size'=>'10', 'class'=>'form-control')); ?>
							<?php echo $form->error($group,'cost', array('style'=>'color: red;')); ?>
						</div>
					</div>

					<?php if($group->id_group!=""){ ?>
						<div class="form-group">
							<label class="col-lg-3" for="user_last_name">Slug:</label>
							<div class="col-lg-7">
								<?php echo $form->textField($group,'slug', array('size'=>'10', 'class'=>'form-control')); ?>
								<?php echo $form->error($group,'slug', array('style'=>'color: red;')); ?>
							</div>
						</div>
					<?php } ?>

					<hr>

					<div class="form-group">
						<label class="col-lg-3" for="user_nickname">Cover:</label>
						<div class="col-lg-7">
							<?php $this->widget('ext.EAjaxUpload.EAjaxUpload',
						    array(
						            'id'=>'uploadFile',
						            'config'=>array(
						                   'action'=>Yii::app()->createUrl('admin/upload'),
						                   'allowedExtensions'=>array("jpg", "jpeg", "png", "gif"),//array("jpg","jpeg","gif","exe","mov" and etc...
						                   'debug'=>false,
						                   'sizeLimit'=>5*1024*1024,// maximum file size in bytes
						                   'minSizeLimit'=>10*1024,// minimum file size in bytes
						                   'onComplete'=>"js:function(id, fileName, responseJSON){						                        
						                        $('#picture img').attr('src','".Yii::app()->request->baseUrl."/images/covers/'+fileName);
						                        $('#picture').css('display','block');
						                   }",
						                   //'messages'=>array(
						                   //                  'typeError'=>"{file} has invalid extension. Only {extensions} are allowed.",
						                   //                  'sizeError'=>"{file} is too large, maximum file size is {sizeLimit}.",
						                   //                  'minSizeError'=>"{file} is too small, minimum file size is {minSizeLimit}.",
						                   //                  'emptyError'=>"{file} is empty, please select files again without it.",
						                   //                  'onLeave'=>"The files are being uploaded, if you leave now the upload will be cancelled."
						                   //                 ),
						                   //'showMessage'=>"js:function(message){ alert(message); }"
						                  )
						    )); ?>
						    <div id='picture' class='picture' style='display:none;float:left;margin-left:20px;'>
						    	<img style='width:200px;height:200px' />
						    </div>
						</div>						
					</div>								

					<input class="btn btn-success btn-lg col-lg-offset-3" type="submit" value="Guardar Diplomado">

				<?php $this->endWidget(); ?>

			</div>
		</div>
	</div>

</div>