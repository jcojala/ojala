<?php
Yii::app()->clientScript->registerCssFile(Yii::app()->baseUrl . '/css/jquery.tagsinput.css', 'screen');
Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl . '/js/scholarships/jquery.tagsinput.min.js', CClientScript::POS_END);
Yii::app()->clientScript->registerScript('taginput', "$(document).ready(function(){
			$('#tags').tagsInput({
			width: '100%',
			height: '45px',
			defaultText: ''
		});
	});"
		, CClientScript::POS_END);
?>

<div class="page-header">
    <h1>Editar Propuesta</h1>
    <p class="panel-title" style="font-size: 24px;">Completa el formulario para actualizar los datos de la propuesta. </p>
</div>

<div class="row">
    <div class="col-md-12">
        <a class="btn btn-default" href="<?php echo Yii::app()->urlManager->createUrl('admin/index') ?>">« Regresar al Administrador</a>
    </div>
</div>

<br>

<div class="row">

	<?php if (Yii::app()->user->hasFlash('error')) { ?>

		<div class="col-md-12 alert alert-danger">
			<p>
				<?php echo Yii::app()->user->getFlash('error'); ?>
			</p>
		</div>

	<?php } ?>

    <div class="col-md-9">

		<?php
		$form = $this->beginWidget('CActiveForm', array(
			'id' => 'register-form',
			'htmlOptions' => array(
				'class' => 'form-horizontal',
			)
		));
		?>

        <div class="form-group">
            <label class="col-lg-3" for="proposal_name">Nombre:</label>
            <div class="col-lg-7">
				<?php echo $form->textField($proposal, 'name', array('size' => '500', 'class' => 'form-control', 'id' => 'nombre')); ?>
				<?php echo $form->error($proposal, 'name', array('style' => 'color: red;')); ?>
            </div>
			<?php if ($proposal->id_state == 5) { ?>
				<span class="label label-success">Revisado</span>
			<?php } else { ?>

				<span class="label label-info">No revisado</span>

			<?php } ?>
        </div>

        <hr>

        <div class="form-group">
            <label class="col-lg-3" for="id_cathegory">Categoría</label>
            <div class="col-lg-9">
                <select class="combobox form-control" name="id_cathegory">
                    <option value>Seleccione una Categoría</option>
					<?php foreach ($listCat as $cathegory) { ?>
						<?php if ($proposal->id_cathegory == $cathegory->id_cathegory) { ?>
							<option selected value="<?php echo $cathegory->id_cathegory; ?>"><?php echo $cathegory->na_cathegory; ?></option>
						<?php } else { ?>
							<option value="<?php echo $cathegory->id_cathegory; ?>"><?php echo $cathegory->na_cathegory; ?></option>
						<?php } ?>
					<?php } ?>
                </select>
            </div>
        </div> 

        <div class="form-group">
            <label class="col-lg-3" for="id_cathegory">Nivel</label>
            <div class="col-lg-9">
                <select class="combobox form-control" name="id_level">
                    <option value>Seleccione un Nivel</option>
					<?php foreach ($listNiv as $level) { ?>
						<?php if ($proposal->id_level == $level->id_level) { ?>
							<option selected value="<?php echo $level->id_level; ?>"><?php echo $level->na_level; ?></option>
						<?php } else { ?>
							<option value="<?php echo $level->id_level; ?>"><?php echo $level->na_level; ?></option>
						<?php } ?>
					<?php } ?>
                </select>
            </div>
        </div>

        <div class="form-group">

            <label class="col-lg-3" for="user_email">Tags:</label>

            <div class="col-lg-9">

                <input class="form-control" id="tags" name="tags" value="<?php
				if (count($tags) > 0) {
					foreach ($tags as $key => $value) {
						echo $value->tag->tag . ', ';
					}
				}
				?>" />
            </div>
        </div>

        <div class="form-group">
            <label class="col-lg-3" for="proposal_name">Descripción:</label>
            <div class="col-lg-7">
				<?php // echo $form->textField($proposal, 'description', array('size' => '500', 'class' => 'form-control')); ?>
				<?php echo $form->textarea($proposal, 'description', array('class' => 'form-control', 'id' => 'descripcion')); ?>               
				<?php echo $form->error($proposal, 'description', array('style' => 'color: red;')); ?>
            </div>

        </div>

        <div class="form-group">
            <label class="col-lg-3" for="user_nickname">Cover:</label>
            <div class="col-lg-9">
				<?php
				$this->widget('ext.EAjaxUpload.EAjaxUpload', array(
					'id' => 'uploadFile',
					'config' => array(
						'action' => Yii::app()->createUrl('admin/upload'),
						'allowedExtensions' => array("jpg", "jpeg", "png", "gif"), //array("jpg","jpeg","gif","exe","mov" and etc...
						'debug' => false,
						'sizeLimit' => 5 * 1024 * 1024, // maximum file size in bytes
						'minSizeLimit' => 10 * 1024, // minimum file size in bytes
						'onComplete' => "js:function(id, fileName, responseJSON){
				                        $('#picture img').attr('src','" . Yii::app()->request->baseUrl . "/images/covers/'+fileName);
				                        $('#picture').css('display','block');
                                                        $('#picture p').text('');
				                   }",
					)
				));

				$imgPath = getcwd() . '/' . Yii::app()->params['coverPath'];
				$mediumPath = $imgPath . substr($proposal->cover, 0, -4) . Course::COVER_MEDIUM_SUFFIX;
				$imagen = file_exists($mediumPath) ? $proposal->getCoverURL('small') : NULL;
				?>
                <div id='picture' class='picture' style='display:block;float:left;'>
					<?php if ($imagen) : ?>
						<img style='width:35%; border-radius:5px;' src='<?php echo $imagen; ?>' />
					<?php else: ?>
						<img style='width:35%; border-radius:5px;'  />
						<p>No se ha cargado una portada</p>
					<?php endif; ?>
                </div>
            </div>						
        </div>				

    </div>

    <div class="col-md-12">
        <hr>
        <div class="col-md-9">
            <p>NOTA: Para realizar una exitosa conversión de propuesta a curso, en este campo deben estar sólo los capítulos y las clases. Adicionalmente, cada capítulo debe tener un número seguido de un punto al comienzo de la línea. Por ejemplo: "1. Introducción"</p>
            <br>
            <div class="payment-tabs">
                <ul id="paymentTabs" class="nav nav-tabs">
                    <li id="estrInicial" class="active"><a href="#home" data-toggle="tab" class="credit-card">Estructura Inicial</a></li>
                    <li id="estrFinal" class=""><a href="#profile" data-toggle="tab" class="paypal">Estructura Formateada</a></li>
                </ul>

                <div class="tab-content">
                    <div class="tab-pane active" id="home">
                        <div>

							<?php echo $form->textarea($proposal, 'structure_nat', array('id' => 'textoInicial', 'rows' => '30', 'class' => 'form-control', 'style' => 'margin:20px')); ?>

                        </div>
                    </div>

                    <div class="tab-pane" id="profile">
                        <div>

							<?php echo $form->textarea($proposal, 'structure_format', array('id' => 'textoFinal', 'rows' => '30', 'class' => 'form-control', 'style' => 'margin:20px')); ?>

                        </div>
                    </div>
                </div>
            </div>

        </div>

        <div class="col-md-3">

			<?php
			echo CHtml::ajaxLink(
					'Traducir', array('admin/traducir'), array(
				'success' => 'js:function(data){'
				. '         var res = data.split("###"); 
                            if(res.length >= 3){
                                document.getElementById("nombre").value=res[0];
                                document.getElementById("descripcion").value=res[1];
                                document.getElementById("textoFinal").value=res[2];
                            }else{
                                document.getElementById("nombre").value=res[0];
                                document.getElementById("descripcion").value="";
                                document.getElementById("textoFinal").value="";
                            }
                            document.getElementById("estrInicial").className = "";
                            document.getElementById("estrFinal").className = "active";
                            document.getElementById("home").className = "tab-pane";
                            document.getElementById("profile").className = "tab-pane active";
                            '
				. '}',
				'type' => 'POST',
				'data' => array(
					'textoInicial' => 'js:document.getElementById("textoInicial").value',
					'nombre' => 'js:document.getElementById("nombre").value',
					'descripcion' => 'js:document.getElementById("descripcion").value'
				),
					), array('class' => 'btn btn-danger', 'style' => 'background-color: #FF0000 !important')
			);
			?>

            <p>Usando API de Bing Translator</p>
            <hr>
			<?php
			if ($proposal->id_state == 4) {
				echo CHtml::link(
						'Propuesta Revisada', array('admin/estadoRevisadoIndividual', 'id' => $proposal->id_course), array('class' => 'btn btn-success', 'style' => '')
				);
				?>

				<hr> 
				<?php
			}
			?>

			<?php
			if ($proposal->id_state == 5) {

				echo CHtml::ajaxLink(
						'Convertir en curso', array('admin/convertirACurso'), array(
					'success' => 'js:function(data){'
					. 'alert("¡Listo!");}',
					'type' => 'POST',
					'data' => array('courseid' => $proposal->id_course, 'structure' => 'js:document.getElementById("textoFinal").value'),
						), array('class' => 'btn btn-info')
				);
				?>

				<hr> 
				<a class="btn btn-info" href="<?php echo Yii::app()->urlManager->createUrl('site/propuesta', array('slug' => $proposal->slug)); ?>">Ver portada</a>

				<?php
			}
			?>


			<?php
			if ($proposal->active == 0) {
				?>

				<hr> 
				<a class="btn btn-success" href="<?php echo Yii::app()->urlManager->createUrl('admin/publicarPropuesta', array('id' => $proposal->id_course)); ?>">Publicar Propuesta</a>


				<?php
			} else {
				?>

				<hr> 
				<a class="btn btn-success" href="<?php echo Yii::app()->urlManager->createUrl('admin/ocultarPropuesta', array('id' => $proposal->id_course)); ?>">Ocultar Propuesta</a>


				<?php
			}
			?>


        </div>

    </div>

    <input class="btn btn-warning btn-lg col-lg-offset-3" type="submit" value="Actualizar">

	<?php $this->endWidget(); ?>

    <div class="col-md-3">

    </div>
</div>
<hr>