<div class="page-header">
  <h1>Buscador de Estudiantes</h1>
  <?php if(isset($dato)){ ?>
  <p class="panel-title" style="font-size: 24px;">Resultados de "<?php echo $dato; ?>"</p>
  <?php }else{ ?>
  <p class="panel-title" style="font-size: 24px;">Últimos 10 Estudiantes Registrados</p>
  <?php } ?>
</div>

<ol class="breadcrumb" style="margin-top:-20px; margin-bottom:30px;">
  <li><a href="<?php echo Yii::app()->urlManager->createUrl('admin/index'); ?>">Home admin</a></li>
  <li class="active">Buscador</li>
</ol>

<div class="row">
  <div class="content-fluid">

    <div class="col-md-9">
      <form role="form">
        <div class="form-group col-md-8">
          <label class="sr-only" for="exampleInputEmail2"></label>
          <input type="text" class="form-control input-lg " placeholder="Email del estudiante" name="dato" value="<?php if(isset($dato)){echo $dato;} ?>">
        </div>
        <button type="submit" class="btn btn-primary btn-lg">Buscar correo</button>
      </form>
    </div>
    
    <div class="col-md-3">
      <a class="btn btn-success btn-lg" href="#">Registrar nuevo estudiante</a>
    </div>
  
  </div>
</div>

<br>

<div class="row" id="rowbuscar">
  
  <div class="col-md-12">


    <?php if(count($listaEstudiantes)>0){ ?>
  
    <table class="table table-bordered table-hover">
      <thead>
        <tr>
          <th>Nombres y Apellidos</th>
          <th>Email</th>
          <th>Opción</th>
        </tr>
      </thead>
      
      <tbody>
        <?php foreach($listaEstudiantes as $estudiante) { ?> 
        <tr>
          <td><?php echo $estudiante['name'] . ' ' .  $estudiante['lastname'] ?></td>
          <td><?php echo $estudiante['email1']; if(isset($estudiante['email2']) && $estudiante['email2']!=""){ echo ' ('.$estudiante['email2'].')'; } ?></td>
          <td><a type="button" class="btn btn-primary btn-xs" href="<?php echo Yii::app()->urlManager->createUrl('admin/estudiante', array('id'=>$estudiante['id_user'])); ?>">Ver perfil</a></td>
        </tr>
        <?php }?>
      </tbody>

    </table>
    
    <?php }else{ ?>
      
      <h4>No hay resultados con esa busqueda</h4>
      <h3>Pero puedes agregarlo</h3>
      <?php $form=$this->beginWidget('CActiveForm', array(
        'id'=>'register-form',
        'action'=>Yii::app()->createUrl('admin/registro'),
        'htmlOptions' => array(
          'class'=>"form-inline",
          ),
          )); ?>
          <input name="redirect" type="hidden" value="buscadorEstudiantes" />
          <div class='form-group'>
            <?php echo $form->textField($user,'name', array('size'=>'30', 'class'=>'form-control input-lg', 'placeholder'=>"Nombre:")); ?>
            <?php echo $form->error($user,'name'); ?>
          </div>
          <div class='form-group'>
            <?php echo $form->textField($user,'email1', array('size'=>'30', 'class'=>'form-control input-lg', 'placeholder'=>"Email:")); ?>
            <?php echo $form->error($user,'email1'); ?>
          </div>
          <input class="btn btn-success btn-lg" name="commit" type="submit" value="Registrar Usuario" />
          <?php $this->endWidget(); ?>
          <?php } ?>
        </div>
      </div>

      <div class="row" id="rownuevo" style="display: none;">
        <div class="col-md-12">
          <h3>Agregar un usuario</h3>
          <?php $form=$this->beginWidget('CActiveForm', array(
            'id'=>'register-form',
            'action'=>Yii::app()->createUrl('admin/registro'),
            'htmlOptions' => array(
              'class'=>"form-inline",
              ),
              )); ?>
              <input name="redirect" type="hidden" value="buscadorEstudiantes" />
              <div class='form-group'>
                <?php echo $form->textField($user,'name', array('size'=>'30', 'class'=>'form-control input-lg', 'placeholder'=>"Nombre:")); ?>
                <?php echo $form->error($user,'name'); ?>
              </div>
              <div class='form-group'>
                <?php echo $form->textField($user,'email1', array('size'=>'30', 'class'=>'form-control input-lg', 'placeholder'=>"Email:")); ?>
                <?php echo $form->error($user,'email1'); ?>
              </div>
              <input class="btn btn-success btn-lg" name="commit" type="submit" value="Registrar Usuario" />
              <?php $this->endWidget(); ?>
            </div>
          </div>
          <hr>
          <script type="text/javascript">

          $(function() {
            $(".nuevo").click(function()
            {
              $('#rownuevo').attr('style', '');
              $('#rowbuscar').attr('style', 'display: none;');
            });
          });

          </script>