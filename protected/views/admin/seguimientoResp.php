<!DOCTYPE html> 
<html>
    <head>
        <title>Seguimiento</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="https://code.jquery.com/mobile/1.4.3/jquery.mobile-1.4.3.min.css" />
        <script src="//code.jquery.com/jquery-1.11.0.min.js"></script>
        <script src="//code.jquery.com/jquery-migrate-1.2.1.min.js"></script>
        <script src="https://code.jquery.com/mobile/1.4.3/jquery.mobile-1.4.3.min.js"></script>
    </head>

    <body>
        <div data-role="page">
            <div data-role="header"><h1>Listado de Seguimiento de Estudiantes</h1></div>
            <div role="main" class="ui-content">
                <?php if(!isset($mode)){ ?>
                <div class="row">
                    <div class="col-md-6">
                        <a class="btn btn-default" href="<?php echo Yii::app()->urlManager->createUrl('admin/index') ?>">« Regresar al Administrador</a>
                    </div>
                </div>
                <?php } ?>

                <br>

                <?php if(count($listaEstudiantes)>0){ ?>
                <ul data-role="listview" data-inset="true">
                    <li data-role="list-divider">Lunes, Septiembre 8, 2014 <span class="ui-li-count"><?php echo count($listaEstudiantes); ?></span></li>

                    <?php $i=0; foreach($listaEstudiantes as $estudiante) { $i++; 
                        $user=User::model()->findbyPk($estudiante['id_user']);

                        if(strlen($estudiante['phone'])>9 && strlen($estudiante['phone'])<12){
                            $region=Region::model()->findbyPk($user->id_region);
                            if($region)
                            {
                                $estudiante['phone'] = '+' . $region->country->phone_code.' '.$user->phone;
                                $user->phone = '+' . $region->country->phone_code.' '.$user->phone;
                                $user->update();
                            }
                        } ?>
                    <?php $valor = User::lastLogin($estudiante['email1']); ?>
                    <li>
                        <!-- <li><a href="skype:<?php echo str_replace('-', '', str_replace(' ', '', $estudiante['phone'])); ?>?call"> -->
                        <h2><?php echo $estudiante['name'] . ' ' .  $estudiante['lastname'] ?> - <?php echo $estudiante['email1']; if(isset($estudiante['email2']) && $estudiante['email2']!=""){ echo ' ('.$estudiante['email2'].')'; } ?></h2>
                        <input type="text" value="<?php echo $estudiante['phone']; ?>">                        
                        <p><strong>Pais: <?php if(isset($user->region)){ echo $user->region->country->name.' - '.$user->region->name; }else{ echo OjalaUtils::getIntercomPais($estudiante['email1']); } ?></strong></p>
                        <strong><p>Registrado: <?php echo $estudiante['date']; ?></strong></p>
                        <p>Ultima vez visto: <?php echo $valor; ?></p>
                            <p class="ui-li-aside"><strong><?php echo $estudiante['phone']; ?></strong></p>
                        <!-- </a> -->
                    </li>

                    <?php }?>

                </ul>
                <?php }?>
            </div>
            <div data-role="footer">...</div>
        </div>
    </body>
</html>



        
