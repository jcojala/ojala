<?php $returnUrl = isset($_GET['returnUrl']) ? $_GET['returnUrl'] : Yii::app()->urlManager->createUrl('admin/suscriptores'); ?>
<div class="page-header">
	<h1>Edita los datos de la suscripción</h1>
</div>

<div class="row back-button">
	<div class="col-md-12">
		<a class="btn btn-default" href="<?php echo Yii::app()->urlManager->createUrl('admin/suscriptores'); ?>">« Listado de Suscriptores</a>
  </div>
</div>

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'subscriber-form',
	'htmlOptions' => array('class'=>'form-horizontal'),
)); ?>

<fieldset>
	<legend><?php echo $model->fullname; ?>(<?php echo $model->email1; ?>)</legend>
	<div class="form-group">
		<?php echo CHtml::label('Pasarela de pago','gateway', array('class' => 'col-lg-2 control-label')) ?>
		<div class="col-lg-2">
			<?php 
				$gateway = Subscribers::getGateway($subscription->id_service, $subscription->id_stype);
				echo CHtml::textField('gateway',$gateway, array('id'=>'gateway','class' => 'form-control', 'disabled'=>'true'));
			?>
		</div>
	</div>
	<div class="form-group">
		<?php echo $form->label($model,'id_service', array('class' => 'col-lg-2 control-label')) ?>
		<div class="col-lg-2">
			<?php 
				echo $form->textField($model,'id_service', array('class' => 'col-lg-2 form-control'));
				echo $form->error($model,'id_service');
			?> 
		</div>
		<?php
			$prefix = substr($model->id_service,0,2);
			$isSpecial = $subscription->id_stype == SuscriptionType::SCHOLARSHIP || $subscription->id_stype == SuscriptionType::TWOBYONE;

			if($prefix !== 'cu' && 
				(!is_numeric($prefix) && substr($subscription->id_service,0,2)!='I-') && 
				!$isSpecial
			){
				echo '<span class="alert alert-warning">(Probablemente incorrecto)</span>';
			}

			if(!$confirmedBeca) {
				echo '<span class="alert alert-danger">(Presione guardar para actualizar esta información)</span>';	
			}
		?>
	</div>
</fieldset>
<fieldset>
	<legend>Datos de la suscripción</legend>	
	<div class="form-group">
		<?php echo $form->label($subscription,'id_stype', array('class' => 'col-lg-2 control-label')) ?>
		<div class="col-lg-2">
			<?php 
				echo $form->dropDownList($subscription,'id_stype', $stypes, array('class' => 'col-lg-2 form-control'));
				echo $form->error($subscription,'id_stype');
			?> 
		</div>
	</div>
	<div class="form-group">
		<?php echo $form->label($subscription,'id_service', array('class' => 'col-lg-2 control-label')) ?>
		<div class="col-lg-2">
			<?php 
				echo $form->textField($subscription,'id_service', array('class' => 'col-lg-2 form-control'));
				echo $form->error($subscription,'id_service');
			?> 
		</div>
		<?php
			$prefix = substr($subscription->id_service,0,2);

			if($prefix !== 'SU' && $prefix !== 'su' && $prefix!='I-' && !$isSpecial ) {
				echo '<span class="alert alert-warning">(Probablemente incorrecto)</span>';
			}

			if(!$confirmedBeca) {
				echo '<span class="alert alert-danger">(Presione guardar para actualizar este campo)</span>';	
			}
		?>
	</div>
	
	<div class="form-group">
		<?php echo CHtml::label('Estado','status', array('class'=>'col-lg-2 control-label')); ?>
		<div class="col-lg-2">
			<?php echo CHtml::dropDownList('status', $status, Status::listData() , array('class'=>'form-control')); ?>
		</div>
	</div>

	<div class="form-group">
		<?php echo $form->label($subscription,'subscriptionDate', array('class' => 'col-lg-2 control-label')) ?>
		<div class="col-lg-2">
			<?php
			$this->widget('zii.widgets.jui.CJuiDatePicker',array(
				'model'=>$subscription,
				'attribute'=>'subscriptionDate',
				'language'=>'es',
				'options'=>array(        
				    'showButtonPanel'=>true,
				    'dateFormat'=>'dd/mm/yy',
				    'maxDate'=>'0',
				),
				'htmlOptions'=>array(
				    'class'=>'form-control'
				),
			));
		 ?>
		 <?php echo $form->error($subscription,'subscriptionDate') ?>
		</div>
	</div>

	<div id="cancelation-wrapper" <?php if($subscription->active && $status==1) echo 'style="display:none"'?> class="form-group">
		<?php echo $form->label($subscription,'cancelationDate', array('class' => 'col-lg-2 control-label')) ?>
		<div class="col-lg-2">
			<?php
				$this->widget('zii.widgets.jui.CJuiDatePicker',array(
					'model'=>$subscription,
					'attribute'=>'cancelationDate',
					'language'=>'es',
					'options'=>array(        
					    'showButtonPanel'=>true,
					    'dateFormat'=>'dd/mm/yy'
					),
					'htmlOptions'=>array(
					    'class'=>'form-control'
					),
				));
			 ?>
			<?php echo $form->error($subscription,'cancelationDate') ?>
		</div>
	</div>

	<div class="col-lg-3 col-lg-offset-2">
		<?php echo CHtml::submitButton('Guardar cambios', array('class'=>'btn btn-primary')) ?>
		<a class="btn btn-default" href="<?php echo $returnUrl ?>">Cancelar</a>
	</div>
</fieldset>
<div class="spacer"></div>
<?php $this->endWidget() ?>

<script>
	$(document).ready(function(){
		$('#status').on('change', function(){
			if($(this).val()!=1){
				$('#cancelation-wrapper input').val('');
				$('#cancelation-wrapper').fadeIn();
			} else {
				$('#cancelation-wrapper').fadeOut();
			}
		})
	});
</script>