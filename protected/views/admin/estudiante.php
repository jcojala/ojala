<div class="page-header">
    <h1>Perfil del Estudiante: <b><?php echo $user->name.' '.$user->lastname; ?></b></h1>
    <p>Información general sobre el estudiante y su comportamiento en Oja.la.</p>
</div>

<ol class="breadcrumb" style="margin-top:-20px; margin-bottom:30px;">
    <li><a href="<?php echo Yii::app()->urlManager->createUrl('admin/index'); ?>">Home admin</a></li>
    <li><a href="<?php echo Yii::app()->urlManager->createUrl('admin/buscadorEstudiantes'); ?>">Buscador</a></li>
    <li class="active">Perfil estudiante</li>
</ol>

    <div class="row">
        <div class="col-md-12 page-layout">

            <div class="panel panel-default">
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-8">
                            <div class="row">
                                <div class="col-md-6 text-md">
                                <fieldset>
                                    <legend>Datos Personales</legend>
                                    <ul class="">
                                        <?php if($user->image != ""){ ?>
                                            <img style='width:200px;height:200px' alt="<?php echo $user->image; ?>" src="<?php echo Yii::app()->request->baseUrl; ?>/images/users/<?php echo $user->image; ?>" />
                                        <?php } ?>
                                        <li>
                                            <strong><?php echo $user->getAttributeLabel('fullname')?>:</strong>
                                            <?php echo $user->fullname; ?>
                                        </li>
                                        <li>
                                            <strong><?php echo $user->getAttributeLabel('id_utype')?>:</strong>
                                            <?php echo $user->idUtype->na_utype; ?>
                                        </li>
                                        <li>
                                            <strong><?php echo $user->getAttributeLabel('create')?>:</strong>
                                            <?php echo OjalaUtils::convertDateFromDB($user->create, false); ?>
                                        </li>
                                        <li>
                                            <strong><?php echo $user->getAttributeLabel('id_user')?>:</strong>
                                            <?php echo $user->id_user; ?>
                                        </li>

                                        <?php if($user->nickname) :?>
                                            <li>
                                                <strong><?php echo $user->getAttributeLabel('nickname')?>:</strong>
                                                <?php echo $user->nickname; ?>
                                            </li>
                                        <?php endif; ?>
                                        <li>
                                            <strong><?php echo $user->getAttributeLabel('email1')?>:</strong>
                                            <a href="mailto:<?php echo $user->email1; ?>" title="Enviar un correo"><?php echo $user->email1; ?></a>
                                        </li>

                                        <?php if(!empty($user->email2)) :?>
                                            <strong><?php echo $user->getAttributeLabel('email2')?>:</strong>
                                            <?php echo $user->email2; ?></li>
                                        <?php endif; ?>

                                        <?php if(!empty($user->phone)) :?>
                                        <li>
                                            <strong><?php echo $user->getAttributeLabel('phone')?>:</strong>
                                            <?php echo CHtml::link($user->phone, 'skype:'. $user->phone. '?call'); ?>
                                        </li>
                                        <?php endif; ?>

                                        <?php if(!empty($user->id_service)): ?>
                                        <li>
                                            <strong><?php echo $user->getAttributeLabel('id_service')?>:</strong>
                                            <?php
                                                if($suscripcion)
                                                    $gatewayLink = Subscribers::getGatewayProfile($user->id_service, $suscripcion->id_service);
                                                else
                                                    $gatewayLink = NULL;

                                                if(empty($gatewayLink)) {
                                                    echo $user->id_service;
                                                } else {
                                                    echo CHtml::link($user->id_service, $gatewayLink, array('target'=>'_blank'));
                                                }
                                            ?>
                                        </li>
                                        <?php endif; ?>

                                        <?php if($amigo) :?>
                                        <li><strong>Amigo Referido: </strong> <?php echo $amigo->email1; ?></li>
                                        <?php endif; ?>
                                    </ul>

                                    <div class="spacer text-right">
                                        <a class="btn btn-warning btn-sm" href="<?php echo Yii::app()->urlManager->createUrl('admin/usuario', array('id'=>$user->id_user)); ?>">Editar Datos</a>     
                                        
                                        <?php if(Yii::app()->user->utype==='Administrador'):?>
                                            <a class="btn btn-default btn-sm" href="<?php echo Yii::app()->urlManager->createUrl('admin/detalleSuscriptores', array('busqueda'=>$user->email1)); ?>">Eventos</a>
                                        <?php endif; ?>
                                    </div>
                                </fieldset>
                                <hr>
                                    <?php if(Yii::app()->user->utype==='Administrador'){ ?>
                                        <div class="spacer">
                                            <p><strong>Cambiar a tipo de usuario:</strong></p>
                                            <div class="btn-group">
                                                <a class="btn btn-danger btn-xs" href="<?php echo Yii::app()->urlManager->createUrl('admin/cambiarTipoUsuario', array('id'=>$user->id_user, 'id_utype'=>'1')); ?>">Admin</a>
                                                <a class="btn btn-danger btn-xs" href="<?php echo Yii::app()->urlManager->createUrl('admin/cambiarTipoUsuario', array('id'=>$user->id_user, 'id_utype'=>'5')); ?>">SAC</a>
                                                <a class="btn btn-danger btn-xs" href="<?php echo Yii::app()->urlManager->createUrl('admin/cambiarTipoUsuario', array('id'=>$user->id_user, 'id_utype'=>'2')); ?>">Instructor</a>
                                                <a class="btn btn-danger btn-xs" href="<?php echo Yii::app()->urlManager->createUrl('admin/cambiarTipoUsuario', array('id'=>$user->id_user, 'id_utype'=>'3')); ?>">Estudiante</a>
                                            </div>
                                        </div>
                                    <?php } ?>

                                <hr>
                                    <?php if($user->idUtype->na_utype==='Estudiante' || $user->idUtype->na_utype==='Lead' || $user->idUtype->na_utype==='AAC'){ ?>
                                        <div class="spacer">
                                            <p><strong>Iniciar sesión:</strong></p>
                                            <div class="btn-group">
                                                <a class="btn btn-default" href="<?php echo Yii::app()->urlManager->createUrl('admin/loginuser', array('email'=>$user->email1)); ?>">Real del Usuario</a>
                                                <a class="btn btn-primary" href="<?php echo Yii::app()->urlManager->createUrl('admin/LoginEstudiante', array('email'=>$user->email1)); ?>">Simulado del Usuario</a>
                                            </div>
                                        </div>
                                    <?php } ?>
                                </div>
                                <div class="col-md-6">
                                    <legend>Suscripción Principal</legend>
                                    <?php if($suscripcion){ ?>
                                    <ul>
                                        <li>
                                            <strong><?php echo $suscripcion->getAttributeLabel('active') ?></strong>
                                            <?php
                                                if($suscripcion->active==2)
                                                    $alertClass = 'warning';
                                                elseif($suscripcion->active==1)
                                                    $alertClass = 'success';
                                                else
                                                    $alertClass = 'danger';
                                            ?>
                                            <span class="label label-<?php echo $alertClass ?>"><?php echo $suscripcion->activeStr ?></span>
                                        </li>
                                        <li>
                                            <strong><?php echo $suscripcion->getAttributeLabel('id_stype') ?>:</strong>
                                            <?php echo $suscripcion->type->na_stype; ?>
                                        </li>
                                        <li>
                                            <strong><?php echo $suscripcion->getAttributeLabel('date') ?>:</strong>
                                            <?php echo OjalaUtils::convertDateFromDB($suscripcion->date); ?>
                                        </li>
                                        <?php if($suscripcion->id_stype != SuscriptionType::SCHOLARSHIP && $suscripcion->id_stype != SuscriptionType::TWOBYONE ): ?>
                                            <li>
                                                <strong><?php echo $suscripcion->getAttributeLabel('gateway') ?>: </strong>
                                                <?php echo Subscribers::getGateway($suscripcion->id_service, $suscripcion->id_stype); ?>
                                            </li>
                                        <?php endif; ?>
                                        <li>
                                            <strong><?php echo $suscripcion->getAttributeLabel('exp_date') ?>:</strong>
                                            <?php echo OjalaUtils::convertDateFromDB($suscripcion->exp_date, false); ?>
                                        </li>
                                        <li><strong><?php echo $suscripcion->getAttributeLabel('with_downloads') ?>: </strong>
                                            <?php if($suscripcion->with_downloads == '1' || $suscripcion->with_downloads=='1'){ ?>
                                                <span class="label label-success">Si</span>
                                            <?php }else{ ?>
                                                <span class="label label-danger">No</span>
                                            <?php } ?>
                                        </li>
                                        <br>
                                        <form role="form" action="<?php echo Yii::app()->urlManager->createUrl('admin/materialDescarga'); ?>" method="GET">
                                            <input type="hidden" name="id" value="<?php echo $user->id_user; ?>" />
                                            <div class="btn-group">
                                                <button type="submit" class="btn btn-default <?php if(!Yii::app()->user->utype==='Administrador') echo 'btn-block' ?>">
                                                    Material Descarga
                                                </button>

                                                <?php 
                                                    if(Yii::app()->user->utype==='Administrador') {
                                                        echo CHtml::link(
                                                            'Editar', 
                                                            Yii::app()->urlManager->createUrl('admin/editarSuscripcion', array(
                                                                'id'=>$suscripcion->id_suscription,
                                                                'returnUrl' => Yii::app()->urlManager->createUrl('admin/estudiante/' . $_GET['id'])
                                                            )), 
                                                            array('class'=>'btn btn-warning')
                                                        );

                                                        if($suscripcion->active=='1'){
                                                            echo CHtml::link(
                                                                'Cancelar', 
                                                                Yii::app()->urlManager->createUrl('admin/cancelarSuscripcion', array('id'=>$suscripcion->id_suscription )), 
                                                                array('class'=>'btn btn-danger')
                                                            );
                                                        }
                                                        else {
                                                            echo CHtml::link(
                                                                'Activar', 
                                                                Yii::app()->urlManager->createUrl('admin/activarSuscripcion', array('id'=>$suscripcion->id_suscription)),
                                                                array('class'=>'btn btn-success')
                                                            );
                                                        }
                                                    } 
                                                ?>
                                            </div>
                                        </form>                                        

                                        <hr>
                                    </ul>
                                    <?php }else{ ?>
                                        <span class="label label-info">No tiene ninguna suscripción recurrente</span>
                                    <?php } ?>
                                </div>
                            </div>

                            <hr>

                            <div class="row">
                                <div class="col-md-12">
                                    <legend>Pagos</legend>
                                    <?php 
                                        $pays = 0;

                                        if(isset($user->suscripcion)){
                                            foreach($user->suscripcion as $sub):
                                                if(count($sub->pays)>0){ $pays++; 
                                            ?>
                                            <table class="table table-bordered table-striped">
                                                <thead>
                                                  <tr>
                                                    <th>ID</th>
                                                    <th>Fecha</th>
                                                    <th>Monto</th>
                                                    <th>Email</th>
                                                    <th>Metodo</th>
                                                    <th>Status</th>
                                                  </tr>
                                                </thead>
                                                <tbody>
                                                    <?php foreach($sub->pays as $pago) { ?>
                                                    <tr>
                                                        <td><?php echo $pago->id_pay ?></td>
                                                        <td><?php echo $pago->date ?></td>
                                                        <td><?php echo $pago->amount ?></td>
                                                        <td><?php echo $pago->email ?></td>
                                                        <td><?php echo $pago->na_ptype ?></td>
                                                        <td><?php echo $pago->status ?></td>
                                                    </tr>
                                                    <?php } ?>
                                                </tbody>
                                            </table>
                                        <?php 
                                                } 
                                            endforeach;
                                        }
                                         ?>

                                        <?php if(!$pays){ ?>
                                            <span class="label label-info">No se ha registrado ningún pago</span>
                                        <?php } ?>
                                    <hr>

                                    <legend>Diplomados</legend>
                                    <?php if( count($user->subDiplo) > 0 ){ ?>
                                    <table class="table table-bordered table-striped">
                                        <thead>
                                          <tr>
                                            <th>Diplomado</th>
                                            <th>Nivel Actual</th>
                                            <th></th>
                                          </tr>
                                        </thead>
                                        <tbody>
                                            <?php foreach($user->subDiplo as $gs) : ?>
                                            <tr>
                                                <td><?php echo $gs->group->nb_group; ?></td>
                                                <td><?php echo $gs->current_level; ?></td>
                                                <td>
                                                    <a type="button" class="btn btn-warning btn-xs nivel" data-nivel="<?php echo $gs->current_level; ?>" href="#">Cambiar Nivel</a>
                                                    <a type="button" class="btn btn-danger btn-xs eliminar" href="<?php echo Yii::app()->urlManager->createUrl('admin/desinscribirDiplomado', array('id'=>$gs->id_group_susc, 'idu'=>$user->id_user)); ?>">Eliminar</a>
                                                </td>
                                            </tr>
                                            <?php endforeach; ?>
                                        </tbody>
                                    </table>
                                    <?php }else{ ?>
                                        <span class="label label-info">No se ha inscrito en ningún diplomado</span>
                                    <?php } ?>

                                    <hr>

                                    <legend>Compras Realizadas</legend>
                                    <?php if(count($compras)>0){ ?>
                                    <table class="table table-bordered table-striped">
                                        <thead>
                                          <tr>
                                            <th>Curso</th>
                                            <th>Metodo</th>
                                            <th>Información</th>
                                          </tr>
                                        </thead>
                                        <tbody>
                                            <?php foreach($compras as $curso) { ?>
                                            <tr>
                                                <td><?php echo $curso['name']; ?></td>
                                                <td><?php echo $curso['na_stype']; ?></td>
                                                <td><?php echo $curso['description']; ?></td>
                                            </tr>
                                            <?php } ?>
                                        </tbody>
                                    </table>
                                    <?php }else{ ?>
                                        <span class="label label-info">No ha realizado ninguna compra</span>
                                    <?php } ?>
                                    
                                    <hr>

                                    <legend>Cursos Actuales</legend>
                                    <?php if(count($cursos)>0){ ?>
                                    <table class="table table-bordered table-striped">
                                        <thead>
                                          <tr>
                                            <th>Progreso</th>
                                            <th>Cetificado</th>
                                            <th>Curso</th>
                                            <th>Opciones</th>
                                          </tr>
                                        </thead>
                                        <tbody>
                                            <?php foreach($cursos as $curso) { ?>
                                            <tr>
                                                <td><span class="label label-<?php if($curso['progress']<90){ echo 'info'; }else{ echo 'success'; }?>"><?php if($curso['progress']==""){ echo '0'; }else{ echo $curso['progress']; } ?> %</span></td>
                                                <td>
                                                    <?php if($curso['progress']>90 && $curso['certificate']==""){ ?>
                                                    
                                                    <?php $this->widget('ext.EAjaxUpload.EAjaxUpload',
                                                    array(
                                                            'id'=>'uploadFile'.$curso['idcs'],
                                                            'config'=>array(
                                                                    'action'=>Yii::app()->createUrl('admin/subirPdf', array('id_csuscription'=>$curso['idcs'])),
                                                                    'allowedExtensions'=>array("pdf"),//array("jpg","jpeg","gif","exe","mov" and etc...
                                                                    'sizeLimit'=>10*1024*1024,// maximum file size in bytes
                                                                    'minSizeLimit'=>1*1024,// minimum file size in bytes
                                                                    'onComplete'=>"js:function(id, fileName, responseJSON){ $('#uploadFile".$curso['idcs']."').attr('style', 'display: none;'); $('#boton".$curso['idcs']."').attr('style', ''); }",
                                                                    //'messages'=>array(
                                                                    //                  'typeError'=>"{file} has invalid extension. Only {extensions} are allowed.",
                                                                    //                  'sizeError'=>"{file} is too large, maximum file size is {sizeLimit}.",
                                                                    //                  'minSizeError'=>"{file} is too small, minimum file size is {minSizeLimit}.",
                                                                    //                  'emptyError'=>"{file} is empty, please select files again without it.",
                                                                    //                  'onLeave'=>"The files are being uploaded, if you leave now the upload will be cancelled."
                                                                    //                 ),
                                                                    //'showMessage'=>"js:function(message){ alert(message); }"
                                                                )
                                                    )); ?>
                                                    <a type="button" id="boton<?php echo $curso['idcs']; ?>" class="btn btn-success btn-xs" href="<?php echo Yii::app()->request->baseUrl; ?>/certificados/Certificado<?php echo $curso['idcs']; ?>.pdf" style="display: none;">Certificado<?php echo $curso['idcs']; ?>.pdf</a>
                                                    
                                                    <?php }elseif($curso['certificate']!=""){ ?>
                                                        <a href="<?php echo Yii::app()->request->baseUrl; ?>/certificados/<?php echo $curso['certificate']; ?>" class="btn btn-success btn-xs"><?php echo $curso['certificate']; ?></a>
                                                        <!-- <a id="ac<?php echo $curso['idcs']; ?>" class="btn btn-primary btn-xs" href="<?php echo Yii::app()->urlManager->createUrl('admin/generarCertificado', array('idcs'=>$curso['idcs'], 'id'=>$user->id_user, 'guardar'=>'0')); ?>" >Abrir</a> -->
                                                    <?php }else{ ?>
                                                        <a type="button" class="btn btn-primary btn-xs" href="" disabled="disabled">No Disponible</a>
                                                    <?php } ?>
                                                    <a type="button" id="gc<?php echo $curso['idcs']; ?>" class="btn btn-success btn-xs" href="<?php echo Yii::app()->urlManager->createUrl('admin/generarCertificado', array('idcs'=>$curso['idcs'], 'id'=>$user->id_user, 'guardar'=>'1')); ?>" >Generar</a>
                                                    
                                                </td>
                                                <td><strong>Título:</strong> <?php echo $curso['name'].' </br> <strong>Clase:</strong> '.$curso['current']; ?></td>
                                                <td>
                                                    <?php if($curso['progress']=="" or $curso['progress']<10){ ?>
                                                    <a type="button" class="btn btn-danger btn-xs eliminar" href="<?php echo Yii::app()->urlManager->createUrl('admin/eliminarCs', array('idcs'=>$curso['idcs'], 'id'=>$user->id_user)); ?>">Eliminar</a>
                                                    <?php } ?>
                                                </td>
                                            </tr>
                                            <?php } ?>
                                        </tbody>
                                    </table>
                                    <?php }else{ ?>
                                        <span class="label label-info">No ha visto ningun curso</span>
                                    <?php } ?>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <?php if(empty($suscripcion) || (!empty($suscripcion) && $suscripcion->id_stype != SuscriptionType::SCHOLARSHIP)): ?>
                            <form role="form" action="<?php echo Yii::app()->urlManager->createUrl('admin/agregarSuscripcion'); ?>" method="GET">
                                <input type="hidden" name="id" value="<?php echo $user->id_user; ?>" />
                                <input type="hidden" name="id_stype" value="<?php echo SuscriptionType::SCHOLARSHIP; ?>" />
                                <button type="submit" class="btn btn-default btn-block">Activar Beca</button>
                            </form>

                            <hr>
                            <?php endif;?>

                            <?php if(empty($suscripcion) || (!empty($suscripcion) && $suscripcion->id_stype != SuscriptionType::TWOBYONE)): ?>
                            <form role="form" action="<?php echo Yii::app()->urlManager->createUrl('admin/agregarSuscripcion'); ?>" method="GET">
                                <input type="hidden" name="id" value="<?php echo $user->id_user; ?>" />
                                <input type="hidden" name="id_stype" value="<?php echo SuscriptionType::TWOBYONE; ?>" />
                                <button type="submit" class="btn btn-default btn-block">Activar 2x1</button>
                            </form>
                            
                            <hr>
                            <?php endif;?>
                            
                            <?php if($suscripcion) { ?>
                            <div class="panel panel-primary">
                                <div class="panel-heading">
                                    <h5>Cambiar Plan</h5>
                                </div>
                                <div class="panel-body">
                                    <form role="form" action="<?php echo Yii::app()->urlManager->createUrl('admin/CambiarPlan'); ?>" method="GET">
                                        <input type="hidden" name="id" value="<?php echo $suscripcion->id_suscription; ?>" />
                                        <div class="form-group">
                                            <?php 
                                                echo CHtml::dropDownList('id_stype', NULL, $listaPlanes, array(
                                                    'empty'=>'Seleccione un Diplomado',
                                                    'class'=>'combobox form-control',
                                                ));
                                            ?>
                                        </div>  
                                        <button type="submit" class="btn btn-default btn-block">Cambiar Plan</button>
                                    </form>
                                </div>
                            </div>
                            <?php } ?> 

                            <div class="panel panel-primary">
                                <div class="panel-heading">
                                    <h5>Inscribir en Diplomado</h5> 
                                </div>
                                <div class="panel-body">
                                    <form role="form" action="<?php echo Yii::app()->urlManager->createUrl('admin/inscribirDiplomado'); ?>" method="GET">
                                        <input type="hidden" name="dato" value="<?php echo $user->email1; ?>" />                                    
                                        <input type="hidden" name="idu" value="<?php echo $user->id_user; ?>" />
                                        <div class="form-group">
                                            <?php 
                                                echo CHtml::dropDownList('diplomado', NULL, $listaDiplomados, array(
                                                    'empty'=>'Seleccione un Diplomado',
                                                    'class'=>'combobox form-control',
                                                ));
                                            ?>
                                        </div>  
                                        <button type="submit" class="btn btn-default btn-block">Inscribir en este diplomado</button>
                                    </form>
                                </div>
                            </div>

                            <div class="panel panel-primary">
                                <div class="panel-heading">
                                    <h5>Agregar Curso (Con Descarga de Material)</h5> 
                                </div>
                                <div class="panel-body">
                                    <form role="form" action="<?php echo Yii::app()->urlManager->createUrl('admin/agregarCurso'); ?>" method="GET">
                                        <input type="hidden" name="id" value="<?php echo $user->id_user; ?>" />
                                        <input type="hidden" name="id_stype" value="1" />
                                        <div class="form-group">
                                        <?php 
                                            echo CHtml::dropDownList('id_course', NULL, $listaCursos, array(
                                                'empty'=>'Seleccione un Curso',
                                                'class'=>'combobox form-control',
                                            ));
                                        ?>
                                        </div>  
                                        <button type="submit" class="btn btn-default btn-block">Agregar Curso</button>
                                    </form>
                                </div>
                            </div>

                            <div class="panel panel-primary">
                                <div class="panel-heading">
                                    <h5>Agregar Compra (Con Descarga de Material)</h5> 
                                </div>
                                <div class="panel-body">
                                    <form role="form" action="<?php echo Yii::app()->urlManager->createUrl('admin/agregarCupon'); ?>" method="GET">
                                        <input type="hidden" name="id" value="<?php echo $user->id_user; ?>" />
                                        <input type="hidden" name="id_stype" value="1" />
                                        <div class="form-group">
                                        <?php 
                                            echo CHtml::dropDownList('id_course', NULL, $listaCursos, array(
                                                'empty'=>'Seleccione un Curso',
                                                'class'=>'combobox form-control',
                                            ));
                                        ?>
                                        </div>  
                                        <button type="submit" class="btn btn-default btn-block">Agregar Compra</button>
                                    </form>
                                </div>
                            </div>

                            <div class="panel panel-primary">
                                <div class="panel-heading">
                                    <h5>Agregar Groupon</h5> 
                                </div>
                                <div class="panel-body">
                                    <form role="form" action="<?php echo Yii::app()->urlManager->createUrl('admin/agregarCupon'); ?>" method="GET">
                                        <input type="hidden" name="id" value="<?php echo $user->id_user; ?>" />
                                        <input type="hidden" name="id_stype" value="15" />
                                        <div class="form-group">
                                            <input class="form-control" name="cupon" placeholder="Codigo Groupon">
                                            <?php 
                                                echo CHtml::dropDownList('id_course', NULL, $listaCursos, array(
                                                    'empty'=>'Seleccione un Curso',
                                                    'class'=>'combobox form-control',
                                                ));
                                            ?>
                                        </div>  
                                        <button type="submit" class="btn btn-default btn-block">Agregar Groupon</button>
                                    </form>
                                </div>
                            </div>

                            <div class="panel panel-primary">
                                <div class="panel-heading">
                                    <h5>Suscripciones compartidas</h5>  
                                </div>
                                <div class="panel-body">
                                    <form role="form" action="<?php echo Yii::app()->urlManager->createUrl('admin/AgregarAmigo'); ?>" method="GET">
                                        <input type="hidden" name="id" value="<?php echo $user->id_user; ?>" />
                                        <div class="form-group">
                                            <input class="form-control" name="email" placeholder="Correo del amigo">
                                        </div>  
                                        <button type="submit" class="btn btn-default btn-block">Añadir a esta suscripción</button>
                                    </form>
                                </div>
                            </div>

                            <div class="panel panel-primary">
                                <div class="panel-heading">
                                    <h5>Cuenta duplicada</h5>
                                    <p>Esta accion es irreversible y la cuenta que va a quedar activa es <b><?php echo $user->email1; ?></b>, la otra se va a eliminar y todos los cursos y progresos van a pasar a la cuenta principal.</p>  
                                </div>
                                <div class="panel-body">
                                    <form role="form" action="<?php echo Yii::app()->urlManager->createUrl('admin/CuentaDuplicada'); ?>" method="GET">
                                        <input type="hidden" name="id" value="<?php echo $user->id_user; ?>" />
                                        <div class="form-group">
                                            <input class="form-control" name="email" placeholder="Correo duplicado">
                                        </div>  
                                        <button type="submit" class="btn btn-default btn-block">Borrar esta cuenta</button>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>

        </div>
    </div>
<script>
    $(document).ready(function(){
        $('.nivel').click(function(e)
        {
            var nivel=$(this).data('nivel'),
                nuevo_nivel=prompt("Nuevo Nivel",nivel);

            e.preventDefault();

            if (nuevo_nivel != null) {
                //Llamar ajax
                alert(nuevo_nivel);
            }
        });

        $('.eliminar').click(function()
        {
            var r=confirm("¿Esta seguro de eliminar?");
            if (r==false) {
                return false;
            }
        });
    });
</script>
