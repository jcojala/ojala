<div class="page-header">
	<h1>Evento</h1>
</div>

<?php if(isset($email)){ ?>
<a class="btn btn-default" href="<?php echo Yii::app()->urlManager->createUrl('admin/detalleSuscriptores', array('busqueda'=>$email)) ?>">« Regresar Usuario</a>
<?php } ?>

<div class="row">
	<div class="container">
		<div class="col-md-6 col-md-offset-3 page-layout">

				<?php $form=$this->beginWidget('CActiveForm', array(
					'id'=>'register-form',
					'htmlOptions' => array('class'=>'form-horizontal'),
				)); ?>
				<?php if(isset($id)){ ?>

					<input type="hidden" name="id" value="<?php echo $id; ?>">
					<input type="hidden" class="form-control" name="email" id="inputEmail3" value="<?php echo $email; ?>" style="background-color: #eee;">
					<div class="form-group">
						<label for="inputEmail3" class="col-sm-2 control-label">Email</label>
						<div class="col-sm-10">
						  <input type="email" class="form-control" name="email" id="inputEmail3" value="<?php echo $email; ?>" disabled>
						</div>
					</div>

				<?php }else{ ?>

				<div class="form-group">
					<label for="inputEmail3" class="col-sm-2 control-label">Email</label>
					<div class="col-sm-10">
					  <input type="email" class="form-control" name="email" id="inputEmail3" placeholder="Email">
					</div>
				</div>

				<?php } ?>
				<div class="form-group">
					<label for="inputEmail3" class="col-sm-2 control-label">Fecha</label>
					<div class="col-sm-10">
						<?php
                            $this->widget('zii.widgets.jui.CJuiDatePicker',array(
                                'model'=> $model,
                                'attribute'=>'id_time',
                                'language'=>'es',
                                'options'=>array(        
                                    'showButtonPanel'=>true,
                                    'dateFormat'=>'dd/mm/yy'
                                ),
                                'htmlOptions'=>array(
                                    'id'=>'datetimepicker1',
                                    'class'=>'form-control'
                                ),
                            ));
                        ?>
					</div>
				</div>

		        <div class="form-group">
					<label for="inputEmail3" class="col-sm-2 control-label">Evento</label>
					<div class="col-sm-10">
			            	<select class="combobox form-control" name="id_event">
							<option value>Seleccione un Evento</option>
							<?php foreach ($list as $event){ ?>
								<?php if(isset($id) && $model->id_event==$event['id_event']){ ?>
									<option selected value="<?php echo $event['id_event']; ?>"><?php echo $event['na_event']; ?></option>
								<?php }else{ ?>
									<option value="<?php echo $event['id_event']; ?>"><?php echo $event['na_event']; ?></option>
								<?php } ?>
							<?php } ?>
			            	</select>
					</div>
		        </div>  

		        <div class="form-group">
					<label for="inputEmail3" class="col-sm-2 control-label">Tipo</label>
					<div class="col-sm-10">
			            	<select class="combobox form-control" name="id_ptype">
							<option value>Seleccione un Tipo</option>
							<?php foreach ($listtipo as $tipo){ ?>
								<?php if(isset($id) && $model->id_ptype==$tipo['id_ptype']){ ?>
									<option selected value="<?php echo $tipo['id_ptype']; ?>"><?php echo $tipo['na_ptype']; ?></option>
								<?php }else{ ?>
									<option value="<?php echo $tipo['id_ptype']; ?>"><?php echo $tipo['na_ptype']; ?></option>
								<?php } ?>
							<?php } ?>
			            	</select>
					</div>
		        </div>  

				<div class="row">
					<?php if(isset($id)){ ?>
						<div class="col-md-6">
			        		<a class="btn btn-block btn-danger" href="<?php echo Yii::app()->urlManager->createUrl('admin/eliminarEvento', array('id'=>$id, 'email'=>$email)) ?>" >Eliminar</a>
				        </div>  
					<?php } ?>
					<div class="col-md-6">
		        		<input type="submit" class="btn btn-block btn-default " value="Guardar" />
			        </div>  
			   	</div>  
			<?php $this->endWidget(); ?>
        </div>  
   	</div>  
</div>