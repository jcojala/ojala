<div class="page-header">
	<h1>Nuevos suscriptores</h1>
	<p>En este listado están todos los suscriptores en Oja.la.</p>
</div>

<ol class="breadcrumb" style="margin-top:-20px; margin-bottom:30px;">
    <li><a href="<?php echo Yii::app()->urlManager->createUrl('admin/index'); ?>">Home admin</a></li>
    <li class="active">Suscriptores</li>
</ol>

<?php if(!empty($msg)):?>
	<div class="alert <?php echo $msgType === 'error' ? 'alert-danger' : 'alert-success' ?>">
		<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
		<?php echo htmlspecialchars($msg) ?>
	</div>
<?php endif;?>

<h3>Filtrar fecha de suscripción</h3>

<?php
	$form = $this->beginWidget('CActiveForm', array(
		'method' => 'get',
		'htmlOptions'=>array('class'=>'form-horizontal', 'id'=>'js-search-form')
	));
?>
<div class="search-form">
	<div class="form-group col-date">
	<?php
		$this->widget('zii.widgets.jui.CJuiDatePicker',array(
			'model'=>$model,
			'attribute'=>'fromSubDate',
			'language'=>'es',
			'options'=>array(        
			    'showButtonPanel'=>true,
			    'dateFormat'=>'dd/mm/yy',
			    'maxDate'=>'0',
			),
			'htmlOptions'=>array(
			    'placeholder'=>'Desde',
			    'class'=>'form-control'
			),
		));
	 ?>
	</div>
	<div class="form-group col-date">
		<?php
		$this->widget('zii.widgets.jui.CJuiDatePicker',array(
			'model'=>$model,
			'attribute'=>'toSubDate',
			'language'=>'es',
			'options'=>array(        
			    'showButtonPanel'=>true,
			    'dateFormat'=>'dd/mm/yy',
			    'maxDate'=>'0',
			),
			'htmlOptions'=>array(
			    'placeholder'=>'Hasta',
			    'class'=>'form-control'
			),
		));
	 ?>
	</div>
		&nbsp;<?php echo CHtml::submitButton('Filtrar', array('class'=>'btn btn-primary'));?>
</div>

<div class="row">
	<div class="col-md-12">
	<?php
	$this->widget('zii.widgets.grid.CGridView', array(
    'id' => 'user-grid',
		'cssFile'=>'',
		'dataProvider'=>$model->getSqlDataProvider(),
		'filter'=>$model,
		'filterCssClass'=>'filter-wrapper',
		'afterAjaxUpdate' => 'reinstallDatePicker',
		'itemsCssClass'=>'admin-table table table-bordered table-hover',
		'columns' => array(
			array(
				'name'=>'fullname',
				'header'=>'Nombre completo',
				'value'=>'CHtml::link($data["fullname"], Yii::app()->createUrl("admin/estudiante",array("id"=>$data["id_user"])), array("target"=>"_blank"))',
				'type'=>'raw'
			),
			array(
				'name'=>'email1',
				'header'=>'Correo principal'
			),
			array(
				'name'=>'email2',
				'header'=>'Correo secundario'
			),
			array(
				'header'=>'Fecha de suscripción',
				'name'=>'subscriptionDate',
				'value'=>'OjalaUtils::convertDateFromDB($data["subscriptionDate"])',
				'htmlOptions'=>array('class'=>'text-center'),
				'filter'=> $this->widget('zii.widgets.jui.CJuiDatePicker', array(
					'model'=>$model, 
					'attribute'=>'subscriptionDate', 
					'language' => 'es',
					'htmlOptions' => array('id' => 'datepicker'),
					'defaultOptions' => array(
						'showOn' => 'focus', 
						'dateFormat' => 'dd/mm/yy',
					)
				), 
				true),
			),
			array(
				'header'=>'Pasarela',
				'filter'=>CHtml::activeDropDownList($model,'gateway', Subscribers::gatewayList(), array('class'=>'form-control')),
				'name'=>'id_service',
				'value'=>'Subscribers::getGateway($data["subIdService"], $data["id_stype"])',
				'htmlOptions'=>array('class'=>'text-center')
			),
			array(
				'header'=>'Estado',
				'name'=>'lastStatus',
				'filter'=>CHtml::activeDropDownList($model,'lastStatus', Status::listData('--Cualquiera--'), array('class'=>'form-control')),
				'value'=>'Subscribers::getSubscriptionState($data["active"], $data["lastStatus"], $data["lastStatusDate"])',
				'type'=>'html',
				'htmlOptions'=>array('class'=>'text-center')
			),
			array(
				'class'=>'zii.widgets.grid.CButtonColumn',
				'template'=>'{profile} {update}',
				'header'=>'Opción',
				'updateButtonUrl' => 'Yii::app()->urlManager->createUrl("admin/editarSuscripcion/".$data["id_suscription"])',
				'updateButtonLabel' => 'Editar',
				'updateButtonImageUrl' => '',
				'updateButtonOptions' => array('class'=>'btn btn-xs btn-primary'),
				'buttons'=>array(
					'profile'=> array
        	(
            'label'=>'Ver perfil',
            'options'=> array('class'=>'btn btn-xs btn-success','target'=>'_blank'),
            'imageUrl'=>false,
            'url'=>'Subscribers::getGatewayProfile($data["id_service"], $data["subIdService"])',
            'visible'=>'Subscribers::getGatewayProfile($data["id_service"], $data["subIdService"])!=""'
        	),
				)
			),
		)
	)); ?>
	</div>
</div>
<script>
	$(document).ready(function(){
		$('table tr').on('click', function(e){
			$(this).removeClass('selected');
			$(this).toggleClass('success');
		})
	});
</script>
<?php $this->endWidget()?>	

<?php
	Yii::app()->clientScript->registerScript('search', "
		$('#js-search-form').submit(function(e){
			e.preventDefault();
			$.fn.yiiGridView.update('user-grid', {
				data: $(this).serialize()
	        });
	        return false;
		});
");

Yii::app()->clientScript->registerScript('re-install-date-picker', "
	function reinstallDatePicker(id, data) {
		$('#datepicker').datepicker(
			jQuery.extend({showMonthAfterYear:false},jQuery.datepicker.regional['es'],{'dateFormat':'dd/mm/yy'}
		));
	}
");