<div class="page-header">
	<h1>Nuevo Pago</h1>
</div>

<a class="btn btn-default" href="<?php echo Yii::app()->urlManager->createUrl('admin/resumen') ?>">« Regresar al Reporte</a>

<div class="row">
	<div class="container">
		<div class="col-md-6 col-md-offset-3 page-layout">

			<?php $form=$this->beginWidget('CActiveForm', array(
				'id'=>'register-form',
				'htmlOptions' => array('class'=>'form-horizontal'),
			)); ?>

				<div class="form-group">
					<label for="inputEmail3" class="col-sm-2 control-label">Email</label>
					<div class="col-sm-10">
						<input type="email" class="form-control" name="email" id="inputEmail3" placeholder="Email">
					</div>
				</div>

				<div class="form-group">
					<label for="inputEmail3" class="col-sm-2 control-label">Monto</label>
					<div class="col-sm-10">
						<?php echo $form->textField($model, 'amount', array('class'=>'form-control')); ?>
					</div>
				</div>

				<div class="form-group">
					<label for="inputEmail3" class="col-sm-2 control-label">Fecha</label>
					<div class="col-sm-10">
						<?php
                            $this->widget('zii.widgets.jui.CJuiDatePicker',array(
                                'model'=>$model,
                                'attribute'=>'date',
                                'language'=>'es',
                                'options'=>array(        
                                    'showButtonPanel'=>true,
                                    'dateFormat'=>'dd/mm/yy'
                                ),
                                'htmlOptions'=>array(
                                    'id'=>'datetimepicker1',
                                    'placeholder'=>'Fecha del pago',
                                    'class'=>'form-control'
                                ),
                            ));
                        ?>
					</div>
				</div>

		        <div class="form-group">
					<label for="inputEmail3" class="col-sm-2 control-label">Tipo</label>
					<div class="col-sm-10">
			            	<select class="combobox form-control" name="id_ptype">
							<option value>Seleccione un Tipo</option>
							<?php foreach ($listtipo as $tipo){ ?>
								<?php if(isset($id) && $model->id_ptype==$tipo['id_ptype']){ ?>
									<option selected value="<?php echo $tipo['id_ptype']; ?>"><?php echo $tipo['na_ptype']; ?></option>
								<?php }else{ ?>
									<option value="<?php echo $tipo['id_ptype']; ?>"><?php echo $tipo['na_ptype']; ?></option>
								<?php } ?>
							<?php } ?>
			            	</select>
					</div>
		        </div>  

				<div class="row">
					<div class="col-md-6">
		        		<input type="submit" class="btn btn-block btn-default " value="Guardar" />
			        </div>  
			   	</div>  

			<?php $this->endWidget(); ?>
        </div>  
   	</div>  
</div>