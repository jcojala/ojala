<?php
    Yii::app()->clientScript->registerCssFile(Yii::app()->request->baseUrl . '/css/adicionales/jquery-ui.min.css','screen');
    Yii::app()->clientScript->registerCssFile(Yii::app()->request->baseUrl . '/css/adicionales/jquery-ui-timepicker-addon.css','screen');
    Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl . '/js/jquery-ui-timepicker-addon.js', CClientScript::POS_HEAD);
    Yii::app()->clientScript->registerCssFile(Yii::app()->request->baseUrl . '/css/adicionales/bootstrap-table.css','screen');
    Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl . '/js/bootstrap-table.js', CClientScript::POS_HEAD);
?>

<div class="page-header">
    <h1>Listado de Seguimiento de Estudiantes</h1>
<?php if(!isset($mode)){ ?>
    <?php if(isset($dato)){ ?>
        <p class="panel-title" style="font-size: 24px;">Resultados de <?php echo $dato; ?></p>
    <?php }else{ ?>
        <p class="panel-title" style="font-size: 24px;">Mas recientemente contactados o por contactar</p>
    <?php } ?>
<?php } ?>
</div>

<?php if(!isset($mode)){ ?>
<div class="row">
    <div class="col-md-2">
        <a class="btn btn-default" href="<?php echo Yii::app()->urlManager->createUrl('admin/index') ?>">« Regresar al Administrador</a>
    </div>
    <div class="col-md-4">
        <a class="btn btn-default" href="<?php echo Yii::app()->urlManager->createUrl('admin/seguimiento') ?>">Todos</a>
        <a class="btn btn-default" href="<?php echo Yii::app()->urlManager->createUrl('admin/seguimiento', array('asesor'=>'herna')) ?>">Hernan</a>
        <a class="btn btn-default" href="<?php echo Yii::app()->urlManager->createUrl('admin/seguimiento', array('asesor'=>'elena')) ?>">Elena</a>
        <a class="btn btn-default" href="<?php echo Yii::app()->urlManager->createUrl('admin/seguimiento', array('asesor'=>'benja')) ?>">Benjamin</a>
    </div>
    <div class="col-md-4">
        <a class="btn btn-default" href="<?php echo Yii::app()->urlManager->createUrl('admin/seguimiento', array('mode'=>'print')) ?>">Imprimir</a>
    </div>
</div>
<?php } ?>

<br>


<div class="row" id="rowbuscar">
    <div class="col-md-12">
        <?php if(count($listaEstudiantes)>0){ ?>
        <table data-toggle="table" data-show-refresh="true" data-show-toggle="true" data-show-columns="true" data-search="true" class="table table-bordered table-hover" <?php if(isset($mode)){ ?> style="font-size: 18px; border: 1px solid black;" <?php } ?> >
            <thead>
              <tr <?php if(isset($mode)){ ?> style="font-size: 18px; border: 1px solid black;" <?php } ?>>
                <th data-sortable="true">Numero</th>
                <th data-sortable="true">Fecha de Registro</th>
                <th data-sortable="true">Nombre</th>
                <th data-sortable="true">Teléfono</th>
                <?php if(!isset($mode)){ ?>
                <th data-sortable="true" style="width: 215px;">Editar</th>
                <?php } ?>
                <th data-sortable="true">Email</th>
                <?php if(!isset($mode)){ ?>
                <th data-sortable="true">Servicios</th>
                <?php } ?>
                <th data-sortable="true">Check</th>
                <th data-sortable="true">Última Vez Visto</th>
                <th data-sortable="true">Último Contacto</th>
                <th data-sortable="true">Próximo Contacto</th>
                <th data-sortable="true">Evento Recurrente</th>
              </tr>
            </thead>
            <tbody>
              <?php $i=0; foreach($listaEstudiantes as $estudiante) { 
                $i++;
                $user=User::model()->findbyPk($estudiante['id_user']);
                if(strlen($estudiante['phone'])>9 && strlen($estudiante['phone'])<12){
                    $region=Region::model()->findbyPk($user->id_region);
                    if($region)
                    {
                        $estudiante['phone'] = '+' . $region->country->phone_code.' '.$user->phone;
                        $user->phone = '+' . $region->country->phone_code.' '.$user->phone;
                        $user->update();
                    }
                } ?>
              <tr>
              <td <?php if(isset($mode)){ ?> style="font-size: 18px; border: 1px solid black;" <?php } ?>>
              <?php echo $i; ?>
              <?php if($estudiante['newsletter']==""){ ?>
                    <span style="display: none;" class="label label-success" id="ids<?php echo $estudiante['id_user']; ?>"></span>
                    <select class="form-control combo" idu="<?php echo $estudiante['id_user']; ?>">
                        <option selected>Seleccione</option>
                        <option>benjamin</option>
                        <option>elena</option>
                        <option>hernan</option>
                        <option>*cancelar</option>
                    </select>
                <?php }else{ ?>
                    <span class="label label-success" id="ids<?php echo $estudiante['id_user']; ?>"><?php echo $estudiante['newsletter']; ?></span>
                    <select class="form-control combo" idu="<?php echo $estudiante['id_user']; ?>">
                        <option selected>Seleccione</option>
                        <option>benjamin</option>
                        <option>elena</option>
                        <option>hernan</option>
                        <option>*cancelar</option>
                    </select>
                <?php } ?>
              </td>
                <td <?php if(isset($mode)){ ?> style="font-size: 18px; border: 1px solid black;" <?php } ?>><?php echo $estudiante['date']; ?></td>
                <td <?php if(isset($mode)){ ?> style="font-size: 18px; border: 1px solid black;" <?php } ?>><?php echo $estudiante['name'] . ' ' .  $estudiante['lastname']; if(isset($user->region)){ echo ' / '.$user->region->country->name.' - '.$user->region->name; } ?></td>
                <td <?php if(isset($mode)){ ?> style="font-size: 18px; border: 1px solid black;" <?php } ?>><a href="skype:<?php echo $estudiante['phone']; ?>?call"><?php echo $estudiante['phone']; ?></a></td>
                <?php if(!isset($mode)){ ?>
                <td <?php if(isset($mode)){ ?> style="font-size: 18px; border: 1px solid black;" <?php } ?>>
                    <form class="form-inline" method="get" action="<?php echo Yii::app()->urlManager->createUrl('admin/editarTelefono'); ?>">
                        <input type="hidden" name="id" value="<?php echo $estudiante['id_user']; ?>"/>
                        <input class="form-control bfh-phone" type="text" name="phone" style="width: 70%;" data-format="+dd (ddd) ddd-dddd" value="<?php echo $estudiante['phone']; ?>"/>
                        <input class="btn btn-warning" type="submit" value="Editar"/>
                    </form>
                </td>
                <?php } ?>
                <td <?php if(isset($mode)){ ?> style="font-size: 18px; border: 1px solid black;" <?php } ?>>
                    <a type="button" href="<?php echo Yii::app()->urlManager->createUrl('admin/perfil', array('id'=>$estudiante['id_user'])); ?>">
                    <?php echo $estudiante['email1']; if(isset($estudiante['email2']) && $estudiante['email2']!=""){ echo ' ('.$estudiante['email2'].')'; } ?>
                    </a>
                </td>
                <?php if(!isset($mode)){ ?>
                <td <?php if(isset($mode)){ ?> style="font-size: 18px; border: 1px solid black;" <?php } ?>>
                    <a target="_blank" href="https://app.intercom.io/apps/wmlo15pq/users/<?php echo OjalaUtils::getIntercomUrl($estudiante['email1']); ?>">Intercom</a>
                </td>
                <?php } ?>
                <?php $valor = User::lastLogin($estudiante['email1']); ?>
                <td <?php if(isset($mode)){ ?> style="font-size: 18px; border: 1px solid black;" <?php } ?>> <?php if(strstr($valor, '*')!=null){ echo "X"; } ?></td>
                <td <?php if(isset($mode)){ ?> style="font-size: 18px; border: 1px solid black;" <?php } ?>><?php echo $valor; ?></td>

                <td <?php if(isset($mode)){ ?> style="font-size: 18px; border: 1px solid black;" <?php } ?>>
                    <?php if($estudiante['text']=="Contesto"){ ?>
                        <span class="label label-success"><?php echo $estudiante['text']; ?></span>
                    <?php }elseif($estudiante['text']=="No Contesto"){ ?>
                        <span class="label label-danger"><?php echo $estudiante['text']; ?></span>
                    <?php }elseif($estudiante['text']!=""){ ?>
                        <span class="label label-default"><?php echo $estudiante['text']; ?></span>
                    <?php } echo ' '.Dates::FechaRelativa(User::lastContact($estudiante['id_user'])); ?>
                    <span style="display: none;" class="label label-success" id="idpu<?php echo $estudiante['id_user']; ?>"></span>
                    <select class="form-control contesto" idp="<?php echo $estudiante['id_user']; ?>">
                        <option selected>Seleccione</option>
                        <option>Contesto</option>
                        <option>No Contesto</option>
                        <option>No Se LLamo</option>
                    </select>
                </td>

                <td <?php if(isset($mode)){ ?> style="font-size: 18px; border: 1px solid black;" <?php } ?>>
                    <?php $proximo = User::nextContact($estudiante['id_user']);
                    if($proximo!="No Existe"){ ?>
                    <span id="idpf<?php echo $estudiante['id_user']; ?>"><?php echo $proximo; ?></span>
                    <?php }else{ ?>
                    <span style="display: none;" id="idpf<?php echo $estudiante['id_user']; ?>"></span>
                    <?php } ?>
                    <input class="form-control fecha" name="fecha" id="idf<?php echo $estudiante['id_user']; ?>"/>
                    <a class="btn boton" usuario="<?php echo $estudiante['id_user']; ?>">Agendar en Calendar</a>
                </td>
                <td <?php if(isset($mode)){ ?> style="font-size: 18px; border: 1px solid black;" <?php } ?>>
                    <?php $proximo = User::nextContact($estudiante['id_user']);
                    if($proximo!="No Existe"){ ?>
                    <span id="idpf2<?php echo $estudiante['id_user']; ?>"><?php echo $proximo; ?></span>
                    <?php }else{ ?>
                    <span style="display: none;" id="idpf2<?php echo $estudiante['id_user']; ?>"></span>
                    <?php } ?>
                    <input class="form-control fecha" name="fecha" id="idf2<?php echo $estudiante['id_user']; ?>"/>
                    <a class="btn boton2" usuario="<?php echo $estudiante['id_user']; ?>">Agendar en Calendar</a>
                </td>
              </tr>
              <?php } ?>
            </tbody>
        </table>
        <?php } ?>
    </div>
</div>
<script type="text/javascript">
    $(function() {
        $('.combo').change(function(event)
        {
            var valor=$(this).attr('value');
            var id=$(this).attr('idu');
            if(valor!="Seleccione")
            {
                $.ajax({
                    'url': '<?php echo Yii::app()->urlManager->createUrl('admin/guardarSeguidor'); ?>?valor='+valor+'&id='+id,
                    'cache': false,
                    success: function (data)
                    {
                        $('#ids'+id).attr('style','');
                        $('#ids'+id).html(data);
                    },
                });
            }
        });

        $('.contesto').change(function(event)
        {
            var valor=$(this).attr('value');
            var id=$(this).attr('idp');
            if(valor!="Seleccione")
            {
                $.ajax({
                    'url': '<?php echo Yii::app()->urlManager->createUrl('admin/guardarEvento'); ?>?valor='+valor+'&id='+id,
                    'cache': false,
                    success: function (data)
                    {
                        $('#idpu'+id).attr('style','');
                        $('#idpu'+id).html(data);
                    },
                });
            }
        });

        $('.boton').click(function(event) 
        {
            var id=$(this).attr('usuario');
            var fe=$('#idf'+id).attr('value');
            $.ajax({
                'url': '<?php echo Yii::app()->urlManager->createUrl('admin/AgregarCalendar'); ?>?fecha='+fe+'&id_user='+id,
                'cache': false,
                success: function (data)
                {
                    $('#idpf'+id).attr('style','');
                    $('#idpf'+id).html(fe);
                    window.open(data, '_blank');
                },
            });
        });

        $('.boton2').click(function(event) 
        {
            var id=$(this).attr('usuario');
            var fe=$('#idf2'+id).attr('value');
            $.ajax({
                'url': '<?php echo Yii::app()->urlManager->createUrl('admin/AgregarCalendar'); ?>?fecha='+fe+'&id_user='+id,
                'cache': false,
                success: function (data)
                {
                    $('#idpf2'+id).attr('style','');
                    $('#idpf2'+id).html(fe);
                    window.open(data, '_blank');
                },
            });
        });

        $('.fecha').datetimepicker({
            dateFormat: 'yy-mm-dd'
        });
    });
</script>
