<div class="page-header">
	<h1>Suscripciones y Compras</h1>
	<?php if(isset($dato)){ ?>
		<p class="panel-title" style="font-size: 24px;">Resultados de <?php echo $dato; ?></p>
	<?php }else{ ?>
		<p class="panel-title" style="font-size: 24px;">Últimos 50 registros</p>
	<?php } ?>
</div>

<form class="form-inline" role="form">
	<div class="form-group col-xs-4">
		<label class="sr-only" for="exampleInputEmail2">Buscar por email</label>
		<input type="text" class="form-control" placeholder="Email" name="dato" value="<?php if(isset($dato)){echo $dato;} ?>">
	</div>
	<button type="submit" class="btn btn-default">Buscar</button>
</form>

<br>

<div class="row">
	<div class="col-md-12">
        <table class="table table-bordered table-hover">
            <thead>
              <tr>
            	<th>Cliente</th>
            	<th>Correo</th>
              	<th>Tipo Suscripcion</th>
              	<th>Fecha Suscripcion</th>
              	<th>Estado</th>
              	<th></th>
              </tr>
            </thead>
            <tbody>
				<?php foreach($suscripciones as $suscripcion) { ?>
				<tr>
					<td><?php echo $suscripcion['name']; ?></td>
					<td><?php echo $suscripcion['email1']; ?></td>
					<td><?php echo $suscripcion['na_stype']; ?></td>
					<td><?php echo $suscripcion['date']; ?></td>
					<?php if($suscripcion['active']=="1"){ ?>
						<td><span class="label label-success">Activo</span></td>
					<?php }else{ ?>
						<td><span class="label label-warning">No Activo</span></td>
					<?php } ?>
					<td>
						<a type="button" class="btn btn-primary btn-xs" href="<?php echo Yii::app()->urlManager->createUrl('admin/estudiante', array('id'=>$suscripcion['id_user'])); ?>">Detalles</a>
					</td>
				</tr>
				<?php } ?>
            </tbody>
      	</table>
		</div>
</div>