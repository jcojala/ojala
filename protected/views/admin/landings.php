<div class="page-header">
	<h1>Listado de Landings</h1>
	<p class="panel-title" style="font-size: 24px;">Listado de landings</p>
</div>

<div class="row">
	<form class="form-inline">
	    <div class="col-md-4">
	        <a class="btn btn-default" href="<?php echo Yii::app()->urlManager->createUrl('admin/index') ?>">« Regresar al Administrador</a>
	    </div>
		<div class="form-group col-md-2 col-md-offset-6">
			<a class="btn btn-success btn-block" disabled="disabled" href="<?php echo Yii::app()->urlManager->createUrl('admin/crearDiplomado'); ?>">Nuevo Landing</a>
		</div>
	</form>
</div>

<br>

<div class="row">
	<div class="col-md-12">
        <table class="table table-bordered table-hover">
            <thead>
              <tr>
            	<th style="width: 33%;">Titulo</th>
            	<th style="width: 33%;">Vencimiento</th>
              	<th style="width: 33%;">Opciones</th>
              </tr>
            </thead>
			<tbody>
				<?php foreach($list as $landing){ ?>
				<tr>
					<td><a href="<?php echo Yii::app()->urlManager->createUrl('l/'.substr($landing['landing'], 0, -4)) ?>" target="_blank"><?php echo substr($landing['landing'], 0, -4); ?></a></td>
					<td class="text-center"><?php echo substr(OjalaUtils::convertDateFromDB($landing['date']),0,10); ?></td>
					<td>
						<form action="<?php echo Yii::app()->urlManager->createUrl('admin/GuardarFechaLanding') ?>" method="get">
							<div class="col-md-4">
								<?php
									$this->widget('zii.widgets.jui.CJuiDatePicker',array(
										'name'=>'fecha',
										'language'=>'es',
										'options'=>array(        
										    'showButtonPanel'=>true,
										    'dateFormat'=>'dd/mm/yy'
										),
										'htmlOptions'=>array(
										    'placeholder'=>'Fecha',
										    'class'=>'form-control'
										),
									));
								 ?>
							</div>
							<input type="hidden" name="archivo" value="<?php echo $landing['landing']; ?>.txt" />
							<input type="submit" class="btn btn-warning editarslug" value="Guardar" />
						</form>
					</td>
				</tr>
				<?php } ?>
			</tbody>
		</table>
	</div>
</div>