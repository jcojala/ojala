<?php
	Yii::app()->clientScript->registerCssFile(Yii::app()->baseUrl . '/css/jquery.tagsinput.css','screen');
    	Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl . '/js/scholarships/jquery.tagsinput.min.js', CClientScript::POS_END);
?>
<div class="page-header">
	<h1><?php echo $model->isNewRecord ? 'Crear' : 'Edita' ?> la campaña</h1>
	<p>
		Completa la información de éste formulario para la <?php echo $model->isNewRecord ? 'creación' : 'edición' ?> 
		de una campaña
	</p>
</div>

<div class="row">
    <div class="col-md-12">
	<a class="btn btn-default" style="" href="<?php echo Yii::app()->urlManager->createUrl('admin/campanas'); ?>">« Listado de Campañas</a>
    </div>
</div>

<br>

<div class="row">
	<div class="col-md-12 page-layout">
		<?php $form=$this->beginWidget('CActiveForm', array(
			'id'=>'register-form',
			'enableClientValidation'=> true,
			'htmlOptions' => array('class'=>'form-horizontal'
			),
		)); ?>

		<?php echo $form->errorSummary($model); ?>

		<fieldset>
			<legend>Datos de la campaña</legend>
			
			<div class="row">
				<div class="form-group">
					<?php echo $form->label($model,'name', array('class'=>'col-lg-3')) ?>
					<div class="col-lg-9">
						<?php echo $form->textField($model,'name', array('class'=>'form-control')); ?>
						<?php echo $form->error($model,'name'); ?>
					</div>
				</div>
			</div>

		</fieldset>
		<fieldset>
			<legend>Cursos asociados</legend>

			<?php 
				$model->printCourseField($form, $this, 'courseBasicName');
				$model->printCourseField($form, $this, 'courseMediumName');
				$model->printCourseField($form, $this, 'courseAdvancedName');
			?>
		</fieldset>

		<?php 
			$i=0;

			//Agrega una pregunta vacía para usar como plantilla para clonarla
			if(count($model->qstnArray) == 1){
				$templateQuestion = new CmpQuestion;
				array_splice($model->qstnArray, count($model->qstnArray), 0, array($templateQuestion));
			}

			foreach($model->qstnArray as $question): 
				$answersArray =  array();
		?>
			<fieldset <?php echo $i === 0 ? 'id="question-template" class="hide"' : '' ?>>
				<legend>Pregunta #<?php echo $i ?></legend>
				<div class="question-wrapper">
					<div class="row">
						<div class="form-group">
							<?php echo $form->label($question,'question', array('class'=>'col-lg-3 qstnLabel', 'for'=>'qstn-'.$i)) ?>
							<div class="col-lg-9">
								<?php echo CHtml::textField("question[$i][question]", $question->question, array('class'=>'form-control qstnInput', 'id'=>'qstn-'.$i)); ?>
								<?php echo $form->error($question,"question"); ?>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="form-group">
							<?php echo $form->label($question,'answers', array('class'=>'col-lg-3')) ?>
							<div class="col-lg-9 checkbox-wrapper">
								<?php
									echo CHtml::checkBoxList(
										"question[$i][answersArray]",
										$question->answersArray,
										CHtml::listData($answers, 'id_answer','value'),
										array('container'=>'')
									);
									echo $form->error($question,'answers');
								?>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="form-group">
							<?php echo $form->label($question,'tagString', array('class'=>'col-lg-3 tagLabel', 'for'=>'tag-'.$i)) ?>
							<div class="col-lg-7">
								<?php echo CHtml::textField("question[$i][tagString]", $question->tagString, array('class'=>'form-control tagInput', 'id'=>'tag-'.$i)); ?>
								<?php echo $form->error($question,"tagString"); ?>
							</div>
						</div>
					</div>
					<div class="col-lg-offset-3">
						<?php echo CHtml::link('Eliminar esta pregunta', '#', array('class'=>'btn btn-danger del-question')); ?>
					</div>
				</div>
				<?php echo CHtml::hiddenField("question[$i][id_question]",$question->id_question) ?>
			</fieldset>
			<?php $i++; ?>
		<?php endforeach; ?>
		<div class="btn-wrapper">
			<input class="btn btn-success btn-lg col-lg-offset-3" type="submit" value="Guardar Campaña">
			<a href="#" id="add-question" class="btn btn-lg btn-info">
				<i class="glyphicon glyphicon-plus"></i>Agregar una nueva pregunta
			</a>
			<a class="btn btn-default btn-lg" style="" href="<?php echo Yii::app()->urlManager->createUrl('admin/campanas'); ?>">
				Regresar
			</a>
		</div>

		<?php $this->endWidget(); ?>
						
	</div>
</div>
<script>
	var formCampaign = {
		applyTagsPlugin: function(selector){
			$(selector).tagsInput({
				width: '100%',
				height: '45px',
				defaultText: ''
			});
		}
	};
	$(document).ready(function(){
		
		formCampaign.applyTagsPlugin('.tagInput:visible');

		$(document).on('click', '.del-question', function(){
			$(this).parents('fieldset').fadeOut('fast', function(){
				$(this).remove();
			})
		});

		$('#add-question').on('click', function(){
			var html = $('#question-template').clone().wrap('<div>').parent().html(),
				number = $('.question-wrapper').length;
			
			html = html.replace('id="question-template"', '');
			html = html.replace('#0', '#' + number);
			html = html.replace('-0', '-' + number);
			html = html.replace('[0]', '[' + number + ']');
			
			$('.btn-wrapper').before(html);
			$('#register-form fieldset:last').hide().removeClass('hide').slideDown('slow', function(){
				formCampaign.applyTagsPlugin('#register-form fieldset:last .tagInput');
			})
		});
	});
</script>

