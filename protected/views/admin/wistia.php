<div class="row">
	<div class="container">
		<div class="col-md-12 page-layout">

			<div class="panel panel-default">
	      		<div class="panel-heading">
	      			<h2 class="panel-title" style="font-size: 24px;">Cursos en Wistia</h2>
	      		</div>
      			<div class="panel-body">
        			<div class="row">
          				<div class="col-md-12">
					        <table class="table table-bordered" style="margin: 30px 0 20px 20px; width: 98%">
					            <thead>
					              <tr>
					            	<th>Curso</th>
					              	<th>Clases</th>
					              	<th>Duración</th>
					            	<th>WistiaID</th>
					              </tr>
					            </thead>
					            <tbody>
									<?php foreach($cursos as $curso) { ?>
									<tr>
										<td><?php echo $curso['curso']; ?></td>
										<td><?php echo $curso['clases']; ?></td>
										<td><?php echo Dates::secondsToTime($curso['duracion']); ?></td>
										<td><?php echo $curso['wistiaid']; ?></td>
									</tr>
									<?php } ?>
					            </tbody>
				          </table>
	          			</div>
	        		</div>
	      		</div>
	    	</div>

		</div>
	</div>
</div>