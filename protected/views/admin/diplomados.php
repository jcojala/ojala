<div class="page-header">
	<h1>Listado de Diplomados</h1>
	<p class="panel-title" style="font-size: 24px;">Listado y editor de diplomados. </p>
</div>

<ol class="breadcrumb" style="margin-top:-20px; margin-bottom:30px;">
  <li><a href="<?php echo Yii::app()->urlManager->createUrl('admin/index'); ?>">Home admin</a></li>
  <li class="active">Diplomados</li>
</ol>

<div class="row">
  <div class="content-fluid">

    <div class="col-md-9">
      <form role="form">
        <div class="form-group col-md-8">
          <label class="sr-only" for="exampleInputEmail2"></label>
          <input type="text" class="form-control input-lg " placeholder="Nombre del diplomado" name="dato" value="<?php if(isset($dato)){echo $dato;} ?>">
        </div>
        <button type="submit" class="btn btn-primary btn-lg">Buscar</button>
      </form>
    </div>
    
    <div class="col-md-3">
      <a class="btn btn-success btn-lg" href="<?php echo Yii::app()->urlManager->createUrl('admin/crearDiplomado'); ?>">Nuevo diplomado</a>
    </div>
  
  </div>
</div>

<br>

<div class="row">
	<div class="col-md-12">

        <table class="table table-bordered table-hover">
            <thead>
              <tr>
            	<th>Titulo</th>
              	<th style="width: 20%;">Opciones</th>
              </tr>
            </thead>
			<tbody>
				<?php foreach($list as $diplomado){ ?>
				<tr>
					<td><?php echo $diplomado['nb_group']; ?></td>
					<td style="text-align: center;">
						<a type="button" class="btn btn-primary btn-xs" href="<?php echo Yii::app()->urlManager->createUrl('admin/diplomado', array('id'=>$diplomado['id_group'])) ?>">Editar</a>
						<a type="button" class="btn btn-danger btn-xs eliminar" href="<?php echo Yii::app()->urlManager->createUrl('admin/eliminarDiplomado', array('id'=>$diplomado['id_group'])) ?>">Eliminar</a>
					</td>
				</tr>
				<?php } ?>
			</tbody>
		</table>
	</div>
</div>
<script type="text/javascript">
	$('.eliminar').click(function(event) 
    {
        var r=confirm("Esta seguro de eliminar este Diplomado?\n\nEsta accion va a desinscribir a todos los estudiantes que esten inscritos en el diplomado");
		if (r==false)
		{
			return false;
		}
    });
</script>