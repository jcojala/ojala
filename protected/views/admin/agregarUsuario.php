<div class="page-header">
	<h1>Usuario</h1>
	<p class="panel-title" style="font-size: 24px;">Completa la información de éste formulario para la inclusión de un nuevo usuario. </p>
</div>

<div class="row">
    <div class="col-md-12">
        <a class="btn btn-default" href="<?php echo Yii::app()->urlManager->createUrl('admin/index') ?>">« Regresar al Administrador</a>
    </div>
</div>

<br>

<div class="row">
	<div class="container user">

		<div class="col-md-12 page-layout">
			<div class="div">

				<?php $form=$this->beginWidget('CActiveForm', array(
					'id'=>'register-form',
					'htmlOptions' => array('class'=>'form-horizontal','enctype'=>'multipart/form-data'),
				)); ?>

					<?php if($user->id_user!=""){ ?>
					<input type="hidden" name="id_user" value="<?php echo $user->id_user; ?>" />
					<?php } ?>

			        <div class="form-group">
						<label class="col-sm-3 control-label" for="id_cathegory">Tipo de usuario</label>
						<div class="col-lg-9">
				            	<select class="combobox form-control" name="id_utype">
								<option value>Seleccione el tipo de usuario</option>
								<?php foreach ($types as $type){ ?>
										<option value="<?php echo $type['id_utype']; ?>"><?php echo $type['na_utype']; ?></option>
								<?php } ?>
				            	</select>
						</div>
			        </div> 	

					<div class="form-group">
						<label class="col-lg-3" for="course_name">Correo electrónico:</label>
						<div class="col-lg-7">
							<?php echo $form->textField($user,'email1', array('class'=>'form-control')); ?>
							<?php echo $form->error($user,'email1', array('style'=>'color: red;')); ?>
						</div>
					</div>					

					<div class="form-group">
						<label class="col-lg-3" for="course_name">Nickname:</label>
						<div class="col-lg-7">
							<?php echo $form->textField($user,'nickname', array('class'=>'form-control')); ?>
							<?php echo $form->error($user,'nickname', array('style'=>'color: red;')); ?>
						</div>
					</div>

					<div class="form-group">
						<label class="col-lg-3" for="course_name">Nombre:</label>
						<div class="col-lg-7">
							<?php echo $form->textField($user,'name', array('class'=>'form-control')); ?>
							<?php echo $form->error($user,'name', array('style'=>'color: red;')); ?>
						</div>
					</div>

					<div class="form-group">
						<label class="col-lg-3" for="course_name">Apellido:</label>
						<div class="col-lg-7">
							<?php echo $form->textField($user,'lastname', array('class'=>'form-control')); ?>
							<?php echo $form->error($user,'lastname', array('style'=>'color: red;')); ?>
						</div>
					</div>										
					<?php if($user->id_user!=""){ ?>
					<input class="btn btn-success btn-lg col-lg-offset-3" type="submit" value="Agregar usuario">
					<?php }else{?>
					<input class="btn btn-success btn-lg col-lg-offset-3" type="submit" value="Actualizar usuario">					
					<?php } ?>
				<?php $this->endWidget(); ?>

			</div>
		</div>
	</div>

</div>