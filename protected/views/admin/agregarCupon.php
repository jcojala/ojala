<div class="row">
	<div class="container">
		<div class="col-md-12 page-layout clearfix">
			
			<div class="page-header">
				<h2>Agregando curso a un estudiante vía Groupon</h2>
			</div>
			<p style="text-align:center; margin:0 0 50px 0;">Este formulario valida si el correo está ya registrado, si responde con q no está registrado <a href="https://oja.la/?utm_source=Groupon&utm_medium=Gmail&utm_term=periodismo-digital&utm_campaign=Registro">registrarlo en este enlace.</a></p>
			<form class="col-md-5 col-md-offset-3" action="<?php echo Yii::app()->urlManager->createUrl('admin/agregarCupon'); ?>" method="post">
				
				<div class="form-group">
					<label for="exampleInputEmail1">Email del estudiante:</label>
					<input type="text" Placeholder="Correo" name="correo" class="form-control" />
				</div>

				<div class="form-group">
					<select class="form-control" name="curso">
						<?php foreach ($list as $item) { ?>
							<option value="<?php echo $item['id_course']; ?>"><?php echo $item['name']; ?></option>
						<?php } ?>
					</select>
				</div>

				<div class="form-group">
					<input type="submit" class="btn btn-primary" value="Agregar curso a este estudiante" />
				</div>
		<?php if(Yii::app()->user->hasFlash('mensaje')){ ?>
			<div class="alert alert-success">
			<?php echo Yii::app()->user->getFlash('mensaje'); ?>
			</div>
		<?php } ?>
	</form>
		</div>
	</div>
</div>
