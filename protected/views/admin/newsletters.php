
<div class="page-header">
	<h1>Crear Newsletter</h1>
	<p>
		Completa la información de éste formulario para la de un newsletter
	</p>
</div>

<div class="row">
    <div class="col-md-10">
		<a class="btn btn-default" style="" href="<?php echo Yii::app()->urlManager->createUrl('admin/index'); ?>">« Administrador</a>
    </div>
    <div class="col-md-2">
		<a class="btn btn-info" style="" href="<?php echo Yii::app()->urlManager->createUrl('admin/newsletters'); ?>">Limpiar Formulario</a>
    </div>
</div>

<br>

<div class="row">
	<div class="col-md-12 page-layout">
		<legend>Template del Newsletter</legend>
		<div class="row">
			<div class="col-lg-12">
				<label>Templates</label>
	            <select class="combobox form-control" id="template" name="template">
	                <option value>Seleccione un Template</option>
	                <?php foreach ($listaTemplates as $template){ ?>
	                	<option value="<?php echo $template['file']; ?>"><?php echo $template['file']; ?></option>
	                <?php } ?>
	            </select>
        	</div>
		</div>

		<legend>Datos del Newsletter</legend>
		<div class="row">
			<div class="col-lg-12">
				<label>Curso</label>
	            <select class="combobox form-control" id="id_course" name="id_course">
	                <option value>Seleccione un Curso</option>
	                <?php foreach ($listaCursos as $curso){ ?>
	                	<option value="<?php echo $curso['id_course']; ?>"><?php echo $curso['name']; ?></option>
	                <?php } ?>
	            </select>
        	</div>
		</div>

        <?php foreach($listaTemplates as $template){ ?>
			<textarea style="display: none;" id="<?php echo str_replace('.','',$template['file']); ?>" class="form-control" rows="5" id="generado" name="html" style="display: none"><?php echo $template['content']; ?></textarea>
        <?php } ?>

        <?php foreach($listaCursos as $curso){ ?>
        	<p style="display: none;" class="<?php echo $curso['cslug']; ?> cursoposible" id="curso<?php  echo $curso['id_course']; ?>" curso="<?php echo $curso['name']; ?>" descr="<?php echo $curso['description']; ?>"  slug="https://oja.la<?php echo Yii::app()->urlManager->createUrl('site/curso', array('slug'=>$curso['slug'], 'cat'=>$curso['cslug'])); ?>" image="<?php echo Yii::app()->request->baseUrl; ?>/images/covers/<?php echo substr($curso['cover'], 0, -4).'medio.jpg'; ?>" imagee="<?php echo Yii::app()->request->baseUrl; ?>/images/covers/<?php echo substr($curso['cover'], 0, -4).'small.jpg'; ?>" imageee="<?php echo substr($curso['cover'], 0, -4).'medio.jpg'; ?>" imageeee="<?php echo substr($curso['cover'], 0, -4).'small.jpg'; ?>"></p>
        <?php } ?>

		<div class="row">
			<div class="col-lg-2">
				<label>Utms</label>
        	</div>
		</div>

		<div class="row">
			<div class="col-lg-2">
				<input type="text" value="" class="col-xs-3 form-control" id="utm_source" name="utm_source" placeholder="utm_source"/>
        	</div>
			<div class="col-lg-2">
				<input type="text" value="" class="col-xs-3 form-control" id="utm_medium" name="utm_medium" placeholder="utm_medium" />
			</div>
			<div class="col-lg-2">
				<input type="text" value="" class="col-xs-3 form-control" id="utm_term" name="utm_term" placeholder="utm_term" />
			</div>
			<div class="col-lg-2">
				<input type="text" value="" class="col-xs-3 form-control" id="utm_content" name="utm_content" placeholder="utm_content" />
			</div>
			<div class="col-lg-2">
				<input type="text" value="" class="col-xs-3 form-control" id="utm_campaign" name="utm_campaign" placeholder="utm_campaign" />
        	</div>
		</div>

		<div class="row" style="display: none;">
			<div class="col-lg-12">
				<label>HTML Newsletter</label>
				<textarea class="form-control" name="html" rows="5" id="html"></textarea>
			</div>
		</div>

		<div class="row">
			<div class="col-lg-12">
				<a class="btn btn-default btn-block" id="actualizar" style="margin: 10px 0;">Crear Newsletter</a>
			</div>
		</div>

		<div id="boton" class="row" style="display: none;">
			<div class="col-lg-12">
				<p style="text-align: center;">Abre el correo en una nueva ventana, para descargar click derecho y "Guardar Página como"</p>
				<form target="_blank" method="post" action="<?php echo Yii::app()->urlManager->createUrl('admin/newsletter'); ?>">
					<textarea class="form-control" id="imagenes" name="imagenes" style="display: none"></textarea>
					<textarea class="form-control" rows="5" id="generado" name="html" style="display: none"></textarea>
					<input type="submit" class="btn btn-success btn-block" style="margin: 10px 0;" value="Descargar"/>
				</form>
			</div>
		</div>

		<div id="preview" style="display: none;">
			<p style="text-align: center;">Preview del Correo</p>
	      	<div id="Displaytitle" style="margin: 10px 0; border-color: black; "></div>
	    </div>

    </div>
</div>

<script type="text/javascript">
$(document).ready(function()
{
	$("#actualizar").click(update);
});
     
function update()
{
	var seleccionado='#'+$('#template').val().replace(".", "");
	//alert(seleccionado);
	$('#html').val($(seleccionado).val());

	$('#preview').slideDown('slow');
	$('#boton').slideDown('slow');

	var html = $('#html').val();
	var id_curso = $('#id_course').val();
	if(id_curso!="")
	{
		var curso = $('#curso' + id_curso).attr('curso'),
		descr = $('#curso' + id_curso).attr('descr'),
		slug  = $('#curso' + id_curso).attr('slug');

		var cat = "." + $('#curso' + id_curso).attr('class').split(" ")[0];
		$('#curso' + id_curso).attr('class', '');
	    var randomCursos = $(cat).get().slice(0, 10);
	}
	else
	{
		var curso = "",
			descr = "",
			slug = "";
		var randomCursos = $('.cursoposible').get().slice(0, 10);
	}

	var resultado=html.replace(new RegExp("#curso#", "g") , curso)
		.replace(new RegExp("#imagen#", "g") , $('#curso' + id_curso).attr('image'))
		.replace(new RegExp("#descripcion#", "g") , descr)
		.replace(new RegExp("#slug#", "g") , slug.replace(new RegExp(" ", "g"), "%20"))
		.replace(new RegExp("#sugerencia1#", "g") , $(randomCursos[1]).attr('curso'))
		.replace(new RegExp("#sugerencia2#", "g") , $(randomCursos[2]).attr('curso'))
		.replace(new RegExp("#sugerencia3#", "g") , $(randomCursos[3]).attr('curso'))
		.replace(new RegExp("#sugerencia4#", "g") , $(randomCursos[4]).attr('curso'))
		.replace(new RegExp("#sugerencia5#", "g") , $(randomCursos[5]).attr('curso'))
		.replace(new RegExp("#sugerencia6#", "g") , $(randomCursos[6]).attr('curso'))
		.replace(new RegExp("#sugerencia7#", "g") , $(randomCursos[7]).attr('curso'))
		.replace(new RegExp("#sugerencia8#", "g") , $(randomCursos[8]).attr('curso'))
		.replace(new RegExp("#sugerencia9#", "g") , $(randomCursos[9]).attr('curso'))
		.replace(new RegExp("#sugerenciaimagen1#", "g") , $(randomCursos[1]).attr('imagee'))
		.replace(new RegExp("#sugerenciaimagen2#", "g") , $(randomCursos[2]).attr('imagee'))
		.replace(new RegExp("#sugerenciaimagen3#", "g") , $(randomCursos[3]).attr('imagee'))
		.replace(new RegExp("#sugerenciaimagen4#", "g") , $(randomCursos[4]).attr('imagee'))
		.replace(new RegExp("#sugerenciaimagen5#", "g") , $(randomCursos[5]).attr('imagee'))
		.replace(new RegExp("#sugerenciaimagen6#", "g") , $(randomCursos[6]).attr('imagee'))
		.replace(new RegExp("#sugerenciaimagen7#", "g") , $(randomCursos[7]).attr('imagee'))
		.replace(new RegExp("#sugerenciaimagen8#", "g") , $(randomCursos[8]).attr('imagee'))
		.replace(new RegExp("#sugerenciaimagen9#", "g") , $(randomCursos[9]).attr('imagee'))
		.replace(new RegExp("#sugerenciaslug1#", "g") , $(randomCursos[1]).attr('slug'))
		.replace(new RegExp("#sugerenciaslug2#", "g") , $(randomCursos[2]).attr('slug'))
		.replace(new RegExp("#sugerenciaslug3#", "g") , $(randomCursos[3]).attr('slug'))
		.replace(new RegExp("#sugerenciaslug4#", "g") , $(randomCursos[4]).attr('slug'))
		.replace(new RegExp("#sugerenciaslug5#", "g") , $(randomCursos[5]).attr('slug'))
		.replace(new RegExp("#sugerenciaslug6#", "g") , $(randomCursos[6]).attr('slug'))
		.replace(new RegExp("#sugerenciaslug7#", "g") , $(randomCursos[7]).attr('slug'))
		.replace(new RegExp("#sugerenciaslug8#", "g") , $(randomCursos[8]).attr('slug'))
		.replace(new RegExp("#sugerenciaslug9#", "g") , $(randomCursos[9]).attr('slug'))
		.replace(new RegExp("#utms#", "g") , "?utm_source="+$('#utm_source').val()+"&utm_medium="+$('#utm_medium').val()+"&utm_term="+$('#utm_term').val()+"&utm_content="+$('#utm_content').val()+"&utm_campaign="+$('#utm_campaign').val());

	var resultado2=html.replace(new RegExp("#curso#", "g") , curso)
		.replace(new RegExp("#imagen#", "g"), $('#curso' + id_curso).attr('imageee'))
		.replace(new RegExp("#descripcion#", "g") , descr)
		.replace(new RegExp("#slug#", "g") , slug.replace(new RegExp(" ", "g"), "%20"))
		.replace(new RegExp("#sugerencia1#", "g") , $(randomCursos[1]).attr('curso'))
		.replace(new RegExp("#sugerencia2#", "g") , $(randomCursos[2]).attr('curso'))
		.replace(new RegExp("#sugerencia3#", "g") , $(randomCursos[3]).attr('curso'))
		.replace(new RegExp("#sugerencia4#", "g") , $(randomCursos[4]).attr('curso'))
		.replace(new RegExp("#sugerencia5#", "g") , $(randomCursos[5]).attr('curso'))
		.replace(new RegExp("#sugerencia6#", "g") , $(randomCursos[6]).attr('curso'))
		.replace(new RegExp("#sugerencia7#", "g") , $(randomCursos[7]).attr('curso'))
		.replace(new RegExp("#sugerencia8#", "g") , $(randomCursos[8]).attr('curso'))
		.replace(new RegExp("#sugerencia9#", "g") , $(randomCursos[9]).attr('curso'))
		.replace(new RegExp("#sugerenciaimagen1#", "g") , $(randomCursos[1]).attr('imageeee'))
		.replace(new RegExp("#sugerenciaimagen2#", "g") , $(randomCursos[2]).attr('imageeee'))
		.replace(new RegExp("#sugerenciaimagen3#", "g") , $(randomCursos[3]).attr('imageeee'))
		.replace(new RegExp("#sugerenciaimagen4#", "g") , $(randomCursos[4]).attr('imageeee'))
		.replace(new RegExp("#sugerenciaimagen5#", "g") , $(randomCursos[5]).attr('imageeee'))
		.replace(new RegExp("#sugerenciaimagen6#", "g") , $(randomCursos[6]).attr('imageeee'))
		.replace(new RegExp("#sugerenciaimagen7#", "g") , $(randomCursos[7]).attr('imageeee'))
		.replace(new RegExp("#sugerenciaimagen8#", "g") , $(randomCursos[8]).attr('imageeee'))
		.replace(new RegExp("#sugerenciaimagen9#", "g") , $(randomCursos[9]).attr('imageeee'))
		.replace(new RegExp("#sugerenciaslug1#", "g") , $(randomCursos[1]).attr('slug'))
		.replace(new RegExp("#sugerenciaslug2#", "g") , $(randomCursos[2]).attr('slug'))
		.replace(new RegExp("#sugerenciaslug3#", "g") , $(randomCursos[3]).attr('slug'))
		.replace(new RegExp("#sugerenciaslug4#", "g") , $(randomCursos[4]).attr('slug'))
		.replace(new RegExp("#sugerenciaslug5#", "g") , $(randomCursos[5]).attr('slug'))
		.replace(new RegExp("#sugerenciaslug6#", "g") , $(randomCursos[6]).attr('slug'))
		.replace(new RegExp("#sugerenciaslug7#", "g") , $(randomCursos[7]).attr('slug'))
		.replace(new RegExp("#sugerenciaslug8#", "g") , $(randomCursos[8]).attr('slug'))
		.replace(new RegExp("#sugerenciaslug9#", "g") , $(randomCursos[9]).attr('slug'))
		.replace(new RegExp("#utms#", "g") , "?utm_source="+$('#utm_source').val()+"&utm_medium="+$('#utm_medium').val()+"&utm_term="+$('#utm_term').val()+"&utm_content="+$('#utm_content').val()+"&utm_campaign="+$('#utm_campaign').val());

	$('#Displaytitle').html(resultado);
	$('#imagenes').val(
		$('#curso' + id_curso).attr('imageee')+"&"+
		$(randomCursos[1]).attr('imageeee')+"&"+
		$(randomCursos[2]).attr('imageeee')+"&"+
		$(randomCursos[3]).attr('imageeee')+"&"+
		$(randomCursos[4]).attr('imageeee')+"&"+
		$(randomCursos[5]).attr('imageeee')+"&"+
		$(randomCursos[6]).attr('imageeee')+"&"+
		$(randomCursos[7]).attr('imageeee')+"&"+
		$(randomCursos[8]).attr('imageeee')+"&"+
		$(randomCursos[9]).attr('imageeee')
	);
	$('#generado').val(resultado2);
}

</script>
<br>
<br>