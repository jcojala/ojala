<div class="page-header">
	<h1>Editar Usuario</h1>
	<p>Formulario de edicion de los datos de un usuario. </p>

</div>

<div class="row">
    <div class="col-md-12">
		<a class="btn btn-default" style="" href="<?php echo Yii::app()->urlManager->createUrl('admin/estudiante', array('id'=>$user->id_user)); ?>">« Regresar a la Ficha</a>
    </div>
</div>

<br>
<div class="row">
		<div class="col-md-12">

			<?php $form=$this->beginWidget('CActiveForm', array(
				'id'=>'register-form',
				'htmlOptions' => array('class'=>'form-horizontal','enctype'=>'multipart/form-data'),
			)); ?>
				<input type="hidden" name="id" value="<?php echo $user->id_user; ?>" />
				<div class="form-group">
					<?php echo $form->label($user,'nickname', array('class'=>'col-lg-3  control-label')) ?>
					<div class="col-lg-2">
						<?php echo $form->textField($user,'nickname', array('size'=>'500', 'class'=>'form-control')); ?>
						<?php echo $form->error($user,'nickname'); ?>
					</div>
				</div>

				<div class="form-group">
					<?php echo $form->label($user,'name', array('class'=>'col-lg-3  control-label')) ?>
					<div class="col-md-4">
						<?php echo $form->textField($user,'name', array('size'=>'10', 'class'=>'form-control')); ?>
						<?php echo $form->error($user,'name'); ?>
					</div>
					<?php echo $form->label($user,'lastname', array('class'=>'col-lg-1  control-label')) ?>
					<div class="col-md-4">
							<?php echo $form->textField($user,'lastname', array('rows'=>'10', 'class'=>'form-control')); ?>
							<?php echo $form->error($user,'lastname'); ?>
					</div>
				</div>

				<div class="form-group">
					<?php echo $form->label($user,'email1', array('class'=>'col-lg-3  control-label')) ?>
					<div class="col-lg-4">
						<?php echo $form->textField($user,'email1', array('rows'=>'10', 'class'=>'form-control')); ?>
						<?php echo $form->error($user,'email1'); ?>
					</div>
				</div>

				<div class="form-group">
					<?php echo $form->label($user,'email2', array('class'=>'col-lg-3  control-label')) ?>
					<div class="col-lg-4">
						<?php echo $form->textField($user,'email2', array('rows'=>'10', 'class'=>'form-control')); ?>
						<?php echo $form->error($user,'email2'); ?>
					</div>
				</div>

				<div class="form-group">
					<?php echo $form->label($user,'phone', array('class'=>'col-lg-3  control-label')) ?>
					<div class="col-lg-2">
						<?php echo $form->textField($user,'phone', array('rows'=>'10', 'class'=>'form-control')); ?>
						<?php echo $form->error($user,'phone'); ?>
					</div>
				</div>

				<div class="form-group">
					<?php echo $form->label($user,'id_service', array('class'=>'col-lg-3  control-label')) ?>
					<div class="col-lg-2">
						<?php echo $form->textField($user,'id_service', array('rows'=>'10', 'class'=>'form-control')); ?>
						<?php echo $form->error($user,'id_service'); ?>
					</div>
				</div>

				<hr>

				<div class="text-align: center;">
					<input class="btn btn-warning btn-lg btn-block" type="submit" value="Guardar">
				</div>

			<?php $this->endWidget(); ?>
	</div>
</div>