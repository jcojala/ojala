<div class="page-header">
	<h1>Usuarios del Sistema</h1>
	<p class="panel-title" style="font-size: 24px;">Listado y editor de Usuarios del Backend. </p>
</div>

<div class="row">
	<form class="form-inline">
	    <div class="col-md-4">
	        <a class="btn btn-default" href="<?php echo Yii::app()->urlManager->createUrl('admin/index') ?>">« Regresar al Administrador</a>
	    </div>
		<div class="form-group col-md-3">
			<input class="form-control" id="exampleInputEmail2" placeholder="Filtrar usuario" name="dato" value="<?php if(isset($dato)){echo $dato;} ?>">
		</div>
		<div class="form-group col-md-3">
			<button type="submit" class="btn btn-default">Buscar</button>
		</div>
		<div class="form-group col-md-2">
			<a class="btn btn-success btn-block" href="<?php echo Yii::app()->urlManager->createUrl('admin/agregarUsuario'); ?>">Agregar Usuario</a>
		</div>
	</form>
</div>

<br>

<div class="row">
	<div class="col-md-12">

        <table class="table table-bordered table-hover">
            <thead>
              <tr>
            	<th>Tipo de usuario</th>
            	<th>Email</th>            	
            	<th>Nickname</th>
            	<th>Nombre</th>
            	<th>Fecha de registro</th>            	            	            	
              	<th style="width: 20%;">Opciones</th>
              </tr>
            </thead>
			<tbody>
				<?php foreach($listUsers as $admin){ ?>
				<tr>
					<td><?php echo $admin['na_utype']; ?></td>
					<td><?php echo $admin['email1']; ?></td>
					<td><?php echo $admin['nickname']; ?></td>
					<td><?php echo $admin['name']." ".$admin['lastname']; ?></td>
					<td><?php echo $admin['create']; ?></td>
					<td style="text-align: center;">
						<a type="button" class="btn btn-primary btn-xs" href="<?php echo Yii::app()->urlManager->createUrl('admin/agregarUsuario', array('id'=>$admin['id_user'])) ?>">Editar</a>
						<a type="button" class="btn btn-danger btn-xs eliminar" href="<?php echo Yii::app()->urlManager->createUrl('admin/eliminarUsuario', array('id'=>$admin['id_user'])) ?>">Eliminar</a>
					</td>
				</tr>
				<?php } ?>			
			</tbody>
		</table>
	</div>
</div>
<script type="text/javascript">
	$('.eliminar').click(function(event) 
    {
        var r=confirm("¿Esta seguro de eliminar este Usuario?");
		if (r==false)
		{
			return false;
		}
    });
</script>