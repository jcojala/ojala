<div class="page-header">
	<h1>Trending de Cursos</h1>
	<p class="panel-title" style="font-size: 24px;">Listado de Cursos mas Populares y Cursos que estan Trending.</p>
</div>

<a class="btn btn-default" href="<?php echo Yii::app()->urlManager->createUrl('admin/index') ?>">« Regresar al Administrador</a>

<br>

<div class="row">
	<div class="col-md-12">
		<h2>Cursos en Trending</h2>
		<p>Listado basado en el acceso a los curso durante el último mes</p>
		<table class="table table-bordered" style="margin: 30px 0 20px 20px; width: 98%">
			<thead>
			  <tr>
				<th>Categoria</th>
				<th>Curso</th>
			  	<th>Fecha Publicación</th>
			  	<th>Estudiantes Activo</th>
			  </tr>
			</thead>
			<tbody>
				<?php foreach($list1 as $curso) { ?>
				<tr>
					<td><?php echo $curso['cat']; ?></td>
					<td><?php echo $curso['name']; ?></td>
					<td><?php echo $curso['publish']; ?></td>
					<td><?php echo $curso['inscritos']; ?></td>
				</tr>
				<?php } ?>
			</tbody>
		</table>
		<h2>Cursos Populares</h2>
		<p>Listado basado en el acceso tanto de estudiantes a ver el curso como de vitantes que revisaron el contenido del curso</p>
		<table class="table table-bordered" style="margin: 30px 0 20px 20px; width: 98%">
			<thead>
			  <tr>
				<th>Categoria</th>
				<th>Curso</th>
			  	<th>Fecha Publicación</th>
			  	<th>Personas Interesadas</th>
			  </tr>
			</thead>
			<tbody>
				<?php foreach($list2 as $curso) { ?>
				<tr>
					<td><?php echo $curso['cat']; ?></td>
					<td><?php echo $curso['name']; ?></td>
					<td><?php echo $curso['publish']; ?></td>
					<td><?php echo $curso['interesados']; ?></td>
				</tr>
				<?php } ?>
			</tbody>
		</table>
		<h2>Cursos con mas Inscritos</h2>
		<p>Listado basado el numero de estudiantes que se han inscrito el curso</p>
		<table class="table table-bordered" style="margin: 30px 0 20px 20px; width: 98%">
			<thead>
			  <tr>
				<th>Categoria</th>
				<th>Curso</th>
			  	<th>Fecha Publicación</th>
			  	<th>Inscripciones</th>
			  </tr>
			</thead>
			<tbody>
				<?php foreach($list3 as $curso) { ?>
				<tr>
					<td><?php echo $curso['cat']; ?></td>
					<td><?php echo $curso['name']; ?></td>
					<td><?php echo $curso['publish']; ?></td>
					<td><?php echo $curso['inscritos']; ?></td>
				</tr>
				<?php } ?>
			</tbody>
		</table>
	</div>
</div>