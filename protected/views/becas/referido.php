<h2 class="text-center">
	¡<?php echo $user->fullname ?> te ha invitado a suscribirte a Oja.la!
</h2>
<h4 class="text-center">Ofrecemos cursos desde cero con clases en video que te ayudarán a mejorar tus habilidades técnicas y creativas.</h4>
<p>
	Con esta invitación, al <a href="<?php echo Yii::app()->createUrl('suscripciones') ?>" title="Ver los planes disponibles" >suscribirte a cualquiera de nuestros planes</a>
	en Oja.la, les obsequiamos tanto a ti como a tu amigo(a): <strong>1 mes adicional</strong> 
	sin costo alguno. Además, también puedes <a href="<?php echo Yii::app()->createUrl('becas/index') ?>" title="Invita a tus amigos">invitar a tus contactos</a> 
	a participar en esta promoción y seguir recibiendo meses adicionales hasta completar un año. 
</p>

<div class="row">
		<div class="text-center">
			<a href="<?php echo Yii::app()->urlManager->createUrl('site/suscripciones'); ?>" class="btn btn-lg btn-success btn-suscripciones">
				¡Suscríbete ahora y recibe un mes adicional gratis!
			</a>
		</div>
	</div>
</div>

<div class="content">
	<h3>Las suscripciones incluyen ...</h3>
	<div class="row">
		<div class="col-md-6">
			<div class="each">
				<h4>Acceso ilimitado</h4>
				<p>Acceso a más de 3000 video clases, 100 programas de estudio, las 24 horas del día, todos los dias de la semana.</p>
			</div>
			<div class="each">
				<h4>Material de descarga</h4>
				<p>Las suscripciones incluyen acceso al material usado por los instructores, códigos los proyectos y material de apoyo.</p>
			</div>
			<div class="each">
				<h4>Acompañamiento total</h4>
				<p>Vía chat, mail o skype, podrás compartir no solo con instructores sino estudiantes por medio de los foros y salas de chats.</p>
			</div>
		</div>
		<div class="col-md-6">
			<div class="each">
				<h4>Clases paso a paso</h4>
				<p>Aprenderás de clases desde cero hasta experto, paso a paso aprenderás sobre web, iphone apps y más, mucho más.</p>
			</div>
			<div class="each">
				<h4>Variedad de temas</h4>
				<p>Podrás aprender sobre ofimática, desarrollo web, mercadeo, socialmedia, desarrollo para iPhone, Android, diseño.</p>
			</div>
			<div class="each">
				<h4>100% a tu propio tiempo</h4>
				<p>Tendrás acceso 24hrs del día, 7 dias a la semana, sin ningún tipo de restricción, incluso desde cualquier dispositivo.</p>
			</div>
		</div>
	</div>
</div>

