<?php
    Yii::app()->clientScript->registerCssFile(Yii::app()->baseUrl . '/css/jquery.tagsinput.css','screen');
    Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl . '/js/scholarships/jquery.tagsinput.min.js', CClientScript::POS_END);
    Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl . '/js/scholarships/ZeroClipboard.min.js', CClientScript::POS_END);
    Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl . '/js/scholarships/scholarships.js', CClientScript::POS_END);
    Yii::app()->clientScript->registerScript('ZeroClipboardIinit', 
        "var client = new ZeroClipboard( document.getElementById('btnCopy'), {
            moviePath: '" . Yii::app()->request->baseUrl . "/js/scholarships/ZeroClipboard.swf'
        });"
    , CClientScript::POS_END);
    Yii::app()->clientScript->registerScript('FacebookSDK', 
        "window.fbAsyncInit = function() {
            FB.init({
                appId      : '${fbAppId}',
                status     : true,
                xfbml      : true
            });
        };
        (function(d, s, id){
            var js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id)) {return;}
            js = d.createElement(s); js.id = id;
            js.src = '//connect.facebook.net/en_US/all.js';
            fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));"
    , CClientScript::POS_END);


?>
<div class="becas">
    <div class="page-header">
        <h1>Obtén una beca en Oja.la</h1>
        <p>Consigue hasta un año de becas invitando a amigos</p>
    </div>

    <div class="row">
        <div class="col-md-12">
    <?php    if($totalReferrals <= User::MAX_SCHOLARSHIP_MONTHS) : ?>
            <p>
                Por cada contacto que se suscriba a <?php echo CHtml::link('Oja.la', Yii::app()->baseUrl) ?>, 
                les regalaremos a ambos 1 mes de suscripción hasta un máximo de 1 año.
            </p>
            <p>
                ¡Es fácil de enviar invitaciones! Simplemente escoge tu medio preferido:
            </p>
    <?php else: ?>
        <h2 class="text-center">¡Gracias por la cantidad de suscriptores invitados!</h2>
        <p>
            ¡Felicitaciones por lograr obtener un año de becas en Oja.la! 
            Lamentablemente, <strong>por ahora no podrás recibir mas meses de becas</strong> pero puedes
            seguir invitando a tus contactos a unirse a la gran comunidad de Oja.la.
        </p>
    <?php endif; ?>
        </div>
    </div>
    <div class="alert-wrapper">
        <div id="alert-bar" class="alert alert-success hidden">
            <?php if(isset($errorMessage)) echo $errorMessage ?>
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        </div>
        <?php if(isset($errorMessage)): ?>
            <div id="alert-bar" class="alert alert-danger ">
                <?php echo $errorMessage ?>
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            </div>
        <?php endif;?>
    </div>
    <div class="row">
        <div class="col-md-12">
            <h2>Enviando un correo</h2>

            <?php if($this->hasCommonEmail()) : ?>
                <div class="col-md-6" >
                    Proveedor
                </div>
            <?php endif; ?>
            <div class="<?php echo $this->hasCommonEmail() ? 'col-md-6' : 'col-md-6 col-md-offset-3' ?>" >
                <form id="mail-form" method="post" class="form-vertical">
                        <input type="hidden" id="mailLink" name="mailLink" value="<?php echo $this->getShareLink('email') ?>">
                        <div class="form-group">
                            <label for="address">Invita tus amigos(as) por su correo</label> <em>(separados por coma)</em>
                            <div class="col-lg">
                                <textarea name="emails" id="emails" class="form-control"></textarea>
                                <div class="errorMessage text-center hidden"></div>
                            </div>
                        </div>
                        <button id="sendEmails" class="btn btn-primary col-md-12" data-url="<?php echo $this->createUrl('becas/sendInvitations') ?>">
                            <i class="glyphicon glyphicon-envelope"></i>
                            <span>Enviar</span>
                        </button>
                </form>
            </div>

        </div>
    </div>
    <div class="row alternative-share">
        <div class="col-md-12 text-center">
            <h2>Otras alternativas</h2>
            <p>Comparte el enlace a través de tus redes sociales y gana becas con cada nuevo suscriptor referido.</p>
        </div>

        <div class="row">
           <div class="col-md-4 global col-md-offset-4">
                 <div class="input-group">
                    <span class="input-group-btn">
                        <button id="btnCopy" class="btn btn-info" data-clipboard-text="<?php echo $this->getShareLink('global') ?>">
                            <i class="glyphicon glyphicon-link"></i> Copiar el enlace
                        </button>
                    </span>
                    <input 
                        type="text" 
                        name="shareLink" 
                        id="shareLink" 
                        class="form-control" 
                        readonly="readonly" 
                        value="<?php echo $this->getShareLink('global') ?>"
                        style="font-size: 0.7em"                  
                    >
               </div>
           </div>
        </div>
        <div class="row">
            <div class="col-md-2 text-center col-md-offset-4">
                   <button 
                        class="btn btn-info facebook"
                        data-text="Te invitamos a obtener una beca de 1 mes, al suscribirte a cualquiera de nuestros planes."
                        data-url="<?php echo $this->getShareLink('facebook') ?>"
                   >
                        <span class="fb-logo">Compartir en Facebook</span>
                    </button>
            </div>
            <div class="col-md-2 text-center">
                   <button 
                    class="btn btn-info twitter" 
                    data-text="Te invito a obtener gratis un mes adicional en Oja.la al suscribirte desde el siguiente enlace"
                    data-url="<?php echo urlencode($this->getShareLink('twitter')) ?>"
                    >
                        <i class="tw-logo"></i>Compartir en Twitter
                    </button>
            </div>
        </div>
    </div>
    <div style="position: absolute">
    <div id="fb-root"></div>
    </div>
</div>