<?php

/**
 * UserIdentity represents the data needed to identity a user.
 * It contains the authentication method that checks if the provided
 * data can identity the user.
 */
require_once('Password.php');

class UserIdentity extends CUserIdentity
{
    /**
     * Authenticates a user.
     * The example implementation makes sure if the username and password
     * are both 'demo'.
     * In practical applications, this should be changed to authenticate
     * against some persistent user identity storage (e.g. database).
     * @return boolean whether authentication succeeds.
     */
    private $id_user;
    private $na_user;
    private $ln_user;
    private $utype;
    private $email;
    
    public function authenticate() 
    {
        $user = User::model()->findByAttributes(array(
            'email1' => $this->username
        ));

        if ($user === null) 
        {
            $this->errorCode = self::ERROR_USERNAME_INVALID;
        } 
        elseif($this->access!==1 AND !password_verify($this->password, $user->pass) AND !YII_DEBUG)
        {
            $this->errorCode = self::ERROR_PASSWORD_INVALID;
        }
        else 
        {
            $this->id_user=$user->id_user;
            $this->na_user=$user->name;
            $this->ln_user=$user->lastname;
            $utype=UserType::model()->findByPk($user->id_utype);
            $this->utype=$utype->na_utype;
            $this->email=$user->email1;

            $this->setState('utype', $utype->na_utype);
            $this->setState('email', $user->email1);
            $this->setState('lastname', $user->lastname);
            
            $this->errorCode = self::ERROR_NONE;
        }
        return !$this->errorCode;
    }
    
    public function getId()
    {
        return $this->id_user;
    }

    public function getName()
    {
        return $this->na_user;
    }

    public function getLastname()
    {
        return $this->ln_user;
    }

    public function getEmail()
    {
        return $this->email;
    }

    public function getUtype()
    {
        return $this->utype;
    }
}