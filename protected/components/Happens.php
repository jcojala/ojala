<?php

/**
 * Guardar sucesos de enevntos para Oja.la
 *
 * @author Juan Robles
 */
class Happens 
{
	public function saveEvent($repevent, $email, $stype='', $ptype='', $currency='', $curso='', $diplomado='', $monto='', $clase='')
	{
		if($email == '')
		{
			$user=User::model()->findByAttributes(array('email1'=>Yii::app()->user->email));
		}
		else
		{
			$user=User::model()->findByAttributes(array('email1'=>$email));
		}
		if($user)
		{
            $user->last_sign = new CDbExpression('NOW()');
            $user->update();
		}


		$repuser=RepUser::model()->findByAttributes(array('email1' => $email));
		$repstype=RepSuscriptionType::model()->findByAttributes(array('na_stype' => $stype));
		$repptype=RepPayType::model()->findByAttributes(array('na_ptype' => $ptype));
		$repcurrency=RepCurrency::model()->findByAttributes(array('currency' => $currency));
		$repcourse=RepCourse::model()->findByAttributes(array('name' => $curso));
		$repgroup=RepGroup::model()->findByAttributes(array('na_group' => $diplomado));
		$replesson=RepLesson::model()->findByAttributes(array('na_lesson' => $clase));

		$utms=array();
		if(isset(Yii::app()->request->cookies['utm_source']))
		    $utms+=array('utm_source' => Yii::app()->request->cookies['utm_source']);
		if(isset(Yii::app()->request->cookies['utm_medium']))
		    $utms+=array('utm_medium' => Yii::app()->request->cookies['utm_medium']);
		if(isset(Yii::app()->request->cookies['utm_term']))
		    $utms+=array('utm_term' => Yii::app()->request->cookies['utm_term']);
		if(isset(Yii::app()->request->cookies['utm_content']))
		    $utms+=array('utm_content' => Yii::app()->request->cookies['utm_content']);
		if(isset(Yii::app()->request->cookies['utm_campaign']))
		    $utms+=array('utm_campaign' => Yii::app()->request->cookies['utm_campaign']);

		$transaction = Yii::app()->db->getCurrentTransaction();
		$isNested = $transaction !== NULL;

		if(!$isNested){
			$transaction = Yii::app()->db->beginTransaction();
		}

		try
		{
			if(isset($utms) && !$reputm=RepUtm::model()->findByAttributes($utms))
			{
				$reputm=new RepUtm;
				$reputm->attributes=$utms;
				$reputm->save();
			}

			if(!$repuser)
			{
				$repuser=new RepUser;
				$repuser->email1=$email;
				$repuser->save();
			}

			if(!$repstype)
			{
				$repstype=new RepSuscriptionType;
				$repstype->na_stype=$stype;
				$repstype->save();
			}

			if(!$repptype && $stype!='')
			{
				$repptype=new RepPayType;
				$repptype->na_ptype=$stype;
				$repptype->save();
			}

			if(!$repcourse && $curso!='')
			{
				$repcourse=new RepCourse;
				$repcourse->name=$curso;
				$repcourse->save();
			}

			if(!$replesson && $clase!='')
			{
				$replesson=new RepLesson;
				$replesson->na_lesson=$clase;
				$replesson->save();
			}

			if(!$repgroup && $diplomado!='')
			{
				$repgroup=new RepGroup;
				$repgroup->na_group=$diplomado;
				$repgroup->save();
			}

			$happen= new RepHappen;
			$happen->id_event=$repevent->id_event;
			$happen->id_user=$repuser->id_user;
			if(isset($reputm))
				$happen->id_utm=$reputm->id_utm;
			if($repcurrency)
				$happen->id_currency=$repcurrency->id_currency;
			if($repstype)
				$happen->id_suscription=$repstype->id_suscription;
			if($repptype)
				$happen->id_ptype=$repptype->id_ptype;
			if($repcourse)
				$happen->id_course=$repcourse->id_course;
			if($replesson)
				$happen->id_lesson=$replesson->id_lesson;
			if($repgroup)
				$happen->id_group=$repgroup->id_group;
			if($monto)
				$happen->id_group=$monto;
			$happen->id_time=new CDbExpression('NOW()');
			$happen->created_at=new CDbExpression('NOW()');
			$happen->save();

			if(!$isNested){
				$transaction->commit();
			}
		} 
		catch(Exception $e) 
		{
			$transaction->rollback();
			Yii::log($e->getMessage(), CLogger::LEVEL_ERROR);
		}
	}

	/**
	 * EVENTOS DE VER
	 * landing
	 * formapago*/
	public static function vioLanding($email, $landing)
	{
		$repevent=RepEvent::model()->findByAttributes(array('na_event' => 'landing'));
		if(!$repevent){
			$repevent=new RepEvent;
			$repevent->na_event='landing';
			$repevent->save();
		}

		if(Yii::app()->user->isGuest)
		{
			Yii::app()->metrics->record("Vio Landing", array(
				'landing' => $landing,
			) + Controller::getUtmStatic());
		}
		else
		{
			Yii::app()->metrics->record("Vio Landing", array(
				'landing' => $landing,
			) + Controller::getUtmStatic(), $email);

			$hap=new Happens();
			$hap->saveEvent($repevent, $email);
		}
	}

	public static function seleccionoPlan($email, $stype)
	{
		$repevent=RepEvent::model()->findByAttributes(array('na_event' => 'formapago'));
		if(!$repevent){
			$repevent=new RepEvent;
			$repevent->na_event='formapago';
			$repevent->save();
		}

		if(Yii::app()->user->isGuest)
		{
			Yii::app()->metrics->record("Selecciono Plan", array(
			    'Plan' => $stype,
			) + Controller::getUtmStatic());
		}
		else
		{
			Yii::app()->metrics->record("Selecciono Plan", array(
			    'Plan' => $stype,
			) + Controller::getUtmStatic(), $email);

			$hap=new Happens();
			$hap->saveEvent($repevent, $email, $stype);
		}
	}

	/**
	 * EVENTOS DE NUEVOS
	 * trial
	 * suscriptor
	 * compra*/
	public static function nuevoSuscriptor($email, $stype, $ptype, $currency='USD')
	{
		try{
			$repevent=RepEvent::model()->findByAttributes(array('na_event' => RepEvent::SUBSCRIPTOR));
			if(!$repevent){
				$repevent=new RepEvent;
				$repevent->na_event = RepEvent::SUBSCRIPTOR;
				$repevent->save();
			}
			
			Yii::app()->metrics->record("Tiene Acceso", array(
			    'Plan' => $stype,
			    'Tipo Pago' => $ptype,
			    'Moneda' => $currency,
			) + Controller::getUtmStatic(), $email);
			Yii::app()->metrics->record("Suscriptor", array(
			    'Plan' => $stype,
			    'Tipo Pago' => $ptype,
			    'Moneda' => $currency,
			) + Controller::getUtmStatic(), $email);
			
			$hap=new Happens();
			$hap->saveEvent($repevent, $email, $stype, $ptype, $currency);
		}
		catch(Exception $e){}
	}

	/**
	 * EVENTOS DE NUEVOS POR INVITACION
	 * trial
	 * correo del referido
	 * */
	public static function suscriptorPorInvitacion($email, $referredEmail)
	{
		$repevent=RepEvent::model()->findByAttributes(array('na_event' => 'suscriptorPorInvitacion'));
		if(!$repevent){
			$repevent=new RepEvent;
			$repevent->na_event='suscriptorPorInvitacion';
			$repevent->save();
		}
		
		Yii::app()->metrics->record("Suscriptor por invitacion", array(
		    'Invitado por' => $referredEmail
		) + Controller::getUtmStatic(), $email);
		
		$hap=new Happens();
		$hap->saveEvent($repevent, $email);
	}

	public static function nuevaCompra($email, $curso, $ptype, $currency='USD')
	{
		$repevent=RepEvent::model()->findByAttributes(array('na_event' => 'compra'));
		if(!$repevent){
			$repevent=new RepEvent;
			$repevent->na_event='compra';
			$repevent->save();
		}

		Yii::app()->metrics->record("Tiene Acceso", array(
		    'Curso' => $curso,
		    'Tipo Pago' => $ptype,
		    'Moneda' => $currency,
		), $email);
		Yii::app()->metrics->record("Compra", array(
		    'Curso' => $curso,
		    'Tipo Pago' => $ptype,
		    'Moneda' => $currency,
		) + Controller::getUtmStatic(), $email);
		
		$hap=new Happens();
		$hap->saveEvent($repevent, $email, 'Compra', $ptype, $currency, $curso);
	}

	/**
	 * ////LOGIN
	 * registro
	 * login
	 * logout*/
	public static function registro($email)
	{
		$repevent=RepEvent::model()->findByAttributes(array('na_event' => 'registro'));
		if(!$repevent){
			$repevent=new RepEvent;
			$repevent->na_event='registro';
			$repevent->save();
		}

		Yii::app()->metrics->record("Se Registro", Controller::getUtmStatic(), $email);

		$hap=new Happens();
		$hap->saveEvent($repevent, $email);
	}

	public static function inicioSesion($email)
	{
		$repevent=RepEvent::model()->findByAttributes(array('na_event' => 'login'));
		if(!$repevent){
			$repevent=new RepEvent;
			$repevent->na_event='login';
			$repevent->save();
		}

		Yii::app()->metrics->record("Login", array(), $email);
		
		$hap=new Happens();
		$hap->saveEvent($repevent, $email);
	}

	public static function cerroSesion($email)
	{
		$repevent=RepEvent::model()->findByAttributes(array('na_event' => 'logout'));
		if(!$repevent){
			$repevent=new RepEvent;
			$repevent->na_event='logout';
			$repevent->save();
		}

		Yii::app()->metrics->record("Logout", array(), $email);
		
		$hap=new Happens();
		$hap->saveEvent($repevent, $email);
	}

	/**
	 * //// USUARIO
	 * diplomado
	 * curso
	 * clase
	 * terminoclase*/
	public static function vioPortada($email, $curso)
	{
		$repevent=RepEvent::model()->findByAttributes(array('na_event' => 'portada'));
		if(!$repevent){
			$repevent=new RepEvent;
			$repevent->na_event='portada';
			$repevent->save();
		}

		if(Yii::app()->user->isGuest)
		{
			Yii::app()->metrics->record("Vio Portada", array(
			    'Curso' => $curso,
			) + Controller::getUtmStatic());
		}
		else
		{
			Yii::app()->metrics->record("Vio Portada", array(
			    'Curso' => $curso,
			) + Controller::getUtmStatic(), $email);
			$hap=new Happens();
			$hap->saveEvent($repevent, $email);
		}
	}

	public static function vioCurso($email, $curso)
	{
		$repevent=RepEvent::model()->findByAttributes(array('na_event' => 'curso'));
		if(!$repevent){
			$repevent=new RepEvent;
			$repevent->na_event='curso';
			$repevent->save();
		}

		Yii::app()->metrics->record("Vio Curso", array(
		    'Curso' => $curso,
		), $email);
		
		$hap=new Happens();
		$hap->saveEvent($repevent, $email, '', '', '', $curso);
	}

	public static function vioDiplomado($email, $diplomado)
	{
		$repevent=RepEvent::model()->findByAttributes(array('na_event' => 'diplomado'));
		if(!$repevent){
			$repevent=new RepEvent;
			$repevent->na_event='diplomado';
			$repevent->save();
		}

		Yii::app()->metrics->record("Vio Diplomado", array(
			'Diplomado' => $diplomado,
		), $email);
		
		$hap=new Happens();
			$hap->saveEvent($repevent, $email, '', '', '', '', $diplomado);
	}

	
	public static function vioClase($email, $curso, $clase)
	{
		$repevent=RepEvent::model()->findByAttributes(array('na_event' => 'clase'));
		if(!$repevent){
			$repevent=new RepEvent;
			$repevent->na_event='clase';
			$repevent->save();
		}

		Yii::app()->metrics->record("Vio Clase", array(
		    'Curso' => $curso,
		    'Clase' => $clase,
		), $email);
		
		$hap=new Happens();
		$hap->saveEvent($repevent, $email, '', '', '', $curso, '', '', $clase);
	}

	public static function vioFinClase($email, $curso, $clase)
	{
		$repevent=RepEvent::model()->findByAttributes(array('na_event' => 'terminoclase'));
		if(!$repevent){
			$repevent=new RepEvent;
			$repevent->na_event='terminoclase';
			$repevent->save();
		}

		Yii::app()->metrics->record("Termino Clase", array(
		    'Curso' => $curso,
		    'Clase' => $clase,
		), $email);
		
		$hap=new Happens();
		$hap->saveEvent($repevent, $email, '', '', '',  $curso, '', '', $clase);
	}
	

	/**
	 * Dinero
	 * pago*/
	public static function pago($email, $email, $stype, $ptype, $currency, $monto)
	{
		$repevent=RepEvent::model()->findByAttributes(array('na_event' => 'pago'));
		if(!$repevent){
			$repevent=new RepEvent;
			$repevent->na_event='pago';
			$repevent->save();
		}
		
		/* Yii::app()->metrics->record("Pago", array(
			'Diplomado' => $diplomado,
		), $email); */
		
		$hap=new Happens();
		$hap->saveEvent($repevent, $email, $stype, $ptype, $currency, '', '', $monto);		
	}

	/**
	 * //Estado
	 * deudor
	 * activado
	 * cancelado*/
	public static function deudor($email, $stype)
	{
		$repevent=RepEvent::model()->findByAttributes(array('na_event' => 'deudor'));
		if(!$repevent){
			$repevent=new RepEvent;
			$repevent->na_event='deudor';
			$repevent->save();
		}

		Yii::app()->metrics->record("Deudor", array(
		    'Plan' => $stype,
		), $email);
		
		$hap=new Happens();
		$hap->saveEvent($repevent, $email, $stype);
	}

	public static function activado($email, $stype)
	{
		$repevent=RepEvent::model()->findByAttributes(array('na_event' => 'activado'));
		if(!$repevent){
			$repevent=new RepEvent;
			$repevent->na_event='activado';
			$repevent->save();
		}

		Yii::app()->metrics->record("Activado", array(
		    'Plan' => $stype,
		), $email);
		
		$hap=new Happens();
		$hap->saveEvent($repevent, $email, $stype);
	}

	public static function cancelado($email, $stype)
	{
		$repevent=RepEvent::model()->findByAttributes(array('na_event' => 'cancelado'));
		if(!$repevent){
			$repevent=new RepEvent;
			$repevent->na_event='cancelado';
			$repevent->save();
		}

		Yii::app()->metrics->record("Cancelado", array(
		    'Plan' => $stype,
		), $email);
		
		$hap=new Happens();
		$hap->saveEvent($repevent, $email, $stype);
	}

	public function enviarCorreo($template, $correos, $mensaje, $user)
	{		
		try
		{
			$mandrill = new MandrillWrapper();
			$template_name = $template;
		    	$result = $mandrill->templates->info($template_name);
			$message = array(
			        'html' => $result['code'].'<br>Nombre: '.$user->name.'<br>Email: '.$user->email1.'<br>Mensaje: '.$mensaje,
			        'text' => $result['text'].'  \\n Nombre: '.$user->name.' \\n Email: '.$user->email1.'  \\n Mensaje: '.$mensaje,
			        'subject' => $result['subject'],
			        'from_email' => $user->email1,
			        'from_name' => $user->name,
			        'to' => $correos,
			        'headers' => array('Reply-To' => $user->email1)
			);
			$async = false;
			$send_at = date("Y-m-d H:i:s");

			//if(!YII_DEBUG)
			if($_SERVER["SERVER_ADDR"] == "162.243.61.108") // Debido a que esta !debug en guineo pero no se deben enviar correos desde alli
				$result = $mandrill->messages->send($message, $async);
			echo 'exito';
		}
		catch(Mandrill_Error $e)
		{
			$errorTxt = 'A mandrill error occurred: ' . get_class($e) . ' - ' . $e->getMessage();
		    Yii::log($errorTxt, 'error');
		}
	}

	public static function correoSuscriptor($mensaje, $email='')
	{
		$correos = array(
		    array(
		        'email' => 'h@oja.la',
		        'name' => 'Hernan',
		        'type' => 'to'
		    ),
		    array(
		        'email' => 'jc@oja.la',
		        'name' => 'Juan Carlos',
		        'type' => 'to'
		    )
		);
        
		if($email == '')
		{
			$user=User::model()->findByAttributes(array('email1'=>Yii::app()->user->email));
		}
		else
		{
			$user=User::model()->findByAttributes(array('email1'=>$email));
		}

		$hap=new Happens();
	    $hap->enviarCorreo('Suscriptor', $correos, $mensaje, $user);
	}


	public static function envioInvitaciones($email, $qnty)
	{
		$repevent=RepEvent::model()->findByAttributes(array('na_event' => 'invitaciones'));
		if(!$repevent){
			$repevent=new RepEvent;
			$repevent->na_event='invitaciones';
			$repevent->save();
		}
		
		Yii::app()->metrics->record("Envio invitaciones", array(
		    'Numero de invitaciones' => $qnty
		) + Controller::getUtmStatic(), $email);
		
		$hap=new Happens();
		$hap->saveEvent($repevent, $email);
	}


	/**
	 * Almacena un pago fallido para contactar al usuario 
	 * @param  string $email 	Correo del comprador
	 * @param  string $gateway 	Método de pago
	 */
	public static function pagoFallido($email, $gateway)
	{
		$repevent=RepEvent::model()->findByAttributes(array('na_event' => 'compraFallida'));
		if(!$repevent){
			$repevent=new RepEvent;
			$repevent->na_event='compraFallida';
			$repevent->save();
		}
		
		Yii::app()->metrics->record("Compra fallida", array(
		    'Método de pago' => $gateway
		) + Controller::getUtmStatic(), $email);
		
		$hap=new Happens();
		$hap->saveEvent($repevent, $email);
	}


	/**
	 * Guarda el evento
	 * @param string evento
	 */
	/*public static function saveEvent($email, $evento, $currency='', $stype='', $ptype='', $diplomado='', $curso='', $clase='')
	{
		$repevent=RepEvent::model()->findByAttributes(array('na_event' => $evento));
		$repuser=RepUser::model()->findByAttributes(array('email1' => $email));
		$repcurrency=RepCurrency::model()->findByAttributes(array('currency' => $currency));
		$repstype=RepSuscriptionType::model()->findByAttributes(array('na_stype' => $stype));
		$repptype=RepPType::model()->findByAttributes(array('na_ptype' => $ptype));
		
		$hap=new Happens();
			$hap->saveEvent($repevent, $repuser, $repcurrency, $repstype, $repptype);
	}*/
}