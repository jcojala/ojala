<?php
/**
*  Clase para agregar los registros de scripts
*/
class OjalaScripts
{
	public static function registerGoogleAnalytics()
	{
		$jsCode = "var _gaq = _gaq || []; _gaq.push(['_setAccount', 'UA-26307872-1']); _gaq.push(['_setDomainName', 'oja.la']);
			_gaq.push(['_trackPageview']);
			(function() {
			var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
			ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
			var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
			})();";

		Yii::app()->clientScript->registerScript('googleAnalytics', $jsCode, CClientScript::POS_END);
	}	

	public static function registerKissmetrics()
	{
		$key = YII_DEBUG ? '1c42d708dc81c5d7fbea705306e34ec3ab4e3a39' : '180d38c17b41ceefca2da1e0a4391465d66998b5';
		$jsCode = "var _kmq = _kmq || []; var _kmk = _kmk || '${key}';
			        function _kms(u){ setTimeout(function(){var d = document, f = d.getElementsByTagName('script')[0],
			          s = d.createElement('script'); s.type = 'text/javascript'; s.async = true; s.src = u; f.parentNode.insertBefore(s, f);
			        },1);} _kms('//i.kissmetrics.com/i.js'); _kms('//doug1izaerwt3.cloudfront.net/' + _kmk + '.1.js');";

	  	Yii::app()->clientScript->registerScript('kissmetrics', $jsCode, CClientScript::POS_END);
	}

	public static function registerOlark()
	{
		$jsCode = 'window.olark||(function(c){var f=window,d=document,l=f.location.protocol=="https:"?"https:":"http:",z=c.name,r="load";
			(function(){f[z]=function(){ (a.s=a.s||[]).push(arguments)};
			var a=f[z]._={},q=c.methods.length;while(q--){(function(n){f[z][n]=function(){f[z]("call",n,arguments)}})(c.methods[q] )}a.v=0;a.l=c.loader;a.i=arguments.callee;
			a.f=setTimeout(function(){if(a.f){(new Image).src=l+"//"+a.l.replace(".js",".png")+"&"+escape(f.location.href) }a.f=null},20000);a.p={0:+new Date};
			a.P=function(u){a.p[u]=new Date-a.p[0]};function s(){a.P(r);f[z](r)}f.addEventListener?f.addEventListener(r,s,false ):f.attachEvent("on"+r,s);
			(function(){function p(){return["<head></head><",i,\' onload="var d=\',g,";d.getElementsByTagName(\'head\')[0].",j,"(d.",h, "(\'script\')).",k,"=\'",l,"//",a.l,"\'\"></",i,">"].join("")}
			var i="body",m=d[i];if(!m){return setTimeout(arguments.callee,100)}a.P(1);
			var j="appendChild", h="createElement",k="src",n=d[h]("div"),v=n[j](d[h](z)),b=d[h]("iframe"),g="document",e="domain",o;n.style.display="none";
			m.insertBefore(n,m.firstChild ).id=z;b.frameBorder="0";b.id=z+"-loader";if(/MSIE\s+6/.test(navigator.userAgent)){b.src="javascript:false"}b.allowTransparency="true";v[j](b);
			try{ b.contentWindow[g].open()}catch(w){c[e]=d[e];o="javascript:var d="+g+".open();d.domain=\'"+d.domain+"\';";b[k]=o+"void(0);"}try{var t=b.contentWindow[g]; t.write(p());
			t.close()}catch(x){b[k]=o+\'d.write("\'+p().replace(/"/g,\'\\"\')+\'");d.close();\'}a.P(2)})()})()})({loader:(function(m){ return"static.olark.com/jsclient/loader0.js?ts="+(m?m[1]:(+new Date))})
			(document.cookie.match(/olarkld=(\d+)/)), name:"olark",methods:["configure","extend","declare","identify"]}); olark.identify(\'5119-735-10-3270\');';

		if(!Yii::app()->user->isGuest)
			$jsCode .= 'olark(\'api.visitor.updateEmailAddress\', {emailAddress: \'' . Yii::app()->user->email . '\'});';

	  	Yii::app()->clientScript->registerScript('olark', $jsCode, CClientScript::POS_END, array('data-cfasync'=>"false"));
	}

	public static function registerZopim()
	{
		if(!YII_DEBUG){
			$jsCode = "window.\$zopim||(function(d,s){var z=\$zopim=function(c){z._.push(c)},\$=z.s=d.createElement(s),e=d.getElementsByTagName(s)[0];
				z.set=function(o){z.set._.push(o)};z._=[];z.set._=[];\$.async=!0;\$.setAttribute('charset','utf-8');
				\$.src='//v2.zopim.com/?1vOf5ce16Ivpexv0SqeOvKyqXePMmarh';z.t=+new Date;\$.type='text/javascript';
				e.parentNode.insertBefore(\$,e)})(document,'script');";

		  	Yii::app()->clientScript->registerScript('zopim', $jsCode, CClientScript::POS_END);

		  	if(!Yii::app()->user->isGuest && !YII_DEBUG) {
		  		Yii::app()->clientScript->registerScript('olark', 
				'$zopim(function() {
				  $zopim.livechat.setName(\'' . Yii::app()->user->name . '\');
				  $zopim.livechat.setEmail(\'' . Yii::app()->user->email . '\');
				})', CClientScript::POS_END);
		  	}
		}
	}

	public function registerIntercom($email = NULL)
	{
		if(!$email && !Yii::app()->user->isGuest){
			$email = Yii::app()->user->email;
		}

		$jsCode = "window.intercomSettings = {email: '" . $email . "',app_id: 'wmlo15pq','widget':{'activator': '#IntercomDefaultWidget'}};" .
				"(function(){var w=window;var ic=w.Intercom;if(typeof ic==='function'){ic('reattach_activator');ic('update',intercomSettings);" . 
				"}else{var d=document;var i=function(){i.c(arguments)};i.q=[];i.c=function(args){i.q.push(args)};w.Intercom=i;" . 
				"function l(){var s=d.createElement('script');s.type='text/javascript';s.async=true;s.src='https://static.intercomcdn.com/intercom.v1.js';" . 
				"var x=d.getElementsByTagName('script')[0];x.parentNode.insertBefore(s,x);}if(w.attachEvent){w.attachEvent('onload',l);}" .
				"else{w.addEventListener('load',l,false);}};})();";

		Yii::app()->clientScript->registerScript('intercomJs', $jsCode, CClientScript::POS_HEAD);
	}

	public static function registerSegumiento()
	{
		$jsCode = "window.__insp = window.__insp || []; __insp.push(['wid', 127395287]);
			(function() { function __ldinsp(){var insp = document.createElement('script');
			insp.type = 'text/javascript'; insp.async = true; insp.id = 'inspsync'; 
			insp.src = ('https:' == document.location.protocol ? 'https' : 'http') + '://cdn.inspectlet.com/inspectlet.js';
			var x = document.getElementsByTagName('script')[0]; x.parentNode.insertBefore(insp, x); }
			if (window.attachEvent){ window.attachEvent('onload', __ldinsp); } else {
			window.addEventListener('load', __ldinsp, false); } })();";

		Yii::app()->clientScript->registerScript('inspectletJs', $jsCode, CClientScript::POS_HEAD);
	}

	public static function registerRemarketing()
	{
		echo '<noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-WRNHC2"
			height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>';

		$jsCode = "(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
			new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
			j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
			'//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
			})(window,document,'script','dataLayer','GTM-WRNHC2');";

		Yii::app()->clientScript->registerScript('remakerting', $jsCode, CClientScript::POS_HEAD);
	}

	public static function registerAdroll()
	{
		$jsCode = "adroll_adv_id = '34SGSPBKG5G5NMP5D2FSPU';
			adroll_pix_id = 'WBC5766R2BFGZMX2WQOKR2';
			(function () {var oldonload = window.onload; window.onload = function(){
			   __adroll_loaded=true; var scr = document.createElement('script'); 
			   var host = (('https:' == document.location.protocol) ? 'https://s.adroll.com' : 'http://a.adroll.com');
			   scr.setAttribute('async', 'true'); scr.type = 'text/javascript'; scr.src = host + '/j/roundtrip.js';
			   ((document.getElementsByTagName('head') || [null])[0] || document.getElementsByTagName('script')[0].parentNode).appendChild(scr);
			   if(oldonload){oldonload()}}; }());";

		Yii::app()->clientScript->registerScript('addroll', $jsCode, CClientScript::POS_END);
	}
}