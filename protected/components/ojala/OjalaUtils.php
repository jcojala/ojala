<?php
/**
*  Clase para agregar funciones que se utilizan mucho en diversos lugares del sistema
*/
class OjalaUtils
{
	const INTERCOM_APPID = 'wmlo15pq';
	const INTERCOM_APPKEY = '6b33c947d29bd05e0ac2bf71cfe4921e27e06e6d';

	/**
	 * Función para tener el drop down de la fecha de expiración 
	 * en formularios donde se use la tarjeta de crédito
	 * @param string 	$name 		Nombre del select
	 * @param Array 	$htmlOptions	Arreglo con las opciones HTML a incluir
	 * @return string        			HTML con el código a mostrar
	 */
	public static function getExpireYears($name, $htmlOptions)
	{
		$data = array();
		$currentYear = date('Y');

		for($i=$currentYear; $i < $currentYear + 15; $i++)
			$data[$i] = $i;


		return CHtml::dropDownList($name,  NULL, $data, $htmlOptions);
	}

	/**
	 * Permite listar los meses del año en los formularios de pago
	 * @param string 	$name 		Nombre del select
	 * @param Array 	$htmlOptions	Arreglo con las opciones HTML a incluir
	 * @return string        			HTML con el código a mostrar
	 */
	public static function getDropdownMonths($name, $htmlOptions)
	{
		$data = array(
			'1' => '1 - Enero',
			'2' => '2 - Febrero',
			'3' => '3- Marzo',
			'4' => '4 - Abril',
			'5' => '5 - Mayo',
			'6' => '6 - Junio',
			'7' => '7- Julio',
			'8' => '8 - Agosto',
			'9' => '9 - Septiembre',
			'10' => '10 - Octubre',
			'11' => '11 - Noviembre',
			'12' => '12 - Diciembre',
		);

		return CHtml::dropDownList($name,  NULL, $data, $htmlOptions);
	}

	/**
	 * Función para crear una cookie desde un parametro GET
	 * @param  string $urlParam Nombre del parametro GET a crearle su cookie
	 */
	private static function createCookieFromGetParam($urlParam)
	{
		if(isset($_GET[$urlParam])){
			Yii::app()->request->cookies[$urlParam] = new CHttpCookie($urlParam, $_GET[$urlParam]);
		}
	}

	/**
	 * Función para crear las cookies de UTM para ser utilizadas en otros 
	 * métodos del sistema.
	 */
	public static function createCookieUtms()
	{
		self::createCookieFromGetParam('utm_source');
		self::createCookieFromGetParam('utm_medium');
		self::createCookieFromGetParam('utm_term');
		self::createCookieFromGetParam('utm_content');
		self::createCookieFromGetParam('utm_campaign');

		if(Yii::app()->request->cookies['utm_term']=="mex")
		{
			Yii::app()->session['utmMex']=Yii::app()->request->cookies['utm_term'];
			unset(Yii::app()->session['utmCol']);
		}
		elseif(Yii::app()->request->cookies['utm_term']=="col")
		{
			Yii::app()->session['utmCol']=Yii::app()->request->cookies['utm_term'];
			unset(Yii::app()->session['utmMex']);
		}
		else
		{
			unset(Yii::app()->session['utmCol']);
			unset(Yii::app()->session['utmMex']);
		}

		$path = Yii::app()->request->hostInfo.'/'.Yii::app()->request->getPathInfo();
		Yii::app()->request->cookies['url'] = new CHttpCookie('url', $path);
	}

	public function convertDateFromDB($date, $datetime = true)
	{
		$format = strlen($date) > 10 ? 'yyyy-MM-dd hh:mm:ss' : 'yyyy-MM-dd';
		$newDate = Yii::app()->dateFormatter->formatDateTime( CDateTimeParser::parse($date, $format ), 'medium', 'short');

		return $datetime ? $newDate : substr($newDate, 0, 10);
	}

	public function convertDateToDB($date)
	{
		return date('Y-m-d', CDateTimeParser::parse($date, Yii::app()->locale->dateFormat));
	}

	public static function enviarIntercom($id, $email, $name, $dataExtra, $sesion, $url)
	{
		if(!YII_DEBUG)
		{
			$intercom = new Intercom(self::INTERCOM_APPID, self::INTERCOM_APPKEY);
			$date = new DateTime();

			$registerDate = $sesion ? $date->getTimestamp() : NULL;

			$intercom->updateUser($id, $email, $name, $dataExtra, $registerDate, null, null, $date->getTimestamp());
			$intercom->createImpression($id, $email, $_SERVER['REMOTE_ADDR'], $_SERVER['HTTP_USER_AGENT'], $url);

			OjalaScripts::registerIntercom($email);
		}
	}

	/**
	 * Función para crear o editar datos de usuario de Intercom
	 */
	public static function createIntercomUser($user, $customData = array())
	{
		$intercomUser; 
		try {
			$intercom = new Intercom(self::INTERCOM_APPID, self::INTERCOM_APPKEY);
			$intercomUser = $intercom->createUser($user->id_user, $user->email1, $user->fullname, $customData);
		} 
		catch(Exception $e){
			$intercomUser = NULL;
			Yii::log('Error al guardar en Intercom: ' . $e->getMessage(), CLogger::LEVEL_ERROR);
		}

		return $intercomUser;
	}

	/**
	 * Permite extraer cualquier parametro GET de una URL
	 *
	 * @param string $url  La dirección a analizar
	 * @param string $key El nombre de la variable a extraer
	 */
	public static function extractParamFromString($url, $key)
	{
		$urlParts = parse_url($url);

		if(isset($urlParts['query'])) {
			parse_str($urlParts['query'], $query);
			return isset($query[$key]) ? $query[$key] : NULL;
		} else {
			return NULL;
		}
	}

	public static function getFechaLanding($landing)
	{
		$fecha="now";
		$file=getcwd().'/protected/views/l/'.strtolower($landing).'.php.txt';
		if(file_exists($file))
		{
			if(filesize($file)>0)	
			{
				$handle = fopen($file, "r");
				$fecha = fread($handle, filesize($file));
				fclose($handle);
			}
		}
		return date("F j, Y", strtotime($fecha));
	}

	/**
	 * Función para calcular el incremento porcentual de un valor
	 * @param  integer 	$current  	Valor actual
	 * @param  integer 	$previous 	Valor anterior (pasado)
	 * @return  integer           		Valor númerico del incremento porcentual
	 */
	public function getIncrementPercentaje($current, $previous)
	{
		return $previous === 0 ? 0 : round(($current*100)/$previous, 2)-100;
	}

	
	public static function sendSlack($title, $message, $channel = "#general", $username = "ojala-server", $color = "#d3d3d3", $icon = ":ghost:")
	{
		$url="https://ojalab.slack.com/services/hooks/incoming-webhook?token=iMBhWNAm5hcVnjXx7p5xkBX2";

		$paypal_icon="https://s3-us-west-2.amazonaws.com/slack-files2/bot_icons/2014-07-24/2485913480_36.png";

		if(YII_DEBUG)
		{
			if($channel=="logs")
				$channel = "logs-pruebas";
			else
				$channel = "desarrollo-pruebas";
		}
		else
		{
			$data= array(
				"channel" => $channel,
				"text" => "Hook Recibido desde ".$_SERVER["SERVER_ADDR"],
				"username" => $username,
				"unfurl_links" => true,
				"icon_emoji" => ":ghost:",
				"attachments" => array()
			); 

			array_push($data["attachments"], array(
						"fallback" => $title,
						"color" =>  $color,
						'fields' => array()
					));

			array_push($data["attachments"][0]['fields'], array(
						"title" => $title,
						"value" => $message,
						"short" => false
					));

			$data_string = json_encode($data);
			$ch = curl_init($url);                                           
			curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");             
			curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);            
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);                
			curl_setopt($ch, CURLOPT_HTTPHEADER, array(
			    'Content-Type: application/json',
			    'Content-Length: ' . strlen($data_string))
			);
			$result = curl_exec($ch);
			print_r($result);
		}
	}

	public static function getIntercomUrl($email)
	{
		try {
            $user=User::model()->findByAttributes(array('email1'=>$email));
			if($user && $user->intercom == "")
			{
				$intercom = new Intercom(self::INTERCOM_APPID, self::INTERCOM_APPKEY);
				$var = $intercom->getUser($email);
	            $user->intercom=isset($var, $var->intercom_id) ? $var->intercom_id : '';
				$user->update();
			}
			return $user->intercom;
		} catch (Exception $e) {
			return '';
		}
	}

	public static function getIntercomLastSign($email)
	{
		try {
			$intercom = new Intercom(self::INTERCOM_APPID, self::INTERCOM_APPKEY);
			$var = $intercom->getUser($email);
			return isset($var, $var->last_impression_at) ? $var->last_impression_at : '-1';
		} catch (Exception $e) {
				return "-1";
		}
	}

	public static function getIntercomPais($email)
	{
		try 
		{
			$var="";
			$intercom = new Intercom(self::INTERCOM_APPID, self::INTERCOM_APPKEY);
			$var = $intercom->getUser($email);
			if(isset($var, $var->location_data, $var->location_data->country_name))
				return $var->location_data->country_name;
			else
				return "";
		} catch (Exception $e) {
			return "";
		}
	}
}