<?php
/**
* Clase para mandar correos
*/
class Mail
{
	public $from;
	public $subject;
	public $message;
	public $toAddress; //Puede ser un array para múltiples direcciones


	function __construct()
	{
		Yii::app()->mailer->IsSMTP();
		Yii::app()->mailer->IsHTML(true);
		Yii::app()->mailer->Mailer = 'smtp';
		Yii::app()->mailer->Host = 'smtp.gmail.com';
		Yii::app()->mailer->Port = 587;
		Yii::app()->mailer->Username = 'founders@oja.la';
		Yii::app()->mailer->Password = 'A1234567890b';
		Yii::app()->mailer->SMTPSecure = 'tls';
		Yii::app()->mailer->SMTPAuth = true;
		Yii::app()->mailer->SMTPDebug = false;

		$this->from = 'Oja.la';
		Yii::app()->mailer->From = Yii::app()->params['fromEMail'];
	}

	private function setAddress()
	{
		if(is_array($this->toAddress)){
			foreach($this->toAddress as $addr){
				Yii::app()->mailer->AddAddress($addr);
			}
		} else {
			Yii::app()->mailer->AddAddress($this->toAddress);
		}
	}

	private function sendMail()
	{
		Yii::app()->mailer->FromName = $this->from;
		$this->setAddress();
		Yii::app()->mailer->Subject = $this->subject;
		Yii::app()->mailer->Body = $this->message;

		return Yii::app()->mailer->Send();
	}

	public function sendSuscriptores($attachment)
	{
		$this->subject = 'Reporte de suscriptores';
		$this->message = 'A continuaci&oacute;n le adjuntamos el reporte de los suscriptores hasta la fecha actual.' .
				'<br /><br />Atentamente:<br />El equipo de Oja.la.';
		Yii::app()->mailer->AddAttachment($attachment);

		return $this->sendMail();
	}

	public static function sendAcceptedInvitation($referredUser, $user)
	{
		try
  		{
  			//Envia el correo a quien se suscribió
 		    	$template_name = 'alert-aditional-free-month';
  			$message = array(
  				'global_merge_vars' => array( 
  					array('name' => 'REFERRED', 'content' => $referredUser->fullname),
  					array('name' => 'NAME', 'content' => $user->fullname),
				),
				'to' => array(array(
					'email' => $user->email1,
					'name' => $user->fullname,
					'type' => 'to'
				)),
			);
			$mandrill = new MandrillWrapper();
 			$mandrill->messages->sendTemplate($template_name, NULL, $message , true);

 			//Envia el correo a quien envió la invitación
 			$template_name = 'alert-aditional-free-month-from-a-sent-invitation';
  			$message = array(
  				'global_merge_vars' => array( 
  					array('name' => 'REFERRED', 'content' => $user->fullname),
  					array('name' => 'NAME', 'content' => $referredUser->fullname)
				),
				'to' => array(array(
					'email' => $referredUser->email1,
					'name' => $referredUser->fullname,
					'type' => 'to'
				)),
			);
			$mandrill = new MandrillWrapper();
 			$mandrill->messages->sendTemplate($template_name, NULL, $message , true);
  		}
 		catch(Mandrill_Error $e)
  		{
 		    	Yii::log('Mandrill error: ' .  $e->getMessage() , CLogger::LEVEL_ERROR);
  		}
	}


	public function sendScholarship($emails, $name, $invitationUrl)
	{
		try
		{
			$qnty = 0;
 			$to = array();
			foreach($emails as $email)
			{
				if (isset($sent[$email])) { //Si ya envío no es necesario volverlo a hacer
				      continue; // check if sent already, skip if so
				}

			   	$to[] = array( 'email' => $email,  'type' => 'bcc' );
				$sent[$email] = true;
				$qnty++;
			}

			$template_name = 'invitation';
  			$message = array(
  				'global_merge_vars' => array(
  					array('name' => 'FULLNAME', 	'content' => $name),
  					array('name' => 'URL', 		'content' => $invitationUrl),
  				),
				'to' => $to
			);
			$mandrill = new MandrillWrapper();
 			$mandrill->messages->sendTemplate($template_name, NULL, $message , true);

			return $qnty;
                        }
		catch(Mandrill_Error $e)
  		{
 		    	Yii::log('Mandrill error: ' .  $e->getMessage() , CLogger::LEVEL_ERROR);
 		    	return false;
  		}
	}

	public static function sendFailedPayment($user)
	{
		try
  		{
 		    $template_name = 'fallo-de-una-forma-de-pago';
  			$message = array(
  				'global_merge_vars' => array( 
  					array('name' => 'NAME', 'content' => $user->fullname),
				),
				'to' => array(
					array(
						'email' => $user->email1,
						'name' => $user->fullname,
						'type' => 'to'
					)
				),
			);
			if(!YII_DEBUG)
			{
				$mandrill = new MandrillWrapper();
	 			$mandrill->messages->sendTemplate($template_name, NULL, $message , true);
			}
  		}
 		catch(Mandrill_Error $e)
  		{
 		    	Yii::log('Mandrill error: ' .  $e->getMessage() , CLogger::LEVEL_ERROR);
  		}
	}

	public function sendError($subject, $message, $aditionalInfo = '')
	{
		$prefix = YII_DEBUG ? '[Error Desarrollo] ' : '[Error] ';
		$this->toAddress = Yii::app()->params['devEmails'];
		$this->subject = htmlspecialchars($prefix . $subject);
		$this->message = '<b>Cuerpo del error:</b><br><br>' . htmlspecialchars($message);

		if(!empty($aditionalInfo))
			$this->message .= '<br><br><b>Informaci&oacute;n adicional:</b>' . var_export($aditionalInfo, true);

		$this->sendMail();
	}
}