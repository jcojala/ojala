<?php

/**
 * Clase personalizada de Colegionline para el manejo de fechas
 *
 * @author Miguel Useche
 */
class Dates {

	/**
	 * Obtiene dia, mes o año de un modelo dependiendo de lo solicitado
	 * @param string tipo (dia, mes, anio)
	 * @return dia, mes o año del modelo
	 */
	public static function getDayMonthYear($type, $date) {
			if (empty($date))
					$date = '0000-00-00';

			switch ($type) {
					case 'year':
							return intval(substr($date, 0, 4));
					case 'month':
							return intval(substr($date, 5, 2));
					default:
							return intval(substr($date, 8, 2));
			}
	}

	/**
	 * Calcular si el año es bisiesto
	 */
	public static function calcularBisiesto($year) {
			$year = intval($year);

			//Calcula si es bisiesto
			if ($year % 4 == 0) {
					if ($year % 100 == 0) {
							if ($year % 400 == 0) {
									$month = 29;
							} else {
									$month = 28;
							}
					} else {
							$month = 29;
					}
			} else {
					$month = 28;
			}

			return $month;
	}

	/**
	 * Metodo que devuelve el array con los dias dependiendo del mes
	 */
	public static function getDropDays($year, $month = 1) {

			if (empty($year))
					$year = date('Y');

			$days = array();
			$max = 31;

			if ($month == 2)
					$max = Dates::calcularBisiesto($year);

			if ($month == 4 || $month == 6 || $month == 9 || $month == 11)
					$max = 30;

			for ($i = 1; $i <= $max; $i++) {
					$days[$i] = $i;
			}

			return $days;
	}

	/**
	 * Metodo que devuelve el array con los meses para el dropdown
	 */
	public static function getDropMonths() {
			return array(
				'01' => 'Enero',
				'02' => 'Febrero',
				'03' => 'Marzo',
				'04' => 'Abril',
				'05' => 'Mayo',
				'06' => 'Junio',
				'07' => 'Julio',
				'08' => 'Agosto',
				'09' => 'Septiembre',
				'10' => 'Octubre',
				'11' => 'Noviembre',
				'12' => 'Diciembre',
			);
	}

	/**
	 * Metodo que devuelve el array con los años para el dropdown
	 */
	public static function getDropYears() {
			$years = array();
			$year_actual = date('Y');
			$year_min = $year_actual - 4;

			for ($i = $year_actual; $i > $year_min; $i--)
					$years[$i] = $i;

			return $years;
	}

	/**
	 * Metodo para ver formatear la fecha corta
	 */
	public static function getShortDate($date) {

			$day = substr($date, 8, 2);
			$month = substr($date, 5, 2);
			$year = substr($date, 0, 4);

			$hour = substr($date, 10, 6);

			return $day . '/' . $month . '/' . $year . ' ' . $hour;
	}

	/**
	 * Metodo para transformar al formato de MySQL
	 *
	 * @param date $date fecha a transformar
	 * @return date
	 */
	public static function toMySql($date) {
			return date('Y-m-d', CDateTimeParser::parse($date, app()->locale->dateFormat));
	}

	/**
	 * Devuelve la fecha extraida de la BD
	 *
	 * @param date $date fecha a transformar
	 */
	public static function fromMysql($date) {
			return app()->dateFormatter->formatDateTime(CDateTimeParser::parse($date, 'yyyy-MM-dd'), 'medium', null);
	}
	
	public static function diferenciaHoras($hora, $salida) 
	{
		$duration=strtotime($salida)-strtotime($hora);
		$signo="";
		if($hora>$salida){
			$signo="-";
			$duration=strtotime($hora)-strtotime($salida);
		}
		$second = $duration%60;
		$minute = (($duration-$second)/60)%60;
		$hour = (($duration-$second)-($minute*60))/3600;
		return $signo.date("H:i", strtotime($hour . ":" . $minute));
	}
	
	public static function diferenciaPlan($horas, $entrada, $salida) 
	{
		$hora=$salida;
		
		$salida = strtotime($entrada);
		$salida += ($horas * 60 * 60);
		$salida = date("Y-m-d H:i:s", $salida);
						
		$duration=strtotime($salida)-strtotime($hora);
		$signo="";
		if($hora>$salida){
			$signo="-";
			$duration=strtotime($hora)-strtotime($salida);
		}
		$second = $duration%60;
		$minute = (($duration-$second)/60)%60;
		$hour = (($duration-$second)-($minute*60))/3600;
		return $signo.date("H:i", strtotime($hour . ":" . $minute));
	}

	/**
	 * Funcion para formatear la fecha de una manera mas simple
	 * @param type $year año
	 * @param type $month mes
	 * @param type $day día
	 * @param type $hours horas (opcional)
	 * @param type $minutes minutos (opcional)
	 * @return string
	 */
	public static function formatDate($year, $month, $day, $hours = '', $minutes = '') {

			$date = $year . '-' . $month . '-' . $day;

			if (!empty($hours) && !empty($minutes))
					$date .= ' ' . $hours . ':' . $minutes . ':00';

			return $date;
	}


	public static function secondsToTime($inputSeconds) {

		$secondsInAMinute = 60;
		$secondsInAnHour  = 60 * $secondsInAMinute;
		$secondsInADay    = 24 * $secondsInAnHour;

		// extract days
		$days = floor($inputSeconds / $secondsInADay);

		// extract hours
		$hourSeconds = $inputSeconds % $secondsInADay;
		$hours = floor($hourSeconds / $secondsInAnHour);

		// extract minutes
		$minuteSeconds = $hourSeconds % $secondsInAnHour;
		$minutes = floor($minuteSeconds / $secondsInAMinute);

		// extract the remaining seconds
		$remainingSeconds = $minuteSeconds % $secondsInAMinute;
		$seconds = ceil($remainingSeconds);

		// return the final array
		$obj = array(
			'd' => (int) $days,
			'h' => (int) $hours,
			'm' => (int) $minutes,
			's' => (int) $seconds,
		);

		$time='';
		if($days>0)
			$time.=$days. ' dias ';
		if($hours>0)
			$time.=$hours. ' horas ';
		if($minutes>0)
			$time.=$minutes. ' minutos ';
		if($seconds>0)
			$time.=$seconds. ' segundos ';

		return $time;
	}

	public static function secondsToTime2($inputSeconds) {

		$secondsInAMinute = 60;
		$secondsInAnHour  = 60 * $secondsInAMinute;
		$secondsInADay    = 24 * $secondsInAnHour;

		// extract days
		$days = floor($inputSeconds / $secondsInADay);

		// extract hours
		$hourSeconds = $inputSeconds % $secondsInADay;
		$hours = floor($hourSeconds / $secondsInAnHour);

		// extract minutes
		$minuteSeconds = $hourSeconds % $secondsInAnHour;
		$minutes = floor($minuteSeconds / $secondsInAMinute);

		// extract the remaining seconds
		$remainingSeconds = $minuteSeconds % $secondsInAMinute;
		$seconds = ceil($remainingSeconds);

		// return the final array
		$obj = array(
			'd' => (int) $days,
			'h' => (int) $hours,
			'm' => (int) $minutes,
			's' => (int) $seconds,
		);

		$time='';
		if($hours>0)
			$time.=$hours. ' horas ';
		if($minutes>0)
			$time.=$minutes. ' minutos ';

		return $time;
	}

	public static function FechaRelativa($fecha_recibida)
	{
		$fecha="";
		if($fecha_recibida!="" && $fecha_recibida!="No Existe")
		{
			$fechaAbsoluta = mktime(
			date("H", strtotime($fecha_recibida)), 
			date("i", strtotime($fecha_recibida)), 
			date("s", strtotime($fecha_recibida)), 
			date("n", strtotime($fecha_recibida)), 
			date("j", strtotime($fecha_recibida)), 
			date("Y", strtotime($fecha_recibida)));
			
			$dias       = intval((time() - $fechaAbsoluta) / 86400);
			$segundos   = intval((time() - $fechaAbsoluta));
		 
			if($dias < 0) {
				return "-"; //Error
			} elseif ($segundos==0) {
				$fecha = "En este momento";
			} elseif ($segundos > 0 && $segundos < 60) {
				$fecha = "Hace " . $segundos . " segundos";
			} elseif ($segundos >= 60 && $segundos <120 ) {
				$fecha = "Hace un minuto";
			} elseif ($segundos >= 120 && $segundos < 3600 ) {
				$fecha = "Hace " . intval($segundos/60) . " minutos";
			} elseif ($segundos >= 3600 && $segundos < 7200) {
				$fecha = "Hace una hora";
			} elseif ($segundos >= 7200 && $segundos < 86400) {
				$fecha = "Hace " . intval($segundos/3600) . " horas";
			} elseif ($dias==1) {
				$fecha = "Ayer";
			} elseif ($dias >= 2 && $dias <= 6) {
				$fecha =  "Hace " . $dias . " días";
			} elseif ($dias >= 7 && $dias < 14) {
				$fecha= "*La semana pasada";
			} elseif ($dias >= 14 && $dias <= 365) {
				$fecha =  "*Hace " . intval($dias / 7) . " semanas";
			} elseif ($dias>365) {
				$fecha = '*'.date("d-m-Y", $fechaAbsoluta);
			}else{
			    $fecha = $dias.'&'.$segundos.'&'.$fecha_recibida;
			}
		}
		return $fecha;
	}

	public static function FechaRelativaUnix($fechaAbsoluta)
	{
		$fecha="";
		if($fechaAbsoluta!="")
		{
			$dias       = intval((time() - $fechaAbsoluta) / 86400);
			$segundos   = intval((time() - $fechaAbsoluta));
		 	
		 	$segundos+=20000;

			if($dias < 0) {
				return "-"; //Error
			} elseif ($segundos==0) {
				$fecha = "En este momento";
			} elseif ($segundos > 0 && $segundos < 60) {
				$fecha = "Hace " . $segundos . " segundos";
			} elseif ($segundos >= 60 && $segundos <120 ) {
				$fecha = "Hace un minuto";
			} elseif ($segundos >= 120 && $segundos < 3600 ) {
				$fecha = "Hace " . intval($segundos/60) . " minutos";
			} elseif ($segundos >= 3600 && $segundos < 7200) {
				$fecha = "Hace una hora";
			} elseif ($segundos >= 7200 && $segundos < 86400) {
				$fecha = "Hace " . intval($segundos/3600) . " horas";
			} elseif ($dias==1) {
				$fecha = "Ayer";
			} elseif ($dias >= 2 && $dias <= 6) {
				$fecha =  "Hace " . $dias . " días";
			} elseif ($dias >= 7 && $dias < 14) {
				$fecha= "*La semana pasada";
			} elseif ($dias >= 14 && $dias <= 365) {
				$fecha =  "*Hace " . intval($dias / 7) . " semanas";
			} elseif ($dias>365) {
				$fecha = '*'.date("d-m-Y", $fechaAbsoluta);
			}
		}
		return $fecha;
	}
}

?>
