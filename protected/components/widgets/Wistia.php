<?php 
/**
* Widget para mostar un video de Wistia
*/
class Wistia extends CWidget
{
	public $videoId;

	public function init()
	{

	}
	
	public function run()
	{
		echo '<div class="video-panel"><div class="video-box">' .
  			'<iframe allowfullscreen="true" allowtransparency="true" class="wistia_embed" frameborder="0" ' .
  			'id="wistia_' . $this->videoId . '" mozallowfullscreen="" msallowfullscreen="" name="wistia_embed" ' .
  			'oallowfullscreen="" scrolling="no" src="https://fast.wistia.net/embed/iframe/' . $this->videoId . '" ' .
  			'video_quality="hd-only" webkitallowfullscreen="">' .
  			'</iframe>' .
  			'</div></div>';
	}
}