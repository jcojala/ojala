<?php 
/**
* Widget para mostar el menú lateral
*/
class MainMenu extends CWidget
{
	public $show = 'byRole';

	public function init()
	{

	}

	public function run()
	{
		switch($this->show){
			case 'byRole':
				if(Yii::app()->user->isGuest){
					$this->TopMenu();
				} else {
					$this->SideMenu();
				}
				break;
			case 'logo':
				$this->logoMenu();
				break;
			case 'top':
				$this->TopMenu();
				break;
			default:
				$this->SideMenu();
				break;

		}
	}

	public function TopMenu()
	{
	?>
		<header class="navbar navbar-static-top bs-docs-nav" id="top" role="banner">
		  <div class="container-fluid">
		    
		    <div class="navbar-header">
		      <button class="navbar-toggle collapsed" type="button" data-toggle="collapse" data-target=".bs-navbar-collapse">
		        <span class="sr-only">Toggle</span>
		        <span class="icon-bar"></span>
		        <span class="icon-bar"></span>
		        <span class="icon-bar"></span>
		        <span class="icon-bar"></span>
		      </button>
		      <a class="navbar-brand" href="<?php echo Yii::app()->homeUrl; ?>">Oja.la</a>
		    </div>

		    <nav class="navbar-collapse bs-navbar-collapse collapse" role="navigation" style="height: 1px;">
		      <ul class="nav navbar-nav">
		        <li><a href="<?php echo Yii::app()->urlManager->createUrl('site/cursos'); ?>">Cursos</a></li>
						<li><a href="<?php echo Yii::app()->urlManager->createUrl('site/diplomados'); ?>">Diplomados</a></li>
						<li><a href="<?php echo Yii::app()->urlManager->createUrl('site/blog'); ?>">Blog</a></li>	
						<!-- <li><a href="<?php echo Yii::app()->urlManager->createUrl('site/proximoscursos'); ?>">Próximos cursos</a></li> -->
		      </ul>
		      <ul class="nav navbar-nav navbar-right">
					  <?php if(isset(Yii::app()->request->cookies['becado'])){ ?>
					  	<?php if(Yii::app()->controller->action->id == 'loginlms'){ ?>
							<li><a href="<?php echo Yii::app()->urlManager->createUrl('site/registro'); ?>" class="btn btn-success">Crear una cuenta</a></li>
					  	<?php }else{ ?>
							<li><a href="<?php echo Yii::app()->urlManager->createUrl('site/loginlms'); ?>" class="btn btn-success">Entrar a mi cuenta</a></li>
					  	<?php } ?>
					  <?php }else{ ?>
		        <li><a href="<?php echo Yii::app()->urlManager->createUrl('site/login'); ?>">Entrar a mi cuenta</a></li>
					  <?php } ?>
		      </ul>
		    </nav>
		  </div>
		</header>

	<?php
	}
	
	public function SideMenu()
	{
	?>
		<div class="side-nav">
			<nav>
				<ul>
					<li>
						<a href="#" class="brand">
							<span class="logo"></span>
							<span class="name"></span>
						</a>
					</li>
					<li <?php if(Yii::app()->controller->getRoute()=="site/cursos"){ echo 'class="active"'; } ?>>
						<a href="<?php echo Yii::app()->urlManager->createUrl('site/cursos'); ?>">
							<span class="glyphicon glyphicon-th"></span>
							<span class="text-link">Cursos</span>
						</a>
					</li>
					<li class="new <?php if(Yii::app()->controller->getRoute()=="site/diplomados"){ echo 'active'; } ?>">
					  <a href="<?php echo Yii::app()->urlManager->createUrl('site/diplomados'); ?>">
						<span class="glyphicon glyphicon-th-large"></span>
						<small>Nuevo</small>
						<span class="text-link">Diplomados</span>
					  </a>
					</li>

					<!-- <li <?php if(Yii::app()->controller->getRoute()=="site/proxumoscursos"){ echo 'class="active"'; } ?>>
						<a href="<?php echo Yii::app()->urlManager->createUrl('site/proximoscursos'); ?>">
							<span class="glyphicon glyphicon-th"></span>
							<span class="text-link">Próximos Cursos</span>
						</a>
					</li> -->
					
					<div class="title">Tus enlaces</div>

					<li <?php if(Yii::app()->controller->getRoute()=="site/tuscursos"){ echo 'class="active"'; } ?>>
						<a href="<?php echo Yii::app()->urlManager->createUrl('site/tuscursos'); ?>">
							<span class="glyphicon glyphicon-th-list"></span>
							<span class="text-link">Tus Cursos</span>
						</a>
					</li>
					<li <?php if(Yii::app()->controller->getRoute()=="site/tusdiplomados"){ echo 'class="active"'; } ?>>
						<a href="<?php echo Yii::app()->urlManager->createUrl('site/tusdiplomados'); ?>">
							<span class="glyphicon glyphicon-list-alt"></span>
							<span class="text-link">Tus Diplomados</span>
						</a>
					</li>
					<?php /*<li <?php if(Yii::app()->controller->getRoute()=="becas/index"){ echo 'class="active"'; } ?>>
				  <a href="<?php echo Yii::app()->urlManager->createUrl('becas/index'); ?>">
				    <span class="glyphicon glyphicon-bold"></span>
				    <span class="text-link">Becas</span>
				  </a>
				</li>*/?>
					<?php if(Yii::app()->user->utype != 'Becado'): ?>
					<li <?php if(Yii::app()->controller->getRoute()=="site/perfileditar"){ echo 'class="active"'; } ?>>
						<a href="<?php echo Yii::app()->urlManager->createUrl('site/perfilEditar'); ?>">
							<span class="glyphicon glyphicon-user"></span>
							<span class="text-link">Tu Perfil</span>
						</a>
					</li>
					<?php endif; ?>

					<?php if(!Yii::app()->user->isGuest): ?>
						<?php if(Yii::app()->user->utype === 'Administrador' || Yii::app()->user->utype === 'AAC'): ?>
						<li <?php if(Yii::app()->controller->getRoute()=="admin/index"){ echo 'class="active"'; } ?>>
							<a href="<?php echo Yii::app()->urlManager->createUrl('admin/index'); ?>">
								<span class="glyphicon glyphicon-cog"></span>
								<span class="text-link">Administración</span>
							</a>
						</li>
						<?php endif; ?>
					<?php endif; ?>
					<li>
						<a href="<?php echo Yii::app()->urlManager->createUrl('site/logout'); ?>" class="logout">
							<span class="text-link">Cerrar sesión</span>
						</a>
					</li>
				</ul>
			</nav>
		</div>
	<?php
	}

	public function logoMenu()
	{
	?>
		<header>
			<nav class="navbar navbar-default navbar-fixed-top" role="navigation">
				<div class="container">
					<div class="navbar-header">
						<div class="navbar-brand">Oja.la</div>
					</div>
					<!-- <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
					  <ul class="nav navbar-nav navbar-right">
						<li style="color: white; margin-top: 9px;">Ya tienes una cuenta?</li>
						<li><a href="<?php //echo Yii::app()->urlManager->createUrl('site/login'); ?>" class="btn btn-success">Inicia Sesión</a></li>  
					  </ul>
					</div> -->
				</div>
			</nav>
		</header>
	<?php
	}
}