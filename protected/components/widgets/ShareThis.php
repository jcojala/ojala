<?php 
/**
* Widget para mostar botones de redes sociales de Share This
*/
class ShareThis extends CWidget
{
	public function init()
	{

	}
	
	public function run()
	{
		echo '<div class="mod share">
				<h4>Comparte este curso:</h4>
				<span class="st_facebook_large" displayText="Facebook"></span>
				<span class="st_googleplus_large" displayText="Google +"></span>
				<span class="st_twitter_large" displayText="Tweet"></span>
				<span class="st_linkedin_large" displayText="LinkedIn"></span>
				<span class="st_delicious_large" displayText="Delicious"></span>
				<span class="st_email_large" displayText="Email"></span>
			</div>';
		Yii::app()->clientScript->registerScript('switchShareThis', 'var switchTo5x=true;', CClientScript::POS_END);
	  	Yii::app()->clientScript->registerScriptFile('https://ws.sharethis.com/button/buttons.js', CClientScript::POS_END);
		Yii::app()->clientScript->registerScript(
			'activateShareThis', 
			'(stLight.options({publisher: "ur-90a181e5-9694-450c-d4ac-21bf6f0ab4e4", doNotHash: true, doNotCopy: true,hashAddressBar: false});)();', 
			CClientScript::POS_END
		);
	}
}