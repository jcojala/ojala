<?php 
/**
* Widget para seleccionar el país y región
*/
class CountrySelection extends CWidget
{
	public $form;
	public $model;
	public $countryAttribute;
	public $regionAttribute;
	public $regionUrl = 'api/regions';

	public function init()
	{

	}
	
	public function run()
	{
		$countryList = Country::model()->findAll();
		$countries = array(''=>'Selecciona tu país') + CHtml::listData($countryList, 'id_country','name');

		$countriesCode = array();
		foreach($countryList as $country){
			$countriesCode[$country->id_country] = array('data-code'=>$country->phone_code);
		}

		?>
				<div class="form-group">
					<?php 
						echo $this->form->dropDownList($this->model, $this->countryAttribute, $countries,
							array('id'=>'country-list', 'class'=>'form-control input-lg','options'=>$countriesCode)
						);
						echo $this->form->error($this->model, $this->countryAttribute);
					?>
				</div>

				<div class="region-wrapper form-group">
					<img class="loading hidden" src="<?php echo Yii::app()->baseUrl ?>/images/icons/loading.gif" alt="Cargando...">
					<?php 
						echo $this->form->dropDownList($this->model, $this->regionAttribute, array(),
							array('id'=>'region-list', 'class'=>'form-control input-lg hidden js-hide-country')
						);
						echo $this->form->error($this->model, $this->regionAttribute);
					?>
				</div>
	<?php
		Yii::app()->clientScript->registerScript(
			'countrySelection', 
			"(function(){\$(document).ready(function(){
				\$('#country-list').on('change', function(){
					\$('.region-wrapper').slideDown('fast');
					\$.ajax({
						url: baseUrl + '/' + '" . $this->regionUrl . "',
						data: {country: \$('#country-list').val() },
						dataType: 'json',
						type: 'POST',
						beforeSend: function(){
							\$('.region-wrapper .loading').removeClass('hidden');
							\$('.js-hide-country').addClass('hidden');
						},
						complete: function(){
							\$('.region-wrapper .loading').addClass('hidden');
							\$('.js-hide-country').removeClass('hidden');
						},
						success: function(data){
							var drpRegion = \$('#region-list');
							drpRegion.html('<option value=\"\">Seleciona tu ciudad</option>');

							\$.each(data, function(key, value) {
							    drpRegion.append(
							    	'<option data-code=\"' + value['area_code'] + 
							    	'\" value=\"' + value['id_region'] + 
							    	'\">' + value['name'] + '</option>'
							    );
							});
						}
					});	
				});
			});})();", 
			CClientScript::POS_END
		);
	}
}