<?php
require(dirname(__FILE__) . '/geoiploc.php');
/**
* Clase para definir el objeto que va a realizar pagos
*/
class OjalaPayments
{
	const MXN_RATE=  '13.5';
	const COP_RATE=  '1900';
	const DEFAULT_CURRENCY = 'usd'; //Stripe usa minúscula
	const MXN_ACTIVE = true;
	const COP_ACTIVE = true;

	const TYPE_PAYPAL = 1;
	const TYPE_STRIPE = 2;
	const TYPE_CONEKTA = 3;
	const TYPE_REFERRAL_SENT = 4;
	const TYPE_REFERRAL_RECEIVED = 5;

	const ERROR_FAILED_COURSE = 'Error en la compra de curso';
	const ERROR_FAILED_PAYMENT = 'Pago no guardado';
	const ERROR_FAILED_STATUS = 'Cambio de estado no guardado';
	const ERROR_FAILED_SUBSCRIPTION = 'Suscripción no guardada';
	const ERROR_FAILED_USER = 'Usuario no modificado';
	const ERROR_MULTIPLE_SUBSCRIPTION = 'El usuario esta creando múltiples suscripciones';
	const ERROR_NO_SUBSCRIPTION = 'Llegó un hook sin suscripción';
	const ERROR_NO_SUBSCRIPTION_DB = 'No se encontró la suscripción en la BD';
	const ERROR_NO_USER = 'Renovación de una suscripción de un usuario no registrado: ';
	const ERROR_NO_PLAN = 'No existe el plan en BD: ';

	const MAIL_ERROR_SUBJECT = 'Problema durante un pago';

	public $tokenName;
	public $intercomUser;
	public $status;

	/**
	 * Constructor de la clase
	 * @param string $wrapperType Tipo de gateway a usar
	 */
	function __construct()
	{
		if(isset($_COOKIE['currency']) && $_COOKIE['currency']=="local")
		{
			Yii::app()->session['currency'] = "usd";
			Yii::app()->session['currencyName'] = "Dólar Americano";
		}
		else
		{
			if(($this->getCountryCode() === 'MX' || isset(Yii::app()->session['utmMex'])) && self::MXN_ACTIVE)
			{
				$_COOKIE['currency']='foreign';
				Yii::app()->session['currency'] = "mxn";
				Yii::app()->session['currencyName'] = "Peso Mexicano";
			}
			elseif(($this->getCountryCode() === 'CO' || isset(Yii::app()->session['utmCol'])) && self::COP_ACTIVE)
			{
				$_COOKIE['currency']='foreign';
				Yii::app()->session['currency'] = "cop";
				Yii::app()->session['currencyName'] = "Peso Colombiano";
			}
			else
			{
				unset($_COOKIE['currency']);
				Yii::app()->session['currency'] = "usd";
				Yii::app()->session['currencyName'] = "Dólar Americano";
			}
		}
	}

	protected function buildErrorMessage($error, $data){
		return $error . ' Data: ' . var_export($data, true);
	}

	public function getCountryCode(){
		return getCountryFromIP( $_SERVER['REMOTE_ADDR']);
	}

	public static function getForeignCurrency()
	{
		if((OjalaPayments::getCountryCode() === 'MX' || isset(Yii::app()->session['utmMex'])) && self::MXN_ACTIVE)
			return "Pesos Mexicanos";
		elseif((OjalaPayments::getCountryCode() === 'CO' || isset(Yii::app()->session['utmCol'])) && self::MXN_ACTIVE)
			return "Pesos Colombianos";
		else
			return 'Dólares americanos';
	}

	/**
	 * Permite realizar conversiones de la moneda almacenada a otro valor
	 * @param  	float  	$amount		Cantidad monetaria
	 * @param  	string 	$to     	Código internacional de la moneda a utilizar
	 * @return  integer         	Monto en la moneda solicitada
	 */
	public static function convertCurrency($amount, $to='MXN')
	{
		$rate = NULL;

		switch($to){
			case 'MXN':
				$rate = self::MXN_RATE;
				break;
			case 'COP':
				$rate = self::COP_RATE;
				break;
			default:
				$rate = 1;
				break;
		}

		return ceil($amount * $rate);
	}

	/**
	 * Funcion para calcular fecha de vencimiento de una subscripción
	 * @return Date fecha de vencimiento
	 */
	public function getExpDate($interval)
	{
		switch ($interval) {
			case 'month':
				$timeStr = '+1 month';
				break;
			case 'year':
				$timeStr = '+1 year';
				break;
			default:
				$timeStr = '+1 month';
				break;
		}

		return date('Y-m-d H:i:s', strtotime($timeStr));
	}

	protected function savePayment($id_suscription, $email, $amount, $externalId, $currency = 'USD', $gateway = 'STRIPE', $status = 'Completed')
	{
		$na_ptype = strtoupper($gateway);

		$pay=Pay::model()->findByAttributes(array('code' => $externalId));
		$resultado=true;
		if(!$pay)
		{
			$pay = new Pay;
			$pay->date = new CDbExpression('NOW()');
			$pay->email = $email;
			$pay->amount = $na_ptype == 'STRIPE' ? $amount / 100 : $amount;
			$pay->code = $externalId;
			$pay->status = $status;
			$pay->currency = $currency;
			$pay->id_ptype = PayType::model()->findByAttributes(array(
				'na_ptype' => $na_ptype
			))->id_ptype;

			if($id_suscription!=""){
				$pay->id_suscription = $id_suscription;
			}

			$resultado=$pay->save();
		}
		return $resultado;

	}

	/**
	 * Envia un correo electrónico a los desarrolladores para advertir sobre un error
	 */
	protected function sendErrorMail($subject, $msg, $data){
		try {
			$mailer = new Mail;
			$mailer->sendError($subject, $msg, $data);
		} catch(Exception $e){}
	}

	/**
	* Guarda el estado de una suscripción
	* @param  int 	$id_suscription 	ID de la suscripción a guardar el estado
	* @param  int 	$status         	Nuevo estado de la suscripcion
	* @param  User 	$user         		Usuario al cual se va a actualizar el estado en Intercom
	* @return bool 	                 	Indica si se pudo guardar o no el estado
	*/
	public function saveStatus($id_suscription, $status = Status::ACTIVE)
	{
		$ss = new SuscriptionStatus;
		$ss->id_suscription = $id_suscription;
		$ss->id_status = $status;
		$ss->date = new CDbExpression('NOW()');
		$ss->active = 1;

		return $ss->save();
	}

	/**
	* Guarda el estado de una suscripción
	* @return bool 		Retorna el usuario de intercom
	*/
	public function syncStatus()
	{
		if($this->intercomUser && $this->status){
			return OjalaUtils::createIntercomUser($this->intercomUser, array(
				'STATUS_SUSCRIPCION' => Status::model()->findByPk($this->status)->na_status,
			));
		}
	}
}
