<?php
/**
*  Clase para procesar los eventos de Paypal
*/
class PaypalWrapper extends OjalaPayments
{
	const PETITION_ERROR = 'No hemos podido procesar su petición. Por favor intente mas tarde.';

	public static function getDomain() {
		return YII_DEBUG ? 'https://www.sandbox.paypal.com/' : 'https://www.paypal.com/';
	}

	/*Crea el token para ser enviado a Paypal*/
	static function processPayment()
	{
		if(isset($_GET['amount']) && isset($_GET['desc']) && isset($_GET['id']))
		{
			$paymentInfo['Order']['theTotal'] = str_replace('.', '', $_GET['amount']);
			$paymentInfo['Order']['description'] = $_GET['desc'];
			$paymentInfo['Order']['quantity'] = '1';
			Yii::app()->session['theTotal'] = str_replace('.', '', $_GET['amount']);

			// call paypal
			$result = Yii::app()->Paypal->SetExpressCheckout($paymentInfo);

			//Detect Errors
			if(!Yii::app()->Paypal->isCallSucceeded($result))
			{
			     $user=User::model()->findByPk(Yii::app()->user->id);
			     Happens::pagoFallido($user->email1, 'Paypal');
			     echo Yii::app()->Paypal->apiLive ? self::PETITION_ERROR : $result['L_LONGMESSAGE0'];
			     return false;
			}
			else
			{
			     Yii::app()->session['compraCurso']=$_GET['id'];
			     $token = urldecode($result["TOKEN"]);

			     return Yii::app()->Paypal->paypalUrl . $token;
			}
		}
		return false;
	}

	static function confirmPayment($user, $result)
	{
		//Detect errors
		if(!Yii::app()->Paypal->isCallSucceeded($result))
		{
			echo Yii::app()->Paypal->apiLive ? self::PETITION_ERROR  : $result['L_LONGMESSAGE0'];
			return false;
		}
		else
		{
			$paymentResult = Yii::app()->Paypal->DoExpressCheckoutPayment($result);
			//Detect errors
			if(!Yii::app()->Paypal->isCallSucceeded($paymentResult))
			{
				echo Yii::app()->Paypal->apiLive ? self::PETITION_ERROR : $paymentResult['L_LONGMESSAGE0'];
				return false;
			}
			else
			{
				if(isset(Yii::app()->session['compraCurso']) && isset(Yii::app()->session['theTotal']))
				{
					$course=Course::model()->findByPk(Yii::app()->session['compraCurso']);
					$transaction= Yii::app()->db->beginTransaction();

					try
					{
						$subscription=new Suscription;
						$subscription->id_user=$user->id_user;
						$subscription->id_stype=1; /// Compra curso
						$subscription->id_course = $course->id_course;
						$subscription->id_ptype=1; /// Paypal
						$subscription->date = new CDbExpression('NOW()');
						$subscription->active = 1;
						$subscription->status = 1; // 0-Cancelada, 1-Activa, 2-PENDING

						if($subscription->save())
						{

							if(!$this->savePayment(
								$subscription->id_suscription,
								$result['EMAIL'],
								$course->cost,
								$result['TOKEN'],
								'usd',
								'PAYPAL'
							)){
								throw new Exception(self::ERROR_FAILED_PAYMENT);
							}

							if($result['EMAIL']!=$user->email1)
							{
								$user->email2=$result['EMAIL'];

								if(!$user->update()){
									throw new Exception(self::ERROR_FAILED_USER);
								}
							}

						} else {
							throw new Exception(self::ERROR_FAILED_SUBSCRIPTION);
						}

						$courseSusc=CourseSuscription::model()->findByAttributes(array(
							'id_course'=>$course->id_course,
							'id_user'=>$user->id_user
						));

						if(!$courseSusc)
						{
							$courseSusc=new CourseSuscription;
 							$courseSusc->id_course = $course->id_course;
							$courseSusc->id_user = $user->id_user;
							$courseSusc->active = 1;
						}
						elseif($courseSusc->active == 0)
						{
							$courseSusc->active = 1;
						}

						if(!$courseSusc->save()){
							throw new Exception(self::ERROR_FAILED_COURSE);
						}

						$s_status = new SuscriptionStatus;
						$s_status->id_suscription = $subscription->id_suscription;
						$s_status->id_status= Status::ACTIVE;
						$s_status->date=new CDbExpression('NOW()');

						if($s_status->save()){
							throw new Exception(self::ERROR_FAILED_STATUS);
						}

						$transaction->commit();
					}
					catch( Exception $e)
					{
						$transaction->rollback();
						Yii::log('Error al comprar curso: ' . $e->getMessage(), CLogger::LEVEL_ERROR);
					}

					//Happens::nuevaCompra($email, $course->name, 'Paypal', 'USD');
					return array('user'=>$user, 'curso'=>$course, 'hasSuscription' => $hasSuscription);
				}
				elseif(isset(Yii::app()->session['tipoPlan']))
				{
					$st=SuscriptionType::model()->findByAttributes(array('id_service'=>Yii::app()->session['tipoPlan']));
					$user=User::model()->findByPk(Yii::app()->user->id);
					$hasSuscription = User::hasActiveSubscription(Yii::app()->user->id);
					return array('user'=>$user, 'plan'=>$st->na_stype, 'hasSuscription' => $hasSuscription);
				}
				else
				{
					return array('user'=>$user);
				}
			}
		}
	}

	/**
	 * Función que verifica si un IPN recibido fue enviado por Paypal y no un atacante
	 * @return bool  Si el mensaje fue enviado por Paypal
	 */
	public static function validateIPNSource()
	{
		if(YII_DEBUG){
			return true;
		}

		$req = 'cmd=_notify-validate';

		foreach ($_POST as $key => $value) {
			$value = urlencode(stripslashes($value));
			$req  .= "&${key}=${value}";
		}

		$header  = "POST /cgi-bin/webscr HTTP/1.1\r\n";
		$header .= "Content-Type: application/x-www-form-urlencoded\r\n";
		$header .= "Content-Length: " . strlen($req) . "\r\n\r\n";

		$domain =  YII_DEBUG ? 'www.sandbox.paypal.com' : 'www.paypal.com';
		$fp = fsockopen($domain, 443, $errno, $errorString, 30);

		fputs($fp, $header . $req);

		while (!feof($fp)) {
		    	$res = fgets($fp, 1024);
			if (strcmp ($res, "INVALID") == 0) {
				Yii::log('Se ha recibido una IPN inválida de Paypal: ' . var_export($_POST, true) , CLogger::LEVEL_ERROR);
				return false;
			}
	 	}

		fclose($fp);
		return true;
	}

	/**
	 * Funcion para procesar el IPN recibido desde Paypal,
	 * la llave 'option_selection1' es email del usuario en Oja.la
	 * y la llave 'option_selection2' es ID del curso en Paypal
	 */
	public function processIPN()
	{
		$paypalData = isset($_POST) ? $_POST : NULL;
		
		//Yii::log('Paquete:' . var_export($paypalData), CLogger::LEVEL_ERROR);
		//OjalaUtils::sendSlack("IPN de Paypal", "Prueba de evento de paypal", "pruebas", "paypal", "good");

		if(self::validateIPNSource()  && isset($paypalData['txn_type']))
		{
			//Se carga un usuario, el cual siempre se crea en el sistema antes
			//de enviar el pago a Paypal
			$user = NULL;

			if(!$user && isset($paypalData['option_selection1']))
				$user = User::model()->findByAttributes(array('email1'=>$paypalData['option_selection1']));

			if(!$user && isset($paypalData['option_selection1']))
				$user = User::model()->findByAttributes(array('email2'=>$paypalData['option_selection1']));

			if(!$user && isset($paypalData['payer_email']))
				$user = User::model()->findByAttributes(array('email1'=>$paypalData['payer_email']));

			if(!$user && isset($paypalData['payer_email']))
				$user = User::model()->findByAttributes(array('email2'=>$paypalData['payer_email']));

			if($user)
			{
				$transaction= Yii::app()->db->beginTransaction();
				//$subscription = $user->activeSubscription('stripe');

				try
				{
					switch ($paypalData['txn_type'])
					 {
					 	case 'subscr_signup': //Suscripcion Nueva

							$user->id_service = $paypalData['payer_id'];
							$user->id_utype = User::TYPE_STUDENT; // Actualización del usuario a Estudiante

							if(!$user->update()){
								throw new Exception($this->buildErrorMessage(self::ERROR_FAILED_USER, $paypalData));
							}

							if(isset($paypalData['option_selection2']))
								$plan = SuscriptionType::model()->findByAttributes(array('_id'=>$paypalData['option_selection2']));
							else
								$plan = SuscriptionType::model()->findByAttributes(array('_id'=>"80"));

							$title="Nueva Suscripcion de Paypal";
							$message="El usuario ".$user->email1." se Suscribió al plan ".$plan->na_stype."";
							//OjalaUtils::sendSlack($title, $message, "desarrollo-pagos", "paypal", "good");
							OjalaUtils::sendSlack($title, $message, "suscripciones", "paypal", "good");

							//Si ya tiene una suscripción avisa que esta creando una nueva
							if(User::hasActiveSubscription($user->id_user, 'paypal'))
							{
								$this->sendErrorMail(self::MAIL_ERROR_SUBJECT, self::ERROR_MULTIPLE_SUBSCRIPTION, $paypalData);
							}

							$subscription = new Suscription;
							$subscription->id_user = $user->id_user;
							$subscription->id_stype = $plan->id_stype;
							$subscription->id_ptype = self::TYPE_PAYPAL;
							$subscription->date = new CDbExpression('NOW()');
							$subscription->active = 1;
							$subscription->display_name = $user->fullname;
							$subscription->paid = true;
							$subscription->id_service = $paypalData['subscr_id'];
							$subscription->exp_date = $this->getExpDate($plan->interval);

							//Cancela cualquier suscripción de Stripe existente
							StripeWrapper::cancelSuscriptionIfExists($user->email1);

							if(!empty($user->email2)){
								StripeWrapper::cancelSuscriptionIfExists($user->email2);
							}

							if(!$subscription->save()){
								throw new Exception($this->buildErrorMessage(self::ERROR_FAILED_SUBSCRIPTION, $paypalData));
							}

							Happens::correoSuscriptor('Suscripción: '.$plan->na_stype.' Metodo: Stripe', $user->email1);
							Happens::nuevoSuscriptor($user->email1, $plan->na_stype, 'PAYPAL','USD');
							break;
						case 'subscr_modify':
							$plan = SuscriptionType::model()->findByAttributes(array('_id'=>$paypalData['option_selection2']));
							$subscription = Suscription::model()->findByAttributes(array('id_service'=>$paypalData['subscr_id']));

							if(!$subscription){
								Yii::log(self::ERROR_NO_SUBSCRIPTION . ' ' . $paypalData['subscr_id'], CLogger::LEVEL_ERROR);
								exit();
							}

							$user = User::model()->findByPk($subscription->id_user);
							$subscription->exp_date = $this->getExpDate($plan->interval);
							$subscription->id_stype = $plan->id_stype;
							$subscription->id_service = $paypalData['subscr_id'];
							$subsInfo = Yii::app()->Paypal->hash_call('GetRecurringPaymentsProfileDetails','&PROFILEID=' . $paypalData['subscr_id']);

							switch($subsInfo['STATUS'])
							{
								case 'Active':
									if(!$this->saveStatus($subscription->id_suscription)){
										throw new Exception($this->buildErrorMessage(self::ERROR_FAILED_STATUS, $paypalData));
									}

									$this->intercomUser = $user;
									$this->status = Status::ACTIVE;
									Happens::activado($user->email1, $plan->na_stype);

									if($subscription->active != 1)
										$subscription->active = 1;
									break;
								case 'Suspended':
								case 'Expired':
									if($subscription->active != 2)
									{
										if(!$this->saveStatus($subscription->id_suscription, Status::DEBT)){
											throw new Exception($this->buildErrorMessage(self::ERROR_FAILED_STATUS, $paypalData));
										}
										$subscription->active = 2;
										$this->intercomUser = $user;
										$this->status = Status::DEBT;
										Happens::deudor($user->email1, $plan->na_stype);
									}
									break;
								case 'Pending':
									$subscription->active = 3;
									break;
							}

							if(!$subscription->update()){
								throw new Exception($this->buildErrorMessage(self::ERROR_FAILED_SUBSCRIPTION, $paypalData));
							}
							break;
						case 'subscr_cancel': // Cancelado
							$plan = SuscriptionType::model()->findByAttributes(array('_id'=>$paypalData['option_selection2']));
							$subscription = Suscription::model()->findByAttributes(array('id_service'=>$paypalData['subscr_id']));

							if(!$subscription)
							{
								$error = 'Falló la cancelación de suscripcion con Paypal: ' . var_export($paypalData, true);
								Yii::log($error, CLogger::LEVEL_ERROR);
							}
							else
							{
								$user = User::model()->findByPk($subscription->id_user);
								$title="Suscripcion Cancelada de Paypal";
								$message="El usuario ".$user->email1." Cancelo al plan ".$plan->na_stype."";
								OjalaUtils::sendSlack($title, $message, "suscripciones", "paypal", "danger");

								if(!$this->saveStatus($subscription->id_suscription, Status::CANCELED)){
									throw new Exception($this->buildErrorMessage(self::ERROR_FAILED_STATUS, $paypalData));
								}

								$this->intercomUser = $user;
								$this->status = Status::CANCELED;
								$subscription->active = 0;

								if(!$subscription->update()){
									throw new Exception($this->buildErrorMessage(self::ERROR_FAILED_SUBSCRIPTION, $paypalData));
								}

								Happens::cancelado($user->email1, $plan->na_stype);
							}
							break;
						case 'recurring_payment_suspended': //Cancelación desde el admin
							$subscription = Suscription::model()->findByAttributes(array('id_service'=>$paypalData['recurring_payment_id']));

							//OjalaUtils::sendSlack($title, $message, "desarrollo-pagos", "paypal", "danger");

							if(!$subscription)
							{
								$error = 'Falló la cancelación de suscripcion con Paypal: ' . var_export($paypalData, true);
								Yii::log($error, CLogger::LEVEL_ERROR);
							}
							else
							{
								$user = User::model()->findByPk($subscription->id_user);
								
								$title="Suscripcion Cancelada de Paypal";
								$message="El usuario ".$user->email1." cancelo el plan";
								OjalaUtils::sendSlack($title, $message, "suscripciones", "paypal", "danger");

								if(!$this->saveStatus($subscription->id_suscription, Status::CANCELED)){
									throw new Exception($this->buildErrorMessage(self::ERROR_FAILED_STATUS, $paypalData));
								}

								$this->intercomUser = $user;
								$this->status = Status::CANCELED;
								$subscription->active = 0;

								if(!$subscription->update()){
									throw new Exception($this->buildErrorMessage(self::ERROR_FAILED_SUBSCRIPTION, $paypalData));
								}

								Happens::cancelado($user->email1, $paypalData['product_name']);
							}
							break;
						case 'subscr_payment':
							sleep(3);

							$title="Pago de Paypal Exitoso";
							$message="$".$paypalData['mc_gross']." USD Pago Exitoso de ".$user->email1."";
							OjalaUtils::sendSlack($title, $message, "desarrollo-pagos", "paypal", "good"); //"#36a64f" "desarrollo-pagos"

							$subscription = Suscription::model()->findByAttributes(array('id_service'=>$paypalData['subscr_id']));
							/*$user = User::model()->with('suscriptions','suscriptions.type')->find('email1=:email1 OR email2=:email2', array(
								':email1' => $user->email1,
								':email2' => $user->email1,
							));*/
							
							if(!$subscription)
							{
								if(isset($user->suscriptions) && is_array($user->suscriptions))
								{
									foreach ($user->suscriptions as $suscription)
									{
										if($suscription->type->recurrence == 1 && $suscription->active > 0) 
										{
											$subscription=$suscription;
											$subscription->id_service=$paypalData['subscr_id'];
											$subscription->active=1;
											$subscription->update();
											break;
										}
									}
								}
							} 

							if(!$subscription && !isset($user->suscriptions))
						 	{
								throw new Exception($this->buildErrorMessage(self::ERROR_NO_SUBSCRIPTION_DB, $paypalData));
							}

							if(!$this->savePayment(
								$subscription->id_suscription,
								$user->email1,
								$paypalData['mc_gross'],
								$paypalData['txn_id'],
								'usd',
								'PAYPAL'
							)){
								throw new Exception($this->buildErrorMessage(self::ERROR_FAILED_PAYMENT, $paypalData));
							}

							if(!$this->saveStatus($subscription->id_suscription))
							{
								throw new Exception($this->buildErrorMessage(self::ERROR_FAILED_STATUS, $paypalData));
							}

							$this->intercomUser = $user;
							$this->status = Status::ACTIVE;

							/**Si esta pagando un usuario que fue invitado**/
							if(!empty($user->invitation_from) || !empty($user->id_refuser))
							{
							    $user->addExtraMonth($subscription);
							}

							break;
						case 'subscr_failed': //subscription payment failure.
							$title="Pago de Paypal Fallido";
							$message="$".$paypalData['mc_gross']." USD Pago Fallido de ".$user->email1."";
							OjalaUtils::sendSlack($title, $message, "desarrollo-pagos", "paypal", "danger");

							$subscription = Suscription::model()->findByAttributes(array('id_service'=>$paypalData['subscr_id']));

							if($subscription){
								if(!$this->savePayment(
										$subscription->id_suscription,
										$paypalData['option_selection1'],
										$paypalData['mc_gross'],
										$paypalData['subscr_id'], //txn_id no existe en este tipo de Hook
										'usd',
										'PAYPAL',
										Pay::FAILED
									)){
										throw new Exception($this->buildErrorMessage(self::ERROR_FAILED_PAYMENT, $paypalData));
									}

								Mail::sendFailedPayment($user);
							} else {
								$this->sendErrorMail(self::MAIL_ERROR_SUBJECT, self::ERROR_NO_SUBSCRIPTION, $paypalData);
								throw new Exception($this->buildErrorMessage(self::ERROR_NO_SUBSCRIPTION, $paypalData));
							}
							// Happens::pagoFallido($paypalData['payer_email'], 'Paypal');
							break;
					}
					$transaction->commit();
				}
				catch(Exception $e) {
					$transaction->rollback();
					Yii::log('Paypal Error: ' . $e->getMessage() . ' - ' .var_export($paypalData, true), CLogger::LEVEL_ERROR);
				}
				
				$this->syncStatus(); //Afuera para que no dañe la transacción

			} else {
				$this->sendErrorMail(self::MAIL_ERROR_SUBJECT, self::ERROR_NO_USER, $paypalData);
			} //fin de si existe el usuario
			header("Status: 200 OK");
		} //fin de validar  user y correo
		else {
			Yii::log('Paypal Error: IPN inválido - ' . var_export($paypalData, true), CLogger::LEVEL_ERROR);
			header('HTTP/1.0 403 Forbidden');
		}
	}

	/**
	 * Funcion que suspende una suscripción (si existe), se utiliza
	 * en caso de que el usuario cambie de método de pago y evitar
	 * que tenga multiples suscripciones
	 * @param  string $email 	correo del suscriptior
	 */
	public static function cancelSuscriptionIfExists($email)
	{
		$updated = false;
		$user = User::model()->with('suscriptions','suscriptions.type')->find('email1=:email1', array(
			':email1' => $email,
		));

		if(!$user){
			$user = User::model()->with('suscriptions','suscriptions.type')->find('email2=:email2', array(
				':email2' => $email,
			));			
		}

		if(isset($user->suscriptions) && is_array($user->suscriptions))
		{
			foreach ($user->suscriptions as $subscription)
			{
				$isPaypal = strpos($subscription->id_service, 'sub_') === false;
				if($isPaypal && $subscription->type->recurrence == 1 && $subscription->active > 0) {
					$nvpstr = '&TRANSACTIONID=' . $subscription->id_service;
					$resArray = Yii::app()->Paypal->hash_call("GetTransactionDetails",$nvpstr);
					$profileId = $resArray['SUBSCRIPTIONID'];

					$nvpstr = "&PROFILEID=" . $profileId . '&ACTION=Cancel';
					$resArray = Yii::app()->Paypal->hash_call("ManageRecurringPaymentsProfileStatus",$nvpstr);

					if(!$updated) {
						$updated = isset($resArray[' ACK']) && $resArray['ACK']  == 'Success';
					}
				}
			}
		}
		return $updated;
	}
}