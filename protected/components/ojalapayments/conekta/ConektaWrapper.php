<?php

require(dirname(__FILE__) . '/Conekta.php');
Yii::import('application.components.Controller');

class ConektaWrapper implements iPaymentGateway
{
    function __construct() {
        $this->tokenName = 'conektaTokenId';
    }

    private function setConektaApiKey() {
        $key = YII_DEBUG ? "key_VWGhDkHVzdWb9GgM" : "key_qACyejwT455P5UXP";
        Conekta::setApiKey($key);
    }

    public function getConektaCode($cssSelector) {
        $key = YII_DEBUG ? 'key_D1ELL4B89GbhjepJ' : 'key_QqYwjie3J3UTMtQG';

        return 'Conekta.setPublishableKey("' . $key . '");' .
            'function ConektaDataAttributes(){' .
                '$(\'#email\').attr(\'data-conekta\',\'card[name]\');' .
                '$(\'#number\').attr(\'data-conekta\',\'card[number]\');' .
                '$(\'#exp-month\').attr(\'data-conekta\',\'card[exp_month]\');' .
                '$(\'#exp-year\').attr(\'data-conekta\',\'card[exp_year]\');' .
                '$(\'#cvc\').attr(\'data-conekta\',\'card[cvc]\');' .
            '};' .
            'var handleSuccess=function(response){' .
                'var $form = $(\'' . $cssSelector . '\');' .
                'var token = response.id;' .
                '$form.append(\'<input type="hidden" name="' . $this->tokenName . '" value="\' + token + \'"/>\');' .
                '$form.get(0).submit();' .
            '};' .
            'var handleError=function(response){' .
                'var $form = $(\'' . $cssSelector . '\');' .
                '$form.find(\'.payment-errors\').text(response.message);' .
                '$form.find(\'button\').prop(\'disabled\', false);' .
            '};' .
            'jQuery().ready(function(){' .
                    'ConektaDataAttributes();' .
                    '$(\'' . $cssSelector . '\').submit(function(event) ' .
                    '{' .
                        'var $form = $(this);' .
                        '$(\'#button\').prop(\'disabled\', true);' .
                        'Conekta.token.create($form, handleSuccess,handleError);' .
                        'return false;' .
                    '});' .
            '});';
    }

    public function init() {
        $this->setConektaApiKey();
    }

    public function registerScripts($params) {
        Yii::app()->clientScript->registerScriptFile('https://s3.amazonaws.com/conektaapi/v0.3.1/js/conekta.js', CClientScript::POS_END);
        Yii::app()->clientScript->registerScript('coneketaCode', $this->getConektaCode($params), CClientScript::POS_END);
        Yii::app()->clientScript->registerScript('coneketaCodeExtra',
        	"$('#paymentTabs a').click(function (e) {
		e.preventDefault();
		$(this).tab('show');
	});" .
        	'$(function(){' .
        	'$(\'.stripe\').change(function(){' .
        	'if($("#button").attr("disabled") == "disabled"){' .
        		'$("#button").removeAttr("disabled");' .
        		'$(".payment-errors").empty();' .
        	'}' .
        	'});' .
        '});', CClientScript::POS_END);
    }

    /**
     * Función para procesar el pago por Conekta
     *
     * @return string 	El error ocurrido al procesar el pago
     * @return boolean 	Devuelve true si el cargo se pudo hacer y false si no se recibió un token
     */
    public function processPayment($user) {
        if (isset($_GET['conektaTokenId'])) {     //Verificar validez del token de Stripe
            $token = $_GET['conektaTokenId'];

            try {
                if (isset($_GET['plan'])) {     //Verificar si se está adquiriendo una suscripción
                    $plan = $_GET['plan'];

                    $customer = Conekta_Customer::where(array(
                        'email'=>$user->email1,
                        'limit' => 1
                    ));

                    if(!isset($customer[0]->id)){
                        Conekta_Customer::create(array(
                                'name' => Yii::app()->user->name, //Nombre del cliente en conekta
                                'email' => $user->email1,
                                'plan' => $plan,
                                'cards' => array($token)
                        ));
                    }

                    return true;
                } else if (isset($_GET['id'])) {
                    //Verificar si se está comprando un curso
                    $value = $_GET['id'];
                    $course = Course::model()->findByPk($value);

                    $charge = Conekta_Charge::create(array(
                            "description" => $user->email1 . " " . $course->id_course . " " . $course->name,
                            "amount" => OjalaPayments::convertCurrency($course->cost, 'MXN'),
                            "currency" => "MXN",
                            "card" => $token,
                    ));

                    return true;
                }
            } catch (Conekta_Error $e) {
                return $e->getMessage();
            }
        } else {
            return false;
        }
    }

    /**
     * Función para obtener información del cliente
     * @param  integer $id ID del pago realizado
     */
    public function getCustomer($id) {
        $customer = Conekta_Customer::find($id);

        if($customer) {
            return $customer;
        }  else {
           echo(header("Status: 200 OK"));
           exit;
        }
    }

    public function getCharge($id) {
        return Conekta_Charge::find($id);
    }

    public function getInvoice($id) {
        return Conekta_Charge::find($id);
    }

  public function getTokenName()
    {
        return $this->tokenName;
    }

    public function handleException($e) {
        $class = get_class($e);

        switch ($class) {
            case 'Conekta_MalformedRequestError':
                Yii::log('Conekta_MalformedRequestError: ' . $e->message, 'error');
                break;
            case 'Conekta_ProcessingError':
                Yii::log('Conekta_ProcessingError: ' . $e->message, 'error');
                break;
            case 'Conekta_AuthenticationError':
                // Authentication with Conekta's API failed
                // (maybe you changed API keys recently)
                Yii::log('Conekta_AuthenticationError: ' . $e->message, 'error');
                break;
            case 'Conekta_NoConnectionError':
                // Network communication with Conekta failed
                Yii::log('Conekta_NoConnectionError: ' . $e->message, 'error');
                break;
            case 'Conekta_ApiError':
                // Network communication with Conekta failed
                Yii::log('Conekta_ApiError: ' . $e->message, 'error');
                break;
            case 'Conekta_Error':
                // Display a very generic error to the user
                Yii::log('Conekta_Error: ' . $e->message, 'error');
                break;
            case 'Exception':
                // Something else happened, completely unrelated to Conekta
            	   Yii::log('Error  desconocido de Conekta: ' . $e->message, 'error');
                break;
        }
    }

    public function processLandingHook()
    {
        if(isset($_POST['token']) && Yii::app()->request->isAjaxRequest)
        {
            $this->setConektaApiKey();
            $token = $_POST['token'];
            $plan = isset($_POST['plan']) ? $_POST['plan'] : '25-st';
            $amount = isset($_POST['plan']) ? 3700 : 2500;

            try 
            {
                /*$charge = Stripe_Charge::create(array(
                  "amount" => $amount, 
                  "currency" => "USD",
                  "card" => $token,
                  "description" => "Compra de landing de :" . $_POST['email']
                ));*/

                $customer = Conekta_Customer::create(array(
                    "card" => $token,
                    "plan" => $plan,
                    "email" => $_POST['email'])
                );
                
                return 'exito';
            } 
            catch(Stripe_CardError $e) 
            {
                Yii::log('Error al procesar la tarjeta: ' . $e->getMessage(), CLogger::LEVEL_ERROR);
                return 'Ocurrio un error con tu tarjeta, puede ser que tu banco tenga restricciones o tu tarjeta no tenga el cupo disponible, intentalo de nuevo o consulta con tu banco' . $e->getMessage();
            }
        } 
        else 
        {
            Yii::log('Error leyendo las variables', CLogger::LEVEL_ERROR);
            return 'Error procesando el pago, comuniquese via email a fr@oja.la';
        }
    }
    
    /**
     * Función para procesar los webhooks
     */
    public function getEvent($id) {
        //AVERIGUAR COMO SE PASA EL ID COMO PARAMETRO
        return Conekta_Event::where(array('id' => $id));
    }

    public function processPayWebHook($value='')
    {
        $body = @file_get_contents('php://input'); //Obtención de solicitud HTTP desde Conekta
        $payEvent = json_decode($body);     //Transformación a objeto PHP

        $transaction= Yii::app()->db->beginTransaction();

        //Creación de objeto evento de Conekta
        //Válido para cualquier solicitud recibida desde el Webhook de Conekta
        if(!empty($payEvent)){
            try {
                switch($payEvent->type)
                {
                    case 'subscription.payment_failed':
                        $customer = $this->getCustomer($payEvent->data->object->customer_id);
                        $plan = SuscriptionType::model()->findByAttributes(array('id_service'=>$payEvent->data->object->plan_id));
                        $user = User::model()->findByAttributes(array('email1'=>$customer->email));

                        //Comprobar existencia de usuario
                        if($user !== NULL)
                        {
                            $suscription = Suscription::model()->findByAttributes(array('id_user'=>$user->id_user, 'id_stype'=>$plan->id_stype));

                            if($suscription){
                                $s_status = SuscriptionStatus::model()->findByAttributes(array('id_suscription'=>$suscription->id_suscription,'active'=>1));
                                $s_status->id_status = 3;   //Se cambia el estado de la suscripción a deudor.
                                $s_status->update();
                            }
                        }
                        echo(header("Status: 200 OK"));
                        break;

                    case 'subscription.canceled':
                        $customer = $this->getCustomer($payEvent->data->object->customer_id);
                        $user = User::model()->findByAttributes(array('email1'=>$customer->email));
                        $plan = SuscriptionType::model()->findByAttributes(array('id_service'=>$payEvent->data->object->plan_id));

                        if($user){
                            $suscription = Suscription::model()->findByAttributes(array('id_user'=>$user->id_user, 'id_stype'=>$plan->id_stype));
                            if($payEvent->data->object->status==="canceled" && $suscription)
                            {
                                $s_status = SuscriptionStatus::model()->findByAttributes(array('id_suscription'=>$suscription->id_suscription,'active'=>1));
                                if($s_status)
                                {
                                    $s_status->id_status = 2;   //Se cambia el estado de la suscripción a cancelado.
                                    $s_status->active = 0;      //Se desactiva la cuenta.
                                    $s_status->update();

                                    $suscription->active = 0;
                                    $suscription->update();

                                    Happens::cancelado($user->email1, $plan->na_stype);
                                    echo(header("Status: 200 OK")); //Indica a Conekta que todo fue bien
                                }
                            }
                        }
                        break;

                    case 'subscription.paid':
                        $customer = $this->getCustomer($payEvent->data->object->customer_id);
                        $user = User::model()->findByAttributes(array('email1'=>$customer->email));
                        $plan = SuscriptionType::model()->findByAttributes(array('id_service'=>$payEvent->data->object->plan_id));

                        //Comprobar existencia de usuario
                        if($user !== NULL)
                        {
                            $suscription = Suscription::model()->findByAttributes(array('id_user'=>$user->id_user));

                            //Comprobar existencia del plan
                            if($plan != NULL)
                            {
                                //Se crea nueva suscripción si no hay suscripción recurrente o no hay suscripción de ningún tipo.
                                if($suscription === NULL || (SuscriptionType::model()->findByPk($suscription->id_stype)->recurrence == 0))
                                {
                                    //Crear nueva suscripción al ser encontrado el usuario y validado el pago por Conekta
                                    $suscription = new Suscription;
                                    $suscription->id_user = $user->id_user;
                                    $suscription->id_stype = $plan->id_stype;
                                    $suscription->id_ptype = 3; /// Conekta
                                    $suscription->date = new CDbExpression('NOW()');
                                    $suscription->active = 1; // Variable pronto a ser deprecated por status
                                    $suscription->status = 1; // 0-Cancelada, 1-Activa, 2-PENDING
                                    $suscription->display_name = $user->name." ".$user->lastname;
                                    $suscription->paid = true;
                                    $suscription->id_service = $payEvent->data->object->id;

                                    /*Interval debe cambiar por recurrence en el futuro*/
                                    if($plan->interval == "month")
                                        $suscription->exp_date = date('Y-m-d H:i:s', strtotime("+1 month"));
                                    else if($plan->interval == "year")
                                        $suscription->exp_date = date('Y-m-d H:i:s', strtotime("+1 year"));
                                    else
                                        $suscription->exp_date = date('Y-m-d H:i:s', strtotime("+1 month"));

                                    //Guardar suscripción nueva
                                    $suscription->save();

                                    $repuser=RepUser::model()->findByAttributes(array('email1'=>$user->email1));
                                    if($repuser)
                                    {
                                        $rephappen=RepHappen::model()->findByAttributes(array('id_event'=>2, 'id_user'=>$repuser->id_user)); //Registro
                                        if($rephappen)
                                        {
                                            $reputm=RepUtm::model()->findByPk($rephappen->id_utm);
                                            if($reputm && $reputm->utm_campaign=="iphone5s")
                                            {
                                                $courseSusc=CourseSuscription::model()->findByAttributes(array('id_course' => 239,'id_user'=> $user->id_user));
                                                if(!$courseSusc)
                                                {
                                                    $courseSusc=new CourseSuscription;
                                                    $courseSusc->id_course = 239;
                                                    $courseSusc->id_user = $user->id_user;
                                                    $courseSusc->active=1;
                                                    $courseSusc->save();
                                                }
                                            }
                                        }
                                    }

                                    $pay = new Pay;
                                    $pay->id_ptype = PayType::model()->findByAttributes(array('na_ptype' =>'CONEKTA'))->id_ptype;
                                    $pay->id_suscription = $suscription->id_suscription;
                                    $pay->date = new CDbExpression('NOW()');
                                    $pay->email = $user->email1;
                                    $pay->plan = $plan->id_service;
                                    $pay->amount = OjalaPayments::convertCurrency($plan->amount, "MXN");
                                    $pay->status = "Completed";
                                    $pay->code = $payEvent->id;
                                    $pay->save();

                                    /*Inclusión en suscription status*/
                                    $s_status = new SuscriptionStatus;
                                    $s_status->id_suscription = $suscription->id_suscription;
                                    $s_status->date = new CDbExpression('NOW()');
                                    $s_status->id_status = 1;
                                    $s_status->save();

                                    /**Si esta pagando un usuario que fué invitado**/
                                    /*if(!empty($user->invitation_from) || !empty($user->id_refuser)){
                                        $user->addExtraMonth($suscription);
                                    }*/

                                    /* Actualización de tipo de usuario a Estudiante */
                                    $user->id_utype = 3;
                                    $user->update();

                                    Happens::nuevoSuscriptor($user->email1, $plan->na_stype, 'Conekta', 'MXN');
                                }
                            }
                            else
                            {
                                /*Plan no encontrado*/
                                /*Redirect*/
                            }
                        }else{
                            /*Usuario no encontrado*/
                            /*Redirect*/
                        }
                        // echo "\nEl id del plan es: ". $p_lines['data'][0]->plan['id'];
                        // echo "\nEl nombre del plan es: ". $p_lines['data'][0]->plan['name'];
                        break;
                    case 'charge.paid':
                        $description = explode(' ', $payEvent->data->object->description);
                        $email = $description[0];
                        $id_course = $description[1];

                        $user = User::model()->findByAttributes(array('email1'=>$email));

                        if($user)
                        {
                                $payTipeId = PayType::model()->findByAttributes(array('na_ptype' =>'CONEKTA' ))->id_ptype;

                                $suscription=new Suscription;
                                $suscription->id_user=$user->id_user;
                                $suscription->id_stype=1; /// Compra curso
                                $suscription->id_course = $id_course;
                                $suscription->id_ptype= $payTipeId;
                                $suscription->date=new CDbExpression('NOW()');
                                $suscription->active=1;
                                $suscription->status = 1;
                                $suscription->save();

                                $pay = new Pay;
                                $pay->id_ptype = $payTipeId;
                                $pay->id_suscription = $suscription->id_suscription;
                                $pay->date = new CDbExpression('NOW()');
                                $pay->email = $user->email1;
                                $pay->plan = "";
                                $pay->amount = $payEvent->data->object->amount / 100;
                                $pay->status = "Completed";
                                $pay->code = $payEvent->data->object->id;
                                $pay->save();

                                $courseSusc=CourseSuscription::model()->findByAttributes(array('id_course' => $id_course,'id_user'=> $user->id_user));
                                if(!$courseSusc)
                                {
                                    $courseSusc=new CourseSuscription;
                                    $courseSusc->id_course = $id_course;
                                    $courseSusc->id_user = $user->id_user;
                                    $courseSusc->display_name = $user->name +" "+$user->lastname;
                                    $courseSusc->active=1;
                                    $courseSusc->save();
                                }
                                elseif($courseSusc->active==0)
                                {
                                    $courseSusc->active=1;
                                    $courseSusc->update();
                                }

                                $s_status=new SuscriptionStatus;
                                $s_status->id_status=1;
                                $s_status->date = new CDbExpression('NOW()');
                                $s_status->id_suscription=$suscription->id_suscription;
                                $s_status->save();

                                echo(header("Status: 200 OK")); //Indica a Conekta que todo fue bien
                                $course=Course::model()->findByPk($id_course);
                                Happens::nuevaCompra($user->email1, $course->name, 'Conekta', 'MXN');
                            }
                            else
                            {
                                echo "No se encuentra el usuario";
                            }
                            break;
                } // fin switch
                $transaction->commit();
            } catch(Conekta_ProcessingError $e) {
                $this->handleException($e);
                $transaction->rollback();
            } catch (Conekta_MalformedRequestError $e) {
                $this->handleException($e);
                $transaction->rollback();
            } catch (Conekta_AuthenticationError $e) {
                $this->handleException($e);
                $transaction->rollback();
            } catch (Conekta_ApiError $e) {
                $this->handleException($e);
                $transaction->rollback();
            } catch (Conekta_Error $e) {
                $this->handleException($e);
                $transaction->rollback();
            } catch (Exception $e) {
                $this->handleException($e);
                $transaction->rollback();
            }
        } //end if empty
    }

    public function cancelSuscription($user_id,  $susc_id)
    {
        try
                    {
            if(!empty($user_id) && !empty($susc_id))
            {
                $cu = $this->getCustomer($user_id);
                $cu->subscription->cancel();
            }
                }
                catch (Exception $e)
                {
                    $this->handleException($e);
                }
    }

    public function cancelSuscriptionIfExists($e)
    {
        echo 'No implementado';
        die;
    }
}
