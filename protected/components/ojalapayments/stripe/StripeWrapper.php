<?php

/**
 *  Clase para implementar Stripe
 */
require(dirname(__FILE__) . '/Stripe.php');
Yii::import('application.components.Controller');

class StripeWrapper extends OjalaPayments
{
	const WAITING_TIME = 5;
	
	function __construct()
	{
		$this->tokenName = 'stripeToken';
		$this->setStripeApiKey();

		parent::__construct();
	}

	private function setStripeApiKey()
	{
		$key = YII_DEBUG ? "sk_test_rwu1L2JZFYIFL5weBfUzp8YP" : "sk_live_RE8GsQH7UK6IQ3JKSsFywxTI";
		Stripe::setApiKey($key);
	}

	public function getStripeCode($cssSelector)
	{
		$key = YII_DEBUG ? 'pk_test_PquLfiHIRJDiZXSLhNtDAl3a' : 'pk_live_A8Ri0rI6Ux0McPCK4H6lLT24';

		return 'Stripe.setPublishableKey("' . $key . '");' .
			'function StripeDataAttributes(){' .
				'$(\'#number\').attr(\'data-stripe\',\'number\');' .
				'$(\'#exp-month\').attr(\'data-stripe\',\'exp-month\');' .
				'$(\'#exp-year\').attr(\'data-stripe\',\'exp-year\');' .
				'$(\'#cvc\').attr(\'data-stripe\',\'cvc\');' .
			'};' .
			'var handler=function(status, response){' .
			'var $form = $(\'' . $cssSelector . '\');' .
				'if(response.error){' .
					'var message;' .
					'switch (response.error.message) {' .
						'case "This card number looks invalid":' .
							'message = "El número de la tarjeta es inválido";' .
							'break;' .
						'case "Your card\'s expiration month is invalid.":'.
							'message = "La fecha de vencimiento es inválida";' .
							'break;' .
						'case "Your card\'s security code is invalid.":'.
							'message = "El código de verificación es inválido";' .
							'break;' .
						'case "Your card\'s security code is incorrect.":'.
							'message = "El código de verificación es incorrecto";' .
							'break;' .
						'case "Your card number is incorrect.":' .
							'message = "El número de su tarjeta es incorrecto";' .
							'break;' .
						'case "Your card was declined.":' .
							'message = "Tu tarjeta fue rechazada";' .
							'break;' .
						'case "Your card does not support this type of purchase.":' .
							'message = "Tu tarjeta no soporta este tipo de compra";' .
							'break;' .
						'case "Invalid amount.":' .
							'message = "Lo sentimos. No podemos procesar tu tarjeta. contacta a tu banco.";' .
							'break;' .
						'case "Your card has expired.":' .
							'message = "Tu tarjeta ha expirado";' .
							'break;' .
						'case "An error occurred while processing your card. Try again in a little bit.":' .
							'message = "Ocurrió un error mientras se procesaba la tarjeta. Intenta de nuevo en unos momentos.";' .
							'break;' .
						'default:' .
							'message = response.error.message;' .
							'break;' .
					'};' .
					'$form.find(\'.payment-errors\').text(message);' .
					'$form.find(\'button\').prop(\'disabled\', false);' .
				'}else {' .
					'var token = response.id;' .
					'$form.append($(\'<input type="hidden" name="' . $this->tokenName . '" />\').val(token));' .
					'$form.get(0).submit();' .
				'}' .
			'};' .
			'jQuery(function($){' .
				'StripeDataAttributes();' .
					'$(\'' . $cssSelector . '\').submit(function(e) ' .
					'{' .
						'e.preventDefault();' .
						'if($("#email").val()){' .
							'$("#email").removeClass("error");' .
							'$("#formemail .help-block").css("color","#737373");' .
							'var $form = $(this);' .
							'$(\'#button\').prop(\'disabled\', true);' .
							'Stripe.card.createToken($form, handler);' .
						'} else {' .
							'$("#formemail .help-block").css("color","#FF0000");' .
							'$("#email").addClass("error");' .
						'} ' .
					'});' .
			'});';
	}

	public function registerScripts($params) {
		Yii::app()->clientScript->registerScriptFile('https://js.stripe.com/v2/', CClientScript::POS_END);
		Yii::app()->clientScript->registerScript('stripeCode', $this->getStripeCode($params), CClientScript::POS_END);
		Yii::app()->clientScript->registerScript('stripeCodeExtra',
			"$('#paymentTabs a').click(function (e) {
			e.preventDefault();
			$(this).tab('show');
			});" .
			'$(function(){' .
				'$(\'.stripe\').change(function(){' .
				'if($("#button").attr("disabled") == "disabled"){' .
					'$("#button").removeAttr("disabled");' .
					'$(".payment-errors").empty();' .
				'}' .
			'});' .
		'});', CClientScript::POS_END);
	}

	/**
	 * Función para procesar el pago por Stripe
	 *
	 * @return string 	El error ocurrido al procesar el pago
	 * @return boolean 	Devuelve true si el cargo se pudo hacer y false si no se recibió un token
	 */
	public function processPayment($user)
	{
		if (isset($_GET['stripeToken'])) {     //Verificar validez del token de Stripe
			$token = $_GET['stripeToken'];

			try {
				if (isset($_GET['plan'])) {     //Verificar si se está adquiriendo una suscripción
					$plan = $_GET['plan'];

					$customer = Stripe_Customer::create(array(
						"card" => $token,
						"plan" => $plan,
						"email" => $user->email1)
					);

					return true;
				} else if (isset($_GET['id'])) {
					//Verificar si se está comprando un curso
					$value = $_GET['id'];
					$course = Course::model()->findByPk($value);
					$currency = strtolower(Yii::app()->session['currency']);
					$descr = $user->email1 . " " . $course->id_course . " " . $course->name;

					$charge = Stripe_Charge::create(array(
						"amount" => $course->cost,
						"currency" => $currency,
						"card" => $token,
						"description" => $descr
					));

					return true;
	  		}
    	} catch (Stripe_CardError $e) {
				Happens::pagoFallido($user->email1, 'Stripe');
				return $e->getMessage();
			}
		} else {
			return false;
		}
	}

	public function handleException($e)
	{
		$class = get_class($e);

		switch($class)
		{
			case 'Stripe_CardError':
				// Since it's a decline, Stripe_CardError will be caught
				Yii::log('Stripe_CardError: ' . $e->getMessage(), CLogger::LEVEL_ERROR);
				break;
			case 'Stripe_InvalidRequestError':
				// Invalid parameters were supplied to Stripe's API
				Yii::log('Stripe_InvalidRequestError: ' . $e->getMessage(), CLogger::LEVEL_ERROR);
				break;
			case 'Stripe_AuthenticationError':
				// Authentication with Stripe's API failed
				// (maybe you changed API keys recently)
				Yii::log('Stripe_AuthenticationError: ' . $e->getMessage(), CLogger::LEVEL_ERROR);
				break;
			case 'Stripe_ApiConnectionError':
				// Network communication with Stripe failed
				Yii::log('Stripe_ApiConnectionError: ' . $e->getMessage(), CLogger::LEVEL_ERROR);
				break;
			case 'Stripe_Error':
				// Display a very generic error to the user, and maybe send
				// yourself an email
				Yii::log('Stripe_Error: ' . $e->getMessage(), CLogger::LEVEL_ERROR);
				break;
			default:
				// Something else happened, completely unrelated to Stripe
				Yii::log('Error: ' . $e->getMessage(), CLogger::LEVEL_ERROR);
				break;
		}
	}

	public function processPayWebHook()
	{
		$body = @file_get_contents('php://input');	//Obtención de solicitud HTTP desde Stripe
		$event_json = json_decode($body, true); 	//Transformación a objeto PHP

		$transaction= Yii::app()->db->beginTransaction();

		//Creación de objeto evento de Stripe
		//Válido para cualquier solicitud recibida desde el Webhook de Stripe
		try
		{
			switch($event_json['type'])
			{
				case 'customer.subscription.created':  //Suscripcion Nueva
					$payEvent = Stripe_Event::retrieve($event_json['id']);
					$customer = Stripe_Customer::retrieve($payEvent->data->object['customer']);

					$user = User::model()->findByAttributes(array('email1'=>$customer['email']));
					$user->id_service=$customer['id'];
					$user->id_utype = User::TYPE_STUDENT; // Actualización del usuario a Estudiante

					if(!$user->update()){
						throw new Exception($this->buildErrorMessage(self::ERROR_FAILED_USER, $event_json));
					}

					$plan = SuscriptionType::model()->findByAttributes(array('id_service'=>$payEvent->data->object->plan['id']));
					if(!$plan){
						throw new Exception($this->buildErrorMessage(self::ERROR_NO_PLAN, $event_json));
					}

					//Si ya tiene una suscripción avisa que esta creando una nueva
					if(User::hasActiveSubscription($user->id_user, 'stripe'))
					{
						$this->sendErrorMail(self::MAIL_ERROR_SUBJECT, self::ERROR_MULTIPLE_SUBSCRIPTION, $event_json);
					}

					$suscription = new Suscription;
					$suscription->id_user = $user->id_user;
					$suscription->id_stype = $plan->id_stype;
					$suscription->id_ptype = self::TYPE_STRIPE;
					$suscription->date = new CDbExpression('NOW()');
					$suscription->active = 1; // Variable pronto a ser deprecated por status
					$suscription->display_name = $user->fullname;
					$suscription->paid = true;
					$suscription->id_service = $payEvent->data->object['id'];
					$suscription->exp_date = $this->getExpDate($plan->interval);

					//Cancela cualquier suscripción Paypal existente
					PaypalWrapper::cancelSuscriptionIfExists($user->email1);

					if(!$suscription->save()){
						throw new Exception($this->buildErrorMessage(self::ERROR_FAILED_SUBSCRIPTION, $event_json));
					}

					Happens::correoSuscriptor('Suscripción: '.$plan->na_stype.' Metodo: Stripe', $user->email1);
					Happens::nuevoSuscriptor($user->email1, $plan->na_stype, 'STRIPE', $payEvent->data->object->plan['currency']);
					break;
				case 'customer.subscription.updated': // Renovacion de la Suscripcion, Cambio de plan /
					$payEvent = Stripe_Event::retrieve($event_json['id']);
					$customer = Stripe_Customer::retrieve($payEvent->data->object['customer']);
					$subscription = $customer->subscriptions->retrieve($payEvent->data->object['id']);
					$user = User::model()->findByAttributes(array('email1'=>$customer->email));
					$plan = SuscriptionType::model()->findByAttributes(array('id_service'=>$subscription->plan['id']));
					$suscription = Suscription::model()->findByAttributes(array('id_service'=>$subscription->id));

					if(!$suscription){
						Yii::log(self::ERROR_NO_USER . ' ' .$customer->email, CLogger::LEVEL_ERROR);
						exit();
					}

					if(isset($payEvent->data->previous_attributes['current_period_end']))
					{
						$suscription->exp_date = $this->getExpDate($plan->interval);

						if(!$suscription->update()){
							throw new Exception($this->buildErrorMessage(self::ERROR_FAILED_SUBSCRIPTION, $event_json));
						}
					}
					elseif(isset($payEvent->data->previous_attributes['status']))
					{
						if($subscription->status === "unpaid" or $subscription->status === "past_due")
						{
							if($suscription->active != 2)
							{
								if(!$this->saveStatus($suscription->id_suscription, Status::DEBT)){
									throw new Exception($this->buildErrorMessage(self::ERROR_FAILED_STATUS, $event_json));
								}

								$suscription->active = 2;

								$this->intercomUser = $user;
								$this->status = Status::DEBT;

								if(!$suscription->update()){
									throw new Exception($this->buildErrorMessage(self::ERROR_FAILED_SUBSCRIPTION, $event_json));
								}

								Happens::deudor($user->email1, $plan->na_stype);
							}
						}
						else if($subscription->status === "active")
						{
							if(!$this->saveStatus($suscription->id_suscription)){
								throw new Exception($this->buildErrorMessage(self::ERROR_FAILED_STATUS, $event_json));
							}

							$this->intercomUser = $user;
							$this->status = Status::ACTIVE;

							if($suscription->active != 1)
							{
								$suscription->active = 1;
								if(!$suscription->update()){
									throw new Exception($this->buildErrorMessage(self::ERROR_FAILED_SUBSCRIPTION, $event_json));
								}
							}

							Happens::activado($user->email1, $plan->na_stype);
						}
					} 
					elseif(isset($payEvent->data->previous_attributes['plan']))
					{
						$plan = SuscriptionType::model()->findByAttributes(array('id_service'=>$event_json['data']['object']['plan']['id']));

						if($plan){
							$suscription->id_stype = $plan->id_stype;
							$suscription->update();
						} else {
							throw new Exception($this->buildErrorMessage(self::ERROR_NO_SUBSCRIPTION_DB, $event_json));
						}
					}
					break;
				case 'customer.subscription.deleted': // Cancelado
					$plan = SuscriptionType::model()->findByAttributes(array('id_service'=>$event_json['data']['object']['plan']['id']));
					$suscription = Suscription::model()->findByAttributes(array('id_service'=>$event_json['data']['object']['id']));

					if(!$suscription)
					{
						Yii::log('Falló la cancelación de suscripcion con Stripe: ' . var_export($event_json, true), CLogger::LEVEL_ERROR);
					}
					else
					{
						$user = User::model()->findByPk($suscription->id_user);

						if(!$this->saveStatus($suscription->id_suscription, Status::CANCELED)){
							throw new Exception($this->buildErrorMessage(self::ERROR_FAILED_STATUS, $event_json));
						}

						$this->intercomUser = $user;
						$this->status = Status::CANCELED;

						$suscription->active = 0;

						if(!$suscription->update()){
							throw new Exception($this->buildErrorMessage(self::ERROR_FAILED_SUBSCRIPTION, $event_json));
						}

						Happens::cancelado($user->email1, $plan->na_stype);
					}
					break;
				case 'invoice.payment_succeeded': //Pago de una suscripcion recurrente
					sleep(self::WAITING_TIME);
					$payEvent = Stripe_Event::retrieve($event_json['id']);
					$payment = Stripe_Invoice::retrieve($payEvent->data->object['id']);
					$p_lines = Stripe_Invoice::retrieve($payment->id)->lines->all(array('count'=>1)); //Obtención de información de plan adquirido
					$customer = Stripe_Customer::retrieve($payment->customer);
					$suscription = Suscription::model()->findByAttributes(array('id_service'=>$payEvent->data->object['subscription']));

					if(!$suscription) {
						throw new Exception($this->buildErrorMessage(self::ERROR_NO_SUBSCRIPTION_DB, $event_json));
					}

					$user = User::model()->findByPk($suscription->id_user);

					if(!$this->savePayment(
						$suscription->id_suscription,
						$customer->email,
						$p_lines['data'][0]->plan['amount'],
						$payment->charge,
						$event_json['data']['object']['currency']
					)){
						throw new Exception($this->buildErrorMessage(self::ERROR_FAILED_PAYMENT, $event_json));
					}

					if(!$this->saveStatus($suscription->id_suscription))
					{
						throw new Exception($this->buildErrorMessage(self::ERROR_FAILED_STATUS, $event_json));
					}

					$this->intercomUser = $user;
					$this->status = Status::ACTIVE;

		    		/**Si esta pagando un usuario que fue invitado**/
					if(!empty($user->invitation_from) || !empty($user->id_refuser))
					{
					    $user->addExtraMonth($suscription);
					}

					break;  //Pago de una suscripcion existente //
				case 'charge.succeeded': // Pago de una nueva compra de un curso
					$payEvent = Stripe_Event::retrieve($event_json['id']);
					$charge = Stripe_Charge::retrieve($payEvent->data->object['id']);
					if(!empty($charge->description))
					{
						$description = explode(" ", $charge->description);
						$email = $description[0];
						$id_course = $description[1];
						$user = User::model()->findByAttributes(array('email1'=>$email));

						if($user)
						{
							$suscription=new Suscription;
							$suscription->id_user=$user->id_user;
							$suscription->id_stype=1; /// Compra curso
							$suscription->id_course = $id_course;
							$suscription->id_ptype= 2; /// Stripe
							$suscription->date=new CDbExpression('NOW()');
							$suscription->active=1;
							$suscription->status = Status::ACTIVE;

							if($suscription->save())
							{
								if(!$this->savePayment(
									$suscription->id_suscription,
									$user->email1,
									$charge->amount,
									$charge->id,
									$event_json['data']['object']['currency']
								)){
									throw new Exception($this->buildErrorMessage(self::ERROR_FAILED_PAYMENT, $event_json));
								}

								//Happens::pago($user->email1, SuscriptionType::model()->findByAttributes(array('id_stype' =>'1' ))->na_stype, 'Stripe', 'USD', $pay->amount);

								$courseSusc=CourseSuscription::model()->findByAttributes(array(
									'id_course' => $id_course,
									'id_user'=> $user->id_user
								));

								if(!$courseSusc)
								{
									$courseSusc=new CourseSuscription;
									$courseSusc->id_course = $id_course;
									$courseSusc->id_user = $user->id_user;
									$courseSusc->active=1;
								}
								elseif($courseSusc->active == 0)
								{
									$courseSusc->active=1;
								}

								if(!$courseSusc->save()){
									throw new Exception($this->buildErrorMessage(self::ERROR_FAILED_COURSE, $event_json));
								}

								if(!$this->saveStatus($suscription->id_suscription)){
									throw new Exception($this->buildErrorMessage(self::ERROR_FAILED_STATUS, $event_json));
								}

								$this->intercomUser = $user;
								$this->status = Status::ACTIVE;

								$course=Course::model()->findByPk($id_course);
								//Happens::nuevaCompra($user->email1, $course->name, 'Stripe', 'USD');
							}
						} else {
							$this->sendErrorMail(self::MAIL_ERROR_SUBJECT, self::ERROR_NO_USER, $event_json);
							throw new Exception($this->buildErrorMessage(self::ERROR_NO_USER, $event_json));
						}
					}
					break;
				case 'charge.failed': //Pago Fallido
					if(empty($event_json['data']['object']['customer'])){
						if(!$this->savePayment(
							NULL,
							NULL,
							$event_json['data']['object']['amount'],
							$event_json['id'],
							$event_json['data']['object']['currency'],
							'STRIPE',
							Pay::FAILED
						)){
							throw new Exception($this->buildErrorMessage(self::ERROR_FAILED_PAYMENT, $event_json));
						}
					}
					break;
				case 'invoice.payment_failed': // Pago fallido de una Suscripcion
					sleep(self::WAITING_TIME); //Por si llega
					$customer = Stripe_Customer::retrieve($event_json['data']['object']['customer']);
					$user = User::model()->findByAttributes(array('email1'=>$customer->email));

					if($user)
					{
						$subIndex = 0;

						for ($subIndex=0; $subIndex < count($event_json['data']['object']['lines']['data']) ; $subIndex++) { 
							if(substr($event_json['data']['object']['lines']['data'][$subIndex]['id'], 0, 4) == 'sub_'){
								break;
							}
						}

						$suscription = Suscription::model()->findByAttributes(array(
							'id_service' => $event_json['data']['object']['lines']['data'][$subIndex]['id']
						));

						if($suscription){
							//Se guarda el pago Fallido
							if(!$this->savePayment(
								$suscription->id_suscription,
								$user->email1,
								$event_json['data']['object']['lines']['data'][$subIndex]['plan']['amount'],
								$event_json['data']['object']['customer'],
								$event_json['data']['object']['currency'],
								'STRIPE',
								Pay::FAILED
							)){
								throw new Exception($this->buildErrorMessage(self::ERROR_FAILED_PAYMENT, $event_json));
							}

							Mail::sendFailedPayment($user);
						} else {
							$this->sendErrorMail(self::MAIL_ERROR_SUBJECT, self::ERROR_NO_SUBSCRIPTION, $event_json);
							throw new Exception($this->buildErrorMessage(self::ERROR_NO_SUBSCRIPTION, $event_json));
						}
					} else {
						$this->sendErrorMail(self::MAIL_ERROR_SUBJECT, self::ERROR_NO_USER, $event_json);
						throw new Exception($this->buildErrorMessage(self::ERROR_NO_USER, $event_json));
					}
					break;
			}
			$transaction->commit();
		}
		catch (Exception $e)
		{
		  	$this->handleException($e);
		  	$transaction->rollback();
		}
			
		$this->syncStatus(); //Afuera para que no dañe la transacción
	}

	/**
	 * Permite realizar un cargo a partir de un token
	 * @return boolean Si la compra fue realizada
	 */
	public function processLandingHook()
	{
		if(isset($_POST['token']) && Yii::app()->request->isAjaxRequest)
		{
			$this->setStripeApiKey();
			$token = $_POST['token'];
			$plan = isset($_POST['plan']) ? $_POST['plan'] : '25-st';
			$amount = isset($_POST['plan']) ? 3700 : 2500;

			try
			{
				$customer = Stripe_Customer::create(array(
					"card" => $token,
					"plan" => $plan,
					"email" => $_POST['email'])
				);

				return 'exito';
			}
			catch(Stripe_CardError $e)
			{
				Yii::log('Error al procesar la tarjeta: ' . $e->getMessage(), CLogger::LEVEL_ERROR);
				return 'Ocurrio un error con tu tarjeta, puede ser que tu banco tenga restricciones ' .
					'o tu tarjeta no tenga el cupo disponible, intentalo de nuevo o consulta con tu banco'
					. $e->getMessage();
			}
		}
		else
		{
			Yii::log('Error leyendo las variables', CLogger::LEVEL_ERROR);
			return 'Error procesando el pago, comuniquese via email a fr@oja.la';
		}
	}

	public function cancelSuscription($user_id,  $susc_id)
	{
		try
            		{
			if(!empty($user_id) && !empty($susc_id))
			{
			    $cu = Stripe_Customer::retrieve($user_id);
			    $cu->subscriptions->retrieve($susc_id)->cancel();
			}
		}
		catch (Exception $e)
		{
			$this->handleException($e);
		}
	}

	public static function translateString($response)
	{
		$message = '';
		switch ($response) {
			case "This card number looks invalid":
				$message = "El número de la tarjeta es inválido";
				break;
			case "Your card\'s expiration month is invalid.":
				$message = "La fecha de vencimiento es inválida";
				break;
			case "Your card\'s security code is invalid.":
				$message = "El código de verificación es inválido";
				break;
			case "Your card\'s security code is incorrect.":
				$message = "El código de verificación es incorrecto";
				break;
			case "Your card number is incorrect.":
				$message = "El número de su tarjeta es incorrecto";
				break;
			case "Your card was declined.":
				$message = "Tu tarjeta fue rechazada";
				break;
			case "Invalid amount.":
				$message = "Lo sentimos. No podemos procesar tu tarjeta. contacta a tu banco.";
				break;
			case "Your card does not support this type of purchase.":
				$message = "Tu tarjeta no soporta este tipo de compra";
				break;
			case "Your card has expired.":
				$message = "Tu tarjeta ha expirado";
				break;
			case 'An error occurred while processing your card. Try again in a little bit.':
				$message = 'Ocurrió un error mientras se procesaba la tarjeta. Intenta de nuevo en unos momentos.';
				break;
			default:
				$message = $response;
				break;
		}

		return $message;
	}


	/**
	 * Funcion que suspende una suscripción (si existe), se utiliza
	 * en caso de que el usuario cambie de método de pago y evitar
	 * que tenga multiples suscripciones
	 * @param  string $email 	correo del suscriptior
	 */
	public static function cancelSuscriptionIfExists($email)
	{
		$res = false;
		$updated = false;
		$user = User::model()->with('suscriptions','suscriptions.type')->find('email1=:email1', array(
			':email1' => $email,
		));

		if(!$user){
			 $user = User::model()->with('suscriptions','suscriptions.type')->find('email2=:email2', array(
				':email2' => $email,
			));			
		}

		$gw = new self; //Permite activar el key del api

		if(isset($user->suscriptions) && is_array($user->suscriptions))
		{
			foreach ($user->suscriptions as $suscription)
			{
				$isStripe = strpos($suscription->id_service, 'sub_') === 0;
				if($isStripe && $suscription->type->recurrence == 1 && $suscription->active > 0) {
					$customer = Stripe_Customer::retrieve($user->id_service);

					try {
						$stripeSuscription = $customer->subscriptions->retrieve($suscription->id_service);
						$res = $stripeSuscription->cancel();
					} catch(Exception $e) {
						$updated = false;
					}

					if(!$updated) {
						$updated = $res;
					}
				}
			}
		}
		return $updated;
	}
}