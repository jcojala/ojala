<?php
/**
 * Controller is the customized base controller class.
 * All controller classes for this application should extend from this base class.
 */
class Controller extends CController
{
	/**
	 * @var string the default layout for the controller view. Defaults to '//layouts/column1',
	 * meaning using a single column layout. See 'protected/views/layouts/column1.php'.
	 */
	public $layout='//layouts/global';
	/**
	 * @var array context menu items. This property will be assigned to {@link CMenu::items}.
	 */
	public $menu=array();
	/**
	 * @var array the breadcrumbs of the current page. The value of this property will
	 * be assigned to {@link CBreadcrumbs::links}. Please refer to {@link CBreadcrumbs::links}
	 * for more details on how to specify this property.
	 */
	public $breadcrumbs=array();

	public function getUtms()
	{
		return $utms = array(
			'Campaign Source' => isset(Yii::app()->request->cookies['utm_source']->value) ? Yii::app()->request->cookies['utm_source']->value : null,
			'Campaign Medium' => isset(Yii::app()->request->cookies['utm_medium']->value) ? Yii::app()->request->cookies['utm_medium']->value : null,
			'Campaign Term' => isset(Yii::app()->request->cookies['utm_term']->value) ? Yii::app()->request->cookies['utm_term']->value : null,
			'Campaign Content' => isset(Yii::app()->request->cookies['utm_content']->value) ? Yii::app()->request->cookies['utm_content']->value : null,
			'Campaign Name' => isset(Yii::app()->request->cookies['utm_campaign']->value) ? Yii::app()->request->cookies['utm_campaign']->value : null,
			'Campaign Name' => isset(Yii::app()->request->cookies['utm_campaign']->value) ? Yii::app()->request->cookies['utm_campaign']->value : null,
			'URL' => isset(Yii::app()->request->cookies['url']->value) ? Yii::app()->request->cookies['url']->value : null,
		);
	}

	public static  function getUtmStatic(){
		$controller = new Controller('utms');
		return $controller->getUtms();
	}
}
