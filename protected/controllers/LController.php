<?php

class LController extends Controller
{
    public $layout='//layouts/landings';

    public function actionunbounce()
    {
        $this->redirect("http://el.oja.la/iphone5-1");
    }

    public function actionVero()
    {
        $this->render('vero');
    }

    public function actionFacebook()
    {
        Yii::app()->session['facebook'] = "true"; 
        $this->abrirLanding('facebook', '51b7455cb4');
    }

    //Landing para probar el flujo de Udemy y los cupones
    public function actionCupon()
    {
        Yii::app()->session['facebook'] = "true"; 
        $this->abrirLanding('cupon', 'fa2c3ceeb2');
    } 

    public function actionPromou()
    {
        Yii::app()->session['mailchimp'] = "f268504c03"; 
        $this->abrirLanding('promou', 'f268504c03');
    }

    private function abrirLanding($pagina, $mailchimp)
    {
        OjalaUtils::createCookieUtms();
        Yii::app()->facebook->run();
        $facebookUser = Yii::app()->facebook->getUser();

        if ($facebookUser) 
        {
            $user=null;
            $fb_user_profile = Yii::app()->facebook->getUserProfile();
            if(isset($fb_user_profile['email']))
            {
                $email=$fb_user_profile['email'];
                $user=User::model()->findByAttributes(array('email1'=>$email));
                if($user)
                {
                    $identity=new UserIdentity($fb_user_profile['email'], '', 1);
                    $identity->authenticate();
                    Yii::app()->user->login($identity);
                    Happens::inicioSesion(Yii::app()->user->email);
                }
                else
                {
                    $campana="";
                    if(isset($_GET['utm_campaign']))
                    {
                        $campaign=Campaign::model()->findByAttributes(array('utm_campaign'=>$_GET['utm_campaign'], 'utm_medium'=>$_GET['utm_medium'], 'utm_term'=>$_GET['utm_term'], 'utm_content'=>$_GET['utm_content'], 'utm_source'=>$_GET['utm_source']));
                        if(!$campaign)
                        {
                            $campaign = new Campaign;
                            if(isset($_GET['utm_source']))
                                $campaign->utm_source=$_GET['utm_source'];
                            if(isset($_GET['utm_medium']))
                                $campaign->utm_medium=$_GET['utm_medium'];
                            if(isset($_GET['utm_term']))
                                $campaign->utm_term=$_GET['utm_term'];
                            if(isset($_GET['utm_content']))
                                $campaign->utm_content=$_GET['utm_content'];
                            if(isset($_GET['utm_campaign']))
                                $campaign->utm_campaign=$_GET['utm_campaign'];
                            $campaign->save();
                        }
                        $campana=$campaign->id_campaign;
                    }

                    $user = new User;
                    $user->name=$fb_user_profile['first_name'];
                    $user->lastname=$fb_user_profile['last_name'];
                    $user->email1=$fb_user_profile['email'];
                    if($campana!="")
                        $user->id_campaign=$campana;
                    $utype = UserType::model()->findByAttributes(array('na_utype'=>'Estudiante'));
                    $user->id_utype=$utype->id_utype;
                    $user->user=$fb_user_profile['email'];
                    $user->create=new CDbExpression('NOW()');
                    $user->pass="";
                    $user->verifcode= hash_hmac('sha256',microtime().$user->pass, 'dlfkgknbcvjkbsdkjflsdkhfdf34534jkHL$@#K$^kb');
                    $user->save(false);

                    $activation_url = 'https://' . $_SERVER['HTTP_HOST'].Yii::app()->urlManager->createUrl('site/cambiarClave', array("verifcode" => $user->verifcode,"id" => $user->id_user));
                    try 
                    {
                        $mandrill = new MandrillWrapper();
                        $template_name = 'Welcome';
                        $result = $mandrill->templates->info($template_name);
                        $message = array(
                            'html' => '<span>Hola '.$user->name.'</span><br>'.$result['code'].'<br><a href='.$activation_url.'> >>> Para cambiar tu contraseña, clickea el siguiente <<< </a>',
                            'text' => 'Hola '.$user->name.', \n '.$result['text'].'\n'.$activation_url,
                            'subject' => 'Hola '.$user->name.' '.$result['subject'],
                            'from_email' => $result['from_email'],
                            'from_name' => $result['from_name'],
                            'to' => array(
                                array(
                                    'email' => $user->email1,
                                    'name' => $user->name,
                                    'type' => 'to'
                                )
                            ),
                            'headers' => array('Reply-To' => $result['from_email'])
                        );
                        $result = $mandrill->messages->send($message, false);
                    }
                    catch(Mandrill_Error $e)
                    {
                        //echo 'A mandrill error occurred: ' . get_class($e) . ' - ' . $e->getMessage();
                        //throw $e;
                    }

                    $utms=array();
                    if(isset($_GET['utm_campaign']))
                    {
                        $utms=array('UTM_SOURCE'=>$_GET['utm_source'], 'UTM_MEDIUM'=>$_GET['utm_medium'], 'UTM_TERM'=>$_GET['utm_term'], 'UTM_CONTEN'=>$_GET['utm_content'], 'UTM_CAMPN'=>$_GET['utm_campaign']);
                    }

                    require_once 'MailChimp.php';

                    $MailChimp = new MailChimp('1b6d2d0238718961fb3bf87d131141e9-us6');

                    $result = $MailChimp->call('lists/subscribe', array(
                        'id'                => $mailchimp,
                        'email'             => array('email'=>$user->email1),
                        'merge_vars'        => array('FNAME'=>$user->name, 'LNAME'=>$user->lastname) + $utms,
                        'double_optin'      => false,
                        'update_existing'   => true,
                        'replace_interests' => false,
                        'send_welcome'      => false,
                    ));
                    
                    $identity=new UserIdentity($fb_user_profile['email'], '', 1);
                    $identity->authenticate();
                    Yii::app()->user->login($identity);
                    Happens::registro(Yii::app()->user->email);
                }
                Happens::vioLanding($user->email1, $pagina);
            }
            else
            {
                Happens::vioLanding('', $pagina);
            }

            $model=new User;
            $this->render($pagina, array('model'=>$model));
        }
        elseif(isset($_GET['dev']))
        {
            Happens::vioLanding('', $pagina);

            $model=new User;
            $this->render($pagina, array('model'=>$model));
        }
        else
        {
            $redirectUrl = Yii::app()->params['fbAppUrl'];
            $redirectUrl .= isset($_SERVER["QUERY_STRING"]) ? urlencode('?'.$_SERVER["QUERY_STRING"]) : '';
            
            $fbLoginUrl = sprintf("https://www.facebook.com/dialog/oauth?client_id=%s&redirect_uri=%s&scope=email",
                Yii::app()->params['fbAppId'], $redirectUrl); 

            //Redirects to FB Auth page
            echo "<script>top.location.href=\"{$fbLoginUrl}\";</script>";
        }
    }

    //Landing para el diploamdo de Desarrollo web
    public function actionLanding($id)
    {
        OjalaUtils::createCookieUtms();
        Happens::vioLanding('', 'landingChat');
        
        $campaign = CmpCampaign::model()
            ->with(
                    'questions','questions.answers',
                    'basicCourse','basicCourse.lessons','basicCourse.lessons.topics'
                )
            ->findByPk($id);

        $this->renderPartial('landing', array(
            'campaign'=>$campaign
        ), false, true);
    }

    //Landing para el diploamdo de Desarrollo web
    public function actionDiplomadoWeb()
    {
        $this->layout = 'landingMenu';
        OjalaUtils::createCookieUtms();
        Happens::vioLanding('', 'diplomadoweb');

        $model=new User;
        $this->render('diplomadoweb', array('model'=>$model));
    }

    //Landing para el diploamdo de SocialMedia
    public function actionDiplomadoSm()
    {
        $this->layout = 'landingMenu';
        OjalaUtils::createCookieUtms();
        Happens::vioLanding('', 'diplomadosm');

        $model=new User;
        $this->render('diplomadosm', array('model'=>$model));
    }

    public function actionChat()
    {
        OjalaUtils::createCookieUtms();
        Happens::vioLanding('', 'chat');
        
        $model=new User;
        $this->render('chat', array('model'=>$model));
    }

    //html para un invoice
    public function actionInvoice()
    {

       if(isset($_GET['idu']) && isset($_GET['is']) && isset($_GET['ip']))
       {
            $user = User::model()->findByPk($_GET['idu']);
            $pay  = Pay::model()->findByPk($_GET['ip']);
            $susc = Suscription::model()->findByPk($_GET['is']);
            $st = SuscriptionType::model()->findByPk($susc->id_stype);
            $mPDF1 = Yii::app()->ePdf->mpdf('', 'Letter');    
            $stylesheet = file_get_contents('css/invoice.css');
            $mPDF1->WriteHTML($stylesheet, 1);
            $mPDF1->WriteHTML($this->render('invoice', array('user'=>$user, 'pay'=>$pay, 'susc'=>$susc, 'st'=>$st), true) , 0, true, false );
            $mPDF1->Output();   

        }else{

        }                

    }

    //Landing para el pago con Stripe - Checkout 
    public function actionTips()
    {
        OjalaUtils::createCookieUtms();
        Happens::vioLanding('', 'tips');
        
        $model=new User;
        $this->render('tips', array('model'=>$model));
    }

    public function actionTipLlamadas()
    {
        OjalaUtils::createCookieUtms();
        Happens::vioLanding('', 'tipLlamadas');
        
        $model=new User;
        $this->render('tipLlamadas', array('model'=>$model));
    }

    public function actionTipSecretaria()
    {
        OjalaUtils::createCookieUtms();
        Happens::vioLanding('', 'tipSecretaria');
        
        $model=new User;
        $this->render('tipSecretaria', array('model'=>$model));
    }

    public function actionTipBateria()
    {
        OjalaUtils::createCookieUtms();
        Happens::vioLanding('', 'tipBateria');
        
        $model=new User;
        $this->render('tipBateria', array('model'=>$model));
    }

    public function actionTipColores()
    {
        OjalaUtils::createCookieUtms();
        Happens::vioLanding('', 'tipColores');
        
        $model=new User;
        $this->render('tipColores', array('model'=>$model));
    }

    public function actionTipCargar()
    {
        OjalaUtils::createCookieUtms();
        Happens::vioLanding('', 'tipCargar');
        
        $model=new User;
        $this->render('tipCargar', array('model'=>$model));
    }

    public function actionTipUbicar()
    {
        OjalaUtils::createCookieUtms();
        Happens::vioLanding('', 'tipUbicar');
        
        $model=new User;
        $this->render('tipUbicar', array('model'=>$model));
    }

    public function actionTipFotos()
    {
        OjalaUtils::createCookieUtms();
        Happens::vioLanding('', 'tipFotos');
        
        $model=new User;
        $this->render('tipFotos', array('model'=>$model));
    }

    public function actionTipRafagas()
    {
        OjalaUtils::createCookieUtms();
        Happens::vioLanding('', 'tipRafagas');
        
        $model=new User;
        $this->render('tipRafagas', array('model'=>$model));
    }

    public function actionTipTareas()
    {
        OjalaUtils::createCookieUtms();
        Happens::vioLanding('', 'tipTareas');
        
        $model=new User;
        $this->render('tipTareas', array('model'=>$model));
    }

    public function actionTipLocalizacion()
    {
        OjalaUtils::createCookieUtms();
        Happens::vioLanding('', 'tipLocalizacion');
        
        $model=new User;
        $this->render('tipLocalizacion', array('model'=>$model));
    }

    public function actionTipFotoscontraste()
    {
        OjalaUtils::createCookieUtms();
        Happens::vioLanding('', 'tipFotoscontraste');
        
        $model=new User;
        $this->render('tipFotoscontraste', array('model'=>$model));
    }

    public function actionTipProgramas()
    {
        OjalaUtils::createCookieUtms();
        Happens::vioLanding('', 'tipProgramas');
        
        $model=new User;
        $this->render('tipProgramas', array('model'=>$model));
    }

    public function actionTipBuscar()
    {
        OjalaUtils::createCookieUtms();
        Happens::vioLanding('', 'tipBuscar');
        
        $model=new User;
        $this->render('tipBuscar', array('model'=>$model));
    }

    public function actionTipcompartir()
    {
        OjalaUtils::createCookieUtms();
        Happens::vioLanding('', 'tipCompartir');
        
        $model=new User;
        $this->render('tipCompartir', array('model'=>$model));
    }

    public function actionTipPano()
    {
        OjalaUtils::createCookieUtms();
        Happens::vioLanding('', 'tipPano');
        
        $model=new User;
        $this->render('tipPano', array('model'=>$model));
    }

    public function actionTipAlbum()
    {
        OjalaUtils::createCookieUtms();
        Happens::vioLanding('', 'tipAlbum');
        
        $model=new User;
        $this->render('tipAlbum', array('model'=>$model));
    }

    //Landing para el landing de Groupon
    public function actionDiplomadoiOSyAndroid()
    {
        OjalaUtils::createCookieUtms();
        Happens::vioLanding('', 'diplomadoiosyandroid');

        Yii::app()->session['mailchimp'] = "f268504c03"; 
        
        $model=new User;
        $this->render('diplomadoiosyandroid', array('model'=>$model));
    }

    //Landing para el Diplomado de Android
    public function actionDiplomadoAndroid()
    {
        $this->layout = 'landingMenu';
        OjalaUtils::createCookieUtms();
        Happens::vioLanding('', 'diplomadoAndroid');
        
        $model=new User;
        $this->render('diplomadoAndroid', array('model'=>$model));
    }

    //Landing para el diploamdo de iOS7
    public function actionDiplomadoiOS()
    {
        $this->layout = 'landingMenu';
        OjalaUtils::createCookieUtms();
        Happens::vioLanding('', 'diplomadoios');

        Yii::app()->session['mailchimp'] = "ec46694dd4"; 

        $model=new User;
        $this->render('diplomadoios', array('model'=>$model));
    }

    //Landing para el curso de Coin - pagando de una
    public function actionCoinn()
    {
        OjalaUtils::createCookieUtms();
        Happens::vioLanding('', 'coinn');

        $model=new User;
        $this->render('coinn', array('model'=>$model));
    }


    //Landing para el curso de Coin - pidiendo el correo con más info
    public function actionCoin()
    {
        OjalaUtils::createCookieUtms();
        Happens::vioLanding('', 'coin');

        $model=new User;
        $this->render('coin', array('model'=>$model));
    }

    //Landing para el curso de iOS7
    public function actionAmigos2x1()
    {
        OjalaUtils::createCookieUtms();
        Happens::vioLanding('', 'Amigos2x1');

        $model=new User;
        $this->render('Amigos2x1', array('model'=>$model));
    }

    //Landing para el curso de iOS7
    public function actionIos7()
    {
        OjalaUtils::createCookieUtms();
        Happens::vioLanding('', 'coin');

        $model=new User;
        $this->render('coin', array('model'=>$model));
    }

    //Landing para el curso de Pulsosocial - capturando el correo
    public function actionPulso()
    {
        OjalaUtils::createCookieUtms();
        Happens::vioLanding('', 'pulso');

        //VARIABLE DE SESION PULSO
        Yii::app()->session['pulso'] = "true"; 
        $model=new User;
        $this->render('pulso', array('model'=>$model));
    }

    //Landing para el curso de Pulsosocial - para pago
    public function actionPulsoinstituteAcceso()
    {
        $this->render('pulsoinstituteacceso');
    }

    //Landing para el tech de Monterrey, evento de Alta y el Tech
    public function actionIncmty()
    {
        OjalaUtils::createCookieUtms();
        Happens::vioLanding('', 'incmty');

        //VARIABLE DE SESION MEXICO
        Yii::app()->session['mexico'] = "true";
        $model=new User;
        $this->render('incmty', array('model'=>$model));
    }    

    //Landing para el curso de iPhone
    public function actionIphone()
    {
        OjalaUtils::createCookieUtms();
        Happens::vioLanding('', 'iphone');

        $model=new User;
        $this->renderPartial('iphone', array('model'=>$model));
    }

    //Landing parael curso de iPhone Apps
    public function actionIphoneapps()
    {
        OjalaUtils::createCookieUtms();
        Happens::vioLanding('', 'iphoneapps');

        $model=new User;
        $this->renderPartial('iphoneapps', array('model'=>$model));
    }

    public function actionMensaje()
    {
        $model=new User;

        if(isset($_POST['User']))
        {

            Yii::app()->user->setFlash('mensaje', 'Gracias por registrarte, un tutor te va a contactar al correo robles.juanc@gmail.com<br><a href='.Yii::app()->urlManager->createUrl('l/iphone').'>Editar Correo</a>');                    
            $this->refresh();

            $this->renderPartial('mensaje', array('model' => $model));

            sleep(40);

            try 
            {
                $mandrill = new MandrillWrapper();
                $template_name = 'iPhone';
                $result = $mandrill->templates->info($template_name);
                $message = array(
                    'html' => $result['code'],
                    'text' => $result['text'],
                    'subject' => $result['subject'],
                    'from_email' => $result['from_email'],
                    'from_name' => $result['from_name'],
                    'to' => array(
                        array(
                            'email' => "robles.juanc@gmail.com",
                            'name' => '',
                            'type' => 'to'
                        )
                    ),
                    'headers' => array('Reply-To' => $result['from_email'])
                );
                $async = false;
                $result = $mandrill->messages->send($message, $async);
            }
            catch(Mandrill_Error $e)
            {
                //$mensaje = 'A mandrill error occurred: ' . get_class($e) . ' - ' . $e->getMessage();
                //throw $e;
            }

        }else
        {
            $this->renderPartial('mensaje', array('model' => $model));
        }
    }

    public function actionRegistro()
    {
        $model=new User;

        if(isset($_POST['User']))
        {
            $model->attributes=$_POST['User'];
            $utype=UserType::model()->findByAttributes(array('na_utype'=>'Lead'));
            $model->id_utype=$utype->id_utype;
            $model->user=$_POST['User']['email1'];
            $model->name=$_POST['User']['email1'];
            $model->create=new CDbExpression('NOW()');
            $model->about=Yii::app()->request->urlReferrer;
            $model->pass="00000";
            $model->repeatpass="00000";

            if($model->validate())
            {
                $model->pass="";
                
                if($model->save(false))
                {
                    date_default_timezone_set('UTC');

                    $model->verifcode= hash_hmac('sha256',microtime().$model->pass, 'dlfkgknbcvjkbsdkjflsdkhfdf34534jkHL$@#K$^kb');
                    $model->update();

                    $identity=new UserIdentity($model->email1, '', 1);
                    $identity->authenticate();
                    Yii::app()->user->login($identity);
                    
                    $activation_url = 'https://' . $_SERVER['HTTP_HOST'].Yii::app()->urlManager->createUrl('site/cambiarClave', array("verifcode" => $model->verifcode,"id" => $model->id_user));

                    try 
                    {
                        $mandrill = new MandrillWrapper();
                        $template_name = 'Welcome';
                        $result = $mandrill->templates->info($template_name);
                        $message = array(
                            'html' => '<span>Hola '.$model->name.'</span><br>'.$result['code'].'<br><a href='.$activation_url.'> >>> Para cambiar tu contraseña, clickea el siguiente <<< </a>',
                            'text' => 'Hola '.$model->name.', \n '.$result['text'].'\n'.$activation_url,
                            'subject' => 'Hola '.$model->name.' '.$result['subject'],
                            'from_email' => $result['from_email'],
                            'from_name' => $result['from_name'],
                            'to' => array(
                                array(
                                    'email' => $model->email1,
                                    'name' => $model->name,
                                    'type' => 'to'
                                )
                            ),
                            'headers' => array('Reply-To' => $result['from_email'])
                        );
                        $result = $mandrill->messages->send($message, false);
                    }
                    catch(Mandrill_Error $e)
                    {
                        //echo 'A mandrill error occurred: ' . get_class($e) . ' - ' . $e->getMessage();
                        //throw $e;
                    }
                    
                    $user=User::model()->findByPk($model->id_user);
                    Happens::registro($user->email1);

                    require_once 'MailChimp.php';
                    $MailChimp = new MailChimp('1b6d2d0238718961fb3bf87d131141e9-us6');
                    $result = $MailChimp->call('lists/subscribe', array(
                        'id'                => 'ec46694dd4',
                        'email'             => array('email'=>$user->email1),
                        'merge_vars'        => array('FNAME'=>$user->name, 'LNAME'=>$user->lastname),
                        'double_optin'      => false,
                        'update_existing'   => true,
                        'replace_interests' => false,
                        'send_welcome'      => false,
                    ));


                    Yii::app()->user->setFlash('mensaje', 'Gracias por registrarte, un tutor te va a contactar al correo '.$model->email1.'<br><a href='.Yii::app()->urlManager->createUrl('l/iphone').'>Editar Correo</a>');                    
                    $this->refresh();
                }
            }
            else
            {
                $model=User::model()->findByAttributes(array('email1'=>$_POST['User']['email1']));
                if($model)
                {
                    date_default_timezone_set('UTC');
                    try 
                    {
                        $mandrill = new MandrillWrapper();
                        $template_name = 'iPhone';
                        $result = $mandrill->templates->info($template_name);
                        $message = array(
                            'html' => $result['code'],
                            'text' => $result['text'],
                            'subject' => $result['subject'],
                            'from_email' => $result['from_email'],
                            'from_name' => $result['from_name'],
                            'to' => array(
                                array(
                                    'email' => $model->email1,
                                    'name' => '',
                                    'type' => 'to'
                                )
                            ),
                            'headers' => array('Reply-To' => $result['from_email'])
                        );
                        $async = false;
                        $ip_poll = "";
                        $send_at = date('Y-m-d H:i:s', strtotime("+2 minutes"));

                        $result = $mandrill->messages->send($message, $async);
                    }
                    catch(Mandrill_Error $e)
                    {
                        //echo 'A mandrill error occurred: ' . get_class($e) . ' - ' . $e->getMessage();
                        //throw $e;
                    }

                    $user=User::model()->findByPk($model->id_user);
                    Happens::registro($user->email1);

                    require_once 'MailChimp.php';
                    $MailChimp = new MailChimp('1b6d2d0238718961fb3bf87d131141e9-us6');
                    $result = $MailChimp->call('lists/subscribe', array(
                        'id'                => 'ec46694dd4',
                        'email'             => array('email'=>$user->email1),
                        'merge_vars'        => array('FNAME'=>$user->name, 'LNAME'=>$user->lastname),
                        'double_optin'      => false,
                        'update_existing'   => true,
                        'replace_interests' => false,
                        'send_welcome'      => false,
                    ));


                    Yii::app()->user->setFlash('mensaje', 'Gracias por registrarte, un tutor te va a contactar al correo '.$model->email1.'<br><a href='.Yii::app()->urlManager->createUrl('l/iphone').'>Editar Correo</a>');                    
                    $this->refresh();
                }

            }
        }

        if(strpos(Yii::app()->request->urlReferrer, 'pulso') !== false)
        {
            $this->redirect(Yii::app()->urlManager->createUrl('l/pulsoinstitute'));
        }
        else
        {
            $this->redirect(Yii::app()->urlManager->createUrl('site/suscripciones'));
        }
    }

    public function actionWhRegistro()
    {
        if($_POST['data_json'])
        {
            $form_data = json_decode($_POST['data_json']);
            
            $user=User::model()->findByAttributes(array('email1'=>$form_data->tu_email[0]));
            if(!$user)
            {
                $model=new User;   
                $model->email1=$form_data->tu_email[0];
                $utype=UserType::model()->findByAttributes(array('na_utype'=>'Lead'));
                $model->id_utype=$utype->id_utype;
                $model->user=$form_data->tu_email[0];
                $model->verifcode= hash_hmac('sha256',microtime().$model->pass, 'dlfkgknbcvjkbsdkjflsdkhfdf34534jkHL$@#K$^kb');

                if(isset($form_data->tu_nombre[0]))
                    $model->name=$form_data->tu_nombre[0];
                else
                    $model->name=$form_data->tu_email[0];

                $model->create=new CDbExpression('NOW()');
                $model->about="unbounce";
                $model->save(false);
                
                $activation_url = 'https://' . $_SERVER['HTTP_HOST'].Yii::app()->urlManager->createUrl('site/cambiarClave', array("verifcode" => $model->verifcode,"id" => $model->id_user));

                try 
                {
                    $mandrill = new MandrillWrapper();
                    $template_name = 'Welcome';
                    $result = $mandrill->templates->info($template_name);
                    $message = array(
                        'html' => '<span>Hola '.$model->name.'</span><br>'.$result['code'].'<br><a href='.$activation_url.'> >>> Para cambiar tu contraseña, clickea el siguiente <<< </a>',
                        'text' => 'Hola '.$model->name.', \n '.$result['text'].'\n'.$activation_url,
                        'subject' => 'Hola '.$model->name.' '.$result['subject'],
                        'from_email' => $result['from_email'],
                        'from_name' => $result['from_name'],
                        'to' => array(
                            array(
                                'email' => $model->email1,
                                'name' => $model->name,
                                'type' => 'to'
                            )
                        ),
                        'headers' => array('Reply-To' => $result['from_email'])
                    );
                    $result = $mandrill->messages->send($message, false);
                }
                catch(Mandrill_Error $e)
                {
                    //echo 'A mandrill error occurred: ' . get_class($e) . ' - ' . $e->getMessage();
                    //throw $e;
                }
                
                $user=User::model()->findByPk($model->id_user);
                Happens::registro($user->email1);

                require_once 'MailChimp.php';

                $MailChimp = new MailChimp('1b6d2d0238718961fb3bf87d131141e9-us6');

                $result = $MailChimp->call('lists/subscribe', array(
                    'id'                => 'ec46694dd4',
                    'email'             => array('email'=>$user->email1),
                    'merge_vars'        => array('FNAME'=>$user->name, 'LNAME'=>$user->lastname),
                    'double_optin'      => false,
                    'update_existing'   => true,
                    'replace_interests' => false,
                    'send_welcome'      => false,
                ));
            }
        }

        Yii::app()->end();
    }

    public function actionRegistroIphone()
    {
        $model=new User;

        if(isset($_POST['User']))
        {
            $model->attributes=$_POST['User'];
            $utype=UserType::model()->findByAttributes(array('na_utype'=>'Lead'));
            $model->id_utype=$utype->id_utype;
            $model->user=$_POST['User']['email1'];
            $model->name=$_POST['User']['email1'];
            $model->create=new CDbExpression('NOW()');
            $model->about=Yii::app()->request->urlReferrer;
            $model->pass="00000";
            $model->repeatpass="00000";

            if($model->validate())
            {
                $model->pass="";
                
                if($model->save(false))
                {
                    date_default_timezone_set('UTC');

                    try 
                    {
                        $mandrill = new MandrillWrapper();
                        $template_name = 'iPhone';
                        $result = $mandrill->templates->info($template_name);
                        $message = array(
                            'html' => $result['code'],
                            'text' => $result['text'],
                            'subject' => $result['subject'],
                            'from_email' => $result['from_email'],
                            'from_name' => $result['from_name'],
                            'to' => array(
                                array(
                                    'email' => $model->email1,
                                    'name' => '',
                                    'type' => 'to'
                                )
                            ),
                            'headers' => array('Reply-To' => $result['from_email'])
                        );
                        $async = false;
                        $ip_poll = "";
                        $send_at = date('Y-m-d H:i:s', strtotime("+2 minutes"));

                        $result = $mandrill->messages->send($message, $async);
                    }
                    catch(Mandrill_Error $e)
                    {
                        //echo 'A mandrill error occurred: ' . get_class($e) . ' - ' . $e->getMessage();
                        //throw $e;
                    }
                    $user=User::model()->findByPk($model->id_user);
                    Happens::registro($user->email1);

                    require_once 'MailChimp.php';
                    $MailChimp = new MailChimp('1b6d2d0238718961fb3bf87d131141e9-us6');
                    $result = $MailChimp->call('lists/subscribe', array(
                        'id'                => 'ec46694dd4',
                        'email'             => array('email'=>$user->email1),
                        'merge_vars'        => array('FNAME'=>$user->name, 'LNAME'=>$user->lastname),
                        'double_optin'      => false,
                        'update_existing'   => true,
                        'replace_interests' => false,
                        'send_welcome'      => false,
                    ));


                    Yii::app()->user->setFlash('mensaje', 'Gracias por registrarte, un tutor te va a contactar al correo '.$model->email1.'<br><a href='.Yii::app()->urlManager->createUrl('l/iphone').'>Editar Correo</a>');                    
                    $this->refresh();
                }
            }
            else
            {
                $model=User::model()->findByAttributes(array('email1'=>$_POST['User']['email1']));
                if($model)
                {
                    date_default_timezone_set('UTC');
                    try 
                    {
                        $mandrill = new MandrillWrapper();
                        $template_name = 'iPhone';
                        $result = $mandrill->templates->info($template_name);
                        $message = array(
                            'html' => $result['code'],
                            'text' => $result['text'],
                            'subject' => $result['subject'],
                            'from_email' => $result['from_email'],
                            'from_name' => $result['from_name'],
                            'to' => array(
                                array(
                                    'email' => $model->email1,
                                    'name' => '',
                                    'type' => 'to'
                                )
                            ),
                            'headers' => array('Reply-To' => $result['from_email'])
                        );
                        $async = false;
                        $ip_poll = "";
                        $send_at = date('Y-m-d H:i:s', strtotime("+2 minutes"));

                        $result = $mandrill->messages->send($message, $async);
                    }
                    catch(Mandrill_Error $e)
                    {
                        //echo 'A mandrill error occurred: ' . get_class($e) . ' - ' . $e->getMessage();
                        //throw $e;
                    }

                    $user=User::model()->findByPk($model->id_user);
                    Happens::registro($user->email1);

                    require_once 'MailChimp.php';
                    $MailChimp = new MailChimp('1b6d2d0238718961fb3bf87d131141e9-us6');

                    $result = $MailChimp->call('lists/subscribe', array(
                        'id'                => 'ec46694dd4',
                        'email'             => array('email'=>$user->email1),
                        'merge_vars'        => array('FNAME'=>$user->name, 'LNAME'=>$user->lastname),
                        'double_optin'      => false,
                        'update_existing'   => true,
                        'replace_interests' => false,
                        'send_welcome'      => false,
                    ));


                    Yii::app()->user->setFlash('mensaje', 'Gracias por registrarte, un tutor te va a contactar al correo '.$model->email1.'<br><a href='.Yii::app()->urlManager->createUrl('l/iphone').'>Editar Correo</a>');                    
                    $this->refresh();
                }

            }
        }

        $this->renderPartial('iphone', array('model' => $model));
    }
}