<?php

class AdminController extends Controller {

    const TXT_CAMPAIGN_CREATED = 'Campaña creada correctamente';
    const TXT_CAMPAIGN_MODIFIED = 'Cprocesaampaña modificada correctamente';

    public $layout = '//layouts/admin';

    public function actions() {
        return array(
            'page' => array(
                'class' => 'CViewAction',
            ),
        );
    }

    public function filters() {
        return array(
            'accessControl',
        );
    }

    public function accessRules() {
        return array(
            array('allow',
                'actions' => array('index', 'reportemx', 'agregarCupon', 'upload', 'curso', 'CambiarTipoUsuario',
                    'editarCurso', 'Loginuser', 'LoginEstudiante', 'LoginBack',
                    'EliminarCs', 'AgregarSuscripcion', 'ejecutarScript', 'eliminarCurso', 'suscriptores', 'editarSuscripcion',
                    'escanearMaterial', 'agregarAmigo', 'agregarCurso', 'suscripciones', 'campana', 'cursos',
                    'guardarId', 'inscribirDiplomado', 'crearDiplomado', 'diplomados', 'AgregarCursoDiplomado',
                    'desinscribirDiplomado', 'procesar', 'BuscadorEstudiantes', 'Estudiante', 'ActivarSuscripcion', 'CancelarSuscripcion',
                    'Registro', 'reporteCampanas', 'editarPorWistia', 'EditarClase', 'ReporteSuscriptores',
                    'pdfSuscriptores', 'excelSuscriptores', 'mailSuscriptores', 'detalleSuscriptores', 'nuevoEvento',
                    'CambiarEstado', 'SubirPdf', 'diplomado', 'EliminarCursoDiplomado', 'eliminarDiplomado', 'landings',
                    'Usuario', 'usuarios', 'agregarUsuario', 'eliminarUsuario', 'EditarSlugClase', 'generarCertificado',
                    'planes', 'agregarPlan', 'eliminarPlan', 'eliminarEvento', 'Wistia', 'DetalleMes', 'agregarDuplicado', 'editarStype',
                    'EditarLesson', 'BorrarLesson', 'GuardarFechaLanding', 'TopClases', 'TopLogin', 'topFrecuencia', 'borradores', 'resumen', 'ChurnRate', 'NuevoPago',
                    'roi', 'roiDetalle', 'ActualizarFechaSuscripcion', 'CursosTrends', 'EditarWistiaClase',
                    'campanas', 'crearCampana', 'editarCampana', 'eliminarCampana', 'obtenerCursos', 'materialDescarga',
                    'newsletters', 'newsletter', 'CuentaDuplicada', 'topCursos', 'perfil', 'seguimiento', 'editarTelefono',
                    'cambiarEstadoContacto', 'GuardarContacto', 'SubirClase', 'BajarClase', 'reporteRegreso', 'DetalleRegreso',
                    'guardarSeguidor', 'AgregarCalendar', 'AgregarCalendar2', 'CambiarPlan', 'guardarEvento', 'PublicoClase', 'propuestas', 'nuevaPropuesta',
                    'editarPropuesta', 'traducir', 'estadoRevisadoIndividual', 'ConvertirACurso', 'convertirTodas', 'convertirDirecto',
                    'publicarPropuesta','ocultarPropuesta'),
                'users' => array('@'),
                'expression' => "isset(Yii::app()->user->utype) && (Yii::app()->user->utype==='Administrador')",
            ),
            array('allow',
                'actions' => array('index', 'BuscadorEstudiantes', 'cursos', 'diplomados', 'Loginuser', 'Registro', 'LoginEstudiante', 'LoginBack',
                    'upload', 'SubirPdf', 'generarCertificado',
                    'Estudiante', 'Usuario', 'agregarAmigo', 'agregarDuplicado',
                    'EditarSlugClase', 'EditarClase', 'materialDescarga',
                    'AgregarSuscripcion', 'ActivarSuscripcion', 'CancelarSuscripcion',
                    'agregarCupon', 'agregarCurso', 'EliminarCs', 'inscribirDiplomado', 'desinscribirDiplomado',
                    'curso', 'editarCurso', 'eliminarCurso', 'CambiarEstado', 'editarPorWistia', 'escanearMaterial', 'guardarId',
                    'diplomado', 'crearDiplomado', 'eliminarDiplomado', 'AgregarCursoDiplomado', 'EliminarCursoDiplomado',
                    'nuevoEvento', 'EditarLesson', 'CuentaDuplicada', 'EditarWistiaClase', 'cambiarEstadoContacto',
                    'GuardarContacto', 'CambiarPlan',
                ),
                'users' => array('@'),
                'expression' => "isset(Yii::app()->user->utype) && (Yii::app()->user->utype==='AAC')",
            ),
            array('deny',
                'users' => array('*'),
            ),
        );
    }

    /*     * ************* */
	
	/*
	 * Función que permite ocultar las propuestas ya publicadas
	 */
	
	public function actionOcultarPropuesta($id){
		
		$course = Course::model()->findByPk($id);

        if ($course->active == 0) {

            $this->redirect(Yii::app()->urlManager->createUrl('site/propuesta', array('slug' => $course->slug)));
        } else {

            if (Course::model()->updateByPk($id, array(
                        'active' => 0
                    ))) {

                $this->redirect(Yii::app()->urlManager->createUrl('site/propuesta', array('slug' => $course->slug)));
            } else {

                Yii::app()->user->setFlash('error', "Ocurrió un error para ocultar la propuesta. Por favor, inténtelo de nuevo.");
                $this->redirect(Yii::app()->urlManager->createUrl('admin/editarPropuesta', array('id' => $id)));
            }
        }
		
	}
	
    
    /*
     * Función que publica las propuestas.
     */

    public function actionPublicarPropuesta($id) {

        $course = Course::model()->findByPk($id);

        if ($course->active == 1) {

            $this->redirect(Yii::app()->urlManager->createUrl('site/propuesta', array('slug' => $course->slug)));
        } else {

            if (Course::model()->updateByPk($id, array(
                        'active' => 1
                    ))) {

                $this->redirect(Yii::app()->urlManager->createUrl('site/propuesta', array('slug' => $course->slug)));
            } else {

                Yii::app()->user->setFlash('error', "Ocurrió un error durante la publicación de la propuesta. Por favor, inténtelo de nuevo.");
                $this->redirect(Yii::app()->urlManager->createUrl('admin/editarPropuesta', array('id' => $id)));
            }
        }
    }
    
    /*
     * Función que realiza la conversión de la propuesta en un curso.
     * Primero elimina todas las lessons y topics (si existen).
     * Luego revisa línea por línea de la estructura formateada,
     * verifica que no esté vacía y que sea un capítulo o no.
     * Realiza el insert en lesson y topic.
     */

    public function conversion($course) {

        $structure = $course->structure_format;

        //Sólo las propuestas revisadas pueden convertirse en Curso.
        if ($course->id_state == 5) {

            //Primero borrando todos los capítulos y las lecciones
            //que tiene asociada la propuesta.
            $criteria = new CDbCriteria;
            $criteria->addCondition('t.id_course = :courseid');
            $criteria->params = array(':courseid' => $course->id_course);
            $lessons = Lesson::model()->findAll($criteria);

            if (count($lessons) > 0) {

                foreach ($lessons as $lesson) {
                    Topic::model()->deleteAll('id_lesson = :lessonid', array(
                        ':lessonid' => $lesson->id_lesson,
                    ));
                }

                Lesson::model()->deleteAll('id_course = :courseid', array(
                    ':courseid' => $course->id_course,
                ));
            }

            //Separando el texto por cada línea
            //$structure = nl2br($structure);
            $structure = preg_replace("/\r|\n/", "<br />", $structure);
            $lineas = explode('<br />', $structure);

            $string = "";

            $currentLesson = null;

            $i = 1;

            $bandera = 0;

            foreach ($lineas as $linea) {

                //Quitándole los espacios en blanco al principio y al final de la cadena
                $linea = trim($linea, " ");

                if ($linea != "" && $linea != null) {

                    $capitulo = 0;

                    $primero = explode(' ', $linea);

                    if (isset($primero[0])) {

                        if (preg_match("/\d+./", $primero[0])) {

                            $capitulo = 1;
                        }
                    }

                    //Si tiene la expresión regular EN LA PRIMERA PARTE DE LA LÍNEA, es un capítulo.
                    if ($capitulo == 1) {

                        $linea = "";
                        $j = 0;
                        foreach ($primero as $prim) {

                            if ($j == 0) {

                                $prim = "";

                                $j = 1;
                            }

                            $linea = $linea . " " . $prim;
                        }
                        $lesson = new Lesson();
                        $lesson->id_course = $course->id_course;
                        $lesson->na_lesson = ucfirst($linea);
                        if ($lesson->save()) {
                            $currentLesson = $lesson->id_lesson;
                            $bandera = 1;
                        }
                    } else {

                        //Si aún no hay un capítulo insertado.
                        if ($currentLesson == null) {
                            $lesson = new Lesson();
                            $lesson->id_course = $course->id_course;
                            $lesson->na_lesson = 'Introducción';
                            if ($lesson->save()) {
                                $currentLesson = $lesson->id_lesson;
                                $bandera = 1;
                            }
                        }

                        //Insertando las clases
                        $topic = new Topic;
                        $topic->id_lesson = $currentLesson;
                        $topic->id_ttype = 1;
                        $topic->na_topic = $i . ". - " . ucfirst($linea);
                        $topic->wistia_uid = 'temp';
                        $topic->duration = null;
                        $topic->position = 0;
                        $topic->public = 0;
                        $topic->active = 1;
                        $topic->save();

                        $i++;
                    }
                }
            }

            return $bandera;
        } else {

            return 0;
        }
    }
    
    /*
     * Función que revisa todas las propuestas con estado revisado
     * y que no tengan ni lessons ni topics asociados
     * y llama a la función conversión.
     */

    public function actionConvertirTodas() {

        //Buscar todos los cursos con estado 5 -> 'Propuesta revisada'.
        $criteria = new CDbCriteria;
        $criteria->addCondition('t.id_state = 5');
        $criteria->with = array(
            'lessons'
        );

        $courses = Course::model()->findAll($criteria);

        foreach ($courses as $course) {

            //Verificar que no haya sido convertido en curso antes.
            if (count($course->lessons) <= 0) {

                $resultado = $this->conversion($course);

            }
        }

        $this->redirect(Yii::app()->urlManager->createUrl('admin/propuestas',array('filtro'=>0)));
    }
    
    /*
     * Función que convierte una propuesta al momento de su
     * inserción y redirige a la portada de la propuesta.
     */

    public function actionConvertirDirecto($id) {

        $course = Course::model()->findByPk($id);

        $resultado = $this->conversion($course);

        if ($resultado == 0) {

            Yii::app()->user->setFlash('error', "Ocurrió un error durante la conversión. Por favor, revise que la estructura esté correctamente escrita.");
            $this->redirect(Yii::app()->urlManager->createUrl('admin/editarPropuesta', array('id' => $course->id_course)));
        } else {

            $this->redirect(Yii::app()->urlManager->createUrl('site/propuesta', array('slug' => $course->slug)));
        }
    }
    
    /*
     * Función llamada con una petición AJAX que realiza el cambio
     * de propuesta a curso llamando la función conversión.
     */

    public function actionConvertirACurso() {

        $id = Yii::app()->request->getParam('courseid');

        $structure = Yii::app()->request->getParam('structure');

        $course = Course::model()->findByPk($id);

        $course->structure_format = $structure;

        $course->update();

        $resultado = $this->conversion($course);

        echo $resultado;

    }
    
    /*
     * Función que cambia el estado de una propuesta específica.
     */

    public function actionEstadoRevisadoIndividual($id) {

        $course = Course::model()->findByPk($id);

        if ($course->id_state == 4) {

            Course::model()->updateByPk(array('id_course' => $id), array('id_state' => 5, 'active' => 0));
        }

        $this->redirect(Yii::app()->urlManager->createUrl('admin/editarPropuesta', array('id' => $id)));
    }
    
    /*
     * Función que liosta todas las propuestas.
     */

    public function actionPropuestas() {
       
        $dataProvider = Course::model()->getPropuestas($_GET['filtro']);

        if (isset($_GET['dato'])) {

            $dataProvider = Course::model()->getPropuestas($_GET['filtro'],$_GET['dato']);
        }

        $this->render('propuestas', array('dataProvider' => $dataProvider));
    }
    
    /*
     * Función que mediante una llamada AJAX. toma la estructura inicial de la 
     * propuesta y usa el traductor de BING para traducirlo al inglés.
     */

    public function actionTraducir() {

        $texto = Yii::app()->request->getParam('textoInicial');

        $nombre = Yii::app()->request->getParam('nombre');

        $descripcion = Yii::app()->request->getParam('descripcion');

        $trans = new OjalaTranslator();

        $nuevo = explode(' ', trim($texto, " "));

        $texto = preg_replace("/\r|\n/", " &&& ", $texto);

        //$texto = str_replace("\t", " $$$ ", $texto);

        $texto = $nombre . " #### " . $descripcion . " #### " . $texto;

        $newString = trim($trans->translate('en', 'es', $texto), ' &&& ');

        $newString = str_replace("&&&", "\r", $newString);

        //$newString = str_replace("$$$", "\t\t\t", $newString);

        /*
         * Hasta aquí es lo de traducir, luego comienza lo de 
         * convertir en curso
         */

        //Quitando los tiempos de las clases LYNDA
        $newString = preg_replace("/\d+m\s\d+s|\d+h\s\d+m|\d+m|\d+s/", "", $newString);

        //Quitando los tiempos de las clases TEAMTREEHOUSE
        $newString = preg_replace("/\d+:\d+/", "", $newString);

        //Eliminando cosas de TEAMTREEHOUSE
        $newString = preg_replace("/\d+\spreguntas|\d+\spregunta/", "", $newString);
        $newString = preg_replace("/\d+\sobjetivos|\d+\sobjetivo/", "", $newString);

        //Eliminando los saltos de línea que sobran.
        $newString = preg_replace("/\r\s\r/", "\n", $newString);

        /* $nombreTraducir = $trans->translate('en', 'es', $nombre);

          $descripcionTraducir = $trans->translate('en', 'es', $descripcion); */

        /*
         * Aquí termina lo de convertir curso.
         */

        echo $newString;
    }
    
    /*
     * Función que permite la inserción de una nueva propuesta.
     */

    public function actionNuevaPropuesta() {

        $course = new Course;

        if (isset($_POST["Course"])) {

            $course->attributes = $_POST['Course'];
            $course->id_cathegory = $_POST['id_cathegory'];
            $course->id_level = $_POST['id_level'];
            $course->id_state = 5; //Propuesta

            if (isset(Yii::app()->session['images_cover'])) {
                $course->deleteImages();
                $course->cover = Yii::app()->session['images_cover'];
                $course->generateThumbnails();
                unset(Yii::app()->session['images_cover']);
            }

            $course->category = "";
            $course->cost = 0;
            $course->create = new CDbExpression('NOW()');
            $course->duration = 0;
            $course->active = 0;
            if ($course->save(false)) {
                unset(Yii::app()->session['images_cover']);
                $this->redirect(Yii::app()->urlManager->createUrl('admin/convertirDirecto', array('id' => $course->id_course)));
            }
        }

        $listCat = Cathegory::model()->findAll();
        $listNiv = Level::model()->findAll();

        $this->render('nuevaPropuesta', array('proposal' => $course, 'listCat' => $listCat, 'listNiv' => $listNiv));
    }
    
    /*
     * Función que realiza la actualización de una propuesta.
     */

    public function actionEditarPropuesta($id) {

        $course = Course::model()->findByPk($id);

        if ($course->id_state == 4 || $course->id_state == 5) {

            if (isset($_POST['Course'])) {

                $transaction = Yii::app()->db->beginTransaction();

                try {

                    $course->attributes = $_POST['Course'];
                    $course->id_cathegory = $_POST['id_cathegory'];
                    $course->id_level = $_POST['id_level'];
                    if (isset(Yii::app()->session['images_cover'])) {
                        $course->deleteImages();
                        $course->cover = Yii::app()->session['images_cover'];
                        $course->generateThumbnails();
                        unset(Yii::app()->session['images_cover']);
                    }

                    $course->update();

                    if (isset($_POST['tags'])) {
                        CourseTag::model()->deleteAll('id_course=' . $course->id_course);
                        $tags = sprintf("%s", $_POST['tags']);  //Avoids XSS
                        $tagsArray = preg_split('/ |,/', $tags);
                        foreach ($tagsArray as $key => $value) {
                            $tag = Tag::model()->findByAttributes(array('tag' => $value));
                            if (!$tag) {
                                $tag = new Tag;
                                $tag->tag = $value;
                                $tag->save();
                            }
                            $ptag = CourseTag::model()->findByAttributes(array('id_course' => $course->id_course, 'id_tag' => $tag->id_tag));
                            if (!$ptag) {
                                $ptag = new CourseTag;
                                $ptag->id_course = $course->id_course;
                                $ptag->id_tag = $tag->id_tag;
                                $ptag->save();
                            }
                        }
                    }

                    $transaction->commit();

                    $this->redirect(Yii::app()->urlManager->createUrl('site/propuesta', array('slug' => $course->slug)));
                } catch (Exception $e) {
                    $transaction->rollback();
                    Yii::log($e->getMessage(), CLogger::LEVEL_ERROR);
                    //$this->redirect(Yii::app()->urlManager->createUrl('admin/editarCurso', array('id' => $course->id_course, 'msg' => 'Ocurrio un error al intentar guardar')));
                }
            }

            $tags = CourseTag::model()->with('tag')->findAllByAttributes(array(
                'id_course' => $id,
            ));

            $listCat = Cathegory::model()->findAll();
            $listNiv = Level::model()->findAll();
            $this->render('editarPropuesta', array('proposal' => $course, 'tags' => $tags, 'listCat' => $listCat, 'listNiv' => $listNiv));
        }
    }

    /*     * ************* */

    public function actionNewsletter() {
        if (isset($_POST['html']) && isset($_POST['imagenes'])) {
            $html = $_POST['html'];

            $url = getcwd() . '/protected/views/admin/newsletters/generados/';

            $imagenes = explode("&", $_POST['imagenes']);

            $archivosCompletos = array();
            foreach ($imagenes as $value) {
                //copy(getcwd()."/images/covers/".$value, $url.$value);
                $archivosCompletos[] = getcwd() . "/images/covers/" . $value;
            }

            if (file_exists($url . "newfile.zip"))
                unlink($url . "newfile.zip");

            $myfile = fopen($url . "newfile.html", "w") or die("Unable to open file!");
            fwrite($myfile, $html);
            fclose($myfile);

            $archivosCompletos[] = $url . "newfile.html";
            $result = $this->create_zip($archivosCompletos, $url . "newfile.zip");

            if (file_exists($url . "newfile.zip")) {
                $path = "/var/Downloads/package/newfile.zip";
                if (copy($url . "newfile.zip", $path)) {
                    if (file_exists($path)) {
                        return Yii::app()->request->xSendFile($path, array('saveName' => "newsletter.zip"));
                    }
                }
            }
            //$this->render('newsletter', array('file'=>$url."newfile.zip"));
        }
    }

    public function actionNewsletters() {
        $connection = Yii::app()->db;
        $command = $connection->createCommand();
        $command->setSelect("c.id_course, c.name, c.cover, c.description, c.slug, ca.slug cslug, ca.na_cathegory cat");
        $command->setFrom(Course::model()->tableName() . ' c');
        $command->join(State::model()->tableName() . ' s', 's.id_state=c.id_state');
        $command->join(Cathegory::model()->tableName() . ' ca', 'ca.id_cathegory=c.id_cathegory');
        $command->setOrder('RAND()');
        $command->setWhere("c.active=1 and s.id_state=1");
        $listaCursos = $command->queryAll();

        foreach ($listaCursos as $key => $curso) {
            $course = Course::model()->findByPk($curso['id_course']);
            $course->generateThumbnails();
        }

        $url = getcwd() . '/protected/views/admin/newsletters/';
        //$url=getcwd().'\protected\views\admin\newssletters\\';

        $archivos = scandir($url);
        $listaTemplates = array();
        $i = 0;
        if (count($archivos) > 2) {
            foreach ($archivos as $archivo) {
                if (substr($archivo, -5) == ".html") {
                    if ($archivo != "." && $archivo != "..") {
                        $handle = fopen($url . $archivo, "r");
                        if (filesize($url . $archivo) > 0) {
                            $contents = fread($handle, filesize($url . $archivo));
                            fclose($handle);

                            $listaTemplates[$i]['file'] = $archivo;
                            $listaTemplates[$i]['content'] = $contents;
                            $i++;
                        }
                    }
                }
            }
        }

        $this->render('newsletters', array('listaCursos' => $listaCursos, 'listaTemplates' => $listaTemplates));
    }

    public function actionLandings() {
        $url = getcwd() . '/protected/views/l/';

        $archivos = scandir($url);
        $list = array();
        $i = 0;
        if (count($archivos) > 2) {
            foreach ($archivos as $archivo) {
                if ($archivo != "." && $archivo != "..") {
                    if (substr($archivo, -4) != ".txt") {
                        $list[] = array('landing' => $archivo, 'url' => $url . $archivo, 'date' => '');
                        $i++;
                    } else {
                        $handle = fopen($url . $archivo, "r");
                        if (filesize($url . $archivo) > 0) {
                            $contents = fread($handle, filesize($url . $archivo));
                            fclose($handle);
                            $list[$i - 1]['date'] = $contents;
                        }
                    }
                }
            }
        }

        $this->render('landings', array('list' => $list));
    }

    public function actionGuardarFechaLanding() {
        if (isset($_GET['archivo']) && isset($_GET['fecha'])) {
            $url = getcwd() . '/protected/views/l/';
            $fp = fopen($url . strtolower($_GET['archivo']), "wb");
            fwrite($fp, OjalaUtils::convertDateToDB($_GET['fecha']));
            fclose($fp);

            $this->redirect(Yii::app()->urlManager->createUrl('admin/landings'));
        }
    }

    public function actionLoginuser() {
        if (isset($_GET['email'])) {
            $identity = new UserIdentity($_GET['email'], '', 1);
            if ($identity->authenticate())
                Yii::app()->user->login($identity);
            else
                echo $identity->errorMessage;

            $this->redirect(Yii::app()->homeUrl);
        }
    }

    public function actionLoginEstudiante() {
        if (isset($_GET['email'])) {
            Yii::app()->session['admin'] = Yii::app()->user->id;

            unset(Yii::app()->session['deudor']);
            unset(Yii::app()->session['inactivo']);
            unset(Yii::app()->session['cancelado']);

            $user = User::model()->findByAttributes(array('email1' => $_GET['email']));

            Yii::app()->user->id = $user->id_user;
            Yii::app()->user->name = $user->name;
            Yii::app()->user->email = $user->email1;

            $this->redirect(Yii::app()->homeUrl);
        }
    }

    public function actionLoginBack() {
        if (isset(Yii::app()->session['admin'])) {
            $id_user = Yii::app()->user->id;

            $user = User::model()->findByPk(Yii::app()->session['admin']);
            Yii::app()->user->id = $user->id_user;
            Yii::app()->user->name = $user->name;
            Yii::app()->user->email = $user->email1;

            unset(Yii::app()->session['admin']);

            $this->redirect(Yii::app()->urlManager->createUrl('admin/estudiante', array('id' => $id_user)));
        }
    }

/////////////MENU ADMINISTRACION

    public function actionIndex()
    {
        $activeCourses = Course::model()->countByAttributes(array('active'=>1, 'id_state'=>State::PUBLISHED));
        $hiddenCourses = Course::model()->hidden()->count();
        $draftCourses = Course::model()->draft()->count();
        $onboarding= CmpCampaign::model()->count();

        $activeProposal = Course::model()->proposal()->countByAttributes(array('active' => 1));
        $reviewedProposals = Course::model()->reviewedProposal()->countByAttributes(array('active' => 1));
        $unactiveProposal = Course::model()->proposal()->countByAttributes(array('active' => 0));
        $unactiveReviewedProposals = Course::model()->reviewedProposal()->countByAttributes(array('active' => 0));

        $diplomados = Group::model()->countByAttributes(array('active' => 1));
        $suscritos_diplomados = GroupSuscription::model()->countByAttributes(array('active' => 1));

        $this->render('index', array(
            'activeCourses'=>$activeCourses,
            'hiddenCourses'=>$hiddenCourses,
            'draftCourses'=>$draftCourses,
            'diplomados' => $diplomados,
            'suscritos_diplomados' => $suscritos_diplomados,
            'proposalPublished'=>$activeProposal+$reviewedProposals,
            'proposalUnpublished'=>$unactiveProposal+$unactiveReviewedProposals,
            'onboarding' => $onboarding
        ));
    }

    public function actionRegistro()
    {
        $model=new User;

        if (isset($_POST['User'])) {
            $model->attributes = $_POST['User'];
            $utype = UserType::model()->findByAttributes(array('na_utype' => 'Lead'));
            $model->id_utype = $utype->id_utype;
            $model->user = $_POST['User']['email1'];
            $model->create = new CDbExpression('NOW()');
            $model->about = Yii::app()->request->urlReferrer;
            $model->pass = "00000";
            $model->repeatpass = "00000";

            if ($model->validate()) {
                $model->pass = "";

                if ($model->save(false)) {
                    date_default_timezone_set('UTC');

                    $model->verifcode = hash_hmac('sha256', microtime() . $model->pass, 'dlfkgknbcvjkbsdkjflsdkhfdf34534jkHL$@#K$^kb');
                    $model->update();

                    $activation_url = 'https://' . $_SERVER['HTTP_HOST'] . Yii::app()->urlManager->createUrl('site/cambiarClave', array("verifcode" => $model->verifcode, "id" => $model->id_user));

                    try {
                        $mandrill = new MandrillWrapper();
                        $template_name = 'Welcome';
                        $result = $mandrill->templates->info($template_name);
                        $message = array(
                            'html' => '<span>Hola ' . $model->name . '</span><br>' . $result['code'] . '<br><a href=' . $activation_url . '> >>> Para cambiar tu contraseña, haz clic el siguiente <<< </a>',
                            'text' => 'Hola ' . $model->name . ', \n ' . $result['text'] . '\n' . $activation_url,
                            'subject' => 'Hola ' . $model->name . ' ' . $result['subject'],
                            'from_email' => $result['from_email'],
                            'from_name' => $result['from_name'],
                            'to' => array(
                                array(
                                    'email' => $model->email1,
                                    'name' => $model->name,
                                    'type' => 'to'
                                )
                            ),
                            'headers' => array('Reply-To' => $result['from_email'])
                        );
                        $result = $mandrill->messages->send($message, false);
                    } catch (Mandrill_Error $e) {
                        //echo 'A mandrill error occurred: ' . get_class($e) . ' - ' . $e->getMessage();
                        //throw $e;
                    }

                    $user = User::model()->findByPk($model->id_user);

                    Happens::registro($user->email1);

                    $this->redirect(Yii::app()->urlManager->createUrl('admin/estudiante', array('id' => $user->id_user)));
                }
            }
        }
        $this->redirect(Yii::app()->urlManager->createUrl('admin/buscadorEstudiantes'));
    }

    public function actionSuscripciones() {
        $connection = Yii::app()->db;
        $command = $connection->createCommand();
        $command->setSelect("s.id_suscription, u.name, u.id_user, u.email1, s.active, s.date, na_stype");
        $command->setFrom(Suscription::model()->tableName() . ' s');
        $command->join(User::model()->tableName() . ' u', 'u.id_user=s.id_user');
        $command->join(SuscriptionType::model()->tableName() . ' st', 'st.id_stype=s.id_stype');

        if (isset($_GET['dato'])) {
            $command->setWhere("u.email1 LIKE '%" . $_GET['dato'] . "%'");
        } else {
            $command->setLimit("50");
        }

        $command->setOrder("s.id_suscription DESC, st.id_stype");
        $list = $command->queryAll();

        if (isset($_GET['dato'])) {
            $this->render('suscripciones', array('suscripciones' => $list, 'dato' => $_GET['dato']));
        } else {
            $this->render('suscripciones', array('suscripciones' => $list));
        }
    }

    public function actionCursosTrends() {
        $connection = Yii::app()->db;

        $command = $connection->createCommand();
        $command->setSelect("c.id_course as id, c.name as name, count(cs.id_csuscription) as inscritos, c.publish, c.slug, ca.slug as cat");
        $command->setFrom(Course::model()->tableName() . ' c');
        $command->join(Cathegory::model()->tableName() . ' ca', 'ca.id_cathegory=c.id_cathegory');
        $command->join(CourseSuscription::model()->tableName() . ' cs', 'cs.id_course=c.id_course');
        $command->setWhere("MONTH(cs.updated_at)=5 AND YEAR(cs.updated_at)=2014");
        $command->setGroup("c.id_course");
        $command->setOrder("count(cs.id_csuscription) DESC");
        $command->setLimit(20);
        $list1 = $command->queryAll();

        $command = $connection->createCommand();
        $command->setSelect("c.id_course as id, c.name as name, count(id_link) as interesados, c.wistia_uid as wistia, c.publish, c.slug, ca.slug as cat");
        $command->setFrom(Course::model()->tableName() . ' c');
        $command->join(Cathegory::model()->tableName() . ' ca', 'ca.id_cathegory=c.id_cathegory');
        $command->leftjoin(Link::model()->tableName() . ' l', 'l.id_course=c.id_course');
        $command->setGroup("c.id_course");
        $command->setOrder("count(id_link) DESC");
        $command->setLimit(20);
        $list2 = $command->queryAll();


        $command = $connection->createCommand();
        $command->setSelect("c.id_course as id, c.name as name, count(cs.id_csuscription) as inscritos, c.wistia_uid as wistia, c.publish as publish, c.slug, ca.slug as cat");
        $command->setFrom(Course::model()->tableName() . ' c');
        $command->join(Cathegory::model()->tableName() . ' ca', 'ca.id_cathegory=c.id_cathegory');
        $command->join(CourseSuscription::model()->tableName() . ' cs', 'cs.id_course=c.id_course');
        $command->setGroup("c.id_course");
        $command->setOrder("count(cs.id_csuscription) DESC");
        $command->setLimit(20);
        $list3 = $command->queryAll();

        $this->render('cursosTrends', array('list1' => $list1, 'list2' => $list2, 'list3' => $list3));
    }

    public function actionCursos() {
        $titulo = "Todos los Cursos";
        $query = "";
        $connection = Yii::app()->db;

        if (!isset($_GET['dato'])) {
            if (isset($_GET['state'])) {
                $query = "s.id_state=" . $_GET['state'];
                if ($_GET['state'] == "1") {
                    if (!isset($_GET['publish'])) {
                        $query.="";
                        $titulo = "Todos los Cursos Publicados";
                    } elseif ($_GET['publish'] == "1") {
                        $query.=" AND c.publish<= NOW()";
                        $titulo = "Cursos Publicados";
                    } else {
                        $query.=" AND c.publish > NOW()";
                        $titulo = "Cursos Programados";
                    }
                } elseif ($_GET['state'] == "2")
                    $titulo = "Cursos en Borrador";
                elseif ($_GET['state'] == "3")
                    $titulo = "Cursos Ocultos";

                $command = $connection->createCommand();
                $command->setSelect("c.id_course as id, GROUP_CONCAT(t.tag SEPARATOR ', ') as tags, c.duration, c.name as name, c.wistia_uid as wistia, c.`create` as `create`, c.publish as publish, s.na_state as status, c.slug, ca.slug as cat");
                $command->setFrom(Course::model()->tableName() . ' c');
                $command->join(Cathegory::model()->tableName() . ' ca', 'ca.id_cathegory=c.id_cathegory');
                $command->join(State::model()->tableName() . ' s', 's.id_state=c.id_state');
                $command->leftJoin(CourseTag::model()->tableName() . ' ct', 'ct.id_course=c.id_course');
                $command->leftJoin(Tag::model()->tableName() . ' t', 't.id_tag=ct.id_tag');
                $command->setGroup("c.id_course");
                $command->setWhere($query . " AND c.active!=0 AND c.id_state != 4 AND c.id_state != 5");
                $command->setOrder("s.na_state DESC, c.id_course DESC");
                $list1 = $command->queryAll();
            }
            else {
                $command = $connection->createCommand();
                $command->setSelect("c.id_course as id, GROUP_CONCAT(t.tag SEPARATOR ', ') as tags, c.duration, c.name as name, c.wistia_uid as wistia, c.`create` as `create`, c.publish as publish, s.na_state as status, c.slug, ca.slug as cat");
                $command->setFrom(Course::model()->tableName() . ' c');
                $command->join(Cathegory::model()->tableName() . ' ca', 'ca.id_cathegory=c.id_cathegory');
                $command->join(State::model()->tableName() . ' s', 's.id_state=c.id_state');
                $command->leftJoin(CourseTag::model()->tableName() . ' ct', 'ct.id_course=c.id_course');
                $command->leftJoin(Tag::model()->tableName() . ' t', 't.id_tag=ct.id_tag');
                $command->setGroup("c.id_course");
                $command->setWhere("c.active!=0 AND c.id_state != 4 AND c.id_state != 5");
                $command->setOrder("s.na_state DESC, c.id_course DESC");
                $list1 = $command->queryAll();
            }
        } else {
            $command = $connection->createCommand();
            $command->setSelect("c.id_course as id, GROUP_CONCAT(t.tag SEPARATOR ', ') as tags, c.duration, c.name as name, c.wistia_uid as wistia, c.`create` as `create`, c.publish as publish, s.na_state as status, c.slug, ca.slug as cat");
            $command->setFrom(Course::model()->tableName() . ' c');
            $command->join(Cathegory::model()->tableName() . ' ca', 'ca.id_cathegory=c.id_cathegory');
            $command->join(State::model()->tableName() . ' s', 's.id_state=c.id_state');
            $command->leftJoin(CourseTag::model()->tableName() . ' ct', 'ct.id_course=c.id_course');
            $command->leftJoin(Tag::model()->tableName() . ' t', 't.id_tag=ct.id_tag');
            $command->setGroup("c.id_course");
            $command->setWhere("c.active!=0 AND c.name LIKE '%" . $_GET['dato'] . "%'  AND c.id_state != 4 AND c.id_state != 5");
            $command->setOrder("s.na_state DESC, c.id_course DESC");
            $list1 = $command->queryAll();
            $titulo = "Resultado de la Busqueda";
        }
        $titulo.=" (" . count($list1) . ")";

        if (isset($_GET['dato'])) {
            $this->render('cursos', array('list1' => $list1, 'titulo' => $titulo, 'dato' => $_GET['dato']));
        } else {
            $this->render('cursos', array('list1' => $list1, 'titulo' => $titulo));
        }
    }

    public function actionPlanes() {
        $plans = SuscriptionType::model()->findAll();
        $this->render('planes', array('plans' => $plans));
    }

    public function actionAgregarPlan() {
        $plan = new SuscriptionType;

        if (isset($_POST['SuscriptionType'])) {
            if (isset($_POST['id_stype'])) {
                SuscriptionType::model()->updateByPk($_POST['id_stype'], $_POST['SuscriptionType']);
                $this->redirect(Yii::app()->urlManager->createUrl('admin/planes'));
            } else {
                $plan->attributes = $_POST['SuscriptionType'];
                if ($plan->save())
                    $this->redirect(Yii::app()->urlManager->createUrl('admin/planes'));
            }
        }
        if (isset($_GET['id'])) {
            $plan = SuscriptionType::model()->findByPk($_GET['id']);
        }
        $this->render('agregarPlan', array('plan' => $plan));
    }

    public function actionEliminarPlan() {
        if (isset($_GET['id'])) {
            $plan = SuscriptionType::model()->findByPk($_GET['id']);
            $plan->delete();
            $this->redirect(Yii::app()->urlManager->createUrl('admin/planes'));
        }
    }

////////////REPORTES

    public function actionResumen() {
        $mes_actual = date('Y-m-d', strtotime("-15 days"));
        $mes_anterior = date('Y-m-d', strtotime("-45 days"));

        $connection = Yii::app()->db;
        $command = $connection->createCommand();
        $command->setSelect("id_course");
        $command->setFrom(Course::model()->tableName() . ' c');
        $command->setWhere('active=1');
        $cursos = count($command->queryAll());

        $command = $connection->createCommand();
        $command->setSelect("id_course");
        $command->setFrom(Course::model()->tableName() . ' c');
        $command->setWhere("active=1 AND MONTH(c.create)=MONTH('" . $mes_actual . "') AND YEAR(c.create)=YEAR('" . $mes_actual . "')");
        $cursos_nuevos = count($command->queryAll());

        $command = $connection->createCommand();
        $command->setSelect("id_course");
        $command->setFrom(Course::model()->tableName() . ' c');
        $command->setWhere("active=1 AND MONTH(c.create)=MONTH('" . $mes_anterior . "') AND YEAR(c.create)=YEAR('" . $mes_anterior . "')");
        $cursos_nuevos_anterior = count($command->queryAll());

        $command = $connection->createCommand();
        $command->setSelect("id_course, name, create");
        $command->setFrom(Course::model()->tableName() . ' c');
        $command->setWhere("active=1 AND MONTH(c.create)=MONTH('" . $mes_actual . "') AND YEAR(c.create)=YEAR('" . $mes_actual . "')");
        $lista_cursos = $command->queryAll();

        $command = $connection->createCommand();
        $command->setSelect("id_course, name, create");
        $command->setFrom(Course::model()->tableName() . ' c');
        $command->setWhere("active=1 AND MONTH(c.create)=MONTH('" . $mes_anterior . "') AND YEAR(c.create)=YEAR('" . $mes_anterior . "')");
        $lista_cursos_anterior = $command->queryAll();

        $command = $connection->createCommand();
        $command->setSelect("s.id_suscription");
        $command->setFrom(Suscription::model()->tableName() . " s");
        $command->join(SuscriptionType::model()->tableName() . " st", "s.id_stype=st.id_stype");
        $command->setGroup("s.id_user");
        $command->setWhere("s.active!=0 AND st.recurrence=1 AND st.id_stype!=17 AND st.id_stype!=40");
        $suscriptores_deudores = count($command->queryAll());

        $command = $connection->createCommand();
        $command->setSelect("s.id_suscription");
        $command->setFrom(Suscription::model()->tableName() . " s");
        $command->join(SuscriptionType::model()->tableName() . " st", "s.id_stype=st.id_stype");
        $command->setGroup("s.id_user");
        $command->setWhere("s.active!=0 AND st.recurrence=1 AND st.id_stype!=17 AND st.id_stype!=40 AND MONTH(s.date)=MONTH('" . $mes_actual . "') AND YEAR(s.date)=YEAR('" . $mes_actual . "')");
        $suscriptores_deudores_nuevos = count($command->queryAll());

        $command = $connection->createCommand();
        $command->setSelect("s.id_suscription");
        $command->setFrom(Suscription::model()->tableName() . " s");
        $command->join(SuscriptionType::model()->tableName() . " st", "s.id_stype=st.id_stype");
        $command->setGroup("s.id_user");
        $command->setWhere("s.active!=0 AND st.recurrence=1 AND st.id_stype!=17 AND st.id_stype!=40 AND MONTH(s.date)=MONTH('" . $mes_anterior . "') AND YEAR(s.date)=YEAR('" . $mes_anterior . "')");
        $suscriptores_deudores_nuevos_anterior = count($command->queryAll());

        $command = $connection->createCommand();
        $command->setSelect("s.id_suscription, u.email1, st.na_stype, s.date");
        $command->setFrom(Suscription::model()->tableName() . " s");
        $command->join(SuscriptionType::model()->tableName() . " st", "s.id_stype=st.id_stype");
        $command->join(User::model()->tableName() . " u", "s.id_user=u.id_user");
        $command->setGroup("s.id_user");
        $command->setWhere("s.active=1 AND st.recurrence=1 AND st.id_stype!=17 AND st.id_stype!=40");
        $suscriptores = $command->queryAll();

        $command = $connection->createCommand();
        $innerQuery = "SELECT ss.id_status FROM " . SuscriptionStatus::model()->tableName() . " ss " .
                " WHERE ss.id_suscription=s.id_suscription AND MONTH(ss.date)=MONTH('${mes_actual}') " .
                " ORDER BY ss.`date` DESC LIMIT 1";
        $command->setSelect("COUNT(s.id_suscription)");
        $command->setFrom(Suscription::model()->tableName() . " s");
        $command->join(SuscriptionType::model()->tableName() . " st", "s.id_stype=st.id_stype");
        $command->setWhere("s.active=1 AND st.recurrence=1 AND st.id_stype!=17 AND st.id_stype!=40 AND 1=(${innerQuery})");
        $suscriptores_pasados = $command->queryScalar();

        $command = $connection->createCommand();
        $innerQuery = "SELECT COUNT(ss.id_sstatus) FROM " . SuscriptionStatus::model()->tableName() . " ss " .
                " WHERE ss.id_suscription=s.id_suscription";
        $command->setSelect("s.id_suscription, u.id_user as id, u.email1, st.na_stype, s.date");
        $command->setFrom(Suscription::model()->tableName() . " s");
        $command->join(SuscriptionType::model()->tableName() . " st", "s.id_stype=st.id_stype");
        $command->join(User::model()->tableName() . " u", "s.id_user=u.id_user");
        $command->setGroup("s.id_user");
        $command->setWhere("st.recurrence=1 AND st.id_stype!=17 AND st.id_stype!=40  AND MONTH(s.date)=MONTH('" . $mes_actual . "') AND YEAR(s.date)=YEAR('" . $mes_actual . "')");
        $suscriptores_nuevos = $command->queryAll();

        $command = $connection->createCommand();
        $command->setSelect("s.id_suscription, u.id_user as id, u.email1, st.na_stype, s.date");
        $command->setFrom(Suscription::model()->tableName() . " s");
        $command->join(SuscriptionType::model()->tableName() . " st", "s.id_stype=st.id_stype");
        $command->join(User::model()->tableName() . " u", "s.id_user=u.id_user");
        $command->setGroup("s.id_user");
        $command->setWhere("st.recurrence=1 AND st.id_stype!=17 AND st.id_stype!=40  AND MONTH(s.date)=MONTH('" . $mes_anterior . "') AND YEAR(s.date)=YEAR('" . $mes_anterior . "')");
        $suscriptores_nuevos_anterior = $command->queryAll();

        $command = $connection->createCommand();
        $command->setSelect("email1, p.id_pay, p.amount, p.date, pt.na_ptype, u.id_user id, u.name, u.lastname");
        $command->setFrom(Pay::model()->tableName() . " p");
        $command->join(PayType::model()->tableName() . " pt", "pt.id_ptype=p.id_ptype");
        $command->join(Suscription::model()->tableName() . " s", "s.id_suscription=p.id_suscription");
        $command->join(User::model()->tableName() . " u", "s.id_user=u.id_user");
        $command->setWhere("p.`status`='Completed' AND MONTH(p.date)=MONTH('" . $mes_actual . "') AND YEAR(p.date)=YEAR('" . $mes_actual . "')");
        $command->setOrder("pt.id_ptype, p.date DESC");
        $lista_revenue = $command->queryAll();

        $command = $connection->createCommand();
        $command->setSelect("SUM(p.amount) total");
        $command->setFrom(Pay::model()->tableName() . " p");
        $command->setWhere("p.`status`='Completed' AND p.currency='USD' AND MONTH(p.date)=MONTH('" . $mes_actual . "') AND YEAR(p.date)=YEAR('" . $mes_actual . "')");
        $revenue_dolar_actual = $command->queryRow();

        $command = $connection->createCommand();
        $command->setSelect("SUM(p.amount) total");
        $command->setFrom(Pay::model()->tableName() . " p");
        $command->setWhere("p.`status`='Completed' AND p.currency='MXN' AND MONTH(p.date)=MONTH('" . $mes_actual . "') AND YEAR(p.date)=YEAR('" . $mes_actual . "')");
        $revenue_peso_actual = $command->queryRow();

        $command = $connection->createCommand();
        $command->setSelect("SUM(p.amount) total");
        $command->setFrom(Pay::model()->tableName() . " p");
        $command->setWhere("p.`status`='Completed' AND p.currency='USD' AND MONTH(p.date)=MONTH('" . $mes_anterior . "') AND YEAR(p.date)=YEAR('" . $mes_anterior . "')");
        $revenue_dolar_anterior = $command->queryRow();

        $command = $connection->createCommand();
        $command->setSelect("SUM(p.amount) total");
        $command->setFrom(Pay::model()->tableName() . " p");
        $command->setWhere("p.`status`='Completed' AND p.currency='MXN' AND MONTH(p.date)=MONTH('" . $mes_anterior . "') AND YEAR(p.date)=YEAR('" . $mes_anterior . "')");
        $revenue_peso_anterior = $command->queryRow();

        $mes_anterior = DateTime::createFromFormat('Y-m-d', $mes_anterior)->format('d/m/Y');
        $mes_actual = DateTime::createFromFormat('Y-m-d', $mes_actual)->format('d/m/Y');

        $this->render('reportes/resumen', array(
            'mes_actual' => $mes_actual,
            'mes_anterior' => $mes_anterior,
            'lista_revenue' => $lista_revenue,
            'revenue' => round($revenue_dolar_actual['total'] + ($revenue_peso_actual['total'] / OjalaPayments::MXN_RATE), 2),
            'revenue_anterior' => round($revenue_dolar_anterior['total'] + ($revenue_peso_anterior['total'] / OjalaPayments::MXN_RATE), 2),
            'suscriptores' => count($suscriptores),
            'lista_suscriptores' => $suscriptores,
            'suscriptores_nuevos' => count($suscriptores_nuevos),
            'suscriptores_nuevos_anterior' => count($suscriptores_nuevos_anterior),
            'lista_suscriptores_nuevos_anterior' => $suscriptores_nuevos_anterior,
            'lista_suscriptores_nuevos' => $suscriptores_nuevos,
            'suscriptores_90' => "",
            'suscriptores_90_nuevos' => "",
            'suscriptores_deudores' => $suscriptores_deudores,
            'suscriptores_deudores_nuevos' => $suscriptores_deudores_nuevos,
            'suscriptores_deudores_nuevos_anterior' => $suscriptores_deudores_nuevos_anterior,
            'suscriptores_pasados' => $suscriptores_pasados,
            'cursos' => $cursos,
            'cursos_nuevos' => $cursos_nuevos,
            'cursos_nuevos_anterior' => $cursos_nuevos_anterior,
            'lista_cursos' => $lista_cursos,
            'lista_cursos_anterior' => $lista_cursos_anterior,
        ));
    }

    public function actionRoiDetalle() {
        if (isset($_GET['mes']) && isset($_GET['ano'])) {
            if (isset($_GET['cont'])) {
                $mes_suscripcion = $_GET['mes'];
                $ano_suscripcion = $_GET['ano'];

                for ($i = 0; $i < 10; $i++) {
                    if ($i == $_GET['cont'])
                        break;
                    $mes_suscripcion = date('m', strtotime($ano_suscripcion . "-" . $mes_suscripcion . "-01 +1 month"));
                    $ano_suscripcion = date('Y', strtotime($ano_suscripcion . "-" . $mes_suscripcion . "-01 +1 month"));
                }

                $connection = Yii::app()->db;
                $command = $connection->createCommand();
                $command->setSelect("u.id_user, u.id_service, s.id_suscription, s.date suscripcion, u.email1 email, u.email2, u.create registro, na_stype, na_ptype");
                $command->setFrom(Suscription::model()->tableName() . ' s');
                $command->join(User::model()->tableName() . ' u', 'u.id_user=s.id_user');
                $command->join(SuscriptionType::model()->tableName() . ' st', 'st.id_stype=s.id_stype');
                $command->leftjoin(PayType::model()->tableName() . ' pt', 'pt.id_ptype=s.id_ptype');
                $command->setWhere("st.recurrence=1 AND st.id_stype!=17 AND st.id_stype!=40 AND MONTH(u.create)=" . $_GET['mes'] . " AND YEAR(u.create)=" . $_GET['ano'] . " AND MONTH(s.date)=" . $mes_suscripcion . " AND YEAR(s.date)=" . $ano_suscripcion);
                $suscriptores = $command->queryAll();

                $suscripcion = new DateTime($ano_suscripcion . '-' . $mes_suscripcion . '-01');
                $registro = new DateTime($_GET['ano'] . '-' . $_GET['mes'] . '-01');

                $fecha_registro = $registro->format('F, Y');
                $fecha_suscripcion = $suscripcion->format('F, Y');

                $this->render('reportes/roiDetalle', array('suscriptores' => $suscriptores, 'fecha_suscripcion' => $fecha_suscripcion, 'fecha_registro' => $fecha_registro));
            }
            else {
                $connection = Yii::app()->db;
                $command = $connection->createCommand();
                $command->setSelect("u.id_user, u.id_service, s.id_suscription, s.date suscripcion, u.email1 email, u.email2, u.create registro, na_stype, na_ptype");
                $command->setFrom(Suscription::model()->tableName() . ' s');
                $command->join(User::model()->tableName() . ' u', 'u.id_user=s.id_user');
                $command->join(SuscriptionType::model()->tableName() . ' st', 'st.id_stype=s.id_stype');
                $command->leftjoin(PayType::model()->tableName() . ' pt', 'pt.id_ptype=s.id_ptype');
                $command->setWhere("st.recurrence=1 AND st.id_stype!=17 AND st.id_stype!=40 AND MONTH(u.create)=" . $_GET['mes'] . " AND YEAR(u.create)=" . $_GET['ano']);
                $command->setOrder("u.create");
                $suscriptores = $command->queryAll();

                $registro = new DateTime($_GET['ano'] . '-' . $_GET['mes'] . '-01');
                $fecha_registro = $registro->format('F, Y');

                $this->render('reportes/roiDetalle', array('suscriptores' => $suscriptores, 'fecha_registro' => $fecha_registro));
            }
        }
    }

    public function actionActualizarFechaSuscripcion() {
        if (isset($_GET['id_suscription']) && isset($_GET['fecha'])) {
            $suscription = Suscription::model()->findByPk($_GET['id_suscription']);
            $suscription->date = OjalaUtils::convertDateToDB($_GET['fecha']);
            $suscription->update();

            $this->redirect(Yii::app()->urlManager->createUrl('admin/roi'));
        }
    }

    public function actionRoi() {
        $tipo = 'month';

        $deudores = '';
        if (isset($_GET['deudores']))
            $deudores = $_GET['deudores'];

        $connection = Yii::app()->db2;
        $command = $connection->createCommand();
        $command->setSelect(@"rp.id_time, rp.week AS semana, rp.month AS mes, rp.year AS ano");
        $command->setFrom(RepHappen::model()->tableName() . ' rh');
        $command->join(RepTime::model()->tableName() . ' rp', "rp.id_time=rh.id_time");
        $command->setWhere("rp.id_time > '2013-03-05'");
        $command->setGroup("rp." . $tipo . ', rp.year');
        $command->setOrder("rp.year ASC, rp.month ASC, rp.week ASC");
        $list = $command->queryAll();

        $listaChurn = array();
        $j = 0;
        foreach ($list as $value) {
            $gasto = 0;
            switch ($j) {
                case 0: $gasto = "445,16";
                    break;
                case 1: $gasto = "1.078,96";
                    break;
                case 2: $gasto = "980,40";
                    break;
                case 3: $gasto = "3.210,07";
                    break;
                case 4: $gasto = "100,27";
                    break;
                case 5: $gasto = "1.430,34";
                    break;
                case 6: $gasto = "0,00";
                    break;
                case 7: $gasto = "831,01";
                    break;
                case 8: $gasto = "1.638,93";
                    break;
                case 9: $gasto = "752,63";
                    break;
                case 10: $gasto = "2.266,33";
                    break;
                case 11: $gasto = "2.443,85";
                    break;
                case 12: $gasto = "5.124,05";
                    break;
                case 13: $gasto = "9.910,63";
                    break;
                case 14: $gasto = "3.355,44";
                    break;
            }

            $j++;

            $anorep = $value['ano']; //date("Y", strtotime($value['ano']."-".$value['month']."-".$value['1']))
            $mesrep = $value['mes'];

            $ano = $value['ano'];
            $mes = $value['mes'];

            $meses = array();
            $suma = 0;
            for ($i = 0; $i < 10; $i++) {
                /* $connection=Yii::app()->db3;
                  $command=$connection->createCommand();
                  $command->setSelect("id");
                  $command->setFrom(Mongo::model()->tableName() . " m");
                  $command->setWhere("MONTH(m.registro)=".$mesrep." AND YEAR(m.registro)=".$anorep." AND MONTH(m.ppago)=".$mes." AND YEAR(m.ppago)=".$ano);
                  $resultado = $command->queryAll(); */

                $connection = Yii::app()->db;
                $command = $connection->createCommand();
                $command->setSelect("s.id_suscription, u.name, u.id_user, u.email1, s.active, s.date, na_stype");
                $command->setFrom(Suscription::model()->tableName() . ' s');
                $command->join(User::model()->tableName() . ' u', 'u.id_user=s.id_user');
                $command->join(SuscriptionType::model()->tableName() . ' st', 'st.id_stype=s.id_stype');
                $command->setWhere("st.recurrence=1 AND st.id_stype!=17 AND st.id_stype!=40 AND MONTH(s.date)=" . $mes . " AND YEAR(s.date)=" . $ano . " AND MONTH(u.create)=" . $mesrep . " AND YEAR(u.create)=" . $anorep);
                $resultado = $command->queryAll();

                $suma += count($resultado);
                $meses+=array(
                    $i => count($resultado),
                    'suma' . $i => $suma,
                );

                $ano = date('Y', strtotime($ano . "-" . $mes . "-01 +1 " . $tipo));
                $mes = date('m', strtotime($ano . "-" . $mes . "-01 +1 " . $tipo));
                if (strtotime($ano . "-" . $mes . "-01") > strtotime("now"))
                    break;
            }

            /* $connection=Yii::app()->db;
              $command=$connection->createCommand();
              $command->setSelect("id_user");
              $command->setFrom(User::model()->tableName() . ' u');
              $command->setWhere("MONTH(u.`create`)=".$mesrep." AND YEAR(u.`create`)=".$anorep);
              $leads = count($command->queryAll()); */

            /* $connection=Yii::app()->db3;
              $command=$connection->createCommand();
              $command->setSelect("id");
              $command->setFrom(Mongo::model()->tableName() . " m");
              $command->setWhere("MONTH(m.registro)=".$mesrep." AND YEAR(m.registro)=".$anorep);
              $leads = $command->queryAll(); */

            $connection = Yii::app()->db;
            $command = $connection->createCommand();
            $command->setSelect("u.id_user, u.name, u.email1");
            $command->setFrom(User::model()->tableName() . ' u');
            $command->setWhere("MONTH(u.create)=" . $mesrep . " AND YEAR(u.create)=" . $anorep);
            $leads = $command->queryAll();

            $connection = Yii::app()->db;
            $command = $connection->createCommand();
            $command->setSelect("s.id_suscription, u.name, u.id_user, u.email1, s.active, s.date, na_stype");
            $command->setFrom(Suscription::model()->tableName() . ' s');
            $command->join(User::model()->tableName() . ' u', 'u.id_user=s.id_user');
            $command->join(SuscriptionType::model()->tableName() . ' st', 'st.id_stype=s.id_stype');
            $command->setWhere("st.recurrence=1 AND st.id_stype!=17 AND st.id_stype!=40 AND MONTH(u.create)=" . $mesrep . " AND YEAR(u.create)=" . $anorep);
            $suscriptores = $command->queryAll();

            /* $command=$connection->createCommand();
              $command->setSelect("count(id) cuantos");
              $command->setFrom(Mongo::model()->tableName() . " m");
              $command->setWhere("MONTH(m.ppago)=".$mesrep." AND YEAR(m.ppago)=".$anorep);
              $suscriptores = $command->queryRow(); */

            /* $connection=Yii::app()->db2;
              $command=$connection->createCommand();
              $command->setSelect("h.`id_happen`");
              $command->setFrom(RepHappen::model()->tableName() . ' h');
              $command->join(RepEvent::model()->tableName() . ' e', "h.id_event=e.id_event");
              $command->join(RepTime::model()->tableName() . ' t', "h.id_time=t.id_time");
              $command->setWhere(@"e.`na_event`='suscriptor' AND t.`month`=".$mesrep." AND t.`year`=".$anorep);
              $suscriptores = $command->queryAll(); */

            $totalLeads = count($leads) == 0 ? 1 : count($leads);

            $lista[] = array(
                'mes' => $value['mes'],
                'ano' => $value['ano'],
                'gasto' => $gasto,
                'leads' => count($leads),
                'cleads' => (count($leads) != 0) ? round(str_replace('.', '', $gasto) / count($leads), 2) : 0,
                'suscriptores' => count($suscriptores),
                'conversion' => round(((count($suscriptores) / $totalLeads) * 100), 2) . "%",
                'csuscriptor' => (count($suscriptores) != 0) ? round(str_replace('.', '', $gasto) / count($suscriptores), 2) : 0,
                '0' => (isset($meses['0'])) ? $meses['0'] : "",
                '1' => (isset($meses['1'])) ? $meses['1'] : "",
                '2' => (isset($meses['2'])) ? $meses['2'] : "",
                '3' => (isset($meses['3'])) ? $meses['3'] : "",
                '4' => (isset($meses['4'])) ? $meses['4'] : "",
                '5' => (isset($meses['5'])) ? $meses['5'] : "",
                '6' => (isset($meses['6'])) ? $meses['6'] : "",
                '7' => (isset($meses['7'])) ? $meses['7'] : "",
                '8' => (isset($meses['8'])) ? $meses['8'] : "",
                '9' => (isset($meses['9'])) ? $meses['9'] : "",
                '10' => (isset($meses['0'])) ? round((($meses['0'] / $totalLeads) * 100), 2) . "%" : "",
                '11' => (isset($meses['1'])) ? round((($meses['1'] / $totalLeads) * 100), 2) . "%" : "",
                '12' => (isset($meses['2'])) ? round((($meses['2'] / $totalLeads) * 100), 2) . "%" : "",
                '13' => (isset($meses['3'])) ? round((($meses['3'] / $totalLeads) * 100), 2) . "%" : "",
                '14' => (isset($meses['4'])) ? round((($meses['4'] / $totalLeads) * 100), 2) . "%" : "",
                '15' => (isset($meses['5'])) ? round((($meses['5'] / $totalLeads) * 100), 2) . "%" : "",
                '16' => (isset($meses['6'])) ? round((($meses['6'] / $totalLeads) * 100), 2) . "%" : "",
                '17' => (isset($meses['7'])) ? round((($meses['7'] / $totalLeads) * 100), 2) . "%" : "",
                '18' => (isset($meses['8'])) ? round((($meses['8'] / $totalLeads) * 100), 2) . "%" : "",
                '19' => (isset($meses['9'])) ? round((($meses['9'] / $totalLeads) * 100), 2) . "%" : "",
                '20' => (isset($meses['0'])) ? ($meses['suma0'] != 0) ? "$" . round($gasto / $meses['suma0'], 2) : "$0" : "",
                '21' => (isset($meses['1'])) ? ($meses['suma1'] != 0) ? "$" . round($gasto / $meses['suma1'], 2) : "$0" : "",
                '22' => (isset($meses['2'])) ? ($meses['suma2'] != 0) ? "$" . round($gasto / $meses['suma2'], 2) : "$0" : "",
                '23' => (isset($meses['3'])) ? ($meses['suma3'] != 0) ? "$" . round($gasto / $meses['suma3'], 2) : "$0" : "",
                '24' => (isset($meses['4'])) ? ($meses['suma4'] != 0) ? "$" . round($gasto / $meses['suma4'], 2) : "$0" : "",
                '25' => (isset($meses['5'])) ? ($meses['suma5'] != 0) ? "$" . round($gasto / $meses['suma5'], 2) : "$0" : "",
                '26' => (isset($meses['6'])) ? ($meses['suma6'] != 0) ? "$" . round($gasto / $meses['suma6'], 2) : "$0" : "",
                '27' => (isset($meses['7'])) ? ($meses['suma7'] != 0) ? "$" . round($gasto / $meses['suma7'], 2) : "$0" : "",
                '28' => (isset($meses['8'])) ? ($meses['suma8'] != 0) ? "$" . round($gasto / $meses['suma8'], 2) : "$0" : "",
                '29' => (isset($meses['9'])) ? ($meses['suma9'] != 0) ? "$" . round($gasto / $meses['suma9'], 2) : "$0" : "",
                    ) + $meses;
        }

        $this->render('reportes/roi', array('lista' => $lista));
    }

    public function actionDetalleRegreso() {
        if (isset($_GET['semana']) && isset($_GET['ano'])) {
            $connection = Yii::app()->db;
            $command = $connection->createCommand();
            $command->setSelect("u.email1, u.name, u.id_user, s.date suscripcion, u.phone");
            $command->setFrom(User::model()->tableName() . ' u');
            $command->join(Suscription::model()->tableName() . ' s', 'u.id_user=s.id_user');
            $command->join(SuscriptionType::model()->tableName() . ' st', 'st.id_stype=s.id_stype');
            $command->setWhere("st.recurrence=1 and st.id_stype!=17 AND st.id_stype!=40 AND WEEK(s.date)='" . $_GET['semana'] . "' AND YEAR(s.date)='" . $_GET['ano'] . "'");
            $command->setOrder("s.date");
            $command->setGroup("u.id_user");
            $list = $command->queryAll();

            $this->render('reportes/detalleRegreso', array('lista' => $list, 'semana' => $_GET['semana'], 'ano' => $_GET['ano']));
        }
    }

    public function actionReporteRegreso() {
        $list = array();

        $connection = Yii::app()->db;
        $command = $connection->createCommand();
        $command->setSelect("COUNT(u.email1) emails, WEEK(s.date) semana, MONTH(s.date) mes, YEAR(s.date) ano");
        $command->setFrom(User::model()->tableName() . ' u');
        $command->join(Suscription::model()->tableName() . ' s', 'u.id_user=s.id_user');
        $command->join(SuscriptionType::model()->tableName() . ' st', 'st.id_stype=s.id_stype');
        $command->setWhere("st.recurrence=1 AND st.id_stype!=17 AND st.id_stype!=40 AND s.date >= DATE_SUB(NOW(), INTERVAL 120 DAY)");
        $command->setGroup("WEEK(s.date), YEAR(s.date)");
        $suscriptores = $command->queryAll();

        foreach ($suscriptores as $item) {
            $command = $connection->createCommand();
            $command->setSelect("u.email1");
            $command->setFrom(User::model()->tableName() . ' u');
            $command->join(Suscription::model()->tableName() . ' s', 'u.id_user=s.id_user');
            $command->join(SuscriptionType::model()->tableName() . ' st', 'st.id_stype=s.id_stype');
            $command->setGroup("u.id_user");
            $command->setWhere("st.recurrence=1 and  st.id_stype!=17 AND st.id_stype!=40 AND WEEK(s.date)='" . $item['semana'] . "' AND YEAR(s.date)='" . $item['ano'] . "'");
            $suscriptoresMes = $command->queryAll();

            $text = "";
            $i = 0;
            foreach ($suscriptoresMes as $value) {
                if ($i != 0)
                    $text.=", ";
                $text.="'" . $value['email1'] . "'";
                $i++;
            }

            $meses = array("Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre");

            $primeraColumna = array(
                'semana' => $item['semana'],
                'ano' => $item['ano'],
                'mes' => $meses[$item['mes']-1],
                'suscriptores' => count($suscriptoresMes) 
            );

            $semana=$item['semana'];
            for ($i=0; $i < 15; $i++)
            {
                $connection2=Yii::app()->db2;
                $command=$connection2->createCommand();
                $command->setSelect("COUNT(h.created_at) porcentaje");
                $command->setFrom(RepHappen::model()->tableName() . ' h');
                $command->join(RepUser::model()->tableName() . ' u', "h.id_user=u.id_user");
                $command->setWhere(@"(h.id_event=17) AND WEEK(h.created_at)='" . $semana . "' AND YEAR(h.created_at)='" . $item['ano'] . "' AND u.email1 IN (" . $text . ")");
                $command->setGroup("h.id_user");
                $eventos = $command->queryAll();
                $columna = array('semana' . ($i + 1) => count($eventos));
                $primeraColumna = array_merge($primeraColumna, $columna);
                $semana++;
            }
            $list[] = $primeraColumna;
        }

        $this->render('reportes/reporteRegreso', array('lista' => $list));
    }

    public function actionReporteCampanas() {
        $connection = Yii::app()->db;

        $command = $connection->createCommand();
        $command->setSelect("s.id_suscription, u.name, u.id_user, u.email1, s.active, s.date, na_stype, utm_campaign");
        $command->setFrom(Suscription::model()->tableName() . ' s');
        $command->join(User::model()->tableName() . ' u', 'u.id_user=s.id_user');
        $command->leftjoin(Campaign::model()->tableName() . ' c', 'u.id_campaign=c.id_campaign');
        $command->join(SuscriptionType::model()->tableName() . ' st', 'st.id_stype=s.id_stype');
        $command->setWhere("st.recurrence=1");
        $command->setLimit("20");
        $command->setOrder("s.id_suscription DESC, st.id_stype");
        $suscriptores = $command->queryAll();

        /* $command=$connection->createCommand();
          $command->setSelect("utm_campaign as campana, count(id_user) as cantidad, c.id_campaign as id");
          $command->setFrom(Campaign::model()->tableName() . ' c');
          $command->leftjoin(User::model()->tableName() . ' u', 'u.id_campaign=c.id_campaign');
          $command->setGroup("c.utm_campaign");
          $command->setOrder("c.id_campaign DESC");
          $command->setLimit("5");
          $campanas = $command->queryAll(); */

        $command = $connection->createCommand();
        $command->setSelect("utm_campaign as campana, c.id_campaign as id");
        $command->setFrom(Campaign::model()->tableName() . ' c');
        $command->setOrder('c.id_campaign DESC');
        $command->setlimit(8);
        $campanas = $command->queryAll();

        $this->render('reportes/reporteCampanas', array('suscriptores' => $suscriptores, 'campanas' => $campanas));
    }

    public function actionTopClases() {
        $connection = Yii::app()->db;
        $command = $connection->createCommand();
        $command->setSelect("sum(p.progress) as minutos, u.id_user, u.email1, u.`create`, CONCAT(u.name, ' ', u.lastname) as name");
        $command->setFrom(Progress::model()->tableName() . ' p');
        $command->join(CourseSuscription::model()->tableName() . ' cs', 'cs.id_csuscription=p.id_csuscription');
        $command->join(User::model()->tableName() . ' u', 'cs.id_user=u.id_user');
        $command->setGroup("cs.id_user");
        $command->setOrder("sum(p.progress) desc");
        $command->setLimit("10");
        $list = $command->queryAll();

        $this->render('reportes/topClases', array('list' => $list));
    }

    public function actionTopCursos() {
        $connection = Yii::app()->db;

        $command = $connection->createCommand();
        $command->setSelect("(SELECT csi.id_csuscription from course_suscription csi WHERE cse.id_user=csi.id_user ORDER BY csi.updated_at desc LIMIT 1) id");
        $command->setFrom(CourseSuscription::model()->tableName() . ' cse');
        $command->setGroup("cse.id_user");
        $csd = $command->queryAll();

        $text = "";
        $i = 0;
        foreach ($csd as $value) {
            if ($i != 0)
                $text.=", ";
            $text.="'" . $value['id'] . "'";
            $i++;
        }

        $command = $connection->createCommand();
        $command->setSelect("c.name curso, count(c.id_course) cantidad");
        $command->setFrom(Course::model()->tableName() . ' c');
        $command->join(CourseSuscription::model()->tableName() . ' cs', "cs.id_course=c.id_course AND cs.updated_at is not null  AND cs.id_csuscription in (" . $text . ")");
        $command->setGroup("c.id_course");
        $command->setOrder("count(c.id_course) desc");
        $command->setLimit("5");
        $ultimosEmpezados = $command->queryAll();

        $command = $connection->createCommand();
        $command->setSelect("c.name curso, count(c.id_course) cantidad");
        $command->setFrom(Course::model()->tableName() . ' c');
        $command->join(CourseSuscription::model()->tableName() . ' cs', "cs.id_course=c.id_course AND cs.updated_at is not null  AND cs.id_csuscription in (" . $text . ")");
        $command->setWhere("cs.progress > 90"); // Completados
        $command->setGroup("c.id_course");
        $command->setOrder("count(c.id_course) desc");
        $command->setLimit("5");
        $ultimosCompletados = $command->queryAll();

        $command = $connection->createCommand();
        $command->setSelect("c.name curso, count(c.id_course) cantidad");
        $command->setFrom(Course::model()->tableName() . ' c');
        $command->join(CourseSuscription::model()->tableName() . ' cs', "cs.id_course=c.id_course AND cs.updated_at is not null  AND cs.id_csuscription in (" . $text . ")");
        $command->join(User::model()->tableName() . ' u', 'cs.id_user=u.id_user');
        $command->join(Suscription::model()->tableName() . ' s', 's.id_user=u.id_user');
        $command->setWhere("s.active=0"); // Completados
        $command->setGroup("c.id_course");
        $command->setOrder("count(c.id_course) desc");
        $command->setLimit("5");
        $ultimosEmpezadosCancelados = $command->queryAll();

        $command = $connection->createCommand();
        $command->setSelect("c.name curso, count(c.id_course) cantidad");
        $command->setFrom(Course::model()->tableName() . ' c');
        $command->join(CourseSuscription::model()->tableName() . ' cs', "cs.id_course=c.id_course AND cs.updated_at is not null  AND cs.id_csuscription in (" . $text . ")");
        $command->join(User::model()->tableName() . ' u', 'cs.id_user=u.id_user');
        $command->join(Suscription::model()->tableName() . ' s', 's.id_user=u.id_user');
        $command->setWhere("cs.progress > 90 AND s.active=0"); // Completados
        $command->setGroup("c.id_course");
        $command->setOrder("count(c.id_course) desc");
        $command->setLimit("5");
        $ultimosCompletadosCancelados = $command->queryAll();

        $this->render('reportes/topCursos', array(
            'ultimosEmpezados' => $ultimosEmpezados,
            'ultimosCompletados' => $ultimosCompletados,
            'ultimosEmpezadosCancelados' => $ultimosEmpezadosCancelados,
            'ultimosCompletadosCancelados' => $ultimosCompletadosCancelados
        ));
    }

    public function actionTopLogin() {
        $connection = Yii::app()->db;
        $command = $connection->createCommand();
        $command->setSelect("count(l.id_link) as login, u.id_user, u.email1, u.`create`, CONCAT(u.name, ' ', u.lastname) as name");
        $command->setFrom(Link::model()->tableName() . ' l');
        $command->join(User::model()->tableName() . ' u', 'l.id_user=u.id_user');
        $command->setGroup("l.id_user");
        $command->setOrder("count(l.id_link) desc");
        $command->setLimit("10");
        $list = $command->queryAll();

        $this->render('reportes/topLogin', array('list' => $list));
    }

    public function actionTopFrecuencia() {
        $connection = Yii::app()->db2;
        $command = $connection->createCommand();
        $command->setSelect("rt.id_time, rt.week, rt.month, rt.year");
        $command->setFrom(RepHappen::model()->tableName() . ' rh');
        $command->join(RepTime::model()->tableName() . ' rt', "rt.id_time=rh.id_time");
        $command->setWhere("rt.id_time > '2013-03-05'");
        $command->setGroup("rt." . $tipo . ', rt.year');
        $command->setOrder("rt.year ASC, rt.month ASC, rt.week ASC");
        $list = $command->queryAll();

        $this->render('reportes/topFrecuencia', array('list' => $list));
    }

    public function actionEditarStype() {
        if (isset($_GET['idev']) && isset($_GET['stype'])) {
            $rephappen = RepHappen::model()->findByPk($_GET['idev']);
            $stype = SuscriptionType::model()->findByPk($_GET['stype']);
            $repstype = RepSuscriptionType::model()->findByAttributes(array('na_stype' => $stype->na_stype));
            $rephappen->id_suscription = $repstype->id_suscription;
            $rephappen->update();
            echo $stype->na_stype;
        }
    }

    public function actionDetalleMes() {
        if (isset($_GET['mes']) && isset($_GET['ano'])) {
            $tipo = 'month';

            $connection = Yii::app()->db2;
            $command = $connection->createCommand();
            $command->setSelect(@"rp.id_time, rp.week AS semana, rp.month AS mes, rp.year AS ano");
            $command->setFrom(RepHappen::model()->tableName() . ' rh');
            $command->join(RepTime::model()->tableName() . ' rp', "rp.id_time=rh.id_time");
            $command->setWhere("rp.id_time > '2013-03-05'");
            $command->setGroup("rp." . $tipo . ', rp.year');
            $command->setOrder("rp.year ASC, rp.month ASC, rp.week ASC");
            $list = $command->queryAll();

            $lista = array();

            $anorep = $_GET['ano'];
            $mesrep = $_GET['mes'];

            $ano = $_GET['ano'];
            $mes = $_GET['mes'];

            $command = $connection->createCommand();
            $command->setSelect("COUNT(h.`id_happen`) as cuantos");
            $command->setFrom(RepHappen::model()->tableName() . ' h');
            $command->join(RepEvent::model()->tableName() . ' e', "h.id_event=e.id_event");
            $command->join(RepTime::model()->tableName() . ' t', "h.id_time=t.id_time");
            $command->setWhere(@"e.`na_event`='suscriptor' AND t.`month`=" . $mesrep . " AND t.`year`=" . $anorep);
            $sus = $command->queryRow();

            $command = $connection->createCommand();
            $command->setSelect("h.`id_happen`, (select `email1` from `rep_user` where `id_user`=h.`id_user`) as email, (SELECT `id_happen` from `rep_happen` where `id_user`=h.`id_user` and (`id_event`=3 or `id_event`=6) and DATE(id_time)>='" . $anorep . "-" . $mesrep . "-01' limit 1) as activo, h.`id_time` as fecha, `na_event` as estado, city as valid, na_ptype as servicio, st.amount as plan");
            $command->setFrom(RepHappen::model()->tableName() . ' h');
            $command->join(RepEvent::model()->tableName() . ' e', "h.`id_event`=e.`id_event`");
            $command->join(RepTime::model()->tableName() . ' t', "h.`id_time`=t.`id_time`");
            $command->leftjoin(RepPayType::model()->tableName() . ' pt', "h.`id_ptype`=pt.`id_ptype`");
            $command->leftjoin(RepSuscriptionType::model()->tableName() . ' st', "h.`id_suscription`=st.`id_suscription`");
            $command->setGroup("h.id_user");
            $command->setWhere(@"e.`na_event`='suscriptor' AND t.`month`=" . $mesrep . " AND t.`year`=" . $anorep);
            $inscritos = $command->queryAll();

            $meses[] = array(
                'titulo' => 'Suscriptores No Deudores ni Cancelados de',
                'mes' => $mes,
                'ano' => $ano,
                'cuantos' => count($inscritos),
                'lista' => $inscritos
            );

            $base = $sus['cuantos'];
            for ($i = 0; $i < 15; $i++) {
                $command = $connection->createCommand();
                $command->setSelect("h.`id_happen`, (select `email1` from `rep_user` where `id_user`=h.`id_user`) as email, h.`id_time` as fecha, `na_event` as estado, city as valid, na_ptype as servicio, st.amount as plan");
                $command->setFrom(RepHappen::model()->tableName() . ' h');
                $command->join(RepEvent::model()->tableName() . ' e', "h.`id_event`=e.`id_event`");
                $command->join(RepPayType::model()->tableName() . ' pt', "h.`id_ptype`=pt.`id_ptype`");
                $command->leftjoin(RepSuscriptionType::model()->tableName() . ' st', "h.`id_suscription`=st.`id_suscription`");
                if ($_GET['deudores'] !== "") {
                    if ($_GET['deudores'] === "90") {
                        if (strtotime("now -90 days") > strtotime($ano . "-" . $mes . "-01")) {
                            $command->setWhere(@"(h.`id_event`=6 OR h.`id_event`=3) AND MONTH(h.`id_time`)=" . $mes . " AND YEAR(h.`id_time`)=" . $ano . " AND
                                (SELECT rh.`id_user`
                                FROM `rep_happen` rh
                                WHERE rh.`id_event`=2 AND MONTH(rh.`id_time`)=" . $mesrep . " AND YEAR(rh.`id_time`)=" . $anorep . " AND rh.`id_user`=h.`id_user` LIMIT 1) IS NOT NULL");
                        } else {
                            $command->setWhere(@"h.`id_event`=6 AND MONTH(h.`id_time`)=" . $mes . " AND YEAR(h.`id_time`)=" . $ano . " AND
                                (SELECT rh.`id_user`
                                FROM `rep_happen` rh
                                WHERE rh.`id_event`=2 AND MONTH(rh.`id_time`)=" . $mesrep . " AND YEAR(rh.`id_time`)=" . $anorep . " AND rh.`id_user`=h.`id_user` LIMIT 1) IS NOT NULL");
                        }
                    } else {
                        $command->setWhere(@"(h.`id_event`=6 OR h.`id_event`=3) AND MONTH(h.`id_time`)=" . $mes . " AND YEAR(h.`id_time`)=" . $ano . " AND
                            (SELECT rh.`id_user`
                            FROM `rep_happen` rh
                            WHERE rh.`id_event`=2 AND MONTH(rh.`id_time`)=" . $mesrep . " AND YEAR(rh.`id_time`)=" . $anorep . " AND rh.`id_user`=h.`id_user` LIMIT 1) IS NOT NULL");
                    }
                } else {
                    $command->setWhere(@"h.`id_event`=6 AND MONTH(h.`id_time`)=" . $mes . " AND YEAR(h.`id_time`)=" . $ano . " AND
                        (SELECT rh.`id_user`
                        FROM `rep_happen` rh
                        WHERE rh.`id_event`=2 AND MONTH(rh.`id_time`)=" . $mesrep . " AND YEAR(rh.`id_time`)=" . $anorep . " AND rh.`id_user`=h.`id_user` LIMIT 1) IS NOT NULL");
                }

                $resultado = $command->queryAll();

                $meses[] = array(
                    'titulo' => 'Bajas de ',
                    'mes' => $mes,
                    'ano' => $ano,
                    'cuantos' => count($resultado),
                    'lista' => $resultado
                );

                $ano = date('Y', strtotime($ano . "-" . $mes . "-01 +1 " . $tipo));
                $mes = date('m', strtotime($ano . "-" . $mes . "-01 +1 " . $tipo));
                $base-=count($resultado);
            }

            $connection = Yii::app()->db;
            $command = $connection->createCommand();
            $command->setSelect("id_stype, na_stype");
            $command->setFrom(SuscriptionType::model()->tableName() . ' st');
            $command->setWhere("st.active=1 AND st.recurrence=1 AND na_stype LIKE '%Sin Trial%'");
            $listaPlanes = $command->queryAll();

            $this->render('reportes/detalleMes', array('lista' => $meses, 'listaPlanes' => $listaPlanes));
        }
    }

    public function actionMailSuscriptores() {
        $report = new Reports;
        $excelReport = $report->createSuscriptores('excel5', false);

        $mail = new Mail;
        $mail->toAddress = explode(',', $_POST["mails"]);
        $mail->sendSuscriptores($excelReport);
    }

    public function actionPdfSuscriptores() {
        $report = new Reports;
        $report->createSuscriptores('pdf', true);
    }

    public function actionExcelSuscriptores() {
        $report = new Reports;
        $report->createSuscriptores('excel5', true);
    }

    public function actionDetalleSuscriptores() {
        if (isset($_GET['busqueda'])) {
            $connection = Yii::app()->db2;
            $command = $connection->createCommand();
            $command->setSelect("id_happen, email1, na_event, h.created_at, na_ptype, st.amount as plan");
            $command->setFrom(RepHappen::model()->tableName() . ' h');
            $command->join(RepEvent::model()->tableName() . ' e', 'e.id_event=h.id_event');
            $command->join(RepUser::model()->tableName() . ' u', 'u.id_user=h.id_user');
            $command->leftjoin(RepSuscriptionType::model()->tableName() . ' st', "h.`id_suscription`=st.`id_suscription`");
            $command->join('rep_paytype p', 'p.id_ptype=h.id_ptype');
            $command->setWhere("u.email1 LIKE '%" . $_GET['busqueda'] . "%'");
            $list = $command->queryAll();

            $this->render('reportes/detalleSuscriptores', array('list' => $list, 'busqueda' => $_GET['busqueda']));
        } elseif (isset($_GET['tipo'])) {
            $connection = Yii::app()->db2;
            $command = $connection->createCommand();
            $command->setSelect("id_happen, email1, na_event, h.created_at, na_ptype, st.amount as plan");
            $command->setFrom(RepHappen::model()->tableName() . ' h');
            $command->join(RepEvent::model()->tableName() . ' e', 'e.id_event=h.id_event');
            $command->join(RepUser::model()->tableName() . ' u', 'u.id_user=h.id_user');
            $command->leftjoin(RepSuscriptionType::model()->tableName() . ' st', "h.`id_suscription`=st.`id_suscription`");
            $command->join('rep_paytype p', 'p.id_ptype=h.id_ptype');
            $command->setWhere("e.na_event LIKE '%" . $_GET['tipo'] . "%'");
            $list = $command->queryAll();

            $this->render('reportes/detalleSuscriptores', array('list' => $list, 'busqueda' => $_GET['tipo']));
        } else {
            $connection = Yii::app()->db2;
            $command = $connection->createCommand();
            $command->setSelect("id_happen, email1, na_event, h.created_at, na_ptype");
            $command->setFrom(RepHappen::model()->tableName() . ' h');
            $command->join(RepEvent::model()->tableName() . ' e', 'e.id_event=h.id_event');
            $command->join(RepUser::model()->tableName() . ' u', 'u.id_user=h.id_user');
            $command->join('rep_paytype p', 'p.id_ptype=h.id_ptype');
            $command->setWhere("h.id_time>='" . $_GET['ini'] . "' AND h.id_time<='" . $_GET['fin'] . "'");
            $list = $command->queryAll();

            $this->render('reportes/detalleSuscriptores', array('list' => $list, 'ini' => $_GET['ini'], 'fin' => $_GET['fin']));
        }
    }

    public function actionReporteSuscriptores() {
        $report = new Reports();

        $tipo = "semanal";
        if (isset($_GET['mensual']))
            $tipo = "mensual";

        $deudores = "0";
        if (isset($_GET['deudores']))
            $deudores = $_GET['deudores'];

        $list = $report->getSuscriptoresData($tipo, $deudores);

        $connection = Yii::app()->db;
        $command = $connection->createCommand();
        $command->setSelect("id_user");
        $command->setFrom(User::model()->tableName() . ' u');
        $command->setWhere("u.create > '" . date("Y-m-d", strtotime("-1 week")) . "'");
        $list2 = $command->queryAll();
        $leads = count($list2);

        $this->render('reportes/reporteSuscriptores', array('list' => $list, 'leads' => $leads, 'tipo' => $tipo));
    }

    public function actionCampana() {
        if (isset($_GET['utm_campaign'])) {
            $connection = Yii::app()->db;

            $command = $connection->createCommand();
            $command->setSelect("utm_campaign as utm_campaign, count(u.id_user) as registros, count(h.id_happen) as cupones, count(s.id_suscription) as conversiones");
            $command->setFrom(Campaign::model()->tableName() . ' c');
            $command->join(User::model()->tableName() . ' u', 'u.id_campaign=c.id_campaign');
            $command->leftjoin(Suscription::model()->tableName() . ' s', 's.id_user=u.id_user');
            $command->leftjoin(Happen::model()->tableName() . ' h', 'h.id_user=u.id_user AND id_event=844');
            $command->setWhere("c.utm_campaign='" . $_GET['utm_campaign'] . "'");
            $command->setGroup("c.utm_campaign");
            $campana = $command->queryRow();

            $command = $connection->createCommand();
            $command->setSelect("utm_source, utm_medium, utm_term, utm_content, utm_campaign, count(u.id_user) as registros, count(s.id_suscription) as conversiones");
            $command->setFrom(Campaign::model()->tableName() . ' c');
            $command->join(User::model()->tableName() . ' u', 'u.id_campaign=c.id_campaign');
            $command->leftjoin(Suscription::model()->tableName() . ' s', 's.id_user=u.id_user');
            $command->setWhere("c.utm_campaign='" . $_GET['utm_campaign'] . "'");
            $command->setGroup("utm_source, utm_medium, utm_term, utm_content, utm_campaign");
            $campanas = $command->queryAll();

            $command = $connection->createCommand();
            $command->setSelect("u.email1, u.name, st.na_stype, s.date");
            $command->setFrom(Campaign::model()->tableName() . ' c');
            $command->join(User::model()->tableName() . ' u', 'u.id_campaign=c.id_campaign');
            $command->join(Suscription::model()->tableName() . ' s', 's.id_user=u.id_user');
            $command->join(SuscriptionType::model()->tableName() . ' st', 's.id_stype=st.id_stype');
            $command->setWhere("c.utm_campaign='" . $_GET['utm_campaign'] . "'");
            $conversiones = $command->queryAll();

            $this->render('reportes/campana', array('campana' => $campana, 'campanas' => $campanas, 'conversiones' => $conversiones));
        }
    }

    public function actionReportemx() {
        $connection = Yii::app()->db;
        $command = $connection->createCommand();
        $command->setSelect("u.name, u.email1, c.name AS curso");
        $command->setFrom(Suscription::model()->tableName() . ' s');
        $command->join(User::model()->tableName() . ' u', 'u.id_user=s.id_user');
        $command->join(CourseSuscription::model()->tableName() . ' cs', 'cs.id_suscription=s.id_suscription');
        $command->join(Course::model()->tableName() . ' c', 'c.id_course=cs.id_course');
        $command->setWhere("s.id_stype=16");
        $command->setOrder("s.id_suscription DESC");
        $list = $command->queryAll();

        $this->render('reportes/reportemx', array('list' => $list));
    }

//////////////////EVENTOS

    public function actionEliminarEvento() {
        if (isset($_GET['id']) && isset($_GET['email'])) {
            $model = RepHappen::model()->findByPk($_GET['id']);
            $model->delete();

            $this->redirect(Yii::app()->urlManager->createUrl('admin/detalleSuscriptores', array('busqueda' => $_GET['email'])));
        }
    }

    public function actionNuevoPago() {
        $listtipo = PayType::model()->findAll();
        $model = new Pay;

        if (isset($_POST['Pay']) && isset($_POST['email']) && isset($_POST['id_ptype'])) {
            $user = User::model()->findByAttributes(array('email1' => $_POST['email']));
            if ($user) {
                $connection = Yii::app()->db;
                $command = $connection->createCommand();
                $command->setSelect("s.id_suscription");
                $command->setFrom(Suscription::model()->tableName() . ' s');
                $command->join(SuscriptionType::model()->tableName() . ' st', 'st.id_stype=s.id_stype');
                $command->setWhere("st.recurrence=1 AND s.active=1 AND s.id_user=" . $user->id_user);
                $command->setOrder('s.date DESC');
                $resultado = $command->queryRow();
                if ($resultado) {
                    $model->amount = $_POST['Pay']['amount'];
                    $model->date = OjalaUtils::convertDateToDB($_POST['Pay']['date']);
                    $model->id_suscription = $resultado['id_suscription'];
                    $model->id_ptype = $_POST['id_ptype'];
                    $model->email = $_POST['email'];
                    $model->status = "Completed";
                    $model->currency = 'USD';
                    $model->save();

                    $this->redirect(Yii::app()->urlManager->createUrl('admin/resumen'));
                }
            }
        }

        $this->render('nuevoPago', array('model' => $model, 'listtipo' => $listtipo));
    }

    public function actionNuevoEvento() {
        if (isset($_POST['RepHappen'])) {
            if (isset($_POST['id'])) {
                $model = RepHappen::model()->findByPk($_POST['id']);
                if ($model) {
                    $model->attributes = $_POST['RepHappen'];
                    $model->created_at = OjalaUtils::convertDateToDB($_POST['RepHappen']['id_time']);
                    $model->id_event = $_POST['id_event'];
                    $model->id_ptype = $_POST['id_ptype'];
                    if ($_POST['id_ptype'] == '3') {
                        $model->id_currency = 2; //MXN
                    } else {
                        $model->id_currency = 1; //USD
                    }
                    $model->update();
                }
            } else {
                $model = new RepHappen;
                $model->attributes = $_POST['RepHappen'];
                $model->created_at = OjalaUtils::convertDateToDB($_POST['RepHappen']['id_time']);
                $model->id_event = $_POST['id_event'];

                $repuser = RepUser::model()->findByAttributes(array('email1' => $_POST['email']));

                $transaction = Yii::app()->db->beginTransaction();
                try {
                    if (!$repuser) {
                        $repuser = new RepUser;
                        $repuser->email1 = $_POST['email'];
                        $repuser->save();
                    }
                    if ($_POST['id_ptype'] == '3') {
                        $model->id_currency = 2; //MXN
                    } else {
                        $model->id_currency = 1; //USD
                    }
                    $model->id_ptype = $_POST['id_ptype'];
                    $model->id_user = $repuser->id_user;
                    $model->save();

                    $transaction->commit();
                } catch (Exception $e) {
                    $transaction->rollback();
                    Yii::log($e->getMessage(), CLogger::LEVEL_ERROR);
                }
            }
            $this->redirect(Yii::app()->urlManager->createUrl('admin/detalleSuscriptores', array('busqueda' => $_POST['email'])));
        }

        $list = RepEvent::model()->findAll();
        $listtipo = RepPayType::model()->findAll();
        if (isset($_GET['id'])) {
            $model = RepHappen::model()->findByPk($_GET['id']);
            $user = RepUser::model()->findByPk($model->id_user);
            $this->render('nuevoEvento', array('model' => $model, 'id' => $_GET['id'], 'email' => $user->email1, 'list' => $list, 'listtipo' => $listtipo));
        } else {
            $model = new RepHappen;
            $this->render('nuevoEvento', array('model' => $model, 'list' => $list, 'listtipo' => $listtipo));
        }
    }

//////////////GESTOR DIPLOMADO

    public function actionCrearDiplomado() {
        $group = new Group;
        if (isset($_POST['Group'])) {
            if (isset($_POST['id_group'])) {
                $imagen = array();
                if (isset($_SESSION['images_cover'])) {
                    $imagen = array('image' => $_SESSION['images_cover']);
                }
                Group::model()->updateByPk($_POST['id_group'], $_POST['Group'] + $imagen);
                $this->redirect(Yii::app()->urlManager->createUrl('admin/diplomados'));
            } else {
                $group->attributes = $_POST['Group'];
                $group->image = $_SESSION['images_cover'];
                $group->create_date = new CDbExpression('NOW()');

                if ($group->save())
                    $this->redirect(Yii::app()->urlManager->createUrl('admin/diplomados'));
            }
        }

        if (isset($_GET['id'])) {
            $group = Group::model()->findByPk($_GET['id']);
        }

        $this->render('crearDiplomado', array('group' => $group));
    }

    public function actionAgregarCursoDiplomado() {
        $groupc = new GroupCourse;
        if (isset($_POST['GroupCourse'])) {
            if (isset($_POST['id_gcourse'])) {
                GroupCourse::model()->updateByPk($_POST['id_gcourse'], $_POST['GroupCourse']);
                $this->redirect(Yii::app()->urlManager->createUrl('admin/diplomado', array('id' => $_POST['id_group'])));
            } else {
                $groupc->attributes = $_POST['GroupCourse'];
                $groupc->id_group = $_POST['id_group'];
                if ($groupc->save()) {
                    $this->redirect(Yii::app()->urlManager->createUrl('admin/diplomado', array('id' => $_POST['id_group'])));
                }
            }
        }
        $group = Group::model()->findByPk($_GET['id']);
        $lista = Course::model()->findAll('active = 1');

        if (isset($_GET['gc'])) {
            $groupc = GroupCourse::model()->findByPk($_GET['gc']);
        }
        $this->render('agregarCursoDiplomado', array('groupc' => $groupc, 'lista' => $lista, 'group' => $group));
    }

    public function actionEliminarCursoDiplomado() {
        if (isset($_GET['gc']) && isset($_GET['id'])) {
            $gc = GroupCourse::model()->findByPk($_GET['gc']);
            $gc->delete();

            $this->redirect(Yii::app()->urlManager->createUrl('admin/diplomado', array('id' => $_GET['id'])));
        }
    }

    public function actionDiplomados() {
        $list = Group::model()->findAll();
        $this->render('diplomados', array('list' => $list));
    }

    public function actionEliminarDiplomado() {
        if (isset($_GET['id'])) {
            $group = Group::model()->findByPk($_GET['id']);

            $lgs = GroupSuscription::model()->findAllByAttributes(array('id_group' => $_GET['id']));
            foreach ($lgs as $value) {
                $value->delete();
            }

            $lgc = GroupCourse::model()->findAllByAttributes(array('id_group' => $_GET['id']));
            foreach ($lgc as $value) {
                $value->delete();
            }
            $group->delete();

            $this->redirect(Yii::app()->urlManager->createUrl('admin/diplomados'));
        }
    }

    public function actionDiplomado() {
        if (isset($_GET['id'])) {
            $group = Group::model()->findByPk($_GET['id']);

            $connection = Yii::app()->db;
            $command = $connection->createCommand();
            $command->setSelect("c.name, gc.id_gcourse, gc.nivel");
            $command->setFrom(Course::model()->tableName() . ' c');
            $command->leftjoin(GroupCourse::model()->tableName() . ' gc', 'gc.id_course=c.id_course');
            $command->setWhere('gc.id_group=' . $_GET['id']);
            $command->setOrder('gc.nivel');
            $list = $command->queryAll();

            $this->render('diplomado', array('group' => $group, 'list' => $list));
        }
    }

////////////GESTOR CURSO

    public function actionCurso() {
        $course = new Course;

        if (isset($_POST['Course'])) {
            $course->attributes = $_POST['Course'];
            $course->requirements = $_POST['Course']['requirements'];
            $course->learn = $_POST['Course']['learn'];
            $course->directed = $_POST['Course']['directed'];
            $course->id_cathegory = $_POST['id_cathegory'];
            $course->id_level = $_POST['id_level'];
            $course->id_state = 2; //Borrador
            $course->cover = isset($_SESSION['images_cover']) ? $_SESSION['images_cover'] : NULL;
            $course->category = "technology";
            $course->cost = floatval($course->cost) * 100;
            $course->create = new CDbExpression('NOW()');
            $course->duration = 0;
            if ($course->save(false)) {
                unset($_SESSION['images_cover']);
                $this->redirect(Yii::app()->urlManager->createUrl('admin/cursos'));
            }
        }

        $listCat = Cathegory::model()->findAll();
        $listNiv = Level::model()->findAll();
        $this->render('curso', array('course' => $course, 'listCat' => $listCat, 'listNiv' => $listNiv));
    }

    public function actionEliminarCurso() {
        if (isset($_GET['id_course'])) { //Guardar datos del curso
            $mensaje = "";
            $course = Course::model()->findByPk($_GET['id_course']);
            $links = Link::model()->findAllByAttributes(array('id_course' => $_GET['id_course']));
            $transaction = Yii::app()->db->beginTransaction();

            try {

                //Eliminar los link
                foreach ($links as $link) {
                    $link->delete();
                }

                //Eliminar los course_tag de existir
                $courseTags = CourseTag::model()->findAllByAttributes(array('id_course' => $_GET['id_course']));

                foreach ($courseTags as $courseTag) {
                    $courseTag->delete();
                }

                //Eliminar los group_course de existir
                $groupCourses = GroupCourse::model()->findAllByAttributes(array('id_course' => $_GET['id_course']));

                foreach ($groupCourses as $groupCourse) {
                    $groupCourse->delete();
                }

                //Eliminar los lesson de existir
                $lessons = Lesson::model()->findAllByAttributes(array('id_course' => $_GET['id_course']));

                foreach ($lessons as $lesson) {
                    //Buscar y eliminar los topic
                    $topics = Topic::model()->findAllByAttributes(array('id_lesson' => $lesson->id_lesson));

                    foreach ($topics as $topic) {
                        //Buscar y los progress de ese topic
                        $progresses = Progress::model()->findAllByAttributes(array('id_topic' => $topic->id_topic));

                        //Eliminar el progress
                        foreach ($progresses as $progress) {
                            $progress->delete();
                        }

                        $topic->delete();
                    }

                    $lesson->delete();
                }

                //Buscar todos los course_suscription y eliminarlos
                $courseSuscriptions = CourseSuscription::model()->findAllByAttributes(array('id_course' => $_GET['id_course']));

                foreach ($courseSuscriptions as $courseSuscription) {
                    $courseSuscription->delete();
                }

                $course->delete();
                $transaction->commit();
            } catch (Exception $e) {
                $transaction->rollback();
                Yii::log($e->getMessage(), CLogger::LEVEL_ERROR);
            }
            echo "Curso Eliminado Correctamente";
        } else {
            echo "Curso no encontrado, por favor revisar";
        }
    }

    public function actionCambiarEstadoContacto() {
        if (isset($_GET['id_contact']) AND isset($_GET['id_state'])) {
            $contact = Contact::model()->findByPk($_GET['id_contact']);
            if (isset($_GET['text']))
                $contact->text = $_GET['text'];
            $contact->id_state = $_GET['id_state'];
            $contact->modified = new CDbExpression('NOW()');
            $contact->update();

            $this->redirect(Yii::app()->urlManager->createUrl('admin/perfil', array(
                        'id' => $_GET['id_user']
            )));
        }
    }

    public function actionGuardarContacto() {
        if (isset($_GET['text']) AND isset($_GET['id_user'])) {
            try {
                $contact = new Contact;
                $contact->date_contact = OjalaUtils::convertDateToDB($_GET['date']) . " 08:00:00";
                $contact->date_created = new CDbExpression('NOW()');
                $contact->modified = new CDbExpression('NOW()');
                $contact->id_user = $_GET['id_user'];
                $contact->id_type = 1;
                $contact->id_state = 1;
                $contact->text = $_GET['text'];
                if ($contact->save()) {
                    $this->redirect(Yii::app()->urlManager->createUrl('admin/perfil', array('id' => $_GET['id_user'])));
                } else {
                    print_r($contact->getErrors());
                }
            } catch (Exception $ex) {
                print_r($ex);
            }
        }
    }

    public function actionCambiarEstado() {
        if (isset($_GET['id']) AND isset($_GET['id_state'])) {
            $course = Course::model()->findByAttributes(array('id_course' => $_GET['id']));
            $course->id_state = $_GET['id_state'];
            //Publicado
            if ($_GET['id_state'] == 1 && isset($_GET['date'])) {
                if (empty($_GET['date'])) {
                    $course->publish = new CDbExpression('NOW()');
                } else {
                    $course->publish = OjalaUtils::convertDateToDB($_GET['date']);
                }
            }
            $course->update();
            $this->redirect(Yii::app()->urlManager->createUrl('admin/editarCurso', array(
                        'id' => $course->id_course
            )));
        }
    }

    public function actionGuardarId() {
        if (isset($_GET['id']) && isset($_GET['wistiaid'])) {
            $curso = Course::model()->findByPk($_GET['id']);
            $curso->wistia_uid = $_GET['wistiaid'];
            $curso->update();
            $this->redirect(Yii::app()->urlManager->createUrl('admin/cursos'));
        }
    }

    public function actionGuardarSeguidor() {
        if (isset($_GET['id'], $_GET['valor'])) {
            $user = User::model()->findByPk($_GET['id']);
            $user->newsletter = $_GET['valor'];
            $user->update();
            echo $user->newsletter;
        }
    }

    public function actionAgregarCalendar() {
        if (isset($_GET['id_user'], $_GET['fecha'])) {
            $user = User::model()->findByPk($_GET['id_user']);

            $contact = new Contact;
            $contact->date_contact = $_GET['fecha'] . ':00';
            $contact->date_created = new CDbExpression('NOW()');
            $contact->modified = new CDbExpression('NOW()');
            $contact->id_user = $user->id_user;
            $contact->id_type = 1;
            $contact->id_state = 1;
            $contact->text = "Programado con Calendar";
            $contact->save();
            $fecha1 = date('Ymd\THi00\Z', strtotime($_GET['fecha']." +5 hour"));
            $fecha2 = date('Ymd\THi00\Z', strtotime($_GET['fecha']." +6 hour"));
            $url = "http://www.google.com/calendar/event?action=TEMPLATE&text=Clase de Ojala (".$user->name.")&dates=".$fecha1."/".$fecha2."&details=".$user->email1." ".$user->phone."&location=&trp=false&sprop=&sprop=name:";
            //$this->redirect($url);
            echo $url;
        }
    }

    public function actionAgregarCalendar2() {
        if (isset($_GET['id_user'], $_GET['fecha'])) {
            $user = User::model()->findByPk($_GET['id_user']);

            $contact = new Contact;
            $contact->date_contact = $_GET['fecha'] . ':00';
            $contact->date_created = new CDbExpression('NOW()');
            $contact->modified = new CDbExpression('NOW()');
            $contact->id_user = $user->id_user;
            $contact->id_type = 1;
            $contact->id_state = 1;
            $contact->text = "Programado con Calendar";
            $contact->save();

            $fecha1 = date('Ymd\THi00\Z', strtotime($_GET['fecha'] . " +5 hour"));
            $fecha2 = date('Ymd\THi00\Z', strtotime($_GET['fecha'] . " +6 hour"));
            $url = "http://www.google.com/calendar/event?action=TEMPLATE&text=Clase de Ojala&dates=" . $fecha1 . "/" . $fecha2 . "&details=" . $user->email1 . " " . $user->phone . "&location=&trp=false&sprop=&sprop=name:";
            //$this->redirect($url);
            echo $url;
        }
    }

    public function actionEditarLesson() {
        if (isset($_GET['id'])) {
            $lesson = Lesson::model()->findByPk($_GET['id']);
            $lesson->na_lesson = $_GET['nombre'];
            $lesson->update();
            echo $lesson->na_lesson;
        }
    }

    public function actionPublicoClase() {
        if (isset($_GET['id'])) {
            $topic = Topic::model()->findByPk($_GET['id']);
            if ($topic->public == 1)
                $topic->public = 0;
            else
                $topic->public = 1;
            $topic->update();
            if ($topic->public == 1)
                echo "Clase Cambiada a: Publica";
            else
                echo "Clase Cambiada a: Privada";
        }
    }

    public function actionSubirClase() {
        if (isset($_GET['id'])) {
            $topic = Topic::model()->findByPk($_GET['id']);
            $topic->position = $topic->position - 1;
            $topic->update();
            echo $topic->position;
        }
    }

    public function actionBajarClase() {
        if (isset($_GET['id'])) {
            $topic = Topic::model()->findByPk($_GET['id']);
            $topic->position = $topic->position + 1;
            $topic->update();
            echo $topic->position;
        }
    }

    public function actionBorrarLesson() {
        if (isset($_GET['id'])) {
            $nuevolesson = 0;
            $lesson = Lesson::model()->findByPk($_GET['id']);
            $lessons = Lesson::model()->findAllByAttributes(array('id_course' => $lesson->id_course));
            foreach ($lessons as $value) {
                if ($value->id_lesson != $lesson->id_lesson) {
                    $nuevolesson = $value->id_lesson;
                    break;
                }
            }

            if ($nuevolesson != 0)
                Topic::model()->updateAll(array('id_lesson' => $nuevolesson), "id_lesson=" . $_GET['id']);

            //$lessons=Lesson::model()->updateAll(array('active'=>'0'), "id_lesson=".$_GET['id']);


            $lesson->delete();
            echo $lesson->na_lesson;
        }
    }

    public function actionEditarCurso() {
        if (isset($_GET['id'])) {
            $course = Course::model()->findByAttributes(array('id_course' => $_GET['id']));

            if (isset($_POST['Course'])) {
                $transaction = Yii::app()->db->beginTransaction();

                try {
                    $course->attributes = $_POST['Course'];
                    ;

                    if (isset($_SESSION['images_cover'])) {
                        $course->deleteImages();
                        $course->cover = $_SESSION['images_cover'];
                        $course->generateThumbnails();
                    }


                    //???
                    if ($course->cost < 100) {
                        $course->cost = floatval($course->cost) * 100;
                    }
                    $course->update();
                    unset($_SESSION['images_cover']);

                    if (isset($_POST['tags'])) {
                        CourseTag::model()->deleteAll('id_course=' . $course->id_course);

                        $tags = sprintf("%s", $_POST['tags']);  //Avoids XSS
                        $tagsArray = preg_split('/ |,/', $tags);
                        foreach ($tagsArray as $key => $value) {
                            $tag = Tag::model()->findByAttributes(array('tag' => $value));
                            if (!$tag) {
                                $tag = new Tag;
                                $tag->tag = $value;
                                $tag->save();
                            }
                            $ctag = CourseTag::model()->findByAttributes(array('id_course' => $course->id_course, 'id_tag' => $tag->id_tag));
                            if (!$ctag) {
                                $ctag = new CourseTag;
                                $ctag->id_course = $course->id_course;
                                $ctag->id_tag = $tag->id_tag;
                                $ctag->save();
                            }
                        }
                    }
                    //State
                    //$course->publish = new CDbExpression('NOW()');
                    $transaction->commit();

                    $this->redirect(Yii::app()->urlManager->createUrl('admin/editarCurso', array('id' => $course->id_course, 'msg' => 'Datos guardados correctamente')));
                } catch (Exception $e) {
                    $transaction->rollback();
                    Yii::log($e->getMessage(), CLogger::LEVEL_ERROR);
                    $this->redirect(Yii::app()->urlManager->createUrl('admin/editarCurso', array('id' => $course->id_course, 'msg' => 'Ocurrio un error al intentar guardar')));
                }
            }

            $connection = Yii::app()->db;
            $command = $connection->createCommand();
            $command->setSelect("t.wistia_uid, l.id_lesson, l.na_lesson, l.position as positionl, IF(t.public = 0, 'Pri', 'Pub') as public, t.id_topic, t.package, t.position as positiont, t.na_topic, t.slug");
            $command->setFrom(Topic::model()->tableName() . ' t');
            $command->join(Lesson::model()->tableName() . ' l', 'l.id_lesson = t.id_lesson');
            $command->setWhere("l.id_course=" . $course->id_course);
            $command->setOrder("l.id_lesson, t.position");
            $list = $command->queryAll();

            $listCat = Cathegory::model()->findAll();
            $listNiv = Level::model()->findAll();
            $state = State::model()->findByAttributes(array('id_state' => $course->id_state));

            $tags = CourseTag::model()->with('tag')->findAllByAttributes(array(
                'id_course' => $course->id_course
            ));

            $this->render('editarCurso', array('course' => $course, 'tags' => $tags, 'state' => $state, 'listCat' => $listCat, 'listNiv' => $listNiv, 'list' => $list));
        }
    }

    public function actionEditarPorWistia() {
        if (isset($_GET['id'])) {
            $course = Course::model()->findByAttributes(array('id_course' => $_GET['id']));

            $url_wistia = "https://api.wistia.com/v1/projects/" . $course->wistia_uid . ".xml?api_password=798c438ba43ef0f968474824d764b297c4ba332e";

            if (($response_xml_data = file_get_contents($url_wistia)) === false) {
                echo "Error fetching XML\n";
            } else {
                libxml_use_internal_errors(true);
                $data = simplexml_load_string($response_xml_data);
                if (!$data) {
                    echo "Error loading XML\n";
                    foreach (libxml_get_errors() as $error) {
                        echo "\t", $error->message;
                    }
                } else {
                    $transaction = Yii::app()->db->beginTransaction();

                    try {
                        //XML Wistia Cargo Correctamente $data toda la informacion del curso
                        $course->duration = '0';
                        $course->update();

                        //Editar los lessons de ese curso
                        $lessons = Lesson::model()->updateAll(array('na_lesson' => '#'), "id_course=" . $course->id_course);

                        $position = 0;
                        foreach ($data->medias->media as $key => $media) {
                            $lesson = Lesson::model()->findByAttributes(array('na_lesson' => substr($media->section, 3), 'id_course' => $course->id_course));

                            if ($lesson == null) {
                                //Para comenzar la position de videos en 0
                                $lesson = new Lesson;
                                $lesson->id_course = $course->id_course;
                                $lesson->na_lesson = $media->section;
                                if ($lesson->duration == null) {
                                    $lesson->duration = 0;
                                }
                                $lesson->duration = floatval($lesson->duration) + floatval($media->duration);
                                $lesson->position = (int) substr($media->section, 0, 1) - 1;
                                $lesson->active = 1;
                                $lesson->save();
                            } else { //Si existe la leccion del curso obtener aumentar la duration
                                $lesson->duration = floatval($lesson->duration) + floatval($media->duration);
                                $position = $position + 1;
                            }

                            $course->duration = floatval($course->duration) + floatval($media->duration);
                            $course->update();

                            $id = 'hashed-id';
                            //Guardar el topic del video
                            $topic = Topic::model()->findByAttributes(array('wistia_uid' => $media->$id));
                            if (!$topic) {
                                $topic = new Topic;
                                $topic->id_lesson = $lesson->id_lesson;
                                $topic->id_ttype = 1;
                                $topic->na_topic = $media->name;
                                $topic->wistia_uid = $media->$id;
                                $topic->duration = floatval($media->duration);
                                $topic->position = $position;
                                $topic->active = 1;
                                $topic->save();
                            } else {
                                $topic->id_lesson = $lesson->id_lesson;
                                $topic->id_ttype = 1;
                                $topic->na_topic = $media->name;
                                $topic->duration = floatval($media->duration);
                                $topic->position = $position;
                                $topic->active = 1;
                                $topic->update();
                            }
                        }
                        //Eliminar los lessons de ese curso
                        $ll = Lesson::model()->findAllByAttributes(array('na_lesson' => '#'));
                        foreach ($ll as $l) {
                            $tl = Topic::model()->findAllByAttributes(array('id_lesson' => $l->id_lesson));
                            foreach ($tl as $t) {
                                Progress::model()->deleteAllByAttributes(array('id_topic' => $t->id_topic));
                                $t->delete();
                            }
                            $l->delete();
                        }
                        $transaction->commit();
                    } catch (Exception $e) {
                        $transaction->rollback();
                        Yii::log($e->getMessage(), CLogger::LEVEL_ERROR);
                    }
                }
            }
            $this->redirect(Yii::app()->urlManager->createUrl('admin/editarCurso', array('id' => $_GET['id'])));
        }
    }

    public function actionEditarClase() {
        if (isset($_GET['id']) && isset($_GET['nombre'])) {
            $topic = Topic::model()->findByPk($_GET['id']);
            $topic->na_topic = $_GET['nombre'];
            $topic->update();
            echo $topic->na_topic;
        } else if (isset($_GET['id']) && isset($_GET['descripcion'])) {
            $topic = Topic::model()->findByPk($_GET['id']);
            $topic->description = $_GET['descripcion'];
            $topic->update();
        } else if (isset($_GET['id']) && isset($_GET['posicion'])) {
            $topic = Topic::model()->findByPk($_GET['id']);
            $topic->position = $_GET['posicion'];
            $topic->update();
        }
    }

    public function actionEditarSlugClase() {
        if (isset($_GET['id']) && isset($_GET['slug'])) {
            $topic = Topic::model()->findByPk($_GET['id']);
            $topic->slug = $_GET['slug'];
            $topic->update();
            echo $topic->slug;
        }
    }

    public function actionEditarTelefono() {
        if (isset($_GET['id']) && isset($_GET['phone'])) {
            $user = User::model()->findByPk($_GET['id']);
            $user->phone = $_GET['phone'];
            $user->update();

            $this->redirect(Yii::app()->urlManager->createUrl('admin/seguimiento', array('id' => $user->id_user)));
        }
    }

    public function actionEditarWistiaClase() {
        if (isset($_GET['id']) && isset($_GET['wistia'])) {
            $topic = Topic::model()->findByPk($_GET['id']);
            $topic->wistia_uid = $_GET['wistia'];
            $topic->update();
            echo $topic->wistia_uid;
        }
    }

//////////////FUNCIONES

    public function actionUpload() {
        Yii::import("ext.EAjaxUpload.qqFileUploader");

        $folder = Yii::app()->params['coverPath']; // folder for uploaded files
        $allowedExtensions = array("jpg", "jpeg", "png", "gif"); //array("jpg","jpeg","gif","exe","mov" and etc...
        $sizeLimit = 5 * 1024 * 1024; // maximum file size in bytes
        $uploader = new qqFileUploader($allowedExtensions, $sizeLimit);
        $result = $uploader->handleUpload($folder);
        $return = htmlspecialchars(json_encode($result), ENT_NOQUOTES);

        $fileSize = filesize($folder . $result['filename']); //GETTING FILE SIZE
        $fileName = $result['filename']; //GETTING FILE NAME

        $_SESSION['images_cover'] = $fileName;
        Yii::app()->session['images_cover'] = $fileName;

        echo $return; // it's array*/
    }

    public function actionSubirPdf() {
        Yii::import("ext.EAjaxUpload.qqFileUploader");

        $folder = 'certificados/'; // folder for uploaded files
        $allowedExtensions = array("pdf"); //array("jpg","jpeg","gif","exe","mov" and etc...
        $sizeLimit = 10 * 1024 * 1024; // maximum file size in bytes
        $uploader = new qqFileUploader($allowedExtensions, $sizeLimit, 'Certificado' . $_GET['id_csuscription']);
        $result = $uploader->handleUpload($folder);
        $return = htmlspecialchars(json_encode($result), ENT_NOQUOTES);

        $fileSize = filesize($folder . $result['filename']); //GETTING FILE SIZE
        $fileName = $result['filename']; //GETTING FILE NAME

        $cs = CourseSuscription::model()->findByPk($_GET['id_csuscription']);
        $cs->certificate = 'Certificado' . $_GET['id_csuscription'] . '.pdf';
        $cs->update();

        echo $return; // it's array
    }

    public function actionEscanearMaterial() {
        if (isset($_GET['id_course'])) {
            $mensaje2 = "";
            $course = Course::model()->findByPk($_GET['id_course']);
            $files = array();
            if ($course) {

                $files = scandir('/var/Downloads/package/' . $course->wistia_uid . '/');

                if (count($files) > 2) {
                    $connection = Yii::app()->db;
                    $command = $connection->createCommand();
                    $command->setSelect("t.id_topic");
                    $command->setFrom(Topic::model()->tableName() . ' t');
                    $command->join(Lesson::model()->tableName() . ' l', 'l.id_lesson = t.id_lesson');
                    $command->setWhere("l.id_course=" . $course->id_course);
                    $command->setOrder("l.id_lesson, t.position");
                    $list = $command->queryAll();
                    $i = 1;
                    foreach ($list as $class) {
                        $file = array_search($i . '.zip', $files);
                        if ($file) {
                            $mensaje2.="/ Actualizo" . $i;
                            Topic::model()->updateByPk($class['id_topic'], array('package' => $i . '.zip'));
                        } else {
                            Topic::model()->updateByPk($class['id_topic'], array('package' => null));
                        }
                        $i++;
                    }
                }
            } else {
                $mensaje2 = "No existe el curso";
            }

            $this->render('ejecutarScript', array('mensaje' => $files, 'mensaje2' => $mensaje2));

            //if(!empty($content))
            //touch "allcode.txt";
        }
    }

    public function actionEjecutarScript() {
        $listaStripe = Usuarios::model()->findAll();
        $salida = "";
        foreach ($listaStripe as $usuario) {
            $user = User::model()->findByAttributes(array('email1' => $usuario->Email));
            if ($user) {
                if ($usuario->Delinquent == "true") {
                    $cuantos = Suscription::model()->updateAll(array('active' => '0'), "id_user=" . $user->id_user);
                    $salida.=" & " . $user->email1 . " : " . $cuantos;
                }
            }
        }
        $this->render('ejecutarScript', array('mensaje' => $salida, 'mensaje2' => ""));
    }

////////////ARCHIVOS

    public function procesarArchivos() {
        $url_old = '/var/Downloads/topicos_totales/old_format/';
        $url_new = '/var/Downloads/topicos_totales/nuevo/';
        $resultado = '';
        $carpetas = scandir($url_old);
        foreach ($carpetas as $carpeta) {
            $topic = Topic::model()->findByAttributes(array('id_old' => $carpeta));
            if ($topic) {
                $lesson = Lesson::model()->findByPk($topic->id_lesson);
                $course = Course::model()->findByPk($lesson->id_course);
                if ($course->wistia_uid != "") {
                    if (!file_exists($url_new . $course->wistia_uid)) {
                        mkdir($url_new . $course->wistia_uid, 0777, true);
                    }
                    $archivos = scandir($url_old . $carpeta . '/');

                    if (count($archivos) > 2) {
                        $archivosCompletos = array();
                        foreach ($archivos as $archivo) {
                            if ($archivo != "." && $archivo != "..")
                                $archivosCompletos[] = $url_old . $carpeta . '/' . $archivo;
                        }
                        $result = $this->create_zip($archivosCompletos, $url_new . $course->wistia_uid . '/' . $topic->package);
                        $resultado.=' & ' . $carpeta . ' Zip=' . $result . ': ' . $topic->package;
                    }
                    else {
                        $resultado.=' & ' . $carpeta . ' No Hay Archivos';
                    }
                } else {
                    $resultado.=' & ' . $carpeta . ' No existe WistiaID';
                }
            } else {
                $resultado.=' & ' . $carpeta . ' No existe Topic ' . $carpeta;
            }
        }

        $this->render('procesar', array('resultado' => $resultado));
    }

    public function actionProcesar() {
        $resultado = "";

        $transaction = Yii::app()->db->beginTransaction();
        try {
            ini_set('max_execution_time', 100000);
            set_time_limit(10000);

            if (isset($_GET['mailchimp'])) {
                require_once 'MailChimp.php';
                $MailChimp = new MailChimp('1b6d2d0238718961fb3bf87d131141e9-us6');
                $listas = array("ec46694dd4", "3b83efc289", "a02a81c784");

                $n = 0;
                $a = 0;
                $i = 0;

                $connection = Yii::app()->db;
                $command = $connection->createCommand();
                $command->setSelect("u.email1, u.create");
                $command->setFrom(User::model()->tableName() . ' u');
                $command->join(Suscription::model()->tableName() . ' s', 's.id_user=u.id_user');
                $command->join(SuscriptionType::model()->tableName() . ' st', 'st.id_stype=s.id_stype');
                $command->setWhere("st.recurrence=1 and  st.id_stype!=17 AND st.id_stype!=40");
                $usuarios = $command->queryAll();
                foreach ($usuarios as $key => $value) {
                    $existe = 0;
                    foreach ($listas as $lista) {
                        $params = array(
                            'id' => $lista,
                            'emails' => array(array('email' => $value['email1']))
                        );
                        $object = $MailChimp->call('lists/member-info', $params);

                        if (isset($object['data'][0])) {
                            $n++;
                            $existe = 1;

                            $user = User::model()->findByAttributes(array('email1' => $object['data'][0]['email']));

                            $datetime1 = new DateTime($user->create);
                            $datetime2 = new DateTime($object['data'][0]['timestamp']);
                            $interval = $datetime1->diff($datetime2);
                            if ($interval->format('%R%a') < 0) {
                                $a++;
                                $resultado.=" &" . $user->email1 . " : " . $user->create . " -> " . $object['data'][0]['timestamp'] . ' -> ' . $interval->format('%R%a days ');
                                $user->create = $object['data'][0]['timestamp'];
                                $user->update();
                            }

                            break;
                        }
                    }
                    if ($existe == 0) {
                        $i++;
                    }
                }
                $resultado.=" &EXISTEN: " . $n . '&INEXISTENTES: ' . $i . '&ACTUALIZADOS: ' . $a;
            }

            if (isset($_GET['eliminar_duplicados'])) {
                $connection = Yii::app()->db3;
                $command = $connection->createCommand();
                $command->setSelect("h.id_happen, id_user, e.na_event");
                $command->setFrom(RepHappen::model()->tableName() . ' h');
                $command->join(RepEvent::model()->tableName() . ' e', 'e.id_event=h.id_event');
                $command->setWhere('h.id_event=2 or h.id_event=3 or h.id_event=6');
                $command->setOrder('h.id_user, e.id_event');
                $usuarios = $command->queryAll();
            }

            if (isset($_GET['usuarios_abril'])) {
                $connection = Yii::app()->db3;
                $command = $connection->createCommand();
                $command->setSelect("email, registro, hora");
                $command->setFrom(Mongo::model()->tableName() . ' m');
                $usuarios = $command->queryAll();
                $n = 0;
                $a = 0;
                $c = 0;
                foreach ($usuarios as $value) {
                    $user = User::model()->findByAttributes(array('email1' => $value['email']));
                    if ($user) {
                        if ($user->create != $value['registro']) {
                            $c++;
                            $datetime1 = new DateTime($user->create);
                            $datetime2 = new DateTime($value['registro']);
                            $interval = $datetime1->diff($datetime2);
                            if ($interval->format('%R%a') < 0) {
                                $a++;
                                $user->create = $value['registro'];
                                $user->update();
                                $resultado.=" &" . $user->create . "->" . $value['registro'] . ':' . $interval->format('%R%a days');
                            }
                        }
                    } else {

                        $user = new User;
                        $user->email1 = $value['email'];
                        $user->create = $value['registro'];
                        $user->save(false);
                        $n++;
                        $resultado.=" &NUEVO:" . $value['email'];
                    }
                }
                $resultado.=" &NUEVOS: " . $n . '&CONICIDENCIAS: ' . $c . '&ACTUALIZADOS: ' . $a;
            }

            if (isset($_GET['actualizar_fechas'])) {
                $connection = Yii::app()->db3;
                $command = $connection->createCommand();
                $command->setSelect("email, registro, ppago");
                $command->setFrom(Mongo::model()->tableName() . ' m');
                $command->setWhere('ppago is not null');
                $usuarios = $command->queryAll();

                foreach ($usuarios as $value) {
                    $user = User::model()->findByAttributes(array('email1' => $value['email']));
                    if ($user) {
                        $resultado.=" &" . $user->create . "->" . $value['registro'];
                        $user->create = $value['registro'];
                        $user->update();
                    } else {
                        $resultado.=" &ERROR:" . $value['email'];
                    }

                    $repuser = RepUser::model()->findByAttributes(array('email1' => $value['email']));
                    if (!$repuser) {
                        $repuser = new RepUser;
                        $repuser->email1 = $value['email'];
                        $repuser->save();
                    }

                    $repevent = RepEvent::model()->findByAttributes(array('na_event' => 'suscriptor'));
                    $rephappen = RepHappen::model()->findByAttributes(array('id_event' => $repevent->id_event, 'id_user' => $repuser->id_user));
                    if (!$rephappen) {
                        $rephappen = new RepHappen;
                        $rephappen->id_time = substr($value['ppago'], 0, 10);
                        $rephappen->id_user = $repuser->id_user;
                        $rephappen->id_event = $repevent->id_event;
                        $rephappen->id_currency = 1; //USD
                        $rephappen->id_ptype = 1; //Paypal
                        $rephappen->created_at = $value['ppago'];
                        $rephappen->save();
                        $resultado.="&H+";
                    }
                }
            }

            if (isset($_GET['actualizar_compradores'])) {
                $i = 0;
                $connection = Yii::app()->db;
                $command = $connection->createCommand();
                $command->setSelect("u.email1");
                $command->setFrom(User::model()->tableName() . ' u');
                $command->join(Suscription::model()->tableName() . ' s', 's.id_user=u.id_user');
                $command->join(SuscriptionType::model()->tableName() . ' st', 'st.id_stype=s.id_stype');
                $command->setWhere("s.id_course is not null and (select id_suscription from suscription si inner join suscription_type sti on sti.id_stype=si.id_stype where sti.recurrence=1 and si.id_user=u.id_user limit 1) is null");
                $usuarios = $command->queryAll();
                foreach ($usuarios as $key => $value) {
                    $repuser = RepUser::model()->findByAttributes(array('email1' => $value['email1']));
                    if ($repuser) {
                        $repevent = RepEvent::model()->findByAttributes(array('na_event' => 'suscriptor'));
                        $rephappen = RepHappen::model()->findByAttributes(array('id_event' => $repevent->id_event, 'id_user' => $repuser->id_user), "date(id_time)>'2013-03-01'");
                        if ($rephappen) {
                            $rephappen->delete();
                            $resultado.="& " . $value['email1'] . ' ' . $i;
                            $i++;
                        }
                    }
                }
            }

            if (isset($_GET['actualizar_inexistentes'])) {
                $i = 0;

                $repevent = RepEvent::model()->findByAttributes(array('na_event' => 'suscriptor'));
                $rephappen = RepHappen::model()->findAllByAttributes(array('id_event' => $repevent->id_event), "date(id_time)>='2013-03-01'");
                foreach ($rephappen as $key => $value) {
                    $ru = RepUser::model()->findByPk($value->id_user);
                    $repuser = User::model()->findByAttributes(array('email1' => $ru->email1));
                    if (!$repuser) {
                        $repuser = User::model()->findByAttributes(array('email2' => $ru->email1));
                        if (!$repuser) {
                            $value->delete();
                            $resultado.="& " . $ru->email1 . ' ' . $i;
                            $i++;
                        }
                    }
                }
            }

            if (isset($_GET['actualizar_correos'])) {
                $connection = Yii::app()->db;
                $command = $connection->createCommand();
                $command->setSelect("p.email, u.email1, u.email2");
                $command->setFrom(Pay::model()->tableName() . ' p');
                $command->join(Suscription::model()->tableName() . ' s', 's.id_suscription=p.id_suscription');
                $command->join(User::model()->tableName() . ' u', 'u.id_user=s.id_user');
                $command->setGroup("s.id_suscription");
                $command->setWhere("p.email is not null and p.status='Completed'");
                $usuarios = $command->queryAll();

                foreach ($usuarios as $key => $value) {
                    if ($value['email'] != $value['email1'] && $value['email2'] == "") {
                        $user = User::model()->findByAttributes(array('email1' => $value['email1']));
                        $user->email2 = $value['email'];
                        $user->update();
                    } elseif ($value['email'] != $value['email1'] && $value['email2'] != "" && $value['email'] != $value['email2']) {
                        $user = User::model()->findByAttributes(array('email1' => $value['email1']));
                        $user->email2 = $value['email'];
                        $user->update();
                    }
                }
            }

            if (isset($_GET['suscriptores_churn'])) {
                $connection = Yii::app()->db;
                $command = $connection->createCommand();
                $command->setSelect("email1, u.create, s.date");
                $command->setFrom(User::model()->tableName() . ' u');
                $command->leftjoin(Suscription::model()->tableName() . ' s', 's.id_user=u.id_user');
                $command->setWhere("u.create >= '2014-05-01'");
                $usuarios = $command->queryAll();

                foreach ($usuarios as $value) {
                    $mongo = Mongo::model()->findByAttributes(array('email' => $value['email1']));
                    if (!$mongo) {
                        $mongo = new Mongo;
                        $mongo->email = $value['email1'];
                        $mongo->registro = $value['create'];
                        if (isset($value['date']) && $value['date'] != "")
                            $mongo->ppago = $value['date'];
                        $mongo->save();
                        $resultado.=" &N:" . $value['email1'] . "->" . $value['create'];
                    }
                    else {
                        if ($mongo->ppago == "" && isset($value['date']) && $value['date'] != "") {
                            $mongo->ppago = $value['date'];
                            $mongo->update();
                            $resultado.=" &U:" . $value['email1'] . "->" . $value['create'];
                        }
                    }
                }
            }

            if (isset($_GET['suscriptores_becas'])) {
                $connection = Yii::app()->db;
                $command = $connection->createCommand();
                $command->setSelect("email1, u.create, s.date");
                $command->setFrom(User::model()->tableName() . ' u');
                $command->join(Suscription::model()->tableName() . ' s', 's.id_user=u.id_user');
                $command->join(SuscriptionType::model()->tableName() . ' st', 'st.id_stype=s.id_stype');
                $command->setWhere("st.na_stype='Beca'");
                $usuarios = $command->queryAll();
                $i = 0;
                foreach ($usuarios as $value) {
                    $repuser = RepUser::model()->findByAttributes(array('email1' => $value['email1']));
                    if ($repuser) {
                        $repevent = RepEvent::model()->findByAttributes(array('na_event' => 'suscriptor'));
                        $rephappen = RepHappen::model()->findAllByAttributes(array('id_event' => $repevent->id_event, 'id_user' => $repuser->id_user));
                        foreach ($rephappen as $key => $value) {
                            $resultado.="&H+" . $i;
                            $i++;
                            $value->delete();
                        }
                    }
                }
            }

            if (isset($_GET['suscriptores_mayo'])) {
                $connection = Yii::app()->db;
                $command = $connection->createCommand();
                $command->setSelect("email1, u.create, s.date, st.na_stype");
                $command->setFrom(Suscription::model()->tableName() . ' s');
                $command->join(User::model()->tableName() . ' u', 's.id_user=u.id_user');
                $command->join(SuscriptionType::model()->tableName() . ' st', 'st.id_stype=s.id_stype');
                $command->setGroup("email1");
                $command->setWhere("st.recurrence=1 AND st.id_stype!=17 AND st.id_stype!=40 and DATE(s.date)>='2014-05-01' and DATE(s.date)<='2014-05-31'");
                $usuarios = $command->queryAll();

                foreach ($usuarios as $value) {
                    $repuser = RepUser::model()->findByAttributes(array('email1' => $value['email1']));
                    if (!$repuser) {
                        $repuser = new RepUser;
                        $repuser->email1 = $value['email1'];
                        $repuser->save();
                    }

                    $repst = RepSuscriptionType::model()->findByAttributes(array('na_stype' => $value['na_stype']));
                    $repevent = RepEvent::model()->findByAttributes(array('na_event' => 'suscriptor'));
                    $rephappen = RepHappen::model()->findByAttributes(array('id_event' => $repevent->id_event, 'id_user' => $repuser->id_user));
                    if ($rephappen) {
                        $rephappen->id_event = 23;
                        $rephappen->update();
                    }
                    $rephappen = new RepHappen;
                    $rephappen->id_time = substr($value['date'], 0, 10);
                    $rephappen->id_user = $repuser->id_user;
                    $rephappen->id_event = $repevent->id_event;
                    if ($repst != null)
                        $rephappen->id_suscription = $repst->id_suscription;
                    $rephappen->id_ptype = 4;
                    $rephappen->id_currency = 1; //USD
                    $rephappen->created_at = $value['date'];
                    $rephappen->city = "N";
                    $rephappen->save();
                    $resultado.="&H+";
                }
            }

            if (isset($_GET['suscriptores_mayo2'])) {
                $connection = Yii::app()->db3;
                $command = $connection->createCommand();
                $command->setSelect("email as email1, registro as date");
                $command->setFrom(Mongo::model()->tableName() . ' m');
                $command->setWhere("ppago is not null");
                $usuarios = $command->queryAll();
                $a = 0;
                $c = 0;
                foreach ($usuarios as $value) {
                    $repuser = RepUser::model()->findByAttributes(array('email1' => $value['email1']));
                    if (!$repuser) {
                        $repuser = new RepUser;
                        $repuser->email1 = $value['email1'];
                        $repuser->save();
                    }

                    $repevent = RepEvent::model()->findByAttributes(array('na_event' => 'suscriptor'));
                    $rephappen = RepHappen::model()->findByAttributes(array('id_event' => $repevent->id_event, 'id_user' => $repuser->id_user));
                    if (!$rephappen) {
                        $a++;
                    }
                    $rephappen = new RepHappen;
                    $rephappen->id_time = substr($value['date'], 0, 10);
                    $rephappen->id_user = $repuser->id_user;
                    $rephappen->id_event = $repevent->id_event;
                    $rephappen->id_ptype = 4;
                    $rephappen->id_currency = 1; //USD
                    $rephappen->created_at = $value['date'];
                    $rephappen->save();

                    $c++;
                }
                $resultado.="&TOTAL:" . $c . " ACTUALIZADOS:" . $a;
            }

            if (isset($_GET['progresos'])) {
                $connection = Yii::app()->db;
                $command = $connection->createCommand();
                $command->setSelect("id_progress, id_topic");
                $command->setFrom(Progress::model()->tableName() . ' u');
                $command->setWhere("id_topic!=0 and id_csuscription!=0 and progress<300");
                $progresos = $command->queryAll();
                foreach ($progresos as $value) {
                    $progress = Progress::model()->findByPk($value["id_progress"]);
                    $topic = Topic::model()->findByPk($value["id_topic"]);
                    if ($topic) {
                        if ($progress->progress > $topic->duration) {
                            $resultado.=" &" . $progress->progress . "->" . $topic->duration;
                            $progress->progress = $topic->duration;
                            $progress->update();
                        }
                    }
                }
            }

            if (isset($_GET['eventos3'])) {
                $listanueva = Data::model()->findAll();
                foreach ($listanueva as $key => $value) {
                    $nuevo = Mongo::model()->findByAttributes(array('email' => $value['email1']));
                    if (!$nuevo) {
                        $nuevo = new Mongo;
                        $nuevo->email = $value['email1'];
                        $nuevo->registro = $value['created_at'];
                        $nuevo->save();
                    } elseif ($nuevo AND $nuevo->registro == "0000-00-00 00:00:00") {
                        $nuevo->registro = $value['created_at'];
                        $nuevo->update();
                    }
                }
            }

            if (isset($_GET['eventos2'])) {
                $connection = Yii::app()->db3;
                $command = $connection->createCommand();
                $command->setSelect("registro2, cliente");
                $command->setFrom(Registros::model()->tableName() . ' u');
                $command->setLimit(60000);
                $listanueva = $command->queryAll();

                foreach ($listanueva as $key => $value) {
                    $nuevo = Mongo::model()->findByAttributes(array('email' => $value['cliente']));
                    if (!$nuevo) {
                        $nuevo = new Mongo;
                        $nuevo->email = $value['cliente'];
                        $nuevo->registro = $value['registro2'];
                        $nuevo->save();
                    } elseif ($nuevo) {
                        $nuevo->registro = $value['registro2'];
                        $nuevo->update();
                    }
                }
            }

            if (isset($_GET['eventos4'])) {
                $connection = Yii::app()->db3;
                $command = $connection->createCommand();
                $command->setSelect("registro2, cliente");
                $command->setFrom(Registros::model()->tableName() . ' u');
                $command->setLimit(60000);
                $command->setOffset(60000);
                $listanueva = $command->queryAll();

                foreach ($listanueva as $key => $value) {
                    $nuevo = Mongo::model()->findByAttributes(array('email' => $value['cliente']));
                    if (!$nuevo) {
                        $nuevo = new Mongo;
                        $nuevo->email = $value['cliente'];
                        $nuevo->registro = $value['registro2'];
                        $nuevo->save();
                    } elseif ($nuevo) {
                        $nuevo->registro = $value['registro2'];
                        $nuevo->update();
                    }
                }
            }

            if (isset($_GET['eventos'])) {
                $connection = Yii::app()->db2;
                $command = $connection->createCommand();
                $command->setSelect("created_at, email1");
                $command->setFrom(RepUser::model()->tableName() . ' u');
                $command->join(RepHappen::model()->tableName() . ' h', 'h.id_user=u.id_user');
                $command->setGroup('h.id_user');
                $list = $command->queryAll();

                $fecha = '';
                foreach ($list as $key => $value) {
                    $nuevo = Mongo::model()->findByAttributes(array('email' => $value['email1']));
                    if (!$nuevo) {
                        $nuevo = new Mongo;
                        $nuevo->email = $value['email1'];
                        $nuevo->registro = $value['created_at'];
                        $nuevo->save();
                        $resultado.="&" . $value['email1'];
                    }
                }
                $resultado.="&" . count($list);
            }

            if (isset($_GET['fechas'])) {
                $connection = Yii::app()->db;
                $command = $connection->createCommand();
                $command->setSelect("id_user, create, email1");
                $command->setFrom(User::model()->tableName() . ' u');
                $command->setLimit(100000);
                $list = $command->queryAll();

                $fecha = null;
                foreach ($list as $key => $value) {
                    if ($value['create'] != null) {
                        $fecha = $value['create'];
                    } else {
                        $user1 = User::model()->findByPk($value['id_user']);
                        $user1->create = $fecha;
                        $user1->update();
                    }
                    /*
                      $nuevo=Mongo::model()->findByAttributes(array('email'=>$value['email1']));
                      if(!$nuevo)
                      {
                      $user1=User::model()->findByPk($value['id_user']);
                      if($user1->create!="")
                      {
                      $nuevo=new Mongo;
                      $nuevo->email=$user1->email1;
                      $nuevo->registro=$user1->create;
                      $nuevo->save();
                      }
                      } */
                    $resultado.="&" . $value['email1'] . $value['create'];
                }
                $resultado.="&" . count($list);
            }

            if (isset($_GET['fechas2'])) {
                $criteria = new CDbCriteria();
                $criteria->group = 'cliente';
                $user = Data::model()->findAll($criteria);
                foreach ($user as $key => $value) {
                    $connection = Yii::app()->db3;
                    $command = $connection->createCommand();
                    $command->setSelect("registro2");
                    $command->setFrom(Data::model()->tableName() . ' d');
                    $command->setWhere("cliente='" . $value->cliente . "'");
                    $command->setOrder("registro2");
                    $command->setLimit(1);
                    $list = $command->queryRow();

                    Data::model()->updateAll(array('registro2' => $list['registro2']), "cliente='" . $value->cliente . "'");
                }
                $resultado.="&" . count($user);
                //select registro2 from data where  order by registro2 limit 1;
            }

            if (isset($_GET['fechas3'])) {
                $connection = Yii::app()->db3;
                $command = $connection->createCommand();
                $command->setSelect("cliente, pago2, registro2");
                $command->setFrom(Data::model()->tableName() . ' d');
                $command->setWhere("pago2=(select pago2 from data where cliente=d.cliente order by pago2 asc limit 1)");
                $command->setGroup("cliente");
                $list = $command->queryAll();

                $i = 0;
                $j = 0;
                foreach ($list as $key => $value) {
                    $nuevo = Mongo::model()->findByAttributes(array('email' => $value['cliente']));
                    if (!$nuevo) {
                        $nuevo = new Mongo;
                        $nuevo->email = $value['cliente'];
                        $nuevo->ppago = $value['pago2'];
                        $nuevo->registro = $value['registro2'];
                        $nuevo->save();
                        $resultado.="&G" . $i;
                        $i++;
                    } else {
                        $nuevo->ppago = $value['pago2'];
                        $nuevo->registro = $value['registro2'];
                        $nuevo->update();
                        $resultado.="&A" . $j;
                        $j++;
                    }
                }
                $resultado.="&todos" . count($list) . '-guardar' . $i . '-actualizado' . $j;
                //select registro2 from data where  order by registro2 limit 1;
            }

            if (isset($_GET['fechas4'])) {
                $connection = Yii::app()->db3;
                $command = $connection->createCommand();
                $command->setSelect("email, registro");
                $command->setFrom(Mongo::model()->tableName() . ' m');
                $list = $command->queryAll();

                $i = 0;
                $j = 0;
                foreach ($list as $key => $value) {
                    $nuevo = User::model()->findByAttributes(array('email1' => $value['email']));
                    if ($nuevo) {
                        $nuevo->create = $value['registro'];
                        $nuevo->update();
                        $i++;
                    } else {
                        $resultado.="&F" . $j;
                        $j++;
                    }
                }
                $resultado.="&L" . count($list) . '-actualizados' . $i . '-faltan' . $j;
                //select registro2 from data where  order by registro2 limit 1;
            }

            if (isset($_GET['registros'])) {
                $list = Data::model()->findAll('registro IS NULL');
                $i = 0;
                foreach ($list as $value) {
                    $registro = Registros::model()->findByAttributes(array('cliente' => $value->cliente));
                    if ($registro) {
                        $value->registro = $registro->registro;
                        $value->update();
                        $resultado.="$+" . $i;
                        $i++;
                    } else {
                        $user = User::model()->findByAttributes(array('email1' => $value->cliente));
                        if ($user && $user->create != null) {
                            $value->registro = $user->create;
                            $value->update();
                            $resultado.="$+";
                        }
                    }
                }
            }

            if (isset($_GET['registros2'])) {
                $list = Data::model()->findAll('pago2 IS NOT NULL AND registro2 IS NOT NULL');
                $i = 0;
                foreach ($list as $value) {
                    if ($value->pago2 < $value->registro2) {
                        $value->registro2 = $value->pago2;
                        $value->update();
                        $resultado.="$+" . $i . '-' . $value->cliente;
                        $i++;
                    }
                }
            }

            if (isset($_GET['eliminando'])) {
                $connection = Yii::app()->db2;
                $command = $connection->createCommand();
                $command->setSelect("rh.id_happen");
                $command->setFrom(RepHappen::model()->tableName() . ' rh');
                $command->join(RepPayType::model()->tableName() . ' pt', 'pt.id_ptype=rh.id_ptype');
                $command->setWhere("rh.id_event=2 AND pt.na_ptype='STRIPE' AND rh.city IS NULL");
                $list = $command->queryAll();
                foreach ($list as $value) {
                    $repevent = RepEvent::model()->findByAttributes(array('na_event' => 'compra'));
                    $rephappen = RepHappen::model()->findByPk($value['id_happen']);
                    $rephappen->id_event = $repevent->id_event;
                    $rephappen->update();
                    $resultado = "&+";
                }
            }

            if (isset($_GET['cancelados'])) {
                $connection = Yii::app()->db2;
                $command = $connection->createCommand();
                $command->setSelect("rh.id_happen, rh.id_user, rh.id_suscription");
                $command->setFrom(RepHappen::model()->tableName() . ' rh');
                $command->join(RepPayType::model()->tableName() . ' pt', 'pt.id_ptype=rh.id_ptype');
                $command->setWhere("rh.id_event=2 AND pt.na_ptype='PAYPAL' and rh.id_suscription IS NOT NULL");
                $list = $command->queryAll();
                foreach ($list as $value) {
                    $repevent = RepEvent::model()->findByAttributes(array('na_event' => 'cancelado'));
                    $rephappen = RepHappen::model()->findByAttributes(array('id_event' => $repevent->id_event, 'id_user' => $value['id_user']));
                    if ($rephappen) {
                        $repstype = RepSuscriptionType::model()->findByPk($value['id_suscription']);
                        $rephappen->id_suscription = $repstype->id_suscription;
                        $rephappen->update();
                        $resultado.="&H+";
                    } else {
                        $resultado.="&C-";
                    }

                    $repevent = RepEvent::model()->findByAttributes(array('na_event' => 'deudor'));
                    $rephappen = RepHappen::model()->findByAttributes(array('id_event' => $repevent->id_event, 'id_user' => $value['id_user']));
                    if ($rephappen) {
                        $repstype = RepSuscriptionType::model()->findByPk($value['id_suscription']);
                        $rephappen->id_suscription = $repstype->id_suscription;
                        $rephappen->update();
                        $resultado.="&D+";
                    } else {
                        $resultado.="&H-";
                    }
                }
            }

            if (isset($_GET['sr'])) {
                $connection = Yii::app()->db;
                $command = $connection->createCommand();
                $command->setSelect("u.id_user");
                $command->setFrom(User::model()->tableName() . ' u');
                $command->join(Suscription::model()->tableName() . ' s', 's.id_user=u.id_user');
                $command->setGroup('u.id_user');
                $listaEstudiantes = $command->queryAll();
                $resultado.="Cantidad" . count($listaEstudiantes);
                foreach ($listaEstudiantes as $estudiante) {
                    $command = $connection->createCommand();
                    $command->setSelect("s.id_suscription, s.active");
                    $command->setFrom(Suscription::model()->tableName() . ' s');
                    $command->join(SuscriptionType::model()->tableName() . ' st', 'st.id_stype=s.id_stype');
                    $command->setWhere("st.recurrence=1 AND s.id_user=" . $estudiante['id_user']);
                    $suscriptionesRecurrentes = $command->queryAll();

                    if (count($suscriptionesRecurrentes) == 1) {
                        $listaEstados = SuscriptionStatus::model()->findByAttributes(array('id_suscription' => $suscriptionesRecurrentes[0]['id_suscription'], 'active' => 1));
                        if (!$listaEstados) {
                            $suscriptions = new SuscriptionStatus;
                            if ($suscriptionesRecurrentes[0]['active'] == 1)
                                $suscriptions->id_status = 1; //ACTIVA
                            else
                                $suscriptions->id_status = 3; //DEUDOR
                            $suscriptions->id_suscription = $suscriptionesRecurrentes[0]['id_suscription'];
                            $suscriptions->date = '';
                            $suscriptions->active = 1;
                            $suscriptions->save();
                        }
                    }
                    elseif (count($suscriptionesRecurrentes) > 1) {
                        $resultado.="&-recurrente" . count($suscriptionesRecurrentes);

                        $tieneStatus = 0;
                        foreach ($suscriptionesRecurrentes as $sr) {
                            $listaEstados = SuscriptionStatus::model()->findByAttributes(array('id_suscription' => $sr['id_suscription'], 'active' => 1));
                            if ($listaEstados) {
                                $tieneStatus = 1;
                                break;
                            }
                        }

                        $count = 0;
                        $selecciono1 = 0;
                        foreach ($suscriptionesRecurrentes as $sr) {
                            $listaEstados = SuscriptionStatus::model()->findByAttributes(array('id_suscription' => $sr['id_suscription'], 'active' => 1));

                            if ($count == 0 && !$listaEstados && $tieneStatus == 0) {
                                $suscriptions = new SuscriptionStatus;
                                if ($sr['active'] == 1)
                                    $suscriptions->id_status = 1; //ACTIVA
                                else
                                    $suscriptions->id_status = 3; //DEUDOR
                                $suscriptions->id_suscription = $suscriptionesRecurrentes[0]['id_suscription'];
                                $suscriptions->date = '';
                                $suscriptions->active = 1;
                                $suscriptions->save();
                            }
                            elseif ($count == 0 && !$listaEstados && $tieneStatus == 1) {
                                $s = Suscription::model()->findByPk($sr['id_suscription']);
                                $listapagos = Pay::model()->findAllByAttributes(array('id_suscription' => $sr['id_suscription']));
                                foreach ($listapagos as $p)
                                    $p->delete();
                                $listaEstados = SuscriptionStatus::model()->findAllByAttributes(array('id_suscription' => $sr['id_suscription']));
                                foreach ($listaEstados as $ss)
                                    $ss->delete();
                                $s->delete();
                            } elseif ($count != 0 && $selecciono1 == 0 && $tieneStatus == 1 && !$listaEstados) {
                                $s = Suscription::model()->findByPk($sr['id_suscription']);
                                $listapagos = Pay::model()->findAllByAttributes(array('id_suscription' => $sr['id_suscription']));
                                foreach ($listapagos as $p)
                                    $p->delete();
                                $listaEstados = SuscriptionStatus::model()->findAllByAttributes(array('id_suscription' => $sr['id_suscription']));
                                foreach ($listaEstados as $ss)
                                    $ss->delete();
                                $s->delete();
                            } elseif ($count != 0 && $selecciono1 == 1) {
                                $s = Suscription::model()->findByPk($sr['id_suscription']);
                                $listapagos = Pay::model()->findAllByAttributes(array('id_suscription' => $sr['id_suscription']));
                                foreach ($listapagos as $p)
                                    $p->delete();
                                $listaEstados = SuscriptionStatus::model()->findAllByAttributes(array('id_suscription' => $sr['id_suscription']));
                                foreach ($listaEstados as $ss)
                                    $ss->delete();
                                $s->delete();
                            } else {
                                $selecciono1 = 1;
                            }
                            $count++;
                        }
                        if ($selecciono1 == 0) {
                            $respuesta.="Grave Error";
                        }
                    }
                }
            }

            if (isset($_GET['cs'])) {
                $connection = Yii::app()->db;
                $command = $connection->createCommand();
                $command->setSelect("u.id_user, u.email1, id_course, count(cs.id_csuscription) as cursos");
                $command->setFrom(User::model()->tableName() . ' u');
                $command->join(CourseSuscription::model()->tableName() . ' cs', 'cs.id_user=u.id_user');
                $command->setGroup('u.id_user, cs.id_course');
                $listaAsociaciones = $command->queryAll();

                foreach ($listaAsociaciones as $item) {
                    if ($item['cursos'] > 1) {
                        $listacs = CourseSuscription::model()->findAllByAttributes(array('id_course' => $item['id_course'], 'id_user' => $item['id_user']));
                        $count = 0;
                        foreach ($listacs as $cs) {
                            if ($count != 0) {
                                $progress = Progress::model()->findByAttributes(array('id_csuscription' => $cs->id_csuscription));
                                if ($progress) {
                                    CourseSuscription::model()->updateByPk($cs->id_csuscription, array('active' => '0'));
                                    $resultado.="&-cs" . $item['email1'];
                                } else {
                                    $cs->delete();
                                    //$resultado.="&-rm".$cs->id_csuscription;
                                }
                            }
                            $count++;
                        }
                    }
                }
            }

            /*
              if(isset($_GET['stripe']))
              {
              ini_set('max_execution_time', 500);
              $gw = new StripeWrapper; //Permite inicializar al API de Stripe


              for ($i=0; $i < 2000; $i=$i+100)
              {
              $usuarios=Stripe_Customer::all(array('count'=>100, 'offset'=>$i));
              foreach ($usuarios->data as $item)
              {
              //$customer = Stripe_Customer::retrieve($item['id']);
              //$user = User::model()->findByAttributes(array('email1'=>$customer['email']));
              $user = User::model()->findByAttributes(array('id_service'=>$item['id']));
              if(!$user)
              {
              $customer = Stripe_Customer::retrieve($item['id']);
              $user = User::model()->findByAttributes(array('email1'=>$customer['email']));
              if($user)
              {
              $user->id_service=$item['id'];
              $user->update();
              }
              }

              if($user)
              {

              foreach ($item['subscriptions']['data'] as $suscripcion)
              {
              $plan=$suscripcion['plan']['id'];
              $fecha_vencimiento=gmdate("Y-m-d H:i:s", $suscripcion['current_period_end']);
              $id_service=$suscripcion['id'];
              $status=$suscripcion['status'];

              $connection=Yii::app()->db;
              $command=$connection->createCommand();
              $command->setSelect("s.id_suscription, s.active");
              $command->setFrom(Suscription::model()->tableName() . ' s');
              $command->join(SuscriptionType::model()->tableName() . ' st', 'st.id_stype=s.id_stype');
              $command->setWhere("st.recurrence=1 AND s.id_user=".$user->id_user);
              $suscriptionRecurrentes = $command->queryRow();

              if($suscriptionRecurrentes)
              {
              $suscripcionDB=Suscription::model()->findByPk($suscriptionRecurrentes['id_suscription']);
              $suscripcionDB->id_service=$id_service;
              $suscripcionDB->exp_date=$fecha_vencimiento;
              $suscripcionDB->id_ptype=2; //Stripe
              if($status=='active')
              {
              $suscripcionDB->active=1;
              }
              else
              {
              $suscripcionDB->active=0;
              $resultado.='&CSU:'.$user->email1; //Cancelando una suscripcion
              }
              $suscripcionDB->update();
              }
              else
              {
              $resultado.='&SNE:'.$user->email1; //Falta suscripcion, Grave!
              }
              }
              }
              else
              {
              $resultado .='&UNE:'.$customer['email']; //Usuario no Existe en BD, Grave!
              }
              }
              }
              }
             */

            if (isset($_GET['eventos_delete'])) {
                $gw = new StripeWrapper; //Permite inicializar al API de Stripe

                $j = 0;
                $k = 0;
                for ($i = 0; $i < 2000; $i = $i + 100) {
                    $eventos = Stripe_Event::all(array('count' => 100, 'offset' => $i, 'type' => 'customer.subscription.deleted'));
                    foreach ($eventos->data as $item) {
                        $user = User::model()->findByAttributes(array('id_service' => $item['data']['object']['customer']));
                        if (!$user) {
                            $customer = Stripe_Customer::retrieve($item['data']['object']['customer']);
                            $user = User::model()->findByAttributes(array('email1' => $customer['email']));
                            if ($user) {
                                $user->id_service = $item['data']['object']['customer'];
                                $user->update();
                                $resultado.="&ACT:" . $customer['email'];
                            }
                        }

                        if ($user) {
                            $repuser = RepUser::model()->findByAttributes(array('email1' => $user->email1));
                            if (!$repuser) {
                                $repuser = new RepUser;
                                $repuser->email1 = $user->email1;
                                $res = $repuser->save();
                            }

                            $repevent = RepEvent::model()->findByAttributes(array('na_event' => 'cancelado'));
                            $rephappen = RepHappen::model()->findByAttributes(array('id_event' => $repevent->id_event, 'id_user' => $repuser->id_user));
                            if (!$rephappen) {
                                $rephappen = new RepHappen;
                                $rephappen->id_time = gmdate("Y-m-d", $item['created']);
                                $rephappen->id_user = $repuser->id_user;
                                $rephappen->id_event = $repevent->id_event;
                                $rephappen->id_currency = 1; //USD
                                $rephappen->id_ptype = 2; //Stripe
                                $rephappen->created_at = gmdate("Y-m-d H:i:s", $item['created']);
                                $rephappen->save();
                                $j++;
                            }
                        }
                        $k++;
                    }
                }
                $resultado.="&total: " . $j . " de " . $k;
                /*
                  $j=0;
                  for ($i=0; $i < 2000; $i=$i+100)
                  {
                  $eventos=Stripe_Event::all(array('count'=>100, 'offset'=>$i, 'type'=>'customer.subscription.created'));

                  foreach ($eventos->data as $item)
                  {
                  $user = User::model()->findByAttributes(array('id_service'=>$item['data']['object']['customer']));
                  if(!$user)
                  {
                  $customer = Stripe_Customer::retrieve($item['data']['object']['customer']);
                  $user = User::model()->findByAttributes(array('email1'=>$customer['email']));
                  if($user)
                  {
                  $user->id_service=$item['data']['object']['customer'];
                  $user->update();
                  $resultado.="&ACT:".$customer['email'];
                  }
                  }

                  if($user)
                  {
                  $suscription = Suscription::model()->findByAttributes(array('id_service'=>$item['data']['object']['id']));
                  if(!$suscription)
                  {
                  $connection=Yii::app()->db;
                  $command=$connection->createCommand();
                  $command->setSelect("s.id_suscription");
                  $command->setFrom(Suscription::model()->tableName() . ' s');
                  $command->join(SuscriptionType::model()->tableName() . ' st', 'st.id_stype=s.id_stype');
                  $command->leftJoin(PayType::model()->tableName() . ' pt', 'pt.id_ptype=s.id_ptype');
                  $command->setWhere('st.recurrence=1 AND s.id_user='.$user->id_user);
                  $s = $command->queryRow();
                  if($s)
                  {
                  $suscription = Suscription::model()->findByPk($s['id_suscription']);
                  $suscription->id_service = $item['data']['object']['id'];
                  $suscription->update();
                  }
                  }


                  $repuser=RepUser::model()->findByAttributes(array('email1'=>$user->email1));
                  if(!$repuser)
                  {
                  $repuser=new RepUser;
                  $repuser->email1=$user->email1;
                  $res=$repuser->save();
                  }

                  $repevent=RepEvent::model()->findByAttributes(array('na_event'=>'suscriptor'));

                  $rephappen=RepHappen::model()->findByAttributes(array('id_event'=>$repevent->id_event, 'id_user'=>$repuser->id_user));
                  if(!$rephappen)
                  {
                  $rephappen=new RepHappen;
                  $rephappen->id_time=gmdate("Y-m-d", $item['created']);
                  $rephappen->id_user=$repuser->id_user;
                  $rephappen->id_event=$repevent->id_event;
                  $rephappen->id_currency=1; //USD
                  $rephappen->id_ptype=2; //Stripe
                  $rephappen->city='C';
                  $rephappen->created_at=gmdate("Y-m-d H:i:s", $item['created']);
                  $rephappen->save();
                  $resultado.="&+";
                  }
                  else
                  {
                  $rephappen->city='C';
                  $rephappen->update();
                  $resultado.="&-";
                  }
                  }
                  }
                  }
                 */

                /* $j=0;
                  for ($i=0; $i < 2000; $i=$i+100)
                  {
                  $eventos=Stripe_Event::all(array('count'=>100, 'offset'=>$i, 'type'=>'customer.subscription.created'));

                  foreach ($eventos->data as $item)
                  {
                  $user = User::model()->findByAttributes(array('id_service'=>$item['data']['object']['customer']));
                  if($user)
                  {
                  //Yii::log("user: ".var_export($item['data']['object']['plan']['name'], true), CLogger::LEVEL_ERROR);
                  $repuser=RepUser::model()->findByAttributes(array('email1'=>$user->email1));
                  if($repuser)
                  {
                  $repevent=RepEvent::model()->findByAttributes(array('na_event'=>'cancelado'));
                  $rephappen=RepHappen::model()->findByAttributes(array('id_event'=>$repevent->id_event, 'id_user'=>$repuser->id_user));
                  if($rephappen)
                  {
                  $repstype=RepSuscriptionType::model()->findByAttributes(array('na_stype' => $item['data']['object']['plan']['name']));
                  if(!$repstype)
                  {
                  $repstype=new RepSuscriptionType;
                  $repstype->na_stype=$item['data']['object']['plan']['name'];
                  $repstype->save();
                  }
                  $rephappen->id_suscription=$repstype->id_suscription;
                  $rephappen->update();
                  $resultado.="&H+".$user->email1;
                  }
                  else
                  {
                  $resultado.="&H-";
                  }

                  $repevent=RepEvent::model()->findByAttributes(array('na_event'=>'deudor'));
                  $rephappen=RepHappen::model()->findByAttributes(array('id_event'=>$repevent->id_event, 'id_user'=>$repuser->id_user));
                  if($rephappen)
                  {
                  $repstype=RepSuscriptionType::model()->findByAttributes(array('na_stype' => $item['data']['object']['plan']['name']));
                  if(!$repstype)
                  {
                  $repstype=new RepSuscriptionType;
                  $repstype->na_stype=$item['data']['object']['plan']['name'];
                  $repstype->save();
                  }
                  $rephappen->id_suscription=$repstype->id_suscription;
                  $rephappen->update();
                  $resultado.="&H+".$user->email1;
                  }
                  else
                  {
                  $resultado.="&H-";
                  }
                  }
                  }
                  else
                  {
                  $resultado.="&U-";
                  }
                  }
                  } */
            }

            if (isset($_GET['stripe_fechas3'])) {
                $gw = new StripeWrapper; //Permite inicializar al API de Stripe


                $j = 0;
                $k = 0;
                $l = 0;
                for ($i = 0; $i < 2000; $i = $i + 100) {
                    $usuarios = Stripe_Customer::all(array('count' => 100, 'offset' => $i));

                    foreach ($usuarios->data as $item) {
                        if ($item['subscriptions']['total_count'] == 0) {
                            $k++;
                            $user = User::model()->findByAttributes(array('id_service' => $item['id']));
                            if (!$user) {
                                $customer = Stripe_Customer::retrieve($item['id']);
                                $user = User::model()->findByAttributes(array('email1' => $customer['email']));
                            }

                            if ($user) {
                                $connection = Yii::app()->db;
                                $command = $connection->createCommand();
                                $command->setSelect("s.id_suscription, s.active");
                                $command->setFrom(Suscription::model()->tableName() . ' s');
                                $command->join(SuscriptionType::model()->tableName() . ' st', 'st.id_stype=s.id_stype');
                                $command->setWhere('st.recurrence=1 AND s.active=1 AND st.id_stype!=17 AND st.id_stype!=40 AND s.id_user=' . $user->id_user);
                                $query = $command->queryRow();

                                if (!$query) {
                                    $command = $connection->createCommand();
                                    $command->setSelect("s.id_suscription, s.active");
                                    $command->setFrom(Suscription::model()->tableName() . ' s');
                                    $command->join(SuscriptionType::model()->tableName() . ' st', 'st.id_stype=s.id_stype');
                                    $command->setWhere('st.recurrence=1 AND st.id_stype!=17 AND st.id_stype!=40 AND s.id_user=' . $user->id_user);
                                    $query = $command->queryRow();

                                    if ($query) {
                                        $user->id_service = $item['id'];
                                        $user->update();

                                        $l++;
                                        $id_suscription = $query['id_suscription'];
                                        $suscription = Suscription::model()->findByPk($id_suscription);

                                        $datetime1 = new DateTime(gmdate("Y-m-d", $item['created']));
                                        $datetime2 = new DateTime($suscription->date);
                                        $interval = $datetime1->diff($datetime2);

                                        if ($interval->format('%a') != 0) {
                                            $j++;
                                            $suscription->date = gmdate("Y-m-d H:i", $item['created']);
                                        }
                                        $suscription->id_ptype = 2;
                                        $suscription->update();

                                        $resultado.="&ACT: " . $item['subscriptions']['total_count'] . " : " . $suscription->id_service . " : " . $user->email1 . " : " . $suscription->date;
                                    } else {
                                        $resultado.=" &ERROR: " . $user->email1;
                                    }
                                }
                            } else {
                                $resultado.=" &USERERROR: " . $item['id'];
                            }
                        }
                    }
                }
                $resultado.=" &TODOS: " . $k . "-ACTIVOS: " . $l . "-FECHAS: " . $j;
            }

            if (isset($_GET['stripe_fechas2'])) {
                $gw = new StripeWrapper; //Permite inicializar al API de Stripe


                $j = 0;
                $k = 0;
                $l = 0;
                for ($i = 0; $i < 2000; $i = $i + 100) {
                    $usuarios = Stripe_Customer::all(array('count' => 100, 'offset' => $i));

                    foreach ($usuarios->data as $item) {
                        if ($item['subscriptions']['total_count'] > 0 && $item['subscriptions']['data'][0]['status'] != "active") {
                            $k++;
                            $user = User::model()->findByAttributes(array('id_service' => $item['id']));
                            if (!$user) {
                                $customer = Stripe_Customer::retrieve($item['id']);
                                $user = User::model()->findByAttributes(array('email1' => $customer['email']));
                            }

                            if ($user) {
                                $connection = Yii::app()->db;
                                $command = $connection->createCommand();
                                $command->setSelect("s.id_suscription, s.active");
                                $command->setFrom(Suscription::model()->tableName() . ' s');
                                $command->join(SuscriptionType::model()->tableName() . ' st', 'st.id_stype=s.id_stype');
                                $command->setWhere('st.recurrence=1 AND s.active=1 AND st.id_stype!=17 AND st.id_stype!=40 AND s.id_user=' . $user->id_user);
                                $query = $command->queryRow();

                                if (!$query) {
                                    $command = $connection->createCommand();
                                    $command->setSelect("s.id_suscription, s.active");
                                    $command->setFrom(Suscription::model()->tableName() . ' s');
                                    $command->join(SuscriptionType::model()->tableName() . ' st', 'st.id_stype=s.id_stype');
                                    $command->setWhere('st.recurrence=1 AND st.id_stype!=17 AND st.id_stype!=40 AND s.id_user=' . $user->id_user);
                                    $query = $command->queryRow();

                                    $user->id_service = $item['id'];
                                    $user->update();

                                    if ($query) {
                                        $l++;
                                        $id_suscription = $query['id_suscription'];
                                        $suscription = Suscription::model()->findByPk($id_suscription);

                                        $datetime1 = new DateTime(gmdate("Y-m-d", $item['created']));
                                        $datetime2 = new DateTime($suscription->date);
                                        $interval = $datetime1->diff($datetime2);

                                        if ($interval->format('%a') != 0) {
                                            $j++;
                                            $suscription->date = gmdate("Y-m-d H:i", $item['created']);
                                        }
                                        $suscription->id_service = $item['subscriptions']['data'][0]['id'];
                                        $suscription->id_ptype = 2;
                                        $suscription->update();
                                        //$resultado.="&ACT: ".$item['subscriptions']['total_count']." : ".$suscription->id_service." : ".$user->email1." : ".$suscription->date;
                                    } else {
                                        $resultado.=" &ERROR: " . $user->email1;
                                    }
                                }
                            } else {
                                $resultado.=" &USERERROR: " . $item['id'];
                            }
                        }
                    }
                }
                $resultado.=" &TODOS: " . $k . "-ACTIVOS: " . $l . "-FECHAS: " . $j;
            }

            if (isset($_GET['stripe_fechas'])) {
                $gw = new StripeWrapper; //Permite inicializar al API de Stripe


                $j = 0;
                $k = 0;
                $l = 0;
                for ($i = 0; $i < 2000; $i = $i + 100) {
                    $usuarios = Stripe_Customer::all(array('count' => 100, 'offset' => $i));

                    foreach ($usuarios->data as $item) {
                        if ($item['subscriptions']['total_count'] > 0 && $item['subscriptions']['data'][0]['status'] == "active") {
                            $k++;
                            $user = User::model()->findByAttributes(array('id_service' => $item['id']));
                            if (!$user) {
                                $customer = Stripe_Customer::retrieve($item['id']);
                                $user = User::model()->findByAttributes(array('email1' => $customer['email']));
                                if ($user) {
                                    $user->id_service = $item['id'];
                                    $user->update();
                                }
                            }

                            if ($user) {
                                $connection = Yii::app()->db;
                                $command = $connection->createCommand();
                                $command->setSelect("s.id_suscription, s.active");
                                $command->setFrom(Suscription::model()->tableName() . ' s');
                                $command->join(SuscriptionType::model()->tableName() . ' st', 'st.id_stype=s.id_stype');
                                $command->setWhere('st.recurrence=1 AND s.active=1 AND st.id_stype!=17 AND st.id_stype!=40 AND s.id_user=' . $user->id_user);
                                $query = $command->queryRow();

                                if (!$query) {
                                    $command = $connection->createCommand();
                                    $command->setSelect("s.id_suscription, s.active");
                                    $command->setFrom(Suscription::model()->tableName() . ' s');
                                    $command->join(SuscriptionType::model()->tableName() . ' st', 'st.id_stype=s.id_stype');
                                    $command->setWhere('st.recurrence=1 AND st.id_stype!=17 AND st.id_stype!=40 AND s.id_user=' . $user->id_user);
                                    $query = $command->queryRow();
                                }

                                $id_suscription = 0;
                                if ($query) {
                                    $l++;
                                    $id_suscription = $query['id_suscription'];
                                    $suscription = Suscription::model()->findByPk($id_suscription);

                                    $datetime1 = new DateTime(gmdate("Y-m-d", $item['created']));
                                    $datetime2 = new DateTime($suscription->date);
                                    $interval = $datetime1->diff($datetime2);

                                    if ($interval->format('%a') != 0) {
                                        $j++;
                                        $suscription->date = gmdate("Y-m-d H:i", $item['created']);
                                    }
                                    $suscription->active = 1;
                                    $suscription->id_service = $item['subscriptions']['data'][0]['id'];
                                    $suscription->id_ptype = 2;
                                    $suscription->update();
                                    $resultado.="&ACT: " . $item['subscriptions']['total_count'] . " : " . $suscription->id_service . " : " . $user->email1 . " : " . $suscription->date;
                                } else {
                                    $resultado.=" &ERROR: " . $user->email1;
                                }
                            } else {
                                $resultado.=" &USERERROR: " . $item['id'];
                            }
                        }
                    }
                }
                $resultado.=" &TODOS: " . $k . "-ACTIVOS: " . $l . "-FECHAS: " . $j;
            }

            if (isset($_GET['carga_pagos_stripe'])) {
                $gw = new StripeWrapper; //Permite inicializar al API de Stripe


                $j = 0;
                $k = 0;
                for ($i = 0; $i < 2000; $i = $i + 100) {
                    $k++;
                    $usuarios = Stripe_Customer::all(array('count' => 100, 'offset' => $i));

                    foreach ($usuarios->data as $item) {
                        if ($item['subscriptions']['total_count'] > 0 && $item['subscriptions']['data'][0]['status'] == "active") {
                            $k++;
                            $user = User::model()->findByAttributes(array('id_service' => $item['id']));
                            if (!$user) {
                                $customer = Stripe_Customer::retrieve($item['id']);
                                $user = User::model()->findByAttributes(array('email1' => $customer['email']));
                                if ($user) {
                                    $user->id_service = $item['id'];
                                    $user->update();
                                }
                            }

                            if ($user) {
                                $resultado.="&USU:" . $user->email1;
                                $facturas = Stripe_Invoice::all(array("customer" => $item['id'], "count" => 30));
                                $fecha = "";
                                foreach ($facturas->data as $factura) {
                                    if ($factura['paid'] == "1")
                                        break;
                                    $fecha = $factura['date'];
                                }
                            }
                        }
                    }
                }
                $resultado.="&total: " . $j . " de " . $k;
            }

            if (isset($_GET['stripe_deudores'])) {
                $gw = new StripeWrapper; //Permite inicializar al API de Stripe


                $j = 0;
                $k = 0;
                for ($i = 0; $i < 2000; $i = $i + 100) {
                    $k++;
                    $usuarios = Stripe_Customer::all(array('count' => 100, 'offset' => $i));

                    foreach ($usuarios->data as $item) {
                        $user = User::model()->findByAttributes(array('id_service' => $item['id']));
                        if (!$user) {
                            $customer = Stripe_Customer::retrieve($item['id']);
                            $user = User::model()->findByAttributes(array('email1' => $customer['email']));
                            if ($user) {
                                $user->id_service = $item['id'];
                                $user->update();
                            }
                        }

                        if ($user) {
                            if ($item['delinquent'] == 'true') {
                                $resultado.="&USU:" . $user->email1;
                                $facturas = Stripe_Invoice::all(array("customer" => $item['id'], "count" => 30));
                                $fecha = "";
                                foreach ($facturas->data as $factura) {
                                    if ($factura['paid'] == "1")
                                        break;
                                    $fecha = $factura['date'];
                                }

                                if ($fecha != "") {
                                    $connection = Yii::app()->db;
                                    $command = $connection->createCommand();
                                    $command->setSelect("s.id_suscription");
                                    $command->setFrom(Suscription::model()->tableName() . ' s');
                                    $command->join(SuscriptionType::model()->tableName() . ' st', 'st.id_stype=s.id_stype');
                                    $command->leftJoin(PayType::model()->tableName() . ' pt', 'pt.id_ptype=s.id_ptype');
                                    $command->setWhere('st.recurrence=1 AND s.id_user=' . $user->id_user);
                                    $s = $command->queryRow();

                                    if ($s) {
                                        $suscription = Suscription::model()->findByPk($s['id_suscription']);

                                        /* $s_status = SuscriptionStatus::model()->findByAttributes(array('id_status' => 3, 'id_suscription'=>$suscription->id_suscription,'active'=>1));
                                          if(!$s_status)
                                          { */
                                        $sss = SuscriptionStatus::model()->findAllByAttributes(array('id_suscription' => $suscription->id_suscription, 'active' => 1));
                                        foreach ($sss as $ss) {
                                            $ss->active = 0;
                                            $ss->update();
                                        }

                                        $s_statusn = new SuscriptionStatus;
                                        $s_statusn->id_suscription = $suscription->id_suscription;
                                        $s_statusn->id_status = 3;
                                        $s_statusn->date = gmdate("Y-m-d", $fecha);
                                        $s_statusn->active = 1;
                                        $s_statusn->save();
                                        //}

                                        $suscription->active = 2;
                                        $suscription->update();
                                        //$resultado.="&SUB+";
                                    } else {
                                        $resultado.="&SUB-";
                                    }


                                    $repuser = RepUser::model()->findByAttributes(array('email1' => $user->email1));
                                    if (!$repuser) {
                                        $repuser = new RepUser;
                                        $repuser->email1 = $user->email1;
                                        $res = $repuser->save();
                                    }

                                    $repevent = RepEvent::model()->findByAttributes(array('na_event' => 'deudor'));
                                    $rephappen = RepHappen::model()->findByAttributes(array('id_event' => $repevent->id_event, 'id_user' => $repuser->id_user));
                                    if (!$rephappen) {
                                        $rephappen = new RepHappen;
                                        $rephappen->id_time = gmdate("Y-m-d", $fecha);
                                        $rephappen->id_user = $repuser->id_user;
                                        $rephappen->id_event = $repevent->id_event;
                                        $rephappen->id_currency = 1; //USD
                                        $rephappen->id_ptype = 2; //Stripe
                                        $rephappen->created_at = gmdate("Y-m-d H:i:s", $fecha);
                                        $rephappen->save();
                                        $j++;
                                    }
                                } else {
                                    $resultado.="&ERROR";
                                }
                            }
                        }
                    }
                }
                $resultado.="&total: " . $j . " de " . $k;
            }


            if (isset($_GET['cargar_pagos'])) {
                $gw = new StripeWrapper; //Permite inicializar al API de Stripe


                $j = 0;
                $k = 0;

                $connection = Yii::app()->db;
                $command = $connection->createCommand();
                $command->setSelect("email1, id_service");
                $command->setFrom(User::model()->tableName() . ' u');
                $command->setWhere("id_service is not null");
                $usuarios = $command->queryAll();

                foreach ($usuarios as $key => $usuario) {
                    $k++;
                    try {
                        $pagos = Stripe_Charge::all(array("limit" => 100, "customer" => $usuario['id_service']));

                        foreach ($pagos->data as $item) {
                            $j++;
                            /* $user = User::model()->findByAttributes(array('id_service'=>$item['id']));
                              if(!$user)
                              {
                              $customer = Stripe_Customer::retrieve($item['id']);
                              $user = User::model()->findByAttributes(array('email1'=>$customer['email']));
                              if($user)
                              {
                              $user->id_service=$item['id'];
                              $user->update();
                              }
                              } */
                            $pay = new Pay;
                            $pay->id_ptype = 2;
                            //$pay->id_suscription = $suscription->id_suscription;
                            $pay->date = gmdate("Y-m-d", $item['created']);
                            $pay->amount = $item['amount'] / 100;
                            if ($item['paid'] == 1)
                                $pay->status = "Completed";
                            else
                                $pay->status = "Fail";
                            $pay->code = $item['id'];
                            $pay->currency = strtoupper($item['currency']);
                            $pay->info = "hand";
                            $pay->valid = 1;
                            $pay->save();

                            $resultado.="&pago: " . gmdate("Y-m-d", $item['created']) . ' ' . $item['amount'] . ' ' . $item['paid'] . ' ';
                        }
                    } catch (Exception $ex) {
                        $resultado.="&error: " . $usuario['id_service'];
                    }
                }
                $resultado.="&total: " . $j . " de " . $k;
            }


            if (isset($_GET['paypal'])) {
                $resultado.="&---";
                $connection = Yii::app()->db;
                $command = $connection->createCommand();
                $command->setSelect("email1, st._id as plan, date");
                $command->setFrom(Suscription::model()->tableName() . ' s');
                $command->join(User::model()->tableName() . ' u', 'u.id_user=s.id_user');
                $command->join(SuscriptionType::model()->tableName() . ' st', 'st.id_stype=s.id_stype');
                $command->join(PayType::model()->tableName() . ' pt', 'pt.id_ptype=s.id_ptype');
                $command->setWhere("pt.na_ptype='PAYPAL'");
                $list = $command->queryAll();

                foreach ($list as $key => $value) {
                    $repuser = RepUser::model()->findByAttributes(array('email1' => $value['email1']));
                    if (!$repuser) {
                        $repuser = new RepUser;
                        $repuser->email1 = $value['email1'];
                        $res = $repuser->save();
                    }

                    $repevent = RepEvent::model()->findByAttributes(array('na_event' => 'suscriptor'));
                    $rephappen = RepHappen::model()->findByAttributes(array('id_event' => $repevent->id_event, 'id_user' => $repuser->id_user));
                    if (!$rephappen) {
                        $rephappen = new RepHappen;
                        $rephappen->id_time = substr($value['date'], 0, 10);
                        $rephappen->id_user = $repuser->id_user;
                        $rephappen->id_event = $repevent->id_event;
                        $rephappen->id_currency = 1; //USD
                        $rephappen->id_ptype = 1; //Paypal
                        $rephappen->created_at = $value['date'];
                        $rephappen->save();
                        $resultado.="&+";
                    } else {
                        $resultado.="&-";
                    }
                    $resultado.="&" . $value['email1'] . '-' . $value['plan'] . '-' . substr($value['date'], 0, 10);
                }
            }

            if (isset($_GET['idstripeusuarios'])) {
                $gw = new StripeWrapper; //Permite inicializar al API de Stripe


                for ($i = 0; $i < 2000; $i = $i + 100) {
                    $usuarios = Stripe_Customer::all(array('count' => 100, 'offset' => $i));

                    foreach ($usuarios->data as $item) {
                        $user = User::model()->findByAttributes(array('id_service' => $item['id']));
                        if (!$user) {
                            $customer = Stripe_Customer::retrieve($item['id']);
                            $user = User::model()->findByAttributes(array('email1' => $customer['email']));
                            if ($user) {
                                $user->id_service = $item['id'];
                                $user->update();
                            }
                        }
                    }
                }
            }

            if (isset($_GET['stripe4'])) {
                ini_set('max_execution_time', 500);
                $gw = new StripeWrapper; //Permite inicializar al API de Stripe


                for ($i = 0; $i < 2000; $i = $i + 100) {
                    $usuarios = Stripe_Customer::all(array('count' => 100, 'offset' => $i));

                    foreach ($usuarios->data as $item) {
                        $user = User::model()->findByAttributes(array('id_service' => $item['id']));
                        /* if(!$user)
                          {
                          $customer = Stripe_Customer::retrieve($item['id']);
                          $user = User::model()->findByAttributes(array('email1'=>$customer['email']));
                          if($user)
                          {
                          $user->id_service=$item['id'];
                          $user->update();
                          }
                          } */

                        if ($user) {
                            $facturas = Stripe_Invoice::all(array("customer" => $item['id'], "count" => 30));
                            $fecha = "";
                            $bandera = 0;
                            foreach ($facturas->data as $factura) {
                                if ($factura['paid'] == '1' && $factura['total'] > 0) {
                                    $bandera = 1;
                                    break;
                                }
                                $fecha = $factura['date'];
                            }

                            if ($bandera === 0) {
                                $repuser = RepUser::model()->findByAttributes(array('email1' => $user->email1));
                                if ($repuser) {
                                    $rephappen = RepHappen::model()->findAllByAttributes(array('id_user' => $repuser->id_user));
                                    if (count($rephappen) > 0) {
                                        foreach ($rephappen as $evento) {
                                            $evento->delete();
                                        }
                                        $resultado.="&USU:" . $user->email1;
                                    }
                                }
                            }
                        }
                    }
                }
            }
            $resultado.="&+++";
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();
            Yii::log($e->getMessage(), CLogger::LEVEL_ERROR);
            $this->redirect(Yii::app()->urlManager->createUrl('admin/index', array('e' => $e->getMessage())));
        }

        $this->renderPartial('procesar', array('resultado' => $resultado));
    }

    public function actionWistia() {

        $url_wistia = "https://api.wistia.com/v1/projects.json?page=1&per_page=100&sort_by=created&sort_direction=0&api_password=798c438ba43ef0f968474824d764b297c4ba332e";

        if (!($respuesta = @file_get_contents($url_wistia)) === false) {
            $event_json = json_decode($respuesta, true);
            $cursos = array();
            $i = 0;
            foreach ($event_json as $value) {
                $url_wistia = "https://api.wistia.com/v1/medias.json?project_id=" . $value['hashedId'] . "&api_password=798c438ba43ef0f968474824d764b297c4ba332e";
                $duracion = 0;
                if (!($respuesta = @file_get_contents($url_wistia)) === false) {
                    $videos = json_decode($respuesta, true);

                    foreach ($videos as $video) {
                        $duracion+=$video['duration'];
                    }
                }

                $cursos[$i] = array('curso' => $value['name'], 'wistiaid' => $value['hashedId'], 'duracion' => $duracion, 'clases' => $value['mediaCount']);
                $i++;
            }
        }
        $this->render('wistia', array('cursos' => $cursos));
    }

/////////////BUSCADOR ESTUDIANTES

    public function actionSeguimiento() {
        if (isset($_GET['dato'])) {
            $listaEstudiantes = array();

            $value = explode(' ', $_GET['dato']);
            $contador = 0;
            $name = null;
            $email = null;
            $lastname = null;

            foreach ($value as $keyval) {
                if ($contador == 0) {
                    $email = $keyval;
                    $name = $keyval;
                } else if ($contador == 1) {
                    $lastname = $keyval;
                    $email = null;
                }
                $contador = $contador + 1;
            }

            $criteria = new CDbCriteria;
            $criteria->select = 'id_user,name, lastname, email1, email2, t.create';

            if ($email != null) {
                $criteria->condition = sprintf("(email1 LIKE '%%%s%%') OR (email2 LIKE '%%%s%%')", $email, $email);
            } else {
                $criteria->condition = sprintf("(name LIKE '%%%s%%' AND lastname LIKE '%%%s%%') OR (lastname LIKE '%%%s%%' AND name LIKE '%%%s%%' )", $name, $lastname, $name, $lastname);
            }

            $listaEstudiantes = User::model()->findAll($criteria);

            $user = new User;
            $user->email1 = $_GET['dato'];

            $this->render('seguimiento', array('listaEstudiantes' => $listaEstudiantes, 'user' => $user, 'dato' => $_GET['dato']));
        } else {
            $asesor = "";
            if (isset($_GET['asesor'])) {
                $asesor = " AND newsletter LIKE '%" . $_GET['asesor'] . "%'";
            }
            $connection = Yii::app()->db;
            $command = $connection->createCommand();
            $command->setSelect("u.id_user, u.name, u.lastname, u.email1, u.email2, u.create, u.phone, u.id_region, c.modified, s.date, u.last_sign, u.newsletter, c.text");
            $command->setFrom(User::model()->tableName() . ' u');
            $command->join(Suscription::model()->tableName() . ' s', 's.id_user=u.id_user');
            $command->join(SuscriptionType::model()->tableName() . ' st', 'st.id_stype=s.id_stype');
            $command->leftJoin(Contact::model()->tableName() . ' c', 'c.id_user=u.id_user');
            $command->setWhere("u.newsletter NOT LIKE '%*%' AND st.recurrence=1 AND st.id_stype!=17 AND st.id_stype!=40 AND DATE(s.date)>'2014-08-13'" . $asesor);
            $command->setGroup("u.id_user");
            $command->setOrder("s.date DESC");
            $listaEstudiantes = $command->queryAll();

            $user = new User;
            if (isset($_GET['mode'])) {
                if (isset($_GET['mode']) == "responsive") {
                    $this->renderPartial('seguimientoResp', array('user' => $user, 'listaEstudiantes' => $listaEstudiantes));
                } else {
                    $this->renderPartial('seguimiento', array('user' => $user, 'listaEstudiantes' => $listaEstudiantes, 'mode' => $_GET['mode']));
                }
            } else {
                $this->render('seguimiento', array('user' => $user, 'listaEstudiantes' => $listaEstudiantes));
            }
        }
    }

    public function actionBuscadorEstudiantes() {
        if (isset($_GET['dato'])) {
            $listaEstudiantes = array();

            $value = explode(' ', $_GET['dato']);
            $contador = 0;
            $name = null;
            $email = null;
            $lastname = null;

            foreach ($value as $keyval) {
                if ($contador == 0) {
                    $email = $keyval;
                    $name = $keyval;
                } else if ($contador == 1) {
                    $lastname = $keyval;
                    $email = null;
                }
                $contador = $contador + 1;
            }

            $criteria = new CDbCriteria;
            $criteria->select = 'id_user,name, lastname, email1, email2, t.create';

            if ($email != null) {
                $criteria->condition = sprintf("(email1 LIKE '%%%s%%') OR (email2 LIKE '%%%s%%')", $email, $email);
            } else {
                $criteria->condition = sprintf("(name LIKE '%%%s%%' AND lastname LIKE '%%%s%%') OR (lastname LIKE '%%%s%%' AND name LIKE '%%%s%%' )", $name, $lastname, $name, $lastname);
            }

            $listaEstudiantes = User::model()->findAll($criteria);

            $user = new User;
            $user->email1 = $_GET['dato'];

            $this->render('buscadorEstudiantes', array('listaEstudiantes' => $listaEstudiantes, 'user' => $user, 'dato' => $_GET['dato']));
        } else {
            $connection = Yii::app()->db;
            $command = $connection->createCommand();
            $command->setSelect("u.id_user, u.name, u.lastname, u.email1, u.email2, u.create");
            $command->setFrom(User::model()->tableName() . ' u');
            $command->setOrder("u.id_user DESC");
            $command->setLimit(10);
            $listaEstudiantes = $command->queryAll();

            $user = new User;
            $this->render('buscadorEstudiantes', array('user' => $user, 'listaEstudiantes' => $listaEstudiantes));
        }
    }

    public function actionSuscriptores() {
        $model = new Subscribers();
        $model->unsetAttributes();
        $msg = isset($_GET['msg']) ? $_GET['msg'] : NULL;
        $msgType = isset($_GET['msgType']) ? $_GET['msgType'] : NULL;

        if (isset($_GET['Subscribers']))
            $model->attributes = $_GET['Subscribers'];


        $this->render('suscriptores', array(
            'model' => $model,
            'msg' => $msg,
            'msgType' => $msgType,
        ));
    }

    public function actionEditarSuscripcion($id) {
        $confirmedBeca = true;
        $subscription = Suscription::model()->with('statuses','type')->findByPk($id);

        if (!$subscription) {
            $this->redirect(array('admin/suscriptores',
                'msg' => 'Suscripción no encontrada',
                'msgType' => 'error',
                    ), true);
        }

        $model = User::model()->findByPk($subscription->id_user);
        $model->setScenario('subscriber');

        $subscription->setScenario('subscriber');
        $subscription->subscriptionDate = substr(OjalaUtils::convertDateFromDB($subscription->date), 0, 10);

        if (empty($subscription->id_service)) {
            if ($subscription->id_stype == SuscriptionType::SCHOLARSHIP) {
                $subscription->id_service = SuscriptionType::SCHOLARSHIP_TXT;
                $confirmedBeca = false;
            }

            if ($subscription->id_stype == SuscriptionType::TWOBYONE) {
                $subscription->id_service = SuscriptionType::TWOBYONE_TXT;
                $confirmedBeca = false;
            }
        }

        if (empty($model->id_service)) {
            if ($subscription->id_stype == SuscriptionType::SCHOLARSHIP) {
                $model->id_service = SuscriptionType::SCHOLARSHIP_TXT;
                ;
                $confirmedBeca = false;
            }
            if ($subscription->id_stype == SuscriptionType::TWOBYONE) {
                $model->id_service = SuscriptionType::TWOBYONE_TXT;
                $confirmedBeca = false;
            }
        }

        if (isset($subscription->statuses) && count($subscription->statuses) && !$subscription->active) {
            $subscription->cancelationDate = OjalaUtils::convertDateFromDB(
                            $subscription->statuses[0]->date
            );
            $subscription->cancelationDate = substr($subscription->cancelationDate, 0, 10);
        } else {
            $subscription->cancelationDate = NULL;
        }

        $status = 1;

        switch ($subscription->active) {
            case 1:
                $status = 1;
                break;
            case 2:
                $status = 3;
                break;
            case 0:
                $status = 2;
                break;
            default:
                $status = 1;
        }

        if (isset($_POST['User']) && isset($_POST['Suscription']) && isset($_POST['status'])) {
            $model->attributes = $_POST['User'];
            $subscription->attributes = $_POST['Suscription'];
            $status = isset($_POST['status']) ? $_POST['status'] : 1;

            if ($model->validate() && $subscription->validate()) {
                $transaction = Yii::app()->db->beginTransaction();
                try {
                    $savedStatus = true;
                    $savedUser = $model->save();
                    $subscription->date = OjalaUtils::convertDateToDB($subscription->subscriptionDate) . ' 23:59:59';

                    switch ($status) {
                        case 1:
                            $subscription->active = 1;
                            Happens::activado($model->email1, $subscription->type->na_stype);
                            break;
                        case 2:
                            $subscription->active = 0;
                            Happens::cancelado($model->email1, $subscription->type->na_stype);
                            break;
                        case 3:
                            $subscription->active = 2;
                            Happens::deudor($model->email1, $subscription->type->na_stype);
                            break;
                        case 5:
                            $subscription->active = $model->suscriptions[0]->active;
                            break;

                        default:
                            $subscription->active = 1;
                    }
                    $savedSubscription = $subscription->save();

                    $ss = new SuscriptionStatus();
                    $ss->id_suscription = $subscription->id_suscription;
                    $ss->id_status = $status == NULL ? 2 : $status;
                    $ss->active = 1;

                    if ($status == 1 || empty($subscription->cancelationDate)) {
                        $ss->date = new CDbExpression('NOW()');
                    } else {
                        $ss->date = OjalaUtils::convertDateToDB($subscription->cancelationDate) . ' 23:59:59';
                    }

                    $savedStatus = $ss->save();

                    if ($savedUser && $savedSubscription && $savedStatus) {
                        $transaction->commit();

                        $intercomUser = OjalaUtils::createIntercomUser($model, array(
                                    'STATUS_SUSCRIPCION' => Status::model()->findByPk($status)->na_status,
                        ));

                        if (isset($_GET['returnUrl'])) {
                            $this->redirect($_GET['returnUrl']);
                        } else {
                            $this->redirect(array('admin/suscriptores',
                                'msg' => 'Cambios guardados correctamente',
                                'msgType' => 'sucess',
                                    ), true);
                        }
                    } else {
                        $transaction->rollback();
                    }
                } catch (Exception $e) {
                    $transaction->rollback();
                }
            }
        }

        $suscriptionTypes = SuscriptionType::model()->findAll(array('order' => 'na_stype DESC'));
        $stypes = CHtml::listData($suscriptionTypes, 'id_stype', 'na_stype');

        $this->render('editarSuscripcion', array(
            'model' => $model,
            'subscription' => $subscription,
            'status' => $status,
            'confirmedBeca' => $confirmedBeca,
            'stypes' => $stypes
        ));
    }

    public function actionUsuarios() {

        if (isset($_GET['dato'])) {
            $value = explode(' ', $_GET['dato']);
            $contador = 0;
            $name = null;
            $email = null;
            $lastname = null;
            $nickname = null;

            foreach ($value as $keyval) {
                if ($contador == 0) {
                    $email = $keyval;
                    $name = $keyval;
                    $nickname = $keyval;
                } else if ($contador == 1) {
                    $lastname = $keyval;
                    $email = null;
                }
                $contador = $contador + 1;
            }

            if ($email != null) {
                $connection = Yii::app()->db;
                $command = $connection->createCommand();
                $command->setSelect("u.id_user, u.nickname, u.name, u.lastname, u.email1, u.create, ut.na_utype");
                $command->setFrom(User::model()->tableName() . ' u');
                $command->join(UserType::model()->tableName() . ' ut', 'u.id_utype=ut.id_utype');
                $command->setWhere("(u.id_utype=1 OR u.id_utype=2 OR u.id_utype=5) AND (u.email1 LIKE '%" . $email . "%' OR u.name LIKE '%" . $name . "%' OR u.nickname LIKE '%" . $nickname . "%' )");
                $command->setOrder("ut.na_utype ASC");
                $listaUsers = $command->queryAll();
            } else {
                $connection = Yii::app()->db;
                $command = $connection->createCommand();
                $command->setSelect("u.id_user, u.nickname, u.name, u.lastname, u.email1, u.create, ut.na_utype");
                $command->setFrom(User::model()->tableName() . ' u');
                $command->join(UserType::model()->tableName() . ' ut', 'u.id_utype=ut.id_utype');
                $command->setWhere("(u.id_utype=1 OR u.id_utype=2 OR u.id_utype=5) AND ((name LIKE '%" . $name . "%' AND lastname LIKE '%" . $lastname . "%') OR (lastname LIKE '%" . $name . "%' AND name LIKE '%" . $lastname . "%' ))");
                $command->setOrder("ut.na_utype ASC");
                $listaUsers = $command->queryAll();
                //
            }
        } else {
            $connection = Yii::app()->db;
            $command = $connection->createCommand();
            $command->setSelect("u.id_user, u.nickname, u.name, u.lastname, u.email1, u.create, ut.na_utype");
            $command->setFrom(User::model()->tableName() . ' u');
            $command->join(UserType::model()->tableName() . ' ut', 'u.id_utype=ut.id_utype');
            $command->setWhere("u.id_utype=1 OR u.id_utype=2 OR u.id_utype=5");
            $command->setOrder("ut.na_utype ASC");
            $listaUsers = $command->queryAll();
        }




        $this->render('usuarios', array('listUsers' => $listaUsers));
    }

    public function actionUsuario($id) {
        $user = User::model()->findByPk($id);

        if ($user) {
            if (isset($_POST['User'])) {
                $user->attributes = $_POST['User'];
                $intercomUser = OjalaUtils::createIntercomUser($user, array(
                            'phone' => $user->phone,
                ));

                if ($user->save()) {
                    $this->redirect(Yii::app()->urlManager->createUrl('admin/estudiante', array('id' => $user->id_user)));
                }
            }
            $this->render('usuario', array('user' => $user));
        } else {
            throw new CHttpException(404, User::ERROR_NOT_FOUND);
        }
    }

    public function actionAgregarUsuario() {
        $user = new User;

        if (isset($_POST['User'])) {
            if (isset($_POST['id_user'])) {
                User::model()->updateByPk($_POST['id_user'], $_POST['User']);
                $user = User::model()->findByPk($_POST['id_user']);
                $user->id_utype = $_POST['id_utype'];
                $user->update();
            } else {

                $user = new User;
                $user->attributes = $_POST['User'];
                $user->user = $_POST['User']['email1'];
                $user->id_utype = $_POST['id_utype'];
                $user->create = new CDbExpression('NOW()');
                $user->pass = "1";
                $user->repeatpass = "1";
                $user->save(false);
            }
            $this->redirect(Yii::app()->urlManager->createUrl('admin/usuarios'));
        }
        if (isset($_GET['id'])) {
            $user = User::model()->findByPk($_GET['id']);
        }
        $types = UserType::model()->findAll();
        $this->render('agregarUsuario', array('user' => $user, 'types' => $types));
    }

    public function actionEliminarUsuario() {
        if (isset($_GET['id'])) {
            $user = User::model()->findByPk($_GET['id']);
            $user->id_utype = 3;
            $user->update();

            $suscriptions = Suscription::model()->findAllByAttributes(array('id_user' => $user->id_user));
            foreach ($suscriptions as $susc) {
                $susc->active = 0;
            }

            $this->redirect(Yii::app()->urlManager->createUrl('admin/usuarios'));
        }
    }

    public function actionCambiarTipoUsuario() {
        if (isset($_GET['id']) && isset($_GET['id_utype'])) {
            $user = User::model()->findByPk($_GET['id']);
            $user->id_utype = $_GET['id_utype'];
            $user->update();

            $this->redirect(Yii::app()->urlManager->createUrl('admin/estudiante', array('id' => $user->id_user)));
        }
    }

    public function actionPerfil() {
        if (isset($_GET['id'])) {
            $user = User::model()->findByPk($_GET['id']);

            $connection = Yii::app()->db;

            //Listados para los Combos
            $command = $connection->createCommand();
            $command->setSelect("id_group, nb_group");
            $command->setFrom(Group::model()->tableName() . ' g');
            $listaDiplomados = $command->queryAll();

            $command = $connection->createCommand();
            $command->setSelect("c.id_course, c.name");
            $command->setFrom(Course::model()->tableName() . ' c');
            $command->setWhere("c.active=1");
            $listaCursos = $command->queryAll();

            $command = $connection->createCommand();
            $command->setSelect("id_stype, na_stype");
            $command->setFrom(SuscriptionType::model()->tableName() . ' st');
            $command->setWhere("st.active=1 AND st.recurrence=1");
            $listaPlanes = $command->queryAll();

            //Consulto las Suscripciones y Compras del Usuario

            $command = $connection->createCommand();
            $command->setSelect("s.id_suscription, s.exp_date, st.na_stype, s.date, pt.na_ptype, s.with_downloads as material, st.with_downloads as material2, s.active as estado, s.id_service ");
            $command->setFrom(Suscription::model()->tableName() . ' s');
            $command->join(SuscriptionType::model()->tableName() . ' st', 'st.id_stype=s.id_stype');
            $command->leftjoin(PayType::model()->tableName() . ' pt', 'pt.id_ptype=s.id_ptype');
            $command->setWhere('st.recurrence=1 AND s.id_user=' . $user->id_user);
            $command->setOrder('s.date DESC');
            $suscripcion = $command->queryRow();

            $command = $connection->createCommand();
            $command->setSelect("id_group_susc, nb_group, current_level as nivel");
            $command->setFrom(GroupSuscription::model()->tableName() . ' gs');
            $command->join(Group::model()->tableName() . ' g', 'g.id_group=gs.id_group');
            $command->setWhere("gs.active=1 AND gs.id_user=" . $user->id_user);
            $diplomados = $command->queryAll();

            $command = $connection->createCommand();
            $command->setSelect("s.id_suscription, st.na_stype, st.amount, s.date, c.name, s.description");
            $command->setFrom(Suscription::model()->tableName() . ' s');
            $command->join(SuscriptionType::model()->tableName() . ' st', 'st.id_stype=s.id_stype');
            $command->leftJoin(Course::model()->tableName() . ' c', 'c.id_course=s.id_course');
            $command->setWhere('s.active=1 AND st.recurrence=0 AND s.id_user=' . $user->id_user);
            $compras = $command->queryAll();

            $command = $connection->createCommand();
            $command->setSelect("c.name, cs.id_user, cs.active, cs.certificate, cs.id_csuscription AS idcs, (SELECT topic.na_topic FROM progress JOIN topic ON topic.id_topic=progress.id_topic where id_csuscription=cs.id_csuscription order by topic.id_topic DESC LIMIT 1) AS current, cs.progress as progress");
            $command->setFrom(CourseSuscription::model()->tableName() . ' cs');
            $command->Join(Course::model()->tableName() . ' c', 'c.id_course=cs.id_course');
            $command->setWhere("cs.active=1 AND cs.id_user=" . $user->id_user);
            $cursos = $command->queryAll();

            $command = $connection->createCommand();
            $command->setSelect("c.date_contact, c.text, c.modified, c.id_state, c.id_type, c.id_contact");
            $command->setFrom(Contact::model()->tableName() . ' c');
            $command->setWhere("c.id_user=" . $user->id_user . " AND (DATE(c.date_contact) < DATE(NOW()) OR c.id_state=2 OR c.id_state=3)");
            $command->setOrder("c.modified");
            $listPasados = $command->queryAll();

            $command = $connection->createCommand();
            $command->setSelect("c.date_contact, c.text, c.modified, c.id_state, c.id_type, c.id_contact, IF(DATE(c.date_contact) > DATE(NOW()), 'OK', 'PASADO') as tiempo");
            $command->setFrom(Contact::model()->tableName() . ' c');
            $command->setWhere("c.id_user=" . $user->id_user . " AND id_state=1");
            $command->setOrder("c.modified");
            $listProximos = $command->queryAll();

            $amigo = empty($user->id_refuser) ? NULL : User::model()->findByPk($user->id_refuser);

            $this->render('perfil', array('user' => $user, 'listPasados' => $listPasados, 'listProximos' => $listProximos, 'amigo' => $amigo, 'cursos' => $cursos, 'suscripcion' => $suscripcion, 'diplomados' => $diplomados, 'compras' => $compras, 'listaDiplomados' => $listaDiplomados, 'listaCursos' => $listaCursos, 'listaPlanes' => $listaPlanes));
        }
    }

    public function actionEstudiante()
    {
        if(isset($_GET['id']))
        {
            $user = User::model()->with('subDiplo')->findByPk($_GET['id']);

            $connection=Yii::app()->db;

            //Listados para los Combos
            $groupData = Group::model()->findAll(array('select' => 'id_group, nb_group'));
            $listaDiplomados = CHtml::listData($groupData, 'id_group', 'nb_group');

            $courseData = Course::model()->findAll(array(
                'select' => 'id_course, name',
                'condition' => 'active=1'
            ));
            $listaCursos = CHtml::listData($courseData, 'id_course', 'name');

            $planData = SuscriptionType::model()->findAll(array(
                'select' => 'id_stype, na_stype',
                'condition' => 'active=1 AND recurrence=1'
            ));
            $listaPlanes = CHtml::listData($planData, 'id_stype', 'na_stype');

            $suscripcion = Suscription::model()->with('type', 'pays')->find(array(
                'condition' => 'type.recurrence=1 AND t.id_user=:id_user',
                'params' => array(':id_user' => $user->id_user),
                'order' => 't.`date` DESC'
            ));

            /**
             * @todo Migrar estas consultas al formato de Yii
             */
            $command = $connection->createCommand();
            $command->setSelect("s.id_suscription, st.na_stype, st.amount, s.date, c.name, s.description");
            $command->setFrom(Suscription::model()->tableName() . ' s');
            $command->join(SuscriptionType::model()->tableName() . ' st', 'st.id_stype=s.id_stype');
            $command->leftJoin(Course::model()->tableName() . ' c', 'c.id_course=s.id_course');
            $command->setWhere('s.active=1 AND st.recurrence=0 AND s.id_user=' . $user->id_user);
            $compras = $command->queryAll();

            $command = $connection->createCommand();
            $command->setSelect("c.name, cs.id_user, cs.active, cs.certificate, cs.id_csuscription AS idcs, (SELECT topic.na_topic FROM progress JOIN topic ON topic.id_topic=progress.id_topic where id_csuscription=cs.id_csuscription order by topic.id_topic DESC LIMIT 1) AS current, cs.progress as progress");
            $command->setFrom(CourseSuscription::model()->tableName() . ' cs');
            $command->Join(Course::model()->tableName() . ' c', 'c.id_course=cs.id_course');
            $command->setWhere("cs.active=1 AND cs.id_user=" . $user->id_user);
            $cursos = $command->queryAll();

            $amigo = empty($user->id_refuser) ? NULL : User::model()->findByPk($user->id_refuser);

            $this->render('estudiante', array(
                'user' => $user,
                'amigo' => $amigo,
                'cursos' => $cursos,
                'suscripcion' => $suscripcion,
                'compras' => $compras,
                'listaDiplomados' => $listaDiplomados,
                'listaCursos' => $listaCursos,
                'listaPlanes' => $listaPlanes
            ));
        }
    }

    public function actionCambiarPlan() {
        if (isset($_GET['id']) && isset($_GET['id_stype'])) {
            $stype = SuscriptionType::model()->findByPk($_GET['id_stype']);
            $suscription = Suscription::model()->findByPk($_GET['id']);
            $suscription->id_stype = $stype->id_stype;
            $suscription->update();

            $this->redirect(Yii::app()->urlManager->createUrl('admin/estudiante', array('id' => $suscription->id_user)));
        }
    }

    public function actionGuardarEvento() {
        if (isset($_GET['id']) && isset($_GET['valor'])) {
            $contact = new Contact;
            $contact->date_contact = new CDbExpression('NOW()');
            $contact->date_created = new CDbExpression('NOW()');
            $contact->modified = new CDbExpression('NOW()');
            $contact->id_user = $_GET['id'];
            $contact->id_type = 1;
            $contact->id_state = 2;
            $contact->text = $_GET['valor'];
            $contact->save();

            echo $_GET['valor'];
        }
    }

    public function actionAgregarSuscripcion() {
        /* Comprobar consulta del tipo de suscripción */
        if (isset($_GET['id']) && isset($_GET['id_stype'])) {
            $stype = SuscriptionType::model()->findByPk($_GET['id_stype']);
            $user = User::model()->findByPk($_GET['id']);

            $connection = Yii::app()->db;
            $transaction = $connection->beginTransaction();

            try {
                $ojaPayments = new OjalaPayments;

                if (isset($user->suscriptions) && count($user->suscriptions)) {
                    $lastSubId = $user->suscriptions[0]->id_suscription;
                    $lastSubscription = Suscription::model()->findByPk($lastSubId);
                    $lastSubscription->active = 0;
                    $lastSubscription->save();

                    $ojaPayments->saveStatus($lastSubscription->id_suscription, Status::CANCELED);

                    $plan = SuscriptionType::model()->find('id_stype=:id_stype', array(
                        ':id_stype' => $lastSubscription->id_stype
                    ));
                    Happens::cancelado($user->email1, $plan->na_stype);
                }

                $subscription = new Suscription;
                $subscription->id_user = $user->id_user;
                $subscription->id_stype = $stype->id_stype;
                $subscription->exp_date = $ojaPayments->getExpDate($stype->interval);
                $subscription->with_downloads = 1;
                $subscription->date = new CDbExpression('NOW()');
                $subscription->active = 1;
                $subscription->save();

                $ojaPayments->saveStatus($subscription->id_suscription, Status::ACTIVE, $user);
                Happens::activado($user->email1, $stype->na_stype);

                if ($stype->id_stype == SuscriptionType::SCHOLARSHIP || $stype->id_stype == SuscriptionType::TWOBYONE) {
                    $ojaPayments->saveStatus($subscription->id_suscription, Status::HIDDEN, $user);

                    switch ($stype->id_stype) {
                        case SuscriptionType::SCHOLARSHIP:
                            $stype = SuscriptionType::SCHOLARSHIP_TXT;
                            break;
                        case SuscriptionType::TWOBYONE:
                            $stype = SuscriptionType::TWOBYONE_TXT;
                            break;
                    }
                    $user->id_service = $stype;
                    $user->update();

                    $subscription->id_service = $stype;
                    $subscription->update();
                }

                $transaction->commit();
            } catch (Exception $e) {
                $transaction->rollback();
                Yii::log($e->getMessage(), CLogger::LEVEL_ERROR);
            }
            $this->redirect(Yii::app()->urlManager->createUrl('admin/estudiante', array('id' => $user->id_user)));
        }
    }

    public function actionMaterialDescarga() {
        if (isset($_GET['id'])) {
            Suscription::model()->updateAll(array('with_downloads' => '1'), "id_user = " . $_GET['id']);
            $this->redirect(Yii::app()->urlManager->createUrl('admin/estudiante', array('id' => $_GET['id'])));
        }
    }

    public function actionActivarSuscripcion() {
        if (isset($_GET['id'])) {
            $sub = Suscription::model()->findByPk($_GET['id']);
            $sub->active = 1;
            $sub->update();
			
			$user = User::model()->findByPk(Yii::app()->user->id);

            $new = new SuscriptionStatus;
            $new->id_suscription = $sub->id_suscription;
            $new->id_status = Status::ACTIVE;
            $new->date = new CDbExpression('NOW()');
            $new->save();
            
            Happens::activado($user->email1, $plan->na_stype);

            $this->redirect(Yii::app()->urlManager->createUrl('admin/estudiante', array('id' => $sub->id_user)));
        }
    }

    public function actionCancelarSuscripcion() {
        if (isset($_GET['id'])) {
            $transaction = Yii::app()->db->beginTransaction();

            try {
                $susc = Suscription::model()->findByPk($_GET['id']);
                $user = User::model()->findByPk($susc->id_user);

                Suscription::model()->updateByPk($_GET['id'], array('active' => 0));

                $new = new SuscriptionStatus;
                $new->id_suscription = $_GET['id'];
                $new->id_status = 2; //Cancelado
                $new->date = new CDbExpression('NOW()');
                $new->save();

                $transaction->commit();
            } catch (Exception $e) {
                $transaction->rollback();
                Yii::log($e->getMessage(), CLogger::LEVEL_ERROR);
            }

            $this->redirect(Yii::app()->urlManager->createUrl('admin/estudiante', array(
                        'id' => $user->id_user
            )));
        }
    }

    public function actionInscribirDiplomado() {
        $user = User::model()->findByPk($_GET['idu']);
        $group = Group::model()->findByPk($_GET['diplomado']);

        $existe = GroupSuscription::model()->findByAttributes(array('id_group' => $group->id_group, 'id_user' => $user->id_user));
        if ($existe == null) { //no estÃ¡ inscrito
            $transaction = Yii::app()->db->beginTransaction();

            try {
                $group_susc = new GroupSuscription;
                $group_susc->id_group = $group->id_group;
                $group_susc->id_user = $user->id_user;
                $group_susc->date = new CDbExpression('NOW()');
                $group_susc->active = 1;
                $group_susc->current_level = 1;
                $group_susc->save();

                $cursos_d = GroupCourse::model()->findAllByAttributes(array('id_group' => $group->id_group));
                foreach ($cursos_d as $curso) {
                    //Verificar que el curso ya tiene suscripción por parte del usuario.
                    $course_s = CourseSuscription::model()->findByAttributes(array('id_user' => $user->id_user, 'id_course' => $curso->id_course, "active" => 1));
                    if (!$course_s) {
                        $csi = CourseSuscription::model()->findByAttributes(array("id_user" => $user->id_user, "id_course" => $curso->id_course, "active" => 0));
                        if ($csi) {
                            $csi->active = 1;
                            $csi->update();
                        } else {
                            $coursesu = new CourseSuscription;
                            $coursesu->id_user = $user->id_user;
                            $coursesu->id_course = $curso->id_course;
                            $coursesu->active = 1;
                            $coursesu->save();
                        }
                    }
                }

                $date = date_create();
                $transaction->commit();
            } catch (Exception $e) {
                $transaction->rollback();
                Yii::log($e->getMessage(), CLogger::LEVEL_ERROR);
            }

            OjalaUtils::enviarIntercom(Yii::app()->user->id, $user->email1, $user->name, array("REGISTRO_DIPLOMADO" => $date, "DIPLOMADO" => $group->nb_group), false, "https://oja.la");
        }

        $this->redirect(Yii::app()->urlManager->createUrl('admin/estudiante', array('id' => $user->id_user)));
    }

    public function actionDesinscribirDiplomado() {
        if (isset($_GET['id']) && isset($_GET['idu'])) {
            $gs = GroupSuscription::model()->findByPk($_GET['id']);
            $gs->delete();

            $this->redirect(Yii::app()->urlManager->createUrl('admin/estudiante', array('id' => $_GET['idu'])));
        }
    }

    public function actionEliminarCs() {
        if (isset($_GET['idcs']) && isset($_GET['id'])) {
            $value = $_GET['idcs'];
            CourseSuscription::model()->updateByPk($value, array('active' => '0'));
            $this->redirect(Yii::app()->urlManager->createUrl('admin/estudiante', array('id' => $_GET['id'])));
        }
    }

    ////Agrega una suscripcion relacionada a un curso
    public function actionAgregarCupon() {
        if (isset($_GET['id']) && isset($_GET['id_course']) && isset($_GET['id_stype'])) {
            $transaction = Yii::app()->db->beginTransaction();

            try {
                $suscription = new Suscription;
                $suscription->id_user = $_GET['id'];
                $suscription->id_course = $_GET['id_course'];
                $suscription->id_stype = $_GET['id_stype'];
                $suscription->date = new CDbExpression('NOW()');

                if (isset($_GET['cupon']))
                    $suscription->description = $_GET['cupon'];

                $suscription->active = 1;
                $suscription->save();

                $suscriptionst = new SuscriptionStatus;
                $suscriptionst->id_suscription = $suscription->id_suscription;
                $suscriptionst->id_status = 1;
                $suscriptionst->date = new CDbExpression('NOW()');
                $suscriptionst->active = 1;
                $suscriptionst->save();

                $cs = CourseSuscription::model()->findByAttributes(array('id_user' => $_GET['id'], 'id_course' => $_GET['id_course']));
                if ($cs) {
                    $cs->active = 1;
                    $cs->update();
                } else {
                    $cs = new CourseSuscription;
                    $cs->id_user = $_GET['id'];
                    $cs->id_course = $_GET['id_course'];
                    $cs->active = 1;
                    $cs->save();
                }
                $transaction->commit();
            } catch (Exception $e) {
                $transaction->rollback();
                Yii::log($e->getMessage(), CLogger::LEVEL_ERROR);
            }
            $this->redirect(Yii::app()->urlManager->createUrl('admin/estudiante', array('id' => $_GET['id'])));
        }
    }

    public function actionAgregarCurso() {
        if (isset($_GET['id']) && isset($_GET['id_course'])) {
            $suscription = new Suscription;
            $suscription->id_user = $_GET['id'];
            $suscription->id_course = $_GET['id_course'];
            $suscription->id_stype = 16; // Curso Gratis
            $suscription->date = new CDbExpression('NOW()');
            $suscription->active = 1;
            $suscription->save();

            $suscriptionst = new SuscriptionStatus;
            $suscriptionst->id_suscription = $suscription->id_suscription;
            $suscriptionst->id_status = 1;
            $suscriptionst->date = new CDbExpression('NOW()');
            $suscriptionst->active = 1;
            $suscriptionst->save();

            $cs = CourseSuscription::model()->findByAttributes(array('id_user' => $_GET['id'], 'id_course' => $_GET['id_course']));
            if ($cs) {
                $cs->active = 1;
                $cs->update();
            } else {
                $cs = new CourseSuscription;
                $cs->id_user = $_GET['id'];
                $cs->id_course = $_GET['id_course'];
                $cs->active = 1;
                $cs->save();
            }

            $this->redirect(Yii::app()->urlManager->createUrl('admin/estudiante', array('id' => $_GET['id'])));
        }
    }

    public function actionAgregarDuplicado() {
        if (isset($_GET['email']) && isset($_GET['id'])) {
            $user = User::model()->findByPk($_GET['id']);
            $userDuplicado = User::model()->findByAttributes(array('email1' => $_GET['email']));

            $transaction = Yii::app()->db->beginTransaction();
            try {
                if ($userDuplicado) {
                    $userDuplicado->delete();

                    $user->email2 = $_GET['email'];
                    $user->update();
                }

                $transaction->commit();
            } catch (Exception $e) {
                $transaction->rollback();
                Yii::log($e->getMessage(), CLogger::LEVEL_ERROR);
            }
            $this->redirect(Yii::app()->urlManager->createUrl('admin/estudiante', array('id' => $_GET['id'])));
        }
    }

    public function actionCuentaDuplicada() {
        if (isset($_GET['email']) && isset($_GET['id'])) {
            $mensaje = "1";
            $user = User::model()->findByPk($_GET['id']);
            $userDuplicado = User::model()->findByAttributes(array('email1' => $_GET['email']));

            if ($userDuplicado) {
                $mensaje.="2";
                $transaction = Yii::app()->db->beginTransaction();
                try {
                    $mensaje.="3";
                    SuscriptionStatus::model()->deleteAll("id_suscription in (select id_suscription from suscription where id_user in (select id_user from user where email1 like '%" . $userDuplicado->email1 . "%'))");
                    Pay::model()->deleteAll("id_suscription in (select id_suscription from suscription where id_user in (select id_user from user where email1 like '%" . $userDuplicado->email1 . "%'))");
                    Suscription::model()->deleteAll("id_user in (select id_user from user where email1 like '%" . $userDuplicado->email1 . "%')");
                    Happen::model()->deleteAll("id_user in (select id_user from user where email1 like '%" . $userDuplicado->email1 . "%')");
                    Search::model()->deleteAll("id_user in (select id_user from user where email1 like '%" . $userDuplicado->email1 . "%')");

                    $mensaje.="4";
                    CourseSuscription::model()->updateAll(array('id_user' => $user->id_user), "id_user in (select id_user from user where email1 like '%" . $userDuplicado->email1 . "%')");
                    Link::model()->updateAll(array('id_user' => $user->id_user), "id_user in (select id_user from user where email1 like '%" . $userDuplicado->email1 . "%')");
                    GroupSuscription::model()->updateAll(array('id_user' => $user->id_user), "id_user in (select id_user from user where email1 like '%" . $userDuplicado->email1 . "%')");

                    $user->email2 = $_GET['email'];
                    $user->update();

                    $mensaje.="5";
                    User::model()->deleteAll("email1 like '%" . $userDuplicado->email1 . "%'");

                    $mensaje.="6";
                    $transaction->commit();
                    $this->redirect(Yii::app()->urlManager->createUrl('admin/estudiante', array('id' => $_GET['id'], 'mensaje' => 'OK')));
                } catch (Exception $e) {
                    $mensaje.="ERROR";
                    $transaction->rollback();
                    Yii::log($e->getMessage(), CLogger::LEVEL_ERROR);
                }
            }
            $this->redirect(Yii::app()->urlManager->createUrl('admin/estudiante', array('id' => $_GET['id'], 'mensaje' => $mensaje)));
        }
    }

    public function actionAgregarAmigo() {
        if (isset($_GET['email']) && isset($_GET['id'])) {
            $user = User::model()->findByPk($_GET['id']);
            $userAmigo = User::model()->findByAttributes(array('email1' => $_GET['email']));
            $transaction = Yii::app()->db->beginTransaction();

            try {
                if (!$userAmigo) {
                    $userAmigo = new User;
                    $userAmigo->name = $_GET['email'];
                    $userAmigo->email1 = $_GET['email'];
                    $utype = UserType::model()->findByAttributes(array('na_utype' => 'Estudiante'));
                    $userAmigo->id_utype = $utype->id_utype;
                    $userAmigo->user = $_GET['email'];
                    ;
                    $userAmigo->create = new CDbExpression('NOW()');
                    $userAmigo->pass = "";
                    $userAmigo->save(false);
                }
                $user->id_refuser = $userAmigo->id_user;
                $user->update();

                $transaction->commit();
            } catch (Exception $e) {
                $transaction->rollback();
                Yii::log($e->getMessage(), CLogger::LEVEL_ERROR);
            }
            $this->redirect(Yii::app()->urlManager->createUrl('admin/estudiante', array('id' => $_GET['id'])));
        }
    }

////////////FUNCIONES
    public function actionGenerarCertificado() {
        if (isset($_GET['idcs']) && isset($_GET['guardar']) && isset($_GET['id'])) {
            $meses = array("Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre");

            $user = User::model()->findByPk($_GET['id']);
            $cs = CourseSuscription::model()->findByPk($_GET['idcs']);
            $c = Course::model()->findByPk($cs->id_course);
            $fecha = date("d") . ' de ' . $meses[date('m') - 1] . ' de ' . date("Y");
            $numero = date("dGmiY");

            //$pdf = new FPDF('L', 'mm', 'Letter');
            $pdf = new tFPDF('L', 'mm', 'Letter');
            $pdf->AddPage();
            $pdf->AddFont('EdwardianScriptITC', '', 'edwardian_script_itc.ttf', true);
            //$pdf->AddFont('EdwardianScriptITC','','edwardian_script_itc.php');
            $pdf->Image(Yii::app()->basePath . '/../images/certificado.jpg', 0, 0, 279.4, 215.9);
            $pdf->Cell(260, 82);
            $pdf->Ln();
            $pdf->SetTextColor(41, 89, 134);
            $pdf->SetFont('EdwardianScriptITC', '', 40);
            $pdf->Cell(260, 10, $user->name . ' ' . $user->lastname, 0, 0, 'C');
            $pdf->Ln();
            $pdf->Cell(260, 22);
            $pdf->Ln();
            $pdf->SetTextColor(0, 0, 0);
            $tamano = 28;
            if (strlen($c->name) > 80)
                $tamano = 16;
            $pdf->SetFont('EdwardianScriptITC', '', $tamano);
            $pdf->Cell(260, 10, $c->name, 0, 0, 'C');
            $pdf->Ln();
            $pdf->Cell(260, 0);
            $pdf->Ln();
            $pdf->Cell(148);
            $pdf->SetTextColor(134, 134, 135);
            $pdf->SetFont('EdwardianScriptITC', '', 22);
            $pdf->Cell(108, 7, $fecha);
            $pdf->Ln();
            $pdf->Cell(260, 24);
            $pdf->Ln();
            $pdf->SetTextColor(41, 89, 134);
            $pdf->SetFont('Arial', 'I', 16);
            $pdf->Cell(165, 10, date("d-m-Y"), 0, 0, 'C');
            $pdf->Ln();
            $pdf->Cell(260, 12);
            $pdf->Ln();
            $pdf->SetTextColor(0, 0, 0);
            $pdf->SetFont('Arial', '', 12);
            $pdf->Cell(272, 8, $numero, 0, 0, 'C');

            if ($_GET['guardar'] == "1") {
                $cs->certificate = 'Certificado' . $_GET['idcs'] . '.pdf';
                $cs->update();
                $pdf->Output("certificados/Certificado" . $_GET['idcs'] . ".pdf", "F");

                $this->redirect(Yii::app()->urlManager->createUrl('admin/estudiante', array('id' => $_GET['id'])));
            } else {
                $pdf->Output();
            }
        }
    }

    private function create_zip($files = array(), $destination = '', $overwrite = false) {
        //if the zip file already exists and overwrite is false, return false
        if (file_exists($destination) && !$overwrite) {
            return false;
        }
        //vars
        $valid_files = array();
        //if files were passed in...
        if (is_array($files)) {
            //cycle through each file
            foreach ($files as $file) {
                //make sure the file exists
                if (file_exists($file)) {
                    $valid_files[] = $file;
                }
            }
        }
        //if we have good files...
        if (count($valid_files)) {
            //create the archive
            $zip = new ZipArchive();
            if ($zip->open($destination, $overwrite ? ZIPARCHIVE::OVERWRITE : ZIPARCHIVE::CREATE) !== true) {
                return false;
            }
            //add the files
            foreach ($valid_files as $file) {
                $rutas = explode("/", $file);
                $zip->addFile($file, end($rutas));
            }
            //debug
            //echo 'The zip archive contains ',$zip->numFiles,' files with a status of ',$zip->status;
            //close the zip -- done!
            $zip->close();

            //check to make sure the file exists
            return file_exists($destination);
        } else {
            return false;
        }
    }

    public function actionCampanas() {
        $model = new CmpCampaign('search');
        $model->unsetAttributes();

        if (isset($_GET['CmpCampaign']))
            $model->attributes = $_GET['CmpCampaign'];

        $this->render('campanas', array(
            'model' => $model,
        ));
    }

    public function actionCrearCampana() {
        $model = new CmpCampaign;
        $answers = CmpAnswer::model()->findAll();

        //Crear al menos una campaña vacía
        $model->qstnArray = array();
        $model->qstnArray[] = new CmpQuestion;

        if (isset($_POST['CmpCampaign'])) {
            $model->attributes = $_POST['CmpCampaign'];

            $transaction = Yii::app()->db->beginTransaction();

            if ($model->save()) {
                if ($model->saveSubObjectsFromPost()) {
                    $transaction->commit();
                    $this->redirect(array(
                        'campanas', 'msg' => self::TXT_CAMPAIGN_CREATED
                    ));
                } else {
                    $transaction->rollback();
                }
            } else {
                $transaction->rollback();
            }
        }

        $this->render('formCampaign', array(
            'model' => $model,
            'answers' => $answers
        ));
    }

    public function actionEditarCampana($id) {
        $model = CmpCampaign::model()
                ->with('basicCourse', 'mediumCourse', 'advancedCourse', 'questions', 'questions.answers')
                ->findByPk($id);

        //Agrega una pregunta vacía para usarla en la plantilla
        $tmpQstn = new CmpQuestion();
        if (isset($_POST['CmpCampaign'])) {
            $model->qstnArray = array($tmpQstn);
        } else {
            $model->qstnArray = array_merge(array($tmpQstn), $model->questions);
        }

        for ($i = 0; $i < count($model->qstnArray); $i++) {
            foreach ($model->qstnArray[$i]->answers as $answer)
                $model->qstnArray[$i]->answersArray[] = $answer->id_answer;
        }

        $answers = CmpAnswer::model()->findAll();

        if (isset($_POST['CmpCampaign'])) {
            $model->attributes = $_POST['CmpCampaign'];

            $transaction = Yii::app()->db->beginTransaction();

            if ($model->save()) {
                if ($model->saveSubObjectsFromPost()) {
                    $transaction->commit();
                    $this->redirect(array(
                        'campanas', 'msg' => self::TXT_CAMPAIGN_MODIFIED
                    ));
                } else {
                    $transaction->rollback();
                }
            } else {
                $transaction->rollback();
            }
        }

        $this->render('formCampaign', array(
            'model' => $model,
            'answers' => $answers
        ));
    }

    public function actionEliminarCampana($id) {
        if (Yii::app()->request->isAjaxRequest) {

            try {
                $transaction = Yii::app()->db->beginTransaction();
                CmpCampaign::model()->findByPk($id)->delete();
                $transaction->commit();
            } catch (Exception $e) {
                $transaction->rollback();
                throw new CHttpException(500, 'Problema al borrar la campaña: ' . $e->getMessage());
            }

            if (!isset($_GET['ajax']))
                $this->redirect(array('site/tuscursos'));
        } else {
            $this->redirect(Yii::app()->urlManager->createUrl('/'));
            return false;
        }
    }

    public function actionObtenerCursos() {
        $term = isset($_GET['term']) ? strtoupper($_GET['term']) . '%' : NULL;

        $courses = Course::model()->findAll(array(
            'select' => 'id_course, name',
            'condition' => 'name LIKE :term',
            'params' => array(':term' => $term)
        ));

        echo CJSON::encode(CHtml::listData($courses, 'id_course', 'name'));
        die;
    }

}
