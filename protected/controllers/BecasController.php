<?php

class BecasController extends Controller
{
	public $userEmail;
	public $defaultAction = 'index';

	public function actions()
	{
	        return array(
	            'page'=>array(
	                'class'=>'CViewAction',
	            ),
	        );
	}

	public function filters()
	{
		return array(
			'accessControl',
		);
	}

	public function accessRules()
	{
	    return array(
			array('allow',
			  	'actions'=>array('referido','guardarReferido'),
			  	'users'=>array('*'),
			),
			array('allow',
			  	'actions'=>array('index', 'sendInvitations'),
			  	'users'=>array('@'),
			),
			array('deny',
			  	'users'=>array('*'),
			),
	    );
	}

	/**
	 * Genera el link a ser usado 
	 * @param  [type] $source [description]
	 * @return [type]         [description]
	 */
	protected function getShareLink($source)
	{
		$utm_medium = 'global';
		switch ($source) {
			case 'twitter':
				$utm_medium = 'twitter';
				break;
			case 'facebook':
				$utm_medium = 'Loop-FB';
				break;
		}

 		$utms = "utm_source=Becas&utm_medium={$utm_medium}&utm_term=Invitado&utm_content=Correo-de-confirmacion&utm_campaign=Invitacion";
		$email = Yii::app()->user->email;

		return Yii::app()->createAbsoluteUrl('/becas/referido/' . $email . '?' . $utms);
	}

	protected function hasCommonEmail()
	{
		return false;
		$emailProviders = array(
			'gmail',
			'hotmail',
			'outlook',
			'yahoo'
		);
		$domainEmail = explode('.',array_pop(explode('@', Yii::app()->user->email)));

		return  in_array(strtolower($domainEmail[0]), $emailProviders);
	}

	/**
	 * Funcion para validar si un correo debe recibir o no la invitación
	 *
  	 * @param string  $email		correo a validar
	 * @param string  $primaryEmail 	principal correo del usuario
	 * @param string  $alternativeEmail	correo alternativo del usuario
	 * @return boolean			Indica si es válido o no el correo
	 */
	protected function validatePontentialSuscriptorEmail($email, $primaryEmail, $alternativeEmail)
	{
		if(
			filter_var($email, FILTER_VALIDATE_EMAIL) 	&&
			$email !== $primaryEmail 			&&
			$email !== $alternativeEmail
		){
			//Consulta que no exista un suscriptor activo con ese correo
			$criteria = new CDbCriteria();
			$criteria->addCondition('email1=:email1 OR email2=:email2');
			$criteria->addCondition('s.status=1');
			$criteria->join = 'INNER JOIN ' . Suscription::tableName() . ' s ON t.id_user=s.id_user ';
			$criteria->params = array(':email1'=>$email, ':email2'=>$email);

			return !$user = User::model()->exists($criteria);
		}

		return false;
	}

	/**
	 * Esta función se encarga de eliminar los correos de personas que ya están suscritas en el servicio
	 * pues no deben recibir correo de la promoción
	 *
	 * @param Array   $emails                           arreglo con todos los correo
	 * @param string  $primaryEmail              principal correo del usuario
	 * @param string  $alternativeEmail	correo alternativo del usuario
	 * @return Array 			arreglo con los correos de las personas no registradas
	 */
	protected function getUnRegisteredEmails($emails, $primaryEmail, $alternativeEmail)
	{
		$unregisteredEmails = array();

		foreach ($emails as $email){
			if($this->validatePontentialSuscriptorEmail($email, $primaryEmail,$alternativeEmail)){
				$unregisteredEmails[] = $email;
			}
		}
		return $unregisteredEmails;
	}

	public function actionSendInvitations()
	{
		$response = array();

		if(Yii::app()->request->isAjaxRequest){
			if(isset($_POST['emails']) && isset($_POST['link']))
			{
				if(empty($_POST['emails'])){
					$response['msg'] = 'Faltan las direcciones de correo a enviar las invitaciones.';
					$response['error'] = true;
					echo json_encode($response);
					exit();
				}


				$emails = sprintf("%s", $_POST['emails']);  //Avoids XSS
				$shareLink  = sprintf("%s", $_POST['link']);
				$emailsArray = preg_split('/ |,/', $emails);
				$user = User::model()->findByPk( Yii::app()->user->getId() );
				$unregisteredEmails = $this->getUnRegisteredEmails($emailsArray, $user->email1, $user->email2);

				if(count($unregisteredEmails)){
					$qnty = Mail::sendScholarship($unregisteredEmails, $user->fullname, $shareLink);
					if($qnty > 0) {
						Happens::envioInvitaciones($user->email1, $qnty);
						$response['msg'] = 'Las invitaciones se enviaron correctamente';
						$response['error'] = false;
					}
					else{
						$response['msg'] = 'Ocurrió un problema al enviar el correo, por favor, intenta mas tarde.';
						$response['error'] = true;
					}
				}
				else{
					$response['msg'] = 'No se enviaron invitaciones, ya los usuarios poseen suscripciones o no hay direcciones válidas';
					$response['error'] = true;
				}
			} else {
				$response['msg'] = 'Falta escribir las direcciones de correo.';
				$response['error'] = true;
			}
			echo json_encode($response);
		} else {
			throw new CHttpException(400,'Petición inválida');
		}
	}

	public function actionIndex()
	{
		$fbAppId =  YII_DEBUG ? Yii::app()->params['fbDevAppId'] : Yii::app()->params['fbAppId'];
		$user = User::model()->findByPk(Yii::app()->user->getId());
		$totalReferrals = $user->payedReferrals;
		$errorMessage = isset($_GET['errorMessage']) ? $_GET['errorMessage'] : NULL;

		$this->render('index', array(
			'fbAppId'  => $fbAppId,
			'totalReferrals'  => $totalReferrals,
			'errorMessage'  => $errorMessage
		));
	}

	public function actionGuardarReferido()
	{
		if(Yii::app()->request->isAjaxRequest){
			if(!Yii::app()->user->isGuest && isset($_COOKIE['referral'])){
				$securityManager = new CSecurityManager;
				$cryptedEmail = $securityManager->decrypt( $_COOKIE['referral'] , Yii::app()->params['cookieCryptKey']);
				$user = User::model() ->updateByPk(Yii::app()->user->getId(), array('invitation_from'=>$cryptedEmail));
			}
		} else {
			throw new CHttpException(400,'Petición inválida');
		}
	}

	public function actionReferido()
	{
		
		if(isset($_GET['email'])){
			$referrerEmail = filter_var($_GET['email'], FILTER_SANITIZE_EMAIL);

			$referredUser = User::model()->find('email1=:email1 OR email2=:email2', array(
				'email1' => $referrerEmail,
				'email2' => $referrerEmail
			));

			$isNotCurrentEmail = Yii::app()->user->isGuest ? true : ($referrerEmail !== Yii::app()->user->email);

			if($referredUser && $isNotCurrentEmail){
				if(Yii::app()->user->isGuest){
					$hasActiveSubscription = false;
				} else {
					//Consulta que no tenga una suscripción activa con ese correo
					$user = User::model()->findByPk(Yii::app()->user->getId());
					$criteria = new CDbCriteria();
					$criteria->addCondition('t.id_user=:id_user');
					$criteria->addCondition('s.status=1');
					$criteria->join = 'INNER JOIN ' . Suscription::tableName() . ' s ON t.id_user=s.id_user ';
					$criteria->params = array(':id_user'=>$user->id_user);

					$hasActiveSubscription = User::model()->exists($criteria);
				}

				if(!$hasActiveSubscription){
					$securityManager = new CSecurityManager;
					$cryptedEmail = $securityManager->encrypt($referredUser->email1, Yii::app()->params['cookieCryptKey']);

					Yii::app()->request->cookies['referral'] = new CHttpCookie('referral', $cryptedEmail, array(
						'expire'=>0,
						'httpOnly'=>false
					));

					OjalaUtils::createCookieUtms();
					$this->render('referido', array('user' => $referredUser ));
				} else {
					$this->redirect(array(
						'becas/index',
						'errorMessage'=>'Disculpe, por los momentos no puedes participar por una Beca'
					));
				}
			} else  {
				$this->redirect(array(
					'becas/index',
					'errorMessage'=>'Correo de invitación inválido'
				));
			}
		}  else {
			$this->redirect(array(
				'becas/index',
				'errorMessage'=>'Enlace de invitación inválido'
			));
		}
	}
}