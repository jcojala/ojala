<?php

class SiteController extends Controller {

///////////////FILTROS Y REGLAS DE ACCESO
    public function actions() {
        return array(
            'page' => array(
                'class' => 'CViewAction',
            ),
        );
    }

    public function filters() {
        return array(
            'accessControl',
        );
    }

    public function accessRules() {
        return array(
            array('deny',
                'actions' => array('perfilSuscripcion', 'perfilCompras', 'perfilFacturacion', 'perfilEditar',
                    'perfilCursos'),
                'users' => array('*'),
                'expression' => "isset(Yii::app()->user->utype) && (Yii::app()->user->utype==='Becado')",
            ),
            array('allow',
                'actions' => array('error', 'index', 'login', 'logout', 'registro',
                    'recuperarclave', 'suscripciones', 'cursos', 'ideas', 'enviarCorreo',
                    'privacidad', 'terminos', 'ayuda', 'cambiarClave', 'curso', '404', 'diplomados',
                    'confirmacion', 'pagoSuscripcion', 'blog', 'hookPaypal', 'hookConekta', 'hookStripe',
                    'Cancel', 'CompraPaypal', 'AplicarCupon', 'Portada', 'clase', 'LoginLMS', 'SugerenciaCurso',
                    'ValorPlan', 'pagoCurso', 'procesar', 'CrearUsuario', 'guardarTiempo', 'RedireccionLanding',
                    'HookLanding', 'HookPaypal', 'courseInfo', 'Confirm', 'ConfirmPaypal', 'diplomado', 'rt',
                    'unbounce', 'ProximosCursos', 'propuesta', 'EstoyInteresado',
                    'guardarSugerencia', 'masCursos', 'InformacionInteresado'),
                'users' => array('*'),
            ),
            array('allow',
                'actions' => array('error', 'index', 'login', 'logout', 'perfilCursos',
                    'PerfilEditar', 'tuscursos', 'tusdiplomados', 'clase', 'ayuda', 'perfilFacturacion',
                    'pagoCurso', 'perfilSuscripcion', 'perfilCompras', 'Exito', 'Portada',
                    'cambiarClave', 'guardarTiempo', 'descargarArchivo', 'AsignarCurso',
                    'CancelarSuscripcion', 'diplomado', 'finCurso', 'cursoDesbloqueado', 'CompraPaypal',
                    'ConfirmPaypal', 'upload', 'cambiarPlan',
                    'DesbloquearCurso', 'ValorPlan', 'enlaces', 'foro', 'EliminarCurso', 'SugerenciaCurso',
                    'SolicitarCertificado', 'enviarCorreo', 'Confirm', 'invoice', 'hasActiveSubscription',
                    'confirmacionAsesor'),
                'users' => array('@'),
            ),
            array('deny',
                'users' => array('*'),
            ),
        );
    }

    /*     * *********************** */

    /*
     * Función que permite guardar los usuarios que están interesados en más 
     * cursos de esa misma categoría.
     */

    public function actionMasCursos() {

        $idcourse = $_GET["id"];
        $course = Course::model()->findByPk($idcourse);
        $cathegory = Cathegory::model()->findByPk($course->id_cathegory);
        $nombreCategoria = strtolower(str_replace(' ', '-', $cathegory->na_cathegory));
        $link = new LinkProposal;
        $link->id_course = $course->id_course;
        $link->id_ltype = 3; //Más cursos
        if (Yii::app()->user->isGuest) {
            $link->id_user = null;
        } else {
            $link->id_user = Yii::app()->user->id;
        }
        $link->save();

        $this->redirect(Yii::app()->urlManager->createUrl('site/cursos', array('cat' => $nombreCategoria)));
    }

    /*
     * Función que permite obtener los datos de contacto de la persona
     * (que ún no es usuario) para informarle cuando esté disponible el curso.
     * También envía el correo a traves de Mandrill.
     */

    public function actionInformacionInteresado($n) {

        $modelUser = new User;
        $modelAdvisor = new AdvisorForm();

        $course = Course::model()->findByPk($n);

        if (isset($_POST['AdvisorForm'])) {

//Trae nombre y correo electrónico
            $modelUser->name = $_POST['nombreInteresado'];
            $modelUser->email1 = $_POST['emailInteresado'];
//Trae región y teléfono
            $modelAdvisor->attributes = $_POST['AdvisorForm'];

            $region = Region::model()->with('country')->find('id_region=:id_region', array(
                ':id_region' => $modelAdvisor->region
            ));

            try {
                $template_name = 'mail-interesado';
                $reason = 'interesado en el próximo curso';
                $subject = 'El usuario está interesado en un próximo curso';
                $regionCode = empty($region->area_code) && $region->area_code != 0 ? '' : '(' . $region->area_code . ')';
                $regionInfo = $region->name . " " . $regionCode;
                $phone = $modelAdvisor->phoneNumber . " (" . $modelAdvisor->phoneType . ")";
                $country = $region->country->name . " (+" . $region->country->phone_code . ")";

                foreach (Yii::app()->params['advisorEmails'] as $advisorEmail) {
                    $message = array(
                        'subject' => $subject,
                        'global_merge_vars' => array(
                            array('name' => 'REASON', 'content' => $reason),
                            array('name' => 'NAME', 'content' => $modelUser->name),
                            array('name' => 'EMAIL', 'content' => $modelUser->email1),
                            array('name' => 'PHONE', 'content' => $phone),
//                            array('name' => 'PHONETYPE', 'content' => $modelAdvisor->phoneType),
                            array('name' => 'REGION', 'content' => $regionInfo),
//                            array('name' => 'REGION_CODE', 'content' => $regionCode),
                            array('name' => 'COUNTRY', 'content' => $country),
//                            array('name' => 'COUNTRY_CODE', 'content' => $region->country->phone_code),
                            array('name' => 'COURSE', 'content' => $course->name)
                        ),
                        'to' => array(
                            array(
                                'email' => $advisorEmail,
                                'name' => 'Asesor de Oja.la',
                                'type' => 'to'
                            )
                        )
                    );

                    $mandrill = new MandrillWrapper();
                    $mandrill->messages->sendTemplate($template_name, NULL, $message, true);
                }

//Debo enviarlo a la lista de cursos
                $this->redirect(Yii::app()->UrlManager->createUrl('site/cursos'));
            } catch (Mandrill_Error $e) {
                Yii::log('Mandrill error: ' . $e->getMessage(), CLogger::LEVEL_ERROR);
            }
        }

        $this->render('interesadoProxCurso', array(
            'modelUser' => $modelUser,
            'modelAdvisor' => $modelAdvisor
        ));
    }

    /*
     * Funcion que guarda en la tabla link_proposal sobre la 
     * persona interesada en el curso. También envia un correo
     * a los advisors.
     */

    public function actionEstoyInteresado() {

        $idcourse = $_GET["id"];

        $course = Course::model()->findByPk($idcourse);

        if (Yii::app()->user->isGuest) {

            $link = new LinkProposal;
            $link->id_user = null;
            $link->id_course = $course->id_course;
            $link->id_ltype = 2; //Más información
            $link->save();

            $this->redirect(Yii::app()->urlManager->createUrl('site/informacionInteresado', array('n' => $course->id_course)));
        } else {

            $resultado = LinkProposal::model()->find(array(
                'condition' => 'id_course=:courseid AND id_user=:userid',
                'params' => array(':courseid' => $course->id_course, ':userid' => Yii::app()->user->id),
            ));

//            $resultado = LinkProposal::model()->find(array('id_course' => $course->id_course, 'id_user' => Yii::app()->user->id));

            if ($resultado == null) {

                $link = new LinkProposal;
                $link->id_user = Yii::app()->user->id;
                $link->id_course = $course->id_course;
                $link->id_ltype = 2; //Más información
                $link->save();

                $user = User::model()->findByPk(Yii::app()->user->id);

                $regionData = Region::model()->with('country')->find('id_region=:id_region', array(
                    ':id_region' => $user->id_region
                ));

                try {

                    $template_name = 'mail-interesado';
                    $reason = 'está interesado en el próximo curso';
                    $subject = 'El usuario está interesado en un próximo curso';
                    $region_code = "";

                    if ($user->id_region != null) {

                        $region_code = empty($regionData->area_code) && $regionData->area_code != 0 ? '' : '(' . $regionData->area_code . ')';
                    }

                    foreach (Yii::app()->params['advisorEmails'] as $advisorEmail) {

                        $phone = "No especificado";
                        $phone_type = "";
                        $region = "No especificado";
                        $country = "No especificado";
                        $country_code = "";

                        $message = array();

                        if ($user->phone != null) {
                            $phone = $user->phone;
                        }

                        if ($user->id_region != null) {

                            $region = $regionData->name . " " . $region_code;
                            $country = $region->country->name . " (+" . $region->country->phone_code . ")";
                        }

                        $message = array(
                            'subject' => $subject,
                            'global_merge_vars' => array(
                                array('name' => 'REASON', 'content' => $reason),
                                array('name' => 'NAME', 'content' => $user->name),
                                array('name' => 'EMAIL', 'content' => $user->email1),
                                array('name' => 'PHONE', 'content' => $phone),
//                            array('name' => 'PHONETYPE', 'content' => $phone_type),
                                array('name' => 'REGION', 'content' => $region),
//                            array('name' => 'REGION_CODE', 'content' => $region_code),
                                array('name' => 'COUNTRY', 'content' => $country),
//                            array('name' => 'COUNTRY_CODE', 'content' => $country_code),
                                array('name' => 'COURSE', 'content' => $course->name)
                            ),
                            'to' => array(
                                array(
                                    'email' => $advisorEmail,
                                    'name' => 'Asesor de Oja.la',
                                    'type' => 'to'
                                )
                            )
                        );

                        $mandrill = new MandrillWrapper();
                        $mandrill->messages->sendTemplate($template_name, NULL, $message, true);
                    }
                } catch (Mandrill_Error $e) {
                    Yii::log('Mandrill error: ' . $e->getMessage(), CLogger::LEVEL_ERROR);
                }
            }

            $this->redirect(Yii::app()->urlManager->createUrl('site/cursos'));
        }
    }

    /*
     * Función que muestra la portada de una propuesta
     */

    public function actionPropuesta($slug) {

        $acceso = 0;

        $course = Course::model()->findByAttributes(array('slug' => $slug));

        if ($course) {

            $cathegory = Cathegory::model()->findByPk($course->id_cathegory);
            $level = Level::model()->findByPk($course->id_level);

//Creando del link de visita!
//Sólo si no se ha creado antes
            $resultado = Link::model()->find(array(
                'condition' => 'id_course=:courseid AND id_user=:userid AND id_ltype=:ltypeid',
                'params' => array(':courseid' => $course->id_course, ':userid' => Yii::app()->user->id, ':ltypeid' => 1),
            ));

            if ($resultado == null) {

                $link = new Link;
                if (!Yii::app()->user->isGuest) {
                    $link->id_user = Yii::app()->user->id;
                } else {
                    $link->id_user = null;
                }
                $link->id_course = $course->id_course;
                $link->id_ltype = 1; //Me interesa
                $link->save();
            }

//Consultando la lista de clases y lecciones
            $criteria = new CDbCriteria;
            $criteria->with = array(
                'topics'
            );
            $criteria->addCondition('t.id_course=:courseid');
            $criteria->params = array('courseid' => $course->id_course);

            $lessons = Lesson::model()->findAll($criteria);

//$this->render('curso', array('portadaPropuesta' => $course, 'level' => $level, 'cathegory' => $cathegory, 'cs' => null, 'last' => null, 'list' => array()));
            $this->render('portadaPropuesta', array('model' => new User(), 'course' => $course, 'level' => $level, 'cathegory' => $cathegory, 'current' => array(), 'list' => $lessons, 'listNew' => array()));
        } else {

            $this->redirect(Yii::app()->urlManager->createUrl('site/proximoscursos'));
        }
    }

    /*
     * Listado de los próximos cursos que se tendrán disponibles en Oja.la
     * Ordena por categoría.
     */

    public function actionProximosCursos() {

        $ipp = 16;

        $filtroCategoria = "";
        if (isset($_GET['cat'])) {
            $filtroCategoria = Yii::app()->request->getParam('cat');
        }
        $filtroBuscar = "";
        if (isset($_GET['buscar'])) {
            $filtroBuscar = Yii::app()->request->getParam('buscar');
        }
        $filtroOrden = "";
        if (isset($_GET['orden'])) {
            $filtroOrden = Yii::app()->request->getParam('orden');
        }

        $criteria = new CDbCriteria;
        $criteria->with = array(
            'category' => array('select' => 'na_cathegory'),
            'level' => array('select' => 'na_level'),
            'courseTags',
            'links'
        );

        if ($filtroCategoria != "") {
            $criteria->addCondition("category.na_cathegory LIKE '" . $filtroCategoria . "'");
        }

        if ($filtroBuscar != "") {
            $busqueda = new Searchh;
            if (!Yii::app()->user->isGuest) {
                $busqueda->id_user = Yii::app()->user->id;
            }
            $busqueda->text = $filtroBuscar;
            if ($busqueda->validate()) {
                $busqueda->save();
            }
            $criteria->addCondition("t.name LIKE '%" . $filtroBuscar . "%'");
        }

        $criteria->addCondition("t.active = 1");

        if ($filtroOrden != "") {
            if ($filtroOrden == "alfabetico") {
                $criteria->order = 't.name DESC';
            } else if ($filtroOrden == "dificultad") {
                $criteria->order = 't.id_level DESC';
            } else {
                $criteria->order = 't.create DESC';
            }
        } else {
            $criteria->order = 't.create DESC';
        }

        $listCat = Cathegory::model()->findAll();

        $criteria->addCondition("id_state = 5");

        $list = Course::model()->findAll($criteria);

        $dataProvider = new CActiveDataProvider('Course', array(
            'pagination' => array(
                'pageSize' => 18,
            ),
            'criteria' => $criteria
        ));

        $this->render('propuestas', array('categorias' => $listCat, 'list' => $list, 'categoria' => $filtroCategoria, 'buscar' => $filtroBuscar, 'dataProvider' => $dataProvider));
    }

    /*     * *********************** */

///////////////MENU PRINCIPAL
    private function stripslashes_deep($value) {
        $value = is_array($value) ?
                array_map('stripslashes_deep', $value) :
                stripslashes($value);

        return $value;
    }

    public function actionUnbounce() {
//$response = @file_get_contents('php://input');	//Obtención de solicitud HTTP desde Stripe
//$event_json = json_decode($body, true); 	//Transformación a objeto PHP

        /* libxml_use_internal_errors(true);
          $form_data = simplexml_load_string($response); */

        if (get_magic_quotes_gpc()) {
            $unescaped_post_data = $this->stripslashes_deep($_POST);
        } else {
            $unescaped_post_data = $_POST;
        }
        $form_data = json_decode($unescaped_post_data['data_json']);

// If your form data has an 'Email Address' field, here's how you extract it:
        /* $email_address = $form_data->email_address[0];

          // Grab the remaining page data...
          $page_id = $_POST['page_id'];
          $page_url = $_POST['page_url'];
          $variant = $_POST['variant'];

          // Assemble the body of the email...
          $message_body = @"Email: $email_address \n
          Page ID: $page_id \n
          URL: $page_url \n
          Variant: $variant \n"; */

        Yii::log('Unbounce: ' . var_export($form_data, TRUE), CLogger::LEVEL_ERROR);
    }

    public function actionLoginLMS() {
        if (Yii::app()->user->isGuest) {
            OjalaUtils::sendSlack("Entrada por iFrame", Yii::app()->request->urlReferrer . ' ' . Yii::app()->request->getUserHostAddress(), "#general", "iframe", "good");
            Yii::app()->request->cookies['becado'] = new CHttpCookie('becado', Yii::app()->request->urlReferrer);

            if (strpos(Yii::app()->request->urlReferrer, 'schoology.com') !== false || strpos(Yii::app()->request->urlReferrer, 'skatox.com') !== false || strpos(Yii::app()->request->urlReferrer, 'politecnicoindoamericano.edu.co') !== false) {
                $user = User::model()->findByAttributes(array('email1' => 'pmtecnologia@politecnicoindoamericano.edu.co'));

                $identity = new UserIdentity($user->email1, '', 1);
                $identity->authenticate();
                Yii::app()->user->login($identity);

                $this->redirect(Yii::app()->urlManager->createUrl('site/cursos'));
            }

            $model = new LoginForm;

// if it is ajax validation request
            if (isset($_POST['ajax']) && $_POST['ajax'] === 'login-form') {
                echo CActiveForm::validate($model);
                Yii::app()->end();
            }

// collect user input data
            if (isset($_POST['LoginForm'])) {
                $model->attributes = $_POST['LoginForm'];
// validate user input and redirect to the previous page if valid
                if ($model->validate() && $model->login()) {
                    $user = User::model()->findByPk(Yii::app()->user->id);
                    $connection = Yii::app()->db;
                    $command = $connection->createCommand();
                    $command->setSelect("s.id_suscription");
                    $command->setFrom(Suscription::model()->tableName() . ' s');
                    $command->join(SuscriptionStatus::model()->tableName() . ' ss', 'ss.id_suscription=s.id_suscription');
                    $command->setWhere("s.active=1 AND ss.active=1 AND ss.id_status=1 AND s.id_user=" . Yii::app()->user->id);
                    $suscription = $command->queryRow();
                    $tipo = "Lead";

                    if ($suscription)
                        $tipo = "Usuario";

                    Happens::inicioSesion($user->email1);
                    OjalaUtils::enviarIntercom(Yii::app()->user->id, $user->email1, $user->name, array("LAST_NAME" => $user->lastname), false, "https://oja.la/login");

                    $this->redirect(Yii::app()->user->returnUrl);
                }
            }
            $this->render('login', array('model' => $model));
        }
        else {
            $this->redirect(Yii::app()->urlManager->createUrl('site/tuscursos'));
        }
    }

    public function actionIndex() {
        if (Yii::app()->user->isGuest) {
            $this->layout = "home";

            unset(Yii::app()->session['facebook']);

            $model = new User;
            $list = Course::model()->with('category')->published()->findAll(array(
                'select' => 't.id_course, t.name, t.cover, t.slug, category.slug',
                'limit' => 4,
                'order' => 'RAND()'
            ));

            $this->render('index', array('model' => $model, 'list' => $list));
        } else {
            $this->redirect(Yii::app()->urlManager->createUrl('site/tuscursos'));
        }
    }

    public function actionAyuda() {
        $this->render('ayuda');
    }

    public function actionCursos() {
//Items por pagina
        $ipp = 18;

        $filtroCat = "";
        if (isset($_GET['cat']))
            $filtroCat = " AND ca.slug ='" . $_GET['cat'] . "'";

        $filtroBus = "";
        if (isset($_GET['buscar'])) {
            $busqueda = new Searchh;
            if (!Yii::app()->user->isGuest)
                $busqueda->id_user = Yii::app()->user->id;
            $busqueda->text = $_GET['buscar'];
            if ($busqueda->validate()) {
                $busqueda->save();
            }
            $filtroBus = " AND c.name LIKE '%" . $_GET['buscar'] . "%'";
        }

//Cuento total de cursos
        $connection = Yii::app()->db;
        $command = $connection->createCommand();
        $command->setSelect("c.id_course, c.name, c.cover, count(id_link) as interesados, c.slug, ca.slug as cat");
        $command->setFrom(Course::model()->tableName() . ' c');
        $command->join(Cathegory::model()->tableName() . ' ca', 'ca.id_cathegory=c.id_cathegory');
        $command->leftjoin(Link::model()->tableName() . ' l', 'l.id_course=c.id_course');
        $command->setWhere("c.active=1 AND c.publish<= NOW() AND c.id_state=1" . $filtroCat . $filtroBus);  //Publicado
        $command->setGroup("c.id_course");
        $cursos = $command->queryAll();

//Lista de cursos de la pagina actual
        $command = $connection->createCommand();
        $command->setSelect("c.id_course, c.name, c.cover, count(id_link) as interesados, c.slug, ca.slug as cat");
        $command->setFrom(Course::model()->tableName() . ' c');
        $command->join(Cathegory::model()->tableName() . ' ca', 'ca.id_cathegory=c.id_cathegory');
        $command->leftjoin(Link::model()->tableName() . ' l', 'l.id_course=c.id_course');
        $command->setWhere("c.active=1 AND c.publish<= NOW() AND c.id_state=1" . $filtroCat . $filtroBus);  //Publicado
        $command->setGroup("c.id_course");

        $orden = "";
        if (isset($_GET['orden'])) {
            $orden = $_GET['orden'];
            if ($_GET['orden'] == "alfabetico") {
                $command->setOrder("c.name");
            } else if ($_GET['orden'] == "dificultad") {
                $command->setOrder("c.id_level");
            } else {
                $command->setOrder("c.publish DESC");
            }
        } else {
            $command->setOrder("c.publish DESC");
        }

        $command->setLimit($ipp * 2);
        if (isset($_GET['p']))
            $command->setOffset(($_GET['p'] - 1) * $ipp);
        $list = $command->queryAll();

        if (!Yii::app()->user->isGuest) {
            $command = $connection->createCommand();
            $command->setSelect("gc.id_course");
            $command->setFrom(GroupCourse::model()->tableName() . ' gc');
            $command->join(Group::model()->tableName() . ' g', 'g.id_group=gc.id_group');
            $command->join(GroupSuscription::model()->tableName() . ' gs', 'g.id_group=gs.id_group');
            $command->setWhere("gs.active=1 AND gs.id_user=" . Yii::app()->user->id);
            $listNo = $command->queryAll();

            foreach ($list as $key => $item) {
                foreach ($listNo as $itemNo) {
                    if ($item['id_course'] == $itemNo['id_course']) {
                        unset($list[$key]);
                        unset($cursos[$key]);
                    }
                }
            }
        }
        $list = array_slice($list, 0, $ipp);

//Paginación
        $paginas = ceil(count($cursos) / $ipp);
        $paginaactual = 1;
        if (isset($_GET['p']))
            $paginaactual = $_GET['p'];
        $desde = 1;
        if ($paginaactual - 3 > 0)
            $desde = $paginaactual - 3;
        $hasta = $paginas;
        if ($desde + 4 < $paginas)
            $hasta = $desde + 4;


        $cathegory = null;
        if (isset($_GET['cat']))
            $cathegory = Cathegory::model()->findByAttributes(array('slug' => $_GET['cat']));

        $buscar = "";
        if (isset($_GET['buscar']))
            $buscar = $_GET['buscar'];

        $this->render('cursos', array('list' => $list, 'buscar' => $buscar, 'orden' => $orden, 'cathegory' => $cathegory, 'paginas' => $paginas, 'desde' => $desde, 'hasta' => $hasta, 'paginaactual' => $paginaactual));
    }

    public function actionDiplomados() {
        $this->render('diplomados');
    }

    public function actionTuscursos() {
        $connection = Yii::app()->db;
        $command = $connection->createCommand();
        $command->setSelect(@"c.id_course, c.name, c.slug, ca.slug as cat, cs.id_user, cs.certificate, cs.active, c.cover, cs.id_csuscription, cs.progress, l.na_level, ca.na_cathegory, c.duration as duracion,
						(SELECT COUNT(t.id_topic) FROM lesson l JOIN topic t on t.id_lesson=l.id_lesson WHERE id_course=c.id_course) as clases,
						(SELECT topic.na_topic FROM progress JOIN topic ON topic.id_topic=progress.id_topic where id_csuscription=cs.id_csuscription order by topic.id_topic DESC LIMIT 1) AS na_topic,
						(SELECT topic.id_topic FROM progress JOIN topic ON topic.id_topic=progress.id_topic where id_csuscription=cs.id_csuscription order by topic.id_topic DESC LIMIT 1) AS id_topic");
        $command->setFrom(CourseSuscription::model()->tableName() . ' cs');
        $command->join(Course::model()->tableName() . ' c', 'c.id_course=cs.id_course');
        $command->leftJoin(Cathegory::model()->tableName() . ' ca', 'ca.id_cathegory=c.id_cathegory');
        $command->leftJoin(Level::model()->tableName() . ' l', 'l.id_level=c.id_level');
        $command->setWhere("cs.active=1 AND cs.id_user=" . Yii::app()->user->id);
        $command->setOrder("cs.updated_at DESC, cs.id_csuscription DESC");
        $list = $command->queryAll();

        $command = $connection->createCommand();
        $command->setSelect("gc.id_course");
        $command->setFrom(GroupCourse::model()->tableName() . ' gc');
        $command->join(Group::model()->tableName() . ' g', 'g.id_group=gc.id_group');
        $command->join(GroupSuscription::model()->tableName() . ' gs', 'g.id_group=gs.id_group');
        $command->setWhere("gs.active=1 AND gs.id_user=" . Yii::app()->user->id);
        $listNo = $command->queryAll();

        foreach ($list as $key => $item) {
            foreach ($listNo as $itemNo) {
                if ($item['id_course'] == $itemNo['id_course'])
                    unset($list[$key]);
            }
        }
        $showMsg = isset($_GET['payed']);
        $this->render('tuscursos', array('list' => $list, 'showMsg' => $showMsg));
    }

    public function actionTusdiplomados() {
        $connection = Yii::app()->db;
        $command = $connection->createCommand();
        $command->setSelect("id_group_susc, nb_group, g.image, slug");
        $command->setFrom(GroupSuscription::model()->tableName() . ' gs');
        $command->join(Group::model()->tableName() . ' g', 'g.id_group=gs.id_group');
        $command->setWhere("gs.active=1 AND gs.id_user=" . Yii::app()->user->id);
        $listDip = $command->queryAll();

        $this->render('tusdiplomados', array('listDip' => $listDip));
    }

    public function actionPortada() {
        if (isset($_GET['id'])) {
            $course = Course::model()->findByPk($_GET['id']);
            $cathegory = Cathegory::model()->findByPk(array('id_cathegory' => $course->id_cathegory));
            $this->redirect(Yii::app()->urlManager->createUrl('site/curso', array('cat' => $cathegory->slug, 'slug' => $course->slug)));
        } else {
            $this->redirect(Yii::app()->homeUrl);
        }
    }

    public function actionCurso() {
        if (isset($_GET['slug']) && isset($_GET['cat'])) {
            $acceso = 0;

            $course = Course::model()->findByAttributes(array('slug' => $_GET['slug']));
            $cathegory = Cathegory::model()->findByAttributes(array('slug' => $_GET['cat']));

            if ($course && $course->id_level && $cathegory) {
                $level = Level::model()->findByPk($course->id_level);

                if (!Yii::app()->user->isGuest) {
//Hacer link de interes
                    $model = new Link;
                    $model->id_user = Yii::app()->user->id;
                    $model->id_course = $course->id_course;
                    $model->id_ltype = 1; //Me interesa
                    $model->save();

                    if ((Yii::app()->user->utype == 'Administrador' OR Yii::app()->user->utype == 'AAC' OR Yii::app()->user->utype == 'Instructor') AND ! isset(Yii::app()->session['admin'])) {
                        $acceso = 1;
                    } else {
//Verificar si tiene acceso a descargas y acceso al curso
                        $s = Suscription::model()->findByAttributes(array("id_user" => Yii::app()->user->id, 'id_course' => $course->id_course, "active" => 1));

                        if ($s) {
                            $acceso = 1;
                        } else {
                            $connection = Yii::app()->db;
                            $command = $connection->createCommand();
                            $command->setSelect("id_group_susc");
                            $command->setFrom(GroupSuscription::model()->tableName() . ' gs');
                            $command->join(Group::model()->tableName() . ' g', 'g.id_group=gs.id_group');
                            $command->join(GroupCourse::model()->tableName() . ' gc', 'gc.id_group=g.id_group');
                            $command->setWhere("gs.active=1 AND gs.id_user=" . Yii::app()->user->id . " AND gc.id_course=" . $course->id_course);
                            $listDip = $command->queryAll();

                            if (count($listDip) > 0) {
                                $command = $connection->createCommand();
                                $command->setSelect("s.active");
                                $command->setFrom(Suscription::model()->tableName() . ' s');
                                $command->join(SuscriptionType::model()->tableName() . ' st', 'st.id_stype=s.id_stype');
                                $command->setWhere('st.recurrence=1 AND s.active=3 AND s.id_user=' . Yii::app()->user->id);
                                $s = $command->queryRow();

                                if (!$s) {
                                    $acceso = 1;
                                }
                            } else {
                                $acceso = $this->tieneSuscripcion(Yii::app()->user->id);
                            }
                        }
                    }
                }

// Redirige a la portada del curso o al curso
                if ($acceso == 0) {
                    if (isset($_GET['utm_source']))
                        Yii::app()->request->cookies['utm_source'] = new CHttpCookie('utm_source', $_GET['utm_source']);
                    if (isset($_GET['utm_medium']))
                        Yii::app()->request->cookies['utm_medium'] = new CHttpCookie('utm_medium', $_GET['utm_medium']);
                    if (isset($_GET['utm_term']))
                        Yii::app()->request->cookies['utm_term'] = new CHttpCookie('utm_term', $_GET['utm_term']);
                    if (isset($_GET['utm_content']))
                        Yii::app()->request->cookies['utm_content'] = new CHttpCookie('utm_content', $_GET['utm_content']);
                    if (isset($_GET['utm_campaign']))
                        Yii::app()->request->cookies['utm_campaign'] = new CHttpCookie('utm_campaign', $_GET['utm_campaign']);
                    if (!isset(Yii::app()->request->cookies['url']))
                        Yii::app()->request->cookies['url'] = new CHttpCookie('url', Yii::app()->request->hostInfo . '/' . Yii::app()->request->getPathInfo());

                    $email = Yii::app()->user->isGuest ? '' : Yii::app()->user->email;
                    Happens::vioPortada($email, $course->name);

                    $connection = Yii::app()->db;
                    $command = $connection->createCommand();
                    $command->setSelect("t.id_topic, l.na_lesson, t.na_topic, t.duration, t.package, t.public, t.slug as slugc");
                    $command->setFrom(Topic::model()->tableName() . ' t');
                    $command->join(Lesson::model()->tableName() . ' l', 'l.id_lesson=t.id_lesson');
                    $command->setWhere("l.id_course=" . $course->id_course);
                    $command->setOrder("l.id_lesson, t.position");
                    $list = $command->queryAll();

                    $command = $connection->createCommand();
                    $command->setSelect("c.id_course, c.name, c.cover");
                    $command->setFrom(Course::model()->tableName() . ' c');
                    $command->setWhere("c.active=1 AND c.publish<= NOW() AND c.id_state=1");
                    $command->setOrder("c.id_course DESC");
                    $command->setLimit(4);
                    $listNew = $command->queryAll();

                    $this->render('portada', array('course' => $course, 'level' => $level, 'cathegory' => $cathegory, 'current' => $list, 'list' => $list, 'listNew' => $listNew));
                }
                else {
//Si el curso lo tiene en un diplomado, reirige a la portada del diplomado
                    $diplomado = GroupSuscription::model()->findByAttributes(array('id_user' => Yii::app()->user->id));
                    if ($diplomado) {
                        $gl = GroupCourse::model()->findAllByAttributes(array('id_group' => $diplomado->id_group));
                        foreach ($gl as $gc) {
                            if ($gc->id_course == $course->id_course && $gc->nivel > $diplomado->current_level) {
                                $this->redirect(Yii::app()->urlManager->createUrl('site/diplomado', array('id' => $diplomado->id_group_susc)));
                            }
                        }
                    }

                    $cs = CourseSuscription::model()->findByAttributes(array("id_user" => Yii::app()->user->id, "id_course" => $course->id_course, "active" => 1));
                    if (!$cs) {
                        $csi = CourseSuscription::model()->findByAttributes(array("id_user" => Yii::app()->user->id, "id_course" => $course->id_course, "active" => 0));
                        if ($csi) {
                            $csi->active = 1;
                            $csi->update();
                        } else {
                            $cs = new CourseSuscription;
                            $cs->id_course = $course->id_course;
                            $cs->id_user = Yii::app()->user->id;
                            $cs->active = 1;
                            $cs->save();
                        }
                    }

//Si tiene acceso al curso o es administrador
                    if ($cs) {
//Lista los contenidos del curso
                        $connection = Yii::app()->db;
                        $command = $connection->createCommand();
                        $command->setSelect("t.slug as slugc, t.id_topic, l.na_lesson, t.na_topic, t.duration, IF(IF(ROUND((p.progress*100)/t.duration)>100, 100 ,ROUND((p.progress*100)/t.duration)) IS NULL, 0, IF(ROUND((p.progress*100)/t.duration)>100, 100 ,ROUND((p.progress*100)/t.duration))) as progress ");
                        $command->setFrom(Topic::model()->tableName() . ' t');
                        $command->join(Lesson::model()->tableName() . ' l', 'l.id_lesson=t.id_lesson');
                        $command->leftJoin(Progress::model()->tableName() . ' p', 'p.id_topic=t.id_topic AND p.id_csuscription=' . $cs->id_csuscription);
                        $command->setWhere("l.id_course=" . $course->id_course);
                        $command->setOrder("l.id_lesson, t.position");
                        $list = $command->queryAll();

//Busco el ultimo curso que ha visto
                        $command = $connection->createCommand();
                        $command->setSelect("p.id_topic, t.na_topic, t.slug as slugc");
                        $command->setFrom(Progress::model()->tableName() . ' p');
                        $command->join(Topic::model()->tableName() . ' t', 'p.id_topic=t.id_topic');
                        $command->setWhere("id_csuscription=" . $cs->id_csuscription);
                        $command->setOrder("id_topic DESC");
                        $last = $command->queryRow();

                        Happens::vioCurso(Yii::app()->user->email, $course->name);

//Progreso del curso

                        $this->calcularProgreso($course, $cs);

                        $this->render('curso', array('course' => $course, 'level' => $level, 'cathegory' => $cathegory, 'cs' => $cs, 'last' => $last, 'list' => $list));
                    } else {
                        $this->redirect(Yii::app()->urlManager->createUrl('site/curso', array('cat' => $_GET['cat'], 'slug' => $_GET['slug'])));
                    }
                }
            } else {
                $this->redirect(Yii::app()->urlManager->createUrl('site/cursos'));
            }
        } else {
            $this->redirect(Yii::app()->urlManager->createUrl('site/cursos'));
        }
    }

    public function actionDiplomado() {
        if (isset($_GET['slug'])) {
            $group = Group::model()->findByAttributes(array('slug' => $_GET['slug']));

            if (Yii::app()->user->isGuest) {
                if (isset($_GET['landing'])) {
                    $this->redirect(Yii::app()->urlManager->createUrl('l/' . $_GET['landing']));
                } else {
                    $this->redirect(Yii::app()->urlManager->createUrl('site/diplomados'));
                }
            }


            $gs = GroupSuscription::model()->findByAttributes(array('id_group' => $group->id_group, 'id_user' => Yii::app()->user->id));
//Si no esta inscrito, inscribir en el diplomado.


            $acceso = 0;

            if (User::hasActiveSubscription(Yii::app()->user->id) OR Yii::app()->user->utype == 'Administrador')
                $acceso = 1;

            if (!$gs && $acceso == 1) {
                $user = User::model()->findByPk(Yii::app()->user->id);
                $transaction = Yii::app()->db->beginTransaction();
                try {
                    $gs = new GroupSuscription;
                    $gs->id_group = $group->id_group;
                    $gs->id_user = $user->id_user;
                    $gs->date = new CDbExpression('NOW()');
                    $gs->active = 1;
                    $gs->current_level = 1;
                    $gs->save();

                    $cursos_d = GroupCourse::model()->findAllByAttributes(array('id_group' => $group->id_group));
                    foreach ($cursos_d as $curso) {
//Verificar que el curso ya tiene suscripción por parte del usuario.
                        $course_s = CourseSuscription::model()->findByAttributes(array('id_user' => $user->id_user, 'id_course' => $curso->id_course, "active" => 1));
                        if (!$course_s) {
                            $csi = CourseSuscription::model()->findByAttributes(array("id_user" => $user->id_user, "id_course" => $curso->id_course, "active" => 0));
                            if ($csi) {
                                $csi->active = 1;
                                $csi->update();
                            } else {
                                $coursesu = new CourseSuscription;
                                $coursesu->id_user = $user->id_user;
                                $coursesu->id_course = $curso->id_course;
                                $coursesu->active = 1;
                                $coursesu->save();
                            }
                        }
                    }
                    $transaction->commit();
                } catch (Exception $e) {
                    $transaction->rollback();
                    Yii::log($e->getMessage(), CLogger::LEVEL_ERROR);
                }

                $date = date_create();
                OjalaUtils::enviarIntercom($user->id_user, $user->email1, $user->name, array("REGISTRO_DIPLOMADO" => $date, "DIPLOMADO" => $group->nb_group), false, "https://oja.la");
            }

            if ($gs) {
                $connection = Yii::app()->db;
                /* $command=$connection->createCommand();
                  $command->selectDistinct("c.slug AS slugc, ca.slug AS cat, c.name, c.id_course, l.*, t.*, IF(IF(ROUND((p.progress*100)/t.duration)>100, 100 ,ROUND((p.progress*100)/t.duration)) IS NULL, 0, IF(ROUND((p.progress*100)/t.duration)>100, 100 ,ROUND((p.progress*100)/t.duration))) as progress");
                  $command->setFrom(Topic::model()->tableName() . ' t');
                  $command->join(Lesson::model()->tableName() . ' l', 'l.id_lesson=t.id_lesson');
                  $command->join(Course::model()->tableName() . ' c', 'c.id_course=l.id_course');
                  $command->join(Cathegory::model()->tableName() . ' ca', 'ca.id_cathegory=c.id_cathegory');
                  $command->join(GroupCourse::model()->tableName() . ' gc', 'c.id_course=gc.id_course');
                  $command->join(GroupSuscription::model()->tableName() . ' gs', 'gs.id_group=gc.id_group');
                  $command->leftJoin(CourseSuscription::model()->tableName() . ' cs', 'cs.id_user='.Yii::app()->user->id.' AND cs.id_course = c.id_course');
                  $command->leftJoin(Progress::model()->tableName() . ' p', 'p.id_topic=t.id_topic AND p.id_csuscription=cs.id_csuscription');
                  $command->setWhere('gs.active=1 AND gs.id_group_susc='.$gs->id_group_susc." AND gc.nivel <= gs.current_level");
                  $command->setOrder('gc.nivel, l.position ASC, t.position ASC');
                  $listCourses = $command->queryAll(); */

                $command = $connection->createCommand();
                $command->select("c.name, gc.nivel, c.slug AS slugc, ca.slug AS cat, c.id_course, l.*, t.*, IF(IF(ROUND((p.progress*100)/t.duration)>100, 100 ,ROUND((p.progress*100)/t.duration)) IS NULL, 0, IF(ROUND((p.progress*100)/t.duration)>100, 100 ,ROUND((p.progress*100)/t.duration))) as progress");
                $command->setFrom(Topic::model()->tableName() . ' t');
                $command->join(Lesson::model()->tableName() . ' l', 'l.id_lesson=t.id_lesson');
                $command->join(Course::model()->tableName() . ' c', 'c.id_course=l.id_course');
                $command->join(Cathegory::model()->tableName() . ' ca', 'ca.id_cathegory=c.id_cathegory');
                $command->join(GroupCourse::model()->tableName() . ' gc', 'c.id_course=gc.id_course');
                $command->join(Group::model()->tableName() . ' g', 'g.id_group=gc.id_group');
                $command->join(GroupSuscription::model()->tableName() . ' gs', 'gs.id_group=g.id_group');
                $command->leftJoin(CourseSuscription::model()->tableName() . ' cs', 'cs.id_user=' . Yii::app()->user->id . ' AND cs.id_course = c.id_course');
                $command->leftJoin(Progress::model()->tableName() . ' p', 'p.id_topic=t.id_topic AND p.id_csuscription=cs.id_csuscription');
                $command->setWhere('cs.active=1 AND gs.active=1 AND gs.id_group_susc=' . $gs->id_group_susc . " AND gc.nivel <= gs.current_level");
                $command->setOrder('gc.nivel, l.position ASC, t.position ASC');
                $listCourses = $command->queryAll();

                $command = $connection->createCommand();
                $command->select("c.name, gc.nivel");
                $command->setFrom(Course::model()->tableName() . ' c');
                $command->join(GroupCourse::model()->tableName() . ' gc', 'c.id_course=gc.id_course');
                $command->join(Group::model()->tableName() . ' g', 'g.id_group=gc.id_group');
                $command->join(GroupSuscription::model()->tableName() . ' gs', 'gs.id_group=g.id_group');
                $command->setWhere('gs.active=1 AND gs.id_group_susc=' . $gs->id_group_susc . " AND gc.nivel > gs.current_level");
                $command->setOrder('gc.nivel');
                $inactiveCourses = $command->queryAll();

                Happens::vioDiplomado(Yii::app()->user->email, $group->nb_group);

                $this->render('diplomado', array(
                    'gs' => $gs,
                    'group' => $group,
                    'listCourses' => $listCourses,
                    'inactiveCourses' => $inactiveCourses
                ));
            } elseif (isset($_GET['landing'])) {
                $this->redirect(Yii::app()->urlManager->createUrl('l/' . $_GET['landing']));
            } else {
                $this->redirect(Yii::app()->urlManager->createUrl('site/diplomados'));
            }
        } else {
            $this->redirect(Yii::app()->urlManager->createUrl('site/diplomados'));
        }
    }

    public function actionClase() {
        if (isset($_GET['slugc']) && isset($_GET['slug'])) {
            $course = Course::model()->findByAttributes(array('slug' => $_GET['slug']));
            $connection = Yii::app()->db;
            $command = $connection->createCommand();
            $command->setSelect("t.id_topic");
            $command->setFrom(Topic::model()->tableName() . ' t');
            $command->join(Lesson::model()->tableName() . ' l', 'l.id_lesson=t.id_lesson');
            $command->setWhere("l.id_course=" . $course->id_course . " AND t.slug='" . $_GET['slugc'] . "'");
            $row = $command->queryRow();
            $topic = Topic::model()->findByPk($row['id_topic']);

//Si es privado



            if ($topic->public == 0 || !Yii::app()->user->isGuest) {
                
            }

            if ($topic->public == 0 || !Yii::app()->user->isGuest) {
//Verificar si tiene acceso a descargas y acceso al curso
                $acceso = 0;
                $descargas = 0;

                if (Yii::app()->user->isGuest)
                    $this->redirect(Yii::app()->urlManager->createUrl('site/curso', array('cat' => $_GET['cat'], 'slug' => $course->slug)));


                if ((Yii::app()->user->utype == 'Administrador' OR Yii::app()->user->utype == 'AAC' OR Yii::app()->user->utype == 'Instructor') AND ! isset(Yii::app()->session['admin'])) {
                    $acceso = 1;
                    $descargas = 1;
                } else {
                    $s = Suscription::model()->findByAttributes(array("id_user" => Yii::app()->user->id, 'id_course' => $course->id_course, "active" => 1));
                    if ($s) {
                        $acceso = 1;
                        $descargas = 1;
                    } else {
                        $connection = Yii::app()->db;
                        $command = $connection->createCommand();
                        $command->setSelect("id_group_susc");
                        $command->setFrom(GroupSuscription::model()->tableName() . ' gs');
                        $command->join(Group::model()->tableName() . ' g', 'g.id_group=gs.id_group');
                        $command->join(GroupCourse::model()->tableName() . ' gc', 'gc.id_group=g.id_group');
                        $command->setWhere("gs.active=1 AND gs.id_user=" . Yii::app()->user->id . " AND gc.id_course=" . $course->id_course);
                        $listDip = $command->queryAll();

                        if (count($listDip) > 0) {
                            $command = $connection->createCommand();
                            $command->setSelect("s.active");
                            $command->setFrom(Suscription::model()->tableName() . ' s');
                            $command->join(SuscriptionType::model()->tableName() . ' st', 'st.id_stype=s.id_stype');
                            $command->setWhere('st.recurrence=1 AND s.active=3 AND s.id_user=' . Yii::app()->user->id);
                            $s = $command->queryRow();

                            if (!$s) {
                                $acceso = 1;
                                $descargas = 1;
                            }
                        } else {
                            $acceso = $this->tieneSuscripcion(Yii::app()->user->id);

                            $command = $connection->createCommand();
                            $command->setSelect("s.with_downloads as manual, st.with_downloads as plan, s.active");
                            $command->setFrom(Suscription::model()->tableName() . ' s');
                            $command->join(SuscriptionType::model()->tableName() . ' st', 'st.id_stype=s.id_stype');
                            $command->setWhere('st.recurrence=1 AND s.active=1 AND s.id_user=' . Yii::app()->user->id);
                            $s = $command->queryRow();

                            if ($s) {
                                if ($s['active'] == 1 && ($s['manual'] == '1' || $s['plan'] == '1')) {
                                    $descargas = 1;
                                }
                            }
                        }
                    }
                }

                if ($acceso) {
                    $cs = CourseSuscription::model()->findByAttributes(array("id_user" => Yii::app()->user->id, "id_course" => $course->id_course, "active" => 1));
                    if (!$cs) {
                        $csi = CourseSuscription::model()->findByAttributes(array("id_user" => Yii::app()->user->id, "id_course" => $course->id_course, "active" => 0));

                        if ($csi) {
                            $csi->active = 1;
                            $csi->update();
                        } else {
                            $cs = new CourseSuscription;
                            $cs->id_course = $course->id_course;
                            $cs->id_user = Yii::app()->user->id;
                            $cs->active = 1;
                            $cs->save();
                        }
                    }
                } elseif ($topic->public == 1) {
//ver publico
                    $command = $connection->createCommand();
                    $command->setSelect("t.slug, t.id_topic, l.na_lesson, t.na_topic, t.position");
                    $command->setFrom(Topic::model()->tableName() . ' t');
                    $command->join(Lesson::model()->tableName() . ' l', 'l.id_lesson=t.id_lesson');
                    $command->setWhere("l.id_course=" . $course->id_course);
                    $command->setOrder("l.id_lesson, t.position");
                    $list = $command->queryAll();

                    $acceso = 1;
                    $descargas = 1;
                    $this->render('clase', array('course' => $course, 'slug' => $_GET['slug'], 'cat' => $_GET['cat'], 'descargas' => $descargas, 'list' => $list, 'topic' => $topic));
                }

                if ($acceso) {
                    $cs = CourseSuscription::model()->findByAttributes(array("id_user" => Yii::app()->user->id, "id_course" => $course->id_course, "active" => 1));

                    if (!$cs) {
                        $csi = CourseSuscription::model()->findByAttributes(array("id_user" => Yii::app()->user->id, "id_course" => $course->id_course, "active" => 0));

                        if ($csi) {
                            $csi->active = 1;
                            $csi->update();
                        } else {
                            $cs = new CourseSuscription;
                            $cs->id_course = $course->id_course;
                            $cs->id_user = Yii::app()->user->id;
                            $cs->active = 1;
                            $cs->save();
                        }
                    }

                    $progressm = Progress::model()->findByAttributes(array('id_csuscription' => $cs->id_csuscription, 'id_topic' => $topic->id_topic));

                    $progress = 0;

                    if ($progressm != null && $progressm->progress < ($topic->duration - 5))
                        $progress = $progressm->progress;


//// LISTADO DE VIDEOS DEL CURSO
                    $command = $connection->createCommand();
                    $command->setSelect("t.slug, t.id_topic, l.na_lesson, t.na_topic, t.position, IF(IF(ROUND((p.progress*100)/t.duration)>100, 100 ,ROUND((p.progress*100)/t.duration)) IS NULL, 0, IF(ROUND((p.progress*100)/t.duration)>100, 100 ,ROUND((p.progress*100)/t.duration))) as progress");
                    $command->setFrom(Topic::model()->tableName() . ' t');
                    $command->join(Lesson::model()->tableName() . ' l', 'l.id_lesson=t.id_lesson');
                    $command->leftJoin(Progress::model()->tableName() . ' p', 'p.id_topic=t.id_topic AND p.id_csuscription=' . $cs->id_csuscription);
                    $command->setWhere("l.id_course=" . $course->id_course);
                    $command->setOrder("l.id_lesson, t.position");
                    $list = $command->queryAll();

                    $index = 0;
                    foreach ($list as $item) {
                        if ($topic->id_topic == $item['id_topic']) {
                            break;
                        }
                        $index++;
                    }

                    $previous = null;
                    $next = null;

                    if ($index < (count($list) - 1))
                        $next = $list[$index + 1];
                    if ($index > 0)
                        $previous = $list[$index - 1];

                    Happens::vioClase(Yii::app()->user->email, $course->name, $topic->na_topic);

                    $this->render('clase', array('course' => $course, 'slug' => $_GET['slug'], 'cat' => $_GET['cat'], 'descargas' => $descargas, 'idcs' => $cs->id_csuscription, 'list' => $list, 'topic' => $topic, 'progress' => $progress, 'previous' => $previous, 'next' => $next, 'email' => Yii::app()->user->email));
                }
                else {
                    $this->redirect(Yii::app()->urlManager->createUrl('site/curso', array('cat' => $_GET['cat'], 'slug' => $course->slug)));
                }
            } else {
//// LISTADO DE VIDEOS DEL CURSO
                $command = $connection->createCommand();
                $command->setSelect("t.slug, t.id_topic, l.na_lesson, t.na_topic, t.position");
                $command->setFrom(Topic::model()->tableName() . ' t');
                $command->join(Lesson::model()->tableName() . ' l', 'l.id_lesson=t.id_lesson');
                $command->setWhere("l.id_course=" . $course->id_course);
                $command->setOrder("l.id_lesson, t.position");
                $list = $command->queryAll();

                $acceso = 1;
                $descargas = 1;
                $this->render('clase', array('course' => $course, 'slug' => $_GET['slug'], 'cat' => $_GET['cat'], 'descargas' => $descargas, 'list' => $list, 'topic' => $topic));
            }
        } else {
            $this->redirect(Yii::app()->urlManager->createUrl('site/cursos'));
        }
    }

    public function actionForo() {
        if (isset($_GET['slugc']) && isset($_GET['slug']) && isset($_GET['cat'])) {
            $topic = Topic::model()->findByAttributes(array('slug' => $_GET['slugc']));
            $course = Course::model()->findByAttributes(array('slug' => $_GET['slug']));
            $this->render('foro', array('course' => $course, 'topic' => $topic, 'cat' => $_GET['cat']));
        }
    }

    public function actionPrivacidad() {

        $this->render('privacidad');
    }

    public function actionTerminos() {

        $this->render('terminos');
    }

    public function actionError() {
        if ($error = Yii::app()->errorHandler->error) {
            if (Yii::app()->request->isAjaxRequest) {
                echo $error['message'];
            } else {
                $connection = Yii::app()->db;
                $command = $connection->createCommand();
                $command->setSelect("c.id_course, c.name, c.cover, c.slug, ca.slug as cat");
                $command->setFrom(Course::model()->tableName() . ' c');
                $command->join(Cathegory::model()->tableName() . ' ca', 'ca.id_cathegory=c.id_cathegory');
                $command->setWhere("c.active=1 AND c.publish<= NOW() AND c.id_state=1");
                $command->setOrder('RAND()');
                $command->setLimit(4);
                $list = $command->queryAll();
                $this->render('404', array('list' => $list) + $error);
            }
        }
    }

///////////////REGISTRO Y LOGIN

    public function actionRegistro() {
        $model = new User;

        if (isset($_POST['User'])) {
            if (isset(Yii::app()->request->cookies['becado'])) {
                $user = User::model()->findByAttributes(array('email1' => $_POST['User']['email1']));
                if ($user && $user->id_utype == 6) {
                    $user->attributes = $_POST['User'];
                    $user->update();

//Agregar Beca
                    $subscription = new Suscription;
                    $subscription->id_user = $user->id_user;
                    $subscription->id_stype = SuscriptionType::SCHOLARSHIP;
                    $subscription->with_downloads = 1;
                    $subscription->date = new CDbExpression('NOW()');
                    $subscription->active = 1;
                    $subscription->save();

                    $ojaPayments = new OjalaPayments;
                    $ojaPayments->saveStatus($subscription->id_suscription, Status::ACTIVE, $user);

                    $identity = new UserIdentity($user->email1, '', 1);
                    $identity->authenticate();
                    Yii::app()->user->login($identity);

                    OjalaUtils::sendSlack("Nueva Beca LMS Schoology", $user->name . ' ' . $user->email1, "suscripciones", "beca", "good");

                    $this->redirect(Yii::app()->urlManager->createUrl('site/cursos'));
                }
            }

            $model->attributes = $_POST['User'];
            $utype = UserType::model()->findByAttributes(array('na_utype' => 'Estudiante'));
            $model->id_utype = $utype->id_utype;
            $model->user = $_POST['User']['email1'];
            $model->create = new CDbExpression('NOW()');
            if (!isset($_POST['User']['pass'])) {
                $model->pass = "00000";
                $model->repeatpass = "00000";
            }

            if ($model->validate()) {
                if (isset($_POST['User']['pass'])) {
                    require('password.php');
                    $model->pass = password_hash($_POST['User']['pass'], PASSWORD_DEFAULT);
                } else {
                    $model->pass = "";
                }

                if ($model->save(false)) {

                    if (isset($_POST['User']['pass'])) {
                        $identity = new UserIdentity($model->email1, $_POST['User']['pass']);
                    } else {
                        $model->verifcode = hash_hmac('sha256', microtime() . $model->pass, 'dlfkgknbcvjkbsdkjflsdkhfdf34534jkHL$@#K$^kb');
                        $model->update();

                        date_default_timezone_set('America/Caracas');
                        $activation_url = 'https://' . $_SERVER['HTTP_HOST'] . Yii::app()->urlManager->createUrl('site/cambiarClave', array("verifcode" => $model->verifcode, "id" => $model->id_user));

                        try {
                            $mandrill = new MandrillWrapper();
                            $template_name = 'Welcome';
                            $result = $mandrill->templates->info($template_name);
                            $message = array(
                                'html' => '<span>Hola ' . $model->name . '</span><br>' . $result['code'] . '<br><a href=' . $activation_url . '> >>> Para cambiar tu contraseña, clickea el siguiente <<< </a>',
                                'text' => 'Hola ' . $model->name . ', \n ' . $result['text'] . '\n' . $activation_url,
                                'subject' => 'Hola ' . $model->name . ' ' . $result['subject'],
                                'from_email' => $result['from_email'],
                                'from_name' => $result['from_name'],
                                'to' => array(
                                    array(
                                        'email' => $model->email1,
                                        'name' => $model->name,
                                        'type' => 'to'
                                    )
                                ),
                                'headers' => array('Reply-To' => $result['from_email'])
                            );
                            $async = false;
                            $send_at = date("Y-m-d H:i:s");

                            $result = $mandrill->messages->send($message, $async);
                        } catch (Mandrill_Error $e) {
                            Yii::log('A mandrill error occurred: ' . get_class($e) . ' - ' . $e->getMessage(), 'error');
//throw $e;
                        }
                        $identity = new UserIdentity($model->email1, '', 1);
                    }
                    $identity->authenticate();
                    $intercomUser = OjalaUtils::createIntercomUser($model);
                    Yii::app()->user->login($identity);

                    $user = User::model()->findByPk(Yii::app()->user->id);
                    Happens::registro($user->email1);

                    /* MailChaimp */
                    $id_mailchimp = 'ec46694dd4';
                    if (isset(Yii::app()->session['mailchimp']) && Yii::app()->session['mailchimp'] != "")
                        $id_mailchimp = Yii::app()->session['mailchimp'];

                    require_once 'MailChimp.php';
                    $MailChimp = new MailChimp('1b6d2d0238718961fb3bf87d131141e9-us6');
                    $result = $MailChimp->call('lists/subscribe', array(
                        'id' => $id_mailchimp,
                        'email' => array('email' => $user->email1),
                        'merge_vars' => array('FNAME' => $user->name, 'LNAME' => $user->lastname),
                        'double_optin' => false,
                        'update_existing' => true,
                        'replace_interests' => false,
                        'send_welcome' => false,
                    ));

                    /* Intercom */

                    OjalaUtils::enviarIntercom(Yii::app()->user->id, $user->email1, $user->name, array("LAST_NAME" => $user->lastname), true, "https://oja.la/registro");

                    $url = 'site/cursos';
                    if (isset($_POST['redirect'])) {
                        $url = $_POST['redirect'];
                    }

                    $this->redirect(Yii::app()->urlManager->createUrl($url));
                }
            }
        }

        $this->render('registro', array('model' => $model));
    }

    public function actionRecuperarClave() {
        $user = new User;

        if (isset($_POST['User'])) {
            $value = $_POST['User']['email1'];
            $user = User::model()->findByAttributes(array('email1' => $value));

            if ($user) {
                $user->verifcode = hash_hmac('sha256', microtime() . $user->pass, 'dlfkgknbcvjkbsdkjflsdkhfdf34534jkHL$@#K$^kb');
                $user->update();

                date_default_timezone_set('America/Caracas');
                $activation_url = 'https://' . $_SERVER['HTTP_HOST'] . Yii::app()->urlManager->createUrl('site/cambiarClave', array("verifcode" => $user->verifcode, "id" => $user->id_user));


                try {
                    $mandrill = new MandrillWrapper();
                    $template_name = 'Update Password';
                    $result = $mandrill->templates->info($template_name);
                    $message = array(
                        'html' => '<span>Hola ' . $user->name . '</span><br>' . $result['code'] . '<br><a href=' . $activation_url . '>>> Para cambiar tu contraseña, clickea el siguiente <<< </a>',
                        'text' => 'Hola ' . $user->name . ', \n ' . $result['text'] . '\n' . $activation_url,
                        'subject' => 'Hola ' . $user->name . ' ' . $result['subject'],
                        'from_email' => $result['from_email'],
                        'from_name' => $result['from_name'],
                        'to' => array(
                            array(
                                'email' => $user->email1,
                                'name' => $user->name,
                                'type' => 'to'
                            )
                        ),
                        'headers' => array('Reply-To' => $result['from_email'])
                    );
                    $async = false;
                    $send_at = date("Y-m-d H:i:s");
                    $result = $mandrill->messages->send($message, $async);

                    Yii::app()->user->setFlash('recoveryMessage', "Por favor revisa el correo electrónico. Las instrucciones se enviaron a tu Correo.");
                    $this->refresh();
                } catch (Mandrill_Error $e) {
                    echo 'A mandrill error occurred: ' . get_class($e) . ' - ' . $e->getMessage();
                    throw $e;
                }
            } else {
                $user = new User;
                $this->render('recuperarclave', array('mensaje' => 'Correo no Registrado', 'user' => $user));
                Yii::app()->end();
            }
        }

        $this->render('recuperarclave', array('user' => $user));
    }

    public function actionLogin() {
        Yii::app()->facebook->run();
        $facebookUser = Yii::app()->facebook->getUser();


        if ($facebookUser) {
            $fb_user_profile = Yii::app()->facebook->getUserProfile();
            $user = User::model()->findByAttributes(array('email1' => $fb_user_profile['email']));

            if ($user) {
                $identity = new UserIdentity($fb_user_profile['email'], '', 1);
                $identity->authenticate();
                Yii::app()->user->login($identity);

                $user = User::model()->findByPk(Yii::app()->user->id);

                $connection = Yii::app()->db;
                $command = $connection->createCommand();
                $command->setSelect("s.id_suscription");
                $command->setFrom(Suscription::model()->tableName() . ' s');
                $command->join(SuscriptionStatus::model()->tableName() . ' ss', 'ss.id_suscription=s.id_suscription');
                $command->setWhere("s.active=1 AND ss.active=1 AND ss.id_status=1 AND s.id_user=" . Yii::app()->user->id);
                $suscription = $command->queryRow();
                $tipo = "Lead";

                if ($suscription)
                    $tipo = "Usuario";

                Happens::inicioSesion($user->email1);
                OjalaUtils::enviarIntercom(
                        Yii::app()->user->id, $user->email1, $user->name, array("LAST_NAME" => $user->lastname), false, "https://oja.la/login"
                );

                if (isset(Yii::app()->session['tipoPlan'])) {
                    $this->redirect(Yii::app()->urlManager->createUrl('site/pagoSuscripcion'));
                } else {
                    $this->redirect(Yii::app()->user->returnUrl);
                }
            } else {
                $this->redirect(Yii::app()->urlManager->createUrl('site/registro'));
            }
        }

        $model = new LoginForm;

// if it is ajax validation request
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'login-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }

// collect user input data
        if (isset($_POST['LoginForm'])) {
            $model->attributes = $_POST['LoginForm'];
// validate user input and redirect to the previous page if valid
            if ($model->validate() && $model->login()) {
                $user = User::model()->findByPk(Yii::app()->user->id);

                $connection = Yii::app()->db;
                $command = $connection->createCommand();
                $command->setSelect("s.id_suscription");
                $command->setFrom(Suscription::model()->tableName() . ' s');
                $command->join(SuscriptionStatus::model()->tableName() . ' ss', 'ss.id_suscription=s.id_suscription');
                $command->setWhere("s.active=1 AND ss.active=1 AND ss.id_status=1 AND s.id_user=" . Yii::app()->user->id);
                $suscription = $command->queryRow();
                $tipo = "Lead";

                if ($suscription)
                    $tipo = "Usuario";

                Happens::inicioSesion($user->email1);
                OjalaUtils::enviarIntercom(Yii::app()->user->id, $user->email1, $user->name, array("LAST_NAME" => $user->lastname), false, "https://oja.la/login");

                $this->redirect(Yii::app()->user->returnUrl);
            }
        }
        $this->render('login', array('model' => $model));
    }

    public function actionLogout() {
        if (!Yii::app()->user->isGuest) {
            Happens::cerroSesion(Yii::app()->user->email);
            Yii::app()->user->logout();
            $this->redirect(Yii::app()->homeUrl);
        }
    }

    /**
     * Funcion que permite realizar el pago de una suscripción
     */
    public function actionSuscripciones() {
        if (!Yii::app()->user->isGuest && Yii::app()->user->utype == 'Becado')
            $this->redirect(Yii::app()->urlManager->createUrl('site/tusCursos'));

        $this->layout = "pagos";

        OjalaUtils::createCookieUtms();
        $userEmail = Yii::app()->user->isGuest ? '' : Yii::app()->user->email;
        Happens::vioLanding($userEmail, "suscripciones");

        $paygateway = new StripeWrapper;
        $paygateway->registerScripts('#subscriptions-form');

        $plan = '80';
        $amount = '80';

        if (isset(Yii::app()->session['currency']) && Yii::app()->session['currency'] == "mxn") {
            $plan = '80MXN';
            $amount = OjalaPayments::convertCurrency($amount, 'MXN');
        } elseif (isset(Yii::app()->session['currency']) && Yii::app()->session['currency'] == "cop") {
            $plan = '80COP';
            $amount = OjalaPayments::convertCurrency($amount, 'COP');
        }

        $st = SuscriptionType::model()->findByAttributes(array('id_service' => $plan));
        $paypalBtnCode = $st->name;
        $gw = isset($_GET['gw']) ? $_GET['gw'] : NULL;
        $email = isset($_GET['email']) ? $_GET['email'] : NULL;
        $error = isset($_GET['error']) ? StripeWrapper::translateString($_GET['error']) : NULL;
        $emailerror = isset($_GET['emailerror']) ? $_GET['emailerror'] : NULL;

        if (!Yii::app()->user->isGuest) {
            $user = User::model()->findByPk(Yii::app()->user->id);
            $email = $user->email1;
            Happens::seleccionoPlan(Yii::app()->user->email, $plan);

            OjalaUtils::enviarIntercom(Yii::app()->user->id, $user->email1, $user->name, array(
                "VIO_PLAN" => "SI",
                "LAST_NAME" => $user->lastname
                    ), false, "https://oja.la/suscripciones");
        } else {
            Happens::seleccionoPlan('', $plan);
        }

        $courses = Course::model()->published()->findAll(array(
            'select' => 'name,slug, cover',
            'limit' => 4,
            'order' => 'RAND()'
        ));

        $this->render('suscripciones', array(
            'amount' => $amount,
            'courses' => $courses,
            'email' => $email,
            'emailerror' => $emailerror,
            'error' => $error,
            'gw' => $gw,
            'paygateway' => $paygateway,
            'paypalBtnCode' => $paypalBtnCode,
            'plan' => $plan
        ));
    }

    public function actionPagoSuscripcion() {
        if (!Yii::app()->user->isGuest && Yii::app()->user->utype == 'Becado')
            $this->redirect(Yii::app()->urlManager->createUrl('site/tusCursos'));

        $this->layout = "pagos";
        OjalaUtils::createCookieUtms();

        if (isset($_POST['plan'])) {
            Yii::app()->session['tipoPlan'] = $_POST['plan'];
        } else {
            if (isset($_GET['plan'])) {
                Yii::app()->session['tipoPlan'] = $_GET['plan'];
            } else {
                unset(Yii::app()->session['tipoPlan']);
            }
        }

        if (isset(Yii::app()->session['tipoPlan'])) {
            $plan = Yii::app()->session['tipoPlan'];
            $plana = '';

            $st = SuscriptionType::model()->findByAttributes(array('id_service' => $plan));

            if ($st) {
                if ($st->interval == 'month') {
                    $mensual = $st->amount;
                    $anual = $st->amount * 10;
                } elseif ($st->interval == 'year') {
                    $mensual = $st->amount / 10;
                    $anual = $st->amount;
                }

                if (isset(Yii::app()->session['currency']) && Yii::app()->session['currency'] == "mxn") {
                    $mensual = OjalaPayments::convertCurrency($mensual, 'MXN');
                    $anual = OjalaPayments::convertCurrency($anual, 'MXN');
                } elseif (isset(Yii::app()->session['currency']) && Yii::app()->session['currency'] == "cop") {
                    $mensual = OjalaPayments::convertCurrency($mensual, 'COP');
                    $anual = OjalaPayments::convertCurrency($anual, 'COP');
                }

                switch ($st->id_service) {
                    case '25-st':
                        $plana = '250-st';
                        break;
                    case '37-st':
                        $plana = '370-st';
                        break;
                    case '25-mxn':
                        $plana = '250-mxn';
                        break;
                    case '37-mxn':
                        $plana = '370-mxn';
                        break;
                    case '25-cop':
                        $plana = '250-cop';
                        break;
                    case '37-cop':
                        $plana = '370-cop';
                        break;
                    case '50-pbs':
                        $plana = '500-pbs';
                        break;
                    case '74-pps':
                        $plana = '740-pps';
                        break;
                }

                $error = isset($_GET['error']) ? StripeWrapper::translateString($_GET['error']) : NULL;
                $emailerror = isset($_GET['emailerror']) ? $_GET['emailerror'] : NULL;
                $gateway = isset($_GET['gw']) ? $_GET['gw'] : NULL;
                $email = isset($_GET['email']) ? $_GET['email'] : NULL;

                if (!Yii::app()->user->isGuest) {
                    $user = User::model()->findByPk(Yii::app()->user->id);
                    $email = $user->email1;

                    Happens::seleccionoPlan(Yii::app()->user->email, $plan);
                    OjalaUtils::enviarIntercom(Yii::app()->user->id, $user->email1, $user->name, array(
                        "VIO_PLAN" => "SI",
                        "LAST_NAME" => $user->lastname
                            ), false, "https://oja.la/suscripciones");
                } else {
                    Happens::seleccionoPlan('', $plan);
                }

                $this->render('pagoSuscripcion', array(
                    'plan' => $plan,
                    'plana' => $plana,
                    'st' => $st,
                    'mensual' => $mensual,
                    'anual' => $anual,
                    'desc' => 'Ojala',
                    'error' => $error,
                    'emailerror' => $emailerror,
                    'gw' => $gateway,
                    'email' => $email
                ));
            } else {
                unset(Yii::app()->session['tipoPlan']);
                $this->redirect(Yii::app()->urlManager->createUrl('site/suscripciones'));
            }
        } else {
            if (isset($_GET['email']) && Yii::app()->user->isGuest) {
                $this->loginFromMail($_GET['email']);
            }
            $this->redirect(Yii::app()->urlManager->createUrl('site/suscripciones'));
        }
    }

    public function actionPagoCurso() {
        $this->layout = "pagos";
        if (isset($_GET['id'])) {
            if (!Yii::app()->user->isGuest) {
                Yii::app()->session['usuario'] = Yii::app()->user->email;
            }
            $course = Course::model()->findByPk($_GET['id']);
            $costo = ($course->cost / 100);

            if (isset(Yii::app()->session['currency']) && Yii::app()->session['currency'] == "mxn") {
                $costo = OjalaPayments::convertCurrency($costo, 'MXN');
            } elseif (isset(Yii::app()->session['currency']) && Yii::app()->session['currency'] == "cop") {
                $costo = OjalaPayments::convertCurrency($costo, 'COP');
            }
            $this->render('pagoCurso', array('course' => $course, 'costo' => $costo));
        }
    }

    /**
     * Función para logear un usuario desde su correo (si está como invitado)
     * @param  string $email  	Correo del suscriptor
     * @return  mixed 		Retorna el usuario logeado o falso si debe authenticarse
     */
    public function loginFromMail($email) {
        $userExists = User::model()->exists('email1=:email1 OR email2=:email2', array(
            ':email1' => $email,
            ':email2' => $email,
        ));

        if (!$userExists)
            return $this->userRegisterLogin($email);
        else {
            if (Yii::app()->user->isGuest) {
                try {
                    Yii::app()->user->loginRequired();
                } catch (Exception $e) {
                    return false;
                }
                return false;
            } else {
                return $this->userRegisterLogin($email);
            }
        }
    }

    public function actionConfirmacion() {
        $this->layout = "pagos";
        $paygateway = new StripeWrapper();

//Borra los chequeos previos porque acaba de hacer un pago
//y se verifica de nuevo el estado de suscripción
        unset(Yii::app()->session['deudor']);
        unset(Yii::app()->session['cancelado']);
        unset(Yii::app()->session['inactivo']);

        /* Sección registro de pago con el Gateway */
        if (isset($_GET[$paygateway->tokenName]) && isset($_GET['email']) && $_GET['email'] != "") {
            $token = $_GET[$paygateway->tokenName];
            $user = $this->loginFromMail($_GET['email']);

//Verificar validez del token del gateway
            $response = $paygateway->processPayment($user);
            $url = Yii::app()->request->baseUrl;

            if (isset($_GET['plan'])) {//Verificar si se está adquiriendo una suscripción
                if ($response === true) {
                    $plan = $_GET['plan'];
                    $st = SuscriptionType::model()->findByAttributes(array('id_service' => $plan));

                    OjalaUtils::enviarIntercom(Yii::app()->user->id, $user->email1, $user->name, array(
                        "SUSCRIPCION" => $st->na_stype,
                        "PLAN" => $st->na_stype,
                        "LAST_NAME" => $user->lastname
                            ), false, "{$url}/suscripciones");

                    $this->render('confirmacion', array(
                        'currency' => $st->currency,
                        'amount' => $st->amount,
                    ));
                } else {
                    $this->redirect(array('site/suscripciones',
                        'error' => $response,
                        'plan' => $_GET['plan'],
                        '#' => 'payments'
                    ));
                }
            } else if (isset($_GET['id'])) {  //Verificar si se está comprando un curso
                if ($response === true) {
                    OjalaUtils::enviarIntercom(Yii::app()->user->id, $user->email1, $user->name, array("TIPO_COMPRA" => "COMPRA", "LAST_NAME" => $user->lastname), false, "{$url}/oja.la/pagoCurso");
                    $curso = Course::model()->findByPk($_GET['id']);
                    Happens::nuevaCompra($user->email1, $curso->name, 'STRIPE', Yii::app()->session['currency']);

                    $this->render('confirmacion', array(
                        'currency' => Yii::app()->session['currency'],
                        'amount' => $course->cost,
                    ));
                } else {
                    $this->redirect(array('site/pagoCurso', "error" => $response, 'id' => $_GET['id']));
                }
            } else {
//En el caso de curso debería redirigir a un curso
                $this->redirect(array('site/suscripciones',
                    'mensaje' => 'Ocurrió un error inesperado. Intenta de nuevo',
                    '#' => 'payments'
                ));
            }
        } else {
            if (isset($_GET['plan'])) {
                $this->redirect(array('site/suscripciones', "mensaje" => 'Error al recibir la autorización de pago'));
            } else if (isset($_GET['id'])) {
                $this->redirect(array('site/pagoCurso', 'id' => $_GET['id'], "mensaje" => 'Error al recibir la autorización de pago'));
            } else if (isset($_GET['dev'])) {
                $this->render('confirmacion', array('currency' => NULL, 'amount' => NULL));
            } else {
                $this->redirect(array('site/suscripciones'));
            }
        }
    }

    public function actionCrearUsuario() {
        if (isset($_GET['email'])) {
            Yii::app()->session['usuario'] = $_GET['email'];
            if (isset(Yii::app()->session['usuario'])) {
                echo Yii::app()->session['usuario'];
                $this->loginFromMail($_GET['email']);
            } else {
                echo "Error creando varible";
            }
        } else {
            echo 'No llego variable';
        }
    }

    public function actionCompraPaypal() {
        $gatewayUrl = PaypalWrapper::processPayment();
        if ($gatewayUrl)
            $this->redirect($gatewayUrl);
        else
            throw new CHttpException(400, 'Petición inválida.');
    }

    public function actionConfirmPaypal() {
        if (isset(Yii::app()->session['usuario'])) {
            $user = $this->loginFromMail(Yii::app()->session['usuario']);
            Yii::app()->session->remove('usuario');

            if (isset($_GET['token']) && isset($_GET['PayerID'])) {
                $token = trim($_GET['token']);
                $result = Yii::app()->Paypal->GetExpressCheckoutDetails($token);

                $result['PAYERID'] = trim($_GET['PayerID']);
                $result['TOKEN'] = $token;
                $result['ORDERTOTAL'] = Yii::app()->session['theTotal'];

                $redirectParams = PaypalWrapper::confirmPayment($user, $result);

                if (is_array($redirectParams)) {
                    $this->render('confirmacion', $redirectParams);
                } else {
                    $this->redirect(Yii::app()->urlManager->createUrl('site/error'));
                }
            } else {
                $this->redirect(Yii::app()->urlManager->createUrl('site/error'));
            }
        }
    }

    public function actionConfirm() {
        $this->layout = "pagos";
        if (isset(Yii::app()->session['usuario'])) {
            $user = $this->loginFromMail(Yii::app()->session['usuario']);
            Yii::app()->session->remove('usuario');

//Ventana de Retorno de Paypal
            if (isset(Yii::app()->session['tipoPlan'])) {
                $st = SuscriptionType::model()->findByAttributes(array('id_service' => Yii::app()->session['tipoPlan']));
                $this->render('confirmacion', array(
                    'currency' => "usd",
                    'amount' => $st->amount,
                ));
            } else if (isset(Yii::app()->session['compraCurso'])) {
                $course = Course::model()->findByPk(Yii::app()->session['compraCurso']);
                $this->render('confirmacion', array(
                    'currency' => "usd",
                    'amount' => $course->cost,
                ));
            } else {
                $this->render('confirmacion', array(
                    'user' => $user,
                    'currency' => "usd",
                    'amount' => "",
                    'hasSuscription' => false
                ));
            }
        }
    }

///////////////PERFIL

    public function actionPerfilEditar() {
        $user = User::model()->findByPk(Yii::app()->user->id);

        if (isset($_POST['User'])) {
            if (isset($_SESSION['images_cover']))
                $user->image = $_SESSION['images_cover'];

            $user->attributes = $_POST['User'];

            if ($user->update()) {
                $intercomUser = OjalaUtils::createIntercomUser($user, array('phone' => $user->phone));
                unset($_POST['User']);
                $this->redirect(Yii::app()->urlManager->createUrl('site/PerfilEditar'));
            }
        }

        $connection = Yii::app()->db;
        $command = $connection->createCommand();
        $command->setSelect("c.id_course, c.name, id_csuscription");
        $command->setFrom(CourseSuscription::model()->tableName() . ' cs');
        $command->join(Course::model()->tableName() . ' c', 'c.id_course=cs.id_course');
        $command->setWhere("cs.id_user=" . Yii::app()->user->id);
        $list = $command->queryAll();

        $suscripcion = $this->tieneSuscripcion(Yii::app()->user->id);
        $compras = $this->tieneCompras($user);

        $this->render('perfilEditar', array('list' => $list, 'user' => $user, 'compras' => $compras, 'suscripcion' => $suscripcion));
    }

    public function actionPerfilFacturacion() {
        $user = User::model()->findByPk(Yii::app()->user->id);

        $payinfo = new PayInfo;

        if (isset($_POST['PayInfo'])) {
            $payinfo->attributes = $_POST['PayInfo'];
            $payinfo->id_user = $user->id_user;
            if ($payinfo->save()) {
                unset($_POST['PayInfo']);
                echo "Guarda";
                $this->redirect(Yii::app()->urlManager->createUrl('site/perfilSuscripcion'));
            }
        }

        $payinfo = PayInfo::model()->findByAttributes(array('id_user' => $user->id_user), array('order' => 'id_pinfo DESC'));
        if (!$payinfo)
            $payinfo = new PayInfo;

        $connection = Yii::app()->db;
        $command = $connection->createCommand();
        $command->setSelect("s.id_suscription, s.exp_date, st.na_stype, st.amount, s.date, pt.na_ptype, st.interval");
        $command->setFrom(Suscription::model()->tableName() . ' s');
        $command->join(SuscriptionType::model()->tableName() . ' st', 'st.id_stype=s.id_stype');
        $command->leftJoin(PayType::model()->tableName() . ' pt', 'pt.id_ptype=s.id_ptype');
        $command->setWhere('st.recurrence=1 AND s.active=1 AND s.id_user=' . Yii::app()->user->id);
        $suscripcion = $command->queryRow();

        $compras = $this->tieneCompras($user);

        $this->render('perfilFacturacion', array('payinfo' => $payinfo, 'suscripcion' => $suscripcion, 'compras' => $compras));
    }

    public function actionPerfilSuscripcion() {
        $user = User::model()->findByPk(Yii::app()->user->id);

        $connection = Yii::app()->db;
        $command = $connection->createCommand();
        $command->setSelect("s.id_suscription, s.exp_date, st.na_stype, st.amount, s.date, pt.na_ptype, st.interval");
        $command->setFrom(Suscription::model()->tableName() . ' s');
        $command->join(SuscriptionType::model()->tableName() . ' st', 'st.id_stype=s.id_stype');
        $command->leftJoin(PayType::model()->tableName() . ' pt', 'pt.id_ptype=s.id_ptype');
        $command->setWhere('st.recurrence=1 AND s.active=1 AND s.id_user=' . Yii::app()->user->id);
        $suscripcion = $command->queryRow();

        $command = $connection->createCommand();
        $command->setSelect("p.id_pay, p.date, p.amount, st.na_stype, pt.na_ptype, p.status, UPPER(p.currency) as currency");
        $command->setFrom(Pay::model()->tableName() . ' p');
        $command->join(Suscription::model()->tableName() . ' s', 's.id_suscription = p.id_suscription');
        $command->join(SuscriptionType::model()->tableName() . ' st', 'st.id_stype = s.id_stype');
        $command->join(PayType::model()->tableName() . ' pt', 'pt.id_ptype = p.id_ptype');
        $command->setWhere("s.id_user=" . Yii::app()->user->id . " AND p.amount>0");
        $command->setOrder("p.date DESC, p.id_pay DESC");
        $pagos = $command->queryAll();

        $compras = $this->tieneCompras($user);
//print_r($suscription);
        $this->render('perfilSuscripcion', array('user' => $user, 'suscripcion' => $suscripcion, 'pagos' => $pagos, 'compras' => $compras));
    }

    public function actionPerfilCompras() {
        $user = User::model()->findByPk(Yii::app()->user->id);

        $connection = Yii::app()->db;
        $command = $connection->createCommand();
        $command->setSelect("s.id_suscription, st.na_stype, c.cost, s.date, c.name");
        $command->setFrom(Suscription::model()->tableName() . ' s');
        $command->join(SuscriptionType::model()->tableName() . ' st', 'st.id_stype=s.id_stype');
        $command->join(Course::model()->tableName() . ' c', 'c.id_course=s.id_course');
        $command->setWhere('s.id_user=' . Yii::app()->user->id . ' AND s.active=1 AND st.recurrence=0');
        $compras = $command->queryAll();

        $command = $connection->createCommand();
        $command->setSelect("p.id_pay, p.date, p.amount, st.na_stype");
        $command->setFrom(Pay::model()->tableName() . ' p');
        $command->join(Suscription::model()->tableName() . ' s', 's.id_suscription = p.id_suscription');
        $command->join(SuscriptionType::model()->tableName() . ' st', 'st.id_stype = s.id_stype');
        $command->setWhere("s.id_user=" . Yii::app()->user->id);
        $pagos = $command->queryAll();

        $suscripcion = $this->tieneSuscripcion(Yii::app()->user->id);

//print_r($suscription);
        $this->render('perfilCompras', array('user' => $user, 'suscripcion' => $suscripcion, 'pagos' => $pagos, 'compras' => $compras));
    }

    public function actionCambiarClave() {
        $model = NULL;

        if (Yii::app()->user->isGuest && $_GET['id'] && $_GET['verifcode']) {
            $model = User::model()->findByAttributes(array('id_user' => $_GET['id'], 'verifcode' => $_GET['verifcode']));
        } else {
            $model = User::model()->findByPk(Yii::app()->user->id);
        }

        $model->setScenario('changePass');

        if (isset($_POST['User'])) {
            require('password.php');
            $model->attributes = $_POST['User'];

            if ($model->validate()) {
                User::model()->updateByPk($model->id_user, array('pass' => password_hash($model->pass, PASSWORD_DEFAULT)));
                Yii::app()->user->setFlash('recoveryMessage', "Contraseña cambiada exitosamente");
            }

            $model->pass = '';
            $model->repeatpass = '';
        }

        $compras = $this->tieneCompras($model);
        $suscripcion = $this->tieneSuscripcion(Yii::app()->user->id);

        $this->render('cambiarClave', array(
            'model' => $model,
            'suscripcion' => $suscripcion,
            'compras' => $compras
        ));
    }

///////////////OPCIONES

    private function enviarCorreo($template, $correos, $mensaje) {

        try {
            $email = "usuario_guest@oja.la";
            $name = "";
            $lastname = "";

            if (!Yii::app()->user->isGuest) {
                $email = Yii::app()->user->email;
                $name = Yii::app()->user->name;
                if (isset(Yii::app()->user->lastname)) {
                    $lastname = Yii::app()->user->lastname;
                }
            }
            $mandrill = new MandrillWrapper();
            $template_name = $template;
            $result = $mandrill->templates->info($template_name);
            $message = array(
                'html' => $result['code'] . '<br>Nombre: ' . $name . ' ' . $lastname . '<br>Email: ' . $email . '<br>Mensaje: ' . $mensaje,
                'text' => $result['text'] . '  \\n Nombre: ' . $name . ' ' . $lastname . '  \\n Email: ' . $email . '  \\n Mensaje: ' . $mensaje,
                'subject' => $result['subject'],
                'from_email' => $email,
                'from_name' => $name,
                'to' => $correos,
                'headers' => array('Reply-To' => $email)
            );
            $async = false;
            $send_at = date("Y-m-d H:i:s");
            $result = $mandrill->messages->send($message, $async);
            echo $result;
        } catch (Mandrill_Error $e) {
            echo 'A mandrill error occurred: ' . get_class($e) . ' - ' . $e->getMessage();
//throw $e;
        }
    }

    public function actionSolicitarCertificado() {
        if (isset($_GET['idcs'])) {
            $meses = array("Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre");

            $user = User::model()->findByPk(Yii::app()->user->id);
            $cs = CourseSuscription::model()->findByPk($_GET['idcs']);
            $c = Course::model()->findByPk($cs->id_course);
            $fecha = date("d") . ' de ' . $meses[date('m') - 1] . ' de ' . date("Y");
            $numero = date("dGmiY");

//$pdf = new FPDF('L', 'mm', 'Letter');
            $pdf = new tFPDF('L', 'mm', 'Letter');
            $pdf->AddPage();
            $pdf->AddFont('EdwardianScriptITC', '', 'edwardian_script_itc.ttf', true);
//$pdf->AddFont('EdwardianScriptITC','','edwardian_script_itc.php');
            $pdf->Image(Yii::app()->basePath . '/../images/certificado.jpg', 0, 0, 279.4, 215.9);
            $pdf->Cell(260, 82);
            $pdf->Ln();
            $pdf->SetTextColor(41, 89, 134);
            $pdf->SetFont('EdwardianScriptITC', '', 40);
            $pdf->Cell(260, 10, $user->name . ' ' . $user->lastname, 0, 0, 'C');
            $pdf->Ln();
            $pdf->Cell(260, 22);
            $pdf->Ln();
            $pdf->SetTextColor(0, 0, 0);
            $tamano = 28;
            if (strlen($c->name) > 80)
                $tamano = 16;
            $pdf->SetFont('EdwardianScriptITC', '', $tamano);
            $pdf->Cell(260, 10, $c->name, 0, 0, 'C');
            $pdf->Ln();
            $pdf->Cell(260, 0);
            $pdf->Ln();
            $pdf->Cell(148);
            $pdf->SetTextColor(134, 134, 135);
            $pdf->SetFont('EdwardianScriptITC', '', 22);
            $pdf->Cell(108, 7, $fecha);
            $pdf->Ln();
            $pdf->Cell(260, 24);
            $pdf->Ln();
            $pdf->SetTextColor(41, 89, 134);
            $pdf->SetFont('Arial', 'I', 16);
            $pdf->Cell(165, 10, date("d-m-Y"), 0, 0, 'C');
            $pdf->Ln();
            $pdf->Cell(260, 12);
            $pdf->Ln();
            $pdf->SetTextColor(0, 0, 0);
            $pdf->SetFont('Arial', '', 12);
            $pdf->Cell(272, 8, $numero, 0, 0, 'C');

            $cs->certificate = 'Certificado' . $_GET['idcs'] . '.pdf';
            $cs->update();
            $pdf->Output("certificados/Certificado" . $_GET['idcs'] . ".pdf", "D");

//$pdf->Output();
            $correos = array(
                array(
                    'email' => 'm@oja.la',
                    'name' => 'Elena',
                    'type' => 'to'
                ),
                array(
                    'email' => 'jc@oja.la',
                    'name' => 'Juan Carlos',
                    'type' => 'to'
                )
            );
            $this->enviarCorreo('Certificado', $correos, $c->name);
        }
    }

    public function actionSugerenciaCurso() {
        $correos = array(
            array(
                'email' => 'fr@oja.la',
                'name' => 'Francisco',
                'type' => 'to'
            ),
            array(
                'email' => 'jp@oja.la',
                'name' => 'Francisco',
                'type' => 'to'
            ),
            array(
                'email' => 'jc@oja.la',
                'name' => 'Juan Carlos',
                'type' => 'to'
            )
        );
        $this->enviarCorreo('Curso', $correos, $_GET['curso']);
    }

    public function actionDesbloquearCurso() {
        if (isset($_GET['id_course']) && isset($_GET['id_gs'])) {
            $gs = GroupSuscription::model()->findByPk($_GET['id_gs']);

            $c = Course::model()->findByPk($_GET['id_course']);
            $g = Group::model()->findByPk($gs->id_group);
            $gc = GroupCourse::model()->findByAttributes(array('id_group' => $g->id_group, 'id_course' => $c->id_course));
            $gc2 = GroupCourse::model()->findByAttributes(array('id_group' => $g->id_group, 'nivel' => ($gc->nivel + 1)));
            if ($gc2) {
                $gs->current_level = $gs->current_level + 1;
                $gs->update();
                $c2 = Course::model()->findByPk($gc2->id_course);

                $cs2 = CourseSuscription::model()->findByAttributes(array('id_user' => Yii::app()->user->id, 'id_course' => $gc2->id_course));
                if (!$cs2) {
                    $coursesu = new CourseSuscription;
                    $coursesu->id_user = Yii::app()->user->id;
                    $coursesu->id_course = $gc2->id_course;
                    $coursesu->active = 1;
                    $coursesu->save();
                }

                $this->render('cursoDesbloqueado', array('curso1' => $c->name, 'curso2' => $c2->name, 'cid' => $c2->id_course, 'id' => $_GET['id_gs'], 'slug' => $g->slug));
            } else {
                $this->redirect(Yii::app()->urlManager->createUrl('site/diplomado', array('id' => $_GET['id_gs'])));
            }
        }
    }

    public function actionCursoDesbloqueado() {
        if (isset($_GET['idcs']) && isset($_GET['id'])) {
            $cs = CourseSuscription::model()->findByPk($_GET['idcs']);
            $c = Course::model()->findByPk($cs->id_course);

            $gs = GroupSuscription::model()->findByPk($_GET['id']);
            $g = Group::model()->findByPk($gs->id_group);

            $gc = GroupCourse::model()->findByAttributes(array('id_group' => $g->id_group, 'id_course' => $c->id_course));
            $gc2 = GroupCourse::model()->findByAttributes(array('id_group' => $g->id_group, 'nivel' => ($gc->nivel + 1)));

            if ($gc2) {
                $gs->current_level = $gs->current_level + 1;
                $gs->update();
                $c2 = Course::model()->findByPk($gc2->id_course);

                $cs2 = CourseSuscription::model()->findByAttributes(array('id_user' => Yii::app()->user->id, 'id_course' => $gc2->id_course));
                if (!$cs2) {
                    $coursesu = new CourseSuscription;
                    $coursesu->id_user = Yii::app()->user->id;
                    $coursesu->id_course = $gc2->id_course;
                    $coursesu->active = 1;
                    $coursesu->save();
                }

                $this->render('cursoDesbloqueado', array('curso1' => $c->name, 'curso2' => $c2->name, 'cid' => $c2->id_course, 'id' => $_GET['id'], 'slug' => $g->slug));
            } else {
                $this->redirect(Yii::app()->urlManager->createUrl('site/diplomado', array('id' => $_GET['id'])));
            }
        }
    }

    public function actionFinCurso() {
        if (isset($_GET['idcs'])) {
            $cs = CourseSuscription::model()->findByPk($_GET['idcs']);

            $connection = Yii::app()->db;
            $command = $connection->createCommand();
            $command->setSelect("id_group_susc");
            $command->setFrom(GroupSuscription::model()->tableName() . ' gs');
            $command->join(Group::model()->tableName() . ' g', 'g.id_group=gs.id_group');
            $command->join(GroupCourse::model()->tableName() . ' gc', 'gc.id_group=g.id_group');
            $command->setWhere("gs.active=1 AND gs.id_user=" . Yii::app()->user->id . " AND gc.id_course=" . $cs->id_course);
            $listDip = $command->queryAll();

            foreach ($listDip as $value) {
                $gs = GroupSuscription::model()->findByPk($value['id_group_susc']);
                $gs->current_level = $gs->current_level + 1;
                $gs->update();
//echo $diplomado['id_group_susc'];
            }
        }
    }

    public function actionProcesar() {
        ini_set('max_execution_time', 500);
        $cl = Course::model()->findAll();
        foreach ($cl as $course) {
            if ($course->wistia_uid != "") {
                $url_wistia = "https://api.wistia.com/v1/projects/" . $course->wistia_uid . ".xml?api_password=798c438ba43ef0f968474824d764b297c4ba332e";
                try {
                    if (($response_xml_data = @file_get_contents($url_wistia)) !== false) {
                        libxml_use_internal_errors(true);
                        $data = simplexml_load_string($response_xml_data);
                        if ($data) {
                            $course->duration = 0;
                            $course->update();
                            foreach ($data->medias->media as $key => $media) {
                                $course->duration = floatval($course->duration) + floatval($media->duration);
                                $course->update();
                            }
                        }
                    }
                } catch (Exception $e) {
                    echo 'error';
                }
            }
        }

        $csl = CourseSuscription::model()->findAll();
        foreach ($csl as $cs) {
            $course = Course::model()->findByPk($cs->id_course);
            if ($course) {
                $this->calcularProgreso($course, $cs);
            }
        }
    }

    private function calcularProgreso($course, $courseSusc) {
        $connection = Yii::app()->db;
        $command = $connection->createCommand();
        $command->setSelect("SUM(progress) as seg");
        $command->setFrom(Progress::model()->tableName() . ' p');
        $command->setWhere("p.id_csuscription=" . $courseSusc->id_csuscription);
        $progreso = $command->queryRow();

        $command = $connection->createCommand();
        $command->setSelect("SUM(t.duration) as seg");
        $command->setFrom(Topic::model()->tableName() . ' t');
        $command->join(Lesson::model()->tableName() . ' l', 'l.id_lesson=t.id_lesson');
        $command->setWhere("l.id_course=" . $course->id_course);
        $duracion = $command->queryRow();

        if ($duracion['seg'] > 0) {
            $courseSusc->progress = ceil(($progreso['seg'] * 100 ) / $duracion['seg']);
            $courseSusc->updated_at = new CDbExpression('NOW()');
            $courseSusc->update();
//echo '<br>calculo: '.$course->duration.'-'.$courseSusc->progress.'%';
        }
    }

    public function actionGuardarTiempo() {
        if (!Yii::app()->user->isGuest) {
            if (isset($_GET['id']) && isset($_GET['idcs']) && isset($_GET['second'])) {
                $segundo = ceil($_GET['second']);
                $user = User::model()->findByPk(Yii::app()->user->id);

                $topic = Topic::model()->findByPk($_GET['id']);
                $lesson = Lesson::model()->findByPk($topic->id_lesson);
                $course = Course::model()->findByPk($lesson->id_course);
                $courseSusc = CourseSuscription::model()->findByPk($_GET['idcs']);

                $progress = Progress::model()->findByAttributes(array('id_csuscription' => $_GET['idcs'], 'id_topic' => $_GET['id']));
                if ($progress) {
                    if ($segundo > $progress->progress) {
                        $progress->progress = $segundo;
                        $progress->update();
                        echo '<br>guardo: ' . $segundo;
                    } else {
                        if ($progress->progress > $topic->duration) {
                            $progress->progress = $topic->duration;
                            $progress->update();
                        }
                        echo '<br>no guardo: ' . $progress->progress . '-' . $segundo;
                    }
                } else {
                    $progress = new Progress;
                    $progress->id_csuscription = $_GET['idcs'];
                    $progress->id_topic = $_GET['id'];
                    $progress->progress = $segundo;
                    $progress->save();
                    echo '<br>guardo nuevo: ' . $segundo;
                }

                if (isset($_GET['final'])) {
                    Happens::vioFinClase(Yii::app()->user->email, $course->name, $topic->na_topic);
                    OjalaUtils::enviarIntercom(Yii::app()->user->id, $user->email1, $user->name, array($course->id_course => $topic->id_topic, "LAST_NAME" => $user->lastname), false, "https://oja.la/suscripciones");
                }

                $this->calcularProgreso($course, $courseSusc);
                OjalaUtils::enviarIntercom(Yii::app()->user->id, $user->email1, $user->name, array($course->id_course => $topic->id_topic, "LAST_NAME" => $user->lastname), false, "https://oja.la/suscripciones");
            }

            $this->calcularProgreso($course, $courseSusc);
            Yii::app()->end();
        }
    }

    public function actionValorPlan() {
        if (isset($_GET['plan'])) {
            $stype = SuscriptionType::model()->findByAttributes(array('_id' => $_GET['plan']));
            if ($stype) {
                echo $stype->name;
            }
        }
    }

    public function actionAplicarCupon() {
        if (isset($_GET['cupon'])) {
            if ($_GET['cupon'] == 'YAMISMO50') {
                echo @"<input type='hidden' id='plan' name='plan' value='37-st'/>
                <div class='col-md-2 second'>
                    <div class='col-md-12 one'>
                        <h3>Básico</h3>
                        <input id = 'basic' name='plan' type='radio' value='25-st' class='tipoplan'>
                        <p>
                            <span>$</span>
                            <strong>25</strong>
                            <i>/mes</i>
                        </p>
                        <small>dólares américanos</small>
                    </div>
                    <div class='col-md-12 two'><p>âœ“</p></div>
                    <div class='col-md-12 three'><p>âœ“</p></div>
                    <div class='col-md-12 three'><p>âœ“</p></div>
                    <div class='col-md-12 four'></div>
                </div>
                <div class='col-md-2 third'>
                    <div class='col-md-12 one'>
                        <h3>Premium</h3>
                        <input checked id='premium' name='plan' type='radio' value='37-st' class='tipoplan'>
                        <p>
                            <span>$</span>
                            <strong>37</strong>
                            <i>/mes</i>
                        </p>
                        <small>dólares américanos</small>
                    </div>
                    <div class='col-md-12 two'><p>âœ“</p></div>
                    <div class='col-md-12 three'><p>âœ“</p></div>
                    <div class='col-md-12 three'><p>âœ“</p></div>
                    <div class='col-md-12 four'><p>âœ“</p></div>
                </div>'";
            } else {
                echo 'no';
            }
        } else {
            echo 'no';
        }
    }

    public function actionDescargarArchivo() {
        if (isset($_GET['id_topic']) && isset($_GET['package']) && isset($_GET['uid'])) {
///Validar acceso al material

            $path1 = "/var/Downloads/package/" . $_GET['id_topic'] . '/' . $_GET['package'];

            $path2 = "/var/Downloads/package/" . $_GET['uid'] . '/' . $_GET['package'];

            if (trim($_GET['id_topic']) != "" && file_exists($path1)) {
                return Yii::app()->request->xSendFile($path1, array('saveName' => $_GET['package']));
            } else if (file_exists($path2)) {
                return Yii::app()->request->xSendFile($path2, array('saveName' => $_GET['package']));
            }
        }
    }

    public function actionEliminarCurso() {
        if (isset($_GET['id'])) {
            $ps = Progress::model()->findAllByAttributes(array('id_csuscription' => $_GET['id']));
            foreach ($ps as $p)
                $p->delete();
            $cs = CourseSuscription::model()->findByPk($_GET['id']);
            $cs->delete();

            $this->redirect(Yii::app()->homeUrl);
        }
    }

    public function actionUpload() {
        Yii::import("ext.EAjaxUpload.qqFileUploader");

        $folder = 'images/users/'; // folder for uploaded files
        $allowedExtensions = array("jpg", "jpeg", "png", "gif"); //array("jpg","jpeg","gif","exe","mov" and etc...
        $sizeLimit = 5 * 1024 * 1024; // maximum file size in bytes
        $uploader = new qqFileUploader($allowedExtensions, $sizeLimit);
        $result = $uploader->handleUpload($folder);
        $return = htmlspecialchars(json_encode($result), ENT_NOQUOTES);

        $fileSize = filesize($folder . $result['filename']); //GETTING FILE SIZE
        $fileName = $result['filename']; //GETTING FILE NAME

        $_SESSION['images_cover'] = $fileName;

        echo $return; // it's array*/
    }

///////////////FUNCIONES

    public function userRegisterLogin($email) {
//Con email válido se pasa a la autenticación y/o creación del usuario
        if (!Yii::app()->user->isGuest) {
//Si el usuario ya existe y está logueado
            $user = User::model()->findByPk(Yii::app()->user->id);
            return $user;
        } else {
//Si el usuario es invitado
            $user = User::model()->findByAttributes(array('email1' => $email));
            if ($user) {
//Si el usuario existe ya
//Se consulta la suscripción asociada y su estatus, respecto a ese usuario
                $connection = Yii::app()->db;
                $command = $connection->createCommand();
                $command->setSelect("s.id_suscription");
                $command->setFrom(Suscription::model()->tableName() . ' s');
                $command->join(SuscriptionStatus::model()->tableName() . ' ss', 'ss.id_suscription=s.id_suscription');
                $command->setWhere("s.active=1 AND ss.active=1 AND ss.id_status=1 AND s.id_user=" . $user->id_user);
                $suscription = $command->queryRow();
                $tipo = "Lead";

                if ($suscription)
                    $tipo = "Usuario";

                Happens::inicioSesion($user->email1);
            }
            else {
//Si el usuario no existe, se crea un nuevo registro de usuario
                $user = new User;
                $user->create = new CDbExpression('NOW()');
                $user->email1 = $email;
                $utype = UserType::model()->findByAttributes(array('na_utype' => 'Estudiante'));
                $user->id_utype = $utype->id_utype;
                $user->user = $email;
                $user->name = $email;
                $user->pass = "";
                $user->verifcode = hash_hmac('sha256', microtime() . $user->pass, 'dlfkgknbcvjkbsdkjflsdkhfdf34534jkHL$@#K$^kb');

//Si fue invitado se debe guardar quien lo refirió
                if (isset(Yii::app()->request->cookies['referral'])) {
                    $securityManager = new CSecurityManager;
                    $referredEmail = $securityManager->decrypt(Yii::app()->request->cookies['referral']->value, Yii::app()->params['cookieCryptKey']);
                    $referredUser = User::model()->find(array(
                        'select' => 'id_user',
                        'condition' => 'email1=:email',
                        'params' => array(':email' => $referredEmail),
                    ));
                    $user->id_refuser = $referredUser->id_user;
                    $user->invitation_from = $referredUser->email1;
                }
                $user->save(false);

                $activation_url = 'https://' . $_SERVER['HTTP_HOST'] . Yii::app()->urlManager->createUrl('site/cambiarClave', array("verifcode" => $user->verifcode, "id" => $user->id_user));


                try {
                    $mandrill = new MandrillWrapper();
                    $template_name = 'Welcome';
                    $result = $mandrill->templates->info($template_name);
                    $message = array(
                        'html' => '<span>Hola ' . $user->name . '</span><br>' . $result['code'] . '<br><a href=' . $activation_url . '> >>> Para cambiar tu contraseña, clickea el siguiente <<< </a>',
                        'text' => 'Hola ' . $user->name . ', \n ' . $result['text'] . '\n' . $activation_url,
                        'subject' => 'Hola ' . $user->name . ' ' . $result['subject'],
                        'from_email' => $result['from_email'],
                        'from_name' => $result['from_name'],
                        'to' => array(
                            array(
                                'email' => $user->email1,
                                'name' => $user->name,
                                'type' => 'to'
                            )
                        ),
                        'headers' => array('Reply-To' => $result['from_email'])
                    );
                    $result = $mandrill->messages->send($message, false);
                } catch (Mandrill_Error $e) {
//echo 'A mandrill error occurred: ' . get_class($e) . ' - ' . $e->getMessage();
//throw $e;
                }
                $id_mailchimp = 'ec46694dd4';

                if (isset(Yii::app()->session['mailchimp']) && Yii::app()->session['mailchimp'] != "")
                    $id_mailchimp = Yii::app()->session['mailchimp'];

                require_once 'MailChimp.php';
                $MailChimp = new MailChimp('1b6d2d0238718961fb3bf87d131141e9-us6');
                $result = $MailChimp->call('lists/subscribe', array(
                    'id' => $id_mailchimp,
                    'email' => array('email' => $user->email1),
                    'merge_vars' => array('FNAME' => $user->name, 'LNAME' => $user->lastname),
                    'double_optin' => false,
                    'update_existing' => true,
                    'replace_interests' => false,
                    'send_welcome' => false,
                ));

                Happens::registro($user->email1);
                OjalaUtils::enviarIntercom(Yii::app()->user->id, $user->email1, $user->name, array("LAST_NAME" => $user->lastname), true, "https://oja.la/registro");
            }

//Se autentica al usuario dentro del sistema
            $identity = new UserIdentity($user->email1, '', 1);
            $identity->authenticate();
            Yii::app()->user->login($identity);
            return $user;
        }
    }

    public function actionRedireccionLanding() {
        if (Yii::app()->request->urlReferrer != "" && isset($_GET['email'])) {
            $this->userRegisterLogin($_GET['email']);
            if (isset($_GET['diplomado'])) {
                $group = Group::model()->findByAttributes(array('slug' => $_GET['diplomado']));
                $user = User::model()->findByAttributes(array('email1' => $_GET['email']));
                if ($user && $group) {
                    $gs = GroupSuscription::model()->findByAttributes(array('id_group' => $group->id_group, 'id_user' => $user->id_user));
                    if (!$gs) {
                        $group_susc = new GroupSuscription;
                        $group_susc->id_group = $group->id_group;
                        $group_susc->id_user = $user->id_user;
                        $group_susc->date = new CDbExpression('NOW()');
                        $group_susc->active = 1;
                        $group_susc->current_level = 1;
                        $group_susc->save();

                        $cursos_d = GroupCourse::model()->findAllByAttributes(array('id_group' => $group->id_group));
                        foreach ($cursos_d as $curso) {
//Verificar que el curso ya tiene suscripciÃ³n por parte del usuario.
                            $course_s = CourseSuscription::model()->findByAttributes(array('id_user' => $user->id_user, 'id_course' => $curso->id_course, "active" => 1));
                            if (!$course_s) {
                                $csi = CourseSuscription::model()->findByAttributes(array("id_user" => $user->id_user, "id_course" => $curso->id_course, "active" => 0));
                                if ($csi) {
                                    $csi->active = 1;
                                    $csi->update();
                                } else {
                                    $coursesu = new CourseSuscription;
                                    $coursesu->id_user = $user->id_user;
                                    $coursesu->id_course = $curso->id_course;
                                    $coursesu->active = 1;
                                    $coursesu->save();
                                }
                            }
                        }
                    }
                }
                $this->redirect(Yii::app()->urlManager->createUrl('site/diplomado', array('slug' => $group->slug)));
            } elseif (isset($_GET['url'])) {
                $this->redirect(Yii::app()->urlManager->createUrl('site/' . $_GET['url']));
            } else {
                $user = User::model()->findByAttributes(array('email1' => $_GET['email']));
                if ($user) {
                    $suscription = new Suscription;
                    $suscription->id_user = $user->id_user;
                    $suscription->id_stype = 16; /// Compra curso
                    $suscription->id_course = 239; //iphone
                    $suscription->id_ptype = 2; /// Stripe
                    $suscription->date = new CDbExpression('NOW()');
                    $suscription->active = 1;
                    $suscription->status = 1; // 0-Cancelada, 1-Activa, 2-PENDING
                    $suscription->save();
                }

                $curso = Course::model()->findByPk(239);
                $this->redirect(Yii::app()->urlManager->createUrl('site/curso', array('cat' => $curso->category->slug, 'slug' => $curso->slug)));
            }
        }
    }

    private function tieneSuscripcion($userId) {
        $status = false;

        unset(Yii::app()->session['deudor']);
        unset(Yii::app()->session['cancelado']);
        unset(Yii::app()->session['inactivo']);

        if (User::hasActiveSubscription($userId)) {
            return true;
        }

        if (User::hasActiveSubscription($userId, NULL, 0)) {
            Yii::app()->session['cancelado'] = true;
        } else if (User::hasActiveSubscription($userId, NULL, 2)) {
            Yii::app()->session['deudor'] = true;
        } else if (User::hasActiveSubscription($userId, NULL, 3)) {
            Yii::app()->session['inactivo'] = true;
        }

        return false;
    }

    private function tieneCompras($user) {
        $connection = Yii::app()->db;
        $command = $connection->createCommand();
        $command->setSelect("s.id_suscription");
        $command->setFrom(Suscription::model()->tableName() . ' s');
        $command->setWhere("s.id_stype=1 AND s.active=1 AND s.id_user=" . $user->id_user);
        $comprasr = $command->queryRow();

        $compras = 0;

        if ($comprasr)
            $compras = 1;

        return $compras;
    }

///////////////LISTENERS

    public function actionCancel() {
        $var = "";
        $raw_post_data = @file_get_contents('php://input');
        $raw_post_array = explode('&', $raw_post_data);
        $myPost = array();
        foreach ($raw_post_array as $keyval) {
            $keyval = explode('=', $keyval);
            if (count($keyval) == 2)
                $myPost[$keyval[0]] = urldecode($keyval[1]);

            $var.=" & " . $keyval[0] . " = " . urldecode($keyval[1]);
        }

        OjalaUtils::enviarPush("IPN CANCEL " . $myPost['txn_type'], "");
    }

    public function actionHookPaypal() {
        $paygateway = new PaypalWrapper;
        $paygateway->processIPN();
    }

    public function actionHookConekta() {
        $paygateway = new ConektaWrapper;
        $paygateway->processPayWebHook();
    }

    public function actionHookStripe() {
        $paygateway = new StripeWrapper;
        $paygateway->processPayWebHook();
    }

    public function actionHookLanding() {
        $paygateway = new StripeWrapper;
        $paygateway->processLandingHook();
    }

///////////////NOTIFICACIONES

    public function actionCancelarSuscripcion() {
        if (isset($_POST['cancelar'])) {
            $user = User::model()->findByPk(Yii::app()->user->id);
            $this->enviarTagIntercom("cancelar", $user->email1, $user->id_user);
            Yii::app()->end();
        }
    }

    public function actionCambiarPlan() {
        $user = User::model()->findByPk(Yii::app()->user->id);
        $this->enviarTagIntercom("cambiar", $user->email1, $user->id_user);
        $this->redirect(Yii::app()->urlManager->createUrl('site/perfilSuscripcion'));
    }

    private function enviarTagIntercom($name, $email, $id) {
        $intercom = new Intercom('wmlo15pq', '6b33c947d29bd05e0ac2bf71cfe4921e27e06e6d');
        $res = $intercom->createTag($name, array($email), array($id), "teal", "tag");
    }

//html para un invoice
    public function actionInvoice() {
        $this->layout = "landings";
        if (isset($_GET['ip'])) {
            $pay = Pay::model()->findByPk($_GET['ip']);
            if ($pay) {
                $susc = Suscription::model()->findByPk($pay->id_suscription);
                if ($susc && Yii::app()->user->id == $susc->id_user) {
                    $user = User::model()->findByPk(Yii::app()->user->id);
                    $payinfo = PayInfo::model()->findByAttributes(array('id_user' => $user->id_user), array('order' => 'id_pinfo DESC'));

                    if (!$payinfo) {
                        $this->redirect(Yii::app()->urlManager->createUrl('site/perfilFacturacion', array('redirect' => 1)));
                    } else {
                        if (empty($pay->invoice_number)) {
                            $pay->invoice_number = Yii::app()->dateFormatter->format('HHmmdMyyyy', strtotime("now"));
                            $pay->update();
                        }
                        $st = SuscriptionType::model()->findByPk($susc->id_stype);
                        $mPDF1 = Yii::app()->ePdf->mpdf('', 'Letter');
                        $stylesheet = file_get_contents('css/invoice.css');
                        $mPDF1->WriteHTML($stylesheet, 1);
                        $mPDF1->WriteHTML($this->render('invoice', array('user' => $user, 'pay' => $pay, 'susc' => $susc, 'st' => $st, 'payinfo' => $payinfo), true), 0, true, false);
                        $mPDF1->Output('Factura-' . $pay->invoice_number, 'D');
                    }
                } else {
                    $this->redirect(Yii::app()->urlManager->createUrl('site/perfilSuscripcion', array('mensaje' => "Error generando la factura")));
                }
            } else {
                $this->redirect(Yii::app()->urlManager->createUrl('site/perfilSuscripcion', array('mensaje' => "Error generando la factura")));
            }
        } else {
            $this->redirect(Yii::app()->urlManager->createUrl('site/perfilSuscripcion', array('mensaje' => "Error generando la factura")));
        }
    }

    public function actionhasActiveSubscription() {
        if (Yii::app()->request->isAjaxRequest) {
            echo User::hasActiveSubscription(Yii::app()->user->id) ? 'active' : 'inactive';
            exit();
        } else {
            $this->redirect(Yii::app()->urlManager->createUrl('/'));
        }
    }

    public function actionCourseInfo() {
        if (Yii::app()->request->isAjaxRequest && isset($_POST['slug'])) {
            $course = Course::model()->with('lessons', 'lessons.topics')->find('t.slug=:slug', array(
                ':slug' => $_POST['slug']
            ));

            echo CJSON::encode(array(
                'name' => $course->name,
                'wistiaUid' => $course->lessons[0]->topics[0]->wistia_uid,
                'lessons' => $course->lessons,
                'content' => $course->content,
            ));
            exit();
        } else {
            $this->redirect(Yii::app()->urlManager->createUrl('/'));
        }
    }

    /**
     * Pantalla donde se le muestra el asesor y el usuario coloca
     * sus números telefónicos
     */
    public function actionConfirmacionAsesor() {
        $model = new AdvisorForm;
        $model->invalidPayment = isset($_GET['invalidPayment']);
        $user = User::model()->findByPk(Yii::app()->user->id);

//Chequea que hubo cambio de estado del pago
        User::hasActiveSubscription(Yii::app()->user->id);

        if (isset($_POST['AdvisorForm'])) {
            $model->attributes = $_POST['AdvisorForm'];

            if ($model->validate()) {
                $user->phone = $model->phoneNumber;
                $user->id_region = $model->region;
                $user->update();

                $region = Region::model()->with('country')->find('id_region=:id_region', array(
                    ':id_region' => $model->region
                ));

                try {
                    $template_name = 'mail-asesor';
                    $reason = $model->invalidPayment ? 'cuyo pago de primera suscripción falló en ' : 'que se acaba de suscribir a';

                    $intercomUser = OjalaUtils::createIntercomUser($user, array(
                                'phone' => $user->phone,
                                'region' => $region->name,
                                'country' => $region->country->name
                    ));

                    if ($intercomUser) {
                        $intercomUrl = 'https://app.intercom.io/apps/wmlo15pq/users/' . $intercomUser->intercom_id;
                    } else {
                        $intercomUrl = NULL;
                    }

                    $user->phoneType = $model->phoneType;
                    $regionCode = empty($region->area_code) && $region->area_code != 0 ? '' : '(' . $region->area_code . ')';
                    $subject = $model->invalidPayment ? '[Interesado]-Llamar para confirmar el error en el pago' : '[Suscriptor]-¡Llamar para coordinar programa con el estudiante!';

                    foreach (Yii::app()->params['advisorEmails'] as $advisorEmail) {
                        $message = array(
                            'subject' => $subject,
                            'global_merge_vars' => array(
                                array('name' => 'REASON', 'content' => $reason),
                                array('name' => 'NAME', 'content' => $user->fullname),
                                array('name' => 'EMAIL', 'content' => $user->email1),
                                array('name' => 'PHONE', 'content' => $user->phone),
                                array('name' => 'PHONETYPE', 'content' => $model->phoneType),
                                array('name' => 'REGION', 'content' => $region->name),
                                array('name' => 'REGION_CODE', 'content' => $regionCode),
                                array('name' => 'COUNTRY', 'content' => $region->country->name),
                                array('name' => 'COUNTRY_CODE', 'content' => $region->country->phone_code),
                                array('name' => 'INTERCOMURL', 'content' => $intercomUrl),
                            ),
                            'to' => array(
                                array(
                                    'email' => $advisorEmail,
                                    'name' => 'Asesor de Oja.la',
                                    'type' => 'to'
                                )
                            )
                        );

                        $mandrill = new MandrillWrapper();
                        $mandrill->messages->sendTemplate($template_name, NULL, $message, true);
                        $model->dataSent = true;
                    }
                } catch (Mandrill_Error $e) {
                    Yii::log('Mandrill error: ' . $e->getMessage(), CLogger::LEVEL_ERROR);
                }
            }
        } else {

//Si no es nuevo usuario lo manda a sus cursos
            if ((!$user->isNewSubscriptor || !empty($user->phone)) && !isset($_GET['dev'])) {
                $this->redirect(array('site/tuscursos', 'payed' => true), true);
            }

            $intercomUser = OjalaUtils::createIntercomUser($user, array(
                        'phone' => 'NO',
            ));
        }

        $this->render('confirmacionAsesor', array(
            'model' => $model
        ));
    }

    private function saveEvent($id_user, $id_suscription, $id_event, $date, $id_stype, $id_ptype, $id_currency) {
        $happen = new RepHappen;
        $happen->id_user = $id_user;
        $happen->id_suscription = $id_suscription;
        $happen->id_event = $id_event;
        $happen->id_time = $date;
        $happen->id_ptype = $id_ptype;
        $happen->id_currency = $id_currency;
        $happen->created_at = new CDbExpression('NOW()');

        if (!$happen->save()) {
            echo var_export($happen);
            die;
        }

        return true;
    }

    public function actionRt() {
        $malos_registros = 0;
        ini_set('max_execution_time', 500);
        $command = Yii::app()->db2->createCommand();
        $command->delete(RepHappen::model()->tableName(), 'id_event IN (2,3,6)');

        $today = new DateTime('now');
        $startDate = new DateTime('2013-03-01');
        $subscribers = new Subscribers();

        do {
            $subscribers->fromSubDate = $startDate->format('d/m/Y');
            $subscribers->toSubDate = $startDate->format('t/m/Y');

            $canSubEvent = RepEvent::model()->findByAttributes(array('na_event' => RepEvent::CANCELATION));
            $debtSubEvent = RepEvent::model()->findByAttributes(array('na_event' => RepEvent::DEBT));
            $newSubEvent = RepEvent::model()->findByAttributes(array('na_event' => RepEvent::SUBSCRIPTOR));

            $subsSQL = $subscribers->getSQL();
            $command = Yii::app()->db->createCommand($subsSQL);
            $subscriptions = $command->queryAll();

            foreach ($subscriptions as $subscription) {
                $subCurrency = empty($subscription['currency']) ? 'USD' : strtoupper($subscription['currency']);

                $currency = RepCurrency::model()->find(array(
                    'select' => 'id_currency',
                    'condition' => 'currency=:currency',
                    'params' => array(':currency' => $subCurrency)
                ));

                if (!$currency) {
                    $currency = RepCurrency::model()->find(array(
                        'select' => 'id_currency',
                        'condition' => 'currency=:currency',
                        'params' => array(':currency' => 'USD')
                    ));
                }

                try {
                    $this->saveEvent(
                            $subscription['id_user'], $subscription['id_suscription'], $newSubEvent->id_event, $subscription['subscriptionDate'], $subscription['id_stype'], $subscription['id_ptype'], $currency->id_currency
                    );



                    if ($subscription['active'] != 1 && $subscription['subscriptionDate'] != NULL) {
                        $event = $subscription['active'] == 0 ? $canSubEvent : $debtSubEvent;
                        $cancelationDate = !empty($subscription['lastStatusDate']) ? $subscription['lastStatusDate'] : $subscription['exp_date'];
                        $this->saveEvent(
                                $subscription['id_user'], $subscription['id_suscription'], $event->id_event, $cancelationDate, $subscription['id_stype'], $subscription['id_ptype'], $currency->id_currency
                        );
                    }

//Añade los estados en deuda (la cancelación se hace en la comparación anterior)
//coloco los active = 0 porque en el if anterior se debió almacenar el activo
                    $subsStatuses = SuscriptionStatus::model()->findAll(
                            'id_suscription=:id_suscription AND active=0 AND id_status IN (:debt,:debtStandBy)', array(
                        ':id_suscription' => $subscription['id_suscription'],
                        ':debt' => Status::DEBT,
                        ':debtStandBy' => Status::DEBT_STANDBY
                            )
                    );

                    if ($subsStatuses) {
                        foreach ($subsStatuses as $subsStatus) {
                            if ($subsStatus->date) {
                                $this->saveEvent(
                                        $subscription['id_user'], $subscription['id_suscription'], $debtSubEvent->id_event, $subsStatus->date, $subscription['id_stype'], $subscription['id_ptype'], $currency->id_currency
                                );
                            }
                        }
                    }
                } catch (Exception $e) {
                    $malos_registros++;
                }
            }

            $startDate->add(new DateInterval('P1M'));
        } while ($startDate < $today);

        echo 'Malos registros ' . $malos_registros;
    }

}
