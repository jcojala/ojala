<?php

class IdeasController extends Controller {

    public function actionIndex() {
        $this->render('index');
    }

    public function actionProxCursos() {

        $ipp = 16;

        $filtroCategoria = "";
        if (isset($_GET['cat'])) {
            $filtroCategoria = Yii::app()->request->getParam('cat');
        }
        $filtroBuscar = "";
        if (isset($_GET['buscar'])) {
            $filtroBuscar = Yii::app()->request->getParam('buscar');
        }
        $filtroOrden = "";
        if (isset($_GET['orden'])) {
            $filtroOrden = Yii::app()->request->getParam('orden');
        }

        $criteria = new CDbCriteria;
        $criteria->with = array(
            'idCathegory' => array('select' => 'na_cathegory'),
            'idLevel',
            'linkProposals'
        );

        if ($filtroCategoria != "") {
            $criteria->addCondition("idCathegory.na_cathegory LIKE '" . $filtroCategoria . "'");
        }

        if ($filtroBuscar != "") {
            $busqueda = new Searchh;
            if (!Yii::app()->user->isGuest) {
                $busqueda->id_user = Yii::app()->user->id;
            }
            $busqueda->text = $filtroBuscar;
            if ($busqueda->validate()) {
                $busqueda->save();
            }
            $criteria->addCondition("t.name LIKE '%" . $filtroBuscar . "%'");
        }

        $criteria->addCondition("t.active = 1");

        if ($filtroOrden != "") {
            if ($filtroOrden == "alfabetico") {
                $criteria->order = 't.name DESC';
            } else if ($filtroOrden == "dificultad") {
                $criteria->order = 't.id_level DESC';
            } else {
                $criteria->order = 't.create DESC';
            }
        } else {
            $criteria->order = 't.create DESC';
        }

        $listCat = Cathegory::model()->findAll();

        $list = Proposal::model()->findAll($criteria);
        
        $this->render('listado', array('categorias' => $listCat, 'list' => $list, 'categoria' => $filtroCategoria,'buscar'=>$filtroBuscar));
    }

    public function actionActivarPropuesta($id) {

        $proposal = Proposal::model()->findByPk($id);

        $proposal->active = 1;

        $proposal->update();

        $this->redirect(Yii::app()->urlManager->createUrl('ideas/listado', array('desactivo' => 1)));
    }

    public function actionDesactivarPropuesta($id) {
        $proposal = Proposal::model()->findByPk($id);

        $proposal->active = 0;

        $proposal->update();

        $this->redirect(Yii::app()->urlManager->createUrl('ideas/listado', array('desactivo' => 0)));
    }

    public function actionEditarPropuesta($id) {

        $proposal = Proposal::model()->findByPk($id);
        if (isset($_POST['Proposal'])) {

            $transaction = Yii::app()->db->beginTransaction();

            try {

                $proposal->attributes = $_POST['Proposal'];
                $proposal->id_cathegory = $_POST['id_cathegory'];
                $proposal->id_level = $_POST['id_level'];
                if (isset(Yii::app()->session['images_coverProposal'])) {
                    $proposal->deleteImages();
                    $proposal->cover = Yii::app()->session['images_coverProposal'];
                    $proposal->generateThumbnails();
                    unset(Yii::app()->session['images_coverProposal']);
                }

                $proposal->update();

                if (isset($_POST['tags'])) {
                    ProposalTag::model()->deleteAll('id_proposal=' . $proposal->id_proposal);
                    $tags = sprintf("%s", $_POST['tags']);  //Avoids XSS
                    $tagsArray = preg_split('/ |,/', $tags);
                    foreach ($tagsArray as $key => $value) {
                        $tag = Tag::model()->findByAttributes(array('tag' => $value));
                        if (!$tag) {
                            $tag = new Tag;
                            $tag->tag = $value;
                            $tag->save();
                        }
                        $ptag = ProposalTag::model()->findByAttributes(array('id_proposal' => $proposal->id_proposal, 'id_tag' => $tag->id_tag));
                        if (!$ptag) {
                            $ptag = new ProposalTag;
                            $ptag->id_proposal = $proposal->id_proposal;
                            $ptag->id_tag = $tag->id_tag;
                            $ptag->save();
                        }
                    }
                }

                $transaction->commit();
            } catch (Exception $e) {
                $transaction->rollback();
                Yii::log($e->getMessage(), CLogger::LEVEL_ERROR);
                //$this->redirect(Yii::app()->urlManager->createUrl('admin/editarCurso', array('id' => $course->id_course, 'msg' => 'Ocurrio un error al intentar guardar')));
            }
        }
        $tags = ProposalTag::model()->with('idTag')->findAllByAttributes(array(
            'id_proposal' => $id
        ));
        $listCat = Cathegory::model()->findAll();
        $listNiv = Level::model()->findAll();
        $this->render('editar', array('proposal' => $proposal, 'listCat' => $listCat, 'listNiv' => $listNiv, 'tags' => $tags));
    }

    public function actionUpload() {
        Yii::import("ext.EAjaxUpload.qqFileUploader");

        $folder = Yii::app()->params['coverProposalPath']; // folder for uploaded files
        $allowedExtensions = array("jpg", "jpeg", "png", "gif"); //array("jpg","jpeg","gif","exe","mov" and etc...
        $sizeLimit = 5 * 1024 * 1024; // maximum file size in bytes
        $uploader = new qqFileUploader($allowedExtensions, $sizeLimit);
        $result = $uploader->handleUpload($folder);
        $return = htmlspecialchars(json_encode($result), ENT_NOQUOTES);

        $fileSize = filesize($folder . $result['filename']); //GETTING FILE SIZE
        $fileName = $result['filename']; //GETTING FILE NAME

        Yii::app()->session['images_coverProposal'] = $fileName;

        echo $return; // it's array*/
    }

    public function actionListado($desactivo) {

        $criteria = new CDbCriteria;
        $criteria->with = array(
            'idCathegory' => array('select' => 'na_cathegory'),
            'idLevel',
            'ProposalTags' => array(
                'select' => 'id_ptag',
            ),
            'ProposalTags.idTag' => array(
                'select' => 'tag',
            ),
        );

        if ($desactivo == 1) {
            $criteria->addCondition("t.active = 1");
        } else {
            $criteria->addCondition("t.active = 0");
        }

        if (isset($_GET['dato'])) {
            $criteria->addCondition("t.name LIKE '%" . $_GET['dato'] . "%'");
        }

        $list1 = Proposal::model()->findAll($criteria);

        $this->render('propuestas', array('list1' => $list1));
    }

    public function actionPropuesta() {

        $proposal = new Proposal;

        if (isset($_POST['Proposal'])) {

            $proposal->attributes = $_POST['Proposal'];
            $proposal->id_cathegory = $_POST['id_cathegory'];
            $proposal->id_level = $_POST['id_level'];

            if (isset(Yii::app()->session['images_coverProposal'])) {
                $proposal->deleteImages();
                $proposal->cover = Yii::app()->session['images_coverProposal'];
                $proposal->generateThumbnails();
                unset(Yii::app()->session['images_coverProposal']);
            }

            $proposal->create = new CDbExpression('NOW()');
            $proposal->active = 1;
            if ($proposal->save(false)) {
                $this->redirect(Yii::app()->urlManager->createUrl('ideas/listado', array('desactivo' => 1)));
            }
        }

        $listCat = Cathegory::model()->findAll();
        $listNiv = Level::model()->findAll();
        $this->render('propuesta', array('proposal' => $proposal, 'listCat' => $listCat, 'listNiv' => $listNiv));
    }

    // Uncomment the following methods and override them if needed
    /*
      public function filters()
      {
      // return the filter configuration for this controller, e.g.:
      return array(
      'inlineFilterName',
      array(
      'class'=>'path.to.FilterClass',
      'propertyName'=>'propertyValue',
      ),
      );
      }

      public function actions()
      {
      // return external action classes, e.g.:
      return array(
      'action1'=>'path.to.ActionClass',
      'action2'=>array(
      'class'=>'path.to.AnotherActionClass',
      'propertyName'=>'propertyValue',
      ),
      );
      }
     */
}
