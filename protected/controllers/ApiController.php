<?php
require('SiteController.php');

class ApiController extends Controller
{
	public function filters()
	{
		return array(
			'accessControl',
		);
	}

	public function accessRules()
	{
	    return array(
			array('allow',
				'actions'=>array('regFromLanding', 'landingAnswers', 'regions'),
		  		'users'=>array('*'),
			),
			array('allow',
				'actions'=>array('tags'),
		  		'users'=>array('@'),
			),
		);
	}
	/**
	 * Valida que todas las peticiones vienen por Ajax y no son
	 * accedidas directamente
	 *
	 * @return bolean 	Si se debe ejecutar la acción
	 */
	protected function beforeAction($action){
		if (Yii::app()->request->isAjaxRequest) {
			return true;
		} else {
			$this->redirect(Yii::app()->urlManager->createUrl('/'));
			return false;
		}
	}

	protected function sendResponse($status = 200, $body = '', $contentType = 'application/json')
	{
		$statusHeader = 'HTTP/1.1 ' . $status . ' ' . $this->getStatusCodeMessage($status);
		header($statusHeader);
		header('Content-type: ' . $contentType);
		echo $body;

		Yii::app()->end();
	}
/******************************** LANDINGS ***************************************/

	/**
	 * Funcion para registrar un usuario desde un landing mediante su correo,
	 * si ya existe devuelva
	 * @return mixed 	Devuelve codigo HTTP dependiendo del valor: 200 creó la cuenta, 301 deber redirigir, 403 no autorizado (pedir clave)
	 */
	public function actionRegFromLanding()
	{
		$email = isset($_POST['email']) ? $_POST['email'] : null;

		if($email){
			$sc = new SiteController('site');
			$user = $sc->loginFromMail($email);

			if($user)
				echo $user->id_user;
			else
				$this->sendResponse(500);

		} else {
			$this->sendResponse(500);
		}
	}

	/**
	 *  Función para almacenar las respuestas de un landing
	 */
	public function  actionLandingAnswers()
	{
		$email = isset($_POST['email']) ? $_POST['email'] : null;
		$answers = isset($_POST['answers']) ? $_POST['answers'] : null;

		if($email && is_array($answers)){
			$user = User::model()->find(array(
				'select'=>'id_user',
				'condition' => 'email1=:email1 OR email2=:email2',
				'params' => array(':email1'=>$email, ':email2'=>$email)
			));

			if($user){
				$transaction= Yii::app()->db->beginTransaction();

				foreach($answers as $id_question => $id_answer){
					$cmpAU = new CmpAnswerUser();
					$cmpAU->id_user = $user->id_user;
					$cmpAU->id_question = $id_question;
					$cmpAU->id_answer = $id_answer;

					if(!$cmpAU->save()){
						$transaction->rollback();
						$this->sendResponse(500);
					}
				}

				$transaction->commit();
			}

		} else {
			$this->sendResponse(500);
		}
	}

	public function actionTags()
	{
		if(isset($_GET['term'])){
			$term = $_GET['term'];
			$tags = Tag::model()->findAll(array(
				'select'=>'id_tag,tag',
				'condition'=>'tag LIKE :term',
				'params'=>array(':term'=>"$term%")
			));

			$tagArray = array();

			foreach ($tags as $tag) {
				$tagArray[] = array(
					'label'=>$tag->tag,
					'value'=>$tag->tag
				);
			}

			echo CJSON::encode($tagArray);
		}
	}

	public function actionRegions()
	{
		if(isset($_POST['country'])){
			$regions = Region::model()->findAll(array(
				'condition'=>'id_country=:id_country',
				'params'=>array(':id_country'=>$_POST['country']),
				'order'=>'name ASC'
			));
			echo CJSON::encode($regions);
		}
	}
}