<?php
/**
 * Crea el crossover de los individuos
 *
 */
function crossover($plan1, $plan2)
{
	$to = floor(StudyPlan::MAX_COURSES / 2);

	for($i = 0; $i < $to; $i++){
		$plan1[$i] = $plan2[StudyPlan::MAX_COURSES - 1 -$i];
	}

	return rand(0,1) ? $plan1 : $plan2;
}


/**
 * Función que evalua a un curso, es un promedio ponderado
 * 
 * @param  Array $courses 		plan de estudio a evaluar
 * @param  Array 	$this->userTags 	tags del usuario, los cuales se busca concordar
 * @return Integer         		puntaje obtenido en la evaluación
 */
function evaluate($plan, $userTags)
{
	$puntaje = 0; 

	foreach($plan->planCourses as $course){
		foreach($course->courseTags as $ct){
			if(in_array($ct->tag->tag, $userTags)){
				$puntaje += $ct->correlation->value; 
				//A futuro se debe sumar la multiplicación del nivel del usuario
			}
		}
	}

	return $puntaje;
}

function mutation($courses) {
	$indexToChange = rand(0, count($courses));
	$currentIndexes = array();

	foreach($courses as $course){
		$currentIndexes[] = $course->id_course;
	}

	$condition = sprintf("id_course NOT IN (%s)", "'" . implode("','", $currentIndexes) ."'");
	$courses[$indexToChange] = Course::model()->find($condition);

	return $courses;
}


class PlandeestudioController extends Controller
{
	public $defaultAction = 'ver';

	public function actionCrear()
	{
		$ga = new GA();
		$ga->userTags = array('php','frontend','ios');
		$ga->population = StudyPlan::createPopulation($ga->userTags);
		$ga->fitness_function = 'evaluate';
		$ga->num_couples = 1;
		$ga->death_rate = 0;
		$ga->generations = 100;
		$ga->crossover_functions = 'crossover';
		$ga->mutation_function = 'mutation';
		$ga->mutation_rate = 1;

		//$this->printCourses($ga->population);
		$ga->evolve();
		//$this->printCourses($ga->population);
		//echo var_export($ga->userTags);die;
		$this->printCourses(GA::select($ga->population,'evaluate', $ga->userTags,1)); //El mejor resultado
	}

	public function actionVer()
	{
		$this->render('ver');
	}

	public function printCourses($plans){
		echo '<br>-----------------<br>';
		$i = 1;
		
		foreach ($plans as $plan) {
			echo '<br /><br />Plan #' . $i . '<br />';
			foreach($plan->planCourses as $course){
				echo '<br>'. $course->name;
			}
			$i++;
		}
	}

	// Uncomment the following methods and override them if needed
	/*
	public function filters()
	{
		// return the filter configuration for this controller, e.g.:
		return array(
			'inlineFilterName',
			array(
				'class'=>'path.to.FilterClass',
				'propertyName'=>'propertyValue',
			),
		);
	}

	public function actions()
	{
		// return external action classes, e.g.:
		return array(
			'action1'=>'path.to.ActionClass',
			'action2'=>array(
				'class'=>'path.to.AnotherActionClass',
				'propertyName'=>'propertyValue',
			),
		);
	}
	*/
}