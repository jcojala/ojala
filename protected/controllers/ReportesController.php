<?php
class ReportesController extends CController
{
	public $layout='//layouts/admin';

    public function actions()
    {
        return array(
            'page'=>array(
                'class'=>'CViewAction',
            ),
        );
    }

    public function filters()
    {
        return array(
            'accessControl',
        );
    }

    public function accessRules()
    {
        return array(
            array('allow',
                'actions'=>array('churnRate', 'churnDeudores', 'churnDeudores90'),
                'users'=>array('@'),
                'expression'=>"isset(Yii::app()->user->utype) && (Yii::app()->user->utype==='Administrador')",
            ),
            array('deny',
                'users'=>array('*'),
            ),
        );
    }

	public function actionChurnRate($deudores = '')
    {
        $monthToStudy = 20;
        $tipo='month';

        $connection=Yii::app()->db2;
        $command=$connection->createCommand();
        $command->setSelect(@"rp.id_time, rp.month AS mes, rp.year AS ano");
        $command->setFrom(RepHappen::model()->tableName() . ' rh');
        $command->join(RepTime::model()->tableName() . ' rp', "rp.id_time=rh.id_time");
        $command->setWhere("rp.id_time > '2013-03-05' AND rp.id_time < '" . date('Y-m-d') . "' AND rh.id_event IN (2,3,6)");
        $command->setGroup("rp.".$tipo.', rp.year');
        $command->setOrder("rp.year ASC, rp.month ASC");
        $list=$command->queryAll();


        $acumulado=0;
        $cancelacionesAnteriores = 0;
        $listaCohort=array();
        $mes_recorrido=0;
        $total_cancelados=0;

        foreach ($list as $value)
        {
            $anorep=$value['ano'];
            $mesrep=$value['mes'];

            $ano=$value['ano'];
            $mes=$value['mes'];

            $command=$connection->createCommand();
            $command->setSelect("h.id_happen");
            $command->setFrom(RepHappen::model()->tableName() . ' h');
            $command->join(RepEvent::model()->tableName() . ' e', "h.id_event=e.id_event");
            $command->join(RepTime::model()->tableName() . ' t', "h.id_time=t.id_time");
            $command->setGroup("h.id_user");
            $command->setWhere("e.`na_event`='suscriptor' AND t.`month`=".$mesrep." AND t.`year`=".$anorep);
            $sus=count($command->queryAll());

            $meses=array('suscriptores'=>$sus);
            $base=$sus;

            for ($i=0; $i < $monthToStudy; $i++)
            {
                $command=$connection->createCommand()
                    ->select("COUNT(h.`id_happen`) as cuantos")
                    ->from(RepHappen::model()->tableName() . ' h');

                $subQuery = sprintf("(SELECT id_suscription, id_user FROM rep_happen
                                WHERE MONTH(id_time)=%s AND YEAR(id_time)=%s
                                AND id_event = " . RepEvent::SUBSCRIPTOR_ID . ")", $mesrep, $anorep);

                $command->join(RepTime::model()->tableName() . ' rt', 'rt.id_time=h.id_time')
                    ->join($subQuery . ' AS sq', 'sq.id_suscription = h.id_suscription');

                $id_events = '6';
                if(!empty($deudores))
                {
                    if($deudores === 90)
                    {
                        if(strtotime("now -90 days") > strtotime($ano."-".$mes."-01")){
                            $id_events = RepEvent::DEBT_ID . ',' . RepEvent::CANCELATION_ID;
                            $command->setGroup('h.id_user');
                        } else {
                            $id_events = RepEvent::CANCELATION_ID;
                        }
                    }
                    else
                    {
                        $id_events = RepEvent::DEBT_ID . ',' . RepEvent::CANCELATION_ID;
                        $command->setGroup('h.id_user');
                    }
                }

                $command->where(sprintf("rt.`month`=%d AND rt.`year`=%d AND h.id_event IN(%s)", $mes, $ano, $id_events))
                    ->order('h.id_event DESC');

                //Si tiene GROUP BY entonces se cuentan todos los registros obtenidos
                $groupBy = $command->getGroup();
                if(empty($groupBy)){
                    $cuantos = $command->queryScalar();
                } else {
                    $resultado = $command->queryAll();
                    $cuantos = count($resultado);
                }

                $total_cancelados += $cuantos;

                $meses += array(
                    'Mes'.$i => $cuantos,
                    'PMes'.$i => ($base == 0) ? '0' : round((($cuantos*100)/$base), 2),
                );

                $ano = date('Y', strtotime($ano."-".$mes."-01 +1 ".$tipo));
                $mes = date('m', strtotime($ano."-".$mes."-01 +1 ".$tipo));
                $base -= $cuantos;
            }

            //Calculo las cancelaciones de este mes
            $command->reset();
            $command->setSelect("COUNT(h.id_happen)");
            $command->setFrom(RepHappen::model()->tableName() . ' h');
            $command->join(RepTime::model()->tableName() . ' t', "h.id_time=t.id_time");
            $command->setWhere(
                sprintf("h.`id_event` IN (%s) AND t.`month`=%s AND t.`year`=%s", $id_events, $mesrep , $anorep)
            );
            $command->setGroup("h.id_user");
            $resultado = $command->queryAll();
            $cancelacionesAnteriores = count($resultado);

            $acumulado += $sus - $cancelacionesAnteriores;

            $listaCohort[]=array(
                'mes'=>$value['mes'],
                'ano'=>$value['ano'],
                'acumulado'=>$acumulado,
            ) + $meses;

            $mes_recorrido++;
        }

        $this->render('/admin/reportes/churnRate', array(
            'listaCohort'=>$listaCohort,
            'deudores'=>$deudores,
            'monthToStudy' => $monthToStudy,
            'total_cancelados' => $total_cancelados
    	));
    }

    public function actionChurnDeudores()
    {
        $this->actionChurnRate('todos');
    }

    public function actionChurnDeudores90()
    {
        $this->actionChurnRate(90);
    }
}