<?php

/**
 * Clase con funciones genéricas para probar los pagos
 *
 * @author Miguel Useche
 */
class PaymentTest extends WebTestCase
{
	const COURSES_URL = 'site/cursos';
	const SUSCRIPTION_URL = 'site/suscripciones';
	const PHONE_ADVISOR_URL = 'site/confirmacionAsesor';
	const PAY_SUSCRIPTION_URL = 'site/pagoSuscripcion?';
	const COUNTRY = 'Venezuela';
	const STATE = 'San Cristobal';
	const PHONE_NUMBER = '+582763411033';



	/**
	 * Elimina todas las suscripciones de un comprador
	 * @param  string 	$email 		Correo del usuario a eliminarle suscripciones
	 * @return  bollean         		Cantidad de filas afectadas
	 */
	protected function _deleteAnyPayment($email)
	{
		$sql = "DELETE FROM link WHERE id_user=(SELECT id_user FROM `user` WHERE email1='${email}');
			DELETE FROM suscription_status WHERE id_suscription IN (SELECT id_suscription FROM `suscription` WHERE id_user=(SELECT id_user FROM `user` WHERE email1='${email}'));
			DELETE FROM pay WHERE email = '${email}';
			DELETE FROM course_suscription WHERE id_user=(SELECT id_user FROM `user` WHERE email1='${email}');
			DELETE FROM happen WHERE id_user=(SELECT id_user FROM `user` WHERE email1='${email}');
			DELETE FROM group_suscription WHERE id_user=(SELECT id_user FROM `user` WHERE email1='${email}');
			DELETE FROM suscription  WHERE id_user=(SELECT id_user FROM `user` WHERE email1='${email}');
			UPDATE `user` SET phone='' WHERE email1='${email}';"
			;

		$conn = Yii::app()->ojalabDB;
		$command=$conn->createCommand($sql);
		$command->execute();
		echo "\nBorrando \los cursos anteriores: " . $sql;
		return true;
	}

	protected function _selectCourse()
	{
		$this->open(self::COURSES_URL);
		$this->click("css=span.text-link");
		$this->waitForPageToLoad(self::PAGE_LOAD_WAIT_TIME);
		$this->click("link=Tomar curso →");
		$this->waitForPageToLoad(self::PAGE_LOAD_WAIT_TIME);
		$this->click("id=buy");
		$this->waitForPageToLoad(self::PAGE_LOAD_WAIT_TIME);
	}

	protected function _selectSuscription()
	{
		$this->open(self::SUSCRIPTION_URL);
		$this->waitForPageToLoad(self::PAGE_LOAD_WAIT_TIME);
		$this->waitForTextPresent('Financia tu educación');
		$this->click("link=Activar acceso ahora");
		//sleep(self::ANIMATION_WAIT_TIME);
	}

	protected function _selectSuscriptionWithParams( $queryParams)
	{
		$this->open(self::SUSCRIPTION_URL . $queryParams);
		$this->waitForPageToLoad(self::PAGE_LOAD_WAIT_TIME);
		$this->waitForTextPresent('Financia tu educación');
		$this->click("link=Activar acceso ahora");
	}

	public function _checkPhonePage()
	{
		$selBtn = 'name=yt0';

		//Rellena mal y comprueba mensajes de error
		$this->waitForTextPresent('¡Bienvenido a Oja.la!');
		$this->click($selBtn);
		$this->waitForTextPresent('Debes escribir tu número de teléfono');

		//Rellena bien
		$this->select('id=country-list', "label=" . self::COUNTRY);
		sleep(self::AJAX_LOAD_WAIT_TIME/1000);
		$this->select('id=region-list', "label=" . self::STATE);
		$this->click('id=inlineCheckbox1');
		sleep(self::AJAX_LOAD_WAIT_TIME/1000);
		$this->type('id=AdvisorForm_phoneNumber',self::PHONE_NUMBER);
		$this->click($selBtn);
		$this->waitForTextPresent('¡Listo, datos enviados!');

		//Comprueba que si ya tiene telefono lo envie al listado de cursos
		$this->open(self::PHONE_ADVISOR_URL);
		$this->waitForPageToLoad(self::PAGE_LOAD_WAIT_TIME);
		$this->waitForTextPresent('¡Gracias por reactivar tu cuenta!');
	}

	public function _checkValidationPage()
	{
		$this->waitForTextPresent('Estamos confirmando');
		$this->waitForTextPresent('¡Bienvenido a Oja.la!');
	}


}
