<?php

class PaypalTest extends PaymentTest
{
	const EMAIL = 'skatox@gmail.com';
	const PAYPAL_PASS = '1234567890';

	private function fillForm()
	{
		$this->type("id=login_email", self::EMAIL);
		$this->type("id=login_password", self::PAYPAL_PASS);
		$this->clickAndWait("name=login.x");
		$this->clickAndWait("name=submit.x");
		$this->waitForPageToLoad(self::PAGE_LOAD_WAIT_TIME);
		$this->clickAndWait("id=merchantRet");
	}

	public function buySuscription($plan='37-st')
	{
		$this->_selectSuscriptionWithParams('plan='. $plan .'&email=' . self::EMAIL . '&gw=paypal');
		$this->fillForm();
		$this->waitForPageToLoad(self::PAGE_LOAD_WAIT_TIME);
		$this->_checkValidationPage();
	}


	public function testBuySuscription()
	{
		$this->_deleteAnyPayment(self::LOGIN_USER);
		$this->_login();
		$this->_selectSuscription();

		$this->click("link=Cuenta de Paypal");
		$this->clickAndWait("id=button2");
		$this->fillForm();

		$this->waitForPageToLoad(self::PAGE_LOAD_WAIT_TIME);
		$this->_checkValidationPage();

		$this->clickAndWait("css=span.glyphicon.glyphicon-user");
		$this->assertTextPresent("Mi suscripción");
		$this->_deleteAnyPayment(self::LOGIN_USER);
		$this->_deleteAnyPayment(self::LOGIN_USER);
	}

	public function testExistingUserBuyingSubscription()
	{
		$this->_deleteAnyPayment(self::LOGIN_USER);
		$this->_selectSuscriptionWithParams('?email=' . self::EMAIL . '&gw=paypal');
		$this->clickAndWait("id=button2");
		$this->fillForm();

		$this->waitForElementPresent('name=LoginForm[username]');
		$this->type("name=LoginForm[username]", self::LOGIN_USER);
		$this->type('name=LoginForm[password]','1234567890');
		$this->click("//input[@value='Entrar a mi cuenta en Oja.la']");

		$this->_checkValidationPage();
	}
}
