<?php

require('StripeTest.php');
require('PaypalTest.php');

class ChangeGatewayTest extends PaymentTest
{
	const EMAIL = 'contacto@migueluseche.com';
	const CREDIT_CARD = '4242424242424242';
	const EXP_YEAR = '2028';
	const EXP_MONTH = '12 - Diciembre';
	const CVC = '1234';
	const PAYPAL_EMAIL= 'skatox@gmail.com';
	const PAYPAL_PASS = '1234567890';

	private function checkActiveSubscription($gateway = '', $active = 1)
	{
		$extraWhere = '';
		switch($gateway) {
			case 'paypal':
				$extraWhere = "AND s.id_service NOT REGEXP '^sub'";
				break;
			case 'stripe':
				$extraWhere = "AND s.id_service REGEXP '^sub'";
				break;
		}

		$sql = "SELECT id_suscription FROM " . Suscription::model()->tableName() . " s " .
		"INNER JOIN " . SuscriptionType::model()->tableName() . " st ON st.id_stype = s.id_stype WHERE " .
		"s.active=${active} AND s.id_user=(SELECT id_user FROM user WHERE email1='". self::EMAIL . "') " .
		" AND st.recurrence=1 $extraWhere ORDER BY s.id_suscription DESC";

		echo "\nSQL:$sql\n";

		$conn = Yii::app()->ojalabDB;
		$command=$conn->createCommand($sql);
		$suscription = $command->queryScalar();

		echo "\nSuscripcion: " . $suscription;

		return $suscription !== false;
	}

	public static function hasActiveSubscription($userId, $gateway = '', $active = 1)
	{
		$criteria = new CDbCriteria();
		$criteria->addCondition('t.active=:active');
		$criteria->addCondition('t.id_user=:id');
		$criteria->addCondition('st.recurrence=1');
		$criteria->join = 'INNER JOIN ' . SuscriptionType::model()->tableName() . ' st ON st.id_stype = t.id_stype';
		$criteria->params = array(':id'=>$userId, ':active'=> $active);
		$criteria->order = 't.id_suscription DESC';

		switch($gateway) {
			case 'paypal':
				$criteria->addCondition("t.id_service NOT REGEXP '^sub'");
				break;
			case 'stripe':
				$criteria->addCondition("t.id_service REGEXP '^sub'");
				break;
		}

		return Suscription::model()->exists($criteria);
	}


	protected function _buyWithStripe()
	{
		$this->_selectSuscription();
		$this->type("id=email", self::EMAIL);
		$this->type("id=number", self::CREDIT_CARD);
		$this->select("id=exp-year", "label=" . self::EXP_YEAR);
		$this->select("id=exp-month", "label=" . self::EXP_MONTH);
		$this->type("id=cvc", self::CVC);
		$this->click("id=button");
		$this->waitForTextPresent("Estamos confirmando la transacción");
		$this->waitForTextNotPresent("Estamos confirmando la transacción");
	}

	protected function _buyWithPaypal()
	{
		$this->_selectSuscription();
		$this->click("link=Cuenta de Paypal");
		$this->clickAndWait("id=button2");
		$this->type("id=login_email", self::PAYPAL_EMAIL);
		$this->type("id=login_password", self::PAYPAL_PASS);
		$this->clickAndWait("name=login.x");
		$this->clickAndWait("name=submit.x");
		$this->waitForPageToLoad(self::PAGE_LOAD_WAIT_TIME);
		$this->clickAndWait("id=merchantRet");
		$this->waitForTextPresent("Estamos confirmando la transacción");
		$this->waitForTextNotPresent("Estamos confirmando la transacción");
	}

	public function _testStripeToPaypal()
	{
		$this->_deleteAnyPayment(self::LOGIN_USER);
		$this->_deleteAnyPayment(self::LOGIN_USER);
		$this->_login();

		//Copia de compra de suscripcion por Stripe
		$this->_buyWithStripe();
		$this->assertTrue($this->checkActiveSubscription('stripe',  1));

		//Copia del metodo de suscripcion de paypal
		$this->_buyWithPaypal();
		sleep(60); //esperar por el hook de paypal
		$this->assertTrue($this->checkActiveSubscription('paypal',  1));
		sleep(10); //esperar por el hook de paypal
		$this->assertTrue($this->checkActiveSubscription('stripe',  0));

		$this->_deleteAnyPayment(self::LOGIN_USER);
	}


	public function testPaypalToStripe()
	{
		$this->_deleteAnyPayment(self::LOGIN_USER);
		$this->_deleteAnyPayment(self::LOGIN_USER);
		$this->_login();

		//Copia del metodo de suscripcion de paypal
		$this->_buyWithPaypal();
		$this->assertTrue($this->checkActiveSubscription('paypal',  1));

		//Copia de compra de suscripcion por Stripe
		$this->_buyWithStripe();
		sleep(40);
		$this->assertTrue($this->checkActiveSubscription('stripe',  1));
		sleep(10); //esperar por el hook de Stripe
		$this->assertTrue($this->checkActiveSubscription('paypal',  0));

		$this->_deleteAnyPayment(self::LOGIN_USER);
	}
}
