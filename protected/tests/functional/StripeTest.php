<?php

class StripeTest extends PaymentTest
{
	const EMAIL = 'contacto@migueluseche.com';
	const CREDIT_CARD = '4242424242424242';
	const EXP_YEAR = '2028';
	const EXP_MONTH = '12 - Diciembre';
	const CVC = '1234';


	private function fillForm()
	{
		$this->verifyValue("id=email", self::EMAIL);
		$this->type("id=number", self::CREDIT_CARD);
		$this->select("id=exp-year", "label=" . self::EXP_YEAR);
		$this->select("id=exp-month", "label=" . self::EXP_MONTH);
		$this->type("id=cvc", self::CVC);
		$this->click("id=button");
	}

	public function buySuscription($plan='37-st')
	{
		$this->_selectSuscriptionWithParams('plan='. $plan .'&email=' . self::EMAIL . '&gw=stripe');
		$this->fillForm();
		$this->waitForPageToLoad(self::PAGE_LOAD_WAIT_TIME);
		$this->_checkValidationPage();
	}

	public function _testBuySubscription()
	{
		$this->_deleteAnyPayment(self::LOGIN_USER);
		$this->_deleteAnyPayment(self::LOGIN_USER);
		$this->_login();
		$this->_selectSuscription();

		//Prueba que se están validando los campos del formulario
		$this->type("id=email", self::EMAIL);
		$this->click("id=button");
		$this->waitForTextPresent("El número de la tarjeta es inválido");
		$this->type("id=number", self::CREDIT_CARD);
		$this->click("id=button");
		$this->waitForTextPresent("La fecha de vencimiento es inválida");
		$this->select("id=exp-year", "label=" . self::EXP_YEAR);
		$this->select("id=exp-month", "label=" . self::EXP_MONTH);
		$this->click("id=button");
		$this->waitForTextPresent("El código de verificación es inválido");
		$this->type("id=cvc", self::CVC);
		$this->click("id=button");

		$this->_checkValidationPage();
		$this->_checkPhonePage();

		$this->clickAndWait("css=span.glyphicon.glyphicon-user");
		$this->assertTextPresent("Mi suscripción");
		$this->_deleteAnyPayment(self::LOGIN_USER);
		$this->_deleteAnyPayment(self::LOGIN_USER);
	}

	public function testExistingUserBuyingSubscription()
	{
		$this->_deleteAnyPayment(self::LOGIN_USER);
		$this->_deleteAnyPayment(self::LOGIN_USER);
		$this->_selectSuscriptionWithParams('?email=' . self::EMAIL . '&gw=stripe');

		$this->fillForm();
		$this->waitForPageToLoad(self::PAGE_LOAD_WAIT_TIME);

		$this->assertElementPresent('name=LoginForm[username]');
		$this->verifyValue('name=LoginForm[username]', self::EMAIL);
		$this->type('name=LoginForm[password]','1234567890');
		$this->click("//input[@value='Entrar a mi cuenta en Oja.la']");

		$this->_checkValidationPage();
	}
}
