<?php

class SiteTest extends WebTestCase
{
	public function testIndex()
	{
		$this->open('');
		$this->assertTextPresent('Aprende paso a paso');
	}

	public function testLoginLogout()
	{
		$this->open('');
		// ensure the user is logged out
		if($this->isTextPresent('Cerrar sesión'))
			$this->clickAndWait("link='Cerrar sesión'");

		// test login process, including validation
		$this->clickAndWait("//a[.='Entrar a mi cuenta']");
		$this->assertElementPresent('name=LoginForm[username]');
		$this->type('name=LoginForm[username]','contacto@migueluseche.com');
		$this->click("//input[@value='Entrar a mi cuenta en Oja.la']");
		$this->waitForTextPresent('Disculpa, pero en verdad necesitamos tu clave');
		$this->type('name=LoginForm[password]','1234567890');
		$this->clickAndWait("//input[@value='Entrar a mi cuenta en Oja.la']");
		$this->assertTextNotPresent('Disculpa, pero en verdad necesitamos tu clave');
		$this->waitForTextPresent('Cerrar sesión');

		// test logout process
		$this->assertTextNotPresent('Entrar a mi cuenta');
		$this->clickAndWait("//a/span[.='Cerrar sesión']");
		$this->assertTextPresent('Entrar a mi cuenta');
	}
}
