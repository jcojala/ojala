<?php

/**
 * Change the following URL based on your server configuration
 * Make sure the URL ends with a slash so that we can use relative URLs in test cases
 */
define('TEST_BASE_URL','http://ojalab.com/dev/');
//define('TEST_BASE_URL','http://localhost/ojala/');
/**
 * The base class for functional test cases.
 * In this class, we set the base URL for the test application.
 * We also provide some common methods to be used by concrete test classes.
 */
class WebTestCase extends CWebTestCase
{
    const LOGIN_USER = 'contacto@migueluseche.com';
    const PAGE_LOAD_WAIT_TIME = 60000;
    const AJAX_LOAD_WAIT_TIME = 3000;
    const ANIMATION_WAIT_TIME = 3000;

    /**
     * Sets up before each test method runs.
     * This mainly sets the base URL for the test application.
     */
    protected function setUp()
    {
        $this->setBrowser('*firefox');
        $this->setBrowserUrl(TEST_BASE_URL);
        $this->setTimeout(self::PAGE_LOAD_WAIT_TIME);
        parent::setUp();
        $this->prepareTestSession();
    }

    protected function _login()
    {
        $this->windowMaximize();
        $this->open('');
        $this->clickAndWait("//a[.='Entrar a mi cuenta']");
        $this->assertElementPresent('name=LoginForm[username]');
        $this->type('name=LoginForm[username]', self::LOGIN_USER);
        $this->type('name=LoginForm[password]','1234567890');
        $this->click("//input[@value='Entrar a mi cuenta en Oja.la']");
        $this->waitForPageToLoad(self::PAGE_LOAD_WAIT_TIME);
    }
}
