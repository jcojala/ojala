'use strict';

var gulp = require('gulp'),
    uglify = require('gulp-uglify'),
    concat = require('gulp-concat');

gulp.task('scripts', function() {
  return gulp.src('js/core/*.js')
    .pipe(concat('core.js'))
    .pipe(uglify())
    .pipe(gulp.dest('js/'));
});

gulp.task('default', function() {
	gulp.start('scripts');
});